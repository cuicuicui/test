/*!
 * /**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ** /
 */
(function(e, a) { for(var i in a) e[i] = a[i]; }(exports, /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(1);


/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./src/secswans/lib/secswans.js
/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

// handle encryption routines for swans protocol implementation, namely rsa and aes
// methods beginning with a _ are internal and should  not be used
class secswans_Secswans{

	constructor(){
		// initialize some constants for aes
		this.AES_IRR = 0b100011011; // aes irreductible polynom
		this.sbox = this._generateSbox(); // permutation table
		this.t2 = this._t2(); // polynome multiplication by 2
		this.reverseSbox = this._generateReverseSbox( this.sbox );
		
		// available encryption methods
		this.ENCTYPE_NONE = 0; // no encryption used
		this.ENCTYPE_DH_2048_256 = 1; // DH with 2048b prime and 256bit prime subgroup, defined in rfc 5114
		this.ENCTYPE_RSA_1024 = 16; // RSA 1024 bits
		this.ENCTYPE_RSA_2048 = 17; // RSA 2048 bits
		this.ENCTYPE_RSA_4096 = 18; // RSA 4096 bits

		// available hash methods
        this.HASHTYPE_SHA_256 = 4;
        this.HASHTYPE_SHA_512 = 5;

		// expected hash/signature size for various methos :
		this.TOKEN_SIZE = {
            [ this.ENCTYPE_DH_2048_256 ] : 32,
            [ this.ENCTYPE_RSA_1024 ] : 128,
			"rsa1024" : 128,
            [ this.ENCTYPE_RSA_2048 ] : 256,
            "rsa2048" : 256,
            [ this.ENCTYPE_RSA_4096 ] : 512,
            "rsa4096" : 512,
            [ this.HASHTYPE_SHA_256 ] : 32,
            "sha256" : 32,

		};

		// constants for big integer manipulation
		this.supmask = new Uint8Array([128, 192, 224, 240, 248, 252 , 254, 255]); // supMask take only the top i+1 bits of a byte
		this.infmask = new Uint8Array([255, 127, 63, 31, 15, 7, 3, 1, 0]); // infmask take only the lower 8-i bits of a byte
		this.bm = new Uint8Array([128, 64, 32, 16, 8, 4, 2, 1]); 
		
		// initialize the big num generator : those are store in fixed size 0f 256*4 bytes
		this._lib = this._rsaLibGen();
		
		// constants for DH
		this._RFC5114_2048_256_P = this._hexstrToUint8Array("87A8E61DB4B6663CFFBBD19C651959998CEEF608660DD0F25D2CEED4435E3B00E00DF8F1D61957D4FAF7DF4561B2AA3016C3D91134096FAA3BF4296D830E9A7C209E0C6497517ABD5A8A9D306BCF67ED91F9E6725B4758C022E0B1EF4275BF7B6C5BFC11D45F9088B941F54EB1E59BB8BC39A0BF12307F5C4FDB70C581B23F76B63ACAE1CAA6B7902D52526735488A0EF13C6D9A51BFA4AB3AD8347796524D8EF6A167B5A41825D967E144E5140564251CCACB83E6B486F6B3CA3F7971506026C0B857F689962856DED4010ABD0BE621C3A3960A54E710C375F26375D7014103A4B54330C198AF126116D2276E11715F693877FAD7EF09CADB094AE91E1A1597",16);
		this._RFC5114_2048_256_G = this._hexstrToUint8Array("3FB32C9B73134D0B2E77506660EDBD484CA7B18F21EF205407F4793A1A0BA12510DBC15077BE463FFF4FED4AAC0BB555BE3A6C1B0C6B47B1BC3773BF7E8C6F62901228F8C28CBB18A55AE31341000A650196F931C77A57F2DDF463E5E9EC144B777DE62AAAB8A8628AC376D282D6ED3864E67982428EBC831D14348F6F2F9193B5045AF2767164E1DFC967C1FB3F2E55A4BD1BFFE83B9C80D052B985D182EA0ADB2A3B7313D3FE14C8484B1E052588B9B7D2BBD2DF016199ECD06E1557CD0915B3353BBB64E0EC377FD028370DF92B52C7891428CDC67EB6184B523D1DB246C32F63078490F00EF8D647D148D47954515E2327CFEF98C582664B4C0F6CC41659",16);
	
		// some parameters constants
		this.REQUEST_KEY_STRENGTH = 32; // byte length of symetric key
	}
	
	////////////////////////////////////////////////////////////////////
	////////////////// SECSWANS METHODS ////////////////////////////////
	////////////////////////////////////////////////////////////////////
	
	// given a request key with a private part, and a foreign key(receiver) with a public part, compute the public local key
	computePublicKey(requestKey, receiverKey){
		let type = receiverKey.type;
		switch( parseInt(type) ){
			case this.ENCTYPE_NONE : 
				return new Uint8Array();
			case this.ENCTYPE_DH_2048_256 :
				return this._dhToPublic(requestKey.private, this._RFC5114_2048_256_G, this._RFC5114_2048_256_P);
			default : 
				throw "Pubgen : Invalid encryption type : "+type;
				break;
		}
	}
	
	// create a new transaction key, whose data can build a symetric encryption key, given another public key from the receiver
	createRequestKey(){
		return this.createKey(this._random(this.REQUEST_KEY_STRENGTH)); 
	}
	
	// encrypt message with a symetric key
	symetricEncrypt(msg, symkey){
		let res = this.aesCbcCypher(msg, symkey);
        return res;
	}
	
	// decrypt a message with a symetric key
	symetricDecrypt(msg, symkey){
		let res = this.aesCbcInverseCypher(msg, symkey);
		return res;
	}
	
	// compute the aes key to be used to decrypt/encrypt message, from two set of keys
	computeSymetricKey(keys1, keys2){
        let type = parseInt( keys2.hasOwnProperty("type") ? keys2.type : keys1.type);
        let res;
		switch(type){
			case this.ENCTYPE_NONE : 
				return null;
			case this.ENCTYPE_DH_2048_256 :
				if(keys1.public && keys2.private){
                    res = this._commonSecretDh(keys1.public, keys2.private, this._RFC5114_2048_256_P, true);
				}
				else if(keys2.public && keys1.private){
                    res = this._commonSecretDh(keys2.public, keys1.private, this._RFC5114_2048_256_P, true);
				}
				return res;
				break;
			default : 
				throw "Symgen : Invalid encryption type : "+type;
				break
		}
	}
	
	// generate public and private key for a given encryption type
	generateKey(type){
		switch(type){
			case this.ENCTYPE_NONE : 
				return this.createKey( new Uint8Array(), new Uint8Array(), this.ENCTYPE_NONE )
			case this.ENCTYPE_DH_2048_256 :
				return this._generateKeyDh(type);
			case this.ENCTYPE_RSA_1024 : 
			case this.ENCTYPE_RSA_2048 : 
			case this.ENCTYPE_RSA_4096 :
				let fullrsa = this._generateKeyRsa(type);
				let privrsa = { n : fullrsa.n, e : fullrsa.e, d: fullrsa.d, type : type };
				let pubrsa = { n : fullrsa.n, e : fullrsa.e , type : type}
				return this.createKey( privrsa, pubrsa, type);
			case this.HASHTYPE_SHA_256 :
				return this.createKey({type:type}, {type:type}, type);
			default : 
				throw "keygen, Invalid encryption type : "+type;
				break
		}
	}
	
	// create a key pair from given data
	createKey( priv, pub, type){
			return {
				type : type,
				private : priv,
				public : pub
			};
	}
	
	// sign a sequence of bytes
	sign(input, privkey){
		if(!secswans_Secswans.prototype.cs)secswans_Secswans.prototype.cs = 1;
		switch(privkey.type){
			case "rsa1024" : 
			case "rsa2048" : 
			case "rsa4096" :
			case this.ENCTYPE_RSA_1024 : 
			case this.ENCTYPE_RSA_2048 : 
			case this.ENCTYPE_RSA_4096 :  
				return this._rsaSign(input, privkey);
			case this.HASHTYPE_SHA_256 :
			case "sha256" :
				return this._sha256Sign(input);
			throw "unsupported sign method : "+privkey.type;
		}
	}

	// check a signature of a sequence of bytes	
	checkSign(input, pubkey, expectedSignature){
        if(!secswans_Secswans.prototype.us)secswans_Secswans.prototype.us = 1;
		switch(pubkey.type){
			case "rsa1024" : 
			case "rsa2048" : 
			case "rsa4096" : 
			case this.ENCTYPE_RSA_1024 : 
			case this.ENCTYPE_RSA_2048 : 
			case this.ENCTYPE_RSA_4096 :
				return this._rsaCheckSign(input, pubkey, expectedSignature);
            case "sha256" :
            case this.HASHTYPE_SHA_256 :
                return this._sha256CheckSign(input, expectedSignature);
			throw "unsupported check sign method : "+pubkey.type;
		}
	}

	////////////////////////////////////////////////////////////////////
	/////////// DIFFIE HELLMAN METHODS /////////////////////////////////
	////////////////////////////////////////////////////////////////////
	
	// create a key given an encryption type
	_generateKeyDh(type, priv=null){
        if(!secswans_Secswans.prototype.gk)secswans_Secswans.prototype.gk = 1;
		let pub;
		if(!priv) priv = this.createRequestKey().private;
		switch(type){
				case this.ENCTYPE_DH_2048_256 :
					pub =  this.power(this._RFC5114_2048_256_G, priv, this._RFC5114_2048_256_P);
					break;
		}
		return this.createKey(priv, pub, type);
	}
	
	// transform a private key into a public key for DH
	_dhToPublic(priv, g, p){
		return this.power(g, priv, p);
	}
	
	// generate the common secret given a public key and a private key
	_commonSecretDh(pubKey, privKey, prime, hash=false){
		let res = this.power(pubKey, privKey, prime);
		if(hash) res = this.sha256(res);
		return res;
	}

	////////////////////////////////////////////////////////////////////
	/////////////////////  RSA METHODS /////////////////////////////////
	////////////////////////////////////////////////////////////////////
	
	// generate a key for rsa algorithm 
	// return an object with n, e and d as Uint8Array
	_generateKeyRsa(type){
		var B;
		switch(type){
			case this.ENCTYPE_RSA_1024 : 
				B=1024;
				break;
			case this.ENCTYPE_RSA_2048 : 
				B=2048;
				break;
			case this.ENCTYPE_RSA_4096 : 
				B=4096;
				break;
			default :
				throw "unexpected rsa type : "+type;
		}
		var E = "3";
		// TODO : a problem might occur during random generation : for the moment, we just try again until it works
		var hasFailed = true;
        var rsa;
		while(hasFailed){
			try{
                rsa = new this._lib.RSAKey();
                rsa.generate(B,E);
                hasFailed = false;
			}
			catch(e){
				console.log("error during rsa generation : ", e);
			}
		}
		let res = {
			type : type,
			n : rsa.n.toString(16),
			e : (E.length%2 ? "0" : "") + E,
			d : rsa.d.toString(16),
		}
		return res;
	}
	
	// sign a message with a rsa key
	// expect a rsa key with properties n, e and d in hexadecimal string representation
	// bit length accepted for n are : 1024, 2048, 4096
	// expect a hash method : default is sha256
	// expect message to be a Uint8Array
	// hash the message, pad the hash with 0 up to n byte length, encrypt with private key, and return a Uint8Array
	_rsaSign(message, signtools={}){
		let n = signtools.n;
		let e = signtools.e;
		let d = signtools.d;
		let hash = signtools.hash || "sha256";
		let hashSize = this.TOKEN_SIZE[hash] || 32;
		let signatureSize = n.length/2;
		let hash16 = this._hashAndPad(message, hash, hashSize) ;
		// perform encryption
		let encHash = this._rsaPrivateOperation(hash16, n, e, d);
		let sig = encHash.padStart(signatureSize*2, "0");
		return sig;
	}

	// compute sha256 signature of a UintArray
    _sha256Sign(message){
		let sigBytes = this.sha256(message);
		let sigHex = this.bytesToHex(sigBytes);
		return sigHex;
	}

	// perform RSA public operation on an hexadecimal string
	_rsaPublicOperation(inputHex, n, e){
        let rsa = new this._lib.RSAKey();
        rsa.setPublic(n, e);
        var bignum = new this._lib.BigInteger(inputHex,16);
        let res = rsa.doPublic(bignum).toString(16);
        return res;
	}

	// perform the RSA private operation on an hexadecimal string
	_rsaPrivateOperation(inputHex, n, e, d){
        let rsa = new this._lib.RSAKey();
        rsa.setPrivate(n,e,d);
        var bignum = new this._lib.BigInteger(inputHex,16);
        let res = rsa.doPrivate(bignum).toString(16);
        return res;
	}

	// check the signature with a rsa key
	// expect rsa key properties n and e, in hexadecimal string
	// bit length n can be 1024, 2048, 4096
	// expect a hash method : default is sha256
	// expect signature Uint8Array or hexadecimal
	// original message to compare the signature
	// return true if the signatures match
	_rsaCheckSign(message, signtools = {}, signature=""){
		let n = signtools.n;
		let e = signtools.e;
		if( typeof signature !== "string") signature = this.bytesToHex(signature);
        let hash = signtools.hash || "sha256";
        let hashSize = this.TOKEN_SIZE[hash] || 32;
        let signatureSize = signtools.n / 8;
		let expectedHash = this._hashAndPad(message, hash, hashSize);
		// // perform encryption
		let decHash = this._rsaPublicOperation(signature, n, e);
		// convert to hexadecimal
		let decHash16 = decHash.padStart(hashSize*2,"0");

		return decHash16 === expectedHash;
	}

    _sha256CheckSign(message, expectedSignature){
		let hashBytes = this.sha256(message);
		let hashHex = this.bytesToHex(hashBytes);
        expectedSignature = expectedSignature.slice(-64);
		return expectedSignature === hashHex;
	}

	// hash a byte/hexadecimal message with the given hash method, and return an hexadecimal string of the hash, padded with 0 up to the target length
	_hashAndPad(msg, hashMethod, len){
		if(typeof msg === "string") msg = this.hexToBytes(msg);
		if(hashMethod != "sha256") throw "unsupported hash method : "+hashMethod;
		let h = this.sha256(msg);
		let h16 = this.bytesToHex(h);
		if(h16.length>2*len) throw "invalid target length "+len+" for "+hashMethod;
		return h16.padStart(len, "0");
	}
	
	////////////////////////////////////////////////////////////////////
	///////////         HELPERS                     ////////////////////
	////////////////////////////////////////////////////////////////////
	
	// byte to hexadecimal conversion
	bytesToHex(b){
		return Array.from(b).map(i=>(i<16 ? "0" : "")+i.toString(16)).join("");
	}
	
	// hexadecimal to bytes conversion
	hexToBytes(h){
		if(h.length%2) h = "0"+h;
		return new Uint8Array(h.length/2).map((v,i)=>parseInt(h.substr(2*i,2),16));
	}
	
	////////////////////////////////////////////////////////////////////
	/////////// METHODS FOR UINT8ARRAY BIG INTEGERS ////////////////////
	////////////////////////////////////////////////////////////////////
	
	
	// create a random number of the given size
	_random(size){
		let res = new Uint8Array(size);
		for(let i in res) res[i] = Math.random() * 256 >> 0;
		return res;
	}
	
	// check if an array is null
	isNull(a){
		return a.every(i=>i===0);
	}
	
	// auxiliar function to check if an array is equal to one
	isOne (a){
		return a[a.length-1] === 1 && this.isNull(a.slice(0,-1));
	}


	////////////////////////////////////////////////////////////////////
	///////////////////// AES RELATED METHODS //////////////////////////
	////////////////////////////////////////////////////////////////////
	
	// perform aes encryption with cbc mode
	// if not provided, initialization vector is set to 0
	// if data length is not a multiple of 16, append 0 to it
	aesCbcCypher(data, key, vinit = new Uint8Array(16) ){
		let resultLength = data.length + (data.length % 16 ? 16 - (data.length%16) : 0);
		let result = new Uint8Array(resultLength);
		result.set(data);
		let expandedKey = this._keyExpansion(key);
		for(let i=0; i < resultLength; i+=16){
			let state = result.slice(i, i+16);
			for(let j in state) state[j] ^= vinit[j];
			this.aesCypherBlock(state, expandedKey);
			result.set(state, i);
			vinit = state;
		} 
		return result;
	}
	
	// perform aes decryption with cbc mode
	// if not provided, initialization vector is set to 0
	aesCbcInverseCypher(data, key, vinit = new Uint8Array(16) ){
		let result = data.slice();
		let expandedKey = this._keyExpansion(key);
		for(let i=0; i < data.length; i+=16){
			let state = result.slice(i, i+16);
			this.aesInverseCypherBlock(state, expandedKey);
			for(let j in state) state[j] ^= vinit[j];
			result.set(state, i);
			vinit = data.slice(i, i+16);
		} 
		return result;
	}
	
	// cypher a 16 byte long block represented with a Uint8Array
	// state : Uint8Array of size 16 : it is also the state in aes algo, since modifications occurs in place
	// key : Uint8Array of size 16, 24 or 32, or of size 176, 208, 240 if the expanded key is provided directly
	aesCypherBlock(state, key){
		let expandedKey = key.length >= 176 ? key : this._keyExpansion(key);
		this._addRoundKey(state, expandedKey);
		let nr = this._nr(key);
		for(let i=1; i<=nr; i++){
			this._SubBytes(state, this.sbox);
			this._ShiftRows(state);
			if( i !== nr) this._MixColumns(state);
			this._addRoundKey(state, expandedKey, 16*i);
		}
	}
	
	// aes inverse block cypher
	// state : Uint8Array of size 16 : it is also the state in aes algo, since modifications occurs in place
	// key : Uint8Array of size 16, 24 or 32, or of size 176, 208, 240 if the expanded key is provided directly
	aesInverseCypherBlock(state, key){
		let expandedKey = key.length >= 176 ? key : this._keyExpansion(key);
		let nr = this._nr(key);
		this._addRoundKey(state, expandedKey, 16*nr);
		for(let i=nr-1; i>=0; i--){
			this._InvShiftRows(state);
			this._SubBytes(state, this.reverseSbox);
			this._addRoundKey(state, expandedKey, 16*i);
			if(i) this._InvMixColumns(state);
		}
	}	
	
	// generate aes time two correspondance for polynom multiplication
	_t2(){
		let t2 = new Uint8Array(256);
		for(var i = 0; i < 256; i++) {
			t2[i] = i << 1;
			if(i>=128) t2[i] ^= 0x1b;
		}
		return t2;
	}
	
	// calculate the Substitution table Sbox
	_generateSbox(){
		return new Uint8Array([
			0x63, 0x7C, 0x77, 0x7B, 0xF2, 0x6B, 0x6F, 0xC5, 0x30, 0x01, 0x67, 0x2B, 0xFE, 0xD7, 0xAB, 0x76,
			0xCA, 0x82, 0xC9, 0x7D, 0xFA, 0x59, 0x47, 0xF0, 0xAD, 0xD4, 0xA2, 0xAF, 0x9C, 0xA4, 0x72, 0xC0,
			0xB7, 0xFD, 0x93, 0x26, 0x36, 0x3F, 0xF7, 0xCC, 0x34, 0xA5, 0xE5, 0xF1, 0x71, 0xD8, 0x31, 0x15,
			0x04, 0xC7, 0x23, 0xC3, 0x18, 0x96, 0x05, 0x9A, 0x07, 0x12, 0x80, 0xE2, 0xEB, 0x27, 0xB2, 0x75,
			0x09, 0x83, 0x2C, 0x1A, 0x1B, 0x6E, 0x5A, 0xA0, 0x52, 0x3B, 0xD6, 0xB3, 0x29, 0xE3, 0x2F, 0x84,
			0x53, 0xD1, 0x00, 0xED, 0x20, 0xFC, 0xB1, 0x5B, 0x6A, 0xCB, 0xBE, 0x39, 0x4A, 0x4C, 0x58, 0xCF,
			0xD0, 0xEF, 0xAA, 0xFB, 0x43, 0x4D, 0x33, 0x85, 0x45, 0xF9, 0x02, 0x7F, 0x50, 0x3C, 0x9F, 0xA8,
			0x51, 0xA3, 0x40, 0x8F, 0x92, 0x9D, 0x38, 0xF5, 0xBC, 0xB6, 0xDA, 0x21, 0x10, 0xFF, 0xF3, 0xD2,
			0xCD, 0x0C, 0x13, 0xEC, 0x5F, 0x97, 0x44, 0x17, 0xC4, 0xA7, 0x7E, 0x3D, 0x64, 0x5D, 0x19, 0x73,
			0x60, 0x81, 0x4F, 0xDC, 0x22, 0x2A, 0x90, 0x88, 0x46, 0xEE, 0xB8, 0x14, 0xDE, 0x5E, 0x0B, 0xDB,
			0xE0, 0x32, 0x3A, 0x0A, 0x49, 0x06, 0x24, 0x5C, 0xC2, 0xD3, 0xAC, 0x62, 0x91, 0x95, 0xE4, 0x79,
			0xE7, 0xC8, 0x37, 0x6D, 0x8D, 0xD5, 0x4E, 0xA9, 0x6C, 0x56, 0xF4, 0xEA, 0x65, 0x7A, 0xAE, 0x08,
			0xBA, 0x78, 0x25, 0x2E, 0x1C, 0xA6, 0xB4, 0xC6, 0xE8, 0xDD, 0x74, 0x1F, 0x4B, 0xBD, 0x8B, 0x8A,
			0x70, 0x3E, 0xB5, 0x66, 0x48, 0x03, 0xF6, 0x0E, 0x61, 0x35, 0x57, 0xB9, 0x86, 0xC1, 0x1D, 0x9E,
			0xE1, 0xF8, 0x98, 0x11, 0x69, 0xD9, 0x8E, 0x94, 0x9B, 0x1E, 0x87, 0xE9, 0xCE, 0x55, 0x28, 0xDF,
			0x8C, 0xA1, 0x89, 0x0D, 0xBF, 0xE6, 0x42, 0x68, 0x41, 0x99, 0x2D, 0x0F, 0xB0, 0x54, 0xBB, 0x16
		 ]);
	}
	
	// generate the inverse of a transposition array
	_generateReverseSbox(sbox){
		let reverseSbox = new Uint8Array( sbox.length );
		for(let i in sbox) reverseSbox[sbox[i]] = i;
		return reverseSbox;
	}
	
	// SubBytes of the AES : the modification occurs in place
	_SubBytes(state, transpositionTable){
		for(let i in state) state[i] = transpositionTable[state[i]];
	}
	
	// ShiftRows operation of the AES : modification happen in place
	// s : current state 16 bytes Uint8Array
	_ShiftRows(s){
		let t;
		// shift the second row 
		t = s[1];
		s[1] = s[5];
		s[5] = s[9];
		s[9] = s[13];
		s[13] = t;
		// shift the third row 
		t = s[2];
		s[2] = s[10];
		s[10] = t;
		t = s[6];
		s[6] = s[14];
		s[14] = t;
		// shift the fourth row 
		t = s[3];
		s[3] = s[15];
		s[15] = s[11];
		s[11] = s[7];
		s[7] = t;
	}
	
	// implementation of aes InvShiftRows
	_InvShiftRows(s){
		let t;
		// shift the second row 
		t = s[1];
		s[1] = s[13];
		s[13] = s[9];
		s[9] = s[5];
		s[5] = t;
		// shift the third row 
		t = s[2];
		s[2] = s[10];
		s[10] = t;
		t = s[6];
		s[6] = s[14];
		s[14] = t;
		// shift the fourth row 
		t = s[3];
		s[3] = s[7];
		s[7] = s[11];
		s[11] = s[15];
		s[15] = t;
	}
	
	// aes MixColumns
	// modify state s in place
	_MixColumns(s){
		for(let i = 0; i<=12; i+=4) {
			let a0 = s[0 + i];
			let a1 = s[1 + i];
			let a2 = s[2 + i];
			let a3 = s[3 + i];
			s[0+i] = this.t2[a0] ^ a3 ^ a2 ^ this.t2[a1] ^ a1;
			s[1+i] = this.t2[a1] ^ a0 ^ a3 ^ this.t2[a2] ^ a2;
			s[2+i] = this.t2[a2] ^ a1 ^ a0 ^ this.t2[a3] ^ a3;
			s[3+i] = this.t2[a3] ^ a2 ^ a1 ^ this.t2[a0] ^ a0;
		}    
	}
	
	// aes InvMixColumns
	_InvMixColumns(s){
		for(let i=0; i<16; i+=4){
			let s0 = s[i];
			let s1 = s[i+1];
			let s2 = s[i+2];
			let s3 = s[i+3];
			let p1111 = s0 ^ s1 ^ s2 ^ s3; // s0 + s1 + s2 + s3
			let p6464 = this.t2[this.t2[p1111] ^ s0 ^ s2]// 6*s0 + 4*s1 + 6*s2 + 4*s3
			let p4646 = this.t2[this.t2[p1111] ^ s1 ^ s3]// 4*s0 + 6*s1 + 4*s2 + 6*s3
			let p1100 = s0 ^ s1;
			let p0011 = s2 ^ s3;
			s[i]   = this.t2[ p6464 ^ p1100] ^ s1 ^ p0011;
			s[i+1] = this.t2[ p4646 ^ s1 ^ s2] ^ s0 ^ p0011;
			s[i+2] = this.t2[ p6464 ^ p0011] ^ s3 ^ p1100;
			s[i+3] = this.t2[ p4646 ^ s0 ^ s3] ^ s2 ^ p1100;
		}
	}
	
	// aes AddRoundKey
	// modify state s in place
	// key : an array containing the round key parts
	// offset, the offset at wich to start reading the key
	_addRoundKey(s, key, offset=0){
		for(let i=0; i<16; i++){
			s[i] ^= key[offset+i];
		}
	}
	
	// calculate Nr given a byte aes key or extended key
	_nr(key){
		switch(key.length){
			// if the key provided is not extended
			case 16 : return 10;
			case 24 : return 12;
			case 32 : return 14;
			// if the key provided is extended
			case 176 : return 10;
			case 208 : return 12;
			case 240 : return 14;
		}
	}
	
	// calculate nk given the aes key
	_nk(key){
		return key.length/4;
	}
	
	// aes keyExpansion
	// return an array containing 4 * (Nr + 1) bytes
	_keyExpansion(key){
		let tmp;
		let nr = this._nr(key);
		let nk = this._nk(key);
		let nwords = 4 * (nr+1);
		let result = new Uint8Array(nwords * 4);
		// extract the words contained in the key
		result.set(key);
		// keep Rcon up to date
		let rcon = 1;
		for(let i=key.length/4; i < nwords; i++){
			let w0 = result[4*i-4];
			let w1 = result[4*i-3];
			let w2 = result[4*i-2];
			let w3 = result[4*i-1];
			if(i % nk === 0){
				tmp = w0;
				w0 = this.sbox[w1] ^ rcon;
				w1 = this.sbox[w2];
				w2 = this.sbox[w3];
				w3 = this.sbox[tmp];
				// update the Rcon for the next time
				rcon = rcon << 1;
				if(rcon >= 256) rcon ^= 0x11b;
			}
			else if(nk >6 && i%nk === 4){
				w0 = this.sbox[w0];
				w1 = this.sbox[w1];
				w2 = this.sbox[w2];
				w3 = this.sbox[w3];
			}
			result[4 * i] = result[4*i - 4*nk] ^ w0;
			result[4 * i+1] = result[4*i+1 - 4*nk] ^ w1;
			result[4 * i+2] = result[4*i+2 - 4*nk] ^ w2;
			result[4 * i+3] = result[4*i+3 - 4*nk] ^ w3;
		}
		return result;
	}
	
	/////////////////////////////////////////////////////////////////
	//////////  SHA 256 RELATED METHODS /////////////////////////////
	/////////////////////////////////////////////////////////////////
	
	// sha256 algorithm on a Uint8Array
	// return a 32B Uint8Array
	sha256(input){
		// init sha data if it is the first time
		if(!this._K){
			 this._K = this._generateShaK();
		}
		let M = this._sha256Padding(input);
		let H0 = 0x6a09e667, H1=0xbb67ae85, H2=0x3c6ef372, H3=0xa54ff53a,
		    H4 = 0x510e527f, H5=0x9b05688c, H6=0x1f83d9ab, H7=0x5be0cd19;
		let W = new Uint32Array(64);
		for(let i=0; i<M.length; i+=16){
			let a=H0, b=H1, c=H2, d=H3, e=H4, f=H5, g=H6, h=H7;
			for(let t=0; t<64; t++){
				W[t] = t < 16 ? M[i+t] : (
					(W[t-2]>>>17 ^ W[t-2]>>>19  ^ W[t-2]<<15 ^ W[t-2]<<13  ^ W[t-2]>>>10)  + 
					(W[t-15]>>>7 ^ W[t-15]>>>18 ^ W[t-15]>>>3 ^ W[t-15]<<25 ^ W[t-15]<<14) +
					W[t-7] + W[t-16]
				) | 0;
				let t1 = (h + W[t] + this._K[t] + (g ^ e&(f^g)) + (e>>>6  ^ e>>>11  ^ e>>>25  ^ e<<26  ^ e<<21  ^ e<<7)) | 0; 
				let t2 = ((a>>>2 ^ a>>>13 ^ a>>>22 ^ a<<30 ^ a<<19 ^ a<<10) + ((a&b) ^ (c&(a^b)))  ) | 0 
				h = g ;
				g = f ;
				f = e ;
				e = d + t1 | 0;
				d = c ;
				c = b ;
				b = a ;
				a = t1 + t2  | 0;
			}
			H7 = H7 + h | 0;
			H6 = H6 + g | 0;
			H5 = H5 + f | 0;
			H4 = H4 + e | 0;
			H3 = H3 + d | 0;
			H2 = H2 + c | 0;
			H1 = H1 + b | 0;
			H0 = H0 + a| 0;
		}
		let result = new Uint32Array([H0, H1, H2, H3, H4, H5, H6, H7]);
		return this._32to8(result);
	}
	
	// generate the K table as a Uint32Array
	_generateShaK(){
		let K = new Uint32Array(64);
		let primes = this._firstPrimes(K.length);
		for(let i in primes){
			K[i] = (Math.pow(primes[i], 1/3) % 1) * 0x100000000;
		}
		return K;
	}
	
	// generate the first n prime numbers
	_firstPrimes(n){
		let res = [2];
		for(let i=res[0]+1; res.length < n; i++){
			for(let p of res){
				if((i%p) === 0) break;
				if(p*p>i){
					res.push(i);
					break;
				}
			}
		}
		return res;
	}
	
	// perform sha256 padding
	// input : a Uint8Array
	// output : a Uint8Array
	_sha256Padding(input){
		let res = this._8to32( input, 16 + Math.floor((input.byteLength+8)/64)*16 );
		res[ Math.floor( input.byteLength/4) ] += [ 0x80000000, 0x800000, 0x8000, 0x80][input.byteLength%4];
		res[res.length-1] += input.byteLength*8 & 0xffffffff;
		res[res.length-2] += input.byteLength >> 29;
		return res;
	}
	
	// transform a Uint8Array into a Uint32Array
	_8to32(arr, size){
		let len = Math.floor( size || (arr.length+3)/4 );
		let res = new Uint32Array(len);
		let mult = [0x1000000, 0x10000, 0x100, 1];
		for(let i in arr){
			res[ Math.floor(i/4) ] += mult[i%4] * arr[i];
		}
		return res;
	}
	
	// transform a Uint32Array into a Uint8Array
	_32to8(arr){
		let res = new Uint8Array(arr.buffer);
		let t;
		for(let i=0; i<res.length; i+=4){
			t = res[i]; res[i] = res[i+3]; res[i+3] = t;
			t = res[i+1]; res[i+1] = res[i+2]; res[i+2] = t;
		}
		return res;
	}
	
	
	
	////////////////////////////////////////////////////////////////////
	////////  METHODS TO MANIPULATE BIG NUMBERS ////////////////////////
	////////////////////////////////////////////////////////////////////
	
	// create a big number generator, from Tom Wu library : http://www-cs-students.stanford.edu/%7Etjw/jsbn/nRaw for not minified libraries

    /*
	 * Copyright (c) 2003-2005  Tom Wu
	 * All Rights Reserved.
	 *
	 * Permission is hereby granted, free of charge, to any person obtaining
	 * a copy of this software and associated documentation files (the
	 * "Software"), to deal in the Software without restriction, including
	 * without limitation the rights to use, copy, modify, merge, publish,
	 * distribute, sublicense, and/or sell copies of the Software, and to
	 * permit persons to whom the Software is furnished to do so, subject to
	 * the following conditions:
	 *
	 * The above copyright notice and this permission notice shall be
	 * included in all copies or substantial portions of the Software.
	 *
	 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
	 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
	 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
	 *
	 * IN NO EVENT SHALL TOM WU BE LIABLE FOR ANY SPECIAL, INCIDENTAL,
	 * INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND, OR ANY DAMAGES WHATSOEVER
	 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER OR NOT ADVISED OF
	 * THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF LIABILITY, ARISING OUT
	 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
	 *
	 * In addition, the following condition applies:
	 *
	 * All redistributions must retain an intact copy of this copyright notice
	 * and disclaimer.
	 */

	_rsaLibGen(){
		var dbits;var canary=0xdeadbeefcafe;var j_lm=((canary&0xffffff)==0xefcafe);function BigInteger(a,b,c){if(a!=null)
		if("number"==typeof a)this.fromNumber(a,b,c);else if(b==null&&"string"!=typeof a)this.fromString(a,256);else this.fromString(a,b);}
		function nbi(){return new BigInteger(null);}
		function am1(i,x,w,j,c,n){while(--n>=0){var v=x*this[i++]+w[j]+c;c=Math.floor(v/0x4000000);w[j++]=v&0x3ffffff;}
		return c;}
		function am2(i,x,w,j,c,n){var xl=x&0x7fff,xh=x>>15;while(--n>=0){var l=this[i]&0x7fff;var h=this[i++]>>15;var m=xh*l+h*xl;l=xl*l+((m&0x7fff)<<15)+w[j]+(c&0x3fffffff);c=(l>>>30)+(m>>>15)+xh*h+(c>>>30);w[j++]=l&0x3fffffff;}
		return c;}
		function am3(i,x,w,j,c,n){var xl=x&0x3fff,xh=x>>14;while(--n>=0){var l=this[i]&0x3fff;var h=this[i++]>>14;var m=xh*l+h*xl;l=xl*l+((m&0x3fff)<<14)+w[j]+c;c=(l>>28)+(m>>14)+xh*h;w[j++]=l&0xfffffff;}
		return c;}
		if(j_lm&&(typeof navigator!=="undefined")&&(navigator.appName=="Microsoft Internet Explorer")){BigInteger.prototype.am=am2;dbits=30;}
		else if(j_lm&&(typeof navigator!=="undefined")&&(navigator.appName!="Netscape")){BigInteger.prototype.am=am1;dbits=26;}
		else{BigInteger.prototype.am=am3;dbits=28;}
		BigInteger.prototype.DB=dbits;BigInteger.prototype.DM=((1<<dbits)-1);BigInteger.prototype.DV=(1<<dbits);var BI_FP=52;BigInteger.prototype.FV=Math.pow(2,BI_FP);BigInteger.prototype.F1=BI_FP-dbits;BigInteger.prototype.F2=2*dbits-BI_FP;var BI_RM="0123456789abcdefghijklmnopqrstuvwxyz";var BI_RC=new Array();var rr,vv;rr="0".charCodeAt(0);for(vv=0;vv<=9;++vv)BI_RC[rr++]=vv;rr="a".charCodeAt(0);for(vv=10;vv<36;++vv)BI_RC[rr++]=vv;rr="A".charCodeAt(0);for(vv=10;vv<36;++vv)BI_RC[rr++]=vv;function int2char(n){return BI_RM.charAt(n);}
		function intAt(s,i){var c=BI_RC[s.charCodeAt(i)];return(c==null)?-1:c;}
		function bnpCopyTo(r){for(var i=this.t-1;i>=0;--i)r[i]=this[i];r.t=this.t;r.s=this.s;}
		function bnpFromInt(x){this.t=1;this.s=(x<0)?-1:0;if(x>0)this[0]=x;else if(x<-1)this[0]=x+this.DV;else this.t=0;}
		function nbv(i){var r=nbi();r.fromInt(i);return r;}
		function bnpFromString(s,b){var k;if(b==16)k=4;else if(b==8)k=3;else if(b==256)k=8;else if(b==2)k=1;else if(b==32)k=5;else if(b==4)k=2;else{this.fromRadix(s,b);return;}
		this.t=0;this.s=0;var i=s.length,mi=false,sh=0;while(--i>=0){var x=(k==8)?s[i]&0xff:intAt(s,i);if(x<0){if(s.charAt(i)=="-")mi=true;continue;}
		mi=false;if(sh==0)
		this[this.t++]=x;else if(sh+k>this.DB){this[this.t-1]|=(x&((1<<(this.DB-sh))-1))<<sh;this[this.t++]=(x>>(this.DB-sh));}
		else
		this[this.t-1]|=x<<sh;sh+=k;if(sh>=this.DB)sh-=this.DB;}
		if(k==8&&(s[0]&0x80)!=0){this.s=-1;if(sh>0)this[this.t-1]|=((1<<(this.DB-sh))-1)<<sh;}
		this.clamp();if(mi)BigInteger.ZERO.subTo(this,this);}
		function bnpFromUint8Array(s){var k=8;this.t=0;this.s=0;var i=s.length,mi=false,sh=0;while(--i>=0){var x=s[i];if(sh==0)
		this[this.t++]=x;else if(sh+k>this.DB){this[this.t-1]|=(x&((1<<(this.DB-sh))-1))<<sh;this[this.t++]=(x>>(this.DB-sh));}
		else
		this[this.t-1]|=x<<sh;sh+=k;if(sh>=this.DB)sh-=this.DB;}
		this.clamp();if(mi)BigInteger.ZERO.subTo(this,this);}
		function bnpClamp(){var c=this.s&this.DM;while(this.t>0&&this[this.t-1]==c)--this.t;}
		function bnToString(b){if(this.s<0)return"-"+this.negate().toString(b);var k;if(b==16)k=4;else if(b==8)k=3;else if(b==2)k=1;else if(b==32)k=5;else if(b==4)k=2;else return this.toRadix(b);var km=(1<<k)-1,d,m=false,r="",i=this.t;var p=this.DB-(i*this.DB)%k;if(i-->0){if(p<this.DB&&(d=this[i]>>p)>0){m=true;r=int2char(d);}
		while(i>=0){if(p<k){d=(this[i]&((1<<p)-1))<<(k-p);d|=this[--i]>>(p+=this.DB-k);}
		else{d=(this[i]>>(p-=k))&km;if(p<=0){p+=this.DB;--i;}}
		if(d>0)m=true;if(m)r+=int2char(d);}}
		return m?r:"0";}
		function bnAbs(){return(this.s<0)?this.negate():this;}
		function bnToUint8Array(l=0){if(this.s<0)return"-"+this.negate().toString(b);let resLen=l|Math.floor((this.t*this.DB+7)/8);let r=new Uint8Array(resLen);let firstByte=resLen;for(let i=0,d=0,p=0,resI=resLen-1;i<this.t;){d=d+(this[i++]*Math.pow(2,p));p+=this.DB;while(p>0){if(p<8&&i<this.t)break;r[resI]=d&0xff;if(r[resI])firstByte=resI;resI--;d=Math.floor(d/256);p-=8;}}
		if(!l&&firstByte)r=r.slice(firstByte);return r;}
		function bnCompareTo(a){var r=this.s-a.s;if(r!=0)return r;var i=this.t;r=i-a.t;if(r!=0)return(this.s<0)?-r:r;while(--i>=0)if((r=this[i]-a[i])!=0)return r;return 0;}
		function nbits(x){var r=1,t;if((t=x>>>16)!=0){x=t;r+=16;}
		if((t=x>>8)!=0){x=t;r+=8;}
		if((t=x>>4)!=0){x=t;r+=4;}
		if((t=x>>2)!=0){x=t;r+=2;}
		if((t=x>>1)!=0){x=t;r+=1;}
		return r;}
		function bnBitLength(){if(this.t<=0)return 0;return this.DB*(this.t-1)+nbits(this[this.t-1]^(this.s&this.DM));}
		function bnpDLShiftTo(n,r){var i;for(i=this.t-1;i>=0;--i)r[i+n]=this[i];for(i=n-1;i>=0;--i)r[i]=0;r.t=this.t+n;r.s=this.s;}
		function bnpDRShiftTo(n,r){for(var i=n;i<this.t;++i)r[i-n]=this[i];r.t=Math.max(this.t-n,0);r.s=this.s;}
		function bnpLShiftTo(n,r){var bs=n%this.DB;var cbs=this.DB-bs;var bm=(1<<cbs)-1;var ds=Math.floor(n/this.DB),c=(this.s<<bs)&this.DM,i;for(i=this.t-1;i>=0;--i){r[i+ds+1]=(this[i]>>cbs)|c;c=(this[i]&bm)<<bs;}
		for(i=ds-1;i>=0;--i)r[i]=0;r[ds]=c;r.t=this.t+ds+1;r.s=this.s;r.clamp();}
		function bnpRShiftTo(n,r){r.s=this.s;var ds=Math.floor(n/this.DB);if(ds>=this.t){r.t=0;return;}
		var bs=n%this.DB;var cbs=this.DB-bs;var bm=(1<<bs)-1;r[0]=this[ds]>>bs;for(var i=ds+1;i<this.t;++i){r[i-ds-1]|=(this[i]&bm)<<cbs;r[i-ds]=this[i]>>bs;}
		if(bs>0)r[this.t-ds-1]|=(this.s&bm)<<cbs;r.t=this.t-ds;r.clamp();}
		function bnpSubTo(a,r){var i=0,c=0,m=Math.min(a.t,this.t);while(i<m){c+=this[i]-a[i];r[i++]=c&this.DM;c>>=this.DB;}
		if(a.t<this.t){c-=a.s;while(i<this.t){c+=this[i];r[i++]=c&this.DM;c>>=this.DB;}
		c+=this.s;}
		else{c+=this.s;while(i<a.t){c-=a[i];r[i++]=c&this.DM;c>>=this.DB;}
		c-=a.s;}
		r.s=(c<0)?-1:0;if(c<-1)r[i++]=this.DV+c;else if(c>0)r[i++]=c;r.t=i;r.clamp();}
		function bnpMultiplyTo(a,r){var x=this.abs(),y=a.abs();var i=x.t;r.t=i+y.t;while(--i>=0)r[i]=0;for(i=0;i<y.t;++i)r[i+x.t]=x.am(0,y[i],r,i,0,x.t);r.s=0;r.clamp();if(this.s!=a.s)BigInteger.ZERO.subTo(r,r);}
		function bnpSquareTo(r){var x=this.abs();var i=r.t=2*x.t;while(--i>=0)r[i]=0;for(i=0;i<x.t-1;++i){var c=x.am(i,x[i],r,2*i,0,1);if((r[i+x.t]+=x.am(i+1,2*x[i],r,2*i+1,c,x.t-i-1))>=x.DV){r[i+x.t]-=x.DV;r[i+x.t+1]=1;}}
		if(r.t>0)r[r.t-1]+=x.am(i,x[i],r,2*i,0,1);r.s=0;r.clamp();}
		function bnpDivRemTo(m,q,r){var pm=m.abs();if(pm.t<=0)return;var pt=this.abs();if(pt.t<pm.t){if(q!=null)q.fromInt(0);if(r!=null)this.copyTo(r);return;}
		if(r==null)r=nbi();var y=nbi(),ts=this.s,ms=m.s;var nsh=this.DB-nbits(pm[pm.t-1]);if(nsh>0){pm.lShiftTo(nsh,y);pt.lShiftTo(nsh,r);}
		else{pm.copyTo(y);pt.copyTo(r);}
		var ys=y.t;var y0=y[ys-1];if(y0==0)return;var yt=y0*(1<<this.F1)+((ys>1)?y[ys-2]>>this.F2:0);var d1=this.FV/yt,d2=(1<<this.F1)/yt,e=1<<this.F2;var i=r.t,j=i-ys,t=(q==null)?nbi():q;y.dlShiftTo(j,t);if(r.compareTo(t)>=0){r[r.t++]=1;r.subTo(t,r);}
		BigInteger.ONE.dlShiftTo(ys,t);t.subTo(y,y);while(y.t<ys)y[y.t++]=0;while(--j>=0){var qd=(r[--i]==y0)?this.DM:Math.floor(r[i]*d1+(r[i-1]+e)*d2);if((r[i]+=y.am(0,qd,r,j,0,ys))<qd){y.dlShiftTo(j,t);r.subTo(t,r);while(r[i]<--qd)r.subTo(t,r);}}
		if(q!=null){r.drShiftTo(ys,q);if(ts!=ms)BigInteger.ZERO.subTo(q,q);}
		r.t=ys;r.clamp();if(nsh>0)r.rShiftTo(nsh,r);if(ts<0)BigInteger.ZERO.subTo(r,r);}
		function bnMod(a){var r=nbi();this.abs().divRemTo(a,null,r);if(this.s<0&&r.compareTo(BigInteger.ZERO)>0)a.subTo(r,r);return r;}
		function Classic(m){this.m=m;}
		function cConvert(x){if(x.s<0||x.compareTo(this.m)>=0)return x.mod(this.m);else return x;}
		function cRevert(x){return x;}
		function cReduce(x){x.divRemTo(this.m,null,x);}
		function cMulTo(x,y,r){x.multiplyTo(y,r);this.reduce(r);}
		function cSqrTo(x,r){x.squareTo(r);this.reduce(r);}
		Classic.prototype.convert=cConvert;Classic.prototype.revert=cRevert;Classic.prototype.reduce=cReduce;Classic.prototype.mulTo=cMulTo;Classic.prototype.sqrTo=cSqrTo;function bnpInvDigit(){if(this.t<1)return 0;var x=this[0];if((x&1)==0)return 0;var y=x&3;y=(y*(2-(x&0xf)*y))&0xf;y=(y*(2-(x&0xff)*y))&0xff;y=(y*(2-(((x&0xffff)*y)&0xffff)))&0xffff;y=(y*(2-x*y%this.DV))%this.DV;return(y>0)?this.DV-y:-y;}
		function Montgomery(m){this.m=m;this.mp=m.invDigit();this.mpl=this.mp&0x7fff;this.mph=this.mp>>15;this.um=(1<<(m.DB-15))-1;this.mt2=2*m.t;}
		function montConvert(x){var r=nbi();x.abs().dlShiftTo(this.m.t,r);r.divRemTo(this.m,null,r);if(x.s<0&&r.compareTo(BigInteger.ZERO)>0)this.m.subTo(r,r);return r;}
		function montRevert(x){var r=nbi();x.copyTo(r);this.reduce(r);return r;}
		function montReduce(x){while(x.t<=this.mt2)
		x[x.t++]=0;for(var i=0;i<this.m.t;++i){var j=x[i]&0x7fff;var u0=(j*this.mpl+(((j*this.mph+(x[i]>>15)*this.mpl)&this.um)<<15))&x.DM;j=i+this.m.t;x[j]+=this.m.am(0,u0,x,i,0,this.m.t);while(x[j]>=x.DV){x[j]-=x.DV;x[++j]++;}}
		x.clamp();x.drShiftTo(this.m.t,x);if(x.compareTo(this.m)>=0)x.subTo(this.m,x);}
		function montSqrTo(x,r){x.squareTo(r);this.reduce(r);}
		function montMulTo(x,y,r){x.multiplyTo(y,r);this.reduce(r);}
		Montgomery.prototype.convert=montConvert;Montgomery.prototype.revert=montRevert;Montgomery.prototype.reduce=montReduce;Montgomery.prototype.mulTo=montMulTo;Montgomery.prototype.sqrTo=montSqrTo;function bnpIsEven(){return((this.t>0)?(this[0]&1):this.s)==0;}
		function bnpExp(e,z){if(e>0xffffffff||e<1)return BigInteger.ONE;var r=nbi(),r2=nbi(),g=z.convert(this),i=nbits(e)-1;g.copyTo(r);while(--i>=0){z.sqrTo(r,r2);if((e&(1<<i))>0)z.mulTo(r2,g,r);else{var t=r;r=r2;r2=t;}}
		return z.revert(r);}
		function bnModPowInt(e,m){var z;if(e<256||m.isEven())z=new Classic(m);else z=new Montgomery(m);return this.exp(e,z);}
		BigInteger.prototype.copyTo=bnpCopyTo;BigInteger.prototype.fromInt=bnpFromInt;BigInteger.prototype.fromString=bnpFromString;BigInteger.prototype.fromUint8Array=bnpFromUint8Array;BigInteger.prototype.clamp=bnpClamp;BigInteger.prototype.dlShiftTo=bnpDLShiftTo;BigInteger.prototype.drShiftTo=bnpDRShiftTo;BigInteger.prototype.lShiftTo=bnpLShiftTo;BigInteger.prototype.rShiftTo=bnpRShiftTo;BigInteger.prototype.subTo=bnpSubTo;BigInteger.prototype.multiplyTo=bnpMultiplyTo;BigInteger.prototype.squareTo=bnpSquareTo;BigInteger.prototype.divRemTo=bnpDivRemTo;BigInteger.prototype.invDigit=bnpInvDigit;BigInteger.prototype.isEven=bnpIsEven;BigInteger.prototype.exp=bnpExp;BigInteger.prototype.toString=bnToString;BigInteger.prototype.toUint8Array=bnToUint8Array;BigInteger.prototype.abs=bnAbs;BigInteger.prototype.compareTo=bnCompareTo;BigInteger.prototype.bitLength=bnBitLength;BigInteger.prototype.mod=bnMod;BigInteger.prototype.modPowInt=bnModPowInt;BigInteger.ZERO=nbv(0);BigInteger.ONE=nbv(1);function bnClone(){var r=nbi();this.copyTo(r);return r;}
		function bnSigNum(){if(this.s<0)return-1;else if(this.t<=0||(this.t==1&&this[0]<=0))return 0;else return 1;}
		function bnpFromNumber(a,b,c){if("number"==typeof b){if(a<2)this.fromInt(1);else{this.fromNumber(a,c);if(!this.testBit(a-1))
		this.bitwiseTo(BigInteger.ONE.shiftLeft(a-1),op_or,this);if(this.isEven())this.dAddOffset(1,0);while(!this.isProbablePrime(b)){this.dAddOffset(2,0);if(this.bitLength()>a)this.subTo(BigInteger.ONE.shiftLeft(a-1),this);}}}
		else{var x=new Array(),t=a&7;x.length=(a>>3)+1;b.nextBytes(x);if(t>0)x[0]&=((1<<t)-1);else x[0]=0;this.fromString(x,256);}}
		function bnToByteArray(){var i=this.t,r=new Array();r[0]=this.s;var p=this.DB-(i*this.DB)%8,d,k=0;if(i-->0){if(p<this.DB&&(d=this[i]>>p)!=(this.s&this.DM)>>p)
		r[k++]=d|(this.s<<(this.DB-p));while(i>=0){if(p<8){d=(this[i]&((1<<p)-1))<<(8-p);d|=this[--i]>>(p+=this.DB-8);}
		else{d=(this[i]>>(p-=8))&0xff;if(p<=0){p+=this.DB;--i;}}
		if((d&0x80)!=0)d|=-256;if(k==0&&(this.s&0x80)!=(d&0x80))++k;if(k>0||d!=this.s)r[k++]=d;}}
		return r;}
		function bnpBitwiseTo(a,op,r){var i,f,m=Math.min(a.t,this.t);for(i=0;i<m;++i)r[i]=op(this[i],a[i]);if(a.t<this.t){f=a.s&this.DM;for(i=m;i<this.t;++i)r[i]=op(this[i],f);r.t=this.t;}
		else{f=this.s&this.DM;for(i=m;i<a.t;++i)r[i]=op(f,a[i]);r.t=a.t;}
		r.s=op(this.s,a.s);r.clamp();}
		function op_and(x,y){return x&y;}
		function op_or(x,y){return x|y;}
		function op_xor(x,y){return x^y;}
		function bnNot(){var r=nbi();for(var i=0;i<this.t;++i)r[i]=this.DM&~this[i];r.t=this.t;r.s=~this.s;return r;}
		function bnShiftLeft(n){var r=nbi();if(n<0)this.rShiftTo(-n,r);else this.lShiftTo(n,r);return r;}
		function bnShiftRight(n){var r=nbi();if(n<0)this.lShiftTo(-n,r);else this.rShiftTo(n,r);return r;}
		function lbit(x){if(x==0)return-1;var r=0;if((x&0xffff)==0){x>>=16;r+=16;}
		if((x&0xff)==0){x>>=8;r+=8;}
		if((x&0xf)==0){x>>=4;r+=4;}
		if((x&3)==0){x>>=2;r+=2;}
		if((x&1)==0)++r;return r;}
		function bnGetLowestSetBit(){for(var i=0;i<this.t;++i)
		if(this[i]!=0)return i*this.DB+lbit(this[i]);if(this.s<0)return this.t*this.DB;return-1;}
		function cbit(x){var r=0;while(x!=0){x&=x-1;++r;}
		return r;}
		function bnTestBit(n){var j=Math.floor(n/this.DB);if(j>=this.t)return(this.s!=0);return((this[j]&(1<<(n%this.DB)))!=0);}
		function bnpAddTo(a,r){var i=0,c=0,m=Math.min(a.t,this.t);while(i<m){c+=this[i]+a[i];r[i++]=c&this.DM;c>>=this.DB;}
		if(a.t<this.t){c+=a.s;while(i<this.t){c+=this[i];r[i++]=c&this.DM;c>>=this.DB;}
		c+=this.s;}
		else{c+=this.s;while(i<a.t){c+=a[i];r[i++]=c&this.DM;c>>=this.DB;}
		c+=a.s;}
		r.s=(c<0)?-1:0;if(c>0)r[i++]=c;else if(c<-1)r[i++]=this.DV+c;r.t=i;r.clamp();}
		function bnSubtract(a){var r=nbi();this.subTo(a,r);return r;}
		function bnMultiply(a){var r=nbi();this.multiplyTo(a,r);return r;}
		function bnpDAddOffset(n,w){if(n==0)return;while(this.t<=w)this[this.t++]=0;this[w]+=n;while(this[w]>=this.DV){this[w]-=this.DV;if(++w>=this.t)this[this.t++]=0;++this[w];}}
		function bnModPow(e,m){var i=e.bitLength(),k,r=nbv(1),z;if(i<=0)return r;else if(i<18)k=1;else if(i<48)k=3;else if(i<144)k=4;else if(i<768)k=5;else k=6;if(i<8)
		z=new Classic(m);else if(m.isEven())
		z=new Barrett(m);else
		z=new Montgomery(m);var g=new Array(),n=3,k1=k-1,km=(1<<k)-1;g[1]=z.convert(this);if(k>1){var g2=nbi();z.sqrTo(g[1],g2);while(n<=km){g[n]=nbi();z.mulTo(g2,g[n-2],g[n]);n+=2;}}
		var j=e.t-1,w,is1=true,r2=nbi(),t;i=nbits(e[j])-1;while(j>=0){if(i>=k1)w=(e[j]>>(i-k1))&km;else{w=(e[j]&((1<<(i+1))-1))<<(k1-i);if(j>0)w|=e[j-1]>>(this.DB+i-k1);}
		n=k;while((w&1)==0){w>>=1;--n;}
		if((i-=n)<0){i+=this.DB;--j;}
		if(is1){g[w].copyTo(r);is1=false;}
		else{while(n>1){z.sqrTo(r,r2);z.sqrTo(r2,r);n-=2;}
		if(n>0)z.sqrTo(r,r2);else{t=r;r=r2;r2=t;}
		z.mulTo(r2,g[w],r);}
		while(j>=0&&(e[j]&(1<<i))==0){z.sqrTo(r,r2);t=r;r=r2;r2=t;if(--i<0){i=this.DB-1;--j;}}}
		return z.revert(r);}
		function bnGCD(a){var x=(this.s<0)?this.negate():this.clone();var y=(a.s<0)?a.negate():a.clone();if(x.compareTo(y)<0){var t=x;x=y;y=t;}
		var i=x.getLowestSetBit(),g=y.getLowestSetBit();if(g<0)return x;if(i<g)g=i;if(g>0){x.rShiftTo(g,x);y.rShiftTo(g,y);}
		while(x.signum()>0){if((i=x.getLowestSetBit())>0)x.rShiftTo(i,x);if((i=y.getLowestSetBit())>0)y.rShiftTo(i,y);if(x.compareTo(y)>=0){x.subTo(y,x);x.rShiftTo(1,x);}
		else{y.subTo(x,y);y.rShiftTo(1,y);}}
		if(g>0)y.lShiftTo(g,y);return y;}
		function bnpModInt(n){if(n<=0)return 0;var d=this.DV%n,r=(this.s<0)?n-1:0;if(this.t>0)
		if(d==0)r=this[0]%n;else for(var i=this.t-1;i>=0;--i)r=(d*r+this[i])%n;return r;}
		function bnModInverse(m){var ac=m.isEven();if((this.isEven()&&ac)||m.signum()==0)return BigInteger.ZERO;var u=m.clone(),v=this.clone();var a=nbv(1),b=nbv(0),c=nbv(0),d=nbv(1);while(u.signum()!=0){while(u.isEven()){u.rShiftTo(1,u);if(ac){if(!a.isEven()||!b.isEven()){a.addTo(this,a);b.subTo(m,b);}
		a.rShiftTo(1,a);}
		else if(!b.isEven())b.subTo(m,b);b.rShiftTo(1,b);}
		while(v.isEven()){v.rShiftTo(1,v);if(ac){if(!c.isEven()||!d.isEven()){c.addTo(this,c);d.subTo(m,d);}
		c.rShiftTo(1,c);}
		else if(!d.isEven())d.subTo(m,d);d.rShiftTo(1,d);}
		if(u.compareTo(v)>=0){u.subTo(v,u);if(ac)a.subTo(c,a);b.subTo(d,b);}
		else{v.subTo(u,v);if(ac)c.subTo(a,c);d.subTo(b,d);}}
		if(v.compareTo(BigInteger.ONE)!=0)return BigInteger.ZERO;if(d.compareTo(m)>=0)return d.subtract(m);if(d.signum()<0)d.addTo(m,d);else return d;if(d.signum()<0)return d.add(m);else return d;}
		var lowprimes=[2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97,101,103,107,109,113,127,131,137,139,149,151,157,163,167,173,179,181,191,193,197,199,211,223,227,229,233,239,241,251,257,263,269,271,277,281,283,293,307,311,313,317,331,337,347,349,353,359,367,373,379,383,389,397,401,409,419,421,431,433,439,443,449,457,461,463,467,479,487,491,499,503,509,521,523,541,547,557,563,569,571,577,587,593,599,601,607,613,617,619,631,641,643,647,653,659,661,673,677,683,691,701,709,719,727,733,739,743,751,757,761,769,773,787,797,809,811,821,823,827,829,839,853,857,859,863,877,881,883,887,907,911,919,929,937,941,947,953,967,971,977,983,991,997];var lplim=(1<<26)/lowprimes[lowprimes.length-1];function bnIsProbablePrime(t){var i,x=this.abs();if(x.t==1&&x[0]<=lowprimes[lowprimes.length-1]){for(i=0;i<lowprimes.length;++i)
		if(x[0]==lowprimes[i])return true;return false;}
		if(x.isEven())return false;i=1;while(i<lowprimes.length){var m=lowprimes[i],j=i+1;while(j<lowprimes.length&&m<lplim)m*=lowprimes[j++];m=x.modInt(m);while(i<j)if(m%lowprimes[i++]==0)return false;}
		return x.millerRabin(t);}
		function bnpMillerRabin(t){var n1=this.subtract(BigInteger.ONE);var k=n1.getLowestSetBit();if(k<=0)return false;var r=n1.shiftRight(k);t=(t+1)>>1;if(t>lowprimes.length)t=lowprimes.length;var a=nbi();for(var i=0;i<t;++i){a.fromInt(lowprimes[Math.floor(Math.random()*lowprimes.length)]);var y=a.modPow(r,this);if(y.compareTo(BigInteger.ONE)!=0&&y.compareTo(n1)!=0){var j=1;while(j++<k&&y.compareTo(n1)!=0){y=y.modPowInt(2,this);if(y.compareTo(BigInteger.ONE)==0)return false;}
		if(y.compareTo(n1)!=0)return false;}}
		return true;}
		var bip=BigInteger.prototype;bip.fromNumber=bnpFromNumber;bip.bitwiseTo=bnpBitwiseTo;bip.addTo=bnpAddTo;bip.dAddOffset=bnpDAddOffset;bip.modInt=bnpModInt;bip.millerRabin=bnpMillerRabin;bip.clone=bnClone;bip.signum=bnSigNum;bip.toByteArray=bnToByteArray;bip.shiftLeft=bnShiftLeft;bip.shiftRight=bnShiftRight;bip.getLowestSetBit=bnGetLowestSetBit;bip.testBit=bnTestBit;bip.subtract=bnSubtract;bip.multiply=bnMultiply;bip.modPow=bnModPow;bip.modInverse=bnModInverse;bip.gcd=bnGCD;bip.isProbablePrime=bnIsProbablePrime;function rng_get_bytes(ba){for(var i=0;i<ba.length;++i)ba[i]=Math.random()*256>>0;}
		function SecureRandom(){}
		SecureRandom.prototype.nextBytes=rng_get_bytes;function parseBigInt(str,r){return new BigInteger(str,r);}
		function linebrk(s,n){var ret="";var i=0;while(i+n<s.length){ret+=s.substring(i,i+n)+"\n";i+=n;}
		return ret+s.substring(i,s.length);}
		function byte2Hex(b){if(b<0x10)
		return"0"+b.toString(16);else
		return b.toString(16);}
		function pkcs1pad2(s,n){if(n<s.length+11){throw("Message too long for RSA");return null;}
		var ba=new Array();var i=s.length-1;while(i>=0&&n>0){var c=s.charCodeAt(i--);if(c<128){ba[--n]=c;}
		else if((c>127)&&(c<2048)){ba[--n]=(c&63)|128;ba[--n]=(c>>6)|192;}
		else{ba[--n]=(c&63)|128;ba[--n]=((c>>6)&63)|128;ba[--n]=(c>>12)|224;}}
		ba[--n]=0;var rng=new SecureRandom();var x=new Array();while(n>2){x[0]=0;while(x[0]==0)rng.nextBytes(x);ba[--n]=x[0];}
		ba[--n]=2;ba[--n]=0;return new BigInteger(ba);}
		function RSAKey(){this.n=null;this.e=0;this.d=null;this.p=null;this.q=null;this.dmp1=null;this.dmq1=null;this.coeff=null;}
		function RSASetPublic(N,E){if(N!=null&&E!=null&&N.length>0&&E.length>0){this.n=parseBigInt(N,16);this.e=parseInt(E,16);}
		else
		throw("Invalid RSA public key");}
		function RSADoPublic(x){return x.modPowInt(this.e,this.n);}
		function RSAEncrypt(text){var m=pkcs1pad2(text,(this.n.bitLength()+7)>>3);if(m==null)return null;var c=this.doPublic(m);if(c==null)return null;var h=c.toString(16);if((h.length&1)==0)return h;else return"0"+h;}
		RSAKey.prototype.doPublic=RSADoPublic;RSAKey.prototype.setPublic=RSASetPublic;RSAKey.prototype.encrypt=RSAEncrypt;function pkcs1unpad2(d,n){var b=d.toByteArray();var i=0;while(i<b.length&&b[i]==0)++i;if(b.length-i!=n-1||b[i]!=2)
		return null;++i;while(b[i]!=0)
		if(++i>=b.length)return null;var ret="";while(++i<b.length){var c=b[i]&255;if(c<128){ret+=String.fromCharCode(c);}
		else if((c>191)&&(c<224)){ret+=String.fromCharCode(((c&31)<<6)|(b[i+1]&63));++i;}
		else{ret+=String.fromCharCode(((c&15)<<12)|((b[i+1]&63)<<6)|(b[i+2]&63));i+=2;}}
		return ret;}
		function RSASetPrivate(N,E,D){if(N!=null&&E!=null&&N.length>0&&E.length>0){this.n=parseBigInt(N,16);this.e=parseInt(E,16);this.d=parseBigInt(D,16);}
		else
		throw("Invalid RSA private key");}
		function RSAGenerate(B,E){var rng=new SecureRandom();var qs=B>>1;this.e=parseInt(E,16);var ee=new BigInteger(E,16);for(;;){for(;;){this.p=new BigInteger(B-qs,1,rng);if(this.p.subtract(BigInteger.ONE).gcd(ee).compareTo(BigInteger.ONE)==0&&this.p.isProbablePrime(10))break;}
		for(;;){this.q=new BigInteger(qs,1,rng);if(this.q.subtract(BigInteger.ONE).gcd(ee).compareTo(BigInteger.ONE)==0&&this.q.isProbablePrime(10))break;}
		if(this.p.compareTo(this.q)<=0){var t=this.p;this.p=this.q;this.q=t;}
		var p1=this.p.subtract(BigInteger.ONE);var q1=this.q.subtract(BigInteger.ONE);var phi=p1.multiply(q1);if(phi.gcd(ee).compareTo(BigInteger.ONE)==0){this.n=this.p.multiply(this.q);this.d=ee.modInverse(phi);this.dmp1=this.d.mod(p1);this.dmq1=this.d.mod(q1);this.coeff=this.q.modInverse(this.p);break;}}}
		function RSADoPrivate(x){if(this.p==null||this.q==null)
		return x.modPow(this.d,this.n);var xp=x.mod(this.p).modPow(this.dmp1,this.p);var xq=x.mod(this.q).modPow(this.dmq1,this.q);while(xp.compareTo(xq)<0)
		xp=xp.add(this.p);return xp.subtract(xq).multiply(this.coeff).mod(this.p).multiply(this.q).add(xq);}
		function RSADecrypt(ctext){var c=parseBigInt(ctext,16);var m=this.doPrivate(c);if(m==null)return null;return pkcs1unpad2(m,(this.n.bitLength()+7)>>3);}
		RSAKey.prototype.doPrivate=RSADoPrivate;RSAKey.prototype.setPrivate=RSASetPrivate;RSAKey.prototype.generate=RSAGenerate;RSAKey.prototype.decrypt=RSADecrypt;var exportLib={RSAKey:RSAKey,BigInteger:BigInteger,Classic:Classic,Montgomery:Montgomery,SecureRandom:SecureRandom,}
		return exportLib;
	}
	
	// calculate a^b%n
	// n MUST be ODD
	power(a,b,n){
		let bnA = new this._lib.BigInteger();
		bnA.fromUint8Array(a);
		let bnB = new this._lib.BigInteger();
		bnB.fromUint8Array(b);
		let bnN = new this._lib.BigInteger();
		bnN.fromUint8Array(n);
		let bnRes = bnA.modPow(bnB, bnN);

		return bnRes.toUint8Array();
	}
	
	// transform a string into a Uint8Array
	_hexstrToUint8Array(str){
		if(str.length%2) str = "0"+str;
		let res = new Uint8Array(str.length/2);
		for(let i=0; i<str.length;i+=2){
			res[i/2] = parseInt(str.slice(i,i+2), 16);
		}
		return res;
	}
}


// CONCATENATED MODULE: ./src/flaptools/lib/Flaptools.js
/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

// flaptools is a library to help creating flock applications (flap)
// in particular, it defines a way to create protocols and serialize/unserialize data

class Flaptools_Flaptools{
	
	constructor(){
		this._initConstants();
	}
	
	////////////////////////////////////////////////////////////////////
	/////////////////////   TOOLBOX   //////////////////////////////////
	////////////////////////////////////////////////////////////////////
	
	// return a new instance of threadops
	threadops(data={}){
		return new Threadops(data);
	}

    ////////////////////////////////////////////////////////////////////
    /////////////////////   KEY GENERATION    //////////////////////////
    ////////////////////////////////////////////////////////////////////

    // random bytes generation
    randomBytes(len=8){
        let res = new Uint8Array(len);
        for(let i in res) res[i] = Math.random()*256 >> 0;
        return res;
    }

	// random hexadecimal generation
    randomHex(len=8){
        return this.bytesToHex( this.randomBytes(len) );
    }

    ////////////////////////////////////////////////////////////////////
	//////////////////   DATA FORMAT HELPERS   /////////////////////////
	////////////////////////////////////////////////////////////////////
	
	// byte to hexadecimal conversion
	bytesToHex(b){
		let res = Array.from(b).map(i=>(i<16 ? "0" : "")+i.toString(16)).join("");
		return res;
	}
	
	// hexadecimal to bytes conversion
	hexToBytes(h){
		if(h.length%2) h = "0"+h;
		let res= new Uint8Array(h.length/2).map((v,i)=>parseInt(h.substr(2*i,2),16));
		return res
	}
	
	// bytes to ascii
	bytesToAscii(b){
		return String.fromCharCode(...b);
	}
	
	// ascii to bytes
	asciiToBytes(s){
		return new Uint8Array(s.length).map((v,i)=>s.charCodeAt(i));
	}
	
	// integer to bytes : assume i < 2^54
	intToBytes(i, exactLen=null){
		i = parseInt(i);
		let maxLen = 8; // enough to be used with maxLen == vLenLen == 8
		let b = new Uint8Array(maxLen);
		let k = maxLen;
		while(i && k){
			b[--k] = i%256;
			i = Math.floor(i/256);
		}
		return b.slice(exactLen ? maxLen - exactLen : Math.min(k,maxLen-1) );
	}

	// bytesToInteger
	bytesToInt(b){
		let r = 0; 
		for(let i in b) r = 256*r + b[i];
		return r;
	}
	
	boolToBytes(b){
		return b ? this.BYTE_TRUE : this.BYTE_FALSE;
	}
	
	bytesToBool(b){
		return !!this.bytesToInt(b);
	}


	stringToUtf8Bytes(s){
		let res = [];
		for(let i=0; i<s.length; i++){
			let c = s.charCodeAt(i);
			if(c < 0x80) res.push(c);
			else if(c < 0x800) res.push(0xc0 | (c >> 6), 0x80 | (c & 0x3f) )
			else if( c < 0xd800 || c >= 0xe000) res.push( 0xe0 | (c>>12),  0x80 | ((c>>6) & 0x3f) , 0x80 | (c & 0x3f) )
			else{
				let codepoint = 0x10000 + ((c & 0x03ff)<<10) + (s.charCodeAt(++i) & 0x03ff);
				res.push( 0xf0 + (0x03 & (codepoint>>18)), 0x80 | ((codepoint>>12) & 0x3f) , 0x80 | ((codepoint>>6) & 0x3f) , 0x80 | (codepoint & 0x3f) );
			}
		}
		return new Uint8Array(res);
	}

	utf8BytesToString(bytes){
		let res = [];

		for(let i=0; i<bytes.length; i++){
			let b = bytes[i];
			if(b < 0x80) res.push(b);
			else if(b < 0xe0) res.push( ((b&0x1f)<<6) + (bytes[++i] & 0x3f) );
			else if( b <0xf0) res.push( ((b&0x0f)<<12) + ((bytes[++i] & 0x3f)<<6) + (bytes[++i] & 0x3f) );
			else{
				let codepoint = ((b&0x07)<<18) + ((bytes[++i] & 0x3f)<<12) + ((bytes[++i] & 0x3f)<<6) + (bytes[++i] & 0x3f);
				let diffcode = codepoint - 0x10000;
				res.push( 0xd800 + (diffcode>>10), 0xdc00 + (diffcode & 0x03ff) );
			}
		}
		return res.map(c=>String.fromCharCode(c)).join("");
	}

    ////////////////////////////////////////////////////////////////////
    ////////////                OTHER HELPERS             //////////////
    ////////////////////////////////////////////////////////////////////

	// agregate a list of Uint8Array into one array
	agregateBytes(arrList){
		// calculate the size
		let resultSize = arrList.reduce((acc, arr)=>acc+arr.length, 0);
		let result = new Uint8Array(resultSize);
		let index = 0;
		for(let arr of arrList){
			result.set(arr, index);
			index += arr.length;
		}
		return result;
	}

	////////////////////////////////////////////////////////////////////
	//  Extensible entries : data serialization / unserialization   ////
	////////////////////////////////////////////////////////////////////

	// encode an object given a collection model
	// return an Uint8Array
	encode( obj, dict){
        if(Array.isArray(dict.ee)) dict.ee = this.eeArrayToObject(dict.ee);
		// build result Uint8Array
		// let totalLen = this.calculateLength(obj, dict);
		let resultChunks = [];

		// temporary data for macro execution
		let macroData = {};

		// parse each dictionnary field
		for(let eeName in dict.ee){

			let ee = dict.ee[eeName];
			macroData[ee.n] = { data : {}, index : resultChunks.length };
			let v;
			if(obj.hasOwnProperty(eeName)) v = obj[eeName];
			else {
				if(ee.hasOwnProperty("de")) v = ee.de;
				else if(ee.hasOwnProperty("d")) v = ee.d;
				else if(ee.r) throw "Required EE was not provided : " + eeName;
				else continue;
			}
			// compute and push data about the ee
			this._pushParts(v, ee, resultChunks, macroData[ee.n].data);

			// remember the number of chunks associated to the ee
            macroData[ee.n].len = resultChunks.length - macroData[ee.n].index;
		}

		// execute endMacro if there are any
    	for(let eeName in dict.ee){
    		let ee = dict.ee[eeName];
    		if(!ee.endMacro) continue;
    		let endMacroFunc = typeof ee.endMacro === "string" ? this[ee.endMacro].bind(this) : ee.endMacro;
    		endMacroFunc(resultChunks, ee, macroData[eeName]);
        }
		// build the result by concatenating all sub arrays
		let res = this._bytesConcat(resultChunks);
		return res;
	}

	// build and push the bytes for an ee encoding
	_pushParts(v, ee, parts, macroData={}){
        // compute the byte length of key (0 if official code)
        let withKey = !ee.hasOwnProperty("c");
        let keyLength = withKey ? 0 : Math.min(31, ee.n.length);

        // replace the user readable value by its code
        v = this._valueToCode(v, ee);

        // compute the byte value to push
        let bytes = this._eeToBytes(v,ee, macroData);

        // compute the value length length
        let vlen = bytes.length;
        let vlenlen = this._vLenLen(vlen);

        let vlenCode;
        if(vlenlen === 0) vlenCode = 0;
        else if(vlenlen === 1) vlenCode = 1;
        else if(vlenlen === 2) vlenCode = 2;
        else vlenCode = 3;

        // push the first EE byte
        parts.push( (withKey ? 128 : 0) + (vlenCode<<5) + (withKey ? keyLength : ee.c) );

        // push the key if present
        if(withKey){
            let k = this.asciiToBytes(eeName);
            parts.push(k);
        }

        // push the value length if present
        if(vlenlen){
            let vlb = this.intToBytes(vlen, vlenlen);
            // res.set(vlb, index + vlenlen - vlb.length);
            parts.push(vlb);
        }

        // push the value bytes
        parts.push(bytes);
    }

	// transform an ee value into bytes
	_eeToBytes(v, ee, macroData){
		let bytes;
		// in case a macro is defined for the ee, execute it to compute the byte value
		if(ee.macro){
			let macroFunc = typeof ee.macro === "string" ? this[ee.macro] : ee.macro;
			bytes = macroFunc(v, ee, macroData);
        }
		else {
            switch (ee.t) {
                case "ascii" :
                    bytes = this.asciiToBytes(v);
                    break;
                case "utf8" :
                    bytes = this.stringToUtf8Bytes(v);
                    break;
				case "json" :
					bytes = this.stringToUtf8Bytes( JSON.stringify(v) );
					break;
                case "bin" :
                    bytes = v;
                    break;
                case "hex" :
					bytes = this.hexToBytes(v);
                    break;
                case "bool" :
                    bytes = this.boolToBytes(v);
                    break;
                case "int" :
                    bytes = this.intToBytes(v);
                    break;
                case "dict" :
                    bytes = this.encode(v, ee.tpl);
                    break;
                case "arr" :
                	let parts = [];
                    for (let e of v) {
                        let eb = ee.tpl.ee ? this.encode(e, ee.tpl) : this._eeToBytes(e, ee.tpl);
                        // push the element length
						parts.push( this._intToEI(eb.length) );
						// push the element
                        parts.push(eb);
                    }
                    bytes = this._bytesConcat(parts);
                    break;
                default :
                    throw "Unexpected EE type : " + ee.t;
            }
        }
		return bytes;
	}

	// concatenate a list of Uint8Array and integers
	_bytesConcat(parts){
		// compute the length of the result
		let len = this._bytePartsLength(parts);

		// build the concatenation
		let res = new Uint8Array(len);
        let index = 0;
        for(let p of parts){
            if(typeof p === "object"){
                res.set(p, index);
                index += p.length;
            }
            else res[index++] = p;
        }
        return res;
    }

    // compute the total length of an array of bytes array
	_bytePartsLength(parts){
        let len = 0;
        for(let p of parts) len += typeof p === "object" ? p.length : 1;
        return len;
    }

	// push a EI in a byte array, and return the updated array
	_pushEI(bytes, index, eiVal){
		if(eiVal<128){
			bytes[index++] = eiVal;
			return index;
		}
		let bval = this.intToBytes(eiVal);
		bytes[index++] = 128 + bval.length;
		bytes.set(bval, index);
		return index + bval.length;
	}

	// transform an array of EE into a dictionnary of EE indexed by the name "n"
	eeArrayToObject(a){
		let r = {}
		for(let e of a) r[e.n]=e;
		return r;
    }

	// extract an object from an Uint8Array interpreted with a dictionnary
	decode(bytes, dict){
		if(Array.isArray(dict.ee)) dict.ee = this.eeArrayToObject(dict.ee);
		let res = {};
		let maxIndex = bytes.length;
		let index = 0;
		let maxLength = dict.hasOwnProperty("maxLength") && typeof dict.maxLength != "string" ? dict.maxLength : +Infinity;
		// map the dict by key code
		let dmap = this._mapDictByCodes(dict);
		while(index < Math.min(maxLength, maxIndex) ){
			let unknown = false; // boolean to skip unknown keys
			let b0 = bytes[index++];
			
			// extract the custom key flag, 
			let kf = b0 & 128;
			
			// extract the value length code
			let vlc = (b0 & 96)>>5;
			
			// extract the keyLength / keyCode
			let key = b0 & 31;
			
			// extract the key name
			let keyName;
			if(kf){
				keyName = this.bytesToAscii( bytes.slice(index, index+key) );
				index += key;
			}
			else if( dmap[key] && dmap[key].c === key){
				keyName = dmap[key].n;
			}
			else unknown = true;
			
			// extract the valueLength
			let vlen;
			if(vlc === 0) vlen = 1;
			else{
				let vlenlen = vlc === 3 ? 8 : vlc;
				vlen = this.bytesToInt( bytes.slice(index, index+vlenlen) );
				index += vlenlen;
			}
			
			// extract the value 
			let v = bytes.slice(index, index+vlen);
			index += vlen;
			
			// if the key is not known in the dictionnary, and no unknown keys are authorized, skip the value
			let ee = dict.ee[keyName];
			// don't add unknown keys
			if(!ee && (dict.allowUndefined !== "decode") ) continue;
			
			// determine the type to decode the data
			let eeType = (ee && ee.t) || "hex";
			let dv = this._eeDecode(v,ee); // decoded value
			
			// replace the code by its user readable value
			dv = this._codeToValue(dv, ee);
			res[keyName]=dv;

			// if the extracted ee is matching maxLength property, it defines the maximum expected bytes length to decode
			if(""+ee.n === dict.maxLength) maxLength = dv;
		}
		
		// add default decode values
		for(let eeName in dict.ee){
			if(res.hasOwnProperty(eeName)) continue;
			let ee = dict.ee[eeName];
			if(ee.hasOwnProperty("dd")) res[eeName] = ee.dd;
			else if(ee.hasOwnProperty("d")) res[eeName] = ee.d;
			else if(ee.r) throw "Missing EE while decoding : " + eeName;
		}
		return res;
	}
	
	// decode a single ee given the byte the byte sequence value
	_eeDecode(v, ee){
		let dv;
		switch(ee.t) {
			case "ascii" : 
				dv = this.bytesToAscii(v);
				break;
			case "utf8" :
				dv = this.utf8BytesToString(v);
				break;
			case "json" :
				dv = JSON.parse( this.utf8BytesToString(v) );
				break
			case "bin" : 
				dv = v;
				break;
			case "hex" : 
				dv = this.bytesToHex(v); 
				break;
			case "bool" : 
				dv = this.bytesToBool(v);
				break; 
			case "int" : 
				dv = this.bytesToInt(v);
				break;	
			case "dict" :
				dv = this.decode(v, ee.tpl);
				break;
			case "arr" : 
				dv = [];
				let arrIndex = 0;
				while(arrIndex < v.length){
					let ei = this._extractEi(v,arrIndex);
					arrIndex = ei.index;
					let elBytes = v.slice(arrIndex, arrIndex + ei.ei);
					dv.push( ee.tpl.ee ? this.decode(elBytes, ee.tpl) : this._eeDecode(elBytes, ee.tpl) );
					arrIndex += elBytes.length;
				}
				break;
			default :
				throw "Unexpected EE type : "+ee.t;
		}
		return dv;
	}
	
	// extract an EI from a byte array
	// return an object containing ei and the new index
	_extractEi(bytes, index){
		if(bytes[index]<128) return { ei : bytes[index] & 127, index : index+1};
		let eil = bytes[index++] & 127;
		let ei = 0;
		for(let i=0; i<eil; i++) ei = ei*256 + bytes[index++];
		return { ei : ei, index : index};
	}

	// convert an integer to its EI representation
	_intToEI(i){
		if(i<128) return new Uint8Array([i]);
		let res = [];
		while(i){
			res.push(i & 0xff);
			i = i>>8;
        }
        res.push(res.length+128);
		return new Uint8Array( res.reverse() );
    }

	// map a dict ee both by keyName and key code
	_mapDictByCodes(dict){
		let r = {};
		let ees = dict.ee;
		for( let n in ees){
			if( ees[n].hasOwnProperty("c") ) r[ ees[n].c ] = ees[n];
		}
		return r;
	}
	
	// compute a value length length from a value length
	_vLenLen(vlen){
		if(vlen===1) return 0;
		else if(vlen<256) return 1;
		else if(vlen<65536) return 2;
		else return 8;
	}
	
	// replace a code by its user readable value
	_codeToValue(v, ee){
		return ee.rpl && ee.rpl.hasOwnProperty(v) ? ee.rpl[v] : v;
	}

	// replace a user readable value by the matching code
	_valueToCode(v, ee){
		if(ee.rpl && !ee.rpli){
			// compute the inverse array
			ee.rpli = {};
			for(let i in ee.rpl) ee.rpli[ ee.rpl[i] ] = i;
		}
		return ee.rpli && ee.rpli.hasOwnProperty(v) ? ee.rpli[v] : v;
	}

    ////////////////////////////////////////////////////////////////////
    /////////////////////        ENCODING MACRO    /////////////////////
    ////////////////////////////////////////////////////////////////////

	// endMacro that compute the length of the object at the end of the
	$length(parts, ee, data){
		// remove ee length parts
		let lengthParts = parts.splice(data.index, data.len);

		// compute the length of all the elements
        let len = this._bytePartsLength(parts);
        let fullLength = len;
        let tmpLen;
        while(fullLength != tmpLen){
        	tmpLen = fullLength;
            lengthParts = [];
            this._pushParts(fullLength, ee, lengthParts);
			fullLength = len + this._bytePartsLength(lengthParts);
        }

        // insert replacing parts representing the length
		parts.splice(data.index, 0, ...lengthParts);
    }


	////////////////////////////////////////////////////////////////////
	//////////////////////////   OTHER HELPERS     /////////////////////
	////////////////////////////////////////////////////////////////////
	
	_initConstants(){
		// initialize constants
		this.BASE_EE_LENGTH = 1;
		this.BYTE_TRUE = new Uint8Array([1]);
		this.BYTE_FALSE = new Uint8Array([0]);
	}
}

/** 
 * threadops manage operation that rely on asynchronous threads
 * **/
class Threadops{
	
	constructor(data={}){
		this.operations = [];
		this.maxConcurrentThreads = data.maxConcurrentThreads || 2;
		this.maxThreads = data.maxThreads || 100;
		this.REQUEST_ID_LENGTH = 6;
		this.sleepTime = data.sleepTime || 1000;
	}
	
	////////////////////////////////////////////////////////////////////
	//////////////////    THREAD OPERATIONS    /////////////////////////
	////////////////////////////////////////////////////////////////////
	
	// remove an operation from its id
	removeOperation(id){
		if(this.operations[id]) delete this.operations[id];
	}
	
	// get an operation from its id
	getOperation(id){
		return this.operations[id] || null;
	}

	// get the list of operations
	getOperations(){
		return this.operations.slice();
	}
	
	// add an operation to the list of operations
	addOperation(op={}){
		if(!op.id) op.id = this._generateRequestId();
		this.operations[op.id] = op;
		if(!op.startTime) op.startTime = Date.now();
		return op.id;
	}

	// start a new operation thread, given an operation object
	// return false if it was impossible to start a new thread, true if a new thread was started
	startOperationThread(op){
		if(op.done) return false;
		if(op.currentThreads >= op.maxConcurrentThreads) return false;
		if(op.totalThreads >= op.maxThreads){
			if(!op.currentThreads) this.rejectOperation(op.id, "max threads launched ("+op.totalThreads+") without resolving for the operation : "+op.name);
			return false;
		}
		op.currentThreads++;
		op.totalThreads++;
		
		// create a callback function to update thread counters and start new threads
		let $this = this;
		let cb = function(r){
			$this.finishOperationThread(op);
			$this.startOperationThreads(op);
		}
		// launching the thread
		let p = op.thread(op);
		p.then(cb,cb);

		// return the number of available threads left
		return true;
	}

	// return a promise to sleep : to be used to avoid threads to be chained to quickly
    sleep(t=this.sleepTime){
	    return new Promise(r=>setTimeout(r,t));
    }
	
	// start operation threads until the limit is reached
	startOperationThreads(op){
		// start threads until the number of available threads returned is 0 or if it is unable to start a new thread
		let previousValue = +Infinity;
		let hasSTartedThread;
		do{
            hasSTartedThread = this.startOperationThread(op);
			//if(!threadsLeft || threadsLeft == previousValue) break;
			//else previousValue = threadsLeft;
		}
		while(hasSTartedThread)
	}
	
	// notify an operation thread has ended for the given operation
	finishOperationThread(op){
		op.currentThreads--;
		// if it was the last thread to be terminated, try to resolve at best the
        if(op.totalThreads >= op.maxThreads && op.currentThreads === 0) {
            this.resolveAtBest(op.id);
        }
	}

	// resolve an operation by using the resolveAtBest method of the operation
    // if none is provided, reject the operation
    resolveAtBest(opId){
	    let operation = this.getOperation(opId);
        if(operation && operation.resolveAtBest) return this.resolveOperation(operation.id, operation.resolveAtBest(operation) );
        else return this.rejectOperation(opId )
    }

	// resume an operation
	resumeOperation(op){
        if(!op.done) return;
        delete op.done;
        op.totalThreads = 0;
		return this.startOperation(op);
	}

	// add an operation to the list of operations
	startOperation(op){
		// if no thread method is given to start new threads, throw an exception
		if(!op.thread) throw "to start an operation, expect a 'thread' function in the data to start new threads";
		if(!op.maxConcurrentThreads) op.maxConcurrentThreads = this.maxConcurrentThreads;
		if(!op.maxThreads) op.maxThreads = this.maxThreads;
		if(!op.name) op.name = "";
		// create a timeout to reject/resolved the operation automatically
		if(op.timeout){
			op.timeoutId = setTimeout(function(){
				if(op.done) return;
				this.rejectOperation(op.id, "Timeout without resolving for the operation : "+op.name);
			}.bind(this), op.timeout);
		}
		op.currentThreads = 0;
		op.done = false;
		op.totalThreads = 0;
		op.promise = new Promise((res,rej)=>{
			op.resolve = res;
			op.reject = rej;
		});
		this.addOperation(op);
		this.startOperationThreads(op);
		return op.promise;
	}
	
	// reject the promise of an operation and stop it
	rejectOperation(opId, rejectData){
		let op = this.getOperation(opId);
		if(!op || op.done) return;
		op.reject(rejectData);
		op.done = true;
		this._stopOperation(op);
		this.removeOperation(opId);
	}

	// reject all the current operations
    rejectOperations(){
		for(let operationId in this.operations){
			this.rejectOperation(operationId, "Forced failure : rejectOperation");
		}
	}

	// prevent new threads of an operation from beeing launched
	_stopOperation(op){
		// op.maxConcurrentThreads = -1;
	}
	
	// resolve an operation and stop it
	resolveOperation(opId, resolveData){
		let op = this.getOperation(opId);
		if(!op || op.done){
            return;
		}
		op.resolve(resolveData);
		op.done = true;
		this._stopOperation(op);
		this.removeOperation(opId);
	}
	
	// create a request id
	_generateRequestId(){
		let id = Math.floor(Math.random() * Math.pow(2,this.REQUEST_ID_LENGTH*8));
		return id;
	}
}

// export the class

// CONCATENATED MODULE: ./src/swans/lib/SwanClient.js
/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

// abstract class that regroups methods (encryption mainly) for all types of clients
class AbstractSwan {

	// expect to be given a secswans object : if not, try to fetch it itself
	constructor(params = {}){

        // create a flaptools if none is provided
        if(!AbstractSwan.prototype._flaptools){
            AbstractSwan.prototype._flaptools = params.flaptools || new Flaptools();
		}
		// this._flaptools = params.flaptools || new Flaptools();

        // retrieve the cryptico library
        if(!AbstractSwan.prototype.secswans){
            AbstractSwan.prototype.secswans = params.secswans || new Secswans();
        }

        this.initializeAbstractClientConstants();
    }
	 
	////////////////////////////////////////////////////
	///// methods to manage swan packets      //////////
	//////////////////////////////////////////////////// 

	// transform a swan object to a byte array
	// to perform encryption, a transactionKeys object must be provided : it will be completed with missing data if needed
	// if no transactionKeys are provided, the body is not encrypted
	// retruns a bytes sequence
	swanObjectToBytes(swanObj, requestKeys=null){
		// duplicate the object to encode
		let decoded = {};
		for(let i in swanObj) decoded[i] = swanObj[i];
		// encrypt the body if it is present
		if(decoded.body && requestKeys) {
            let decrypted = this._flaptools.encode(decoded.body, this.TEMPLATE_BODY);
			let encrypted = this.encrypt(decrypted, requestKeys);
			decoded.body = encrypted;
        }
		let result = this._flaptools.encode(decoded, this.TEMPLATE_MESSAGE);
		return result;
	}

	// transform a byte packet into an object 
	// if requestKeys is provided, try to decode encrypted body and fill the requestKeys with data found in the packet
	swanBytesToObject( swanBytes, requestKeys = null){
		let result = this._flaptools.decode(swanBytes, this.TEMPLATE_MESSAGE);
        // if request keys are provided, fill it with missing, then decrypt the body
        if(result.body && requestKeys){
        	this.decodeMessage(result, requestKeys);
        }
        return result;
	}

	// decrypt and decode the body of a swan message
	decodeMessage(message, requestKeys){
        this.requestKeysFromTransaction(message, requestKeys);
        let decryptedBody = this.decrypt(message.body , requestKeys);
        message.body = this._flaptools.decode(decryptedBody, this.TEMPLATE_BODY);
	}

	// create a random requestId
	createBid(){
		return this._flaptools.randomHex(this.BID_LENGTH);
	}
	
	// check that the signature of an answer match the signature of a request (same requestId)
	checkSignature( answerObject, match){
		if(typeof match === "object") match = match.bid;
		if(typeof match !== "string") match = this._flaptools.bytesToHex(match);
		let answerSig = answerObject.bid;
		if(!answerSig) return false;
		if(typeof answerSig !== "string") answerSig = this._flaptools.bytesToHex(answerSig);
		return answerSig.toUpperCase() === match.toUpperCase();
	}

	//////////////////////////////////////////
	/////// server layer related methods /////
	//////////////////////////////////////////
	
	// create an error byte packet
	createErrorMessage(params={}){
        if(!params.type || !params.type.startsWith("error")) params.type = "error";
		let packetBytes = this.swanObjectToBytes(params);
		return packetBytes;
	}
	
	// create a ping byte packet
	createPingMessage(params={}){
		if(!params.tid) params.tid = this.createTid();
		params.type = "ping";
		let packetBytes = this.swanObjectToBytes(params);
		return packetBytes;
	}
	
	// create a ping answer byte packet
	createPingAnswerMessage(params={}){
        params.type = "pingAnswer";
		let packetBytes = this.swanObjectToBytes(params);
		return packetBytes;
	}
	
	// create a request answer byte packet
	createResponseMessage(params={}){
		// build the client layer message to send
		if(!params.body ) params.body = {};
        if(!params.body.bid ) params.body.bid = this.createBid();
        if(!params.requestKeys) params.requestKeys = {};
        this.requestKeysFromTransaction(params, params.requestKeys);
        params.type =  "requestAnswer";
		let packetBytes = this.swanObjectToBytes(params, params.requestKeys);
		return packetBytes;
	}

	// create a register byte packet
	createRegisterMessage(params={}){
		params.type = "register";
        if(!params.tid) params.tid = this.createTid();
		let packetBytes = this.swanObjectToBytes(params);
		return packetBytes;
	}
	
	// create a register answer byte packet
	createRegisterAnswerMessage(params={}){
		// build the byteArray containing the ascii url
		params.type = "registerAnswer";
		let packetBytes = this.swanObjectToBytes(params);
		return packetBytes;
	}
	
	// create a close byte packet
	createCloseMessage(params={}){
		params.type = "unregister";
		if(!params.tid) params.tid = this.createTid();
		let packetBytes = this.swanObjectToBytes(params);
		return packetBytes;
	}
	
	// create a request server byte packet
	createRequestMessage(params={}){
		if(!params.body) params.body = {};
		if(!params.body.bid) params.body.bid = this.createBid();
		if(!params.tid) params.tid = this.createTid();
		params.type = "request";
		// build the requestKeys
		if(!params.requestKeys) params.requestKeys = {};
        this.requestKeysFromTransaction(params, params.requestKeys);
        params.key = params.requestKeys.transactionKeys.public;
		let packetBytes = this.swanObjectToBytes(params, params.requestKeys);
		return packetBytes;
	}

	// add missing information to the transaction keys, build from request messages
	requestKeysFromTransaction(data, requestKeys = {}){
        let receiverKeys = data.receiver ? this.createReceiverKey(data.receiver) : requestKeys.receiverKeys;
        let transactionKeys = data.key ? this.secswans.createKey(null, data.key) : requestKeys.transactionKeys;
        // fill missing data in requestKeys
        let res = this.createRequestKeys(transactionKeys, receiverKeys, requestKeys);
		return res;
	}

	// extract a good transaction key from a list of transactionKeys, or create a new one
	getTransactionKeys(receiver, transactionKeysList=[]){
		let receiverKeyType = parseInt( receiver.slice(0,2), 16);
		for(let transactionKeys of transactionKeysList){
			if(transactionKeys.type === receiverKeyType) return transactionKeys;
		}
		SwanClientCaller.prototype.called = SwanClientCaller.prototype.called ? SwanClientCaller.prototype.called + 1 : 1;
		let result = this.secswans.generateKey( receiverKeyType );
		return result
	}

	 // create a server message id randomly
	 // possibilities are considered numerous enough to avoid randomly get the same id twice
	 createTid(){
		return this._flaptools.randomHex(this.TID_LENGTH);
	 }
	
	///////////////////////////////////////////
	/////// Encryption related functions //////
	///////////////////////////////////////////
	
	// generate a new key pair and set it at the identifying key for the receiver
	resetReceiverKey(){
		this._receiverKey = this.createReceiverKey();
	}
	
	// get the public key of the receiver
	getPublicKey(){
		return this.getReceiverKey().public;
	}
	
	// get the rsa key object
	getReceiverKey(){
		if(!this._receiverKey) this.resetReceiverKey();
		return this._receiverKey;
	}
	
	// decrypt a message.
	// some algo like DH make use of a foreign key to decrypt : common key is built from and to tempKey
	// accept only Uint8Arrays as message
	decrypt(encryptedMessage, requestKeys){
        // fetch missing data and fill the requestKeys if needed
        // this.createRequestKeys(requestKeys);
		if(!requestKeys.symetric) requestKeys.symetric = this.secswans.computeSymetricKey(requestKeys.transactionKeys, requestKeys.receiverKeys);
		return this.secswans.symetricDecrypt(encryptedMessage, requestKeys.symetric);
	}
	
	// encrypt a message given the receiver key and the request key
	// accept only Uint8array as messages
	encrypt(message, requestKeys){
		// fetch missing data and fill the requestKeys if needed
		// this.createRequestKeys(requestKeys);
        if(!requestKeys.symetric) requestKeys.symetric = this.secswans.computeSymetricKey(requestKeys.transactionKeys, requestKeys.receiverKeys);
		return this.secswans.symetricEncrypt(message, requestKeys.symetric);
	}
	
	// create a new request key, given a transactionKeys and receiverKeys
	// if the transactionKeys are not defined, get the own receiverKeys if type is the same, or create new ones randomly
	// if the receiverKeys are not defined, take the one of the client
	// create a new object or complete the requestKeys object given
	createRequestKeys(transactionKeys = null, receiverKeys=null, requestKeys = {}){
        requestKeys.receiverKeys = requestKeys.receiverKeys || receiverKeys;
        // if no receiverkeys has been provided, take local receiver keys
        if(!requestKeys.receiverKeys){
        	requestKeys.receiverKeys = this.getReceiverKey();
		}

		requestKeys.transactionKeys =  requestKeys.transactionKeys || transactionKeys;

		if(!requestKeys.transactionKeys){
			// try to add the own receiver key, else build new keys
			let type = requestKeys.receiverKeys.type;
            // requestKeys.transactionKeys = ownReceiver.type == type ?  ownReceiver :  this.secswans.generateKey( requestKeys.receiverKeys.type );
            requestKeys.transactionKeys = this.secswans.generateKey( requestKeys.receiverKeys.type );
        }
		return requestKeys;
	}
	
	// create the string representing the receiver identifier
	// basically, it is the public receiver key in hexadecimal representation, preceded by 2 characters representing the encryption type in hexadecimal
	getReceiverId( receiverKey=null){
		if(!receiverKey) receiverKey = this.getReceiverKey();
		let id = parseInt(receiverKey.type).toString(16);
		while(id.length<2) id = "0"+id;
		id += ( typeof receiverKey.public === "string" ? receiverKey.public : this._flaptools.bytesToHex(receiverKey.public) );
		return id.toLowerCase();
	}
	
	// create a receiver key from scratch (public and private) or deduce it from a receiverId (but only produce public key)
	createReceiverKey(receiverId){
		if(receiverId){
			if(typeof receiverId === "string") receiverId = this._flaptools.hexToBytes(receiverId);
			let type = receiverId[0];
			let pubkey = receiverId.slice(1);
			return this.secswans.createKey(null, pubkey, type);
		}
		else {
			let receiverKey = this.secswans.generateKey(this.DEFAULT_ENCRYPTION_TYPE);
			return receiverKey;
		}
	}

	/////////// HELPERS /////////////////
	
	// initialize constants
	initializeAbstractClientConstants(){
        this.DEFAULT_ENCRYPTION_TYPE = this.secswans.ENCTYPE_DH_2048_256;

        // set default ports
        this.DEFAULT_SERVER_CLIENT_PORT = 53475;
        this.DEFAULT_SERVER_RECEIVER_PORT = 53476
        this.DEFAULT_CLIENT_PORT = 53477;
        this.DEFAULT_RECEIVER_PORT = 53478;

		// size for each element of a swan message
		this.BID_LENGTH = 8;
		this.TID_LENGTH = 6;

		// possible operation types
		this.CODE_PING = 0;
		this.CODE_REQUEST = 1;
		this.CODE_REQUEST_ANSWER = 2;
		this.CODE_REGISTER = 3;
		this.CODE_REGISTER_ANSWER = 4;
		this.CODE_UNREGISTER = 5;
		this.CODE_PING_ANSWER = 6;
		this.CODE_ERROR = 7;
		this.CODE_ERROR_FORGED = 8;
		this.CODE_ERROR_UNREGISTERED_RECEIVER = 9;
		
		// correspondances code => type
		this.CODE_TO_TYPE = {
			[this.CODE_PING] : "ping",
			[this.CODE_REQUEST] : "request",
			[this.CODE_REQUEST_ANSWER] : "requestAnswer",
			[this.CODE_REGISTER] : "register",
			[this.CODE_REGISTER_ANSWER] : "registerAnswer",
			[this.CODE_UNREGISTER] : "unregister",
			[this.CODE_PING_ANSWER] : "pingAnswer",
			[this.CODE_ERROR] : "error",
			[this.CODE_ERROR_FORGED] : "errorForged",
			[this.CODE_ERROR_UNREGISTERED_RECEIVER] : "errorUnregisteredReceiver",
		}
		
		// correspondance type => code
		this.TYPE_TO_CODE = {};
		for(let i in this.CODE_TO_TYPE) this.TYPE_TO_CODE[ this.CODE_TO_TYPE[i] ] = i;
		
		// initialize body template
        let ee_b_version = { n : "version", t : "int", c : 0};
        let ee_b_length = { n : "length", t : "int", c : 1, endMacro : "$length", de : 0};
        let ee_b_bid = { n : "bid", t : "hex", c : 2 };
        let ee_b_body = { n : "body", t : "bin", c : 3 };
        let ee_body = [ee_b_version, ee_b_length, ee_b_bid, ee_b_body];
        this.TEMPLATE_BODY = { ee : {}, maxLength : "length" };
        for(let e of ee_body) this.TEMPLATE_BODY.ee[e.n] = e;

		// initialize message template
        let ee_m_version = { n : "version", t : "int", c : 0, dd : 0};
        let ee_m_length = { n : "length", t : "int", c : 1};
        let ee_m_type = { n : "type", t : "int", c : 2, r : true, rpl : this.CODE_TO_TYPE };
        let ee_m_tid = { n : "tid", t : "hex", c : 3, r : true };
        let ee_m_receiver = { n : "receiver", t : "hex", c : 4 };
        let ee_m_key = { n : "key", t : "bin", c : 5 };
        let ee_m_body = { n : "body", t : "bin", c : 6 };
        let ee_m_authkey = { n : "authkey", t : "hex", c : 7 };
        //let ee_m_contact = { n : "contact", t : "ascii", c : 8 };
        let ee_m_contact = {n:"canals", t:"arr", c:8, tpl : NestedInterface.prototype.TEMPLATE_CANAL };



		let ee_m_info = { n : "info", t : "ascii", c : 9 };
        let ee_message = [ee_m_version, ee_m_length, ee_m_type, ee_m_tid, ee_m_receiver, ee_m_key, ee_m_body, ee_m_authkey, ee_m_contact, ee_m_info];
        this.TEMPLATE_MESSAGE = { ee : {} };
        for(let e of ee_message) this.TEMPLATE_MESSAGE.ee[e.n] = e;
	}

    // getter for the socket-io.client lib
	getSocketioClientLib(){
		// retrieve the socket.io client library
		if(!this.socketioClientLib) this.socketioClientLib = __webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'socket.io-client'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()));
		return this.socketioClientLib;
	}
}

// implementation of the swan receiver client
// it registers to the swan server by giving a public rsa encryption key and wait for the messages
class SwanClientReceiver extends AbstractSwan{
	/***
	 * possible params : 
	 * server : the address of the swan server
	 * token : a registration token (in the form of a Uint8Array) to send to the swans server to enable registration
	 * onsuccess : a callback to be triggered once the registration occured (called with a swan server object as parameter)
	 * ****/
	constructor(params){
		if(!(params instanceof Object && params)) params = {};
		super(params);
		
		// initialize various constants
		this.initializeConstants();
		
		// initialize the swan server address
		this.swansInterface = null;

		// swans server port for nested interface
		this.swansServerPort = this.swansServerPort || this.DEFAULT_SERVER_RECEIVER_PORT;

		// receiver port for nested interface
		this.port = params.port || this.DEFAULT_RECEIVER_PORT;
		
		// at first, the server is stopped
		this.status = this.STATUS_STOP;
	}
	
	// start the server connection
	registerInit(swansInterface, onregister, registerToken){
		let swClient = this;
		onregister = onregister || function(){};
		this.status = this.STATUS_STARTING;
		this.swansInterface = swansInterface;
		this._firstMessage = true; // variable to check the registration result differently
		// send a message for registration
		let registerMessage = this.registerMessage(registerToken);
		if(swansInterface.status() === "UP"){
			swansInterface.send(registerMessage, this.swansServerPort, this.port);
		}
		else{
			swansInterface.addHook(swansInterface.e.CONNECT, function(){
				swansInterface.send(registerMessage, swClient.swansServerPort, swClient.port);
			});
		}
		// set connection event
		this.swansInterface.addHook(this.swansInterface.e.CONNECT, function(){
			swClient.status = swClient.STATUS_RUNNING;
		});
		
		// set message event
		this.swansInterface.addHook(this.swansInterface.e.RECEIVE, function(event){
			let message = event.message;
			// decode the message
			let requestKeys = { receiverKeys : swClient.getReceiverKey() };
			let packetObject = swClient.swanBytesToObject(message, requestKeys);
			// the first message received is the result of the register operation
			if(swClient._firstMessage){
				swClient._firstMessage = false;
				return swClient.processRegisterAnswer(packetObject, onregister);
			}
			// when receiving ping, send a ping answer
			switch(packetObject.type){
				case "ping" : return swClient._processPing(packetObject);
				case "request" : swClient._processRequest(packetObject, requestKeys, event);
			}
		},
			// use the receiver port
			this.port
		);
		
		// set error event
		this.swansInterface.addHook(this.swansInterface.e.ERROR, function(){
			swClient.status = this.STATUS_ERROR;
		});
	}
	
	// process incoming ping message
	_processPing(packetObject){
		let response = swClient.createPingAnswerMessage({
			tid : packetObject.tid,
		});
		this.sendArrayMessage(response);
	}
	
	// process incoming request :
	// packetObject : the swan object to process
	// requestKeys : all the necessary data about how to encrypt the answer
	_processRequest(packetObject, requestKeys, event){
		// if an answer is provided for requests, send it back
		let answer = this.callback ? this.callback( (packetObject.body||{}).body, event ) : null;
		if(!(answer instanceof Promise)) answer = Promise.resolve(answer);
		// send back the request result
		let client = this;
		answer.then( function(answerValue){
			let response = client.createResponseMessage({
				tid : packetObject.tid,
				body : {
					body : answerValue || new Uint8Array(),
					bid : packetObject.body && packetObject.body.bid ? packetObject.body.bid : client.createBid()
				},
                requestKeys : requestKeys  // set the key to encrypt the answer
			});
			client.sendArrayMessage(response);
		});
	}
		
	// create a register message to send to the swans server
	registerMessage(authkey){
		let data = {
            receiver : this.getReceiverId()
        };
		if(authkey) data.authkey = authkey;
		let msg = this.createRegisterMessage(data);
		return msg;
	}
	
	// close the connection with the swan server
	close(stopInterface=true){
		this.status = this.STATUS_STOP;
		let closeMessage = this.createCloseMessage();
		this.sendArrayMessage(closeMessage);
		if(stopInterface && this.swansInterface) this.swansInterface.stop();
	}
	
	// process the register answer : create a contact url associated
	processRegisterAnswer(serverMessageObject, onregister){
		if(serverMessageObject.type.startsWith("error")){
			this.close();
			this.status = this.STATUS_ERROR;
			return;
		}
		else{
			// store the receiver canals
			this.serverCanals = serverMessageObject.canals;
		}
		onregister(serverMessageObject);
		return;
	}
	
	// get the status of the link with the swans server
	getStatus(){
		return this.status;
	}
	
	// send a message in server layer protocol format to the server
	sendArrayMessage(byteMessage){
		if(!this.swansInterface || this.swansInterface.status() !== "UP") return false;
		this.swansInterface.send(byteMessage, this.swansServerPort, this.port);
		return true;
	}
	
	// add a callback to execute upon message reception
	setCallback(f){
		if(f && {}.toString.call(f) === '[object Function]'){
			this.callback = f;
			return true;
		}
		else return false;
	}
	 
	// remove all the current callbacks
	clearCallback(){
		this.callback = null;
	} 
	
	// get the list of callbacks
	getCallback(){
		return this.callback; 
	}
	
	initializeConstants(){
		// set various possible status for receiver client 
		this.STATUS_UNDEFINED = "undefined" // parameters about the connection has not been given
		this.STATUS_STARTING = "starting"; // when connection is beeing established without result yet
		this.STATUS_STOP = "stop"; // the connexion has not been started or has been stoped 
		this.STATUS_RUNNING = "running"; // the connection is going well 
		this.STATUS_ERROR = "error"; // the connection is stopped because of an error
	}
}

// implementation of the swan client caller : 
// it contacts a swans server to transmit messages to a swan client receiver
class SwanClientCaller extends AbstractSwan {
	constructor(params={}){
		super(params);
		// must provide a swans interface where to send the messages
		this.swansServerInterface = params.swansServerInterface;
		// must provide a receiverId if request are to be made to a swans receiver
		this.receiverId(params.receiverId);
		// might provide ports where to send request
		this.port = params.port || this.DEFAULT_SERVER_CLIENT_PORT;
		// might provide a port from which to send messages
		this.fport = params.fport || this.DEFAULT_CLIENT_PORT;

		// initialize the hooks to execute callback when receiving request answers
		if(this.swansServerInterface) this.initHooks(this.swansServerInterface);

		// create a list of callbacks to be called, indexed by a tid
		this._callbacks = {};

		// keep a list of transaction keys ot use instead of generating new ones
		this._transactionKeysList = params.transactionKeysList || [];

		// remember of the pair of keys created by the caller for communication : it is initialized during the first request
		this._requestKeys = {}; // tid => requestKeys
	}

	// reset the receiver id if a value is provided, then return it
	receiverId(v){
		if(v) this._receiverId = v;
		return this._receiverId;
	}

	// get/set the list of transactionKeys to be used by default
	transactionKeysList(trk=null){
		if(trk) this._transactionKeysList = trk;
		return this._transactionKeysList;
	}

	// close the caller and all associated interface
	close(){
		if(this.swansServerInterface) this.swansServerInterface.stop();
	}

	// send a ping to a swan server : return the promise associated 
	// modify pingData in place to add missing packet parameters
	pingServer( options={}){
		// create the ping byte message (keep or create a tid)
		let pingData={ tid : options.tid };
		let pingMessage = this.createPingMessage(pingData);
		options.tid = pingData.tid;
		
		// create the packet to be sent
		let packet = {
			message : pingMessage,
			port : this.port,
			fport : this.fport
		};
		let client = this;
		let promise = new Promise((onsuccess, onerror)=>{
				client.sendMessage( packet, pingData.tid, onsuccess, onerror);
		});
		return promise;
	}
	
	// send a request message to a given receiver
	// bodyContent : the byte to send as the body of the swan request message
	// return a promise wich resolve when data is received about the request
	sendRequest(bodyContent, requestData={}){
		// create the server message
		requestData.body = { body : bodyContent };
		requestData.receiver = this.receiverId();
		// get a transaction key to use, add it if a new one is generated
		let transactionKeys = this.getTransactionKeys(requestData.receiver, this.transactionKeysList());
        requestData.requestKeys = { transactionKeys : transactionKeys }
		let serverMessage = this.createRequestMessage( requestData );
		this._requestKeys[requestData.tid] = requestData.requestKeys;
		let swCaller = this;

		// create the packet to be sent
		let packet = {
			message : serverMessage,
			port : this.port,
			fport : this.fport
		}

		let promise = new Promise(function (onsuccess, onerror){
			// when receiving a server result, decrypt the client message inside it
			let checkSignatureOnSuccess = function(swanPacket){
				// in case of error, return the server message
				if( swanPacket.type !== "requestAnswer"){ 
					onerror(swanPacket); 
					return; 
				} 
				// check the signature
				let signatureCorrect = swCaller.checkSignature( swanPacket.body, requestData.body);
				if(signatureCorrect){
                    if(swCaller._requestKeys[swanPacket.tid]) delete swCaller._requestKeys[swanPacket.tid];
                    onsuccess( swanPacket );
				}
				else onerror({code:swCaller.CODE_ERROR_FORGED, message: "swans client answer reading : incorrect signature"});
			}
			swCaller.sendMessage(packet, requestData.tid, checkSignatureOnSuccess, onerror);
		});
		return promise;
	}
	
	// send a message given through a swan server address
	sendMessage(packet, tid, onsuccess, onerror){
        // save the callbacks for later
        if(tid){
            this._callbacks[tid] = {
                onsuccess : onsuccess,
                onerror : onerror
            }
        }
		this.swansServerInterface.send(packet.message, packet.port, packet.fport);
	}

	// initialize the nested interface hooks
	initHooks(ni){
		let swclient = this;

		// when receiving a new message, check if the tid match a request tid : if so, call the approprate callback if it exists
        ni.addHook(ni.e.RECEIVE, function(event){
            let byteResult = event.message;
            let packetResult = swclient.swanBytesToObject( byteResult);
            let tid = packetResult.tid;
            // decode body if needed
            if(swclient._requestKeys[tid] && packetResult.body) {
                swclient.decodeMessage(packetResult, swclient._requestKeys[tid])
            }
            if(!swclient._callbacks[tid] ) return;
            let onsuccess = swclient._callbacks[tid].onsuccess;
            delete swclient._callbacks[tid];
            if({}.toString.call(onsuccess) === '[object Function]') onsuccess(packetResult);
        },
			this.DEFAULT_CLIENT_PORT
		);
	}

}



// try to export the class for nodejs environment

// CONCATENATED MODULE: ./src/swans/lib/SwansServer.js
/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

/***
 * SWANS stands sor "Signaling With ANonymous Server"
 * it defines a protocol to send message from two client separated by a server that might not be reliable
 * it uses ssl encryption to preserve the integrity of the data transmitted
 * a SwanClient register to a SwanServer by providing the server a ssl public key 
 * this establishes a connection between the client and the server. 
 * the server in return provide a url to contact the client through the server : in particular, the url contains an identifier, which is the public ssl key
 * another client uses the url to send encrypted data to the SwanServer, which retransmit it to the first swan client
***/



// server class to implement the swan protocol
class SwansServer_SwanServer extends AbstractSwan{

	/** create a new swanServer and start it
	 * it relies on express so it should be available with require
	 * constructor needs parameters :
	 * 		clientInterface :  nested interface that receives requests that are transfered to receivers
	 *      receiverInterface : interface used to register and create new connections with receivers
	 *      url  :  if not provided, the receiver url is the root url
	 * 		requestTimeout : if no answer is received for a request after that time, send a server message error (in ms)
	 * 		receiverTimeout : if no message has been received from receivers after that time, close the receiver (in ms)
	 * 		routinePeriod : frequence at which the server perform tasks like checking if the receivers or requests have expired
	 *      port : port used for listening to remote messages
	***/
	constructor(params = {}){
		super(params);

		// initialize constants to be used for the api
		this.initializeConstants();

		this.clientInterface = params.clientInterface;
		this.receiverInterface = params.receiverInterface;
		this.url = params.url || null;
		this.requestTimeout = params.requestTimeout || this.DEFAULT_REQUEST_TIMEOUT;
		this.registerTimeout = params.registerTimeout || this.DEFAULT_REGISTER_TIMEOUT;
		this.receiverTimeout = params.receiverTimeout || this.DEFAULT_RECEIVER_TIMEOUT;
		this.routinePeriod = params.routinePeriod || this.DEFAULT_ROUTINE_PERIOD;
		// this.port = params.port || this.DEFAULT_SERVER_PORT;
		this.clientPort = params.clientPort || this.DEFAULT_SERVER_CLIENT_PORT;
		this.receiverPort = params.receiverPort || this.DEFAULT_SERVER_RECEIVER_PORT;
		// create a list of waiting promises : once an answer is received, the promise is resolved
		this.initializeWaitingPromises();

		this.started = false;

		// create and start the server with those settings
		this.start();
	}

	////////////////////////////////////////////////////////
	/////// GENERAL METHODS TO HANDLE THE SWAN SERVER //////
	////////////////////////////////////////////////////////

	// start the swan server
	start(){
		this.started = true;
		// start the receiver and client interfaces
		this.startClientInterface();
		this.startReceiverInterface();

		// create a routine to check for pending promises / inactive receivers
		let $this = this;
		this.routine = setInterval(function(){
            $this.checkPendingRequests();
            $this.checkReceiverConnections();
		}, this.routinePeriod);
	}

	// stop the swan server
	stop(){
        this.started = false;
		this.stopClientInterface();
		this.stopReceiverInterface();

		// stop the check routine
		clearInterval(this.routine);
	}

	startReceiverInterface(){
		// list of the receiver connections
		// the key is the identifier of the receiver (the public rsa key written in base 64)
		// property interface contains a nestedInterface object representing the connection
		// property lastAlive contains the last time the receiver made contact
		this.receivers = {};

		this.initializeReceiverInterface();
	}

	stopReceiverInterface(){
		// close the main server
		if(this.receiverInterface) this.receiverInterface.stop();

		// close all the sockets
		for(let key in this.receivers){
			this.closeReceiver(key);
		}
	}

    /**
	 * add a callback to be launched when a registration occurs
     */
    registerCallback(c){
    	if(c) this._registerCallback = c;
    	return this._registerCallback;
	}

	initializeReceiverInterface(){
		let swServer = this;
		let spawnHook = function (event) {
			let spawnInterface = event.spawn;
            spawnInterface.test = "serverReceiverspawn"
			let receiverKey = null;
			// initialize events to receive and process messages
			spawnInterface.addHook(spawnInterface.e.RECEIVE, function (event) {
			    let t1 = Date.now();
				let data = new Uint8Array(event.message);
				let swanServerMessage = swServer.swanBytesToObject(data, null, false);
				// processing register messages
				if(swanServerMessage.type === "register"){
					receiverKey = swanServerMessage.receiver;
					// check if this receiver can register
					if( !swServer.canRegister( receiverKey , swanServerMessage.body) ){
						let registerAnswerObject = {
							id : swanServerMessage.tid,
							info : swServer.ERROR_DESCRIPTION_UNAUTHORIZED
						}
						let registrationErrorMessage = swServer.createErrorMessage( registerAnswerObject );
						spawnInterface.send(registrationErrorMessage, swServer.flockPort, event.fport);
					}
					else{
						// send to the receiver an url to be contacted through the server
						let registerAnswerMessage = swServer.createRegisterAnswerMessage({
							tid : swanServerMessage.tid,
							canals : swServer.clientInterface.getCanals()//  swServer.createReceiverUrl()
						});

						// register the receiver
						swServer.registerReceiver( receiverKey, spawnInterface, event.fport );
						// send register answer
						swServer.sendMessageToReceiver( receiverKey, registerAnswerMessage);
					}
					return;
				}

				// processing any other type of message
				let params = {
					fromReceiver : receiverKey,
					tid : swanServerMessage.tid
				};
				let requestResultPromise = swServer.processRequest(swanServerMessage, params);
				if( !requestResultPromise) return; // some request don't wait for any response to be given
				requestResultPromise.then(function(swanServerAnswerMessage){
					if(!swanServerAnswerMessage) return;
					// if the message is not a Uint8array, it is a normal object -> convert it into bytes
					if( !(swanServerAnswerMessage instanceof Uint8Array) ){
						swanServerAnswerMessage = swServer.swanObjectToBytes( swanServerAnswerMessage, false );
					}
					swServer.sendMessageToReceiver( receiverKey, swanServerAnswerMessage);
				});

			},
			swServer.receiverPort);
		}

		this.receiverInterface.addHook( this.receiverInterface.e.SPAWN, spawnHook);
	}

	// hook for spawn event on swans server interfaces
	spawnEvent(){
	}

	// start the http server
	startClientInterface(){
		// stop previous server just in case
		this.initializeClientInterface();
	}

	// stop the http server
	stopClientInterface(){
		if(!this.clientInterface) return;
		this.clientInterface.stop();
	}

	// initialize a client interface
	// on spawn, it checks if a message is attached to the event, if so, it tries to send back an answer
	initializeClientInterface(){
		var swServer = this;

		var spawnHook = function(event){
		  let spawn = event.spawn;
		  spawn.test = "serverClientSpawn";
		  let byteMessage = event.message;

		  // add a hook to process received messages
		  let receiveHook = function(event){
		  	  if(!swServer.started) return;
              let byteMessage = event.message

              // transform the request object into a swan layer server
              let swanServerMessage = swServer.swanBytesToObject( byteMessage, null, false );
              let processPromise = swServer.processRequest(swanServerMessage);
              // for requests that don't return a promise, don't do anything
              if(!processPromise) return;
              processPromise.then(function(swanServerAnswerMessage){
                  if(!swServer.started) return;
                  if(!swanServerAnswerMessage) return;
                  // if the message is not a Uint8array, it is a normal object -> convert it into bytes
                  if( !(swanServerAnswerMessage instanceof Uint8Array)){
                      swanServerAnswerMessage = swServer.swanObjectToBytes( swanServerAnswerMessage, false );
                  }
                  if(spawn.status() !== "UP"){
                      console.log("WARNING : swans Client disconnected, unable to proceed", JSON.stringify(swanServerMessage) );
                      return;
				  }
                  spawn.send( swanServerAnswerMessage, event.fport, swServer.clientPort );
                  // TODO : UNCOMMENT
                  //spawn.close();
              })
			  // .catch(function(e){
				// console.log("ERROR DURING SWANS SERVER REQUEST EXECUTION ", e);
				// spawn.close();
			  // })

		  }

		  spawn.addHook(spawn.e.RECEIVE, receiveHook, swServer.clientPort);
		  // in some interface, a message is sent during the connection initialization
		  if(event.message) spawn.triggerReceive(event);
		};

		// link the route to the request processing
		this.clientInterface.addHook(this.clientInterface.e.SPAWN, spawnHook);
	}

	////////////////////////////////////////////////////
	//////////  METHODS TO HANDLE RECEIVERS ////////////
	////////////////////////////////////////////////////

	// check if a given client can register to the swanServer
	// this function should be overwritten to match specific needs :
	// by default, it counts the number of active connections and only open a new one if there is some room left
	// receiver is the hexadecimal receiver key to register
	// request is an Uint8Array
	canRegister( receiver, request ){
		// a key must be present to identify the caller : it is generally a public rsa key
		if(!( receiver && typeof receiver === "string") ) return false;

		// cannot register the same caller twice
		if( receiver in this.receivers ) return false;

		// everything is ok
		return true;
	}

	// send a message to a receiver
	sendMessageToReceiver( receiverId, byteMessage){
		if(!this.isRegistered(receiverId)){
			throw "Unknown receiver : "+receiverId;
		}
		let rec = this.receivers[receiverId];
		if(!rec.interface){
			console.log("ERROR : CANNOT sendMessageToReceiver without interface");
			return;
		}
		if(!rec.interface.status() === "UP"){
            console.log("ERROR : CANNOT sendMessageToReceiver without UP interface");
            return;
		}
		rec.interface.send( byteMessage, rec.port, this.flockPort );
	}

	// check the socket.io connections state : remove dead ones
	checkReceiverConnections(){
		for(let key in this.receivers){
			// check the last update of the receiver
			if(this.receivers[key].interface.status() === "UP") this.receivers[key].lastAlive = Date.now();
			let isExpired = Date.now() - this.receivers[key].lastAlive > this.receiverTimeout;
			if(isExpired){
				this.closeReceiver(key);
			}
		}
	}

	// check pending requests : remove the expired ones by resolving them with errors
	checkPendingRequests(){
		for(let key in this.waitingPromises){
			let isExpired = Date.now() - this.waitingPromises[key].time > this.requestTimeout;
			if(isExpired){
				let swanTimeoutMessage = this.createErrorMessage({
					tid : this.waitingPromises[key].originalId,
					info : this.ERROR_DESCRIPTION_TIMEOUT
				});
				this.resolveWaitingPromise( key, this.waitingPromises[key].receiverKey, swanTimeoutMessage)
			}
		}
	}

	// check if an id is a registered receiver
	isRegistered(id){
		return id in this.receivers;
	}

	// register a new receiver
	registerReceiver(key, nestedInterface, port){
		this.receivers[key] = {
			interface : nestedInterface,
			port : port,
			key : key
		};
		this.updateReceiverLastAlive(key);
		let $this = this;
		nestedInterface.addHook(nestedInterface.e.DISCONNECT, function(){
            delete $this.receivers[key];
		});
		if(this._registerCallback) this._registerCallback(this.receivers[key]);
	}

	// retrieve a receiver
	getReceiver(id){
		return this.receivers[id];
	}

	// update the lastAlive date of a receiver
	updateReceiverLastAlive(receiverKey){
		let receiver = this.getReceiver(receiverKey);
		if(receiver) receiver.lastAlive = Date.now();
	}

	// close a receiver connection
	closeReceiver(key){
		if(!(key in this.receivers)) return;
		this.receivers[key].interface.stop();
		delete this.receivers[key];
	}

	////////////////////////////////////////////////////
	///// methods to manage swan server layer //////////
	////////////////////////////////////////////////////

	// create a contact url to send request to a registered client
	createReceiverUrl(){
		return this.url || "";
	}

	/*** process swan server message
	     params might define :
			fromReceiver : the id of the receiver sending the request
			promiseId : the id of the promise to resolve once an answer is retrieved
	***/
	processRequest(swanServerMessage, params = {}){

		// remember the receiver is still alive
		if(params.fromReceiver) this.updateReceiverLastAlive(params.fromReceiver);
		switch( swanServerMessage.type ){
			case "ping" :
				let swanPingAnswerMessage = this.createPingAnswerMessage({
					tid : swanServerMessage.tid
				});
				return Promise.resolve(swanPingAnswerMessage);
				break;
			case "request" :
				return this._processRequestServerMessage(swanServerMessage, params);
				break;
			case "requestAnswer" :
				return this._processResponseServerMessage(swanServerMessage, params);
				break;
			case "unregister" :
				return this._processCloseServerMessage(swanServerMessage, params);
				break;
			// registration, registrationAnswer message are not expected
			case "register" :
			case "registerAnswer" :
			// no special treatment when receiving ping answers or errors
			case "pingAnswer" :
			case "error" :
			// by default, dont do anything either
			default :
				return null;
				break;
		}
	}

	// process a close request
	// only valid if it comes from a receiver
	_processCloseServerMessage(swanRequestMessage, params){
		if(!params.fromReceiver) return;
		this.closeReceiver(params.fromReceiver);
	}

	// process a response given as a swan server layer object
	// try to find the matching request, and forward the answer to the caller
	_processResponseServerMessage( swanRequestMessage, params){
		// rename the tid if an alias is found
		swanRequestMessage.tid = this.promiseAlias( swanRequestMessage.tid, params.fromReceiver);
		// let byteMessage = this.swanObjectToBytes(swanRequestMessage, false);
		// find out if there is a waiting promise, and resolve it
		this.resolveWaitingPromise(params.tid, params.fromReceiver, swanRequestMessage);
	}

	// process a request given as a swan server layer object
	// try to find a matching receiver, forward the message and wait for an answer
	_processRequestServerMessage( swanRequestMessage, params){
		// check if the caller is registered
		let targetReceiver = swanRequestMessage.receiver;
		if(!this.isRegistered(targetReceiver)){
			let errorMessage = this.createErrorMessage({
				tid : swanRequestMessage.tid, // this.promiseAlias( params.promiseId, params.fromReceiver),
				info : "unknown receiver",
                type : this.CODE_TO_TYPE[this.CODE_ERROR_UNREGISTERED_RECEIVER]
			});
			return Promise.resolve(errorMessage);
		}
		let receiver = this.getReceiver(targetReceiver);
		// store a promise to be resolved when receiving an answer
		let originalTid = swanRequestMessage.tid;
		let tidAlias = this.createTid(); // id that will be sent to the caller
		let resolver;
		let callbackPromise = new Promise(res=>resolver=res);
		this.addWaitingPromise(tidAlias, resolver, targetReceiver, originalTid);
		// forward the request to the receiver
		swanRequestMessage.tid = tidAlias;
		let byteMessage = this.swanObjectToBytes(swanRequestMessage, false);
		this.sendMessageToReceiver( targetReceiver, byteMessage);
		// return the promise
		return callbackPromise;
	}

	// create a list of waiting promises : once an answer is received, the promise is resolved
	// key : a swan server message id
	// promise : the promise to resolve, with a byte message
	// receiverKey* : the key of the receiver if it is associated to it
	// originalId* : the id of the message when the request was received (since the server may create aliases when forwarding messages)
	// time : the ms time at which the promise was set as pending
	initializeWaitingPromises(){
		this.waitingPromises = {};
	}

	// add a promise to the list of waiting promises
	addWaitingPromise(tid, resolver, receiverKey=null, originalId=null){
		this.waitingPromises[tid] = {
			resolver : resolver,
			receiverKey : receiverKey,
			originalId : originalId,
			time : Date.now(),
		}
	}

	// check if there is a waiting promise matching
	isWaitingPromise(tid, receiverKey=null){
		return !!(this.waitingPromises[tid] && this.waitingPromises[tid].receiverKey == receiverKey);
	}

	// retrieve a given promise
	retrieveWaitingPromise(tid, receiverKey=null){
		return this.waitingPromises[tid] && this.waitingPromises[tid].receiverKey == receiverKey
		? this.waitingPromises[tid] : null;
	}

	// remove a waiting promise
	removeWaitingPromise(tid, receiverId){
		if(this.waitingPromises[tid] && this.waitingPromises[tid].receiverKey == receiverId){
			delete this.waitingPromises[tid];
		}
	}

	// retrieve the original request Id for a given waiting receiver/requestAlias
	promiseAlias( messageAlias, receiverKey){
		return this.waitingPromises[messageAlias] && this.waitingPromises[messageAlias].receiverKey == receiverKey ?
			   this.waitingPromises[messageAlias].originalId : null;
	}

	// resolve a waiting promise with a given swan server message object
	// don't do anything if no matching promise is found
	resolveWaitingPromise( tid, receiverId, swanServerMessage){
		let waitingPromise = this.retrieveWaitingPromise(tid, receiverId);
		if(!waitingPromise) return;
		waitingPromise.resolver( swanServerMessage );
		this.removeWaitingPromise(tid, receiverId);
	}

	////////////////////////////////////////////////////
	//////////   HELPERS ///////////////////////////////
	////////////////////////////////////////////////////

	initializeConstants(){
        this.DEFAULT_REQUEST_TIMEOUT = 120000;
		this.DEFAULT_RECEIVER_TIMEOUT = 10000;
		this.DEFAULT_REGISTER_TIMEOUT = 10000;
		this.DEFAULT_ROUTINE_PERIOD = 8000;
		this.ERROR_DESCRIPTION_TIMEOUT = "swans timeout";
		this.ERROR_DESCRIPTION_UNAUTHORIZED = "unauthorized";
	}
} 


// CONCATENATED MODULE: ./src/NestedInterface.js/lib/NestedInterface.js
/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/





class NestedInterface_NestedInterface{
	constructor(){
		// initialize the events
		this.initializeEvents();
		
		// current status
		this._status = this.NEW;
	}
	
	/*** the following are abstract methods that needs to be implemented
	***/
	
	// method to start the interface
	start(){
		this.triggerConnect();
		this.status(this.RUNNING);
	}
	
	// method to stop the interface
	stop(){
		this.status(this.STOPPED);
		this.triggerDisconnect();
	}
	
	// method to send a Uint8Array through the nested interface
	send(message, dp=0, fp=0){
		this.triggerSend({
			message : message, 
			port : dp,
			fport : fp
		});
	}
	
	// check if the spawner can create a spawn from a canal data
	canSpawn(canal={}){
		return false;
	}

	// check if one of the given canal can be spawned
	canSpawnAny(canals=[]){
		for(let c of canals){
			if(this.canSpawn(c)) return c;
		}
		return false;
	}
	
	// create a new spawn from a canal data
	spawn(canal={}){
		throw "cannot create spawn from NestedInterface abstract class";
	}
	
	// for spawner only : give the list of canals that can be used to access the spawner
	// result is an array of canal, each of which must contains :
	// type : the integer type identifying the canal type
	getCanals(){
		return [];
	}
	
	status(newStatus=null){
		switch(newStatus){
			case this.RUNNING :
			case this.STOPPED :
			case this.UNKNOWN :
			case this.NEW     :
				this._status = newStatus;
				break;
			// by default, don't change the status if non eis porvide or fi invalid
			default : 
				break;
		}
		return this._status;
	}
	
	/*** THE FOLLOWING ARE THE METHDOS TO BE USED IN INHERITING CLASS
	 IT TRIGGERS REGISTERED HOOKS UPON EVENTS
	***/
	
	// trigger the CONNECT event
	triggerConnect(data={}){
		let triggerData = {
			name : this.e.CONNECT,
			interface : data.interface || this,
		}
		this.trigger( triggerData );
	}
	
	// trigger the DISCONNECT EVENT
	triggerDisconnect(data={}){
		let triggerData = {
			name : this.e.DISCONNECT,
			interface : data.interface || this,
		}
		this.trigger( triggerData );
	}
	
	// trigger the RECEIVE EVENT
	triggerReceive(data={}){
		let triggerData = {
			name : this.e.RECEIVE,
			message : data.message,
			port : data.port,
			fport : data.fport,
			interface : data.interface || this,
		}
		this.trigger( triggerData );
	}
	
	// trigger the SEND EVENT
	triggerSend(data={}){
		let triggerData = {
			name : this.e.SEND,
			message : data.message,
			port : data.port,
			fport : data.fport,
			interface : data.interface || this
		}
		this.trigger( triggerData );
	}
	
	// trigger the SPAWN EVENT : input is the bytes associated to the generation of the spawn
	triggerSpawn(data={}){
		let triggerData = {
			name : this.e.SPAWN,
			message : data.message,
			port : data.port,
			fport : data.fport,
			spawn : data.spawn,
			interface : data.interface || this,
		}
		this.trigger( triggerData );
	}

	// check if the interface has a registered hook for the event / port
	hasHook(eventName, port=null){
		let events = this.hooks[eventName] || [];
		for(let e of events){
			if(port === null || port == e.port) return e;
		}
		return false;
	}
	
	// expect at least a hook name, and any number of parameter for the hook
	// the hook can either be a hook name, or an object containing the name and the port of the hook triggered
	trigger(event, hooksData){
		// retrieve the list of hooks that might be activated
		if(!hooksData) hooksData = this.hooks;
		if(!this.e.hasOwnProperty(event.name)){
			throw "no event "+event.name+"could be triggered. Only the following events are available : "+Object.keys(this.e).join(", ");
		}
		if(!hooksData) hooksData = this.hooks;
		// check there is at least one hook to trigger
		if(hooksData[event.name].length === 0 ) return;

		for(let hookData of hooksData[event.name]){
			// skip hooks with the wrond port
			if( (event.port && hookData.port) && event.port != hookData.port) continue;
			try{
				hookData.method.call(null, event);
			}
			catch(e){
				console.log(e);
			}
		}
	}
	
	/*** methods to initialize and manage hooks
	 * ***/
	 addHook(hookName, method, port=null){
		 // check the hook to implement exists
		 if(!this.hookExists(hookName)) return false;
		 // check the method is a function
		 if( !(method && {}.toString.call(method) === '[object Function]')) return false;
		 this.hooks[hookName].push({
			method : method,
			port : port
		});
	 }
	 
	 getHooks(hookName){
		return this.hookExists(hookName) ? this.hooks[hookName] :  null;
	 }
	 
	 hookExists(hookName){
		 return typeof hookName === "string" && hookName in this.hooks;
	 }
	 
	 // remove hooks given a hookname
	 // accept additional params : if methods are provided after the hookname, remove only the given methods
	 removeHooks(hookName){
		 // check the hook to process exists
		 if(!this.hookExists(hookName)) return false;
		 let methodsToRemove = Array.from(arguments).slice(1);
		 if(methodsToRemove.length === 0){
			 this.hooks[hookName] = [];
		 }
		 else{
			var newHooks = [];
			for(let hook of this.hooks[hookName]){
				if(!methodsToRemove.includes(hook.method)){
					newHooks.push(hook);
				}
			}
			this.hooks[hookName] = newHooks;
		 }
		 return true;
	 }
	
	// initialize the list of available events for use
	initializeEvents(){
		// list some hooks to be executed upon every event
		this.hooks = {};
		for(let event in this.e) this.hooks[event] = [];
	}
	
	// retrieve/set the core data
	core(data=null){
		if(data) this._core = data;
		return this._core;
	}
	

	/*** some interface are connexions created by a server (socket.io for instance)
	 * those interfaces are called spawns : they keep a reference of a parent (spawnParent property) and their parent refer to them with an id (spawnId property)
	 * ***/
	isSpawn(){ return !!this.getSpawnParent() }
	
	getSpawnParent(){ return this._spawnParent; }
	setSpawnParent(parent){ this._spawnParent = parent; }
	
	
	/*** other helpers
	 * ***/
	 // check if an object is inheriting of AbstractNestedInterface
	isInterface(i){
		return i instanceof AbstractNestedInterface;
	}
	
	bytesToAscii(codePoints){
		let result = String.fromCharCode(...codePoints);
		return result;
	}
	
	asciiToBytes(ascii="", arr=null, offset=0){
		if(!offset) offset = 0;
		if(!arr) arr = new Uint8Array(ascii.length);
		for(let i =0; i<ascii.length; i++){
			arr[offset+i] = ascii.charCodeAt(i);
		}
		return arr;
	}
	
	// check if the environment is browser(true) or nodejs
	isNode(){
		return typeof global !== "undefined";
	}
	
	// return a promise that resolves when the interface status is not NEW anymore
	_buildAndWaitInterface(builder){
		let promise = new Promise(function(resolve, reject){
			try{
				let ni = builder();
				if(ni.status() !== ni.NEW) return resolve(ni);
				// add a hook to be triggered when the interface status is ready
				ni.addHook(ni.e.CONNECT, function(e){
					ni.removeHooks(ni.e.CONNECT);
					ni.removeHooks(ni.e.DISCONNECT);
					resolve(ni);
				});
				// add a hook in case the ni stopped right before even starting
				ni.addHook(ni.e.DISCONNECT, function(e){
					ni.removeHooks(ni.e.CONNECT);
					ni.removeHooks(ni.e.DISCONNECT);
					reject("Unable to connect");
				});
			}
			catch(e){ reject(e); }
		});
		return promise;
	}
	
	// transform an array buffer into a packet
	// structure of a buffer is the following :
	// 1 byte : number of 4bytes header words (header are everything until the message excluded)
	// 2 bytes : source port (fport)
	// 2 bytes destination port (port)
	// 4 bytes : message length
	// .... : everything that is not yet specified in the header but might be added later
	// N bytes : message
	bytesToPacket(bytes){
		if(!(bytes instanceof Uint8Array)) bytes = new Uint8Array(bytes);
        return this._flaptools.decode(bytes, this.TEMPLATE_MESSAGE);

		let res = {};
		let index = 0;
		let headerLength = bytes[index++];
		res.fport = 256 * bytes[index++] + bytes[index++];
		res.port  = 256 * bytes[index++] + bytes[index++];
		
		let messageLength = 0;
		for(let i=0; i<4; i++) messageLength = messageLength * 256 + bytes[index++];
		index = 1+4*headerLength;
		res.message = bytes.slice(index, index+messageLength);
		
		return res;
	}
	
	// transform 
	packetToBytes(packet){
        return this._flaptools.encode(packet, this.TEMPLATE_MESSAGE);

		let headLength = 8; // 2*2 for ports +4 for message length
		if(!packet.message) packet.message = new Uint8Array();
		let messageLength = packet.message.length;
		let resultLength = messageLength + headLength + 1;
		let bytes = new Uint8Array(resultLength);
		let index = 0;
		
		// insert header length (counted as 32 bytes words)
		bytes[index++] = headLength / 4;
		
		// insert port
		if(!packet.port) packet.port = 0;
		if(!packet.fport) packet.fport = 0;
		bytes[index++] = Math.floor(packet.fport/256);
		bytes[index++] = packet.fport & 0xff;
		bytes[index++] = Math.floor(packet.port/256);
		bytes[index++] = packet.port & 0xff;
		
		// insert message length
		for(let i=3; i>=0; i--){
			bytes[index+i] = messageLength & 0xff;
			messageLength = messageLength/256 >> 0;
		}
		index += 4;
		
		// insert the message
		bytes.set(packet.message, index);
		return bytes;
	}
}


// initialize NestedInterface prototype
let p = NestedInterface_NestedInterface.prototype;
// set events
p.e = {
	CONNECT : "CONNECT",
	DISCONNECT : "DISCONNECT",
	RECEIVE : "RECEIVE",
	SEND : "SEND",
	SPAWN : "SPAWN", // event when a new sub-connexion is created (specific to server Interfaces)
};

// indicate whether the interface can work for several message exchanges or not
p.multiExchanges = true;

// set possible status
p.RUNNING = "UP";
p.UP = p.RUNNING;
p.STOPPED = "STOPPED";
p.UNKNOWN = "UNKNOWN";
p.NEW = "NEW";
		
// retrieve a flaptools instance
// let globvar = typeof global === "undefined" ? window : global;
// if(! globvar.Flaptools){
//     globvar.Flaptools = require("Flaptools")
// }
// if(typeof global !== "undefined" && global.Flaptools) p._flaptools = new global.Flaptools();
// else p._flaptools = new Flaptools();

p._flaptools = new Flaptools_Flaptools();


// initialize the signature dictionnary
let EE_NI_MSG = [
    {n:"version", t:"int", c:0},
    {n:"length", t:"int", c:1},
    {n:"port", t:"int", c:2, d : 0},
    {n:"fport", t:"int", c:3, d : 0},
    {n:"message", t:"bin", c:4, d : new Uint8Array() },
]
p.TEMPLATE_MESSAGE = {
    ee : EE_NI_MSG
};

// intialize the canal dictionnary
p.TEMPLATE_CANAL = {};
p.TEMPLATE_CANAL.ee = [
    {n:"type", t:"int", c:2},
    {n:"address", t:"ascii", c:3},
    {n:"key", t:"hex", c:4},
    {n:"canals", t:"arr", c:5, tpl : p.TEMPLATE_CANAL},
    {n:"receiver", t:"hex", c:6}
];



// CONCATENATED MODULE: ./src/NestedInterface.js/lib/NIHttp.js
/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

/** the NIHttpSpawner encapsulate a http server for the use of the NestedInterfaces
***/



class NIHttp_NIHttpSpawner extends NestedInterface_NestedInterface {
	// the constructor takes a server.io
	constructor(data={}){
		super();
		this.name = "NIHttpSpawner";
		this.address = data.address || null;
		this.serverPort = String(data.serverPort || "");
		this.serverRoute = String(data.serverRoute || "");
        this.withServer = data.hasOwnProperty("withServer") ? data.withServer : true;
		if(this.withServer){
            let core = this._createHttpServer(this.serverPort, this.serverRoute);
            this.core(core);
		}
		this.status(this.RUNNING);

		// constant initialization
		this.CANAL_HTTP = 1;
	}

	// implement the AbstractNestedInterface methods

	// method to start the connexion
	start(){
		NestedInterface_NestedInterface.prototype.start.call(this);
	}

	// method to stop the server
	stop(){
		if(this._core) this._core.listen.close();
		NestedInterface_NestedInterface.prototype.stop.call(this);
	}

	getCanals(){
		return this.withServer ? [{
			type : this.CANAL_HTTP,
			address : this._getFullAddress()
		}]
		: [];
	}

	// implement canSpawn method
	canSpawn(canal={}){
		if(canal.type != this.CANAL_HTTP) return false;
		if(!canal.address) return false;
		return true;
	}

	// implement the spawn method
	spawn(canal){
		if(!(canal && this.canSpawn(canal))) return Promise.reject("invalid canal for spawn");
		let spawnBuilder = function(){
			return new NIHttp_NIHttpRequest(canal.address);
		};
		return this._buildAndWaitInterface(spawnBuilder);
	}

	_getFullAddress(){
		if(!this.address) return null;
		let address = this.address;
		if(this.serverPort) address += ":"+this.serverPort;
		if(this.serverRoute && this.serverRoute[0]){
			if(this.serverRoute[0] !== "/") address += "/";
			address += this.serverRoute;
		}
		return address;
	}

	// create a spawn from an incoming message
	_createSpawn(packet, res){
		// create a function to send back data
		let resultMethod = function(packet){
            res.header("Access-Control-Allow-Origin", "*");
			let bytes = this.packetToBytes(packet);
			let ascii = this.bytesToAscii(bytes);
            res.end(ascii);
		};
		// create a simple interface to send back an answer
		let spawn = new NIHttp_NIHttpAnswer(resultMethod);
		// trigger the spawn event
		let triggerData = {
			spawn : spawn,
			message : packet.message,
			port : packet.port,
			fport : packet.fport,
			interface : spawn
		};
		// trigger a spawn then a receive event
        this.triggerSpawn(triggerData);
        this.triggerReceive(triggerData);
	}

	// create a http server
	_createHttpServer(port, route){
		if(!port) port = 80;
		if(!route) route = "/";
		if(!NIHttp_NIHttpSpawner.prototype.expressLib) NIHttp_NIHttpSpawner.prototype.expressLib = __webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'express'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()));
		let server = NIHttp_NIHttpSpawner.prototype.expressLib();
		if(!NIHttp_NIHttpSpawner.prototype.bodyParserLib) NIHttp_NIHttpSpawner.prototype.bodyParserLib = __webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'body-parser'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()));
		server.use (function(req, res, next) {
			var data='';
			req.on('data', function(chunk) {
			   data += chunk;
			});

			req.on('end', function() {
				req.body = data;
				next();
			});
		});
		// link the route the message parsing
		let spawner = this;
		let requestProcessor = function(req, res){
			let bytesStr = req.body || "";
			let bytes = spawner.asciiToBytes(bytesStr);
			let packet = spawner.bytesToPacket(bytes);
		    spawner._createSpawn(packet, res);
		};
		// link the route to the request processing
		server.post(route, requestProcessor);
		let listen = server.listen( port );

		let core =  {
			port : port,
			route : route,
			server : server,
			listen : listen
		}
		return core;
	}
}

class NIHttp_NIHttpAnswer extends NestedInterface_NestedInterface{
	constructor(resultMethod){
		super();
		this.name = "NIHttpSpawn";
		this.core(resultMethod.bind(this));
		this.status(this.RUNNING);
	}
	// implement the send method
	send(message, dp=0, fp=0){
		let id = Math.random();
		this.hassent = id;
		let packet = {
			message : message,
			port : dp,
			fport : fp
		};
		this._core(packet);
		NestedInterface_NestedInterface.prototype.send.call(this, message, dp, fp);
		this.stop();
	}
}

// indicate whether the interface can work for several message exchanges or not
NIHttp_NIHttpAnswer.prototype.multiExchanges = false;

class NIHttp_NIHttpRequest extends NestedInterface_NestedInterface{
	constructor(fullAddress){
		super();
		this.name = "NIHttpRequest";
		this.core({
			address : fullAddress
		});
        this._environment = typeof window !== 'undefined' ? "browser" : "nodejs";
		this.status(this.RUNNING);
	}

	// implement the send method
	send(message, dp=0, fp=0){
		let spawn = this;
		let data = {
			url : this._core.address,
			message : message,
			port : dp, // destination port
			fport : fp, // origin port
			callback : function(res){
				spawn.triggerReceive({
						name : spawn.e.RECEIVE,
						port : res.port,
						fport : res.fport,
						message : res.message,
						interface : spawn
					});
				spawn.stop();
			}
		}
		this.httpRequest(data);
		NestedInterface_NestedInterface.prototype.send.call(this, message, dp, fp);
	}

	// perform an http request
	httpRequest(data){
		if( this._environment === "browser") return this.sendHttpBrowser(data);
		else return this.sendHttpNodejs(data);
	}

	// method to send an http/https request with nodejs
	sendHttpNodejs(data){
		let spawn = this;
		if( !NIHttp_NIHttpRequest.prototype.request ) NIHttp_NIHttpRequest.prototype.request = __webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'request'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()));
		let bytes = this.packetToBytes(data);
		let bytesStr = this.bytesToAscii(bytes);
		let req = NIHttp_NIHttpRequest.prototype.request({
		  method : "POST",
		  headers: { 'Content-Type': 'application/octet-stream' },
		  body : bytesStr,
		  url : data.url,
		}, function(error, response, body){
			if(!response) return;
			// in case of success
			try{
				let bytes = spawn.asciiToBytes(body);
				let packet = spawn.bytesToPacket(bytes);
				return data.callback(packet);
			}
			catch(e){
				console.log(e);
			}
		});
	}

	// method to send an http/https request from a web browser
	sendHttpBrowser(data){
		let $this = this;
        let bytes = this.packetToBytes(data);
        let bytesStr = this.bytesToAscii(bytes);
		let http = new XMLHttpRequest();
        http.responseType = "text";
		http.open( "POST", data.url, true);
		http.onload = function() {//Call a function when the state changes.
			if(http.readyState == 4 && http.status == 200) {
                let bytes = $this.asciiToBytes(http.response);
                let packet = $this.bytesToPacket(bytes);
                if(data.callback) data.callback(packet)
			}
		}
        http.send(bytesStr);
	}
}



// CONCATENATED MODULE: ./src/NestedInterface.js/lib/NIMeta.js
/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

/** the NIMeta agregates several spawners, enable manual creation of spawns and add hooks on new spawns
***/



class NIMeta_NIMeta extends NestedInterface_NestedInterface {
	// the constructor
	constructor(){
		super();
		this.name = "NIMeta";
		this.spawners = [];
		this.spawns = [];
		this.core({
			spawners : this.spawners,
			spawns : this.spawns
		});
		this.status(this.RUNNING);
		this.startCron();
		
		// add a hook for spawn
		this.initializeSpawnHook();
	}
	
	// add a spawn manually to the list of spawns
	addSpawn(spawn, eventData={}, triggerEvent=true){
		if(this.spawns.includes(spawn)) return;
		this.linkHooks(spawn);
		this.spawns.push(spawn);
		eventData.spawn = spawn;
		if(triggerEvent) this.triggerSpawn(eventData);
	}
	
	// add a spawn manually to the list of spawns
	addSpawner(spawner){
		this.linkHooks(spawner);
		this.spawners.push(spawner);
	}
	
	// link the hooks of the spawn to those on the metaspawn
	linkHooks(ni){
		// if(this.spawners.includes(ni) || this.spawns.includes(ni)) return;
		// overwrite the trigger method of the NestedInterface
		let metaspawner = this;
		let oldTriggerMethod = ni.trigger;

		let newTriggerMethod = function(){
			if(metaspawner.status() === metaspawner.STOPPED) return;
			var args = Array.prototype.slice.call(arguments);
			// call the hooks of the nested interface
			oldTriggerMethod.call(ni, ...args);
			// call the hooks of the meta
			args[1] = metaspawner.hooks;
			oldTriggerMethod.call(ni, ...args);
		}
		ni.trigger = newTriggerMethod;
	}
	
	// cron task to remove dead spawns
	cron(){
		// check the spawns
		for(let i = 0; i in this.spawns; ){
			let spawn = this.spawns[i];
			let status = spawn.status();
			if( status !== this.RUNNING && status !== this.NEW){
				this.spawns.splice(i, 1);
			}
			else i++;
		}
	}
	
	startCron(){
		this.cronId = setInterval(this.cron.bind(this), 3000);
	}
	
	stopCron(){
		if(this.cronId === null) return;
		clearInterval(this.cronId);
		this.cronId = null;
	}
	
	// method to stop the server
	start(){
		this.startCron();
		NestedInterface_NestedInterface.prototype.start.call(this);
	}
	
	// overwrite the trigger method to call the trigger method of every spawn/spawner associated to the NIMeta
	trigger(){
		var args = Array.prototype.slice.call(arguments);
		for(let spawn of this.spawns){
			spawn.trigger(...args);
		}
		for(let spawner of this.spawners){
			spawner.trigger(...args);
		}
	}
	
	// method to stop the server
	stop(){
		this.stopCron();
		// NestedInterface.prototype.stop.call(this);
		for(let spawner of this.spawners) spawner.stop();
		for(let spawn of this.spawns) spawn.stop();
		this.spawners.length = 0;
		this.spawns.length = 0;
	}
	
	getCanals(recursion=2){
		let canals = [];
		for(let spawner of this.spawners){
			let spawnerCanals = spawner.getCanals(recursion);
			canals = canals.concat(spawnerCanals);
		} 
		return canals;
	}
	
	canSpawn(canal={}){
		for(let spawner of this.spawners){
			let res = spawner.canSpawn(canal);
			if(res) return true;
		}
		return false;
	}
	
	spawn(canal){
		// if canal is an array of canals, retrieve the first canal that can be spawned
		if(Array.isArray(canal)) canal = this.canSpawnAny(canal) || {};
		for(let spawner of this.spawners){
			if(spawner.canSpawn(canal)){
				let meta = this;
				return spawner.spawn(canal)
				.then(function(ni){
					//meta.linkHooks(ni);
                    if(ni) meta.addSpawn(ni, {}, false)
					return ni;
				});

			}
		}
		return Promise.reject("no spawner available to spawn the requested canal");
	}
	
	initializeSpawnHook(){
		let metaspawner = this;
		this.addHook(this.e.SPAWN, function(event){
			metaspawner.addSpawn(event.spawn, event, false)
		});
	}
}


// CONCATENATED MODULE: ./src/NestedInterface.js/lib/NISwans.js
/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

/** encapsulate swans clients SwanClientReceiver and SwanClientCaller as spawner
 * SwanClientReceiver create spawns dynamically when receiving messages from a swans server
 * SwanClientCaller create spawns upon request (one time messages sent to swans receiver)
 ***/



class NISwans_NISwansSpawner extends NestedInterface_NestedInterface {
    // the constructor takes a server.io
    constructor(data={}){
        super();
        this.name = "NISwansSpawner";

        // may provide a SwanClientReceiver instance to create spawn from swans server messages
        // and a spawner to make swans server connection and then swansReceiver requests
        let core = {
            receiver : data.receiver,
            spawner : data.spawner,
            swansServerCanals : data.swansServerCanals || []
        }
        this.core(core);
        this.initializeReceiver(core.receiver);

        // keep in memory key pairs generated for encryption
        this.transactionKeysList = [];

        this.status(this.STARTED);

        this._initConstants();
    }

    // implement the AbstractNestedInterface methods

    // method to start the connexion
    start(){
        NestedInterface_NestedInterface.prototype.start.call(this);
    }

    // method to stop the server
    stop(){
        if(this._core.receiver){
            this._core.receiver.setCallback(function(){});
            this._core.receiver.close();
        }
        NestedInterface_NestedInterface.prototype.stop.call(this);
    }

    // implement the getCanals method
    getCanals(recursion=2){
        if(!this._core.receiver) return [];
        let canal = {
            type: this.CANAL_SWANS,
            // canals: this._core.swansServerCanals,
        };

        if(this._core.receiver){
            canal.canals = this._core.serverCanals || this._core.swansServerCanals;
            canal.receiver = this._core.receiver.getReceiverId();
        }
        return [canal];
    }

    // implement canSpawn
    canSpawn(canal={}){
        // cannot spawn if no canal can be spawne to contact the swans server
        if(!Array.isArray(canal.canals) ) return false;
        if(!( this._core.spawner && this._core.spawner.canSpawnAny(canal.canals))) return false;
        if(canal.type != this.CANAL_SWANS) return false;
        return true;
    }

    // implement spawn method
    spawn(canal){
        if(!(canal && this.canSpawn(canal))) return Promise.reject("invalid canal for spawn");
        // build the swans server spawn and the swans caller
        let swanServerCanal = this._core.spawner.canSpawnAny(canal.canals)
        let swansSpawnPromise = this._core.spawner.spawn(swanServerCanal);
        let $this = this;
        return swansSpawnPromise.then(function(swansSpawn){


            let swansCaller = new SwanClientCaller({
                receiverId : canal.receiver,
                port : canal.port,
                swansServerInterface : swansSpawn
            })
            // build the transactionKeys that will be used for this swansCaller
            let transactionKeys = swansCaller.getTransactionKeys(canal.receiver, $this.transactionKeysList);
            if($this.transactionKeysList.indexOf(transactionKeys) === -1) $this.transactionKeysList.push(transactionKeys)
            swansCaller.transactionKeysList([transactionKeys]);

            let spawn = new NISwans_NISwansCallerSpawn({caller : swansCaller, spawner : $this});
            $this.triggerSpawn({
                spawn : spawn,
                interface : $this
            })
            return spawn
        })
            .catch(e=>console.log("err", e))
    }

    // make the receiver trigger spawns upon receving requests
    initializeReceiver(receiver){
        if(!receiver) return;
        let $this = this;
        var receiverCallback = function(bytes, event){
            let packet = $this.bytesToPacket(bytes);

            // create the promise that will be resolved when the answer is sent
            let resolvePromise;
            let answerPromise = new Promise(res=>resolvePromise = res);

            // create a custom send method for the new spawn
            let customSend = function(packet){
                let msg = $this.packetToBytes(packet);
                resolvePromise(msg);
            }
            // create a spawn and trigger the spawn event
            let spawn = new NISwans_NISwansAnswer({send : customSend});
            let triggerSpawnData = {
                spawn : spawn,
                // message : packet.message,
                port : packet.port,
                fport : packet.fport,
                interface : $this
            };
            $this.triggerSpawn(triggerSpawnData);

            let triggerReceiveData = {
                message : packet.message,
                port : packet.port,
                fport : packet.fport,
                interface : spawn
            };
            spawn.triggerReceive(triggerReceiveData);
            return answerPromise;
        }
        receiver.setCallback(receiverCallback);
    }

    _initConstants(){
        this.CANAL_SWANS = 5;
    }
}

// interface to send a one time answer after receiving a swan request
class NISwans_NISwansAnswer extends NestedInterface_NestedInterface{
    constructor(data={}){
        super();
        this.name = "NISwansAnswer";
        let core = {
            send : data.send.bind(this),
        }
        this.core(core);
        this.status(this.STARTED);
    }
    // implement the send method
    send(message, dp=0, fp=0){
        let packet = {
            message : message,
            port : dp,
            fport : fp
        };
        this._core.send(packet);
        NestedInterface_NestedInterface.prototype.send.call(this, message, dp, fp);
        this.stop();
    }
}

// enable messaging a swan receiver and listening to its answers
class NISwans_NISwansCallerSpawn extends NestedInterface_NestedInterface{
    constructor(data={}){
        super();
        this.name = "NISwansCallerSpawn";
        this.core({
            caller : data.caller, // swans caller
            spawner : data.spawner
        });
        this.status(this.RUNNING);
    }

    // implement the send method
    send(message, dp=0, fp=0){
        let packetBytes = this.packetToBytes({message:message, port:dp, fport:fp});
        let answerPromise = this._core.caller.sendRequest(packetBytes);
        let $this = this;
        let prot = NISwans_NISwansCallerSpawn.prototype;
        let res = answerPromise.then(function(transactionData){
            let packetObject = $this.bytesToPacket(transactionData.body.body);
            $this.triggerReceive(packetObject);
        })
            .catch(function(packet){
            let type = packet.type;
            let isError = type.startsWith("error");
            if(isError) $this.stop();
        })
        NestedInterface_NestedInterface.prototype.send.call(this, message, dp, fp);
        return res;
    }

    stop(){
        if(this._core.caller){
            this._core.caller.close();
        }
        super.stop(this);
    }
}


// CONCATENATED MODULE: ./src/NestedInterface.js/lib/NIWebrtc.js
/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

/** the NIWebrtcSpawner encapsulate protocols to build webrtc connections
 *  the NIWebrtcSpawner is both a nestedInterface(it can create webrtcSpawns) and a flock application(it listen to a port to receive transactions)
 *
 *
 ***/



class NIWebrtc_NIWebrtcSpawner extends NestedInterface_NestedInterface {
    // the constructor takes a server.io
    constructor(data={}){
        super();
        this.setIceServers(data.ice || [ {urls: "stun:stun.l.google.com:19302"} ]);
        // number of RTCPeerConnection to build in advance : this enables to perform ice candidates before the demand occurs
        this.preheat = data.preheat || 1; // TODO : change this to a better value for release
        this.preheatedTransactions = [];
        this.passIceTimeout = data.passIceTimeout || false; // if ice gathering times out and passIceTimeout === true, the transaction is accepted anyway
        this.signalingInterface = data.signalingInterface;
        this.spawner = data.spawner; // a spawner creater interface to initiate messaging with a remote node
        this.port = data.port || this.DEFAULT_SIGNALING_PORT;
        this.cronTime = data.cronTime || 20000;
        this.transactionTimeout = data.transactionTimeout || 120000;
        this.name = "NIWebrtcSpawner";
        let core = {};
        this.core(core);
        this.status(this.STARTED);
        this._transaction = {};
        this.preheatAll();
        this.intializeSignalingInterface();
        this.start();
    }

    // method to start the connexion
    start(){
        this.cronId = setInterval(this.cron.bind(this), this.cronTime);
        NestedInterface_NestedInterface.prototype.start.call(this);
    }

    // method to stop the server
    stop(){
        if(this.cronId !== null){
            clearInterval(this.cronId);
            this.cronId = null;
        }
        // stop every peer connections
        NestedInterface_NestedInterface.prototype.stop.call(this);
    }

    // cron task for the spawner
    cron(){
        let time = Date.now();
        // purge expired transactions
        for(let tid in this._transaction){
            let req = this._transaction[tid];
            if(time-req.startTime > this.transactionTimeout){
                this.removeTransaction(tid);
            }
        }
    }

    // process incoming messages
    processMessage(event){
        if(this.status() !== this.RUNNING) return;
        let msg = event.message;
        let answerInterface = event.interface;
        let destinationPort = event.fport || null;
        if(!this.test) this.test = Math.random();

        // convert the byte message into object data
        let data = this.signalToObject(msg);
        if(destinationPort === null) destinationPort = this.port;

        // if the transaction exists, just add remote data to it
        let transaction = this.getTransaction(data.tid);
        if(transaction){
            this.addTransactionRemoteData(transaction.tid, data);
        }
        // if not, create a new transaction add remote data, and send signaling answer
        else{
            let $this = this;
            this.getPreheatedTransaction({
                tid : data.tid,
                interface : answerInterface,
                signalingPort : destinationPort,
                remoteOffer : data.offer,
                remoteIceCandidates : data.iceCandidates
            }).then(function(transaction){
                $this.addTransaction(transaction);
                $this.sendTransactionSignal(transaction.tid);
            })
        }
    }

    // create a peer connection object
    _createPeerConnection(){
        let servers = {
            iceServers : this.getIceServers(),
        };
        // retrieve the RTCPeerConnection object
        if(!NIWebrtc_NIWebrtcSpawner.prototype.RTCPeerConnection){
            if(typeof RTCPeerConnection != "undefined") NIWebrtc_NIWebrtcSpawner.prototype.RTCPeerConnection = RTCPeerConnection;
            else{
                try{
                    let lib = __webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'wrtc'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()));
                    NIWebrtc_NIWebrtcSpawner.prototype.RTCPeerConnection = lib.RTCPeerConnection;
                }
                catch(e){ }
            }
        }
        if(! NIWebrtc_NIWebrtcSpawner.prototype.RTCPeerConnection) throw "No RTCPeerConnection class available";
        return new NIWebrtc_NIWebrtcSpawner.prototype.RTCPeerConnection(servers);
    }

    // get the ice servers
    getIceServers(){
        return this.iceServers;
    }

    setIceServers(ice){
        this.iceServers = ice;
    }

    canSpawn(canal={}){
        if(canal.type != this.CANAL_WEBRTC) return false;
        // the spawner must be able to establish a connection to a signaling interface
        if(!this.spawner) return false;
        let canalCanSpawn = this.spawner.canSpawnAny(canal.canals);
        return !!canalCanSpawn;
    }

    // implement the getCanals method
    getCanals(recursion=2){
        return [{
            type : this.CANAL_WEBRTC,
            // send the list of canals that can be used for signaling
            canals : this.signalingInterface && recursion ? this.signalingInterface.getCanals(recursion-1) : []
        }]
    }

    // send local data (ice candidates and offer) to the remote RTCPeerConnection
    sendTransactionSignal(tid){
        let transaction = this.getTransaction(tid);
        if(!transaction) return;

        // only send one offer
        let sendOffer = !transaction.offerSent;
        if(sendOffer) transaction.offerSent = true;

        // build the signalaing data to send
        let signaling = {
            version : 1,
            tid : transaction.tid,
            iceCandidates : transaction.newIceCandidates,

            testoffer : transaction.testoffer
        };
        if(sendOffer) signaling.offer = transaction.offer;
        transaction.newIceCandidates = [];
        try{
            let bytes = this.signalToBytes(signaling);
            let sendResult = transaction.interface.send(bytes, transaction.signalingPort, this.port);
            // if signaling is impossible due to signaling interface problems, reject the spawn tentative
            if(sendResult && sendResult.then){
                sendResult.catch(function(e){
                    transaction.onfailure("signaling interface error : " + JSON.stringify(e))
                })
            }
        }
        catch(e){
            console.log("signalingerror", signaling, e)
        }
    }

    // implement the spawn method
    spawn(canal){
        let signalingInterfacePromise = this.spawner.spawn(canal.canals);
        let $this = this;
        return signalingInterfacePromise.then(function(signalingInterface){
            // bind the interface to process the answer
            if(!signalingInterface.hasHook($this.e.RECEIVE, $this.port) ) signalingInterface.addHook($this.e.RECEIVE, $this.processMessage.bind($this), $this.port);

            let resolve, reject;
            let promResult = new Promise((res,rej)=>{resolve=res; reject=rej;});
            let initiateData = {
                interface : signalingInterface,
                signalingPort : canal.port,
                onsuccess : resolve,
                onfailure : reject
            }
            $this.getPreheatedTransaction(initiateData).then(function(transaction){
                if($this.status() === $this.STOPPED) return;
                $this.addTransaction(transaction);
                $this.sendTransactionSignal(transaction.tid);
            })

            return promResult;
        })
    }

    /*** retrieve a preheated transaction, add some unique data to it :
     tid : id of the ongoing transaction connection
     interface : nestedInterface through which signaling is performed
     signalingPort : port through which signaling message must be sent
     onsuccess : a callback with the spawn as argument
     onfailure : a callback with the error as argument
     ****/
    getPreheatedTransaction(data={}){
        // retrieve a preheated transaction, and rebuild the stock for next use
        let preheatPromise = data.remoteOffer ? this.preheatPeerConnection(data.remoteOffer, data.remoteIceCandidates, this.MAXIMUM_PREHEAT_TIME, false) : this.preheatedTransactions.shift();
        this.preheatAll();

        // add the personalized data and callbacks
        let $this = this;
        let t1 = Date.now();
        if(!NIWebrtc_NIWebrtcSpawner.prototype.cnt) NIWebrtc_NIWebrtcSpawner.prototype.cnt = 0;
        let cnt = NIWebrtc_NIWebrtcSpawner.prototype.cnt++;
        return preheatPromise.then(function(transaction){
            transaction.onsuccess = data.onsuccess || (()=>0);
            transaction.onfailure = data.onfailure || (()=>0);
            transaction.tid = data.tid || $this.createTid();
            transaction.interface = data.interface;
            transaction.signalingPort = data.signalingPort || $this.DEFAULT_SIGNALING_PORT;

            // when a receiveChannel is created, the webrtc connection is completed
            transaction.RTCPeerConnection.ondatachannel = function(event) {
                // stop the signaling interface :
                transaction.interface.stop();
                // fire the spawn when the channel is open
                let onOpenChannel = function(channel){
                    let spawn = new NIWebrtc_NIWebrtcSpawn(transaction.RTCPeerConnection, transaction.sendChannel, channel);
                    transaction.spawn = spawn;
                    $this.triggerSpawn({ spawn : spawn });
                    $this.removeTransaction(transaction);
                    transaction.onsuccess(spawn);
                }
                if(event.channel.readyState === "open"){
                    onOpenChannel(event.channel);
                }
                else{
                    event.channel.onopen = ()=>onOpenChannel(event.channel);
                }
            }
            return transaction;
        });
    }

    // transform a signal to bytes
    signalToBytes(data){
        if(!data.tid) data.tid = this.createTid();

        let bytes = this._flaptools.encode(data, this.TEMPLATE_SIGNAL);

        return bytes;
    }

    // transform a signal to object
    signalToObject(bytes){
        let res = this._flaptools.decode(bytes, this.TEMPLATE_SIGNAL);
        return res;
    }

    // add a transaction to the list of pending transactions
    addTransaction(req){
        if(!req.tid) req.tid = this.createTid();
        if(!req.startTime) req.startTime = Date.now();
        this._transaction[ req.tid ] = req;
    }

    // add an offer to a transaction
    addTransactionOffer(tr, offer, tst){
        let transaction = typeof tr === "object" ? tr : this.getTransaction(tr);
        if(!transaction) return;
        if(tst) transaction.testoffer = "testoffer : "+offer;
        transaction.offer = offer || "";
        transaction.RTCPeerConnection.setLocalDescription(offer);
    }

    addTransactionRemoteData(tid, data){
        let transaction = this.getTransaction(tid);
        if(!transaction) return;
        this.addTransactionRemoteCandidates(tid, data.iceCandidates || []);
        let sendSignalAnswer = Promise.resolve();
        let $this = this;
        if(data.offer){
            this.addTransactionRemoteOffer(tid, data.offer);
            // if no offer data has already been sent create an offer answer, and send it
            if(!transaction.offerSent){
                transaction.RTCPeerConnection.createAnswer().then(function(offerAnswer){
                    $this.addTransactionOffer(tid, offerAnswer);
                    $this.sendTransactionSignal(tid);
                });
            }
        }
    }

    // add ice candidates to an offer
    addTransactionRemoteCandidates(tr, candidates){
        let transaction = typeof tr === "object" ? tr : this.getTransaction(tr);
        if(!transaction) return;
        if(!Array.isArray(candidates)) candidates = [candidates];
        // TODO : implement check to not send ice candidates if connection is already happening + filter only states that allo to add ice candidate in the following line
        if(transaction.RTCPeerConnection.connectionState === "connecting") return;
        for(let candidate of candidates){
            transaction.RTCPeerConnection.addIceCandidate(candidate).catch(e => {
                // TODO uncomment the line later
                // console.log("Failure during addIceCandidate(): ", e, transaction.RTCPeerConnection.connectionState);
            });
        }
    }

    // add the remoteDecription of a transaction
    addTransactionRemoteOffer(tr, offer){
        let transaction = typeof tr === "object" ? tr : this.getTransaction(tr);
        if(!transaction) return;
        transaction.remoteOffer = offer;

        return transaction.RTCPeerConnection.setRemoteDescription(offer);
    }

    // remove a transaction from the list of pending transactions
    removeTransaction(tr){
        let transaction = typeof tr === "object" ? tr : this.getTransaction(tr);
        if(!transaction) return;
        if(this._transaction[transaction.tid]) delete this._transaction[transaction.tid];
    }

    // get the data of a pending transaction
    getTransaction(id){
        return this._transaction[id] || null;
    }

    createTid(){
        return this._flaptools.randomHex(this.TRANSACTION_ID_LENGTH);
    }

    preheatAll(){
        while(this.preheatedTransactions.length < this.preheat){
            this.preheatedTransactions.push( this.preheatPeerConnection() );
        }
    }

    // create an rtc peer connection with ice candidates and an offer already created
    // create an offer or an answer, depending if a remote offer is provided

    // if the preheating times out, try with a superior timeout time
    preheatPeerConnection(remoteOffer, iceCandidates, timeout=this.MAXIMUM_PREHEAT_TIME, icePreheating=true){
        let peerConnection = this._createPeerConnection();
        let transaction = {
            RTCPeerConnection : peerConnection,
            sendChannel : peerConnection.createDataChannel('sc1'),
            newIceCandidates : [],
        };

        // retrieve ice candidates : resolve once it is done
        let candidateResolve;
        let candidatePromise = new Promise(r=>candidateResolve=r);
        peerConnection.onconnectionstatechange = function(){ }
        if(icePreheating){
            peerConnection.onicecandidate = function(e){
                if(e.candidate) transaction.newIceCandidates.push(e.candidate);
                else candidateResolve();
            }
            peerConnection.onicegatheringstatechange = function() {
                if(peerConnection.iceGatheringState === "complete") candidateResolve();
            }
        }
        else{
            candidateResolve();
        }

        // add remote offer if provided and create an offer : resolve once it is done
        let addRemotePromise = remoteOffer ? this.addTransactionRemoteOffer(transaction, remoteOffer) : Promise.resolve();

        let $this = this;
        let offerPromise = addRemotePromise.then(function(){
            return (!!remoteOffer ? peerConnection.createAnswer() : peerConnection.createOffer() )

                .then(offer=> {
                        $this.addTransactionOffer(transaction, offer, true);
                        if(iceCandidates) $this.addTransactionRemoteCandidates(transaction, iceCandidates);
                    },
                    function(e){console.log("webrtc preheat error : ", e)}
                );
        });
        // resolve automatically after a timeout
        let isTimeout = false;
        let timeoutPromise = new Promise(r=>setTimeout(()=>{isTimeout=true; r(false);}, timeout));
        NIWebrtc_NIWebrtcSpawner.prototype.spawner = (NIWebrtc_NIWebrtcSpawner.prototype.spawner||0)+1;
        let promres = offerPromise.then( ()=> Promise.race([candidatePromise, timeoutPromise]) )
            .then(function(isCorrect){
                // if timeout occured and peerConnection needs a complete ice gathering, try again with a new peerConnection
                if(isTimeout && !$this.passIceTimeout){
                    peerConnection.close();
                    return $this.preheatPeerConnection(remoteOffer, iceCandidates, timeout*2);
                }
                else{
                    return transaction;
                }
            })
            .catch(
                function(){console.log("CATCHHHHHHHHHHHHHHHHHHH")});
        return promres;
    }

    // initialize hooks on the signalingInterface
    intializeSignalingInterface(){
        if(!this.signalingInterface) return;
        let $this = this;
        this.signalingInterface.addHook(this.e.SPAWN, function(event){
            let spawn = event.spawn;
            spawn.addHook($this.e.RECEIVE, $this.processMessage.bind($this), $this.port);
        })
    }
}


// initialize some constants in the prototype
var pr = NIWebrtc_NIWebrtcSpawner.prototype;
pr.VERSION_LENGTH = 1;
pr.TRANSACTION_ID_LENGTH = 6;
pr.OFFER_LENGTH_LENGTH = 2;
pr.CANDIDATE_NUMBER_LENGTH = 1;
pr.CANDIDATE_LENGTH_LENGTH = 1;
pr.OPERATION_INITIATE_CONNECTION = 0;
pr.OPERATION_ERROR = 1;
pr.CANAL_WEBRTC = 3;
pr.DEFAULT_SIGNALING_PORT = 516;
pr.MAXIMUM_PREHEAT_TIME = 500;
pr.PRELOADED_ICE_CANDIDATES = 100;

// initialize EE for offer

// initialize the EE for communicating with the server
var signal_ee = [
    { n : "version", t : "int", c : 0, d : 0},
    { n : "length", t : "int", c : 1},
    { n : "tid", t : "hex", c : 3},
    { n : "offer", t : "json", c : 4},
    { n : "iceCandidates", t : "arr", c : 5, tpl : { n : "iceCandidate", t : "json", c : 0} }
]
pr.TEMPLATE_SIGNAL = {
    ee : {}
};
signal_ee.forEach(ee=>pr.TEMPLATE_SIGNAL.ee[ee.n] = ee);



class NIWebrtc_NIWebrtcSpawn extends NestedInterface_NestedInterface{
    constructor(peerConnection, sendChannel, receiveChannel){
        super();
        this.name = "NIWebrtcSpawn";
        this.core({
            peerConnection : peerConnection,
            sendChannel : sendChannel,
            receiveChannel : receiveChannel
        });
        this.messages = {};
        // create message id incrementally
        this._mid = 1;

        this.initializeChannels();
        this.start();
    }

    // extends the start method
    start(){
        super.start();
        this.startCron();
    }

    // start the cron method
    startCron(){
        this.cronId = setInterval(this.cron.bind(this), this.cronInterval);
    }

    // stop the cron method
    stopCron(){
        if(typeof this.cronId !== "number") return;
        clearInterval(this.cronId);
        this.cronId = null;
    }

    // cron method
    cron(){
        let now = Date.now();

        // remove messages that timed out
        for(let mid in this.messages){
            let message = this.messages[mid];
            if(message.time + this.expireLimit > now) continue;
            else delete this.messages[mid];
        }
    }

    // implement the send method
    send(message, dp=0, fp=0){
        //if(this.status() === this.STOPPED) return;
        let packet = {
            message : message,
            port : dp,
            fport : fp,
        }
        let bytes = this.packetToBytes(packet);

        // create a new id for the message
        let mid = this._mid++;
        // chunk id MUST start at 0
        let cid = 0;
        // send the chunk one by one
        let index = 0;

        while(index<bytes.length){
            let lastIndex = index + this.maxChunkSize;
            let isLastChunk = lastIndex >= bytes.length;
            let chunkData = {
                mid : mid,
                cid : cid++,
                chunk : bytes.slice(index, lastIndex),
                isLast : isLastChunk,
            }
            this.sendChunk(chunkData);
            if(isLastChunk) break;
            index = lastIndex;
        }
        NestedInterface_NestedInterface.prototype.send.call(this, message, dp, fp);
    }

    // send a chunk of message
    sendChunk(chunkData){
        let bytes = this._flaptools.encode(chunkData, this.TEMPLATE_CHUNK);

        if(this._core.sendChannel.readyState !== "open"){
            throw "the webrtc canal is not open. state : " + this._core.sendChannel.readyState;
        }
        this._core.sendChannel.send( bytes );
    }

    // process a chunk of data : concatenate them into a packet to be sent to NestedInterface onReceive hook
    processChunk(chunkData){

        let mid = chunkData.mid;
        if(!this.messages[mid]){
            this.messages[mid] = {
                chunks: {},
                time : Date.now(),
                waitingChunk : 0,
                lastChunk : +Infinity,
            }
        }
        let msg = this.messages[mid];
        let cid = chunkData.cid;
        msg.chunks[cid] = chunkData.chunk;
        if(chunkData.isLast) msg.lastChunk = cid;
        while(msg.waitingChunk in msg.chunks) msg.waitingChunk++;
        if(msg.waitingChunk < msg.lastChunk) return;

        // since all the chunks are gathered, agregate them
        let chunks = [];
        for(let i=0; i<=msg.lastChunk; i++) chunks.push(msg.chunks[i]);
        let packetBytes = this._flaptools.agregateBytes(chunks);
        delete this.messages[mid];
        let packet = this.bytesToPacket(packetBytes);
        packet.interface = this;
        this.triggerReceive(packet);
    }

    // implementation of stop method
    stop(){
        this._core.peerConnection.close();
        this._core.sendChannel.close();
        this._core.receiveChannel.close();
        NestedInterface_NestedInterface.prototype.stop.call(this);
    }

    // getters for the channels
    getSendChannel(){
        return this._core.sendChannel;
    }
    getReceiveChannel(){
        return this._core.receiveChannel;
    }
    getPeerConnetion(){
        return this._core.peerConnection;
    }

    // initialize channels events
    initializeChannels(){
        let receiveChannel = this.getReceiveChannel();
        let peerConnection = this.getPeerConnetion();
        let $this = this;
        receiveChannel.onmessage = function(event){
            if($this.status() === $this.STOPPED) return;
            let blob = event.data; // new Uint8Array(event.data);
            $this.blobToUint8Array(blob).then(function(bytes){
                try{
                    let chunkData = $this._flaptools.decode(bytes, $this.TEMPLATE_CHUNK);
                    $this.processChunk(chunkData);
                }
                catch(e){
                    console.log("process errorr :: ", e)
                }
                return;
                let packet = $this.bytesToPacket(bytes);
                packet.interface = $this;
                $this.triggerReceive(packet);
            })
        }

        peerConnection.oniceconnectionstatechange = function() {
            if(peerConnection.iceConnectionState == 'disconnected') {
                $this.stop()
            }
        }

        receiveChannel.onclose = function(){
            $this.stop()
        }
    }

    // transform a blob into a Uint8Array : return a promise
    blobToUint8Array(blob){
        // if it is an arrayBuffer
        if(blob instanceof ArrayBuffer){
            return Promise.resolve(new Uint8Array(blob));
        }

        // for browser environment
        if(typeof FileReader != "undefined"){
            let resolve;
            let prom = new Promise(r=>resolve=r);
            var fileReader = new FileReader();
            fileReader.onload = function(event) {
                let bytes = new Uint8Array(event.target.result);
                resolve(bytes);
            }
            fileReader.readAsArrayBuffer(blob);
            return prom;
        }

        if(typeof Buffer != "undefined") {
            var buff = new Buffer( blob, 'binary' );
            var bytes = new Uint8Array(buff);
            return Promise.resolve(bytes);
        }

        throw "No way to extract Blob data : fileReader and Buffer undefined";
    }
}
var pr = NIWebrtc_NIWebrtcSpawn.prototype;
pr.cronInterval = 40000;
pr.expireLimit = 40000; // time after which a pending message will not be processed
pr.maxChunkSize = 15900; // when sending messages, set the maximum chunk to be sent (compatibility firefox to chrome)

// initialize the EE for the message chunks
var message_ee = [
    { n : "version", t : "int", c : 0, d : 0},
    { n : "mid", t : "int", c : 3, d : 0}, // message id
    { n : "cid", t : "int", c : 4, d : 0}, // chunk id
    { n : "chunk", t : "bin", c : 5, d : new Uint8Array()},
    { n : "isLast", t : "bool", c : 6, d : false},
]
pr.TEMPLATE_CHUNK = {
    ee : {}
};
message_ee.forEach(ee=>pr.TEMPLATE_CHUNK.ee[ee.n] = ee);



// CONCATENATED MODULE: ./src/NestedInterface.js/lib/NIWebsocket.js
/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

/** the NestedSocketIoServerInterface encapsulate a socket.io server for the use of the NestedInterfaces
***/



class NIWebsocket_NIWebsocketSpawner extends NestedInterface_NestedInterface {
	// the constructor takes a server.io
	constructor(data={}){
		super();
		this.name = "NIWebsocketSpawner";
		this.socketPort = data.socketPort || 8080;
		this.address = data.address || null;
		// create the socket.io server
        this.withServer = data.hasOwnProperty("withServer") ? data.withServer : true;
		if(this.address && this.withServer){
            if(!NIWebsocket_NIWebsocketSpawner.prototype.websocketLib){
                NIWebsocket_NIWebsocketSpawner.prototype.websocketLib = __webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'ws'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()));
                NIWebsocket_NIWebsocketSpawn.prototype.websocketLib = NIWebsocket_NIWebsocketSpawner.prototype.websocketLib;
            }
            let socketIoServer = new NIWebsocket_NIWebsocketSpawner.prototype.websocketLib.Server({ port : this.socketPort });
            this.core(socketIoServer);
            this.initializeSocketIoServer(socketIoServer);
		}
		// start the spawner officially
		this.status(this.RUNNING);
		this.initializeWebsocketNIConstants();
	}
	
	// implement the AbstractNestedInterface methods
	
	// method to start the connexion
	start(){
		NestedInterface_NestedInterface.prototype.start.call(this);
	}
	
	// method to stop the server
	stop(){
		if(this._core) this._core.close();
		NestedInterface_NestedInterface.prototype.stop.call(this);
	}
	
	// implement the getCanals method
	getCanals(){
		let address = null;
		if(!this.withServer) return [];
		if(this.address){
			address = this.address;
			// if(this.socketPort) address += ":"+this.socketPort;
		}
		return [{
				type : this.CANAL_WEBSOCKET,
				address : address
			}];
	}
	
	// implement canSpawn
	canSpawn(canal={}){
		if(canal.type != this.CANAL_WEBSOCKET) return false;
		if(!canal.address) return false;
		return true;
	}
	
	// implement spawn method
	spawn(canal){
		if(!(canal && this.canSpawn(canal))) return Promise.reject("invalid canal for spawn");
		let spawnBuilder = function(){
			return new NIWebsocket_NIWebsocketSpawn({serverAddress : canal.address});
		};
		return this._buildAndWaitInterface(spawnBuilder);
	}
	
	// method to be launched when a new socket is opened : it should create a new spawn
	_onSpawn(newSocket, connectionData){
		// transform the socket into a nested interface
		let spawn = new NIWebsocket_NIWebsocketSpawn({socket : newSocket});
		// trigger the spawn event
		if(!connectionData) connectionData = {};
		connectionData.spawn = spawn;
		this.triggerSpawn(connectionData);
		// change the status of the spawn to connected
		spawn.status(spawn.RUNNING);
	}
	
	// initialize the socketIoServer, bind the events
	initializeSocketIoServer(socketIoServer){
		if(!socketIoServer) return;
		var spawner = this;
		socketIoServer.on('connection', function (newSpawnSocket) {
			try{
				spawner._onSpawn(newSpawnSocket);
			}
			catch(e){
				console.log(e);
			}
		});
	}
	
	initializeWebsocketNIConstants(){
		this.CANAL_WEBSOCKET = 2;
	}
}

class NIWebsocket_NIWebsocketSpawn extends NestedInterface_NestedInterface{
	constructor(data={}){
		super();
		this.name = "NIWebsocketSpawn";
		this.status(this.STARTED);
		
		let socket = null;
		let spawnMessageBytesStr = null;
		if(data.message){
			let packet = {
				message : data.message,
				port : data.port,
				fport : data.fport
			}
			let spawnMessageBytes = this.packetToBytes(packet);
			spawnMessageBytesStr = this.bytesToAscii(spawnMessageBytes);
		}
		this.serverAddress = data.serverAddress;

		// case 1 : a socket is already provided
		if(data.socket) socket = data.socket;
		// case 2 : a socket is built on a nodejs env
		else if(this.isNode()){
			let socketQuery = spawnMessageBytesStr ? {query : spawnMessageBytesStr} : {};
			if(!NIWebsocket_NIWebsocketSpawn.prototype.websocketLib) NIWebsocket_NIWebsocketSpawn.prototype.websocketLib = __webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'ws'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()));
			socket = new NIWebsocket_NIWebsocketSpawn.prototype.websocketLib(data.serverAddress);
		}
		// case 3 : a socket will be built on webbrower
		else{
			socket = new WebSocket(data.serverAddress);
		}
		this.core(socket);
		
		this.status(this.NEW);
		this.initializeSocketIo();
	}
	
	// there is no real way to restart a socket connection
	start(){
		NestedInterface_NestedInterface.prototype.start.call(this);
	}
	
	// implementation of stop method
	stop(){
		this._core.close();
		NestedInterface_NestedInterface.prototype.stop.call(this);
	}
	
	// implement the send method
	send(body, dp=0, fp=0){
		let packet = {
			message : body,
			port : dp,
			fport : fp,
		};
		let bytes = this.packetToBytes(packet);
		let bytesStr = this.bytesToAscii(bytes);
		try{
			this._core.send(bytesStr);
		}
		catch(e){
			console.log(e, this.manstopped);
		}

		NestedInterface_NestedInterface.prototype.send.call(this, body, dp, fp);
	}	
	
	initializeSocketIo(){
		if(!this._core) return; 
		var spawn = this;
		
		// when receiving message, trigger the RECEIVE event 
		let onmessage = function (data) {
			let port = null;
			let bytesStr = typeof data === "string" ? data : data.data;
			let bytes = spawn.asciiToBytes(bytesStr);
			let packet = spawn.bytesToPacket(bytes);
			packet.interface = spawn;
			if(!spawn.test) spawn.test = Math.random()
			spawn.triggerReceive(packet);
		}
		let onclose = function () {
			// trigger the STOP event
			spawn.triggerDisconnect();
			spawn.status(spawn.STOPPED);
		}
		let onerror = function(e){
			if(e.code === "ECONNREFUSED"){
				this.status(this.STOPPED);
				console.log("stopped socket !");
			}
		};
		let onopen = function(){
			spawn.triggerConnect();
			spawn.status(spawn.RUNNING);
		}

		this._core.onmessage = onmessage;
		this._core.onclose = onclose;
		this._core.onerror = onerror;
		this._core.onopen = onopen;
	}

}


// CONCATENATED MODULE: ./src/flock/lib/flock.js
/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/


class Flock {
	constructor(data={}){

        this.metaInterface = data.metaInterface || new NIMeta();

		this.flockInit();
		this._flaptools = data.flaptools || new Flaptools();
		// build a thredops
		this._threadops = this._flaptools.threadops({
			maxThreads : data.maxThreads || 100,
			maxConcurrentThreads : data.maxConcurrentThreads || 1,
		});
		
		this.port = data.port || 1;

		this.keys = data.keys ? data.keys.slice() : [this.generateKey(32)];
		this.withKeyFormations = !!data.withKeyFormations;
		this.formationDensity = 1;
		this.formations = {}; // indexed by formationId = key + "/" + span
		this._formationOperations = {};
		this.id = "id" in data ? data.id : this.generateId();
		this.metaInterface = data.metaInterface || new NIMeta();

		this.nodeExpireTime = data.nodeExpireTime || 10000;
        this.cronIntervalId = null;
		this._cronInterval = data.cronInterval || 1000;
		this.density = data.density || 3; // number of active node per bucket
		this.iddleDensity = data.iddleDensity || 100; // number of inactive nodes (that are kept just as information)
		this.buckets = {};
		this.nodes = {};
		this._nid = 1; // connections id for internal usage only
		this.canals = [];
		this.maxInterfacedNodes = data.maxInterfacedNodes || 256;

		// add nodes if provided
		if(data.nodes){
			for(let node of data.nodes){
				if(!node.id){
				    // if no id is provided, requests the node before adding it to the list of available nodes
				    this.getNodeInfo(node);
                }
                else{
                    this.addNode(node);
                }

			}
		}

		// bind methods
        if(!this._keyToBinary_bound) this._keyToBinary_bound = this._keyToBinary.bind(this);

		// ongoing operations contains the following properties :
		// type : the name of operation (getNode, getInfo)
		// iterations : number of iterations left before generating an error (for instance, when a getNode operation requires to many calls to adjacent nodes)
		// called : list of nodes already called during the operation
		// threads : number of parallel requests running to perform a task
		// freeThreads : number of threads left for the task
		// onsuccess : success callback
		// onerror : error callback
		// startTime : beginning of the request
		// timeout : number of ms after which the request expire
		// data : data relative to the request
		
		// list of ongoing requests (getNodes, getInfo....) : ensure that newly generated requestId doesn't match those of registered requests
		this.requests = {};
		// list of ongoing operations (reachNode, populate...)
		this.operations = {};
		// list of formations to keep populated
		this._formations = [];
		
		this.start();
		this.initializeInterface(this.metaInterface);
	}
	
	////////////////////////////////////////////////////////////////////
	///////////// getters and setters //////////////////////////////////
	////////////////////////////////////////////////////////////////////
	
	getKeys(){ 
		return this.keys.map(a=>{return {prefix : a.prefix, span : a.span} });
	}

	addKey(key){
		if(typeof key !== "object") key = {prefix : key, span : 0}
		if(!this.hasKey(key)) this.keys.push(key);
	}

	removeKey(key){
        let prefix = typeof key === "object" ? key.prefix : key;
        this.keys = this.keys.filter(k=>k.prefix!== prefix);
	}

	// check if has the given key
	hasKey(key){
		let prefix = typeof key === "object" ? key.prefix : key;
		for(let k of this.keys){
			if(k.prefix == prefix) return true;
		}
		return false;
	}

	getId(){ 
		return this.id;
	}
	
	////////////////////////////////////////////////////////////////////
	//////////// flock management methods //////////////////////////////
	////////////////////////////////////////////////////////////////////
	
	start(){
		let flock = this;

		this.restartCron();
	}
	
	stop(){
		// stop the cron
		this.stopCron();
		
		// close all connections
		if(this.metaInterface) this.metaInterface.stop();
		for(let i in this.connections){
			this.connections[i].connetion.interface.close();
		}
		this.buckets = {};
		
		this.nodes = {}; // nodeId => node

		// stop all the pending operations
        this._threadops.rejectOperations();
	}

	cronInterval(newCronInterval=null){
		if(newCronInterval){
			this._cronInterval = newCronInterval;
			if(this.cronIntervalId !== null) this.restartCron();
		}
		return this._cronInterval;
	}

	restartCron(){
		this.stopCron();
        let cronMethod = this.cron.bind(this);
        this.cronIntervalId = setInterval(cronMethod, this._cronInterval);
	}

	stopCron(){
        if(this.cronIntervalId !== null && this.cronIntervalId !== undefined) clearInterval(this.cronIntervalId);
        this.cronIntervalId = null;
	}
	
	cron(){
		// remove expired nodes
		for(let id in this.nodes){
			let now = Date.now();
			let node = this.nodes[id];
			if(now - node.lastUpdate < this.nodeExpireTime) continue;
			else if(node.interface && node.interface.status() === "UP"){
                // only get node info for connections that are multi messages
				if(node.interface && node.interface.multiExchanges) this.getNodeInfo(node);
			}
			else{
				let n = this.nodes[id];
				delete this.nodes[id];
            }
		}
		// create formations for keys
		if(this.withKeyFormations){
			let keys = this.getKeys();
			for(let key of keys){
				let keyId = this.keyId(key);
				let byteKey = this._flaptools.hexToBytes(key.prefix);
				if(!this.formations[keyId]) this.formations[keyId] = new Formation(byteKey, { beforeKey : this.formationDensity, afterKey : 0 });
			}
		}

		// create operations for the formation
		for(let formationId in this.formations){
			let currentOp = this._formationOperations[formationId];
			if(currentOp && currentOp.done) delete this._formationOperations[formationId];
			if(!this._formationOperations[formationId] ){
				let operationData = {
					formation : this.formations[formationId],
				};
                this.populateFormation(operationData);
				this._formationOperations[formationId] = operationData;
			}
		}

		// display some stats in the logs
		let nodesNumber = this.getNodes().length;
		let connectedNodesNumber = this.getConnectedNodes().length;
        if(typeof document !== "undefined") document.connectedNodes = this.getConnectedNodes();
        if(typeof document !== "undefined") document.flock = this;
	}
	
	initializeInterface(ni){
        ni.addHook(ni.e.RECEIVE, this.processRequests.bind(this), this.port );
        ni.addHook(ni.e.SPAWN, this.addSpawnedNodes.bind(this));
	}

	// add new spawned interface to the list of nodes
	addSpawnedNodes(event){
		let $this = this;
		// execute in timeout to let time to other hooks to add it as a node
		setTimeout(function(){
		    if(!$this.canOpenConnection()){
		        console.log("addSpawnedNodes : Cannot create new connection, close spawn");
		        event.spawn.stop();
		        return;
            }
			// check whether or not there is already a node using that interface
			let interfaceNode = $this.nodeFromInterface(event.spawn);
		    // don't add node when interface is only for a single exchange of data
		    if(event.spawn.multiExchanges){
                $this.getNodeInfo(interfaceNode);
                $this.addNode(interfaceNode);
			}

		}, 1)
	}
	
	////////////////////////////////////////////////////////////////////
	//////////// connections related methods ///////////////////////////
	////////////////////////////////////////////////////////////////////

	nodeCheckup(){
		for(let nodeId in this.nodes){
			let node = this.nodes[nodeId];
			if(!node.interface) continue;
			let status = node.interface.status();
			if(status !== "UP" && status !== "NEW") this.removeNode(node);
		}
	}

	// initiate connections with foreign nodes matching a Formation object
	populateFormation(op={}){
		if(!(op.formation && op.formation instanceof Formation)) throw "populateFormation error : expect a Formation as the formation to populate in the options";
		op.thread = this._populateFormationThread.bind(this);
		op.name = "populateFormation for formation '"+op.formation.key().join(",")+"'";
		// keep the list of the buckets that are beeing investigated by a thread
		op.processedBuckets = {};
		// for each bucket, keep the list of visited node for reachnode operation
		op.visitedNodes = {};
		// keep the number of pending threads for a given bucket
		op.pendingThreads = {};
		op.connected = {}
		// keep a track of nodes (per bucket) that have been passed to the onpopulate callback
		op.advertisedNodes = {}; // bucket=>{ nid=> nid }
		op.retroactiveCallback = true;
		if(!op.skipNodes) op.skipNodes = {};
		// skip nodes for the reach operation
		op.skipNodesReachOperation = Object.assign({}, op.skipNodes);
        // retrieve the connected nodes, without the skipped nodes
        let $this = this;
        op.connectedNodes = function(){
            let connectedNodes = $this.getConnectedNodes();
             // let res = connectedNodes.filter(n=>!op.skipNodes.includes(""+n.id));
            let res = connectedNodes.filter(n=>!op.skipNodes[n.id]);
            return res;
        }

        // check all available connected nodes : for each one of them, call the onpopulate once per request
        op.advertiseNewPopulationNodes = function(){
            if(!op.onpopulate) return;
            let connectedNodes = op.connectedNodes();
            // build binary keys of the connected nodes
            let binaryNodes = {};
            for(let node of connectedNodes) binaryNodes[node.id] = node.keys.map($this._keyToBinary_bound)
            let population = op.formation.mapKeysToBuckets(binaryNodes);

            for(let bucket in population){
                for(let nid of population[bucket]){
                    if(!op.advertisedNodes[bucket]) op.advertisedNodes[bucket] = {};
                    if(!(nid in op.advertisedNodes[bucket])){
                        op.advertisedNodes[bucket][nid] = nid;
                        op.onpopulate( $this.getNode(nid), bucket );
                    }
                }
            }
        }

		return this._threadops.startOperation(op);
	}

	//
	_populateFormationThread(op){
		let $this = this;
		// if(this.getKeys()[0].prefix !== "00") return Promise.resolve();
		// retrieve the list of buckets where there are missing nodes
		let connectedNodes = op.connectedNodes();
		let missingBuckets = this.getMissingBuckets(op.formation, connectedNodes);
		// determine a bucket to investigate
		let minV = +Infinity;
		let targetBucket = -1;
		let hasMissingBuckets = false;
		let testmsb;

        op.advertiseNewPopulationNodes();

		// retrieve the missing buckets and target one to request
		for(let i in missingBuckets){
            hasMissingBuckets = true;
            testmsb = i;
            if(!op.processedBuckets[i]) op.processedBuckets[i] = 0;
            if(!op.pendingThreads[i]) op.pendingThreads[i] = 0;
            // start no more that a thread per missing node per bucket, and target the bucket that have least been treated
			if(op.pendingThreads[i] < missingBuckets[i] && minV >= (op.processedBuckets[i]) ){
				minV = op.processedBuckets[i];
				targetBucket = i;
			}
		}
        // if there are no empty buckets, the operation is a success
        if(!hasMissingBuckets){
            this._threadops.resolveOperation(op.id);
            return Promise.resolve();
		}

		// if all the buckets have pending operations, don't start anything
		if(targetBucket === -1){
            let res = this._threadops.sleep(1000);
			return res;
		}

		// increment this bucket in order to process all buckets equally
		op.processedBuckets[targetBucket]++;
        op.pendingThreads[targetBucket]++;
		let decreasePendingThreads = function(e){
			op.pendingThreads[targetBucket]--;
		}

		// launch a reachNode request for the bucket
		let targetKey = this.inverseSuffix( this._flaptools.bytesToHex(op.formation.key()), targetBucket);
		let fl = this;
		if(!op.visitedNodes[targetBucket]) op.visitedNodes[targetBucket] = {};

		// build a callback to connect nodes once they are reached
		let onreachedCallback = function(node){
			// connect reached nodes
			// NB : promise resolution is checked when the rechNode operation is fully resolved (see callback below)
            //let connectionProm =
			fl.connectNode(node).then(function(){
				op.advertiseNewPopulationNodes();
            })
			.catch(function(){})
		}
		let prom = this.reachNode({
			maxNodes : missingBuckets[targetBucket],
			prefix : targetKey,
			timeout : op.timeout || 0,
			visitedNodes : op.visitedNodes[targetBucket],
            skipNodes : op.skipNodesReachOperation,
			onreached : onreachedCallback,
            withOwnNode : false,
			precision : parseInt(targetBucket),
		}).then(function(nodes){
            // after reaching the nodes, connect to them
            let connectProms = [];
            for(let node of nodes){
            	// prevent the populate operation from asking for the same node twice
                op.skipNodesReachOperation[node.id] = true;
                // it might not trigger a new connection if the node is already connected or connecting (which is the case since node connection are performed one by one)
                let connectProm = fl.connectNode(node);
				connectProms.push(connectProm);
            }
            let promall = Promise.all(connectProms);
            promall.then(function(){
                    op.connected[targetBucket] = op.connected[targetBucket] ? op.connected[targetBucket]+1 : 1;
                },
                function(e){
                    console.log("ENDCONNECT ERROR", e)
                })

            return promall;
        })
		// once thread is done or an error occured, allow the creation of a new thread for this bucket
		.catch(function(e){
			// console.log("errorrrrrrrrrrr ::::", e)
		}).then(decreasePendingThreads);

		return prom;
	}

	// try to reach a node : return a promise when it is done, options may/must specify (* for facultative) :
	// prefix : the key to reach and connect
	reachNode(op={}){
		if(!op.prefix) throw "reachNode error : expect an hexadecimal as the prefix to reach in the options";
		if(!op.precision) op.precision = op.prefix.length * 4;
		op.visitedNodes = op.visitedNodes || {};
		op.maxNodes = op.maxNodes || 1;
		op.name = "reachNode for prefix '"+op.prefix+"'";
		if(!op.skipNodes) op.skipNodes = {};
        op.resolveAtBest = this._reachAtBest.bind(this);
		op.thread = this._startReachNodeThread.bind(this);
		if(!op.hasOwnProperty("withOwnNode")) op.withOwnNode = true;
		// build the list of matching nodes that have already been processed by onreached
		op.reachedList = {};
		// build the target key from the prefix
		op.key = { prefix : op.prefix, span : op.span || 0};
		// save some stats as the operation progress
		if(!op.stats) op.stats = {};
		op.stats.bestBucket = 0;
		op.stats.hexKey = op.prefix;
		op.stats.byteKey = this._flaptools.hexToBytes(op.prefix);
		op.stats.formation = new Formation(op.stats.byteKey);
		return this._threadops.startOperation(op);
	}

	_reachAtBest(operation){
        let reachedNodes = this.reachNodeLocal(operation.key, false, operation.maxNodes, operation.skipNodes, operation.precision, operation.withOwnNode);
        this._threadops.resolveOperation(operation.id, reachedNodes);
    }

	// start a thread to reach a given node
	_startReachNodeThread( operation ){
        // check if there are enough local nodes to resolve the operation
        let flock = this;
        let tryResolve = function(r){
            // retrieve the list of nodes that match the target
            let reachedNodes = flock.reachNodeLocal(operation.key, false, operation.maxNodes, operation.skipNodes, operation.precision, operation.withOwnNode);
            // before resolving the reach operation, launch a callback for each new matching node found
            if(operation.onreached){
                for(let node of reachedNodes) {
                    let nid = node.id;
                    if (!operation.reachedList[nid]) {
                        operation.reachedList[nid] = true;
                        operation.onreached(node)
                    }
                }
			}
			// if enough nodes have been found, resolve the operation
            if(reachedNodes.length >= operation.maxNodes){
                try{
                    flock._threadops.resolveOperation(operation.id, reachedNodes);
				}
                catch(e){
            		console.log("reach resolve error", e)
				}
            }
        }
        tryResolve();
        if(operation.done) return Promise.resolve();

		// find the closest node to the target that have not been visited yet
		let nodes = [];
		let now = Date.now();
		for(let i in this.nodes){
			let node = this.nodes[i];
			// pick nodes that have not already been visited
			if(node.id && !operation.visitedNodes[node.id])  nodes.push(node);
		}
		let closestNodes = this.closestNodes(operation.key, 1, false, nodes, true);
		let closestNode = closestNodes.pop();
		if(!closestNode){
		    return this._threadops.sleep();
		}

		// update the closest bucket found
		let bestBucket = 0;
		for(let key of closestNode.keys){
            bestBucket = Math.max(bestBucket, operation.stats.formation.keyBucket(this._flaptools.hexToBytes(key.prefix || "") , 0) );
		}
        operation.stats.bestBucket = bestBucket;

        // contact the node to ask targeted key
		let options = {
			k : 4,
		}
		let resultPromise = this.getNodeNode(closestNode, operation.key.prefix, options).then(tryResolve,tryResolve);

		// mark the newly visited node as visited
		operation.visitedNodes[closestNode.id] = true;

		return resultPromise;
	}

	// retrieve local nodes that match a given node
	reachNodeLocal(key, nodes=false, maxResults=5, skipNodes = {}, precision=null, withOwnNode=true){
		if(!nodes) nodes = this.getNodes();
		if(!precision) precision = key.prefix.length*4;
        if(withOwnNode) nodes.push(this.getOwnNode());
		nodes = nodes.filter(node=>!(node.id.toString() in skipNodes));
        nodes = this.closestNodes(key, maxResults, false, nodes);
        while(nodes.length){
            let match = false;
            for(let nodeKey of nodes[nodes.length-1].keys){
                if(this.isIncluded(key, nodeKey, precision)){
                    match = true;
                    break;
                }
            }
            if(match) break;
            else nodes.pop();
        }
        return nodes;
	}

	removeNodes(){
		for(let nid in this.nodes) this.removeNode(nid);
	}
	
	removeNode(nid){
        if(this.nodes[nid]) delete this.nodes[nid];
	}

	// getter for a specific node
	getNode(nid){
		return this.nodes[""+nid] || null;
	}
	
	addNode(n){
		// duplicate the node
		// n = Object.assign({},n);
		// if(n.interface && !n.interface.multiExchanges){
		// 	delete n.interface;
		// }

		// a node must at least have an id and not be the flock itself
		if(!n.hasOwnProperty("id") || n.id === this.getId()){
			return;
        }
		
		//don't push already present nodes
		if(this.nodes[n.id]){
			return;
        }

		this.nodes[n.id] = n;
		
		// create an empty key if the node has none
		if(!n.keys) n.keys = [];

		// set the last time the node data has been updated
		n.lastUpdate = Date.now();
	}

	// check if a new connection can be open (or not if the limit is reached)
    canOpenConnection(limit=this.maxInterfacedNodes){
        let interfacedNodes = this.filterNodes(null, ["UP", "NEW"]);
        return interfacedNodes.length < limit;
    }
	
	// connect a given node n, trying to use one of the available canal
	// return a promise that resolves with the nestedInterface or reject with an error message
	connectNode(n){
		// if the node has already a connected interface, return it
		if(n.interface && n.interface.status() == "UP") return Promise.resolve(n.interface);
		// if there is a pending promise for the interface, return it
        if(n.interfacePromise) return n.interfacePromise;

		// retrieve a canal that can be spawned and create a new NestedInterface
		let canal = this.metaInterface.canSpawnAny(n.canals || []);

		// if there is no valid canal to create a connection return a promise reject
		if(!canal) return Promise.reject("no matching canal for connection ::: "+ JSON.stringify(n.keys) + " ::: " + JSON.stringify(n.canals) );

		// throw if there are too much interfaces created

		if(!this.canOpenConnection()) return Promise.reject("connectNode error : maximum interfaced nodes reached : "+this.maxInterfacedNodes);

		let prom = this.metaInterface.spawn(canal).then(
		function(spawn){
			n.interface = spawn;
			return spawn;
		});

		let $this = this;
		let delProm = (e)=>{ delete n.interfacePromise};

		prom = Promise.race([prom, new Promise((res,rej)=>setTimeout(rej,2000))]);
        // save the request in order to perform connection only once
        n.interfacePromise = prom.then(delProm).catch(
        	function(e){
        		// let a promise rejection as the result, since no connection could be established
				return Promise.reject("failed to connect to node : "+JSON.stringify(e) );
            });
		return prom;
	}
	
	// get the info from a node, and push it to the node
	// return a promise
	getNodeInfo(n){
		let prom = this.connectNode(n);
		let fl = this;
		let res = prom.then(function(spawn){
			let getInfoRequest = {
				requestId : fl._generateRequestId(),
				getKeys : true,
				getCanals : true,
				getId : true,
				precision : 4095,
			};
			let bytes = fl.getInfoToBytes(getInfoRequest);
			spawn.send(bytes, fl.port, fl.port);
			getInfoRequest.node = n;
			fl.addRequest(getInfoRequest);
			let promResult = new Promise(r=>getInfoRequest.resolve=r);
			return promResult;
		})
		.then(function(r){
			return r;
		});
		return res;
	}
	
	// send the getNode request on a given node
	// return a promise
	getNodeNode(node, targetPrefix, opt={}){
		let prom = this.connectNode(node);
		let fl = this;
		let res = prom.then(function(spawn){
			let getNodeRequest = {
				requestId : fl._generateRequestId(),
				prefix : targetPrefix,
				precision : opt.precision || 2,
				k : opt.k || 4,
			};
			let bytes = fl.getNodeToBytes(getNodeRequest);
			spawn.send(bytes, fl.port, fl.port);
			fl.addRequest(getNodeRequest);
			let promResult = new Promise(r=>getNodeRequest.resolve=r);
			return promResult;
		})

		return res;
	}
	
	closestNodes(key, k, withOwnNode=false, nodes=null , squawkerPriority=false){
        if(!nodes) nodes = this.getNodes();
		if(withOwnNode) nodes.push(this.getOwnNode());
		// only keep nodes with at least one key or nodes that are considered as squawkers
		nodes = nodes.filter(n=>!!n.keys.length || (squawkerPriority && n.squawker) );
		let nodeIndex = nodes.map((v,i)=>i);
		let fl = this;
		// take the closest key of each node : nodes that are squawkers are affected a constant distance
		let dist = nodes.map((v,i)=>squawkerPriority && v.squawker ? fl.SQUAWKER_DEFAULT_DISTANCE : fl.minDist(key, v.keys));
		// sort each node by their closest key
		nodeIndex.sort(function(i1,i2){
			return fl.isInf( dist[i1], dist[i2]) ? -1 : 1;
		});
		return nodeIndex.slice(0,k).map((i)=>nodes[i]);
	}

	// check whether the node from the interface exists
	nodeExists(nodeInterface){
        let node = this.getNodes().find(n=>n.interface === nodeInterface);
		return node;
	}

	// retrieve a node data object from an interface 
	// if no node data object exist, create and store one
	nodeFromInterface(nodeInterface, opts={}){
		let node = this.nodeExists(nodeInterface);
		if(!node){
			node = {
				interface : nodeInterface,
				id : this.generateId()
			};
		}
		return node;
	}
	
	// build the node representing itself
	getOwnNode(){
		let ownNode = {
			id : this.getId(),
			keys : this.getKeys(),
			canals : this.getCanals(),
		}
		ownNode.canals.push({type : this.CANAL_CURRENT_CONNECTION});
		return ownNode;
	}
	
	// retrieve all the nodes, in an array
	getNodes(){
		return Object.values(this.nodes);
	}
	
	getConnectedNodes(nodes=null){
		if(!nodes) nodes = this.getNodes();
		let connectedNodes = nodes.filter(n=>n.interface && n.interface.status() === "UP");
		return connectedNodes;
	}

	// filter nodes given a list of status
	filterNodes(nodes=null, status=[]){
		if(!nodes) nodes = this.getNodes();
        // aux method for the filterNodes method
        let filterMethod = function (node){
            // if no status is specified, accept all nodes
            if(!status.length) return true;
            if(status.includes("NEW") && node.interfacePromise) return true;
            if(!node.interface) return false;
            let nodeStatus = node.interface.status();
            // if a connection is beeing made
            return status.includes(nodeStatus)
        }
        let filtered = nodes.filter(filterMethod);
        return filtered;
	}

	// retrieve the buckets whith not enough nodes in it
	getMissingBuckets(formation, nodes=null){
		if(!nodes) nodes = this.getNodes();
		let keys = [].concat(...nodes.map(n=>n.keys||[]));
		let flaptools = this._flaptools;
		let cpm = formation.complement(keys.map(this._keyToBinary_bound));
		return cpm.filterBuckets(1, +Infinity, 0, cpm.key().length*8 + 32);
	}

	// transform a list of nodes to an array of nodes where keys are in binary array
	nodesToBinaryArray(nodes){

	}

	// transform a key obect in binary data for prefix
	_keyToBinary(key){
        return { prefix : this._flaptools.hexToBytes(key.prefix), span : key.span}
	}

	////////////////////////////////////////////////////////////////////
	/////////////// requests methods ///////////////////////////////////
	////////////////////////////////////////////CONNECTNODE ::::////////////////////////
	
	getInfo(){
		
	}
	
	startGetNodeThread(){
		
	}
	
	getNodeThreadCallback(){
		
	}
	
	startGetInfoThread(){
		
	}
	
	getInfoThreadCallback(){
		
	}
	
	populate(){
		
	}
	
	// getter and setters for managed formations
	getFormations(){
		return this._formations.slice();
	}
	addFormation(f){
		if(f instanceof Formation) this._formations.push(f);
	}
	removeFormation(f){
		let i = this._formations.indexOf(f);
		if(i === -1) return;
		this._formations.splice(i,1);
	}
	
	// add a pending request
	addRequest(req={}){
		if(!req.requestId) req.requestId = this._generateRequestId();
		this.requests[req.requestId] = req;
		if(!req.startTime) req.startTime = Date.now();
		return req.id;
	}
	
	getRequest(id){
		return this.requests[id] || null;
	}
	
	removeRequest(id){
		if(this.requests[id]) delete this.requests[id];
	}
	
	// add the nodes to the list of nodes
	_processGiveNode(answer){
		// check that the request id of the giveNode match a requestId of a getNode request
		let req = this.getRequest(answer.requestId);
		if(!req){
		    return;
        }
		let nodes = answer.nodes || [];
		for(let node of nodes){
			let keys = [];
			for(let keyData of node.keys||[]){
				let commonPrefix = req.prefix.substr(0, keyData.common*2) + keyData.prefix;
				let prefixLength = Math.min(req.prefix.length, commonPrefix.length);
				let prefix = commonPrefix.substr(0, prefixLength);
				let span = (keyData.span||0) + (commonPrefix.length - prefixLength );
				keys.push({ prefix : prefix, span : span, hiddenLength : keyData.hiddenLength||0} );
			}
			node.keys = keys;
			this.addNode(node);
		}
		if(req.resolve) req.resolve(nodes);
		this.removeRequest(answer.requestId);
	}
	
	_processGetNode(req){
		let res = {};
		res.version = 0;
		res.method = "giveNode";
		res.requestId = req.requestId;
		res.precision = req.precision || 0;
		let reqKey = { prefix : req.prefix, span : 0};

		let nodes = this.closestNodes(reqKey, 2*req.k, true); // take twice the number of requested nodes in case of invalid nodes
		let connectedNodes = this.getConnectedNodes(nodes);
		if(connectedNodes.length >= req.k) nodes = connectedNodes;

		// duplicate nodes with filtered canals so as to send only available interfaces
		res.nodes = []; 
		for(let node of nodes){ 
			let canals = []; 
			for(let canal of node.canals){ 
				let shouldAddCanal = this.isCanalValid(canal);
				if(shouldAddCanal) canals.push(canal);
			}
			// only push non empty canals
			if(canals.length === 0) continue;
			let pushedNode = {};
			res.nodes.push(pushedNode);
			pushedNode.canals = canals;
			pushedNode.id = node.id;
			// compute the common length between the request and node keys
			pushedNode.keys = [];
			for(let nk of node.keys){
				let common = Math.min( nk.prefix.length/2, this.common(nk, reqKey) );
				pushedNode.keys.push({
					prefix : nk.prefix.substr(2*common, 2*res.precision),
					common : common,
					span : nk.span,
					hiddenLength : (nk.hiddenLength || 0) + Math.max(0, nk.prefix.length/2 - common - res.precision)
				});
			}
			if(res.nodes.length == req.k) break;
		}
		return res;
	}

    /***
	 * check if a canal is valid
     */
    isCanalValid( canal ){
        switch(canal.type){
            case this.CANAL_CURRENT_CONNECTION :
            	return false;
            	break;
            case this.CANAL_HTTP :
            case this.CANAL_WEBSOCKET :
                return !!canal.address;
                break;
            case this.CANAL_WEBRTC :
                // can use webrtc if a remote address is specified or if the node is connected and a receiver is registered
				if(!canal.canals || !Array.isArray(canal.canals)) return false;
				for(let c of canal.canals){
					if(this.isCanalValid(c)) return true;
				}
                return false;
                break;
			case this.CANAL_SWANS :
                if(!canal.canals || !Array.isArray(canal.canals)) return false;
                if(!canal.receiver || typeof canal.receiver !== "string") return false;
                for(let c of canal.canals){
                    if(this.isCanalValid(c)) return true;
                }
                return false;
                break;
			default :
				return false;
        }
	}
	
	// add the info given by the node itself
	_processGiveInfo(answer){
		// check that the request id of the giveInfo match a requestId of a getInfo
		let req = this.getRequest(answer.requestId);
		if(!req) return;
		let node = req.node || {};
		let oldId = req.node ? req.node.id : null;
		let answerNode = answer.nodes[0];
		if(answerNode.keys) node.keys = answerNode.keys.map(kdata=>{ return {prefix : kdata.prefix, span : kdata.span }});
		if(answerNode.canals) node.canals = answerNode.canals;
		if(answerNode.id) node.id = answerNode.id;
		if(oldId && oldId !== node.id && this.nodes[oldId] == node){
            delete this.nodes[oldId];
		}
		if(node.id) this.addNode(node);
		this.removeRequest(answer.requestId);
		if(req.resolve) req.resolve(node);
	}
	
	_processGetInfo(req){
		let res = {};
		res.version = 0;
		res.method = "giveInfo";
		res.requestId = req.requestId;

		let resNode = {};

		// add the id if requested
		res.getId = req.getId;
		if(res.getId) resNode.id = this.getId();
		
		// add the key if requested
		res.getKeys = req.getKeys;
		if(res.getKeys) resNode.keys = this.keys.map(function(k){ return { prefix : k.prefix.slice(0, 2*req.precision), span : k.span} });
		// add the canals if requested
		res.getCanals = req.getCanals;
		if(res.getCanals) resNode.canals = this.getCanals();
		res.nodes = [resNode];
		return res;
	}
	
	// process incoming requests
	// event : data received from a nested interface
	processRequests(event){
		let req = this.bytesToObject(event.message);
		let answer;
		try{
			switch(req.method){
				case "getNode" :
					answer = this._processGetNode(req);
					break;
				case "giveNode" :
					this._processGiveNode(req);
					break;
				case "getInfo" :
					answer = this._processGetInfo(req);
					break;
				case "giveInfo" : 
					this._processGiveInfo(req);
					break;
				default : break;
			}
			// send answer when there is one
			if(answer){

				let answerBytes = this.objectToBytes(answer);
				if(event.interface.status() === "UP") event.interface.send(answerBytes, event.fport, this.port);
			}
		}
		catch(e){ console.log(e)}
    }
	
	// transform a byte request into the associated request object
	bytesToObject(bytes){
		// extract the version and the type of message
		let metadata = this._flaptools.decode(bytes, this.TEMPLATE_METADATA);
		if(metadata.version != 0) throw "Unsupported version : " + metadata.version;
		switch(metadata.method){
			case this.OPERATION_GET_NODE :
				return this.getNodeToObject(bytes);
			case this.OPERATION_GIVE_NODE :
				return this.giveNodeToObject(bytes);
			case this.OPERATION_GET_INFO :
				return this.getInfoToObject(bytes);
			case this.OPERATION_GIVE_INFO :
				return this.giveInfoToObject(bytes);
			default :
				return {};
		}
	}
	
	// retrieve an operation type : 
	_retrieveOperationType(bytes){
		
	}
	
	// transform a request object into its byte equivalent
	objectToBytes(req){
		switch(req.method){
			case "getNode" :
				return this.getNodeToBytes(req);
			case "giveNode" :
				return this.giveNodeToBytes(req);
			case "getInfo" :
				return this.getInfoToBytes(req);
			case "giveInfo" :
				return this.giveInfoToBytes(req);
			default : return new Uint8Array(0);
		}
	}
	
	// retrieve all the canals from the meta spawners
	getCanals(){
		let canals = this.metaInterface.getCanals();
		return canals;
	}
	
	////////////////////////////////////////////////////////////////////
	//////////// key related methods ///////////////////////////////////
	////////////////////////////////////////////////////////////////////
	
	// calculate distance between two keys with xor metrics
	dist(k1,k2){
		let r = [];
		let len1 = k1.prefix.length;
		let span1 = k1.span || 0;
		let len2 = k2.prefix.length;
		let span2 = k2.span || 0;
		for(let i=0; i<Math.max(len1,len2); i+=2){
			r.push(
				(i<len1 || len1+2*span2<=i) && (i<len2 || len2+2*span2<=i) ?
				// case 1, the two bytes are defined,
				parseInt( k1.prefix.substr(i,2) || "0",16) ^ parseInt( k2.prefix.substr(i,2) || "0",16)
				// case 2, it is in the span zone of one of the key
				: 0
			);
		}
		return r.map(j=>(j<16 ? "0" : "")+j.toString(16)).join("");
	}
	
	// calculate the minimal distance between a key and an array of keys
	minDist(k, kArr){
		let fl = this;
		let dists = kArr.map(k1=>fl.dist(k, k1))
		let distsIndex = dists.map((v,i)=>i);
		distsIndex.sort((i1,i2) => fl.isInf( dists[i1], dists[i2]) ? -1 : 1 );
		return dists[ distsIndex[0] ];
	}
	
	// check if the prefix of the k1 key is included in the k2 key
	// precision can be used to limit the comparison to the first bits
	isIncluded(k1,k2, precision=k1.prefix.length*4) {
		let res = this.common(k1,k2, false) >= Math.min( k1.prefix.length*4 + (k1.span||0)*8, precision ) || !k1.prefix.length;
		return res;
	}
		
	// inverse the suffix of a key, starting at bit b
	// accept hexadecimal or Uint8array as key
	inverseSuffix(key, b){
		let isHexa = typeof key === "string";
		let res = isHexa ? this._flaptools.hexToBytes(key) : key.slice();
		let byte = b/8 >> 0;
		if(byte >= key.length) return res;
		res[byte] = res[byte] ^ (Math.pow(2,8-(b%8))-1);
		for(let i=byte+1; i<res.length; i++){
			res[i] = 255 - res[i];
		}
		return isHexa ? this._flaptools.bytesToHex(res) : res;
	}
	
	// calculate the common byte/bit length of two keys
	common(k1,k2, byteResult = true){
        let i=0;
        let p1 = this._flaptools.hexToBytes(k1.prefix);
        let p2 = this._flaptools.hexToBytes(k2.prefix);
        let s1 = k1.span||0, s2 = k2.span||0;

        let common = Formation.prototype.common.call(null, p1, p2);
        if(common === 8*Math.min(p1.length, p2.length) ){
            common = 8*Math.min( p1.length + s1, p2.length + s2);
        }
        if(byteResult) common = Math.floor(common/8)
        return common;

        // let i=0;
        // let p1 = k1.prefix.toLowerCase();
        // let p2 = k2.prefix.toLowerCase();
        // let minLen = Math.min(p1.length, p2.length);
        // let s1 = k1.span||0, s2 = k2.span||0;
		//
        // for(; i<minLen && p1[i] === p2[i]; i++){}
        // let common = (i-i%2)/2;
        // if(common === Math.min(p1.length/2, p2.length/2) ){
        //     common += Math.min( p1.length/2 - common + s1, p2.length/2 - common + s2);
        // }
        // return common;
	}


	// compare two Uint8Array, return true if the first is strictly inferior to the second
	isInf(k1,k2){
		for(let i=0; i<Math.min(k1.length, k2.length); i++){
			if(k1[i] < k2[i]) return true;
			if(k1[i] > k2[i]) return false;
		}
		return k1.length < k2.length;
	}
	
	// return a new key as an array
	// first element is the key prefix (represented as an Uint8Array)
	// second element is the key span (represented as an integer or +infinity)
	generateKey(l=32, keySpan=0){
		let r = new Uint8Array(l);
		for(let i=0; i<l;i++) r[i] = Math.random()*256 >> 0;
		return { prefix : this._flaptools.bytesToHex(r), span : Number(keySpan) };
	}
	
	generateId(){
    	return this._flaptools.randomHex(this.ID_LENGTH);
		return Math.floor( Math.random() * Math.pow(2,this.ID_LENGTH*8) );
	}

	// return the id associated to a key
	keyId(key){
		return key.prefix + "/" + (key.span || 0).toString();
	}
	
	////////////////////////////////////////////////////////////////////
	/////////////// api related methods ////////////////////////////////
	////////////////////////////////////////////////////////////////////
	
	// create a request id
	_generateRequestId(){
		while(1){
            // let id = Math.floor(Math.random() * Math.pow(2,this.REQUEST_ID_LENGTH*8));
            let id = this._flaptools.randomHex(this.REQUEST_ID_LENGTH);
			if(id in this.requests) continue;
			else return id;
		}
	}
	
	// transform a Uint8Array representing a getNode request into an object
	getNodeToObject(bytes){
		let res = this._flaptools.decode(bytes, this.TEMPLATE_GETNODE);
		res.method = "getNode";
		return res;
	} 
	
	getNodeToBytes(data){
		data.method = this.OPERATION_GET_NODE;
		let res = this._flaptools.encode(data, this.TEMPLATE_GETNODE);
		return res;
	}
	
	giveNodeToObject(bytes){
		let res = this._flaptools.decode(bytes, this.TEMPLATE_GIVENODE);
		res.method = "giveNode";
		return res;
	}

	giveNodeToBytes(data){
		data.method = this.OPERATION_GIVE_NODE;
		let res = this._flaptools.encode(data, this.TEMPLATE_GIVENODE);
		return res;
	}
	
	getInfoToObject(bytes){
		let res = this._flaptools.decode(bytes, this.TEMPLATE_GETINFO);
		res.method = "getInfo";
		let flags = res.flags || 0;
		res.getKeys = !!(flags & 128);
		res.getCanals = !!(flags & 64);
		res.getId = !!(flags & 32);
		return res;
	}
	
	getInfoToBytes(data){
		data.method = this.OPERATION_GET_INFO;
		data.flags = (data.getKeys ? 128 : 0) + (data.getCanals ? 64 : 0) + (data.getId ? 32 : 0);
		let res = this._flaptools.encode(data, this.TEMPLATE_GETINFO);
		return res;
	}

	giveInfoToObject(bytes){
		let res = this._flaptools.decode(bytes, this.TEMPLATE_GIVEINFO);
		res.method = "giveInfo";
		return res;
	}
	
	giveInfoToBytes(data){
		data.method = this.OPERATION_GIVE_INFO;
		let res = this._flaptools.encode(data, this.TEMPLATE_GIVEINFO);
		return res;
	}
	
	////////////////////////////////////////////////////////////////////
	////////////    HELPERS          ///////////////////////////////////
	////////////////////////////////////////////////////////////////////

	flockInit(){
		// constants
		this.VERSION_LENGTH = 1;
		this.RESULT_NUMBER_LENGTH = 1;
		this.REQUEST_ID_LENGTH = 4;
		this.INFO_AND_PRECISION_LENGTH = 2;
		this.PRECISION_AND_KEY_LENGTH = 3;
		this.COMMON_AND_ID_LENGTH = 3;
		this.COMMON_LENGTH_AND_LENGTH = 3;
		this.CANAL_TYPE_LENGTH = 1;
		this.CANAL_ADDRESS_LENGTH_LENGTH = 1;
		this.CANAL_RECEIVER_LENGTH_LENGTH = 2;
		
		this.OPERATION_GET_NODE = 0;
		this.OPERATION_GIVE_NODE = 1;
		this.OPERATION_GET_INFO = 2;
		this.OPERATION_GIVE_INFO = 3;
		
		this.CANAL_CURRENT_CONNECTION = 0;
		this.CANAL_HTTP = 1;
		this.CANAL_WEBSOCKET = 2;
		this.CANAL_WEBRTC = 3;
		this.CANAL_SWANS = 5;
		this.CANAL_END = 31;
		
		this.ID_LENGTH = 6;

		this.SQUAWKER_DEFAULT_DISTANCE = "000000000000ff";

		// build EE used for key dictionnaries
		let ee_k_prefix = { n : "prefix", t : "hex", c : 2, dd : ""};
		let ee_k_common = { n : "common", t : "int", c : 3};
		let ee_k_hiddenLength = { n : "hiddenLength", t : "int", c : 4, dd : 0};
		let ee_k_span = { n : "span", t : "int", c : 5, dd : 0};
		let ee_key = [ee_k_prefix, ee_k_common, ee_k_hiddenLength, ee_k_span];
		this.TEMPLATE_KEY = {
			ee : this._eeArrToObj(ee_key)
		};
		
		// build EE used in canal dictionnaries
        this.TEMPLATE_CANAL = this.metaInterface.TEMPLATE_CANAL;

		// build EE used in node dictionnaries
		let ee_n_id = {n:"id", t:"hex", c:2};
		let ee_n_keys = {n:"keys", t:"arr", tpl : this.TEMPLATE_KEY, c:3};
		let ee_n_canals = {n:"canals", t:"arr", tpl : this.TEMPLATE_CANAL, c:4};
		let ee_node = [ee_n_id, ee_n_keys, ee_n_canals];
		this.TEMPLATE_NODE = {
			ee : this._eeArrToObj(ee_node)
		};
		
		// build the EE used for flock protocols
		let ee_fl_version = { n : "version", t : "int", c : 0, d : 0};
		let ee_fl_length = { n : "length", t : "int", c : 1};
		let ee_fl_method = { n : "method", t : "int", c : 2, r : true};
		let ee_fl_rid = { n : "requestId", t : "hex", c : 3, r : true};
		let ee_fl_k = { n : "k", t : "int", c : 4, d : 1};
		let ee_fl_precision = { n : "precision", t : "int", c : 5, d : 1};
		let ee_fl_prefix = { n : "prefix", t : "hex", c : 6, d : new Uint8Array() };
		let ee_fl_nodes = { n : "nodes", t : "arr", tpl : this.TEMPLATE_NODE, c : 7, d : []};
		let ee_fl_flags = { n : "flags", t : "int", c : 8, d : 0};

		let ee_fl = [ee_fl_version, ee_fl_length,ee_fl_method ,ee_fl_rid ,ee_fl_k, ee_fl_precision, ee_fl_prefix, ee_fl_nodes, ee_fl_flags];
		this.TEMPLATE_FLOCK_REQUEST = {
			ee : this._eeArrToObj(ee_fl)
		};
		
		// create specific template for each request
		let ee_getnode = [ee_fl_version, ee_fl_method, ee_fl_rid, ee_fl_k, ee_fl_precision, ee_fl_prefix];
		this.TEMPLATE_GETNODE = {
			ee : this._eeArrToObj(ee_getnode)
		}
		
		let ee_givenode = [ee_fl_version, ee_fl_method, ee_fl_rid, ee_fl_nodes];
		this.TEMPLATE_GIVENODE = {
			ee : this._eeArrToObj(ee_givenode)
		}
		
		let ee_getinfo = [ee_fl_version, ee_fl_method, ee_fl_rid, ee_fl_precision, ee_fl_flags];
		this.TEMPLATE_GETINFO = {
			ee : this._eeArrToObj(ee_getinfo)
		}
		
		let ee_giveinfo = [ee_fl_version, ee_fl_method, ee_fl_rid, ee_fl_nodes];
		this.TEMPLATE_GIVEINFO = {
			ee : this._eeArrToObj(ee_giveinfo)
		}
		
		// create a template to extract only the method
		let ee_meta = [ee_fl_version, ee_fl_method];
		this.TEMPLATE_METADATA = {
			ee : this._eeArrToObj(ee_meta)
		}
	}
	
	// small aux method to map an array of EE to an object with ee names as keys
	_eeArrToObj(arr){
		let r={};
		for(let a of arr) r[a.n]=a;
		return r;
	}
	
	bytesToAscii(codePoints){
		let result = String.fromCharCode(...codePoints);
		return result;
	}
	
	asciiToBytes(ascii, arr, offset){
		if(!offset) offset = 0;
		if(!arr) arr = new Uint8Array(ascii.length);
		for(let i =0; i<ascii.length; i++){
			arr[offset+i] = ascii.charCodeAt(i);
		}
		return arr;
	}
	
	// transform an hex into bytes if not already done
	_hexToBytes(h){
		if(!h) return new Uint8Array(0);
		return typeof h === "string" ? this._flaptools.hexToBytes(h) : h;
	}
}

// formations is an object that links every bucket of a key to a number (assumed, a number of required node per bucket)
// it is used to determine the number of connections with foreign nodes to keep the flock network alive, or to insure reliable access to a given key
class Formation{
	constructor(key, data={}){
		this.key(key || new Uint8Array());
		this.afterKey( "afterKey" in data ? data.afterKey : 0 );
		this.beforeKey( "beforeKey" in data ? data.beforeKey : 3 );
		this.buckets( data.buckets || {} );
	}
	
	// getter and setter for the key over which the formation is built
	key(k){
		if(k) this._key = k;
		return this._key;	
	}
	
	// getter and setters for the afterKey property (number of default nodes in buckets after the last byte of the key)
	afterKey(n){
		if(n !== undefined) this._afterKey = n;
		return this._afterKey;
	}
	
	// getter and setters for the beforeKey property (number of default nodes in buckets before the last bit of the key)
	beforeKey(n){
		if(n !== undefined) this._beforeKey = n;
		return this._beforeKey;
	}
	
	// get the value for a specific bucket : ie the number of node requested for a given bucket of the key
	bucket(i){
		if(!(Number.isInteger(i) && i >= 0)) throw "bucket method require a positive integer : got "+i;
		if(i in this._buckets) return this._buckets[i];
		let byteNumber = Math.floor(i/8);
		return byteNumber < this._key.length ? this._beforeKey : this._afterKey;
	}

	// getter and setter for the bucket : object that contains the number of requested nodes per bucket (if not provided for a bucket, use default values beforeKey and afterKey)
	buckets(b){
		if(b){
			this._buckets = {};
			for(let i in b) this._buckets[i] = b[i];
		}
		let duplicate = {};
		for(let i in this._buckets) duplicate[i] = this._buckets[i];
		return duplicate;
	}
	
	// return a filtered list of buckets (in index and in range value)
	filterBuckets(minValue=0, maxValue=+Infinity, minIndex=0, maxIndex=this._key.length*8 ){
		let res = {};
		for(let i=minIndex; i<= maxIndex; i++){
			let v = this.bucket(i);
			if(v>=minValue && v <= maxValue) res[i] = v;
		}
		return res;
	};
	
	// retrieve the number of common bits between two keys
	common(k1, k2){
		let b = 0;
		let bits = [128, 64, 32, 16, 8, 4, 2, 1];
		let maxBit = Math.min(k1.length*8, k2.length*8);
		for(; b<maxBit; b++){
			let byte = b/8>>0;
			if( (k1[byte]^k2[byte]) & bits[b%8]) break;
		}
		return b;
	}

	// retrieve the bucket of the given key, represented by its prefix and its span
	keyBucket(prefix, span=0, key=this._key){
        let cb = this.common(key, prefix);
        // if the key is included into the formation key, the span of the key increase the common part
        if(cb === 8*prefix.length && span){
            cb = Math.min( 8*prefix.length + 8*span, 8*key.length);
        }
        return cb;
    }

	// map a list of nodes (nid=>keys) to the required buckets
	mapKeysToBuckets(nodes){
		let buckets = this.buckets();
		let result = {};
		for(let nid in nodes){
			let keys = nodes[nid];
			// push the node in the maximal buckets it belongs to
			let maximalNodeBucket = -1;
			for(let key of keys){
				let nodeBucket = this.keyBucket(key.prefix, key.span);
                if(!(nodeBucket in buckets)){
                    buckets[nodeBucket] = nodeBucket < this._key.length*8 ? this._beforeKey : this._afterKey;
                }
                if(buckets[nodeBucket]){
                	maximalNodeBucket = Math.max(maximalNodeBucket, nodeBucket);
                }
            }

            if(maximalNodeBucket === -1) continue;
			if(!result[maximalNodeBucket]) result[maximalNodeBucket] = [];
			if(result[maximalNodeBucket].length < buckets[maximalNodeBucket]) result[maximalNodeBucket].push(nid);
        }
		return result;
    }
	
	// return a formation object in which are removed a list of node keys : with a min of 0 per bucket
    // node keys are object with prefix(Uint8Array) and span(integer)
	complement(keys=[], reverseKey = false){
		if(!Array.isArray(keys)) throw "expected array as complement method first argument";
		let formationKey = this.key().slice();
		let cpm = new Formation(formationKey, {
			afterKey : this.afterKey(),
			beforeKey : this.beforeKey(),
		});
		let buckets = this.buckets();
		for(let key of keys){
			let cb = this.keyBucket(key.prefix , key.span);
			if(!(cb in buckets)){
				buckets[cb] = cb < formationKey.length*8 ? cpm.beforeKey() : cpm.afterKey();
			}
			if(buckets[cb]>0) buckets[cb]--;
		}

		cpm.buckets(buckets);
		return cpm;
	}
}

Flock.prototype._Formation = Formation;



// CONCATENATED MODULE: ./src/warehouse/lib/Warehouse.js
/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

/***
 * interface for warehouse
 * 
 * ***/
 
class Warehouse {

	constructor(data={}){
		// initialize default values and constants
		this._whInit();
	}

	///////////////////////////////////////////////////////////////////
	//////////////////   API METHODS //////////////////////////////////
	///////////////////////////////////////////////////////////////////

	// stop the warehouse
	stop(){}

	// check if the warehouse has a given resource, identified byt the key(prefix) and a potential suffix(feather)
	hasResource(prefix, opts={}){
		return Promise.resolve(false);
	}

	/***
	 * get available resources in a range
	 * ***/
	 availableResources(minPrefix, maxPrefix, opts={}){
		 return Promise.resolve([]);
	 }

	 /***
	  * method to be called to store a new resource
	  * opts can contain the following :
	  * 	priority : an integer between 0 and 9 that will enable to know which data to delete when support is full (lower priority, less important)
	  * 	dversion : the dversion of the document
	  * 	expire : time at which the data can be removed automatically, in milliseconds. value lower than 1000 have special significations :
	  * 		0 : never expire
	  * ***/
	 storeResource(prefix,resource, opts={}){
		 return Promise.resolve();
	 }

	 // retrieve a resource
	 getResource(prefix, opts={}){
		 return Promise.reject();
	 }

	 deleteResource(prefix, opts={}){
		 return Promise.resolve();
	 }

	 deleteResourceRange(lowPrefix, highPrefix, opts={}){
		 return Promise.resolve();
	 }

	/***
	 * every time a new resource is added to the warehouse, send a notification about it
	 * this method retrun an id of operation, to stop the watch by calling stopOperation(id)
	 * ***/
	 watch(data){
		 let watchId = data.id || ++this._id;
		 this._watch[watchId] = {
			 callback : data.callback,
		 }
		 return watchId;
	 }

	 unwatch(wid){
		if(this._watch[wid]) delete this._watch[wid];
	 }

	 /***
	  * process all the pending watch to notify about a new key
	  * ***/
	triggerNotify(keys){
         if(!Array.isArray(keys)) keys = [keys];
		 for(let wid in this._watch){
		 	this._watch[wid].callback(keys);
		}
	}

	////////////////////////////////////////////////////////////////////
	/////////////////   HELPERS  ///////////////////////////////////////
	////////////////////////////////////////////////////////////////////

	/** prepare an object before storing it
	 * ***/
	 prepareResource(prefix, resource, opts={}){
		 return {
             prefix : String(prefix).toLowerCase(),
			 data : resource,
			 dversion : opts.dversion || 0,
			 expire : opts.expire || 0,
			 priority : opts.hasOwnProperty("priority") ? opts.priority : 0,
		 }
	 }

	  /**
	  * check if a key is in a given range
	  * ***/
	 inRange(prefix, minRange, maxRange){
		 return prefix >= minRange && prefix <= maxRange;
	 }

	_whInit(){
		this.DB_WAREHOUSE = "warehouse";
		this.TABLE_DOCUMENT = "document";

		// create a list of watch request currently working
		 // data is indexed by watch id, and contains : minPrefix, maxPrefix, callback
		 this._watch = {};
		 this._id = 0;
	}
}

//



// CONCATENATED MODULE: ./src/warehouse/lib/MDBWarehouse.js
/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/


/***
 * Indexeddb base warehouse
 * ***/



class MDBWarehouse_MDBWarehouse extends Warehouse{
	
	constructor(data={}){
		super(data);
		this.dbUrl = data.url || "mongodb://localhost:27017";
		this.mongo = __webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'mongodb'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()));
		this.mongoClient = this.mongo.MongoClient;
		this.database = data.database || this.DB_WAREHOUSE;
		this.collection = data.collection || this.TABLE_DOCUMENT;
		this._mdbInit();
	}
	
	///////////////////////////////////////////////////////////////////
	//////////////////   API METHODS //////////////////////////////////
	///////////////////////////////////////////////////////////////////

	// stop all the connections to databases
	stop(){
		if(this._client){
            this._client.then(client=>client.close())
		}
	}

	// check if the warehouse has a given resource, identified byt the prefix and a potential suffix(feather)
	hasResource(prefix, opts={}){
		return this.availableResources(prefix, prefix, opts).then(keys=>!!keys.length);
	}
	
	/***
	 * get available resources in a range
	 * ***/ 
	 availableResources(minPrefix, maxPrefix, opts={}){
		 let prom = this.dbGetKeys(this.database, this.collection, minPrefix, maxPrefix, opts);
		 // keep unique keys
		 //return prom.then(keys=> keys.filter((v,i) => keys.indexOf(v) == i) );
	 	 return prom;
	 }
	 
	 /***
	  * method to be called to store a new resource
	  * opts can contain the following :
	  * 	priority : an integer between 0 and 9 that will enable to know which data to delete when support is full (lower priority, less important)
	  * 	dversion : the dversion of the document
	  * 	expire : time at which the data can be removed automatically, in milliseconds. value lower than 1000 have special significations :
	  * 		0 : never expire
	  * ***/
	 storeResource(prefix,resource, opts={}){
		 let storeObject = this.prepareResource(prefix, resource, opts);
		 let prom = this.dbStore(this.database, this.collection, storeObject);
		 let wh = this;
		 prom.then((res)=>{
			 // after returning the result, proceed to notification of all those who are watching this warehouse new entries
			 let notifyData = {prefix : prefix};
			 if(opts.hasOwnProperty("dversion")) notifyData.dversion = opts.dversion;
			 wh.triggerNotify(notifyData);
		 });

		 return prom;
	 }
	 
	 // retrieve a resource
	 getResource(prefix, opts={}){
         prefix = prefix.toLowerCase();
		 return this.dbGet(this.database, this.collection, prefix, opts)
		 .then(v=> v ? new Uint8Array(v.data) : Promise.reject("could not retrieve "+prefix+" "+opts.dversion));
	 }
	 
	 deleteResource(prefix, opts={}){
         prefix = prefix.toLowerCase();
		 return this.dbRemove(this.database, this.collection, prefix, opts);
	 }
	 
	 deleteResourceRange(lowPrefix, highPrefix, opts={}){
         lowPrefix = lowPrefix.toLowerCase();
         highPrefix = highPrefix.toLowerCase();
		 return this.dbRemoveRange(this.database, this.collection, lowPrefix, highPrefix, opts);
	 }
	 
	 ///////////////////////////////////////////////////////////////////
	 //////////////  MONGODB   SPECIFIC METHODS  ///////////////////////
	 ///////////////////////////////////////////////////////////////////


	 // open a client
	 dbClient(url){
		 if(!this._client) this._client = this.mongoClient.connect(url);
		 return this._client;
	 }
	 
	 // open an idb, return a promise
	 dbOpen(dbName, url){
		 this._db[dbName] = this.dbClient(url)
		 .then(function(client){
			  return client.db(dbName);
		});
		return this._db[dbName];
	 }
	 
	 // retrieve a given collection
	 dbCollection(dbName, collection){
		 if(!this._collections[dbName]) this._collections[dbName] = {};
		 if(!this._collections[dbName][collection]){
			 let wh = this;
			 this._collections[dbName][collection] = this._db[dbName].then(db=>db.collection(collection));
		 }
		 return this._collections[dbName][collection];
	 }
	 
	 // store elements
	 dbStore(dbName, collectionName, value){
		  let colProm = this.dbCollection(dbName, collectionName);
		  // transform the Uint8Array data into a string
		  value.data = this.mongo.Binary( Buffer.from(value.data) );
		  return colProm.then(col=> col.update(  {prefix : value.prefix, dversion : value.dversion}, value, { upsert: true } ) );
	 }
	 
	 dbRemove(dbName, collectionName, prefix, opts={}){
		 let colProm = this.dbCollection(dbName, collectionName);
		 let filter = { prefix : prefix };
		 if(opts.dversion || opts.dversion === 0) filter.dversion = opts.dversion;
		 return colProm.then(col=> col.deleteMany(filter));
	 }
	 
	 // remove an element
	 dbRemoveRange(dbName, collectionName, prefixMin, prefixMax, opts={}){
		 let colProm = this.dbCollection(dbName, collectionName);
		 let filter = { $and: [ { prefix: { $gte: prefixMin } }, { prefix: { $lte: prefixMax } } ] };
		 if(opts.dversion || opts.dversion === 0) filter.dversion = opts.dversion;
		 return colProm.then(col=> col.deleteMany( filter ));
	 }
	 
	 // retrieve an element
	 dbGet(dbName, collectionName, prefix, opts={}){
		 let colProm = this.dbCollection(dbName, collectionName);
		 let filter = { prefix : prefix };
		 if(opts.dversion || opts.dversion === 0) filter.dversion = opts.dversion;
		 return colProm.then(col=> col.find(filter).sort({dversion:-1}).limit(1).next() )
		 .then(res=>{
			if(!res) return Promise.reject();
			res.data = res.data.buffer;
			return res; 
		 });
	 }
	 
	 // retrieve the available keys in a range
	 dbGetKeys(dbName, collectionName, prefixMin, prefixMax, opts={}){
         prefixMin = prefixMin.toLowerCase();
         prefixMax = prefixMax.toLowerCase();
		 let colProm = this.dbCollection(dbName, collectionName);
		 let filter = { $and: [ { prefix: { $gte: prefixMin } }, { prefix: { $lte: prefixMax } } ] };
		 if(opts.dversion || opts.dversion === 0) filter.dversion = opts.dversion;
		 return colProm.then(
			col=>{
				let p = col.find( filter, {prefix : true} )
				 return new Promise((res,rej)=>{
					 p.toArray(function(err, items){
						res(items ? items.map(i=>{return {prefix : i.prefix, dversion : i.dversion} }) : []);
					 });
				 });
			   }
		).catch(function(e){
			console.log("error", e)
		 });
	 }
	 
	 ///////////////////////////////////////////////////////////////////
	 //////////////////       HELPERS     //////////////////////////////
	 ///////////////////////////////////////////////////////////////////	
	
	 /*** initialize the warehouse
	  * ***/
	 _mdbInit(){
		 // create the lsit of databases 
		 this._db={};
		 
		 // store the available collections : 
		 this._collections = {};
		 
		 // create/open the db
		 this.dbOpen(this.database, this.dbUrl);
	 }
}


// CONCATENATED MODULE: ./src/warehouse/lib/IDBWarehouse.js
/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

/***
 * Indexeddb base warehouse
 * ***/



class IDBWarehouse_IDBWarehouse extends Warehouse{
	
	constructor(data={}){
		super(data);
		this._idbInit();
	}
	
	///////////////////////////////////////////////////////////////////
	//////////////////   API METHODS //////////////////////////////////
	///////////////////////////////////////////////////////////////////
	
	// check if the warehouse has a given resource, identified byt the key(prefix) and a potential suffix(feather)
	hasResource(prefix, opts={}){
        prefix = prefix.toLowerCase();
		return this.availableResources(prefix, prefix, opts).then(keys=>!!keys.length);
	}
	
	/***
	 * get available resources in a range
	 * ***/
	 availableResources(minPrefix, maxPrefix, opts={}){
        minPrefix = minPrefix.toLowerCase();
        maxPrefix = maxPrefix.toLowerCase();
		 // if("version" in opts) version = parseInt(version);
		 let prom = this.dbGetKeys(this.DB_WAREHOUSE, this.TABLE_DOCUMENT, minPrefix, maxPrefix, opts);
		 // keep unique keys
		 return prom.then(keys=> keys.filter((v,i) => keys.indexOf(v) == i) );
	 }
	
	 /***
	  * method to be called to store a new resource
	  * opts can contain the following :
	  * 	priority : an integer between 0 and 9 that will enable to know which data to delete when support is full (lower priority, less important)
	  * 	dversion : the dversion of the document
	  * 	expire : time at which the data can be removed automatically, in milliseconds. value lower than 1000 have special significations :
	  * 		0 : never expire
	  * ***/
	 storeResource(prefix,resource, opts={}){
         prefix = prefix.toLowerCase();
		 let storeObject = this.prepareResource(prefix, resource, opts);
		 let prom = this.dbStore(this.DB_WAREHOUSE, this.TABLE_DOCUMENT, storeObject);
		 let wh = this;
		 prom.then((res)=>{
             // after returning the result, proceed to notification of all those who are watching this warehouse new entries
             let notifyData = {prefix : prefix};
             if(opts.hasOwnProperty("dversion")) notifyData.dversion = opts.dversion;
             wh.triggerNotify(notifyData);
		 });
		 prom.catch(function(e){
		 	console.log("ERROR CAUGHT ::::::: ", e)
		 })

		 return prom;
	 }
	 
	 // retrieve a resource
	 getResource(prefix, opts = {}){
         prefix = prefix.toLowerCase();
		 return this.dbGet(this.DB_WAREHOUSE, this.TABLE_DOCUMENT, prefix, opts)
		 .then(v=> v ? v.data : Promise.reject("could not retrieve "+prefix+" "+(opts.dversion||"") ));
	 }
	 
	 deleteResource(prefix, opts={}){
		 prefix = prefix.toLowerCase();
		 return this.dbRemove(this.DB_WAREHOUSE, this.TABLE_DOCUMENT, prefix, opts);
	 }
	 
	 deleteResourceRange(lowPrefix, highPrefix, opts={}){
         lowPrefix = lowPrefix.toLowerCase();
         highPrefix = highPrefix.toLowerCase();
		 return this.dbRemoveRange(this.DB_WAREHOUSE, this.TABLE_DOCUMENT, lowPrefix, highPrefix, opts);
	 }
	 
	 ///////////////////////////////////////////////////////////////////	 
	 //////////////  INDEXEDDB SPECIFIC METHODS  ///////////////////////
	 ///////////////////////////////////////////////////////////////////
	 
	 // open an idb, return a promise
	 dbOpen(name, version, schema){
		 let warehouse = this;
		 let rq = this.indexedDB.open(name, version);
		 let p = new Promise((res,rej)=>{
			 rq.onerror = (e)=>rej("IDB error : "+e.target.error);
			 rq.onsuccess = (e)=> res(e.target.result);
			 rq.onupgradeneeded = function(e){
				// create the stores
				let db = e.target.result;
				let stores = schema.stores;
				for(let storeName in stores||{}){
					let store = db.createObjectStore(storeName, stores[storeName].keyPath);
					// create store indexes
					for(let indexName in schema.index || {}){
						store.createIndex(indexName, indexName, schema.index[indexName]);
					}
				}
			 }
		 });
		 
		 // update the transactions object to accept transaction from this database
		 this._transactions[name] = {};
		 
		 // store the promise in the database list
		 this._db[name] = p;
		 return p;
	 }
	 
	 // create a transaction
	 dbTransaction(dbname, store){
		 // if a transaction has already been created for this store, return it
		 // if( this._transactions[dbname][store]) return this._transactions[dbname][store];
		 let warehouse = this;
		 let p = this._db[dbname].then((db)=>{
			 let t = db.transaction(store, "readwrite");
			 // store the transaction, but remove it once done
			 warehouse._transactions[dbname][store] = Promise.resolve(t);
			 t.onsuccess = (e)=> delete warehouse._transactions[dbname][store];
			 t.onerror = t.onsuccess;
			 t.oncomplete = t.onsuccess;
			 return t;
		 });
		 return p;
	 }
	 
	 // store elements
	 dbStore(dbname, store, value){
		 // create a transaction
		 let transProm = this.dbTransaction(dbname, store, "readwrite");
		 // create the resulting promise
		 let p = transProm.then((t)=>{
		 	 let storeObj = t.objectStore(store);
		 	 let storeProm = new Promise((res,rej)=>{
				 let storeReq = storeObj.put(value, [value.prefix, value.dversion]);
				 storeReq.onsuccess=()=>res();
				 storeReq.onerror=()=> (e)=>rej("IDB error : "+e.target.errorCode);
			 });
			 return storeProm;
		 });
		 return p;
	 }
	 
	 dbRemove(db, store, prefix, opts={}){
		 return this.dbRemoveRange(db, store, prefix, prefix, opts);
	 }
	 
	 // remove an element
	 dbRemoveRange(db, store, prefixMin, prefixMax, opts={}){
		 let transProm = this.dbTransaction(db, store, "readwrite");
		 let target = this.idbKeyRangeVersion(prefixMin, prefixMax, opts.dversion);
		 let p = transProm.then((t)=>{
			 let deleteProm = new Promise((res,rej)=>{
				 let rq = t.objectStore(store).delete(target);
				 rq.onsuccess = function(){ res()};
				 rq.onerror = (e)=>rej("IDB error : "+e.target.errorCode);
			 })
			 return deleteProm;
		 });
		 return p;
	 }
	 
	 // retrieve an element
	 dbGet(db, store, prefix, opts={}){
		 let transProm = this.dbTransaction(db, store, "readwrite");
		 let range = this.idbKeyRangeVersion(prefix, prefix, opts.dversion);
		 let p = transProm.then((t)=>{
			 let getProm = new Promise((res,rej)=>{
				 try{
                     let rq = t.objectStore(store).getAll(range);
                     rq.onsuccess = (e)=>res(e.target.result);
                     rq.onerror = (e)=>rej("IDB error : "+e.target.errorCode);
				 }
				 catch(e){
			 		console.log("IDB WAREHOUSE ERROR : ",e);
				 }
			 });
			 return getProm;
		 })
		 .then(arr=>{
			if(!arr) return Promise.reject();
			let res = null;
			for(let e of arr){
				if(!res || e.dversion>res.dversion) res=e;
			}
			return res; 
		 });
		 return p;
	 }
	 
	 // retrieve the available keys in a range
	 dbGetKeys(db, store, lower, higher, opts={}){
		 let transProm = this.dbTransaction(db, store, "readwrite");
		 let warehouse = this;
		 let p = transProm.then((t)=>{
			 let getKeysProm = new Promise((res,rej)=>{
				 var range = warehouse.idbKeyRangeVersion(lower, higher, opts.dversion)
				 let rq = t.objectStore(store).getAllKeys(range);
				 rq.onsuccess = (e)=>res(e.target.result);
				 rq.onerror = (e)=>rej("IDB error : "+e.target.errorCode);
			 });
			 return getKeysProm;
		 });
		 return p.then(keys=>keys.map(k=>{return { prefix : k[0], dversion : k[1]}}));
	 }
	 
	 // create a key range
	 idbKeyRange(low, high){
		 return this.IDBKeyRange.bound(low, high);
	 }
	 
	 // create a key range with version
	 idbKeyRangeVersion(low, high, dversion=null){
		 if(dversion === null || dversion === undefined) return this.IDBKeyRange.bound([low,0], [high,999999999999]);
		 else return this.IDBKeyRange.bound([low,dversion], [high,dversion]);
	 }
	 ///////////////////////////////////////////////////////////////////
	 //////////////////       HELPERS     //////////////////////////////
	 ///////////////////////////////////////////////////////////////////	
	
	 /*** initialize the warehouse
	  * ***/
	 _idbInit(){
		 // retrieve indexeddb objects
		 this.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
	 	 this.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction;
		 this.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange
		 
		 // create the lsit of databases 
		 this._db={};
		 
		 // store the pending transactions : remove them once done
		 // indexed by storeName
		 this._transactions = {};
		 
		 // IDB_WAREHOUSE shecma
		 this.DB_WAREHOUSE_VERSION = 1;
		 this.STORE_DOCUMENT_SCHEMA = {
			 stores : {
				 [this.TABLE_DOCUMENT] : {
					 index : {
						 expire : "expire",
						 prefix : "prefix",
                         dversion : "dversion",
						 
					 },
					 opts : { keyPath : ["prefix","dversion"] }
				 }
			 }
		 };
		 // create the DB
		 this.dbOpen(this.DB_WAREHOUSE, this.DB_WAREHOUSE_VERSION, this.STORE_DOCUMENT_SCHEMA);
	 }
}


// CONCATENATED MODULE: ./src/frf/lib/Frf.js
/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/


/*** helper class for flock resources format (frf)
 * it converts data to/from bytes Uint8Array according to frf specifications
 * 
 * 
 * ***/




class Frf_Frf{
	constructor(data={}){
		// retrieve dependencies
		this._flaptools = data.flaptools || new Flaptools_Flaptools();
        this._secswans = data.secswans || new secswans_Secswans();

		// set the list of authorized signing algorithm
		this.authorizedSignatureTypes = data.authorizedSignatureTypes ||
			[ this._secswans.ENCTYPE_RSA_1024 , this._secswans.ENCTYPE_RSA_2048, this._secswans.ENCTYPE_RSA_4096 ];
		// initialize constants
		this._initConstants();
		this._initDictionnaries();

        this.signatureType = data.signatureType || this.DEFAULT_SIGNTOOLS_TYPE;
	}

    ////////////////////////////////////////////////////////////////////
    /////////////        SIGNATURE METHODS            //////////////////
    ////////////////////////////////////////////////////////////////////

    /***
	 * generate a new keypair
     */
    createKeys(type = this.signatureType){
		return this._secswans.generateKey(type);
	}

    /***
	 * build the signature of a given bytes array
     */
    computeSignature(bytes, keys){
    	let privkey = keys.private || keys;
        return this._secswans.sign(bytes, privkey);
	}

    /**
	 * create a sky by pushing a signtools data in a document
     */
	createSky(doc, keys){
		// allow both a full key or the public key data
		let pubkeydata = keys.public || keys;
		doc.signtools = pubkeydata;
		return doc;
	}

    /***
	 * create a new signed document whose body is the input document
     */
	createSignedDocument(doc, keys, skyroot=false, keystart = ""){
        let keyType = (keys.private && keys.private.type) || (keys.public && keys.public.type);
        let hashedKey = skyroot || keyType === this._secswans.HASHTYPE_SHA_256 || keyType === this._secswans.HASHTYPE_SHA_512;
        if(hashedKey && keystart){
        	doc.keystart = keystart;
		}
		let bytes = doc instanceof Uint8Array ? doc : this.documentToBytes(doc);
		let signature = this.computeSignature(bytes, keys);
		let signedDocument = {
			body : bytes,
			signature : signature
		}

		if(hashedKey){
			signedDocument.key = keystart+ this._secswans._hashAndPad( this._flaptools.hexToBytes(signature), "sha256", 2*this.SKY_KEY_LENGTH);
		}
		return signedDocument;
	}

    /**
	 * check the signature of a single document
     */
    checkSignature(signedDocument, signtools=null){
		if(!signedDocument.hasOwnProperty("signature")) return false;
		let providedSignature = signedDocument.signature;
		if(!signtools){
			let document = this.documentToObject(signedDocument.body);
			signtools = document.signtools;
		}
		if(!signtools || typeof signtools != "object") return false;

		// let realSignature = ;

	}

    /**
	 * ensure the validity of a document, and check the signature of the parents recursively
	 * needs every parents to check recursively
     */
	checkDocument(document, signedParents=[]){
		let parents = {};

		// extract data about the document to check and about every parent
		for(let parent of signedParents){
			let pdata = this.extractDataFromSigned(parent);
			parents[ pdata.key ] = pdata;
		}
		let docdata = this.extractDataFromSigned(document);
        parents[ docdata.key ] = docdata;

		// build the array of parent in the right order
		let key = docdata.key;
		let orderedParents = [];
		do{
			if(!parents.hasOwnProperty(key)) return false;
			let parent = parents[key];
			orderedParents.push(parent);
			if(this.isSKyRoot(key)) break;
			// if a parent is not the sky root and has no prefix, the validity can not be checked up to the root
			if(!parent.prefix) return false;
			key = key.slice(0, 2*parent.prefix) // *2 becase hexadecimal
		}
		while(true);

		let signtools = null;
		while(orderedParents.length){
			let doc = orderedParents.pop();

			// for the sky root
			if(!signtools){
                // the sky root must provide for a signtools
				if(!doc.signtools){
                    return false;
				}
				else signtools = doc.signtools;
			}

			// check either the signature if provided, or take the key if not provided
			let providedSignature = doc.signature || doc.key;
			if(!providedSignature) return false;
			let isCorrectlySigned = this._secswans.checkSign( doc.signedBytes, signtools, providedSignature );
			if(!isCorrectlySigned){
                return false;
			}

			// if keystart is provided in the document, check that the keystart match with the key provided
			if(doc.keystart && !doc.key.startsWith(doc.keystart)){
				return false;
			}

			// for sky roots, the key must be the hash of the signature
			if(this.isSKyRoot(doc.key)){
				// compute the hash
				let hashedSignature = this._secswans._hashAndPad( this._flaptools.hexToBytes(providedSignature), "sha256", 2*this.SKY_KEY_LENGTH) ;
				if(doc.key !== hashedSignature){
					return false;
                }
			}
			// if a new signtools is provided, it defines a new subsy
			if(doc.signtools) signtools = doc.signtools;
		}

		return true;
	}

	// check if a key can be consider as a sky root
	// for the moment, it just needs to be a 32 byte hexadecimal
	isSKyRoot( key ){
		if(key instanceof Uint8Array) key = this._flaptools.bytesToHex(key);
		if(typeof key !== "string") return false;
		return key.length === 2*this.SKY_KEY_LENGTH;
	}

	// aux method to extract key, signature, signtools, prefix and body bytes from a document or a signed document
    extractDataFromSigned(outerDocument){
        let result = { }

        // extract the bytes on which signature must be checked
		result.signedBytes = outerDocument.body || new Uint8Array();

        // it is expected to be a signed document containing a document
        let innerDocument = outerDocument.body ? this.documentToObject(outerDocument.body) : {};
        result.signature = outerDocument.signature;
        result.key = outerDocument.key || innerDocument.key || null;
        result.signtools = outerDocument.signtools || innerDocument.signtools;

        if(innerDocument.signtools) result.signtools = innerDocument.signtools;
        else if(outerDocument.signtools) result.signtools = outerDocument.signtools

        if(innerDocument.hasOwnProperty("prefix")) result.prefix = innerDocument.prefix;
        else if(outerDocument.hasOwnProperty("prefix")) result.prefix = outerDocument.prefix;

        // retrieve other data in the innerDocument
		if(innerDocument){
			for(let i in innerDocument){
				if(result.hasOwnProperty(i)) continue;
				result[i] = innerDocument[i];
			}
		}

        result.body = innerDocument.body || outerDocument.body;


        return result;
    }

	////////////////////////////////////////////////////////////////////
	/////////////        CONVERSION METHODS            /////////////////
	////////////////////////////////////////////////////////////////////
	
	// transform a signature Uint8Array data into its object representation
	signtoolsToObject(o){
		return this._flaptools.decode(o, this.TEMPLATE_SIGNTOOLS);
	}
	
	// transform a signature onject data into its byte representation
	signtoolsToBytes(b){
		return this._flaptools.encode(b, this.TEMPLATE_SIGNTOOLS);
	}
	
	// transform a document Uint8Array data into its object representation
	documentToObject(o){
		return this._flaptools.decode(o, this.TEMPLATE_DOCUMENT);
	}
	
	// transform a document onject data into its byte representation
	documentToBytes(b){
		return this._flaptools.encode(b, this.TEMPLATE_DOCUMENT);
	}
	
	////////////////////////////////////////////////////////////////////
	////////////////////////   HELPERS     /////////////////////////////
	////////////////////////////////////////////////////////////////////

	_initConstants(){
    	this.DEFAULT_SIGNTOOLS_TYPE = this._secswans.ENCTYPE_RSA_1024;
        this.SKY_KEY_LENGTH = 32;
	}
	
	// initialize dictionnaries and EE used by frf
	_initDictionnaries(){
		var a2o = (a)=>{
			let r={},e;
			for(e of a) r[e.n]=e;
			return r;
		}
		
		// initialize the signature dictionnary
		let EE_SIGNATURE = [
			{n:"version", t:"int", d:0, c:0},
			{n:"length", t:"int", c:1},
			{n:"type", t:"int", c:2,
				rpl : {
					1 : "rsa1024",
					2 : "rsa2048",
                    3 : "rsa4096",
                    4 : "sha256",
                    // 5 : "sha512",
				}
			},
			{n:"n", t:"hex", c:16},
			{n:"e", t:"hex", c:17},
			{n:"d", t:"hex", c:18},
		]
		this.TEMPLATE_SIGNTOOLS = {
			ee : a2o(EE_SIGNATURE),
		};
		
		// initialize the document dictionnary
		let EE_DOCUMENT = [
			{n:"version", t:"int", d:0, c:0},
			{n:"length", t:"int", c:1},
			{n:"type", t:"int", c:2,
				rpl : {
					0 : "text",
					1 : "multipart",
					2 : "message",
					3 : "image",
					4 : "audio",
					5 : "video",
					6 : "application",
					7 : "font",
					8 : "example",
					9 : "model",
					10 : "bytes",
					128 : "x-flock",
				}
			},
            {n:"subtype", t:"ascii", c:3},
			{n:"name", t:"utf8", c:4, dd : ""},
			{n:"signtools", t:"dict", tpl : this.TEMPLATE_SIGNTOOLS, c:5},
			{n:"signature", t:"hex", c:6},
			{n:"expire", t:"int", c:7, dd : 0},
			{n:"dversion", t:"int", c:8, dd : 0},
			{n:"key", t:"hex", c:9, dd : ""},
			{n:"prefix", t:"int", c:10, dd : 0},
			{n:"wingspan", t:"hex", c:11},
			{n:"nextfeather", t:"hex", c:12},
            {n:"body", t:"bin", c:13},
			{n:"featherlength", t:"int", c:14, dd : 0},
            {n:"winglength", t:"int", c:15},
            {n:"author", t:"utf8", c:16},
            {n:"time", t:"int", c:17},
            {n:"keystart", t:"hex", c:18},
		];
		this.TEMPLATE_DOCUMENT = {
			ee : a2o(EE_DOCUMENT),
		};
	}


}


// CONCATENATED MODULE: ./src/frp/lib/Frp.js
/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

/*** frp (flock resource protocol) implements frp communication messages to contact other frp nodes in order to send request/receive documents
 * frp is a plug service where you add flock applications (flaps) to request or carry resources : when receiving a request, frp will forward it to the flaps
 * 
 * ***/





class Frp_Frp {
	constructor(data={}){
		this._flaptools = data.flaptools || new Flaptools_Flaptools();
		this._threadops = this._flaptools.threadops();
		this._flock = data.hasOwnProperty("flock") ? data.flock : new Flock();
		this._metainterface = data.metaInterface || ( this._flock ? this._flock.metaInterface : null);
		this._frf = data.frf || new Frf_Frf();
		
		this.transactions = {};
		this._warehouse = [];
		this._cronId = null;
		this.cronPeriod = data.cron || 4000; // period at which cron is launched
		this.maxUnrestrained = 20; // number of parallel nodes that can be sent data
		this.frpPort = 3;
		this.stopped = false;

        // data about registration operations, indexed by prefix
        this.registrationsOperations = {};
        // data about feed operations, indexed by prefix
        this.feedOperations = {};
		// fetch operations, indexed by their operation id
		this.fetchOperations = {};

		// data about current registrations, indexed by tid
		this.localRegistrations = {};
		this.foreignRegistrations = {};

		// list of busy nodes
		this._busyNodes = {};
		this._busyNodesArray = []; // array to be passed by reference :

		this._initConstants();
		this._initNodejs();
		this._initTemplates();
		if(this._metainterface) this.initInterface(this._metainterface);
		this.startCron();
	}
	
	////////////////////////////////////////////////////////////////////
	//////////////////      END USER API              //////////////////
	////////////////////////////////////////////////////////////////////
	
	// try to retrieve a resource either locally or remotely
	getResource(prefix, opts={}){
		let $this = this;
		if(!opts.timeout) opts.timeout = 4000;
		// search for the document locally

		let prom = this.getResourceLocal(prefix,opts).then(docBytes=>$this._frf.documentToObject(docBytes))
		// if not found, search for it remotely
		.catch(()=>$this.request(prefix, opts));
		let t1 = Date.now();
		return prom;
	}
	
	// establish connections to nodes in order to retrieve a specific resource
	request(hexKey, op={}){
		// keep the list of nodes that have been asked for the resource
		op.askedNodes = op.askedNodes || {};
		// keep the list of nodes that needs to be asked for the resource
		op.resourceNodes = op.resourceNodes || {};
		op.name = "getResource for prefix '" + hexKey + "'";
		op.thread = this._getResourceThread.bind(this);
		// build the target key from the prefix
		op.key = { prefix : hexKey, span : 0};
		if(op.dversion) op.key.dversion = op.dversion;
		if(!op.hasOwnProperty("check")) op.check = false; // check the validity (signature, expiration time...) of documents retrieved
        // build data for the reach operation
        op.reachOperation = {
            prefix : hexKey,
            // keep the list of nodes visited with reachNode request
            skipNodes : op.skipNodes || {},
            maxNodes : op.maxNodes || 1,
			resourceNodes : op.resourceNodes,
			askedNodes : op.askedNodes,
			withOwnNode : false,
			stats : {}
        }
		return this._threadops.startOperation(op);
	}

    /***
	 * check the recursive validity of a document :
	 * assume the stored parents are correctly signed
	 * if the parent cannot be found locally, send requests, asking first the node that provided the last document
	 * return a promise
     */
	checkDocumentValidity(op={}){
		let $this = this;
		// prevent the algorithm from looping indefinitely
		if(!op.hasOwnProperty("maxIterations")) op.maxIterations = 8;
		if(!op.maxIterations) return Promise.reject("too many iterations");
		else op.maxIterations--;
		// extract data from all given documents / signed documents
        let document = this._frf.extractDataFromSigned(op.document);
		if(!op.docData){
            op.docData = {};
            op.docData[document.key] = document;
            if(!op.parents) op.parents = [];
            for(let parent of op.parents){
                let parentData = this._frf.extractDataFromSigned(parent);
                op.docData[parentData.key] = parentData;
            }
		}
		// ensure the presence of all needed parents
		let nextDoc = document;
		while(!this._frf.isSKyRoot(nextDoc.key)){
            if(!nextDoc.prefix) return Promise.reject("non-root document without prefix property for key : "+nextDoc.key);
            let nextKey = nextDoc.key.slice(0, 2*nextDoc.prefix);
			if(op.docData[nextKey]){
				nextDoc = op.docData[nextKey];
				continue;
			}
			// else try to retrieve the document from local database before continuing
			return this.getResourceLocal(nextKey).then(function(newDoc){
                newDoc = $this._frf.documentToObject(newDoc);
                op.parents.push(newDoc);
                let newDocData = this._frf.extractDataFromSigned(newDoc);
                op.docData[newDocData.key] = newDocData;
                return $this.checkDocumentValidity(op);
			})
			// if the missing parent is not found locally, ask for it remotely
			.catch(function(){
				// if a node is provided for the operation, ask it in priority
				let resourceNodes = {};
				if(op.node) resourceNodes[op.node.id] = op.node;
				return $this.request(nextKey, {
                    resourceNodes : resourceNodes,
					maxConcurrentThreads : 1,
					timeout : op.timeout || 3000,
					check : true
				}).then(function(newDoc){
					op.parents.push(newDoc);
					let newDocData = $this._frf.extractDataFromSigned(newDoc);
					op.docData[newDocData.key] = newDocData;
					let documentBytes = $this._frf.documentToBytes(newDoc);
					$this.pushResourceLocal(newDocData.key, documentBytes);
                    return $this.checkDocumentValidity(op);
				})
				.catch(function(e){
					console.log("checkDocumentValidity error : parent not found : " + nextKey, e);
					return Promise.resolve(false);
				})
			})
		}
		// once all the parents have been gathered, process the answer
		let isCorrect = this._frf.checkDocument(op.document, op.parents);
		return Promise.resolve(isCorrect);
	}

    /*** return the list of registered keys
	 ***/
	getRegistrations(){
		return this.registrationsOperations.slice();
	}

	getResourceLocal(key, opts={}){
		let promises = this._warehouse.map( wh => wh.getResource(key, opts) );
		let raceOpts = {};
		if(opts.timeout) raceOpts.timeout = opts.timeout;
		let res = this.race(promises, raceOpts);
        return res;
	}

	// retrieve all available keys of all warehouses : the result is cached until keys are added/removed
	// option "force" : reload all the keys by asking again the warehouse
	// return a promise that resolves with an array of keys
    getResourceKeysLocal(key="", opts={}){
		if(!this._getKeysPromise || opts.force){
            let span = opts.span || 128;
            let promises = this._warehouse.map( wh => wh.availableResources(key, key+"ff".repeat(span), opts={}) );
            this._getKeysPromise = Promise.all(promises).then(function(promiseResults){
            	let keys = [].concat(...promiseResults);
            	let filteredKeys = [];
            	let pushedKeys = {}; // prefix => dversions
            	for(let key of keys){
            		if(!pushedKeys[key.prefix]) pushedKeys[key.prefix] = [];
            		if(!pushedKeys[key.prefix].includes(key.dversion)){
                        pushedKeys[key.prefix].push(key.dversion);
                        filteredKeys.push(key);
					}
				}
				return filteredKeys;
			});
		}
		return this._getKeysPromise;
    }

    // reset the cached local keys
	resetCachedKeys(){
		if(this._getKeysPromise) delete this._getKeysPromise;
	}

	hasResourceLocal(key, opts={}){
		let promises = this._warehouse.map( wh => wh.hasResource(key, opts) );
		return Promise.all(promises).then(arr=> arr.includes(true))
	}

	deleteResourceLocal(key, opts={}){
		this.resetCachedKeys();
        let promises = this._warehouse.map( wh => wh.deleteResource(key, opts) );
        return Promise.all(promises).then(arr=> arr.includes(true))
	}
	
	pushResourceLocal(key, resource, opts={}){
		this.resetCachedKeys();
		let promises = this._warehouse.map( wh => wh.storeResource(key, resource, opts) );
		return this.race(promises);
	}

	// stop every operations of the frp
	stop(){
		for(let prefix in this.registrationsOperations){
			this.unregister(prefix);
		}
		for(let id in this.fetchOperations){
			this._threadops.resolveOperation(id);
			delete this.fetchOperations[id];
		}
		for(let warehouse of this._warehouse){
			warehouse.stop();
		}
		if(this._flock) this._flock.stop();
        this.stopped = true;
	}

	////////////////////////////////////////////////////////////////////
	//////////////          THREADOPS HELPERS          /////////////////
	////////////////////////////////////////////////////////////////////

	/*** perform a register operation, that retrieve notifications from targeted node
	 *   in parallel, perform a fetch operation that retrieves document from a list of prefix
	***/
	feed(op){
		if(!op.prefix) throw "cannot feed without a defined 'prefix' parameter";
		if(op.prefix in this.feedOperations) throw "prefix : "+op.prefix+" already beeing feeded in another operation";
		if(!op.stats) op.stats = {};
		let $this = this;
		op.key = {
			prefix : op.prefix,
			span : op.hasOwnProperty("span") ? op.span : 100
		}
		// keep track of keys that need to be asked : key => [nodes responsible of it]
		op.nextRequestedKeys = {};
		// keep track of the keys that should not be asked again
		op.skipKeys = [];
		if(!op.span) op.span = 0;
		op.name = "feed operation for prefix : "+op.prefix+" and span "+op.span;

		// start a fetch operation to retrieve documents
		op.fetchOperation = {
            maxConcurrentThreads : op.maxConcurrentThreads || 5,
			targets : {},
            sleepTime : op.sleepTime || 2000,
			check : !!op.check
		}

		op.fetchOperation.ondocument = function(doc, prefix){
			let checkPromise = !op.check ? Promise.resolve(true) : $this.checkDocumentValidity({document : doc});
			checkPromise.then(function(isValid){
				if(!isValid) return
                let byteDoc =  $this._frf.documentToBytes(doc)
                let opts = {
                    dversion : doc.dversion || 0
                }
                $this.pushResourceLocal(prefix, byteDoc, opts);
			})
		}
		this.fetch(op.fetchOperation);

		// start a register operation :
		op.registerOperation = {
			span : op.key.span,
			retro : op.hasOwnProperty("retro") ? !!op.retro : true
		}
		op.registerOperation.onNotify = function(data){
			$this.getResourceKeysLocal().then(function(localKeys){
				// create the list of keys already stored
				let alreadyStoredKeys = {};
				for(let key of localKeys){
                    alreadyStoredKeys[key.prefix] = true;
				}
                for(let key of data.keys){
                    let prefix = key.prefix;
                    if(alreadyStoredKeys[prefix]) continue;
                    // check that the notified key is included in the requested key
                    if(!$this._flock.isIncluded({prefix : key, span : 0}, op.key)) continue;
                    if(!op.fetchOperation.targets[prefix]) op.fetchOperation.targets[prefix] = [];
                    if(!op.fetchOperation.targets[prefix].includes(data.node)) op.fetchOperation.targets[prefix].push(data.node);
                }
			})
		}
		this.register(op.prefix, op.registerOperation);
		this._flock.addKey(op.key);
		this.feedOperations[op.prefix] = op;
	}

    /***
	 * stop a feed operation
     */
    unfeed(prefix){
		let op = this.feedOperations[prefix];
    	if(!op) return;
    	this._threadops.resolveOperation(op.fetchOperation.id);
    	this.unregister(op.prefix);
    	this._flock.removeKey(op.key);
	}

    /***
     * for a given feed prefix key, update the current status of the feed (in term of connected nodes...)
     */
	updateFeedStatus(prefix){
		let op = this.feedOperations[prefix];
		if(!op) return;

		let connectedNodes = this._flock.getConnectedNodes();
		let feedingNodes = [];
		for(let node of connectedNodes){
			for(let key of node.keys||[]){
				if(this._flock.isIncluded(op.key, key)){
					feedingNodes.push(node);
					break;
				}
			}
		}
        if(!op.stats) op.stats = {};
        let stats = this._prepareCurrentPeriod(op.stats);
        stats.connectedNodes = connectedNodes.length;
        stats.feedingNodes = feedingNodes.length;
	}

    /***
	 * for a given prefix key, notify of a new stats to be agregated. Possible keys are :
	 * - requests : number of requests processed
	 * - bandwidth : amount of data transmitted
     */
	updateOperationStats(prefix, data={}){

        let op = this.feedOperations[prefix];
        if(!op) return;
		if(!op.stats) op.stats = {};
        // create the data about the current minute
        let stats = this._prepareCurrentPeriod(op.stats);
        if(data.requests) stats.requests = (stats.requests || 0) + data.requests;
        if(data.bandwidth) stats.bandwidth = (stats.bandwidth || 0) + data.bandwidth;

        return data;
	}

	// compute the minute of a given time
	minute(t=Date.now()){
		return Math.floor(Date.now() / 60000);
	}

	// prepare an object for the current period in a stats object
	_prepareCurrentPeriod(stats={}){
        let currentMinute = this.minute();
        if(!stats[currentMinute]) stats[currentMinute]  = {};
        return stats[currentMinute];
	}

	// retrieve the feed operation a given key belong to
	retrieveFeedOperation(key){
		if(typeof key === "string") key = {prefix : key, span : 0};
		for(let feedPrefix in this.feedOperations){
			let op = this.feedOperations[feedPrefix];
			if(this._flock.isIncluded(key, op.key)) return feedPrefix;
		}
	}

	// retrieve documents given an object { prefix=>[nodes to contact] }
	// pause when no prefix needs to be fetched
	fetch(op){
    	// list of targets=>nodes to be used for requests
		if(!op.targets) op.targets = {};
    	// list of prefix that are currently beeing fetched by a thread
		op.pendingPrefix = {};
		// if(op.prefix){
		// 	for(let i in op.prefix) op.pendingPrefix[i] = op.prefix[i];
		// }
		// by default, run the operation indefinitely, and sleep when no new target are available
        if(!op.hasOwnProperty("maxThreads")) op.maxThreads = +Infinity;
        // timeout for  nodeRequest (request of a single document to a single node)
		if(!op.requestTimeout) op.requestTimeout = 15000;
        // when no action is to be done, sleep before executing the next thread
        if(!op.hasOwnProperty("sleepTime")) op.sleepTime = 2000;
        op.thread = this._fetchThread.bind(this);
        op.name = "fetch operation";
        this._threadops.startOperation(op);
		this.fetchOperations[op.id] = op;
    }

	_fetchThread(op){
    	let $this = this;
    	// determine the next key to reach
		let prefixTarget = null;
		for(let prefix in op.targets){
			if(prefix in op.pendingPrefix) continue;
			let isBlocked = false; // check if another key which impacts the same documents is beeing processed
			for(let pendingPrefix in op.pendingPrefix){
				if(pendingPrefix.slice(0,64) === prefix.slice(0,64)) isBlocked = true;
			}
			if(isBlocked) continue;
			prefixTarget = prefix;
			break;
		}
		// if no target are available, sleep this thread
		if(prefixTarget === null){
			return this._threadops.sleep(op.sleepTime);
		}

        // move the key to fetch in the pending keys
        op.pendingPrefix[prefixTarget] = op.targets[prefixTarget];
        delete op.targets[prefixTarget];

		// create a small aux method to push back or remove completely the prefixTarget, once the document has been retrieved
		let removeTarget = function(){
			if(op.pendingPrefix[prefixTarget]) delete op.pendingPrefix[prefixTarget];
		}
		let pushBackTarget = function(){
			if(op.pendingPrefix[prefixTarget]){
				if(op.pendingPrefix[prefixTarget].length) {
                    op.targets[prefixTarget] = (op.targets[prefixTarget] || []).concat( op.pendingPrefix[prefixTarget] );
				}
                delete op.pendingPrefix[prefixTarget];
			}
		}

		let targetNode = op.pendingPrefix[prefixTarget].pop();
		let prom = this.nodeRequest(targetNode, { key : {prefix : prefixTarget}, timeout : op.timeout });
		return prom.then(function(trAnswer){
            if(!trAnswer.document) return Promise.reject();
            // TODO  : check the document validity
            // store the document
            if(op.ondocument){
            	op.ondocument(trAnswer.document, prefixTarget, targetNode);
			}
            return resultPromise;
		})
		.catch(pushBackTarget)
	}

    /***
	 * stop registration on a given key
     */
    unregister(prefix){
        let regop = this.registrationsOperations[prefix];
        if(!regop) return;
        for(let rid in regop.nodeRegistrations){
            let tid = regop.nodeRegistrations[rid].tid;
            this.cancelLocalRegistration(tid);
        }
        regop.nodeRegistrations = {};
        this._threadops.rejectOperation( regop.id, "stopped operation" );
        delete this.registrationsOperations[prefix];
    }

    /*** reach appropriate nodes to send register request
     * expect the following data :
     * key : the key to register
     * maxNodes : number of nodes to reach
     * onNotify : callback for notify messages
     */
    register(hexKey, op={}){
        if(this.registrationsOperations[hexKey]) throw "The key "+ hexKey +" is already registered";
        else this.registrationsOperations[hexKey] = op;
        let $this = this;
        op.id = this._flaptools.randomHex(16);
        op.hexKey = hexKey;
		op.byteKey = this._flaptools.hexToBytes(hexKey);
        op.name = "register for prefix '" + hexKey + "'";
        op.key = { prefix : hexKey, span : op.span || 0};
        // minimum time to wait between two register threads
        if(!op.hasOwnProperty("transitionTime")) op.transitionTime = 1000;
        // keep the list of nodes that have been asked for the resource and should not be asked again
        op.askedNodes = op.askedNodes || {};
        // keep track of register instructions that havce been sent to a given node
		op.nodeRegistrations = {};
		if(!op.maxNodes) op.maxNodes = 4;

		// the register operation rely on a populate operation (from the Flock) that is launched regularly with a small timeout
		op.populateOperation = {
			timeout : op.populateTimeout || 3000,
			maxThreads : op.maxThreads || 100,
		};

        // build the formation to reach
		if(!op.formation){
            op.formation = new this._flock._Formation(op.byteKey, {
				buckets : {[4*hexKey.length] : op.maxNodes}, // number of nodes with the targeted key to reach
				beforeKey : 0,
				afterKey : 0
			});
		}
		op.populateOperation.formation = op.formation;

		// helper to make the main thread stops and restart it after some time
		op.stopAndRestart = function(){
			$this._threadops.resolveOperation(op.id);
			if(op.mainRegisterThread) delete op.mainRegisterThread;
			op.sleepAndRestart();
		}

		// helper to restart the operation after some time
		op.sleepAndRestart = function(){
            let sleepTime = Math.max(0, op.transitionTime - (Date.now() - op.startTime) );
			setTimeout(function(){
				// try to restart the thread if there isn't any
                op.startRegisterThread()
			}, sleepTime)
		}

		// helper to build callback for when registrations are canceled remotely
        // create a callback for nodeRegister cancels
        op.createRegisterCancelCallback = function(registerId){
            return function(){
                if(!(registerId in op.nodeRegistrations)) return;
                let node = op.nodeRegistrations[registerId].node;
                let nodeId = node.id;
                // make it possible to ask the node that triggered the registration cancel
                if(node._registrationOps && node._registrationOps.includes(op.id) ){
                	node._registrationOps.splice(node._registrationOps.indexOf(op.id),1 );
                }
                delete op.nodeRegistrations[registerId];
                // resume the operation if it was stopped
                op.sleepAndRestart();
            }
        }
		// build populate callback
        op.populateOperation.onpopulate = function(node, bucket){
        	// if the operation has been removed, don't process anything
			if(!$this.registrationsOperations[hexKey]) return;
            // remove the node from the list to nodes to be processed
			let nid = node.id;

			// if a node is already notifying the frp, dont send another request
			if(!node._registrationOps) node._registrationOps = [];
            if(node._registrationOps.includes(op.id)){
                return;
			}
            node._registrationOps.push(op.id);

            // ask this particular node for the resource notification
			let registerRandomId = $this._flaptools.randomHex(16);
            let registerData = {
                keys : [op.key],
                retro : !!op.retro,
                onNotify : op.onNotify,
                // by default, use a cancel callback that redo a register operation
                onCancel : op.onCancel || op.createRegisterCancelCallback(node.id)
            };
            let registerTid = $this.nodeRegister(node, registerData);
            op.nodeRegistrations[node.id] = {
                tid : registerTid,
                node : node
            };

			// stop the populate operation if enough nodes have been reached
            if(Object.keys(op.nodeRegistrations) >= op.maxNodes){
                op.stopAndRestart();
            }
        }
		
        // ensure the register operation should be performed
        op.isOperationRunning = function(){
			return !!$this.registrationsOperations[hexKey]
		}

        // method to start the main register thread
        // start/restart the register thread for a given key, but don't start a new one if there is already one active
        op.startRegisterThread = function(){
			if(!op.isOperationRunning() ) return;
            if(!op || op.mainRegisterThread) return;
            op.startTime = Date.now();
            // reset the list of busy nodes to not call
			op.populateOperation.skipNodes = $this.busyNodes()
            // once the operation is completed, sleep and restart
            op.mainRegisterThread = $this._flock.populateFormation(op.populateOperation).then(op.stopAndRestart).catch(op.stopAndRestart);
            return op.mainRegisterThread;
        }

        op.startRegisterThread();
    }

	_getResourceThread(op){
		if(this.stopped){
			this._threadops.rejectOperation(op.id);
			return Promise.resolve()
        }
		if(op.done) return Promise.resolve();
		// uniformize the number of threads already launched
		let totalThreads = Math.max(op.reachOperation||0, op.reachOperation.totalThreads||0);
		op.totalThreads = totalThreads;
		op.reachOperation.totalThreads = totalThreads;
		// if there are nodes stored in the rightNodes array, test those nodes to ask for the resource
		if(Object.keys(op.resourceNodes).length) return this._getResourceAskThread(op);
		// else, try to reach appropriates nodes
		else{
			op.reachOperation.skipNodes = this.saturatedNodes();
            return this._reachNodeThread(op.reachOperation);
		}
	}

	// start a thread to reach the appropriate node
    _reachNodeThread( op ){
		var $this = this;
		// find a node that might contain the resource
		return this._flock.reachNode( op ).then(function(nodes){
			let nodeWithResource = null;
			for(let node of nodes){
				if(node.id && !(op.askedNodes[node.id] || op.resourceNodes[node.id])){
					op.resourceNodes[node.id] = node;
                    op.skipNodes[node.id] = true;
                }
			}
			if(!Object.keys(op.resourceNodes).length) return $this._threadops.sleep();
			else return Promise.resolve();
		})
	}

	// return the list of node (nid=>nid) that are ongoing too many operations
	saturatedNodes(){
    	let res = {};
    	let targeted = this.targetedNodes();
    	for(let nid in targeted){
    		if(this._targeting[nid] > this.MAX_TARGETING_PER_NODE) res[nid] = nid
		}
		return res;
	}

	// indicate that given nid is undergoing an operation
	targetNode(nid, cnt=1) {
        if(!this._targeting) this._targeting = {};
        if (!this._targeting[nid]) this._targeting[nid] = 0;
        this._targeting[nid] += cnt;
        if(this._targeting[nid] <= 0) delete this._targeting[nid];
    }

    // indicate that a node is no longer under targeting
	untargetNode(nid, cnt=1){
        this.targetNode(nid, -cnt);
	}

	// return the targeted nodes
	targetedNodes(){
    	if(!this._targeting) this._targeting = {};
    	return this._targeting;
	}

	// start a thread to ask appropriate nodes for the resource
	_getResourceAskThread(op){
    	let $this = this;
		// retrieve a node to ask for the resource
		let nid = Object.keys(op.resourceNodes)[0];
		let node = op.resourceNodes[nid];
		// remove the node from the list to nodes to be processed
		delete op.resourceNodes[nid];
		op.askedNodes[nid] = true;
		let threadops = this._threadops;
		// ask this particular node for the resource
		return this.nodeRequest(node, { key : op.key} ).then(function(trAnswer){
			if(!trAnswer.document) return;
			if(op.check){
				let prom = $this.checkDocumentValidity({
					document : trAnswer.document
				}).then(function(isValid){
					if(isValid){
						threadops.resolveOperation(op.id, trAnswer.document);
					}
				})
				return prom;
			}
            threadops.resolveOperation(op.id, trAnswer.document);
		})
		.catch(function(){});
	}

    ////////////////////////////////////////////////////////////////////
    //////////////       FILE CUTTING METHODS          /////////////////
    ////////////////////////////////////////////////////////////////////

    /** given a file object (DOM elemnt for browser or path string for nodejs), cut it and transform it into frp document(s)
	 * for browser, file can be get with document.getElementById("id").files[0]
     */
	cutAndStore(file, opts={}){
		let $this = this;

		// build properties to cut the file
        opts.chunkSize = opts.chunkSize || +Infinity;
        opts.oneChunkLimit = opts.oneChunkLimit || opts.chunkSize;
        // prefix of the key of the new document to create
		opts.prefix = opts.prefix || "";
        // keys to sign the top document
        opts.keys = opts.keys || this._frf.createKeys();
		// new signtools to append to the document : the document will be considered a subSky
        opts.signtools = opts.signtools || null;

		// promise result
		let prom = new Promise(r=>opts.resolver=r);

		// for nodejs, file is a path string
		if(typeof file === "string"){
			opts.name = path.basename(file);
			let fileStats = fs.statSync(file);
			opts.size = fileStats.size;
			let mimeType = this._mime.getExtension(file);
			let mimeParts = mimeType ? mimeType.split("/") : [];
            opts.type = mimeParts[0] || "application";
            if(mimeParts[1]) opts.subtype = mimeParts[1]|| "octet-stream";

            this._fs.open('file', 'r', function(status, fd) {
                opts.getFilePart = function(start, length){
                    length = Math.max( 0, Math.min( length, opts.size - start));
                    let buffer = Buffer.alloc(length);
                    let filePartResolver;
                    let promise = new Promise(r=>filePartResolver=r);
                    fs.read(fd, buffer, 0, length, start, function(err, num) {
                        filePartResolver( new Uint8Array(buffer) );
                    });

                    return promise;
                }
                $this._cutAndStoreAux(opts);
            });
		}
		// for TU, provide Uint8Array directly
		else if(file instanceof Uint8Array){
            if(!opts.name) opts.name = "noname";
            if(!opts.type){
            	opts.type = "application";
            	opts.subtype = "octet-stream";
            }
            opts.size = file.length;
            opts.getFilePart = function(start, length){
            	let end = Math.min(file.length, start + length);
            	return Promise.resolve(file.slice(start,end));
            }
            setTimeout(function(){
                $this._cutAndStoreAux(opts);
			},0)
		}
		// for browser, file is  a File object
		else{
			opts.name = file.name.toString();
            opts.size = parseInt(file.size);
            document.opts = opts
            let mimeParts = file.type.split("/") || [];
            opts.type = mimeParts[0] || "application";
            if(mimeParts[1]) opts.subtype = mimeParts[1]|| "octet-stream";
            var reader = new FileReader();
            reader.onload = function() {
                var arrayBuffer = this.result;
                opts.getFilePart = function(start, length){
                	length = Math.max( 0, Math.min( length, arrayBuffer.byteLength - start));
                	return Promise.resolve( new Uint8Array(arrayBuffer, start, length) );
				}
				$this._cutAndStoreAux(opts);
            }
            reader.readAsArrayBuffer(file);
		}
		return prom;
    }

    /***
	 * cut and store a file, given a method that can return Uint8Arrays part of the file
	 * resolve by calling data.resolve() once it is done
     */
	_cutAndStoreAux(data){
		let $this = this;

		// aux function to trigger a promise that retrieve a part of the file and create a chunk with it
		let pushNextChunk = function(){
			if(fileIndex >= data.size){
                data.resolver(signed_mainDoc);
                return;
            }
            return data.getFilePart(fileIndex, data.chunkSize).then(function(filePart){
                fileIndex += data.chunkSize;
                data.storedFileSize = Math.min(data.size, fileIndex); // indicate the length that have been stored after this step
            	let prefixKey = data.key;
            	let subKeyBytes = $this._flaptools.intToBytes(nextSubKey++);
            	let subKeyhex = $this._flaptools.bytesToHex(subKeyBytes);
            	let key = prefixKey + subKeyhex;
                let doc = {
                	key : key,
					prefix : prefixKey.length/2,
					body : filePart
				}
				let signingKeys = data.signtools || data.keys;
				let signedDoc = $this._frf.createSignedDocument(doc, signingKeys);
                let documentBytes = $this._frf.documentToBytes(signedDoc);
                return $this.pushResourceLocal(key, documentBytes).then(pushNextChunk)
			})
		}

        // prepare the main document
        let mainDoc = {
            type : data.type || "bytes",
			subtype : data.subtype,
            name : data.name,
        }
        if(data.prefix) mainDoc.prefix = data.prefix;
        if(data.signtools) mainDoc.signtools = data.signtools;
        if(data.author) mainDoc.author = data.author;
        if(data.expire) mainDoc.expire = data.expire;
        if(data.wingspan) mainDoc.wingspan = data.wingspan;
        if(data.hasOwnProperty("dversion")) mainDoc.dversion = data.dversion;
        if(data.time) mainDoc.time = data.time;
        if(data.nextfeather) mainDoc.nextfeather = data.nextfeather;
        if(data.featherlength) mainDoc.featherlength = data.featherlength;
        if(data.winglength) mainDoc.winglength = data.winglength;
        if(data.keystart) mainDoc.keystart = data.keystart;
		if(data.key) mainDoc.key = data.key;

        let headChunkOnly = data.size <= data.oneChunkLimit;
		if(headChunkOnly){}
		else{
			mainDoc.winglength = data.size;
            mainDoc.wingspan = this._flaptools.bytesToHex( this._flaptools.intToBytes( Math.floor(data.size / data.chunkSize) ) );
            mainDoc.featherlength = data.chunkSize;
		}
        let fileIndex = headChunkOnly ? data.size : 0;
        let nextSubKey = 0;
        let bodyPromise = headChunkOnly ? data.getFilePart(0, data.size) : Promise.resolve();
        let isSkyRoot = !data.prefix;
		if(isSkyRoot) mainDoc = this._frf.createSky(mainDoc, data.keys);
		let signed_mainDoc;
        return bodyPromise.then(function(body){
        	if(headChunkOnly) mainDoc.body = body;
            signed_mainDoc = $this._frf.createSignedDocument(mainDoc, data.keys, isSkyRoot, data.keystart);
            let documentBytes = $this._frf.documentToBytes(signed_mainDoc);
            data.storedFileSize = fileIndex; // indicate the length that have been stored after this step
			data.key = signed_mainDoc.key || mainDoc.key;
            return $this.pushResourceLocal(data.key, documentBytes)
			.then(function(){
				// first callback when head is pushed
				return pushNextChunk();
			});
		})
	}

	////////////////////////////////////////////////////////////////////
	//////////////               WAREHOUSE             /////////////////
	////////////////////////////////////////////////////////////////////
	
	// warehouse are instances that can provide/store resources
	
	addWarehouse(w){
    	if(this.hasWarehouse(w)) return;
		this._warehouse.push(w);
        // create an id for warehouse watch
        if(!this._whWatchId) this._whWatchId = this.createTid(64);
        // add a watch event to redirect to the notify events
		w.watch({
			callback : this._whWatchEvent.bind(this),
			id : this._whWatchId
		})
    }
	
	removeWarehouse(w){
		let i = this._warehouse.indexOf(w);
		if(i === -1 ) return;
		this._warehouse.splice(i,1);
		// stop watching for new keys in the warehouse
		w.unwatch(this._whWatchId);
	}
	
	getWarehouses(){
		return this._warehouse.slice();
	}

	// check if a warehouse is already in frp
	hasWarehouse(w){
        return this._warehouse.indexOf(w) !== -1;
	}

	// watch event for warehouse
	_whWatchEvent(keys){
		for(let key of keys){
			this.triggerForeignNotify(key);
		}
	}

	////////////////////////////////////////////////////////////////////
	///////////////////// CRON AND REGISTRATIONS   /////////////////////
	////////////////////////////////////////////////////////////////////
	
	// method to start/stop the cron
	startCron(period=null){
		if(!period) period = this.cronPeriod;
		if(this._cronId !== null) return;
		this.cron_id = setInterval(this.cron.bind(this), period);
	}
	
	stopCron(){
		if(this._cronId === null) return;
		clearInterval(this._cronId);
		this._cronId = null;
	}
	
	// cron method to update internal frp state
	cron(){
    	let now = Date.now();
		for(let nid in this.busyNodes){
			if(this.busyNodes[nid] < now) delete this.busyNodes[nid];
		}
	}

	////////////////////////////////////////////////////////////////////
	///////////////////   NODE MANAGEMENT             //////////////////
	////////////////////////////////////////////////////////////////////
	
    // check if a node is  busy and should not be asked data
    isBusy(nid, busyUntil=null) {
    	if(busyUntil){
    		this._busyNodes[nid] = busyUntil;
        }
        return (this._busyNodes[nid] && this._busyNodes[nid] > Date.now())
    }

    // retrieve the list of busy nodes
	busyNodes(){
		this._rebuildBusyNodes();
		let res = {};
		for(let i in this._busyNodes) res[i] = true;
		return res;
	}

	_rebuildBusyNodes(){
    	this._busyNodesArray.length = 0;
        for(let nid in this._busyNodes){
            if(!this.isBusy(nid)) delete this._busyNodes[nid];
        }
	}

	// // add a node
	// addNode(n){
	// 	this._nodes.push(n);
	// }
	//
	// removeNode(n){
	// 	let i = this._nodes.indexOf(n);
	// 	if(i!==-1) n.splice(i,1);
	// }

	// retrieve the list of unrestrained nodes
	currentUnrestrainedNodes(){
		return this._nodes.filter(n=>!n.restrained);
	}
	
	// check if there is enough room to set a node to a given state
	canUnrestrain(){
		return this.currentUnrestrainedNodes().length < this.maxUnrestrained;
	}
	
	// remove dead nodes whose interface is closed
	clenDeadNodes(){
		this._nodes = this._nodes.filter(function(n){
			return n.interface.status() === n.interface.RUNNING;
		});
	}
	
	/////////////////////////////////////////////////////////////////////
	///////////////    REGISTER / NOTIFY METHODS     ////////////////////
	/////////////////////////////////////////////////////////////////////
	
	// add a registration, given the transaction data
	addLocalRegistration(data, node, local=true){
		let tid = data.tid;
		this.localRegistrations[tid] = {
			tid : tid,
			keys : data.keys || [],
			node : node,
			onNotify : data.onNotify,
			onCancel : data.onCancel || null
		};
	}
	addForeignRegistration(data, node, callback){
		let tid = data.tid || Math.floor(Math.random() * 1000000000);
		this.foreignRegistrations[tid] = {
			tid : tid,
			keys : data.keys || [],
			node : node,
			callback : callback
		};
		return tid;
	}
	
	// remove a registration, given a tid
	removeLocalRegistration(tid){
		if(this.localRegistrations[tid]) delete this.localRegistrations[tid];
	}
	removeForeignRegistration(tid){
		if(this.foreignRegistrations[tid]) delete this.foreignRegistrations[tid];
	}
	
	// retrieve the list of registations that match a given key
	triggeredRegistrations(key, registrations){
		let res = [];
		for(let tid in registrations){
			let reg = registrations[tid];
			if( this.isKeyRegistered(key, reg) ) res.push(reg);
		}
		return res;
	}
	
	// check if a key is included in a given registration
	isKeyRegistered(key, registration){
		let rkeys = registration.keys || [];
		for(let rkey of rkeys){
			let prefixes = this.buildKeys(rkey.prefix, rkey.suffixes || [] );
			// add a key limited to the prefix
			prefixes.push(rkey.prefix || "");
			for(let prefix of prefixes){
				if( this._flock.isIncluded( key, {prefix:prefix, span : rkey.span||0} ) ) return true;
			}
		}
		// by default, no key match, return false
		return false;
	}

	// perform a retroactive notification about keys that are already available
	retroNotify(node, reg){
		let res = [];
		// for each key, retrieve local resources keys
		let retrieveKeyPromises = [];
		for(let key of reg.keys){
			let opts = key.hasOwnProperty("dversion") ? {dversion : key.dversion} : {};
            retrieveKeyPromises.push( this.getResourceKeysLocal(key.prefix, opts) );
		}
		let $this = this;
		Promise.all(retrieveKeyPromises).then(function(promiseResults){
			let resultKeys = [].concat(...promiseResults);
			if(!resultKeys.length) return;
            let data = {
                tid : reg.tid,
                keys : resultKeys,
            };
            $this.nodeNotify(node, data);
		});
	}

	// filter keys uniquely and factorize them with a list of given factors
	filterAndFactorize(keys, prefixFactors) {
		// build a list of keys per version and prefix
    	let factorizedKeys = {};
    	for(let prefix of prefixFactors) factorizedKeys[prefix] = {};

    	for(let key of keys){
    		for(let prefix in factorizedKeys){
    			if(!key.prefix.startsWith(prefix)) continue;
    			let version = key.dversion || 0;
    			let suffix = key.prefix.substr(prefix.length);
    			if( !factorizedKeys[prefix][version] ) factorizedKeys[prefix][version] = {};
                factorizedKeys[prefix][version][suffix] = suffix;
    			break;
			}
		}
		// compute the result
		let result = [];
    	for(let prefix in factorizedKeys){
    		for(let version in factorizedKeys[prefix]) {
                result.push({prefix: prefix, dversion: version, suffixes: Object.keys(factorizedKeys[prefix][version])});
            }
		}
		return result;
	}

	// send notifications to all registred nodes triggered by a key
	triggerForeignNotify(key){
		// retrieve the list of registration trigger by the key

		let registrations = this.triggeredRegistrations(key, this.foreignRegistrations);
		for( let reg of registrations){
			let data = {
				tid : reg.tid,
				keys : [key],
			};
			if(reg.node) this.nodeNotify(reg.node, data);
			if(reg.callback) reg.callback(data);
		}
	}
	
	// perform execution of callbacks of all local triggered registrations
	triggerLocalNotify(tid, keys, node){
		// retrieve the registration data
		let reg = this.localRegistrations[tid];
		if(!reg) return; 
		// build the data that will be given as callback
		let data = {
			tid : tid,
			keys : keys,
			node : node // the node that emitted the notification
		}
		reg.onNotify(data);
	}
	
	// cancel a registration emitted by this frp, and send a cancel message to the matching node
	cancelLocalRegistration(tid){
		this._cancelRegistration(tid, this.localRegistrations);
	}
	
	// cancel a foreign notification and send a cancel message to the remote node
	cancelForeignRegistration(tid){
		this._cancelRegistration(tid, this.foreignRegistrations);
	}
	
	// auxiliar function to cancel notification
	_cancelRegistration(tid, registrations){
		let reg = registrations[tid];
		if(!reg) return;
		delete registrations[tid];
		this.nodeCancel( reg.node, {tid : reg.tid});
	}

	/////////////////////////////////////////////////////////////////////
	//////////////////////    TRANSACTIONS           ////////////////////
	/////////////////////////////////////////////////////////////////////
	
	// request a resource on a given node
	nodeRequest(node, opts={}){
		let tr = this._prepareTransaction(node);
		
		// add the transation to watch for an answer
		this.addTransaction(tr);
		
		// send the request message
		let msg = {
			tid : tr.tid,
			key : opts.key,
			type : "request"
		}
		this.send(node, msg);

		let result = tr.promise;
		if(opts.timeout){
			result = this.race([result], {timeout : opts.timeout});
		}

		let $this = this;
		// target the node, untarget it when the request is over
        this.targetNode(node.id);
        let callback = function(){ $this.untargetNode(node.id)}
        result.then(callback).catch(callback);

		return tr.promise;
	}
	
	// register to a foreign node
	nodeRegister(node, data={}){
		let tr = this._prepareTransaction(node);
		
		// ensure a callback is provided for incoming notifications
		if(data.onNotify) tr.onNotify = data.onNotify;
		else throw "Mandatory callback for register operation was not provided under key 'onNotify'";
		// add facultative callback for cancel
		if(data.onCancel) tr.onCancel = data.onCancel;

		
		// add the transation to watch for an answer
		this.addTransaction(tr);
		
		// store the transaction as a local registration
		this.addLocalRegistration(tr, node);
		
		// send the request message
		let msg = {
			tid : tr.tid,
			keys : data.keys,
			type : "register",
			retro : !!data.retro
		}
		this.send(node, msg);
		
		return tr.tid;
	}
	
	// send a notification about a key to a given node
	nodeNotify(node, data={}){
		let msg = {
			tid : data.tid,
			keys : data.keys,
			type : "notify"
		}
		this.send(node, msg);
	}
	
	// send a cancel message to a node
	nodeCancel(node, data = {}){
		let msg = {
			tid : data.tid, 
			type : "cancel"
		};
		this.send(node, msg);
	}
	
	// prepare a transaction object, creating an object with the following :
	// promise : a promise that resolve or reject once the transaction answer is received
	// reject : function to reject the transaction promise
	// resolve : function to reject the transaction promise
	// node : the node targeted by the transaction
	// tid : a new local tid for the transaction
	_prepareTransaction(node, opts){
		let transaction = {
			tid : this.createTid(),
			node : node,
		};
		let prom = new Promise((res,rej)=>{
			transaction.resolve = res;
			transaction.reject = rej;
		});
		transaction.promise = prom;
		return transaction;
	}
	
	// build the answer of a request
	_processRequest(rq, operationData={}){
		let opts = {
			timeout : 2000, // timeout to prevent warehouse from freezing forever
		};
		let $this = this;
		if("dversion" in rq.key) opts.dversion = rq.key.dversion;
		let answer = {
			type : "answer",
			tid : rq.tid
		};
		// retrieve the list of feeding keys that are concerned y the request
        operationData.operationKey = this.retrieveFeedOperation(rq.key.prefix || "");
        operationData.requests = (operationData.requests || 0) + 1; // notify that one requests has been processed under the feedKey span

		// retrieve the resource and then send it if available
		return this.getResourceLocal(rq.key.prefix, opts).then(function(doc){
			answer.status = $this.STATUS_ANSWERED;
			answer.document = $this._frf.documentToObject(doc);
			return answer;
		})
		.catch(function(e){
			answer.status = $this.STATUS_NOT_FOUND;
			return answer;
		});
	}
	
	// process an answer message
	_processAnswer(answer, event){
		// check that the transaction id match an existing one
		let tid = answer.tid;
		let tr = this.getTransaction(tid);
		if(!tr) return;
		tr.resolve(answer);
		this.removeTransaction(tid);
	}
	
	// process a registration message
	_processRegister(tr, event){
		let node = this._flock.nodeFromInterface(event.interface);
		this.addForeignRegistration(tr, node);
		// if asked for, perform a retro notification with documents already available
        if(tr.retro) this.retroNotify(node, tr);
	}
	
	// build the answer of a notify 
	_processNotify(tr, event){
		let node = this._flock.nodeFromInterface(event.interface);
		this.triggerLocalNotify(tr.tid, tr.keys, node);
	}
	
	// process a cancel message
	_processCancel(tr, ev){
		let tid = tr.tid;
		// check if the cancel is associated to a local registration
		if(this.localRegistrations[tid]){
            let cancel = this.localRegistrations[tid].onCancel;
            delete this.localRegistrations[tid];
            if(cancel) cancel();
		}
		// check if the cancel is associated to a foreign registration
		else if(this.foreignRegistrations[tid]){
			delete this.foreignRegistrations[tid];
		}
	}
	
	// process a busy
	_processBusy(tr, event){
    	// retrieve the node from the interface
		let nodeInterface = event.interface;
		let node = this._flock.nodeFromInterface(nodeInterface);
		if(!node ) return;
		let nid = node.id;
        if(!nid) return;
		let busyTime = tr.time || 0;

		this.isBusy(nid, busyTime + Date.now() );
    }
	
	// send a busy message : the frp should not be called by its target for the given amount of time
	sendBusy(node, busyTime = 10000){
		let msg = {
			type : "busy",
			time : busyTime
		}
		this.send(node, msg);
	}
	
	// send a message to a node
	send(node, msg){
		let bytes = this.messageToBytes(msg);
		var $this = this;
		this._flock.connectNode(node).then(function(nodeInterface){
			if($this.stopped) return;
			nodeInterface.send(bytes, $this.frpPort, $this.frpPort);
		});
	}
	
	processTransaction(event){
		// extract the transaction
		let tr = this.messageToObject(event.message);
		// toggle the tid to transform it according the this node perspective
		// tr.tid = this.toggleTid(tr.tid);
		let answerPromise;

        // create an object to store the operation to update (increment the bandwidth used)
		let operationData = {};

		try{
			switch(tr.type){
				case "request" :
					answerPromise = this._processRequest(tr, operationData);
					break;
				case "answer" :
					this._processAnswer(tr);
					break;
				case "register" :
					this._processRegister(tr, event);
					break;	
				case "notify" :
					this._processNotify(tr, event);
					break;
				case "cancel" : 
					this._processCancel(tr,event);
					break;
				case "busy" :
					this._processBusy(tr, event);
					break;
				default : break;
			}
			
			// send answer when there is one
			if(!answerPromise) return;
			let $this = this;
			answerPromise.then(function(answer){

				let answerBytes = $this.messageToBytes(answer);
				if($this.stopped ) return;
                operationData.bandwidth = (operationData.bandwidth || 0) + answerBytes.length;
				if(operationData.operationKey) $this.updateOperationStats(operationData.operationKey, operationData);
				event.interface.send(answerBytes, event.fport, $this.frpPort);
			});
		}
		catch(e){ console.log(e) }
	}
	
	// add a pending request
	addTransaction(tr={}){
		if(!tr.tid) req.tid = this.createTid();
		this.transactions[tr.tid] = tr;
		if(!tr.startTime) tr.startTime = Date.now();
		return tr.tid;
	}
	
	getTransaction(tid){
		return this.transactions[tid] || null;
	}
	
	removeTransaction(id){
		if(this.transactions[id]) delete this.transactions[id];
	}
	
	/////////////////////////////////////////////////////////////////////
	//////////////////   ENCODING AND DECODING         //////////////////
	/////////////////////////////////////////////////////////////////////

	// convert a frp message to its byte representation
	messageToBytes(o){
		if(!o.tid) o.tid = this.createTid();
		switch(o.type){
			case this.TYPE_REQUEST :
			case "request" : 
				return this.requestToBytes(o);
			case this.TYPE_ANSWER :
			case "answer" : 
				return this.answerToBytes(o);
			case this.TYPE_REGISTER :
			case "register" : 
				return this.registerToBytes(o);
			case this.TYPE_NOTIFY :
			case "notify" : 
				return this.notifyToBytes(o);
			case this.TYPE_BUSY :
			case "busy" : 
				return this.busyToBytes(o);
			case this.TYPE_CANCEL :
			case "cancel" : 
				return this.cancelToBytes(o);
			default :
				throw "unknown type for conversion "+o.type;
		}
	}
	
	// convert a frp byte message to its object representation
	messageToObject(b){
		return this._flaptools.decode(b, this.TEMPLATE_FRP_MESSAGE);
	}
	
	requestToBytes(o){
		let data = this._copyProperties(o, ["version","tid", "key"]);
		data.type = "request";
		return this._flaptools.encode(data, this.TEMPLATE_FRP_MESSAGE);
	}
	
	answerToBytes(o){
		let data = this._copyProperties(o, ["version","tid", "document", "status"]);
		data.type = "answer";
		return this._flaptools.encode(data, this.TEMPLATE_FRP_MESSAGE);
	}
	
	registerToBytes(o){
		let data = this._copyProperties(o, ["version","tid", "keys", "retro"]);
		data.type = "register";
		return this._flaptools.encode(data, this.TEMPLATE_FRP_MESSAGE);
	}
	
	notifyToBytes(o){
		let data = this._copyProperties(o, ["version","tid", "keys"]);
		data.type = "notify";
		return this._flaptools.encode(data, this.TEMPLATE_FRP_MESSAGE);
	}

	busyToBytes(o){
		let data = this._copyProperties(o, ["version","tid", "time"]);
		data.type = "busy";
		return this._flaptools.encode(data, this.TEMPLATE_FRP_MESSAGE);
	}
	
	cancelToBytes(o){
		let data = this._copyProperties(o, ["version","tid"]);
		data.type = "cancel";
		return this._flaptools.encode(data, this.TEMPLATE_FRP_MESSAGE);
	}
	
	_copyProperties(input, props, target={}){
		for(let prop of props){
			if(input.hasOwnProperty(prop)){
				target[prop] = input[prop];
			}
		}
		return target;
	}
	
	/////////////////////////////////////////////////////////////////////
	//////////////////   HELPERS                       //////////////////
	/////////////////////////////////////////////////////////////////////

	// build all the keys out of an hexadecimal prefix and a suffixes array
	buildKeys(prefix, suffixes){
		if(!suffixes) suffixes = [""];
		let res = [];
		for(let suffix of suffixes){
			if(suffix.length%2) suffix = "0"+suffix;
			res.push(prefix+suffix); 
		}
		return res;
	}
	
	createTid(length=this.TID_LENGTH){
		//if(length === null) length = this.TID_LENGTH;
		return this._flaptools.randomHex(length);

		let b = new Uint8Array(length);
		for(let i=0; i<length; i++) b[i] = Math.random()*256 % 256;
		if(length) b[0] ^=127;
		return this._flaptools.bytesToHex(b);
	}
	
	// // check if a tid has been generated on a local node or a remote node
	// isTidLocal(tid){
	// 	let b = typeof tid === "string" ? this._flaptools.hexToBytes(tid) : tid;
	// 	return !(b[0] & 128);
	// }
	
	// perform a not operation on first bit of an hexadecimal
	toggleTid(hex){
		let b = this._flaptools.hexToBytes(hex);
		b[0] ^= 128;
		return this._flaptools.bytesToHex(b);
	}
	
	// race an array of Promise : resolve with the first resolution or reject with the last rejection
	race(promises, opts={}){
		let data = {
			nprom : promises.length,
			done : false
		}
		let promRes = new Promise((res,rej)=>{
			data.res = res;
			data.rej = rej;
		});
		let raceResolve = function(v){
			if(data.done) return;
			else data.done = true;
			data.res(v);
		}
		let raceReject = function(v){
			data.nprom--;
			if(data.nprom) return;
			else data.rej(v);
		}
		for(let prom of promises){
			prom.then(raceResolve).catch(raceReject);
		}
		// timeout option : reject when too much time has passed
		if(opts.timeout){
			setTimeout(function(){
                data.rej("RACE TIMEOUT")
			},opts.timeout)
		}

		return promRes;
	}
	
	
	_initConstants(){
		this.TID_LENGTH = 8;
		this.STATUS_ANSWERED = 1;
		this.STATUS_ERROR = 100;
		this.STATUS_NOT_FOUND = 101;

		this.MAX_TARGETING_PER_NODE = 4;
	}

	_initNodejs(){
		// for nodejs environment, loads some modules
		try{
			this._fs = __webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'fs'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()));
            this._path = __webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'path'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()));
            this._mime = __webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'mime'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()));
		}
		catch(e){}
	}
	
	initInterface(ni){
		ni.addHook(ni.e.RECEIVE, this.processTransaction.bind(this), this.frpPort );
	}
	
	_initTemplates(){
		
		// store various message types
		this.TYPE_REQUEST = 1;
		this.TYPE_ANSWER = 2;
		this.TYPE_REGISTER = 3;
		this.TYPE_NOTIFY = 4;
		this.TYPE_BUSY = 5;
		this.TYPE_CANCEL = 6;
		this.TRANSLATION_TYPE = {
			[this.TYPE_REQUEST] : "request",
			[this.TYPE_ANSWER] : "answer",
			[this.TYPE_REGISTER] : "register",
			[this.TYPE_NOTIFY] : "notify",
			[this.TYPE_BUSY] : "busy",
			[this.TYPE_CANCEL] : "cancel"
		};
		
		// set message status 
		this.STATUS_ANSWERED = 1;
		this.STATUS_ERROR = 100;
		this.STATUS_NOT_FOUND = 101;
		this.STATUS_UNAUTHORIZED = 102;
		this.STATUS_TIMEOUT = 103;
		this.TRANSLATION_STATUS = {
			[this.STATUS_ANSWERED] : "ANSWERED",
			[this.STATUS_ERROR] : "ERROR",
			[this.STATUS_NOT_FOUND] : "NOT_FOUND",
			[this.STATUS_UNAUTHORIZED] : "UNAUTHORIZED",
			[this.STATUS_TIMEOUT] : "ANSWER_TIMEOUT",
		}
		
		// build EE for frp keys
		let ee_frp_key = [
			{ n : "prefix", t : "hex", c : 8, d : ""},
			{ n : "span", t : "int", c : 9},
			{ n : "suffixes", t : "arr", c : 10, tpl :  { n : "suffix", t : "hex", c : 12} },
			{ n : "dversion", t : "int", c : 11}
		];
		let eeo_key={};
		for(let e of ee_frp_key) eeo_key[e.n]=e;
		this.TEMPLATE_FRP_KEY = {
			ee : eeo_key
		};

		// build EE for frp messages
		let ee_frp_messages = [
			{ n : "version", t : "int", c : 0, d : 0},
			{ n : "length", t : "int", c : 1},
			{ n : "type", t:"int", c : 2, rpl : this.TRANSLATION_TYPE },
			{ n : "tid", t : "hex", c : 3},
			{ n : "key", t : "dict", c : 4, tpl : this.TEMPLATE_FRP_KEY},
			{ n : "document", t : "dict", tpl : this._frf.TEMPLATE_DOCUMENT, c : 5},
			{ n : "status", t:"int", c : 6, rpl : this.TRANSLATION_STATUS },
			{ n : "keys", t : "arr", c : 7, tpl : this.TEMPLATE_FRP_KEY},
            { n : "retro", t : "bool", c : 12},
            { n : "time", t : "int", c : 13},
		];
		let eeo={};
		for(let e of ee_frp_messages) eeo[e.n]=e;
		this.TEMPLATE_FRP_MESSAGE = {
			ee : eeo
		};
	 }
}


// CONCATENATED MODULE: ./src/flockNode/lib/FlockNode.js
/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

// create and configure a flock environment from a signle configuration object
// main flock components are accessible directly through properties















class FlockNode_FlockNode{

    constructor(data={}){
        // set which nested interface will be created if the environment allow it
        this.withHttpSpawn = data.hasOwnProperty("withHttpSpawn") ? data.withHttpSpawn : true;
        this.withWebsocketSpawn = data.hasOwnProperty("withWebsocketSpawn") ? data.withWebsocketSpawn : true;
        this.withWebrtcSpawn = data.hasOwnProperty("withWebrtcSpawn") ? data.withWebrtcSpawn : true;
        this.withSwansSpawn = data.hasOwnProperty("withSwansSpawn") ? data.withSwansSpawn : true;
        this.withHttpSpawner = data.hasOwnProperty("withHttpSpawner") ? data.withHttpSpawner : true;
        this.withWebsocketSpawner = data.hasOwnProperty("withWebsocketSpawner") ? data.withWebsocketSpawner : true;
        this.withWebrtcSpawner = data.hasOwnProperty("withWebrtcSpawner") ? data.withWebrtcSpawner : true;
        this.withSwansSpawner = data.hasOwnProperty("withSwansSpawner") ? data.withSwansSpawner : true;

        // authorize or forbid the creation of warehouse
        this.withWarehouse =  data.hasOwnProperty("withWarehouse") ? data.withWarehouse : true;
        // set wich Warehouse are created if possible
        this.withMDBWarehouse = data.hasOwnProperty("withMDBWarehouse") ? data.withMDBWarehouse : true;
        this.withIDBWarehouse = data.hasOwnProperty("withIDBWarehouse") ? data.withIDBWarehouse : true;

        // retrieve environments parameters
        this.global = this.globalContext();
        this.serverAddress = data.serverAddress || "127.0.0.1";
        this.websocketPort = data.websocketPort || 8080;
        this.httpPort = data.httpPort;
        this.httpRoute = data.httpRoute;
        this.iceServers = data.iceServers || null;
        this.passIceTimeout = data.passIceTimeout || false;
        this.nodeExpireTime = data.nodeExpireTime || 10000;

        // create a flaptools
        this.flaptools = new Flaptools_Flaptools();

        // retrieve interface for enabling communication through a swans server
        this.swansServerCanals = data.swansServerCanals || { type : 2, address : "ws://127.0.0.1" };
        if(!Array.isArray(this.swansServerCanals)) this.swansServerCanals = [this.swansServerCanals];

        // create the useful nested interface
        this.metaInterface = this._createNIMeta();
        this.createNIMetaSpawners(this.metaInterface);

        // create the flock component
        let flockData = {
            flaptools : this.flaptools,
            nodeExpireTime : this.nodeExpireTime,
            metaInterface : this.metaInterface,
            withKeyFormations : true,
            // retrieve the first flock nodes to be contacted
            nodes : data.nodes || [],
        };
        if(data.maxInterfacedNodes) flockData.maxInterfacedNodes = data.maxInterfacedNodes;
        if(data.nodeExpireTime) flockData.nodeExpireTime = data.nodeExpireTime;
        if(data.cronInterval) flockData.cronInterval = data.cronInterval;
        if(data.keys) flockData.keys = data.keys;

        this.flock = new Flock(flockData);

        // create the frf component
        this.frf = new Frf_Frf();

        // create the frp component
        this.frp = new Frp_Frp({
            flaptools : this.flaptools,
            flock : this.flock,
            frf : this.frf,
            metaInterface : this.metaInterface
        })
        this.createFrpWarehouse(this.frp);
    }

    // start all the node components
    start(){ }

    // return a promise that resolve once all components of the flock node are ready
    // attributes of the flock node like canals should not be used until it has resolved
    isReady(){
        // list of promise to be waited for before the node is ready
        if(!this._readyproms) this._readyproms = [];

        // retrieve the number of current pending promises
        let initialPendingPromises = this._readyproms.length;
        let $this = this;
        return Promise.all(this._readyproms).then(function(){
            if($this._readyproms.length === initialPendingPromises){
                return Promise.resolve()
            }
            else{
                return $this.isReady();
            }
        })
    }

    // add a new promise to wait for before the node is ready
    addReadyPromise(prom){
        // list of promise to be waited for before the node is ready
        if(!this._readyproms) this._readyproms = [];
        this._readyproms.push(prom);
    }

    // stop all the node components
    stop(){
        this.metaInterface.stop();
        this.flock.stop();

        if(this.swansSpawner) this.swansSpawner.stop();
    }

    // create a NIMeta for nodejs or browser environment
    // since some spawner creation are asynchronous, add promises to determine when the node will be ready
    createNIMetaSpawners(nimeta=this.metaInterface){
        let $this = this;
        // for nodejs environment
        if(this.isNodejsEnvironment()){
            if(this.withWebsocketSpawn) nimeta.addSpawner( this._createWebsocketSpawner(true) );
            if(this.withHttpSpawn) nimeta.addSpawner(  this._createHttpSpawner(true) );
            if(this.withWebrtcSpawn) {
                if(!this.global.RTCPeerConnection){
                    try {
                        // this.retrieveLibrary("RTCPeerConnection", "wrtc");
                        this.global.RTCPeerConnection = __webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'wrtc'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())).RTCPeerConnection;
                    }
                    catch (e) {
                        throw "couldn't retrieve webrtc : " + e
                    }
                }
                if (this.global.RTCPeerConnection){
                    let webrtcProm = this._createWebrtcSpawner().then(function(webrtcSpawner){
                        nimeta.addSpawner(webrtcSpawner);
                    })
                    this.addReadyPromise( webrtcProm );
                }
            }
            if(this.withSwansSpawn){
                let swansProm = this._createSwansSpawner(false).then(function(swansSpawner){
                    nimeta.addSpawner( swansSpawner );
                });
                this.addReadyPromise( swansProm );
            }
        }
        // for browser environment
        else{
            if(this.withWebsocketSpawn) nimeta.addSpawner( this._createWebsocketSpawner(false) );
            if(this.withHttpSpawn) nimeta.addSpawner(  this._createHttpSpawner(false) );
            if(this.withSwansSpawn){
                let swansProm = this._createSwansSpawner(false).then(function(swansSpawner){
                     nimeta.addSpawner( swansSpawner );
                })
                this.addReadyPromise( swansProm );
            }
            if(this.withWebrtcSpawn){
                let webrtcProm = this._createWebrtcSpawner().then(function(webrtcSpawner){
                    nimeta.addSpawner(webrtcSpawner);
                })
                this.addReadyPromise( webrtcProm );
            }
        }
    }

    // create warehouse for nodejs or webbrowser environment
    createFrpWarehouse(frp){
        if(!this.withWarehouse) return;
        let warehouse;
        // for nodejs environment
        if(this.isNodejsEnvironment()){
            if(this.withMDBWarehouse){
                warehouse = new MDBWarehouse_MDBWarehouse();
            }
        }
        // for browser environment
        else{
            if(this.withIDBWarehouse){
                warehouse = new IDBWarehouse_IDBWarehouse();
            }
        }
        if(warehouse){
            frp.addWarehouse(warehouse);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    ///////////////////// HELPERS FOR NESTED INTERFACE CREATION ////////////////////
    ////////////////////////////////////////////////////////////////////////////////

    // fill the nimeta with as many spawners it can contain
    _createNIMeta(){
        return new NIMeta_NIMeta();
    }

    _createWebsocketSpawner(withServer=true){
        return new NIWebsocket_NIWebsocketSpawner({
            withServer : this.withWebsocketSpawner,
            socketPort : this.websocketPort,
            address : "ws://"+this.serverAddress+":"+this.websocketPort // +(this.websocketPort ? ":"+this.websocketPort : "")
        });
    }

    _createHttpSpawner(withServer=true){
        return new NIHttp_NIHttpSpawner({
            withServer : this.withHttpSpawner,
            address : "http://"+this.serverAddress,
            serverPort : this.httpPort,
            serverRoute : this.httpRoute
        });
    }

    // create and initialize a receiver connected to a remote swans server
    _createSwansReceiver(){
        let swansServerPromise = this._createSwansServerInterface();
        let $this = this;
        return swansServerPromise.then(function(swansServer){
            let receiver = new SwanClientReceiver({
                flaptools : $this.flaptools
            })
            receiver.registerInit(swansServer);

            return receiver;
        })
    }

    // create a canal to a swansServer
    _createSwansServerInterface(){
        let swansServerInterfacePromise = this.metaInterface.spawn(this.swansServerCanals);
        return swansServerInterfacePromise;
    }

    _createSwansSpawner(withReceiver=true){
        let swansReceiverPromise = withReceiver ? this._createSwansReceiver() : Promise.resolve();
        let $this = this;
        return swansReceiverPromise.then(function(swansReceiver){
            return new NISwans_NISwansSpawner({
                receiver : swansReceiver,
                spawner : $this.metaInterface,
                swansServerCanals : $this.swansServerCanals
            })
        })
    }

    _createWebrtcSpawner(){
        let signalingInterfacePromise = this.withWebrtcSpawner ? this._createSwansSpawner() : Promise.resolve(null);
        let $this = this;
        return signalingInterfacePromise.then(function(signalingInterface){
            $this.swansSpawner = signalingInterface;
            let webrtcSpawner = new NIWebrtc_NIWebrtcSpawner({
                signalingInterface : signalingInterface,
                spawner : $this.metaInterface,
                flaptools : $this.flaptools,
                ice : $this.iceServers,
                passIceTimeout : $this.passIceTimeout
            });
            return webrtcSpawner;
        })
    }

    ////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////   OTHER HELPERS    ////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////

    // check if the program is running through nodejs
    isNodejsEnvironment(){
        return typeof global === "object";
    }

    // retrieve the global context (document or global)
    globalContext(){
        return this.isNodejsEnvironment() ? global : window;
    }
}

var FlockNode_pr = FlockNode_FlockNode.prototype;


// CONCATENATED MODULE: ./src/flockNode/lib/Squawker.js
/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

/***
 implementation of squawker class
 ***/




class Squawker_Squawker extends FlockNode_FlockNode{
    constructor(data={}){
        // by default, forbid warehouse for squawkers since they are not supposed to store/feed any data
        if(!data.hasOwnProperty("withWarehouse")) data.withWarehouse = false;
        super(data);

        // create a swans server
        this.swansServer = this._createSwansServer();
        this.swansServer.registerCallback( this.swansRegisterCallback.bind(this) );

        // warn the flock it s a squawker
        this.flock.squawker = true;

        let canals = this.flock.getCanals();
    }

    // create a node to the flock every time a registration occurs on the swan server
    swansRegisterCallback(receiver){
        let newNode = {
            // this might not be the real id, but it is replaced once information about the node are retrieved
            // add a prefix to recognize this kind of id
            id : "0000000000000000" + this.flock.generateId(),
            interface : receiver.interface
        }
        // this.flock.addNode(newNode);
        this.flock.getNodeInfo(newNode);
    }

    /////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////     HELPERS      ////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////

    _createSwansServer(){
        let swansServer = new SwanServer({
            clientInterface : this.metaInterface,
            receiverInterface : this.metaInterface
        })
        return swansServer;
    }

    stop(){
        super.stop();
        if(this.swansServer) this.swansServer.stop();
    }
}



// CONCATENATED MODULE: ./management/packaging/allFlockExports.js
/* concated harmony reexport Flaptools */__webpack_require__.d(__webpack_exports__, "Flaptools", function() { return Flaptools_Flaptools; });
/* concated harmony reexport Secswans */__webpack_require__.d(__webpack_exports__, "Secswans", function() { return secswans_Secswans; });
/* concated harmony reexport AbstractSwan */__webpack_require__.d(__webpack_exports__, "AbstractSwan", function() { return AbstractSwan; });
/* concated harmony reexport SwanClientReceiver */__webpack_require__.d(__webpack_exports__, "SwanClientReceiver", function() { return SwanClientReceiver; });
/* concated harmony reexport SwanClientCaller */__webpack_require__.d(__webpack_exports__, "SwanClientCaller", function() { return SwanClientCaller; });
/* concated harmony reexport SwanServer */__webpack_require__.d(__webpack_exports__, "SwanServer", function() { return SwansServer_SwanServer; });
/* concated harmony reexport NestedInterface */__webpack_require__.d(__webpack_exports__, "NestedInterface", function() { return NestedInterface_NestedInterface; });
/* concated harmony reexport NIHttpSpawner */__webpack_require__.d(__webpack_exports__, "NIHttpSpawner", function() { return NIHttp_NIHttpSpawner; });
/* concated harmony reexport NIHttpAnswer */__webpack_require__.d(__webpack_exports__, "NIHttpAnswer", function() { return NIHttp_NIHttpAnswer; });
/* concated harmony reexport NIHttpRequest */__webpack_require__.d(__webpack_exports__, "NIHttpRequest", function() { return NIHttp_NIHttpRequest; });
/* concated harmony reexport NIMeta */__webpack_require__.d(__webpack_exports__, "NIMeta", function() { return NIMeta_NIMeta; });
/* concated harmony reexport NISwansSpawner */__webpack_require__.d(__webpack_exports__, "NISwansSpawner", function() { return NISwans_NISwansSpawner; });
/* concated harmony reexport NISwansAnswer */__webpack_require__.d(__webpack_exports__, "NISwansAnswer", function() { return NISwans_NISwansAnswer; });
/* concated harmony reexport NISwansCallerSpawn */__webpack_require__.d(__webpack_exports__, "NISwansCallerSpawn", function() { return NISwans_NISwansCallerSpawn; });
/* concated harmony reexport NIWebrtcSpawner */__webpack_require__.d(__webpack_exports__, "NIWebrtcSpawner", function() { return NIWebrtc_NIWebrtcSpawner; });
/* concated harmony reexport NIWebrtcSpawn */__webpack_require__.d(__webpack_exports__, "NIWebrtcSpawn", function() { return NIWebrtc_NIWebrtcSpawn; });
/* concated harmony reexport NIWebsocketSpawner */__webpack_require__.d(__webpack_exports__, "NIWebsocketSpawner", function() { return NIWebsocket_NIWebsocketSpawner; });
/* concated harmony reexport NIWebsocketSpawn */__webpack_require__.d(__webpack_exports__, "NIWebsocketSpawn", function() { return NIWebsocket_NIWebsocketSpawn; });
/* concated harmony reexport Flock */__webpack_require__.d(__webpack_exports__, "Flock", function() { return Flock; });
/* concated harmony reexport Formation */__webpack_require__.d(__webpack_exports__, "Formation", function() { return Formation; });
/* concated harmony reexport Warehouse */__webpack_require__.d(__webpack_exports__, "Warehouse", function() { return Warehouse; });
/* concated harmony reexport IDBWarehouse */__webpack_require__.d(__webpack_exports__, "IDBWarehouse", function() { return IDBWarehouse_IDBWarehouse; });
/* concated harmony reexport MDBWarehouse */__webpack_require__.d(__webpack_exports__, "MDBWarehouse", function() { return MDBWarehouse_MDBWarehouse; });
/* concated harmony reexport Frf */__webpack_require__.d(__webpack_exports__, "Frf", function() { return Frf_Frf; });
/* concated harmony reexport Frp */__webpack_require__.d(__webpack_exports__, "Frp", function() { return Frp_Frp; });
/* concated harmony reexport FlockNode */__webpack_require__.d(__webpack_exports__, "FlockNode", function() { return FlockNode_FlockNode; });
/* concated harmony reexport Squawker */__webpack_require__.d(__webpack_exports__, "Squawker", function() { return Squawker_Squawker; });
/**
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

// low level dependencies




//
// // transport abstraction layer







// topology layer


// application layer






// high level encapsulation











// TODO : solve this dirty hack
// it should work with libraryTarget=window, somehow it doesn't
try{
    let w = window;

    w.Flaptools = Flaptools_Flaptools;
    w.Secswans = secswans_Secswans;
    w.AbstractSwan = AbstractSwan;
    w.SwanClientReceiver = SwanClientReceiver;
    w.SwanClientCaller = SwanClientCaller;
    w.SwanServer = SwansServer_SwanServer;
    w.NestedInterface = NestedInterface_NestedInterface;
    w.NIHttpSpawner = NIHttp_NIHttpSpawner;
    w.NIHttpAnswer = NIHttp_NIHttpAnswer;
    w.NIHttpRequest = NIHttp_NIHttpRequest;
    w.NIMeta = NIMeta_NIMeta;
    w.NISwansSpawner = NISwans_NISwansSpawner;
    w.NISwansAnswer = NISwans_NISwansAnswer;
    w.NISwansCallerSpawn = NISwans_NISwansCallerSpawn;
    w.NIWebrtcSpawner = NIWebrtc_NIWebrtcSpawner;
    w.NIWebrtcSpawn = NIWebrtc_NIWebrtcSpawn;
    w.NIWebsocketSpawner = NIWebsocket_NIWebsocketSpawner;
    w.NIWebsocketSpawn = NIWebsocket_NIWebsocketSpawn;
    w.Flock = Flock;
    w.Formation = Formation;
    w.Warehouse = Warehouse;
    w.IDBWarehouse = IDBWarehouse_IDBWarehouse;
    w.MDBWarehouse = MDBWarehouse_MDBWarehouse;
    w.Frf = Frf_Frf;
    w.Frp = Frp_Frp;
    w.FlockNode = FlockNode_FlockNode;
    w.Squawker = Squawker_Squawker;

}
catch(e){

}

/***/ })
/******/ ])));