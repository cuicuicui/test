/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

// handle encryption routines for swans protocol implementation, namely rsa and aes
// methods beginning with a _ are internal and should  not be used
class Secswans{

	constructor(){
		// initialize some constants for aes
		this.AES_IRR = 0b100011011; // aes irreductible polynom
		this.sbox = this._generateSbox(); // permutation table
		this.t2 = this._t2(); // polynome multiplication by 2
		this.reverseSbox = this._generateReverseSbox( this.sbox );
		
		// available encryption methods
		this.ENCTYPE_NONE = 0; // no encryption used
		this.ENCTYPE_DH_2048_256 = 1; // DH with 2048b prime and 256bit prime subgroup, defined in rfc 5114
		this.ENCTYPE_RSA_1024 = 16; // RSA 1024 bits
		this.ENCTYPE_RSA_2048 = 17; // RSA 2048 bits
		this.ENCTYPE_RSA_4096 = 18; // RSA 4096 bits

		// available hash methods
        this.HASHTYPE_SHA_256 = 4;
        this.HASHTYPE_SHA_512 = 5;

		// expected hash/signature size for various methos :
		this.TOKEN_SIZE = {
            [ this.ENCTYPE_DH_2048_256 ] : 32,
            [ this.ENCTYPE_RSA_1024 ] : 128,
			"rsa1024" : 128,
            [ this.ENCTYPE_RSA_2048 ] : 256,
            "rsa2048" : 256,
            [ this.ENCTYPE_RSA_4096 ] : 512,
            "rsa4096" : 512,
            [ this.HASHTYPE_SHA_256 ] : 32,
            "sha256" : 32,

		};

		// constants for big integer manipulation
		this.supmask = new Uint8Array([128, 192, 224, 240, 248, 252 , 254, 255]); // supMask take only the top i+1 bits of a byte
		this.infmask = new Uint8Array([255, 127, 63, 31, 15, 7, 3, 1, 0]); // infmask take only the lower 8-i bits of a byte
		this.bm = new Uint8Array([128, 64, 32, 16, 8, 4, 2, 1]); 
		
		// initialize the big num generator : those are store in fixed size 0f 256*4 bytes
		this._lib = this._rsaLibGen();
		
		// constants for DH
		this._RFC5114_2048_256_P = this._hexstrToUint8Array("87A8E61DB4B6663CFFBBD19C651959998CEEF608660DD0F25D2CEED4435E3B00E00DF8F1D61957D4FAF7DF4561B2AA3016C3D91134096FAA3BF4296D830E9A7C209E0C6497517ABD5A8A9D306BCF67ED91F9E6725B4758C022E0B1EF4275BF7B6C5BFC11D45F9088B941F54EB1E59BB8BC39A0BF12307F5C4FDB70C581B23F76B63ACAE1CAA6B7902D52526735488A0EF13C6D9A51BFA4AB3AD8347796524D8EF6A167B5A41825D967E144E5140564251CCACB83E6B486F6B3CA3F7971506026C0B857F689962856DED4010ABD0BE621C3A3960A54E710C375F26375D7014103A4B54330C198AF126116D2276E11715F693877FAD7EF09CADB094AE91E1A1597",16);
		this._RFC5114_2048_256_G = this._hexstrToUint8Array("3FB32C9B73134D0B2E77506660EDBD484CA7B18F21EF205407F4793A1A0BA12510DBC15077BE463FFF4FED4AAC0BB555BE3A6C1B0C6B47B1BC3773BF7E8C6F62901228F8C28CBB18A55AE31341000A650196F931C77A57F2DDF463E5E9EC144B777DE62AAAB8A8628AC376D282D6ED3864E67982428EBC831D14348F6F2F9193B5045AF2767164E1DFC967C1FB3F2E55A4BD1BFFE83B9C80D052B985D182EA0ADB2A3B7313D3FE14C8484B1E052588B9B7D2BBD2DF016199ECD06E1557CD0915B3353BBB64E0EC377FD028370DF92B52C7891428CDC67EB6184B523D1DB246C32F63078490F00EF8D647D148D47954515E2327CFEF98C582664B4C0F6CC41659",16);
	
		// some parameters constants
		this.REQUEST_KEY_STRENGTH = 32; // byte length of symetric key
	}
	
	////////////////////////////////////////////////////////////////////
	////////////////// SECSWANS METHODS ////////////////////////////////
	////////////////////////////////////////////////////////////////////
	
	// given a request key with a private part, and a foreign key(receiver) with a public part, compute the public local key
	computePublicKey(requestKey, receiverKey){
		let type = receiverKey.type;
		switch( parseInt(type) ){
			case this.ENCTYPE_NONE : 
				return new Uint8Array();
			case this.ENCTYPE_DH_2048_256 :
				return this._dhToPublic(requestKey.private, this._RFC5114_2048_256_G, this._RFC5114_2048_256_P);
			default : 
				throw "Pubgen : Invalid encryption type : "+type;
				break;
		}
	}
	
	// create a new transaction key, whose data can build a symetric encryption key, given another public key from the receiver
	createRequestKey(){
		return this.createKey(this._random(this.REQUEST_KEY_STRENGTH)); 
	}
	
	// encrypt message with a symetric key
	symetricEncrypt(msg, symkey){
		let res = this.aesCbcCypher(msg, symkey);
        return res;
	}
	
	// decrypt a message with a symetric key
	symetricDecrypt(msg, symkey){
		let res = this.aesCbcInverseCypher(msg, symkey);
		return res;
	}
	
	// compute the aes key to be used to decrypt/encrypt message, from two set of keys
	computeSymetricKey(keys1, keys2){
        let type = parseInt( keys2.hasOwnProperty("type") ? keys2.type : keys1.type);
        let res;
		switch(type){
			case this.ENCTYPE_NONE : 
				return null;
			case this.ENCTYPE_DH_2048_256 :
				if(keys1.public && keys2.private){
                    res = this._commonSecretDh(keys1.public, keys2.private, this._RFC5114_2048_256_P, true);
				}
				else if(keys2.public && keys1.private){
                    res = this._commonSecretDh(keys2.public, keys1.private, this._RFC5114_2048_256_P, true);
				}
				return res;
				break;
			default : 
				throw "Symgen : Invalid encryption type : "+type;
				break
		}
	}
	
	// generate public and private key for a given encryption type
	generateKey(type){
		switch(type){
			case this.ENCTYPE_NONE : 
				return this.createKey( new Uint8Array(), new Uint8Array(), this.ENCTYPE_NONE )
			case this.ENCTYPE_DH_2048_256 :
				return this._generateKeyDh(type);
			case this.ENCTYPE_RSA_1024 : 
			case this.ENCTYPE_RSA_2048 : 
			case this.ENCTYPE_RSA_4096 :
				let fullrsa = this._generateKeyRsa(type);
				let privrsa = { n : fullrsa.n, e : fullrsa.e, d: fullrsa.d, type : type };
				let pubrsa = { n : fullrsa.n, e : fullrsa.e , type : type}
				return this.createKey( privrsa, pubrsa, type);
			case this.HASHTYPE_SHA_256 :
				return this.createKey({type:type}, {type:type}, type);
			default : 
				throw "keygen, Invalid encryption type : "+type;
				break
		}
	}
	
	// create a key pair from given data
	createKey( priv, pub, type){
			return {
				type : type,
				private : priv,
				public : pub
			};
	}
	
	// sign a sequence of bytes
	sign(input, privkey){
		if(!Secswans.prototype.cs)Secswans.prototype.cs = 1;
		switch(privkey.type){
			case "rsa1024" : 
			case "rsa2048" : 
			case "rsa4096" :
			case this.ENCTYPE_RSA_1024 : 
			case this.ENCTYPE_RSA_2048 : 
			case this.ENCTYPE_RSA_4096 :  
				return this._rsaSign(input, privkey);
			case this.HASHTYPE_SHA_256 :
			case "sha256" :
				return this._sha256Sign(input);
			throw "unsupported sign method : "+privkey.type;
		}
	}

	// check a signature of a sequence of bytes	
	checkSign(input, pubkey, expectedSignature){
        if(!Secswans.prototype.us)Secswans.prototype.us = 1;
		switch(pubkey.type){
			case "rsa1024" : 
			case "rsa2048" : 
			case "rsa4096" : 
			case this.ENCTYPE_RSA_1024 : 
			case this.ENCTYPE_RSA_2048 : 
			case this.ENCTYPE_RSA_4096 :
				return this._rsaCheckSign(input, pubkey, expectedSignature);
            case "sha256" :
            case this.HASHTYPE_SHA_256 :
                return this._sha256CheckSign(input, expectedSignature);
			throw "unsupported check sign method : "+pubkey.type;
		}
	}

	////////////////////////////////////////////////////////////////////
	/////////// DIFFIE HELLMAN METHODS /////////////////////////////////
	////////////////////////////////////////////////////////////////////
	
	// create a key given an encryption type
	_generateKeyDh(type, priv=null){
        if(!Secswans.prototype.gk)Secswans.prototype.gk = 1;
		let pub;
		if(!priv) priv = this.createRequestKey().private;
		switch(type){
				case this.ENCTYPE_DH_2048_256 :
					pub =  this.power(this._RFC5114_2048_256_G, priv, this._RFC5114_2048_256_P);
					break;
		}
		return this.createKey(priv, pub, type);
	}
	
	// transform a private key into a public key for DH
	_dhToPublic(priv, g, p){
		return this.power(g, priv, p);
	}
	
	// generate the common secret given a public key and a private key
	_commonSecretDh(pubKey, privKey, prime, hash=false){
		let res = this.power(pubKey, privKey, prime);
		if(hash) res = this.sha256(res);
		return res;
	}

	////////////////////////////////////////////////////////////////////
	/////////////////////  RSA METHODS /////////////////////////////////
	////////////////////////////////////////////////////////////////////
	
	// generate a key for rsa algorithm 
	// return an object with n, e and d as Uint8Array
	_generateKeyRsa(type){
		var B;
		switch(type){
			case this.ENCTYPE_RSA_1024 : 
				B=1024;
				break;
			case this.ENCTYPE_RSA_2048 : 
				B=2048;
				break;
			case this.ENCTYPE_RSA_4096 : 
				B=4096;
				break;
			default :
				throw "unexpected rsa type : "+type;
		}
		var E = "3";
		// TODO : a problem might occur during random generation : for the moment, we just try again until it works
		var hasFailed = true;
        var rsa;
		while(hasFailed){
			try{
                rsa = new this._lib.RSAKey();
                rsa.generate(B,E);
                hasFailed = false;
			}
			catch(e){
				console.log("error during rsa generation : ", e);
			}
		}
		let res = {
			type : type,
			n : rsa.n.toString(16),
			e : (E.length%2 ? "0" : "") + E,
			d : rsa.d.toString(16),
		}
		return res;
	}
	
	// sign a message with a rsa key
	// expect a rsa key with properties n, e and d in hexadecimal string representation
	// bit length accepted for n are : 1024, 2048, 4096
	// expect a hash method : default is sha256
	// expect message to be a Uint8Array
	// hash the message, pad the hash with 0 up to n byte length, encrypt with private key, and return a Uint8Array
	_rsaSign(message, signtools={}){
		let n = signtools.n;
		let e = signtools.e;
		let d = signtools.d;
		let hash = signtools.hash || "sha256";
		let hashSize = this.TOKEN_SIZE[hash] || 32;
		let signatureSize = n.length/2;
		let hash16 = this._hashAndPad(message, hash, hashSize) ;
		// perform encryption
		let encHash = this._rsaPrivateOperation(hash16, n, e, d);
		let sig = encHash.padStart(signatureSize*2, "0");
		return sig;
	}

	// compute sha256 signature of a UintArray
    _sha256Sign(message){
		let sigBytes = this.sha256(message);
		let sigHex = this.bytesToHex(sigBytes);
		return sigHex;
	}

	// perform RSA public operation on an hexadecimal string
	_rsaPublicOperation(inputHex, n, e){
        let rsa = new this._lib.RSAKey();
        rsa.setPublic(n, e);
        var bignum = new this._lib.BigInteger(inputHex,16);
        let res = rsa.doPublic(bignum).toString(16);
        return res;
	}

	// perform the RSA private operation on an hexadecimal string
	_rsaPrivateOperation(inputHex, n, e, d){
        let rsa = new this._lib.RSAKey();
        rsa.setPrivate(n,e,d);
        var bignum = new this._lib.BigInteger(inputHex,16);
        let res = rsa.doPrivate(bignum).toString(16);
        return res;
	}

	// check the signature with a rsa key
	// expect rsa key properties n and e, in hexadecimal string
	// bit length n can be 1024, 2048, 4096
	// expect a hash method : default is sha256
	// expect signature Uint8Array or hexadecimal
	// original message to compare the signature
	// return true if the signatures match
	_rsaCheckSign(message, signtools = {}, signature=""){
		let n = signtools.n;
		let e = signtools.e;
		if( typeof signature !== "string") signature = this.bytesToHex(signature);
        let hash = signtools.hash || "sha256";
        let hashSize = this.TOKEN_SIZE[hash] || 32;
        let signatureSize = signtools.n / 8;
		let expectedHash = this._hashAndPad(message, hash, hashSize);
		// // perform encryption
		let decHash = this._rsaPublicOperation(signature, n, e);
		// convert to hexadecimal
		let decHash16 = decHash.padStart(hashSize*2,"0");

		return decHash16 === expectedHash;
	}

    _sha256CheckSign(message, expectedSignature){
		let hashBytes = this.sha256(message);
		let hashHex = this.bytesToHex(hashBytes);
        expectedSignature = expectedSignature.slice(-64);
		return expectedSignature === hashHex;
	}

	// hash a byte/hexadecimal message with the given hash method, and return an hexadecimal string of the hash, padded with 0 up to the target length
	_hashAndPad(msg, hashMethod, len){
		if(typeof msg === "string") msg = this.hexToBytes(msg);
		if(hashMethod != "sha256") throw "unsupported hash method : "+hashMethod;
		let h = this.sha256(msg);
		let h16 = this.bytesToHex(h);
		if(h16.length>2*len) throw "invalid target length "+len+" for "+hashMethod;
		return h16.padStart(len, "0");
	}
	
	////////////////////////////////////////////////////////////////////
	///////////         HELPERS                     ////////////////////
	////////////////////////////////////////////////////////////////////
	
	// byte to hexadecimal conversion
	bytesToHex(b){
		return Array.from(b).map(i=>(i<16 ? "0" : "")+i.toString(16)).join("");
	}
	
	// hexadecimal to bytes conversion
	hexToBytes(h){
		if(h.length%2) h = "0"+h;
		return new Uint8Array(h.length/2).map((v,i)=>parseInt(h.substr(2*i,2),16));
	}
	
	////////////////////////////////////////////////////////////////////
	/////////// METHODS FOR UINT8ARRAY BIG INTEGERS ////////////////////
	////////////////////////////////////////////////////////////////////
	
	
	// create a random number of the given size
	_random(size){
		let res = new Uint8Array(size);
		for(let i in res) res[i] = Math.random() * 256 >> 0;
		return res;
	}
	
	// check if an array is null
	isNull(a){
		return a.every(i=>i===0);
	}
	
	// auxiliar function to check if an array is equal to one
	isOne (a){
		return a[a.length-1] === 1 && this.isNull(a.slice(0,-1));
	}


	////////////////////////////////////////////////////////////////////
	///////////////////// AES RELATED METHODS //////////////////////////
	////////////////////////////////////////////////////////////////////
	
	// perform aes encryption with cbc mode
	// if not provided, initialization vector is set to 0
	// if data length is not a multiple of 16, append 0 to it
	aesCbcCypher(data, key, vinit = new Uint8Array(16) ){
		let resultLength = data.length + (data.length % 16 ? 16 - (data.length%16) : 0);
		let result = new Uint8Array(resultLength);
		result.set(data);
		let expandedKey = this._keyExpansion(key);
		for(let i=0; i < resultLength; i+=16){
			let state = result.slice(i, i+16);
			for(let j in state) state[j] ^= vinit[j];
			this.aesCypherBlock(state, expandedKey);
			result.set(state, i);
			vinit = state;
		} 
		return result;
	}
	
	// perform aes decryption with cbc mode
	// if not provided, initialization vector is set to 0
	aesCbcInverseCypher(data, key, vinit = new Uint8Array(16) ){
		let result = data.slice();
		let expandedKey = this._keyExpansion(key);
		for(let i=0; i < data.length; i+=16){
			let state = result.slice(i, i+16);
			this.aesInverseCypherBlock(state, expandedKey);
			for(let j in state) state[j] ^= vinit[j];
			result.set(state, i);
			vinit = data.slice(i, i+16);
		} 
		return result;
	}
	
	// cypher a 16 byte long block represented with a Uint8Array
	// state : Uint8Array of size 16 : it is also the state in aes algo, since modifications occurs in place
	// key : Uint8Array of size 16, 24 or 32, or of size 176, 208, 240 if the expanded key is provided directly
	aesCypherBlock(state, key){
		let expandedKey = key.length >= 176 ? key : this._keyExpansion(key);
		this._addRoundKey(state, expandedKey);
		let nr = this._nr(key);
		for(let i=1; i<=nr; i++){
			this._SubBytes(state, this.sbox);
			this._ShiftRows(state);
			if( i !== nr) this._MixColumns(state);
			this._addRoundKey(state, expandedKey, 16*i);
		}
	}
	
	// aes inverse block cypher
	// state : Uint8Array of size 16 : it is also the state in aes algo, since modifications occurs in place
	// key : Uint8Array of size 16, 24 or 32, or of size 176, 208, 240 if the expanded key is provided directly
	aesInverseCypherBlock(state, key){
		let expandedKey = key.length >= 176 ? key : this._keyExpansion(key);
		let nr = this._nr(key);
		this._addRoundKey(state, expandedKey, 16*nr);
		for(let i=nr-1; i>=0; i--){
			this._InvShiftRows(state);
			this._SubBytes(state, this.reverseSbox);
			this._addRoundKey(state, expandedKey, 16*i);
			if(i) this._InvMixColumns(state);
		}
	}	
	
	// generate aes time two correspondance for polynom multiplication
	_t2(){
		let t2 = new Uint8Array(256);
		for(var i = 0; i < 256; i++) {
			t2[i] = i << 1;
			if(i>=128) t2[i] ^= 0x1b;
		}
		return t2;
	}
	
	// calculate the Substitution table Sbox
	_generateSbox(){
		return new Uint8Array([
			0x63, 0x7C, 0x77, 0x7B, 0xF2, 0x6B, 0x6F, 0xC5, 0x30, 0x01, 0x67, 0x2B, 0xFE, 0xD7, 0xAB, 0x76,
			0xCA, 0x82, 0xC9, 0x7D, 0xFA, 0x59, 0x47, 0xF0, 0xAD, 0xD4, 0xA2, 0xAF, 0x9C, 0xA4, 0x72, 0xC0,
			0xB7, 0xFD, 0x93, 0x26, 0x36, 0x3F, 0xF7, 0xCC, 0x34, 0xA5, 0xE5, 0xF1, 0x71, 0xD8, 0x31, 0x15,
			0x04, 0xC7, 0x23, 0xC3, 0x18, 0x96, 0x05, 0x9A, 0x07, 0x12, 0x80, 0xE2, 0xEB, 0x27, 0xB2, 0x75,
			0x09, 0x83, 0x2C, 0x1A, 0x1B, 0x6E, 0x5A, 0xA0, 0x52, 0x3B, 0xD6, 0xB3, 0x29, 0xE3, 0x2F, 0x84,
			0x53, 0xD1, 0x00, 0xED, 0x20, 0xFC, 0xB1, 0x5B, 0x6A, 0xCB, 0xBE, 0x39, 0x4A, 0x4C, 0x58, 0xCF,
			0xD0, 0xEF, 0xAA, 0xFB, 0x43, 0x4D, 0x33, 0x85, 0x45, 0xF9, 0x02, 0x7F, 0x50, 0x3C, 0x9F, 0xA8,
			0x51, 0xA3, 0x40, 0x8F, 0x92, 0x9D, 0x38, 0xF5, 0xBC, 0xB6, 0xDA, 0x21, 0x10, 0xFF, 0xF3, 0xD2,
			0xCD, 0x0C, 0x13, 0xEC, 0x5F, 0x97, 0x44, 0x17, 0xC4, 0xA7, 0x7E, 0x3D, 0x64, 0x5D, 0x19, 0x73,
			0x60, 0x81, 0x4F, 0xDC, 0x22, 0x2A, 0x90, 0x88, 0x46, 0xEE, 0xB8, 0x14, 0xDE, 0x5E, 0x0B, 0xDB,
			0xE0, 0x32, 0x3A, 0x0A, 0x49, 0x06, 0x24, 0x5C, 0xC2, 0xD3, 0xAC, 0x62, 0x91, 0x95, 0xE4, 0x79,
			0xE7, 0xC8, 0x37, 0x6D, 0x8D, 0xD5, 0x4E, 0xA9, 0x6C, 0x56, 0xF4, 0xEA, 0x65, 0x7A, 0xAE, 0x08,
			0xBA, 0x78, 0x25, 0x2E, 0x1C, 0xA6, 0xB4, 0xC6, 0xE8, 0xDD, 0x74, 0x1F, 0x4B, 0xBD, 0x8B, 0x8A,
			0x70, 0x3E, 0xB5, 0x66, 0x48, 0x03, 0xF6, 0x0E, 0x61, 0x35, 0x57, 0xB9, 0x86, 0xC1, 0x1D, 0x9E,
			0xE1, 0xF8, 0x98, 0x11, 0x69, 0xD9, 0x8E, 0x94, 0x9B, 0x1E, 0x87, 0xE9, 0xCE, 0x55, 0x28, 0xDF,
			0x8C, 0xA1, 0x89, 0x0D, 0xBF, 0xE6, 0x42, 0x68, 0x41, 0x99, 0x2D, 0x0F, 0xB0, 0x54, 0xBB, 0x16
		 ]);
	}
	
	// generate the inverse of a transposition array
	_generateReverseSbox(sbox){
		let reverseSbox = new Uint8Array( sbox.length );
		for(let i in sbox) reverseSbox[sbox[i]] = i;
		return reverseSbox;
	}
	
	// SubBytes of the AES : the modification occurs in place
	_SubBytes(state, transpositionTable){
		for(let i in state) state[i] = transpositionTable[state[i]];
	}
	
	// ShiftRows operation of the AES : modification happen in place
	// s : current state 16 bytes Uint8Array
	_ShiftRows(s){
		let t;
		// shift the second row 
		t = s[1];
		s[1] = s[5];
		s[5] = s[9];
		s[9] = s[13];
		s[13] = t;
		// shift the third row 
		t = s[2];
		s[2] = s[10];
		s[10] = t;
		t = s[6];
		s[6] = s[14];
		s[14] = t;
		// shift the fourth row 
		t = s[3];
		s[3] = s[15];
		s[15] = s[11];
		s[11] = s[7];
		s[7] = t;
	}
	
	// implementation of aes InvShiftRows
	_InvShiftRows(s){
		let t;
		// shift the second row 
		t = s[1];
		s[1] = s[13];
		s[13] = s[9];
		s[9] = s[5];
		s[5] = t;
		// shift the third row 
		t = s[2];
		s[2] = s[10];
		s[10] = t;
		t = s[6];
		s[6] = s[14];
		s[14] = t;
		// shift the fourth row 
		t = s[3];
		s[3] = s[7];
		s[7] = s[11];
		s[11] = s[15];
		s[15] = t;
	}
	
	// aes MixColumns
	// modify state s in place
	_MixColumns(s){
		for(let i = 0; i<=12; i+=4) {
			let a0 = s[0 + i];
			let a1 = s[1 + i];
			let a2 = s[2 + i];
			let a3 = s[3 + i];
			s[0+i] = this.t2[a0] ^ a3 ^ a2 ^ this.t2[a1] ^ a1;
			s[1+i] = this.t2[a1] ^ a0 ^ a3 ^ this.t2[a2] ^ a2;
			s[2+i] = this.t2[a2] ^ a1 ^ a0 ^ this.t2[a3] ^ a3;
			s[3+i] = this.t2[a3] ^ a2 ^ a1 ^ this.t2[a0] ^ a0;
		}    
	}
	
	// aes InvMixColumns
	_InvMixColumns(s){
		for(let i=0; i<16; i+=4){
			let s0 = s[i];
			let s1 = s[i+1];
			let s2 = s[i+2];
			let s3 = s[i+3];
			let p1111 = s0 ^ s1 ^ s2 ^ s3; // s0 + s1 + s2 + s3
			let p6464 = this.t2[this.t2[p1111] ^ s0 ^ s2]// 6*s0 + 4*s1 + 6*s2 + 4*s3
			let p4646 = this.t2[this.t2[p1111] ^ s1 ^ s3]// 4*s0 + 6*s1 + 4*s2 + 6*s3
			let p1100 = s0 ^ s1;
			let p0011 = s2 ^ s3;
			s[i]   = this.t2[ p6464 ^ p1100] ^ s1 ^ p0011;
			s[i+1] = this.t2[ p4646 ^ s1 ^ s2] ^ s0 ^ p0011;
			s[i+2] = this.t2[ p6464 ^ p0011] ^ s3 ^ p1100;
			s[i+3] = this.t2[ p4646 ^ s0 ^ s3] ^ s2 ^ p1100;
		}
	}
	
	// aes AddRoundKey
	// modify state s in place
	// key : an array containing the round key parts
	// offset, the offset at wich to start reading the key
	_addRoundKey(s, key, offset=0){
		for(let i=0; i<16; i++){
			s[i] ^= key[offset+i];
		}
	}
	
	// calculate Nr given a byte aes key or extended key
	_nr(key){
		switch(key.length){
			// if the key provided is not extended
			case 16 : return 10;
			case 24 : return 12;
			case 32 : return 14;
			// if the key provided is extended
			case 176 : return 10;
			case 208 : return 12;
			case 240 : return 14;
		}
	}
	
	// calculate nk given the aes key
	_nk(key){
		return key.length/4;
	}
	
	// aes keyExpansion
	// return an array containing 4 * (Nr + 1) bytes
	_keyExpansion(key){
		let tmp;
		let nr = this._nr(key);
		let nk = this._nk(key);
		let nwords = 4 * (nr+1);
		let result = new Uint8Array(nwords * 4);
		// extract the words contained in the key
		result.set(key);
		// keep Rcon up to date
		let rcon = 1;
		for(let i=key.length/4; i < nwords; i++){
			let w0 = result[4*i-4];
			let w1 = result[4*i-3];
			let w2 = result[4*i-2];
			let w3 = result[4*i-1];
			if(i % nk === 0){
				tmp = w0;
				w0 = this.sbox[w1] ^ rcon;
				w1 = this.sbox[w2];
				w2 = this.sbox[w3];
				w3 = this.sbox[tmp];
				// update the Rcon for the next time
				rcon = rcon << 1;
				if(rcon >= 256) rcon ^= 0x11b;
			}
			else if(nk >6 && i%nk === 4){
				w0 = this.sbox[w0];
				w1 = this.sbox[w1];
				w2 = this.sbox[w2];
				w3 = this.sbox[w3];
			}
			result[4 * i] = result[4*i - 4*nk] ^ w0;
			result[4 * i+1] = result[4*i+1 - 4*nk] ^ w1;
			result[4 * i+2] = result[4*i+2 - 4*nk] ^ w2;
			result[4 * i+3] = result[4*i+3 - 4*nk] ^ w3;
		}
		return result;
	}
	
	/////////////////////////////////////////////////////////////////
	//////////  SHA 256 RELATED METHODS /////////////////////////////
	/////////////////////////////////////////////////////////////////
	
	// sha256 algorithm on a Uint8Array
	// return a 32B Uint8Array
	sha256(input){
		// init sha data if it is the first time
		if(!this._K){
			 this._K = this._generateShaK();
		}
		let M = this._sha256Padding(input);
		let H0 = 0x6a09e667, H1=0xbb67ae85, H2=0x3c6ef372, H3=0xa54ff53a,
		    H4 = 0x510e527f, H5=0x9b05688c, H6=0x1f83d9ab, H7=0x5be0cd19;
		let W = new Uint32Array(64);
		for(let i=0; i<M.length; i+=16){
			let a=H0, b=H1, c=H2, d=H3, e=H4, f=H5, g=H6, h=H7;
			for(let t=0; t<64; t++){
				W[t] = t < 16 ? M[i+t] : (
					(W[t-2]>>>17 ^ W[t-2]>>>19  ^ W[t-2]<<15 ^ W[t-2]<<13  ^ W[t-2]>>>10)  + 
					(W[t-15]>>>7 ^ W[t-15]>>>18 ^ W[t-15]>>>3 ^ W[t-15]<<25 ^ W[t-15]<<14) +
					W[t-7] + W[t-16]
				) | 0;
				let t1 = (h + W[t] + this._K[t] + (g ^ e&(f^g)) + (e>>>6  ^ e>>>11  ^ e>>>25  ^ e<<26  ^ e<<21  ^ e<<7)) | 0; 
				let t2 = ((a>>>2 ^ a>>>13 ^ a>>>22 ^ a<<30 ^ a<<19 ^ a<<10) + ((a&b) ^ (c&(a^b)))  ) | 0 
				h = g ;
				g = f ;
				f = e ;
				e = d + t1 | 0;
				d = c ;
				c = b ;
				b = a ;
				a = t1 + t2  | 0;
			}
			H7 = H7 + h | 0;
			H6 = H6 + g | 0;
			H5 = H5 + f | 0;
			H4 = H4 + e | 0;
			H3 = H3 + d | 0;
			H2 = H2 + c | 0;
			H1 = H1 + b | 0;
			H0 = H0 + a| 0;
		}
		let result = new Uint32Array([H0, H1, H2, H3, H4, H5, H6, H7]);
		return this._32to8(result);
	}
	
	// generate the K table as a Uint32Array
	_generateShaK(){
		let K = new Uint32Array(64);
		let primes = this._firstPrimes(K.length);
		for(let i in primes){
			K[i] = (Math.pow(primes[i], 1/3) % 1) * 0x100000000;
		}
		return K;
	}
	
	// generate the first n prime numbers
	_firstPrimes(n){
		let res = [2];
		for(let i=res[0]+1; res.length < n; i++){
			for(let p of res){
				if((i%p) === 0) break;
				if(p*p>i){
					res.push(i);
					break;
				}
			}
		}
		return res;
	}
	
	// perform sha256 padding
	// input : a Uint8Array
	// output : a Uint8Array
	_sha256Padding(input){
		let res = this._8to32( input, 16 + Math.floor((input.byteLength+8)/64)*16 );
		res[ Math.floor( input.byteLength/4) ] += [ 0x80000000, 0x800000, 0x8000, 0x80][input.byteLength%4];
		res[res.length-1] += input.byteLength*8 & 0xffffffff;
		res[res.length-2] += input.byteLength >> 29;
		return res;
	}
	
	// transform a Uint8Array into a Uint32Array
	_8to32(arr, size){
		let len = Math.floor( size || (arr.length+3)/4 );
		let res = new Uint32Array(len);
		let mult = [0x1000000, 0x10000, 0x100, 1];
		for(let i in arr){
			res[ Math.floor(i/4) ] += mult[i%4] * arr[i];
		}
		return res;
	}
	
	// transform a Uint32Array into a Uint8Array
	_32to8(arr){
		let res = new Uint8Array(arr.buffer);
		let t;
		for(let i=0; i<res.length; i+=4){
			t = res[i]; res[i] = res[i+3]; res[i+3] = t;
			t = res[i+1]; res[i+1] = res[i+2]; res[i+2] = t;
		}
		return res;
	}
	
	
	
	////////////////////////////////////////////////////////////////////
	////////  METHODS TO MANIPULATE BIG NUMBERS ////////////////////////
	////////////////////////////////////////////////////////////////////
	
	// create a big number generator, from Tom Wu library : http://www-cs-students.stanford.edu/%7Etjw/jsbn/nRaw for not minified libraries

    /*
	 * Copyright (c) 2003-2005  Tom Wu
	 * All Rights Reserved.
	 *
	 * Permission is hereby granted, free of charge, to any person obtaining
	 * a copy of this software and associated documentation files (the
	 * "Software"), to deal in the Software without restriction, including
	 * without limitation the rights to use, copy, modify, merge, publish,
	 * distribute, sublicense, and/or sell copies of the Software, and to
	 * permit persons to whom the Software is furnished to do so, subject to
	 * the following conditions:
	 *
	 * The above copyright notice and this permission notice shall be
	 * included in all copies or substantial portions of the Software.
	 *
	 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
	 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
	 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
	 *
	 * IN NO EVENT SHALL TOM WU BE LIABLE FOR ANY SPECIAL, INCIDENTAL,
	 * INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND, OR ANY DAMAGES WHATSOEVER
	 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER OR NOT ADVISED OF
	 * THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF LIABILITY, ARISING OUT
	 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
	 *
	 * In addition, the following condition applies:
	 *
	 * All redistributions must retain an intact copy of this copyright notice
	 * and disclaimer.
	 */

	_rsaLibGen(){
		var dbits;var canary=0xdeadbeefcafe;var j_lm=((canary&0xffffff)==0xefcafe);function BigInteger(a,b,c){if(a!=null)
		if("number"==typeof a)this.fromNumber(a,b,c);else if(b==null&&"string"!=typeof a)this.fromString(a,256);else this.fromString(a,b);}
		function nbi(){return new BigInteger(null);}
		function am1(i,x,w,j,c,n){while(--n>=0){var v=x*this[i++]+w[j]+c;c=Math.floor(v/0x4000000);w[j++]=v&0x3ffffff;}
		return c;}
		function am2(i,x,w,j,c,n){var xl=x&0x7fff,xh=x>>15;while(--n>=0){var l=this[i]&0x7fff;var h=this[i++]>>15;var m=xh*l+h*xl;l=xl*l+((m&0x7fff)<<15)+w[j]+(c&0x3fffffff);c=(l>>>30)+(m>>>15)+xh*h+(c>>>30);w[j++]=l&0x3fffffff;}
		return c;}
		function am3(i,x,w,j,c,n){var xl=x&0x3fff,xh=x>>14;while(--n>=0){var l=this[i]&0x3fff;var h=this[i++]>>14;var m=xh*l+h*xl;l=xl*l+((m&0x3fff)<<14)+w[j]+c;c=(l>>28)+(m>>14)+xh*h;w[j++]=l&0xfffffff;}
		return c;}
		if(j_lm&&(typeof navigator!=="undefined")&&(navigator.appName=="Microsoft Internet Explorer")){BigInteger.prototype.am=am2;dbits=30;}
		else if(j_lm&&(typeof navigator!=="undefined")&&(navigator.appName!="Netscape")){BigInteger.prototype.am=am1;dbits=26;}
		else{BigInteger.prototype.am=am3;dbits=28;}
		BigInteger.prototype.DB=dbits;BigInteger.prototype.DM=((1<<dbits)-1);BigInteger.prototype.DV=(1<<dbits);var BI_FP=52;BigInteger.prototype.FV=Math.pow(2,BI_FP);BigInteger.prototype.F1=BI_FP-dbits;BigInteger.prototype.F2=2*dbits-BI_FP;var BI_RM="0123456789abcdefghijklmnopqrstuvwxyz";var BI_RC=new Array();var rr,vv;rr="0".charCodeAt(0);for(vv=0;vv<=9;++vv)BI_RC[rr++]=vv;rr="a".charCodeAt(0);for(vv=10;vv<36;++vv)BI_RC[rr++]=vv;rr="A".charCodeAt(0);for(vv=10;vv<36;++vv)BI_RC[rr++]=vv;function int2char(n){return BI_RM.charAt(n);}
		function intAt(s,i){var c=BI_RC[s.charCodeAt(i)];return(c==null)?-1:c;}
		function bnpCopyTo(r){for(var i=this.t-1;i>=0;--i)r[i]=this[i];r.t=this.t;r.s=this.s;}
		function bnpFromInt(x){this.t=1;this.s=(x<0)?-1:0;if(x>0)this[0]=x;else if(x<-1)this[0]=x+this.DV;else this.t=0;}
		function nbv(i){var r=nbi();r.fromInt(i);return r;}
		function bnpFromString(s,b){var k;if(b==16)k=4;else if(b==8)k=3;else if(b==256)k=8;else if(b==2)k=1;else if(b==32)k=5;else if(b==4)k=2;else{this.fromRadix(s,b);return;}
		this.t=0;this.s=0;var i=s.length,mi=false,sh=0;while(--i>=0){var x=(k==8)?s[i]&0xff:intAt(s,i);if(x<0){if(s.charAt(i)=="-")mi=true;continue;}
		mi=false;if(sh==0)
		this[this.t++]=x;else if(sh+k>this.DB){this[this.t-1]|=(x&((1<<(this.DB-sh))-1))<<sh;this[this.t++]=(x>>(this.DB-sh));}
		else
		this[this.t-1]|=x<<sh;sh+=k;if(sh>=this.DB)sh-=this.DB;}
		if(k==8&&(s[0]&0x80)!=0){this.s=-1;if(sh>0)this[this.t-1]|=((1<<(this.DB-sh))-1)<<sh;}
		this.clamp();if(mi)BigInteger.ZERO.subTo(this,this);}
		function bnpFromUint8Array(s){var k=8;this.t=0;this.s=0;var i=s.length,mi=false,sh=0;while(--i>=0){var x=s[i];if(sh==0)
		this[this.t++]=x;else if(sh+k>this.DB){this[this.t-1]|=(x&((1<<(this.DB-sh))-1))<<sh;this[this.t++]=(x>>(this.DB-sh));}
		else
		this[this.t-1]|=x<<sh;sh+=k;if(sh>=this.DB)sh-=this.DB;}
		this.clamp();if(mi)BigInteger.ZERO.subTo(this,this);}
		function bnpClamp(){var c=this.s&this.DM;while(this.t>0&&this[this.t-1]==c)--this.t;}
		function bnToString(b){if(this.s<0)return"-"+this.negate().toString(b);var k;if(b==16)k=4;else if(b==8)k=3;else if(b==2)k=1;else if(b==32)k=5;else if(b==4)k=2;else return this.toRadix(b);var km=(1<<k)-1,d,m=false,r="",i=this.t;var p=this.DB-(i*this.DB)%k;if(i-->0){if(p<this.DB&&(d=this[i]>>p)>0){m=true;r=int2char(d);}
		while(i>=0){if(p<k){d=(this[i]&((1<<p)-1))<<(k-p);d|=this[--i]>>(p+=this.DB-k);}
		else{d=(this[i]>>(p-=k))&km;if(p<=0){p+=this.DB;--i;}}
		if(d>0)m=true;if(m)r+=int2char(d);}}
		return m?r:"0";}
		function bnAbs(){return(this.s<0)?this.negate():this;}
		function bnToUint8Array(l=0){if(this.s<0)return"-"+this.negate().toString(b);let resLen=l|Math.floor((this.t*this.DB+7)/8);let r=new Uint8Array(resLen);let firstByte=resLen;for(let i=0,d=0,p=0,resI=resLen-1;i<this.t;){d=d+(this[i++]*Math.pow(2,p));p+=this.DB;while(p>0){if(p<8&&i<this.t)break;r[resI]=d&0xff;if(r[resI])firstByte=resI;resI--;d=Math.floor(d/256);p-=8;}}
		if(!l&&firstByte)r=r.slice(firstByte);return r;}
		function bnCompareTo(a){var r=this.s-a.s;if(r!=0)return r;var i=this.t;r=i-a.t;if(r!=0)return(this.s<0)?-r:r;while(--i>=0)if((r=this[i]-a[i])!=0)return r;return 0;}
		function nbits(x){var r=1,t;if((t=x>>>16)!=0){x=t;r+=16;}
		if((t=x>>8)!=0){x=t;r+=8;}
		if((t=x>>4)!=0){x=t;r+=4;}
		if((t=x>>2)!=0){x=t;r+=2;}
		if((t=x>>1)!=0){x=t;r+=1;}
		return r;}
		function bnBitLength(){if(this.t<=0)return 0;return this.DB*(this.t-1)+nbits(this[this.t-1]^(this.s&this.DM));}
		function bnpDLShiftTo(n,r){var i;for(i=this.t-1;i>=0;--i)r[i+n]=this[i];for(i=n-1;i>=0;--i)r[i]=0;r.t=this.t+n;r.s=this.s;}
		function bnpDRShiftTo(n,r){for(var i=n;i<this.t;++i)r[i-n]=this[i];r.t=Math.max(this.t-n,0);r.s=this.s;}
		function bnpLShiftTo(n,r){var bs=n%this.DB;var cbs=this.DB-bs;var bm=(1<<cbs)-1;var ds=Math.floor(n/this.DB),c=(this.s<<bs)&this.DM,i;for(i=this.t-1;i>=0;--i){r[i+ds+1]=(this[i]>>cbs)|c;c=(this[i]&bm)<<bs;}
		for(i=ds-1;i>=0;--i)r[i]=0;r[ds]=c;r.t=this.t+ds+1;r.s=this.s;r.clamp();}
		function bnpRShiftTo(n,r){r.s=this.s;var ds=Math.floor(n/this.DB);if(ds>=this.t){r.t=0;return;}
		var bs=n%this.DB;var cbs=this.DB-bs;var bm=(1<<bs)-1;r[0]=this[ds]>>bs;for(var i=ds+1;i<this.t;++i){r[i-ds-1]|=(this[i]&bm)<<cbs;r[i-ds]=this[i]>>bs;}
		if(bs>0)r[this.t-ds-1]|=(this.s&bm)<<cbs;r.t=this.t-ds;r.clamp();}
		function bnpSubTo(a,r){var i=0,c=0,m=Math.min(a.t,this.t);while(i<m){c+=this[i]-a[i];r[i++]=c&this.DM;c>>=this.DB;}
		if(a.t<this.t){c-=a.s;while(i<this.t){c+=this[i];r[i++]=c&this.DM;c>>=this.DB;}
		c+=this.s;}
		else{c+=this.s;while(i<a.t){c-=a[i];r[i++]=c&this.DM;c>>=this.DB;}
		c-=a.s;}
		r.s=(c<0)?-1:0;if(c<-1)r[i++]=this.DV+c;else if(c>0)r[i++]=c;r.t=i;r.clamp();}
		function bnpMultiplyTo(a,r){var x=this.abs(),y=a.abs();var i=x.t;r.t=i+y.t;while(--i>=0)r[i]=0;for(i=0;i<y.t;++i)r[i+x.t]=x.am(0,y[i],r,i,0,x.t);r.s=0;r.clamp();if(this.s!=a.s)BigInteger.ZERO.subTo(r,r);}
		function bnpSquareTo(r){var x=this.abs();var i=r.t=2*x.t;while(--i>=0)r[i]=0;for(i=0;i<x.t-1;++i){var c=x.am(i,x[i],r,2*i,0,1);if((r[i+x.t]+=x.am(i+1,2*x[i],r,2*i+1,c,x.t-i-1))>=x.DV){r[i+x.t]-=x.DV;r[i+x.t+1]=1;}}
		if(r.t>0)r[r.t-1]+=x.am(i,x[i],r,2*i,0,1);r.s=0;r.clamp();}
		function bnpDivRemTo(m,q,r){var pm=m.abs();if(pm.t<=0)return;var pt=this.abs();if(pt.t<pm.t){if(q!=null)q.fromInt(0);if(r!=null)this.copyTo(r);return;}
		if(r==null)r=nbi();var y=nbi(),ts=this.s,ms=m.s;var nsh=this.DB-nbits(pm[pm.t-1]);if(nsh>0){pm.lShiftTo(nsh,y);pt.lShiftTo(nsh,r);}
		else{pm.copyTo(y);pt.copyTo(r);}
		var ys=y.t;var y0=y[ys-1];if(y0==0)return;var yt=y0*(1<<this.F1)+((ys>1)?y[ys-2]>>this.F2:0);var d1=this.FV/yt,d2=(1<<this.F1)/yt,e=1<<this.F2;var i=r.t,j=i-ys,t=(q==null)?nbi():q;y.dlShiftTo(j,t);if(r.compareTo(t)>=0){r[r.t++]=1;r.subTo(t,r);}
		BigInteger.ONE.dlShiftTo(ys,t);t.subTo(y,y);while(y.t<ys)y[y.t++]=0;while(--j>=0){var qd=(r[--i]==y0)?this.DM:Math.floor(r[i]*d1+(r[i-1]+e)*d2);if((r[i]+=y.am(0,qd,r,j,0,ys))<qd){y.dlShiftTo(j,t);r.subTo(t,r);while(r[i]<--qd)r.subTo(t,r);}}
		if(q!=null){r.drShiftTo(ys,q);if(ts!=ms)BigInteger.ZERO.subTo(q,q);}
		r.t=ys;r.clamp();if(nsh>0)r.rShiftTo(nsh,r);if(ts<0)BigInteger.ZERO.subTo(r,r);}
		function bnMod(a){var r=nbi();this.abs().divRemTo(a,null,r);if(this.s<0&&r.compareTo(BigInteger.ZERO)>0)a.subTo(r,r);return r;}
		function Classic(m){this.m=m;}
		function cConvert(x){if(x.s<0||x.compareTo(this.m)>=0)return x.mod(this.m);else return x;}
		function cRevert(x){return x;}
		function cReduce(x){x.divRemTo(this.m,null,x);}
		function cMulTo(x,y,r){x.multiplyTo(y,r);this.reduce(r);}
		function cSqrTo(x,r){x.squareTo(r);this.reduce(r);}
		Classic.prototype.convert=cConvert;Classic.prototype.revert=cRevert;Classic.prototype.reduce=cReduce;Classic.prototype.mulTo=cMulTo;Classic.prototype.sqrTo=cSqrTo;function bnpInvDigit(){if(this.t<1)return 0;var x=this[0];if((x&1)==0)return 0;var y=x&3;y=(y*(2-(x&0xf)*y))&0xf;y=(y*(2-(x&0xff)*y))&0xff;y=(y*(2-(((x&0xffff)*y)&0xffff)))&0xffff;y=(y*(2-x*y%this.DV))%this.DV;return(y>0)?this.DV-y:-y;}
		function Montgomery(m){this.m=m;this.mp=m.invDigit();this.mpl=this.mp&0x7fff;this.mph=this.mp>>15;this.um=(1<<(m.DB-15))-1;this.mt2=2*m.t;}
		function montConvert(x){var r=nbi();x.abs().dlShiftTo(this.m.t,r);r.divRemTo(this.m,null,r);if(x.s<0&&r.compareTo(BigInteger.ZERO)>0)this.m.subTo(r,r);return r;}
		function montRevert(x){var r=nbi();x.copyTo(r);this.reduce(r);return r;}
		function montReduce(x){while(x.t<=this.mt2)
		x[x.t++]=0;for(var i=0;i<this.m.t;++i){var j=x[i]&0x7fff;var u0=(j*this.mpl+(((j*this.mph+(x[i]>>15)*this.mpl)&this.um)<<15))&x.DM;j=i+this.m.t;x[j]+=this.m.am(0,u0,x,i,0,this.m.t);while(x[j]>=x.DV){x[j]-=x.DV;x[++j]++;}}
		x.clamp();x.drShiftTo(this.m.t,x);if(x.compareTo(this.m)>=0)x.subTo(this.m,x);}
		function montSqrTo(x,r){x.squareTo(r);this.reduce(r);}
		function montMulTo(x,y,r){x.multiplyTo(y,r);this.reduce(r);}
		Montgomery.prototype.convert=montConvert;Montgomery.prototype.revert=montRevert;Montgomery.prototype.reduce=montReduce;Montgomery.prototype.mulTo=montMulTo;Montgomery.prototype.sqrTo=montSqrTo;function bnpIsEven(){return((this.t>0)?(this[0]&1):this.s)==0;}
		function bnpExp(e,z){if(e>0xffffffff||e<1)return BigInteger.ONE;var r=nbi(),r2=nbi(),g=z.convert(this),i=nbits(e)-1;g.copyTo(r);while(--i>=0){z.sqrTo(r,r2);if((e&(1<<i))>0)z.mulTo(r2,g,r);else{var t=r;r=r2;r2=t;}}
		return z.revert(r);}
		function bnModPowInt(e,m){var z;if(e<256||m.isEven())z=new Classic(m);else z=new Montgomery(m);return this.exp(e,z);}
		BigInteger.prototype.copyTo=bnpCopyTo;BigInteger.prototype.fromInt=bnpFromInt;BigInteger.prototype.fromString=bnpFromString;BigInteger.prototype.fromUint8Array=bnpFromUint8Array;BigInteger.prototype.clamp=bnpClamp;BigInteger.prototype.dlShiftTo=bnpDLShiftTo;BigInteger.prototype.drShiftTo=bnpDRShiftTo;BigInteger.prototype.lShiftTo=bnpLShiftTo;BigInteger.prototype.rShiftTo=bnpRShiftTo;BigInteger.prototype.subTo=bnpSubTo;BigInteger.prototype.multiplyTo=bnpMultiplyTo;BigInteger.prototype.squareTo=bnpSquareTo;BigInteger.prototype.divRemTo=bnpDivRemTo;BigInteger.prototype.invDigit=bnpInvDigit;BigInteger.prototype.isEven=bnpIsEven;BigInteger.prototype.exp=bnpExp;BigInteger.prototype.toString=bnToString;BigInteger.prototype.toUint8Array=bnToUint8Array;BigInteger.prototype.abs=bnAbs;BigInteger.prototype.compareTo=bnCompareTo;BigInteger.prototype.bitLength=bnBitLength;BigInteger.prototype.mod=bnMod;BigInteger.prototype.modPowInt=bnModPowInt;BigInteger.ZERO=nbv(0);BigInteger.ONE=nbv(1);function bnClone(){var r=nbi();this.copyTo(r);return r;}
		function bnSigNum(){if(this.s<0)return-1;else if(this.t<=0||(this.t==1&&this[0]<=0))return 0;else return 1;}
		function bnpFromNumber(a,b,c){if("number"==typeof b){if(a<2)this.fromInt(1);else{this.fromNumber(a,c);if(!this.testBit(a-1))
		this.bitwiseTo(BigInteger.ONE.shiftLeft(a-1),op_or,this);if(this.isEven())this.dAddOffset(1,0);while(!this.isProbablePrime(b)){this.dAddOffset(2,0);if(this.bitLength()>a)this.subTo(BigInteger.ONE.shiftLeft(a-1),this);}}}
		else{var x=new Array(),t=a&7;x.length=(a>>3)+1;b.nextBytes(x);if(t>0)x[0]&=((1<<t)-1);else x[0]=0;this.fromString(x,256);}}
		function bnToByteArray(){var i=this.t,r=new Array();r[0]=this.s;var p=this.DB-(i*this.DB)%8,d,k=0;if(i-->0){if(p<this.DB&&(d=this[i]>>p)!=(this.s&this.DM)>>p)
		r[k++]=d|(this.s<<(this.DB-p));while(i>=0){if(p<8){d=(this[i]&((1<<p)-1))<<(8-p);d|=this[--i]>>(p+=this.DB-8);}
		else{d=(this[i]>>(p-=8))&0xff;if(p<=0){p+=this.DB;--i;}}
		if((d&0x80)!=0)d|=-256;if(k==0&&(this.s&0x80)!=(d&0x80))++k;if(k>0||d!=this.s)r[k++]=d;}}
		return r;}
		function bnpBitwiseTo(a,op,r){var i,f,m=Math.min(a.t,this.t);for(i=0;i<m;++i)r[i]=op(this[i],a[i]);if(a.t<this.t){f=a.s&this.DM;for(i=m;i<this.t;++i)r[i]=op(this[i],f);r.t=this.t;}
		else{f=this.s&this.DM;for(i=m;i<a.t;++i)r[i]=op(f,a[i]);r.t=a.t;}
		r.s=op(this.s,a.s);r.clamp();}
		function op_and(x,y){return x&y;}
		function op_or(x,y){return x|y;}
		function op_xor(x,y){return x^y;}
		function bnNot(){var r=nbi();for(var i=0;i<this.t;++i)r[i]=this.DM&~this[i];r.t=this.t;r.s=~this.s;return r;}
		function bnShiftLeft(n){var r=nbi();if(n<0)this.rShiftTo(-n,r);else this.lShiftTo(n,r);return r;}
		function bnShiftRight(n){var r=nbi();if(n<0)this.lShiftTo(-n,r);else this.rShiftTo(n,r);return r;}
		function lbit(x){if(x==0)return-1;var r=0;if((x&0xffff)==0){x>>=16;r+=16;}
		if((x&0xff)==0){x>>=8;r+=8;}
		if((x&0xf)==0){x>>=4;r+=4;}
		if((x&3)==0){x>>=2;r+=2;}
		if((x&1)==0)++r;return r;}
		function bnGetLowestSetBit(){for(var i=0;i<this.t;++i)
		if(this[i]!=0)return i*this.DB+lbit(this[i]);if(this.s<0)return this.t*this.DB;return-1;}
		function cbit(x){var r=0;while(x!=0){x&=x-1;++r;}
		return r;}
		function bnTestBit(n){var j=Math.floor(n/this.DB);if(j>=this.t)return(this.s!=0);return((this[j]&(1<<(n%this.DB)))!=0);}
		function bnpAddTo(a,r){var i=0,c=0,m=Math.min(a.t,this.t);while(i<m){c+=this[i]+a[i];r[i++]=c&this.DM;c>>=this.DB;}
		if(a.t<this.t){c+=a.s;while(i<this.t){c+=this[i];r[i++]=c&this.DM;c>>=this.DB;}
		c+=this.s;}
		else{c+=this.s;while(i<a.t){c+=a[i];r[i++]=c&this.DM;c>>=this.DB;}
		c+=a.s;}
		r.s=(c<0)?-1:0;if(c>0)r[i++]=c;else if(c<-1)r[i++]=this.DV+c;r.t=i;r.clamp();}
		function bnSubtract(a){var r=nbi();this.subTo(a,r);return r;}
		function bnMultiply(a){var r=nbi();this.multiplyTo(a,r);return r;}
		function bnpDAddOffset(n,w){if(n==0)return;while(this.t<=w)this[this.t++]=0;this[w]+=n;while(this[w]>=this.DV){this[w]-=this.DV;if(++w>=this.t)this[this.t++]=0;++this[w];}}
		function bnModPow(e,m){var i=e.bitLength(),k,r=nbv(1),z;if(i<=0)return r;else if(i<18)k=1;else if(i<48)k=3;else if(i<144)k=4;else if(i<768)k=5;else k=6;if(i<8)
		z=new Classic(m);else if(m.isEven())
		z=new Barrett(m);else
		z=new Montgomery(m);var g=new Array(),n=3,k1=k-1,km=(1<<k)-1;g[1]=z.convert(this);if(k>1){var g2=nbi();z.sqrTo(g[1],g2);while(n<=km){g[n]=nbi();z.mulTo(g2,g[n-2],g[n]);n+=2;}}
		var j=e.t-1,w,is1=true,r2=nbi(),t;i=nbits(e[j])-1;while(j>=0){if(i>=k1)w=(e[j]>>(i-k1))&km;else{w=(e[j]&((1<<(i+1))-1))<<(k1-i);if(j>0)w|=e[j-1]>>(this.DB+i-k1);}
		n=k;while((w&1)==0){w>>=1;--n;}
		if((i-=n)<0){i+=this.DB;--j;}
		if(is1){g[w].copyTo(r);is1=false;}
		else{while(n>1){z.sqrTo(r,r2);z.sqrTo(r2,r);n-=2;}
		if(n>0)z.sqrTo(r,r2);else{t=r;r=r2;r2=t;}
		z.mulTo(r2,g[w],r);}
		while(j>=0&&(e[j]&(1<<i))==0){z.sqrTo(r,r2);t=r;r=r2;r2=t;if(--i<0){i=this.DB-1;--j;}}}
		return z.revert(r);}
		function bnGCD(a){var x=(this.s<0)?this.negate():this.clone();var y=(a.s<0)?a.negate():a.clone();if(x.compareTo(y)<0){var t=x;x=y;y=t;}
		var i=x.getLowestSetBit(),g=y.getLowestSetBit();if(g<0)return x;if(i<g)g=i;if(g>0){x.rShiftTo(g,x);y.rShiftTo(g,y);}
		while(x.signum()>0){if((i=x.getLowestSetBit())>0)x.rShiftTo(i,x);if((i=y.getLowestSetBit())>0)y.rShiftTo(i,y);if(x.compareTo(y)>=0){x.subTo(y,x);x.rShiftTo(1,x);}
		else{y.subTo(x,y);y.rShiftTo(1,y);}}
		if(g>0)y.lShiftTo(g,y);return y;}
		function bnpModInt(n){if(n<=0)return 0;var d=this.DV%n,r=(this.s<0)?n-1:0;if(this.t>0)
		if(d==0)r=this[0]%n;else for(var i=this.t-1;i>=0;--i)r=(d*r+this[i])%n;return r;}
		function bnModInverse(m){var ac=m.isEven();if((this.isEven()&&ac)||m.signum()==0)return BigInteger.ZERO;var u=m.clone(),v=this.clone();var a=nbv(1),b=nbv(0),c=nbv(0),d=nbv(1);while(u.signum()!=0){while(u.isEven()){u.rShiftTo(1,u);if(ac){if(!a.isEven()||!b.isEven()){a.addTo(this,a);b.subTo(m,b);}
		a.rShiftTo(1,a);}
		else if(!b.isEven())b.subTo(m,b);b.rShiftTo(1,b);}
		while(v.isEven()){v.rShiftTo(1,v);if(ac){if(!c.isEven()||!d.isEven()){c.addTo(this,c);d.subTo(m,d);}
		c.rShiftTo(1,c);}
		else if(!d.isEven())d.subTo(m,d);d.rShiftTo(1,d);}
		if(u.compareTo(v)>=0){u.subTo(v,u);if(ac)a.subTo(c,a);b.subTo(d,b);}
		else{v.subTo(u,v);if(ac)c.subTo(a,c);d.subTo(b,d);}}
		if(v.compareTo(BigInteger.ONE)!=0)return BigInteger.ZERO;if(d.compareTo(m)>=0)return d.subtract(m);if(d.signum()<0)d.addTo(m,d);else return d;if(d.signum()<0)return d.add(m);else return d;}
		var lowprimes=[2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97,101,103,107,109,113,127,131,137,139,149,151,157,163,167,173,179,181,191,193,197,199,211,223,227,229,233,239,241,251,257,263,269,271,277,281,283,293,307,311,313,317,331,337,347,349,353,359,367,373,379,383,389,397,401,409,419,421,431,433,439,443,449,457,461,463,467,479,487,491,499,503,509,521,523,541,547,557,563,569,571,577,587,593,599,601,607,613,617,619,631,641,643,647,653,659,661,673,677,683,691,701,709,719,727,733,739,743,751,757,761,769,773,787,797,809,811,821,823,827,829,839,853,857,859,863,877,881,883,887,907,911,919,929,937,941,947,953,967,971,977,983,991,997];var lplim=(1<<26)/lowprimes[lowprimes.length-1];function bnIsProbablePrime(t){var i,x=this.abs();if(x.t==1&&x[0]<=lowprimes[lowprimes.length-1]){for(i=0;i<lowprimes.length;++i)
		if(x[0]==lowprimes[i])return true;return false;}
		if(x.isEven())return false;i=1;while(i<lowprimes.length){var m=lowprimes[i],j=i+1;while(j<lowprimes.length&&m<lplim)m*=lowprimes[j++];m=x.modInt(m);while(i<j)if(m%lowprimes[i++]==0)return false;}
		return x.millerRabin(t);}
		function bnpMillerRabin(t){var n1=this.subtract(BigInteger.ONE);var k=n1.getLowestSetBit();if(k<=0)return false;var r=n1.shiftRight(k);t=(t+1)>>1;if(t>lowprimes.length)t=lowprimes.length;var a=nbi();for(var i=0;i<t;++i){a.fromInt(lowprimes[Math.floor(Math.random()*lowprimes.length)]);var y=a.modPow(r,this);if(y.compareTo(BigInteger.ONE)!=0&&y.compareTo(n1)!=0){var j=1;while(j++<k&&y.compareTo(n1)!=0){y=y.modPowInt(2,this);if(y.compareTo(BigInteger.ONE)==0)return false;}
		if(y.compareTo(n1)!=0)return false;}}
		return true;}
		var bip=BigInteger.prototype;bip.fromNumber=bnpFromNumber;bip.bitwiseTo=bnpBitwiseTo;bip.addTo=bnpAddTo;bip.dAddOffset=bnpDAddOffset;bip.modInt=bnpModInt;bip.millerRabin=bnpMillerRabin;bip.clone=bnClone;bip.signum=bnSigNum;bip.toByteArray=bnToByteArray;bip.shiftLeft=bnShiftLeft;bip.shiftRight=bnShiftRight;bip.getLowestSetBit=bnGetLowestSetBit;bip.testBit=bnTestBit;bip.subtract=bnSubtract;bip.multiply=bnMultiply;bip.modPow=bnModPow;bip.modInverse=bnModInverse;bip.gcd=bnGCD;bip.isProbablePrime=bnIsProbablePrime;function rng_get_bytes(ba){for(var i=0;i<ba.length;++i)ba[i]=Math.random()*256>>0;}
		function SecureRandom(){}
		SecureRandom.prototype.nextBytes=rng_get_bytes;function parseBigInt(str,r){return new BigInteger(str,r);}
		function linebrk(s,n){var ret="";var i=0;while(i+n<s.length){ret+=s.substring(i,i+n)+"\n";i+=n;}
		return ret+s.substring(i,s.length);}
		function byte2Hex(b){if(b<0x10)
		return"0"+b.toString(16);else
		return b.toString(16);}
		function pkcs1pad2(s,n){if(n<s.length+11){throw("Message too long for RSA");return null;}
		var ba=new Array();var i=s.length-1;while(i>=0&&n>0){var c=s.charCodeAt(i--);if(c<128){ba[--n]=c;}
		else if((c>127)&&(c<2048)){ba[--n]=(c&63)|128;ba[--n]=(c>>6)|192;}
		else{ba[--n]=(c&63)|128;ba[--n]=((c>>6)&63)|128;ba[--n]=(c>>12)|224;}}
		ba[--n]=0;var rng=new SecureRandom();var x=new Array();while(n>2){x[0]=0;while(x[0]==0)rng.nextBytes(x);ba[--n]=x[0];}
		ba[--n]=2;ba[--n]=0;return new BigInteger(ba);}
		function RSAKey(){this.n=null;this.e=0;this.d=null;this.p=null;this.q=null;this.dmp1=null;this.dmq1=null;this.coeff=null;}
		function RSASetPublic(N,E){if(N!=null&&E!=null&&N.length>0&&E.length>0){this.n=parseBigInt(N,16);this.e=parseInt(E,16);}
		else
		throw("Invalid RSA public key");}
		function RSADoPublic(x){return x.modPowInt(this.e,this.n);}
		function RSAEncrypt(text){var m=pkcs1pad2(text,(this.n.bitLength()+7)>>3);if(m==null)return null;var c=this.doPublic(m);if(c==null)return null;var h=c.toString(16);if((h.length&1)==0)return h;else return"0"+h;}
		RSAKey.prototype.doPublic=RSADoPublic;RSAKey.prototype.setPublic=RSASetPublic;RSAKey.prototype.encrypt=RSAEncrypt;function pkcs1unpad2(d,n){var b=d.toByteArray();var i=0;while(i<b.length&&b[i]==0)++i;if(b.length-i!=n-1||b[i]!=2)
		return null;++i;while(b[i]!=0)
		if(++i>=b.length)return null;var ret="";while(++i<b.length){var c=b[i]&255;if(c<128){ret+=String.fromCharCode(c);}
		else if((c>191)&&(c<224)){ret+=String.fromCharCode(((c&31)<<6)|(b[i+1]&63));++i;}
		else{ret+=String.fromCharCode(((c&15)<<12)|((b[i+1]&63)<<6)|(b[i+2]&63));i+=2;}}
		return ret;}
		function RSASetPrivate(N,E,D){if(N!=null&&E!=null&&N.length>0&&E.length>0){this.n=parseBigInt(N,16);this.e=parseInt(E,16);this.d=parseBigInt(D,16);}
		else
		throw("Invalid RSA private key");}
		function RSAGenerate(B,E){var rng=new SecureRandom();var qs=B>>1;this.e=parseInt(E,16);var ee=new BigInteger(E,16);for(;;){for(;;){this.p=new BigInteger(B-qs,1,rng);if(this.p.subtract(BigInteger.ONE).gcd(ee).compareTo(BigInteger.ONE)==0&&this.p.isProbablePrime(10))break;}
		for(;;){this.q=new BigInteger(qs,1,rng);if(this.q.subtract(BigInteger.ONE).gcd(ee).compareTo(BigInteger.ONE)==0&&this.q.isProbablePrime(10))break;}
		if(this.p.compareTo(this.q)<=0){var t=this.p;this.p=this.q;this.q=t;}
		var p1=this.p.subtract(BigInteger.ONE);var q1=this.q.subtract(BigInteger.ONE);var phi=p1.multiply(q1);if(phi.gcd(ee).compareTo(BigInteger.ONE)==0){this.n=this.p.multiply(this.q);this.d=ee.modInverse(phi);this.dmp1=this.d.mod(p1);this.dmq1=this.d.mod(q1);this.coeff=this.q.modInverse(this.p);break;}}}
		function RSADoPrivate(x){if(this.p==null||this.q==null)
		return x.modPow(this.d,this.n);var xp=x.mod(this.p).modPow(this.dmp1,this.p);var xq=x.mod(this.q).modPow(this.dmq1,this.q);while(xp.compareTo(xq)<0)
		xp=xp.add(this.p);return xp.subtract(xq).multiply(this.coeff).mod(this.p).multiply(this.q).add(xq);}
		function RSADecrypt(ctext){var c=parseBigInt(ctext,16);var m=this.doPrivate(c);if(m==null)return null;return pkcs1unpad2(m,(this.n.bitLength()+7)>>3);}
		RSAKey.prototype.doPrivate=RSADoPrivate;RSAKey.prototype.setPrivate=RSASetPrivate;RSAKey.prototype.generate=RSAGenerate;RSAKey.prototype.decrypt=RSADecrypt;var exportLib={RSAKey:RSAKey,BigInteger:BigInteger,Classic:Classic,Montgomery:Montgomery,SecureRandom:SecureRandom,}
		return exportLib;
	}
	
	// calculate a^b%n
	// n MUST be ODD
	power(a,b,n){
		let bnA = new this._lib.BigInteger();
		bnA.fromUint8Array(a);
		let bnB = new this._lib.BigInteger();
		bnB.fromUint8Array(b);
		let bnN = new this._lib.BigInteger();
		bnN.fromUint8Array(n);
		let bnRes = bnA.modPow(bnB, bnN);

		return bnRes.toUint8Array();
	}
	
	// transform a string into a Uint8Array
	_hexstrToUint8Array(str){
		if(str.length%2) str = "0"+str;
		let res = new Uint8Array(str.length/2);
		for(let i=0; i<str.length;i+=2){
			res[i/2] = parseInt(str.slice(i,i+2), 16);
		}
		return res;
	}
}

export {
    Secswans
}