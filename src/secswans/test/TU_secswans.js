/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

/**
 * Tests for secswans on nodejs and webbrowser environment
 * test cases are taken from aes specs : https://nvlpubs.nist.gov/nistpubs/fips/nist.fips.197.pdf
 * **/


///////////////////// testing modules /////////////////////
let chai = require("chai");
let chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
let assert = chai.assert;

// tested modules
var flocklibs = require("../../../dist/flock-nodejs.js")
Object.assign(global, flocklibs);

var secswans = new Secswans();



///////////////////////////////////////////////////////////////////////
//////////////  GENERAL HELPERS   /////////////////////////////////////
///////////////////////////////////////////////////////////////////////


describe("testing bytes to hexadecimal conversion", function(){
	it("should convert from bytes to hexadecimal", function(){
		let result, input, expected;
		
		input = new Uint8Array([]);
		expected = "";
		result = secswans.bytesToHex(input);
		assert.deepEqual(result, expected);
		
		input = new Uint8Array([3]);
		expected = "03";
		result = secswans.bytesToHex(input);
		assert.deepEqual(result, expected);
		
		input = new Uint8Array([0,4,5,255]);
		expected = "000405ff";
		result = secswans.bytesToHex(input);
		assert.equal(result, expected);
		
	});
	
	it("should convert hexadecimal to bytes", function(){
		let result, input, expected;
		
		expected = new Uint8Array([]);
		input = "";
		result = secswans.hexToBytes(input);
		assert.deepEqual(result, expected);
		assert.instanceOf(result, Uint8Array);
		
		expected = new Uint8Array([3]);
		input = "3";
		result = secswans.hexToBytes(input);
		assert.deepEqual(result, expected);
		assert.instanceOf(result, Uint8Array);
		
		expected = new Uint8Array([0,4,5,255]);
		input = "000405ff";
		result = secswans.hexToBytes(input);
		assert.deepEqual(result, expected);
		assert.instanceOf(result, Uint8Array);
	});
	
});

///////////////////////////////////////////////////////////////////////
//////////////////    AES TESTING     /////////////////////////////////
///////////////////////////////////////////////////////////////////////

describe("testing expansion of keys for aes", function(){

	it("should expand the 16 bytes key", function(){
		let key = new Uint8Array([0x2b ,0x7e ,0x15 ,0x16 ,0x28 ,0xae ,0xd2 ,0xa6 ,0xab ,0xf7 ,0x15 ,0x88 ,0x09 ,0xcf ,0x4f ,0x3c]);
		let expandedKey = secswans._keyExpansion(key);
		let expectedExpandedKey = new Uint8Array([
										0x2b,0x7e,0x15,0x16,
										0x28,0xae,0xd2,0xa6,
										0xab,0xf7,0x15,0x88,
										0x09,0xcf,0x4f,0x3c,
										0xa0,0xfa,0xfe,0x17,
										0x88,0x54,0x2c,0xb1,
										0x23,0xa3,0x39,0x39, 
										0x2a,0x6c,0x76,0x05,
										0xf2,0xc2,0x95,0xf2,
										0x7a,0x96,0xb9,0x43,
										0x59,0x35,0x80,0x7a,
										0x73,0x59,0xf6,0x7f,
										0x3d,0x80,0x47,0x7d,
										0x47,0x16,0xfe,0x3e,
										0x1e,0x23,0x7e,0x44,
										0x6d,0x7a,0x88,0x3b,
										0xef,0x44,0xa5,0x41,
										0xa8,0x52,0x5b,0x7f,
										0xb6,0x71,0x25,0x3b,
										0xdb,0x0b,0xad,0x00,
										0xd4,0xd1,0xc6,0xf8,
										0x7c,0x83,0x9d,0x87,
										0xca,0xf2,0xb8,0xbc,
										0x11,0xf9,0x15,0xbc,
										0x6d,0x88,0xa3,0x7a,
										0x11,0x0b,0x3e,0xfd,
										0xdb,0xf9,0x86,0x41,
										0xca,0x00,0x93,0xfd,
										0x4e,0x54,0xf7,0x0e,
										0x5f,0x5f,0xc9,0xf3,
										0x84,0xa6,0x4f,0xb2,
										0x4e,0xa6,0xdc,0x4f,
										0xea,0xd2,0x73,0x21,
										0xb5,0x8d,0xba,0xd2,
										0x31,0x2b,0xf5,0x60,
										0x7f,0x8d,0x29,0x2f,
										0xac,0x77,0x66,0xf3,
										0x19,0xfa,0xdc,0x21,
										0x28,0xd1,0x29,0x41,
										0x57,0x5c,0x00,0x6e,
										0xd0,0x14,0xf9,0xa8,
										0xc9,0xee,0x25,0x89,
										0xe1,0x3f,0x0c,0xc8,
										0xb6,0x63,0x0c,0xa6,
		]);
		
		assert.lengthOf( expandedKey, expectedExpandedKey.length, "the expanded key should have the expected length");
		assert.deepEqual( expandedKey, expectedExpandedKey);
	});
	
	it("should expand the 24 bytes key", function(){
		let key = new Uint8Array([0x8e ,0x73 ,0xb0 ,0xf7 ,0xda ,0x0e ,0x64 ,0x52 ,0xc8 ,0x10 ,0xf3 ,0x2b ,0x80 ,0x90 ,0x79 ,0xe5 ,0x62 ,0xf8 ,0xea ,0xd2 ,0x52 ,0x2c ,0x6b ,0x7b ]);
		let expandedKey = secswans._keyExpansion(key);
		let expectedExpandedKey = new Uint8Array([
										0x8e,0x73,0xb0,0xf7,
										0xda,0x0e,0x64,0x52,
										0xc8,0x10,0xf3,0x2b,
										0x80,0x90,0x79,0xe5,
										0x62,0xf8,0xea,0xd2,
										0x52,0x2c,0x6b,0x7b,
										0xfe,0x0c,0x91,0xf7,
										0x24,0x02,0xf5,0xa5,
										0xec,0x12,0x06,0x8e,
										0x6c,0x82,0x7f,0x6b,
										0x0e,0x7a,0x95,0xb9,
										0x5c,0x56,0xfe,0xc2,
										0x4d,0xb7,0xb4,0xbd,
										0x69,0xb5,0x41,0x18,
										0x85,0xa7,0x47,0x96,
										0xe9,0x25,0x38,0xfd,
										0xe7,0x5f,0xad,0x44,
										0xbb,0x09,0x53,0x86,
										0x48,0x5a,0xf0,0x57,
										0x21,0xef,0xb1,0x4f,
										0xa4,0x48,0xf6,0xd9,
										0x4d,0x6d,0xce,0x24,
										0xaa,0x32,0x63,0x60,
										0x11,0x3b,0x30,0xe6,
										0xa2,0x5e,0x7e,0xd5,
										0x83,0xb1,0xcf,0x9a,
										0x27,0xf9,0x39,0x43,
										0x6a,0x94,0xf7,0x67,
										0xc0,0xa6,0x94,0x07,
										0xd1,0x9d,0xa4,0xe1,
										0xec,0x17,0x86,0xeb,
										0x6f,0xa6,0x49,0x71,
										0x48,0x5f,0x70,0x32,
										0x22,0xcb,0x87,0x55,
										0xe2,0x6d,0x13,0x52,
										0x33,0xf0,0xb7,0xb3,
										0x40,0xbe,0xeb,0x28,
										0x2f,0x18,0xa2,0x59,
										0x67,0x47,0xd2,0x6b,
										0x45,0x8c,0x55,0x3e,
										0xa7,0xe1,0x46,0x6c,
										0x94,0x11,0xf1,0xdf,
										0x82,0x1f,0x75,0x0a,
										0xad,0x07,0xd7,0x53,
										0xca,0x40,0x05,0x38,
										0x8f,0xcc,0x50,0x06,
										0x28,0x2d,0x16,0x6a,
										0xbc,0x3c,0xe7,0xb5,
										0xe9,0x8b,0xa0,0x6f,
										0x44,0x8c,0x77,0x3c,
										0x8e,0xcc,0x72,0x04,
										0x01,0x00,0x22,0x02
		]);
		
		assert.lengthOf( expandedKey, expectedExpandedKey.length, "the expanded key should have the expected length");
		assert.deepEqual( expandedKey, expectedExpandedKey);
	});
	
	it("should expand the 32 bytes key", function(){
		let key = new Uint8Array([ 0x60 ,0x3d ,0xeb ,0x10,
								   0x15 ,0xca ,0x71 ,0xbe,
								   0x2b ,0x73 ,0xae ,0xf0,
								   0x85 ,0x7d ,0x77 ,0x81,
								   0x1f ,0x35 ,0x2c ,0x07,
								   0x3b ,0x61 ,0x08 ,0xd7,
								   0x2d ,0x98 ,0x10 ,0xa3, 
								   0x09 ,0x14 ,0xdf ,0xf4 ]);
		let expandedKey = secswans._keyExpansion(key);
		let expectedExpandedKey = new Uint8Array([
										0x60,0x3d,0xeb,0x10,
										0x15,0xca,0x71,0xbe,
										0x2b,0x73,0xae,0xf0,
										0x85,0x7d,0x77,0x81,
										0x1f,0x35,0x2c,0x07,
										0x3b,0x61,0x08,0xd7,
										0x2d,0x98,0x10,0xa3,
										0x09,0x14,0xdf,0xf4,
										0x9b,0xa3,0x54,0x11,
										0x8e,0x69,0x25,0xaf,
										0xa5,0x1a,0x8b,0x5f,
										0x20,0x67,0xfc,0xde,
										0xa8,0xb0,0x9c,0x1a,
										0x93,0xd1,0x94,0xcd,
										0xbe,0x49,0x84,0x6e,
										0xb7,0x5d,0x5b,0x9a,
										0xd5,0x9a,0xec,0xb8,
										0x5b,0xf3,0xc9,0x17,
										0xfe,0xe9,0x42,0x48,
										0xde,0x8e,0xbe,0x96,
										0xb5,0xa9,0x32,0x8a,
										0x26,0x78,0xa6,0x47,
										0x98,0x31,0x22,0x29,
										0x2f,0x6c,0x79,0xb3,
										0x81,0x2c,0x81,0xad,
										0xda,0xdf,0x48,0xba,
										0x24,0x36,0x0a,0xf2,
										0xfa,0xb8,0xb4,0x64,
										0x98,0xc5,0xbf,0xc9,
										0xbe,0xbd,0x19,0x8e,
										0x26,0x8c,0x3b,0xa7,
										0x09,0xe0,0x42,0x14,
										0x68,0x00,0x7b,0xac,
										0xb2,0xdf,0x33,0x16,
										0x96,0xe9,0x39,0xe4,
										0x6c,0x51,0x8d,0x80,
										0xc8,0x14,0xe2,0x04,
										0x76,0xa9,0xfb,0x8a,
										0x50,0x25,0xc0,0x2d,
										0x59,0xc5,0x82,0x39,
										0xde,0x13,0x69,0x67,
										0x6c,0xcc,0x5a,0x71,
										0xfa,0x25,0x63,0x95,
										0x96,0x74,0xee,0x15,
										0x58,0x86,0xca,0x5d,
										0x2e,0x2f,0x31,0xd7,
										0x7e,0x0a,0xf1,0xfa,
										0x27,0xcf,0x73,0xc3,
										0x74,0x9c,0x47,0xab,
										0x18,0x50,0x1d,0xda,
										0xe2,0x75,0x7e,0x4f,
										0x74,0x01,0x90,0x5a,
										0xca,0xfa,0xaa,0xe3,
										0xe4,0xd5,0x9b,0x34,
										0x9a,0xdf,0x6a,0xce,
										0xbd,0x10,0x19,0x0d,
										0xfe,0x48,0x90,0xd1,
										0xe6,0x18,0x8d,0x0b,
										0x04,0x6d,0xf3,0x44,
										0x70,0x6c,0x63,0x1e,
		]);
		
		assert.lengthOf( expandedKey, expectedExpandedKey.length, "the expanded key should have the expected length");
		assert.deepEqual( expandedKey, expectedExpandedKey);
	});
	
	
});



describe("testing the addRoungKey of aes", function(){
	it("should add the right word of an expanded key", function(){
		let state = null; 
		let result = null;
		let expected = null;
		let key = new Uint8Array([  0x2b ,0x7e ,0x15 ,0x16 ,0x28 ,0xae ,0xd2 ,0xa6 ,0xab ,0xf7 ,0x15 ,0x88 ,0x09 ,0xcf ,0x4f ,0x3c]);
		let expandedKey = secswans._keyExpansion(key);
		
		// add the first word
		state = new Uint8Array([0x32 ,0x43 ,0xf6 ,0xa8 ,0x88 ,0x5a ,0x30 ,0x8d ,0x31 ,0x31 ,0x98 ,0xa2 ,0xe0 ,0x37 ,0x07 ,0x34]);
		secswans._addRoundKey(state, expandedKey);
		expected = new Uint8Array([0x19 ,0x3d ,0xe3 ,0xbe ,0xa0 ,0xf4 ,0xe2 ,0x2b ,0x9a ,0xc6 ,0x8d ,0x2a ,0xe9 ,0xf8 ,0x48 ,0x08]);
		assert.deepEqual( state, expected, "state should match expected result after first addRoundKey operation");
		
		// add the second word
		state = new Uint8Array([0x04 ,0x66 ,0x81 ,0xe5 ,0xe0 ,0xcb ,0x19 ,0x9a ,0x48 ,0xf8 ,0xd3 ,0x7a ,0x28 ,0x06 ,0x26 ,0x4c]);
		secswans._addRoundKey(state, expandedKey, 16);
		expected = new Uint8Array([0xa4 ,0x9c ,0x7f ,0xf2 ,0x68 ,0x9f ,0x35 ,0x2b ,0x6b ,0x5b ,0xea ,0x43 ,0x02 ,0x6a ,0x50 ,0x49]);
		assert.deepEqual( state, expected, "state should match expected result after second addRoundKey operation");
	});
});

describe("test subBytes of aes", function(){
	it("should perform subBytes operation on a state", function(){
			let state = new Uint8Array([0x19 ,0x3d ,0xe3 ,0xbe ,0xa0 ,0xf4 ,0xe2 ,0x2b ,0x9a ,0xc6 ,0x8d ,0x2a ,0xe9 ,0xf8 ,0x48 ,0x08]);
			let expected = new Uint8Array([0xd4 ,0x27 ,0x11 ,0xae ,0xe0 ,0xbf ,0x98 ,0xf1 ,0xb8 ,0xb4 ,0x5d ,0xe5 ,0x1e ,0x41 ,0x52 ,0x30]);
			secswans._SubBytes(state, secswans.sbox);
			assert.deepEqual(state, expected);
	});
	
	
	it("should perform subBytes operation on a state (2)", function(){
			let state = new Uint8Array([0xa4 ,0x9c ,0x7f ,0xf2 ,0x68 ,0x9f ,0x35 ,0x2b ,0x6b ,0x5b ,0xea ,0x43 ,0x02 ,0x6a ,0x50 ,0x49]);
			let expected = new Uint8Array([0x49 ,0xde ,0xd2 ,0x89 ,0x45 ,0xdb ,0x96 ,0xf1 ,0x7f ,0x39 ,0x87 ,0x1a ,0x77 ,0x02 ,0x53 ,0x3b]);
			secswans._SubBytes(state, secswans.sbox);
			assert.deepEqual(state, expected);
	});
});

describe("test inverseSubBytes of aes", function(){
	
	it("should perform reverse subBytes operation on a state", function(){
			let state = new Uint8Array([0x7a,0xd5,0xfd,0xa7,0x89,0xef,0x4e,0x27,0x2b,0xca,0x10,0x0b,0x3d,0x9f,0xf5,0x9f]);
			let expected = new Uint8Array([0xbd,0xb5,0x21,0x89,0xf2,0x61,0xb6,0x3d,0x0b,0x10,0x7c,0x9e,0x8b,0x6e,0x77,0x6e]);
			secswans._SubBytes(state, secswans.reverseSbox);
			assert.deepEqual(state, expected);
	});
	
	it("should perform reverse subBytes operation on a state (2)", function(){
			let state = new Uint8Array([0x54,0xd9,0x90,0xa1,0x6b,0xa0,0x9a,0xb5,0x96,0xbb,0xf4,0x0e,0xa1,0x11,0x70,0x2f]);
			let expected = new Uint8Array([0xfd,0xe5,0x96,0xf1,0x05,0x47,0x37,0xd2,0x35,0xfe,0xba,0xd7,0xf1,0xe3,0xd0,0x4e]);
			secswans._SubBytes(state, secswans.reverseSbox);
			assert.deepEqual(state, expected);
	});
	
	
});


describe("test ShiftRows of aes", function(){
	
	it("should perform ShiftRows operation on a state", function(){
			let state = new Uint8Array([0x63,0xca,0xb7,0x04,0x09,0x53,0xd0,0x51,0xcd,0x60,0xe0,0xe7,0xba,0x70,0xe1,0x8c]);
			let expected = new Uint8Array([0x63,0x53,0xe0,0x8c,0x09,0x60,0xe1,0x04,0xcd,0x70,0xb7,0x51,0xba,0xca,0xd0,0xe7]);
			secswans._ShiftRows(state);
			assert.deepEqual(state, expected);
	});
	
	it("should perform reverse subBytes operation on a state (2)", function(){
			let state = new Uint8Array([0xa7,0x61,0xca,0x9b,0x97,0xbe,0x8b,0x45,0xd8,0xad,0x1a,0x61,0x1f,0xc9,0x73,0x69]);
			let expected = new Uint8Array([0xa7,0xbe,0x1a,0x69,0x97,0xad,0x73,0x9b,0xd8,0xc9,0xca,0x45,0x1f,0x61,0x8b,0x61]);
			secswans._ShiftRows(state);
			assert.deepEqual(state, expected);
	});
});


describe("test inverse ShiftRows of aes", function(){
	
	it("should perform reverse ShiftRows operation on a state", function(){
			let state = new Uint8Array([0xbd,0xb5,0x21,0x89,0xf2,0x61,0xb6,0x3d,0x0b,0x10,0x7c,0x9e,0x8b,0x6e,0x77,0x6e]);
			let expected = new Uint8Array([0xbd,0x6e,0x7c,0x3d,0xf2,0xb5,0x77,0x9e,0x0b,0x61,0x21,0x6e,0x8b,0x10,0xb6,0x89]);
			secswans._InvShiftRows(state);
			assert.deepEqual(state, expected);
	});
	
	it("should perform reverse subBytes operation on a state (2)", function(){
			let state = new Uint8Array([0xfd,0xe5,0x96,0xf1,0x05,0x47,0x37,0xd2,0x35,0xfe,0xba,0xd7,0xf1,0xe3,0xd0,0x4e]);
			let expected = new Uint8Array([0xfd,0xe3,0xba,0xd2,0x05,0xe5,0xd0,0xd7,0x35,0x47,0x96,0x4e,0xf1,0xfe,0x37,0xf1]);
			secswans._InvShiftRows(state);
			assert.deepEqual(state, expected);
	});
	
	
});


describe("test MixColumns of aes", function(){
	
	it("should perform MixColumns operation on a state", function(){
			let state = new Uint8Array([0xa7,0xbe,0x1a,0x69,0x97,0xad,0x73,0x9b,0xd8,0xc9,0xca,0x45,0x1f,0x61,0x8b,0x61]);
			let expected = new Uint8Array([0xff,0x87,0x96,0x84,0x31,0xd8,0x6a,0x51,0x64,0x51,0x51,0xfa,0x77,0x3a,0xd0,0x09]);
			secswans._MixColumns(state);
			assert.deepEqual(state, expected);
	});
	
	it("should perform MixColumns operation on a state (2)", function(){
			let state = new Uint8Array([0x3b,0xd9,0x22,0x68,0xfc,0x74,0xfb,0x73,0x57,0x67,0xcb,0xe0,0xc0,0x59,0x0e,0x2d]);
			let expected = new Uint8Array([0x4c,0x9c,0x1e,0x66,0xf7,0x71,0xf0,0x76,0x2c,0x3f,0x86,0x8e,0x53,0x4d,0xf2,0x56]);
			secswans._MixColumns(state);
			assert.deepEqual(state, expected);
	});
});



describe("test inverse MixColumns of aes", function(){
	
	it("should perform MixColumns operation on a state", function(){
			let state = new Uint8Array([0xbd,0x6e,0x7c,0x3d,0xf2,0xb5,0x77,0x9e,0x0b,0x61,0x21,0x6e,0x8b,0x10,0xb6,0x89]);
			let expected = new Uint8Array([0x47,0x73,0xb9,0x1f,0xf7,0x2f,0x35,0x43,0x61,0xcb,0x01,0x8e,0xa1,0xe6,0xcf,0x2c]);
			secswans._InvMixColumns(state);
			assert.deepEqual(state, expected);
	});
	
	it("should perform MixColumns operation on a state (2)", function(){
			let state = new Uint8Array([0xfd,0xe3,0xba,0xd2,0x05,0xe5,0xd0,0xd7,0x35,0x47,0x96,0x4e,0xf1,0xfe,0x37,0xf1]);
			let expected = new Uint8Array([0x2d,0x7e,0x86,0xa3,0x39,0xd9,0x39,0x3e,0xe6,0x57,0x0a,0x11,0x01,0x90,0x4e,0x16]);
			secswans._InvMixColumns(state);
			assert.deepEqual(state, expected);
	});
});




describe("test full aes encrypt algorithm for a single block with aesCypherBlock method", function(){
	it("should produce the right encryption with a 16B key", function(){
		let state = new Uint8Array([0x00,0x11,0x22,0x33,0x44,0x55,0x66,0x77,0x88,0x99,0xaa,0xbb,0xcc,0xdd,0xee,0xff]);
		let key = new Uint8Array([0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f]);
		let expected = new Uint8Array([0x69,0xc4,0xe0,0xd8,0x6a,0x7b,0x04,0x30,0xd8,0xcd,0xb7,0x80,0x70,0xb4,0xc5,0x5a]);
		secswans.aesCypherBlock(state, key);
		assert.deepEqual(state, expected);
	});
	
	it("should produce the right encryption with a 24B key", function(){
		let state = new Uint8Array([0x00,0x11,0x22,0x33,0x44,0x55,0x66,0x77,0x88,0x99,0xaa,0xbb,0xcc,0xdd,0xee,0xff]);
		let key = new Uint8Array([0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17]);
		let expected = new Uint8Array([0xdd,0xa9,0x7c,0xa4,0x86,0x4c,0xdf,0xe0,0x6e,0xaf,0x70,0xa0,0xec,0x0d,0x71,0x91]);
		secswans.aesCypherBlock(state, key);
		assert.deepEqual(state, expected);
	});
	
	it("should produce the right encryption with a 32B key", function(){
		let state = new Uint8Array([0x00,0x11,0x22,0x33,0x44,0x55,0x66,0x77,0x88,0x99,0xaa,0xbb,0xcc,0xdd,0xee,0xff]);
		let key = new Uint8Array([0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1a,0x1b,0x1c,0x1d,0x1e,0x1f]);
		let expected = new Uint8Array([0x8e,0xa2,0xb7,0xca,0x51,0x67,0x45,0xbf,0xea,0xfc,0x49,0x90,0x4b,0x49,0x60,0x89]);
		secswans.aesCypherBlock(state, key);
		assert.deepEqual(state, expected);
	});
	
});





describe("test full aes decrypt algorithm for a single block with aesCypherBlock method", function(){
	it("should produce the right decryption with a 16B key", function(){
		let expected = new Uint8Array([0x00,0x11,0x22,0x33,0x44,0x55,0x66,0x77,0x88,0x99,0xaa,0xbb,0xcc,0xdd,0xee,0xff]);
		let key = new Uint8Array([0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f]);
		let state = new Uint8Array([0x69,0xc4,0xe0,0xd8,0x6a,0x7b,0x04,0x30,0xd8,0xcd,0xb7,0x80,0x70,0xb4,0xc5,0x5a]);
		secswans.aesInverseCypherBlock(state, key);
		assert.deepEqual(state, expected);
	});
	
	it("should produce the right decryption with a 24B key", function(){
		let expected = new Uint8Array([0x00,0x11,0x22,0x33,0x44,0x55,0x66,0x77,0x88,0x99,0xaa,0xbb,0xcc,0xdd,0xee,0xff]);
		let key = new Uint8Array([0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17]);
		let state = new Uint8Array([0xdd,0xa9,0x7c,0xa4,0x86,0x4c,0xdf,0xe0,0x6e,0xaf,0x70,0xa0,0xec,0x0d,0x71,0x91]);
		secswans.aesInverseCypherBlock(state, key);
		assert.deepEqual(state, expected);
	});
	
	it("should produce the right decryption with a 32B key", function(){
		let expected = new Uint8Array([0x00,0x11,0x22,0x33,0x44,0x55,0x66,0x77,0x88,0x99,0xaa,0xbb,0xcc,0xdd,0xee,0xff]);
		let key = new Uint8Array([0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1a,0x1b,0x1c,0x1d,0x1e,0x1f]);
		let state = new Uint8Array([0x8e,0xa2,0xb7,0xca,0x51,0x67,0x45,0xbf,0xea,0xfc,0x49,0x90,0x4b,0x49,0x60,0x89]);
		secswans.aesInverseCypherBlock(state, key);
		assert.deepEqual(state, expected);
	});
	
});


describe("test aes in cbc mode", function(){
	it("should encrypt with cbc mode a 48B array with an init vector", function(){
		let initVector = new Uint8Array([
			0xc7,0x82,0xdc,0x4c,0x09,0x8c,0x66,0xcb,0xd9,0xcd,0x27,0xd8,0x25,0x68,0x2c,0x81
		]);
		let input = new Uint8Array([
			0xd0,0xa0,0x2b,0x38,0x36,0x45,0x17,0x53,0xd4,0x93,0x66,0x5d,0x33,0xf0,0xe8,0x86,
			0x2d,0xea,0x54,0xcd,0xb2,0x93,0xab,0xc7,0x50,0x69,0x39,0x27,0x67,0x72,0xf8,0xd5,
			0x02,0x1c,0x19,0x21,0x6b,0xad,0x52,0x5c,0x85,0x79,0x69,0x5d,0x83,0xba,0x26,0x84,
		]);
		let key = new Uint8Array([0x6c,0x3e,0xa0,0x47,0x76,0x30,0xce,0x21,0xa2,0xce,0x33,0x4a,0xa7,0x46,0xc2,0xcd]);
		let expected = new Uint8Array([
			0xb3,0xd2,0xb2,0xed,0x4f,0xe7,0xf6,0xd7,0x41,0x97,0x91,0x81,0xbb,0x98,0xad,0x59,
			0x93,0xb9,0xf0,0x6b,0x9d,0xa7,0x0e,0x6f,0x69,0x9f,0xb1,0xdb,0x90,0xee,0x81,0x64,
			0x2b,0x66,0x89,0xb6,0x48,0x4e,0xe8,0x59,0x97,0x81,0x86,0x13,0x51,0xb4,0x46,0x4e,
		]);

		let result = secswans.aesCbcCypher(input, key, initVector);
		assert.deepEqual(result, expected);
	});
	
	it("should encrypt with cbc mode a 48B array with an empty unspecified init vector", function(){
		let input = new Uint8Array([
			0xd0,0xa0,0x2b,0x38,0x36,0x45,0x17,0x53,0xd4,0x93,0x66,0x5d,0x33,0xf0,0xe8,0x86,
			0x2d,0xea,0x54,0xcd,0xb2,0x93,0xab,0xc7,0x50,0x69,0x39,0x27,0x67,0x72,0xf8,0xd5,
			0x02,0x1c,0x19,0x21,0x6b,0xad,0x52,0x5c,0x85,0x79,0x69,0x5d,0x83,0xba,0x26,0x84,
		]);
		let key = new Uint8Array([0x6c,0x3e,0xa0,0x47,0x76,0x30,0xce,0x21,0xa2,0xce,0x33,0x4a,0xa7,0x46,0xc2,0xcd]);
		let expected = new Uint8Array([
			0xda,0x34,0xda,0x5c,0x68,0xb7,0x61,0x7e,0x0f,0x4e,0xfa,0x94,0xd9,0x25,0x92,0xb3,
			0xc9,0xfc,0x5b,0x05,0xb1,0x06,0xdd,0xe1,0x85,0x11,0xf3,0x8f,0xa9,0x33,0xe2,0x7b,
			0xd5,0x2c,0x95,0xa4,0xa0,0xb1,0xf6,0x8a,0xcc,0x8f,0xd0,0x1e,0x38,0xea,0x0c,0xe2,
		]);

		let result = secswans.aesCbcCypher(input, key);
		assert.deepEqual(result, expected);
	});
	
	it("should encrypt with cbc mode a 44B array with automatic 0 padding", function(){
		let input = new Uint8Array([
			0xd0,0xa0,0x2b,0x38,0x36,0x45,0x17,0x53,0xd4,0x93,0x66,0x5d,0x33,0xf0,0xe8,0x86,
			0x2d,0xea,0x54,0xcd,0xb2,0x93,0xab,0xc7,0x50,0x69,0x39,0x27,0x67,0x72,0xf8,0xd5,
			0x02,0x1c,0x19,0x21,0x6b,0xad,0x52,0x5c,0x85,0x79,0x69,0x5d,
		]);
		let key = new Uint8Array([0x6c,0x3e,0xa0,0x47,0x76,0x30,0xce,0x21,0xa2,0xce,0x33,0x4a,0xa7,0x46,0xc2,0xcd]);
		let expected = new Uint8Array([
			0xda,0x34,0xda,0x5c,0x68,0xb7,0x61,0x7e,0x0f,0x4e,0xfa,0x94,0xd9,0x25,0x92,0xb3,
			0xc9,0xfc,0x5b,0x05,0xb1,0x06,0xdd,0xe1,0x85,0x11,0xf3,0x8f,0xa9,0x33,0xe2,0x7b,
			0x7b,0xc3,0x11,0x6b,0xc8,0x36,0x56,0xaf,0x55,0x53,0x51,0xf6,0x94,0x55,0x14,0x1a
		]);

		let result = secswans.aesCbcCypher(input, key);
		assert.deepEqual(result, expected);
	});
	
	
	
	it("should decrypt with cbc mode a 48B array with an init vector", function(){
		let initVector = new Uint8Array([
			0xc7,0x82,0xdc,0x4c,0x09,0x8c,0x66,0xcb,0xd9,0xcd,0x27,0xd8,0x25,0x68,0x2c,0x81
		]);
		let expectedDecrypted = new Uint8Array([
			0xd0,0xa0,0x2b,0x38,0x36,0x45,0x17,0x53,0xd4,0x93,0x66,0x5d,0x33,0xf0,0xe8,0x86,
			0x2d,0xea,0x54,0xcd,0xb2,0x93,0xab,0xc7,0x50,0x69,0x39,0x27,0x67,0x72,0xf8,0xd5,
			0x02,0x1c,0x19,0x21,0x6b,0xad,0x52,0x5c,0x85,0x79,0x69,0x5d,0x83,0xba,0x26,0x84,
		]);
		let key = new Uint8Array([0x6c,0x3e,0xa0,0x47,0x76,0x30,0xce,0x21,0xa2,0xce,0x33,0x4a,0xa7,0x46,0xc2,0xcd]);
		let encrypted = new Uint8Array([
			0xb3,0xd2,0xb2,0xed,0x4f,0xe7,0xf6,0xd7,0x41,0x97,0x91,0x81,0xbb,0x98,0xad,0x59,
			0x93,0xb9,0xf0,0x6b,0x9d,0xa7,0x0e,0x6f,0x69,0x9f,0xb1,0xdb,0x90,0xee,0x81,0x64,
			0x2b,0x66,0x89,0xb6,0x48,0x4e,0xe8,0x59,0x97,0x81,0x86,0x13,0x51,0xb4,0x46,0x4e,
		]);

		let result = secswans.aesCbcInverseCypher(encrypted, key, initVector);
		assert.deepEqual(result, expectedDecrypted);
	});
	
	it("should decrypt with cbc mode a 48B array with an empty unspecified init vector", function(){
		let expectedDecrypted = new Uint8Array([
			0xd0,0xa0,0x2b,0x38,0x36,0x45,0x17,0x53,0xd4,0x93,0x66,0x5d,0x33,0xf0,0xe8,0x86,
			0x2d,0xea,0x54,0xcd,0xb2,0x93,0xab,0xc7,0x50,0x69,0x39,0x27,0x67,0x72,0xf8,0xd5,
			0x02,0x1c,0x19,0x21,0x6b,0xad,0x52,0x5c,0x85,0x79,0x69,0x5d,0x83,0xba,0x26,0x84,
		]);
		let key = new Uint8Array([0x6c,0x3e,0xa0,0x47,0x76,0x30,0xce,0x21,0xa2,0xce,0x33,0x4a,0xa7,0x46,0xc2,0xcd]);
		let encrypted = new Uint8Array([
			0xda,0x34,0xda,0x5c,0x68,0xb7,0x61,0x7e,0x0f,0x4e,0xfa,0x94,0xd9,0x25,0x92,0xb3,
			0xc9,0xfc,0x5b,0x05,0xb1,0x06,0xdd,0xe1,0x85,0x11,0xf3,0x8f,0xa9,0x33,0xe2,0x7b,
			0xd5,0x2c,0x95,0xa4,0xa0,0xb1,0xf6,0x8a,0xcc,0x8f,0xd0,0x1e,0x38,0xea,0x0c,0xe2,
		]);

		let result = secswans.aesCbcInverseCypher(encrypted, key);
		assert.deepEqual(result, expectedDecrypted);
	});
	
});


///////////////////////////////////////////////////////////////////////
/////////////////  END OF  AES TESTING     ////////////////////////////
///////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
//////////// TESTING BIG INTEGER METHODS ///////////////////////////////
////////////////////////////////////////////////////////////////////////


describe("testing the random method", function(){
		it("should create an empty Uint8Array", function(){
			let res = secswans._random(0);
			assert.instanceOf(res, Uint8Array);
			assert.lengthOf(res, 0);
		});
		
		it("should create a non null 256Bytes array", function(){
			let res = secswans._random(256);
			assert.instanceOf(res, Uint8Array);
			assert.lengthOf(res, 256);
			assert.notDeepEqual( res, new Uint8Array(256));
		});
	
});


describe("testing the power method", function(){
	
	it("should compute 2 ^ 1 mod 2^15+1", function(){
		let a = new Uint8Array([2]);
		let b = new Uint8Array([1]);
		let m = new Uint8Array([0x80,1]);
		let expected = new Uint8Array([2]);
		let result = secswans.power(a,b, m);
		assert.deepEqual(result, expected);
	});
	
	it("should compute 2 ^ 2 mod 2^15+1", function(){
		let a = new Uint8Array([2]);
		let b = new Uint8Array([2]);
		let m = new Uint8Array([0x80,1]);
		let expected = new Uint8Array([4]);
		let result = secswans.power(a,b, m);
		assert.deepEqual(result, expected);
	});
	
	it("should compute 2 ^ 3 mod 2^15+1", function(){
		let a = new Uint8Array([2]);
		let b = new Uint8Array([3]);
		let m = new Uint8Array([0x80,1]);
		let expected = new Uint8Array([ 8]);
		let result = secswans.power(a,b, m);
		assert.deepEqual(result, expected);
	});
	
	it("should compute 2 ^ 10 mod 2^15+1", function(){
		let a = new Uint8Array([2]);
		let b = new Uint8Array([10]);
		let m = new Uint8Array([0x80,1]);
		let expected = new Uint8Array([ 4,0]);
		let result = secswans.power(a,b, m);
		assert.deepEqual(result, expected);
	});
	
	it("should compute 0xabcdef ^ 0xef", function(){
		let a = new Uint8Array([0xab,0xcd,0xef]);
		let b = new Uint8Array([0xef]);
		let expected = secswans._hexstrToUint8Array("5992bd0c790caf80111690527a861d50362facc8445dcd1e2182505a5d103559536ef12f5c1558edf9ee3708ab676104148e8994104ddaf41a88cae1572eea6858f73c07fbe1cea2e9c5d930fc58ea7eab641503b00c3c69298c905495c0158a9c4d1d5cb99f9eda82acb7222e733c9d3a5e49106652f36af1a98aed5ff5f62bac402eb19f8232fc440ca6eb3bc8433e740bd0b46713def94148a0908d56eb37e4ab91a8417f4326229c1f175a8b1bdedac366296dda766d00fb5722683b277332c4a5aa27746f53c5daad896a4520c8d4a09fd7e91e27a6c66b161df3d0c6416c2d7bb6a3edb3bb4cb3ad5c95fc1375bf38546acede079a7702cfb2775f343ea95b753e28355f89ec645778237e7f36150771e15f35c8b7a305eaccae5a53d707eed981bbc34102fbe837c8d05f701efac0045c6feb2928e4f41ce526dbb3c5de8227fb415d45cd56af1aed795a6cdc6683a70d71ad332e2ce4fcc9e4522d09519ddbf0f910f248d9db9ca9c31b461d4b60a6a3c343e0093ee9443692f8c95d8e82cebc294f975b7aab133cc97a385e7ee4b50e93d1e0029aec77ea4182593835f11fce932e1f6b2d1ffa881eaa50629dfbef67995202be50496af529e8b8ed5f7e9ff14b2fd58141c98916cf7b608d6d5d768dd838de51d4dc113aaf195ab7bdbbc2a1ccf294098016965a31e2bd2bcba48b4419eaf44134c3c5c3ca8b9a9ff5ebf939cc7785c390306c647286160f925bcf9b65f365f7ea58e39d15ea6e12bdc11259609f9777aaa41b967d718bb2496c67ce76334cf41d8c48df139aa0934167581550b010cac8beb7a326f9d8a75ed265c170c02ab645876b1dce5fc2c348048e7a1f9d1a26908e8c79b837f9a1ac075522bd6c9bc94aac55aff04721b50049089c8478dbf32a5ae4829a0782c897e55af4480a7a0b479fc728b5f5de765745ae8435afb54711d1cbb7fb81b493a154d4cb1072a3da2e137a0f");
		
		let m = new Uint8Array(expected.length+1);
		m[0] = 1;
		m[m.length-1] = 1;
		
		let result = secswans.power(a, b, m);
		assert.deepEqual( result, expected );
	});
	
	it("should compute 0x1234567890ABCD^0x0123", function(){
		let a = new Uint8Array([0x12,0x34,0x56,0x78,0x90,0xAB,0xCD]);
		let b = new Uint8Array([0x01,0x23]);
		
		var expectedStr = "048fe0a7121f2bff3281cf752eb3fa4500c74b24fa8dde37b34be8d8eedc6f7168ca2c60556541b592918a5fa4141d324cd9883a283b98d51c01f705a87d8e9692c923ea3093a4ab26da53314a9fd7d7496aac360183e813abb445dc55d1abbc7c53bce8d75484a214faefa8303084de58c5123cf55a088694d73df5768f725cd6a669a7aaf416f3ff71bc945743e412f855b488dc118b6ca78a5cf2f75f8b872200df240f187b3ecc6f5192b1110dc4bc08fdfa801c03eec7addf45be249daeab1844c17593daaf29934f021c06e990f6323552f224bb955f6b7810f6f6574e44a5d733835d6107f3be98ba15e051df38f2b0e3da1e5cc1bf450ecad1f9fb6d7feb146beff7d546a66da9562135a1908ae2bd30e275201c57ca6657dcde85cfbb9320551efd89de5c19233eba4825a373b388b1b9601c11dd8153323508eac5c52e70e97d88711d844bb8bcad4b9978a37e25f3ba3f2694e360055fa213cb9ff2021ac1e3ff9339ef690c73700ff64a7cb2fe4b61da1daf3b79fd95ae6eb8c8f9ce8494a8cbe48f01581f5314ac66c06fa861f59d288ac0d37f2d94c7b44c1f04e77b53caeec33ade272d6a8a2642a867ad7938b997aa1dc0267f6dbc80c7350cc1ef43ebecc74313126142675eb1bf2f18a5fdb72ea66d595e6c16c3290de551bed94fd9a4e11c0a0d59d98962e586919bb55bd13ed65840aca7015c257e547d4464277a489f0c85963155460e105c94226b5f97cca13c4175c1dd59d858d72f5ebd0de5cfc394ec44f8d347c469f5ad01ec25480b6a9934d1e9c90edfa84f418d4fbdd066a87bb4f51c1dac8d4ac0a9efa99c106719e07d4ae1936cc9222b0788c839d79d51b24692c40fced60b8d2d71aa68bc65f2b643526ca62b72c89e046c0004517c5953d99ca9162790c2480164797677a9a6e1f3ed7f3923d1c7ccf77f28eb44ca8a7b7a667c6cb75bda716181b799e4f2d45b7ecb68379d7f8f7106d7a37e3b54ab82ae6373df0294ea1f0cbf350d8e0068124b6ff0086e75432514febbe73d5d945ad3782d51b95eaa679bfd8c87c6d7a6de0306b9a92add4b33ac82ede7020245618d04ea22031ef334e603df8c9da7290133c387e9451d78422d93a349f415b06fa9b2875ba79a829740e2ee25754bd6f61c47f5f8399e4537dcfc30486003115c2b9b19d6d3cd61a98371702ec124710c389cfe3a373bc8639200383c7c2db7ad11949c55c0b5c472b3c41341e2acd1bc456c7e9c21a4238de51ea7c2aa1428cebd0233d7fd072defa409e72fc91681a8da0cba12e868eae821433954d9c5171c0f24096eda39b324ef00e06b35850b38159f87cbe20658e6077314d623de6f488e59ef48d71a8960bb15485946944319be5f7bac27221aaa14016d9fa372b9ac3541ec9262d35e18dcf408e08bfdb5233f2f1d221b36e430038ddd70c7fa79d670f7c87e47b2d1d99f073e38629b3544c28db9fba955a779ccf3228d15bc3475d567dccd7ca50306b81d3e7bd11ef96dd111685a9e89b4f3496829781aad2e145028e433b5c6d4a458aa78be14205aff41cc765e5b626427aaf2e88cdaaf4828b261398b500b2b85e21fec454b220b71b620f2658edfc98e7cefdd45b400a3d967b4ddcbcbf0dbf1ef97b63318137eaf8e14a443bb4a7d6189d441194668a045abac74f4fae50e56c9e313ddf2416457bed429b7c1608c3852a6a579a544ac1f714af7dbba58cb7b99e04db75d86b7aa7688843969d6ccd76420057a0c7036102bb50130214a4475a5d9dac27e932f2bf94432541c0d0c8fb209c4964e8d3d87e6dde315a0c7639593144a771e32e3044c0ae82efaa4d89347a5187b95bf30c623acd8a93be3a215844cf475606451c0cfca965f42a4ab8b1364330c1bae3abb1e77c894aa7a21991207a468cbb507c28dae48650e77f07579fb6b6c0c9cb5ed8936463eb085a8ddc0069ef890d41807267d989a27059050c2062d2cd8b0c711221d7ab43f1958639f751fb370367d570cad97316e8f1f9557ba81d7757ad79a425995f7d7887035651b45ec45c9b9da4e2eba182fae13eba517075f2c39f1f0263a120f2b0b1f65fbb367e74a484d9d9eb9fab1077076ddce536cd790b04ea4ea6cb1185eced0ee8e32b5aad3b983a6070825c84a63589047a2568e052b6c9a17e267ed586cd4a565ef23831b0ff47b8c35dfa27ef2a55139ae5976609b478afa1f1a35d5bd07045daa7778f4835204f663f50445ec7655241f814eb15eb63901aaca482a2dc76b6b9b74c2ad635e43f4a6d33eea54fa7dd2164c3e8ac1fdf1ce899c1cc61922a45f553283e66621262331e2a426ad7a90a7a2732214f98d8f785b5f6f109fd1916305444ae40b8795594f1a13d418a78f86d819221ffc7c5d6d9c6c607a05047453cda8bcda8174e5c67dde4d8bf4f0f491e678fd75337e9a490ca13eb40fae8c6cd6e38adcc384e16a244b89e72a35337acfe8ad9d71deea5c5f37f2dfc96d232acf428adbdf8bc81e4c2794c41d716249c8a07aaaa905c38505161b6d4a2b379d2135bdf584111d5e6f53b1988217d7e3fd59eb681de50c3ea302d0fb5b50373468327802270827a1c312319f778cf6308ffb3e753bdc88a2147cf2447c4818329578aa18aeeee4ef71fe1f677c2ea229c8744e64b12e7487cc55";
		
		var expected = new Uint8Array(expectedStr.length/2);
		for(let i=0; i<expectedStr.length;i+=2) expected[i/2] = parseInt(expectedStr.substr(i,2), 16);
		
		let m = new Uint8Array(expected.length+1);
		m[0] = 1;
		m[m.length-1] = 1;
		
		let result = secswans.power(a, b, m);
		assert.deepEqual( result, expected );
	});
	
	it("should compute 0x1234567890ABCDEF^0x0123 % 0xabcdef1234567891", function(){
		let a = new Uint8Array([0x12,0x34,0x56,0x78,0x90,0xAB,0xCD,0xEF]);
		let b = new Uint8Array([0x01,0x23]);
		let m = new Uint8Array([0xab,0xcd,0xef,0x12,0x34,0x56,0x78,0x91]);
		var expected = new Uint8Array([0x1f,0xed ,0xf8,0xa5 ,0xbc,0x51 ,0x25,0x37]);
		let result = secswans.power(a, b, m);
		assert.deepEqual( result, expected );
	});
	
	it("should compute 0x1234567890ABCDEF^0x0123 % 0xa1b2c3d4e5f7", function(){
		let a = new Uint8Array([0x12,0x34,0x56,0x78,0x90,0xAB,0xCD,0xEF]);
		let b = new Uint8Array([0x01,0x23]);
		let m = new Uint8Array([0xa1,0xb2,0xc3,0xd4,0xe5,0xf7]);
		var expected = new Uint8Array([ 0x76,0x8c ,0x65,0xa1 ,0xdd,0x5a]);
		let result = secswans.power(a, b, m);
		assert.deepEqual( result, expected );
		
	});
	
});

////////////////////////////////////////////////////////////////////////
//////////// END OF TESTING BIG INTEGER METHODS ////////////////////////
////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////
//////////////////////// SHA256 //////////////////////////////////////
//////////////////////////////////////////////////////////////////////

describe("testing the generating of first primes", function(){
	it("should generate the first prime", function(){
		let result = secswans._firstPrimes(1);
		let expected = [2];
		assert.deepEqual(result,expected);
	});
	
	it("should generate the first 20 primes", function(){
		let result = secswans._firstPrimes(20);
		let expected = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71 ];
		assert.deepEqual(result,expected);
	});
	
	it("should generate the first 100 primes", function(){
		let result = secswans._firstPrimes(100);
		let expected = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71,
						73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173,
						179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281,
						283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 
						419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509, 521, 523, 541 ];
		assert.deepEqual(result,expected);
	});
});


describe("testing the generation of K", function(){
		it("should generate the K array used in sha algo", function(){
			let result = secswans._generateShaK();
			let expected = new Uint32Array([
				0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
				0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
				0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
				0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
				0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
				0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
				0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
				0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
			]);
			assert.deepEqual(result, expected);
		});
});

describe("testing sha 256 padding", function(){
	it("should pad an array containing 0", function(){
		let input = new Uint8Array(1);
		let expected = new Uint32Array( [0x00800000, 0, 0,0,0,0,0,0,0,0,0,0,0,0,0,8] );
		let result = secswans._sha256Padding(input);
		assert.deepEqual(result, expected);
	});
	
	it("should pad an array of 55 Bytes", function(){
		let input = new Uint8Array(55);
		let expected = new Uint32Array([0,0,0,0,0,0,0,0,0,0,0,0,0,0x00000080,0,440]);
		let result = secswans._sha256Padding(input);
		assert.deepEqual(result, expected);
	});
	
	it("should pad an array of 56 Bytes", function(){
		let input = new Uint8Array([
							0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 
							0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x20,
							0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x30,
							0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x40,
							0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x50,
							0x51, 0x52, 0x53, 0x54, 0x55, 0x56,
							]);
		let expected = new Uint32Array([0x01020304,0x05060708,0x09101112,0x13141516,0x17181920,0x21222324, 0x25262728, 0x29303132, 0x33343536, 0x37383940, 0x41424344, 0x45464748, 0x49505152,
										0x53545556, 0x80000000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 448]);
		let result = secswans._sha256Padding(input);
		assert.deepEqual(result, expected);
	});

});


describe("test sha256", function(){
	it("should hash 0", function(){
		let input = new Uint8Array([0]);
		let expected = new Uint8Array([0x6e ,0x34 ,0x0b ,0x9c ,0xff ,0xb3 ,0x7a ,0x98 ,0x9c ,0xa5 ,0x44 ,0xe6 ,0xbb ,0x78 ,0x0a ,0x2c ,0x78 ,0x90 ,0x1d ,0x3f ,0xb3 ,0x37 ,0x38 ,0x76 ,0x85 ,0x11 ,0xa3 ,0x06 ,0x17 ,0xaf ,0xa0 ,0x1d]);
		let result = secswans.sha256(input);
		assert.deepEqual(result, expected);
	
	});
	
	it("should hash 'aa' in ascii : 01100001 01100001", function(){
		let input = new Uint8Array([0b01100001, 0b01100001]);
		let expected = new Uint8Array([0x96 ,0x1B ,0x6D ,0xD3 ,0xED ,0xE3 ,0xCB ,0x8E ,0xCB ,0xAA ,0xCB ,0xD6 ,0x8D ,0xE0 ,0x40 ,0xCD ,0x78 ,0xEB ,0x2E ,0xD5 ,0x88 ,0x91 ,0x30 ,0xCC ,0xEB ,0x4C ,0x49 ,0x26 ,0x8E ,0xA4 ,0xD5 ,0x06]);
		let result = secswans.sha256(input);
		assert.deepEqual(result, expected);
	});
	
	it("should hash 'abc' in ascii : 01100001 01100010 01100011", function(){
		let input = new Uint8Array([0b01100001, 0b01100010, 0b01100011]);
		let expected = new Uint8Array([0xba ,0x78 ,0x16 ,0xbf ,0x8f ,0x01 ,0xcf ,0xea ,0x41 ,0x41 ,0x40 ,0xde ,0x5d ,0xae ,0x22 ,0x23 ,0xb0 ,0x03 ,0x61 ,0xa3 ,0x96 ,0x17 ,0x7a ,0x9c ,0xb4 ,0x10 ,0xff ,0x61 ,0xf2 ,0x00 ,0x15 ,0xad]);
		let result = secswans.sha256(input);
		assert.deepEqual(result, expected);
	});
	
	it("should hash 'aaaa' in ascii : 01100001 01100001 01100001 01100001", function(){
		let input = new Uint8Array([0b01100001, 0b01100001, 0b01100001, 0b01100001]);
		let expected = new Uint8Array([0x61 ,0xBE ,0x55 ,0xA8 ,0xE2 ,0xF6 ,0xB4 ,0xE1 ,0x72 ,0x33 ,0x8B ,0xDD ,0xF1 ,0x84 ,0xD6 ,0xDB ,0xEE ,0x29 ,0xC9 ,0x88 ,0x53 ,0xE0 ,0xA0 ,0x48 ,0x5E ,0xCE ,0xE7 ,0xF2 ,0x7B ,0x9A ,0xF0 ,0xB4]);
		let result = secswans.sha256(input);
		assert.deepEqual(result, expected);
	});
	
	it("should hash 'abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq' in ascii (2 block sha256)", function(){
		let input = new Uint8Array([0x61 ,0x62 ,0x63 ,0x64 ,0x62 ,0x63 ,0x64 ,0x65 ,0x63 ,0x64 ,0x65 ,0x66 ,0x64 ,0x65 ,0x66 ,0x67 ,0x65 ,0x66 ,0x67 ,0x68 ,0x66 ,0x67 ,0x68 ,0x69 ,0x67 ,0x68 ,0x69 ,0x6a ,0x68 ,0x69 ,0x6a ,0x6b ,0x69 ,0x6a ,0x6b ,0x6c ,0x6a ,0x6b ,0x6c ,0x6d ,0x6b ,0x6c ,0x6d ,0x6e ,0x6c ,0x6d ,0x6e ,0x6f ,0x6d ,0x6e ,0x6f ,0x70 ,0x6e ,0x6f ,0x70 ,0x71]);
		let expected = new Uint8Array([0x24 ,0x8d ,0x6a ,0x61 ,0xd2 ,0x06 ,0x38 ,0xb8 ,0xe5 ,0xc0 ,0x26 ,0x93 ,0x0c ,0x3e ,0x60 ,0x39 ,0xa3 ,0x3c ,0xe4 ,0x59 ,0x64 ,0xff ,0x21 ,0x67 ,0xf6 ,0xec ,0xed ,0xd4 ,0x19 ,0xdb ,0x06 ,0xc1]);
		let result = secswans.sha256(input);
		assert.deepEqual(result, expected);
	
	});
	
	it("should hash 0xabcdef0123456789abcdef0123456789", function(){
		let input = new Uint8Array([0xab,0xcd,0xef,0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef,0x01,0x23,0x45,0x67,0x89]);
		let expected = new Uint8Array([0x3e,0x4c,0xa0,0x4a,0x7a,0x69,0xcf,0x22,0xad,0xa1,0xcc,0xc2,0x0c,0xde,0xc2,0xf6,0xd8,0xc0,0x98,0xe9,0x4e,0x57,0x76,0xf4,0x74,0x07,0xb2,0x89,0xc9,0xe3,0x8a,0x5c]);
		let result = secswans.sha256(input);
		assert.deepEqual(result, expected);
	});
	
	it("should hash 'a' * 1000000 in ascii ", function(){
		let input = new Uint8Array(1000000);
		for(let i in input) input[i] = 0b01100001;
		let expected = new Uint8Array([0xcd ,0xc7 ,0x6e ,0x5c  ,0x99 ,0x14 ,0xfb ,0x92  ,0x81 ,0xa1 ,0xc7 ,0xe2  ,0x84 ,0xd7 ,0x3e ,0x67  ,0xf1 ,0x80 ,0x9a ,0x48  ,0xa4 ,0x97 ,0x20 ,0x0e  ,0x04 ,0x6d ,0x39 ,0xcc  ,0xc7 ,0x11 ,0x2c ,0xd0]);
		let result = secswans.sha256(input);
		assert.deepEqual(result, expected);
	});
});

//////////////////////////////////////////////////////////////////////
///////////////  END OF SHA256  //////////////////////////////////////
//////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
///////////////// testing methods directly used by DH algo /////////////
////////////////////////////////////////////////////////////////////////

describe("generate keys for DH algo", function(){
	it("should create a key pair for DH", function(){
		this.timeout(5000);
		let pair = secswans._generateKeyDh( secswans.ENCTYPE_DH_2048_256 );
		assert.instanceOf( pair.public, Uint8Array);
		assert.instanceOf( pair.private, Uint8Array);
		assert.lengthOf( pair.public, 256);
		assert.ok( pair.private.length >= 32, "the length of the secret generated should be at least 32 Bytes : "+pair.private.length+" given");
	});
	
});


describe("should compute the right common key from a public key and a secret key", function(){
	let privateKey1 = secswans._hexstrToUint8Array("0881382CDB87660C6DC13E614938D5B9C8B2F248581CC5E31B35454397FCE50E");
	let privateKey2 = secswans._hexstrToUint8Array("7D62A7E3EF36DE617B13D1AFB82C780D83A23BD4EE6705645121F371F546A53D");
	let publicKey1 = secswans._hexstrToUint8Array("2E9380C8323AF97545BC4941DEB0EC3742C62FE0ECE824A6ABDBE66C59BEE0242911BFB967235CEBA35AE13E4EC752BE630B92DC4BDE2847A9C62CB8152745421FB7EB60A63C0FE9159FCCE726CE7CD8523D7450667EF840E4919121EB5F01C8C9B0D3D648A93BFB75689E8244AC134AF544711CE79A02DCC34226684780DDDCB498594106C37F5BC79856487AF5AB022A2E5E42F09897C1A85A11EA0212AF04D9B4CEBC937C3C1A3E15A8A0342E337615C84E7FE3B8B9B87FB1E73A15AF12A30D746E06DFC34F290D797CE51AA13AA785BF6658AFF5E4B093003CBEAF665B3C2E113A3A4E905269341DC0711426685F4EF37E868A8126FF3F2279B57CA67E29");
	let publicKey2 = secswans._hexstrToUint8Array("575F0351BD2B1B817448BDF87A6C362C1E289D3903A30B9832C5741FA250363E7ACBC7F77F3DACBC1F131ADD8E03367EFF8FBBB3E1C5784424809B25AFE4D2262A1A6FD2FAB64105CA30A674E07F7809852088632FC049233791AD4EDD083A978B883EE618BC5E0DD047415F2D95E683CF14826B5FBE10D3CE41C6C120C78AB20008C698BF7F0BCAB9D7F407BED0F43AFB2970F57F8D12043963E66DDD320D599AD9936C8F44137C08B180EC5E985CEBE186F3D549677E80607331EE17AF3380A725B0782317D7DD43F59D7AF9568A9BB63A84D365F92244ED120988219302F42924C7CA90B89D24F71B0AB697823D7DEB1AFF5B0E8E4A45D49F7F53757E1913");
	let commonKey = secswans._hexstrToUint8Array("86C70BF8D0BB81BB01078A17219CB7D27203DB2A19C877F1D1F19FD7D77EF22546A68F005AD52DC84553B78FC60330BE51EA7C0672CAC1515E4B35C047B9A551B88F39DC26DA14A09EF74774D47C762DD177F9ED5BC2F11E52C879BD95098504CD9EECD8A8F9B3EFBD1F008AC5853097D9D1837F2B18F77CD7BE01AF80A7C7B5EA3CA54CC02D0C116FEE3F95BB87399385875D7E86747E676E728938ACBFF7098E05BE4DCFB24052B83AEFFB14783F029ADBDE7F53FAE92084224090E007CEE94D4BF2BACE9FFD4B57D2AF7C724D0CAA19BF0501F6F17B4AA10F425E3EA76080B4B9D6B3CEFEA115B2CEB8789BB8A3B0EA87FEBE63B6C8F846EC6DB0C26C5D7C");


	it("should transform a private key into a public key", function(){
		let result = secswans._dhToPublic(privateKey1, secswans._RFC5114_2048_256_G, secswans._RFC5114_2048_256_P);
		assert.deepEqual(result, publicKey1);
	});
	
	it("should transform a private key into a public key", function(){
		let result = secswans._dhToPublic(privateKey2, secswans._RFC5114_2048_256_G, secswans._RFC5114_2048_256_P);
		assert.deepEqual(result, publicKey2);
	});
	
	it("should create common key from example of rfc 5114", function(){
			let result = secswans._commonSecretDh(publicKey2, privateKey1, secswans._RFC5114_2048_256_P);
			let expected = commonKey;
			assert.deepEqual(result, expected);
		});
})

////////////////////////////////////////////////////////////////////////
/////////  END OF testing methods directly used by DH algo /////////////
////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
///////////////// testing methods directly used by swans ///////////////
////////////////////////////////////////////////////////////////////////


describe("testing the computePublicKey to generate ", function(){
		
		it("should throw an exception when encountering an invalid encryption type", function(){
			try{
				let invalidEncType = 123456;
				var requestKey = secswans.createKey(new Uint8Array(), null, 0);
				var receiverKey = secswans.createKey(null, new Uint8Array(), invalidEncType);
				let result = secswans.computePublicKey(requestKey, receiverKey);
				assert.ok(false, "an exception should have been raised");
			}
			catch(e){
				assert.ok("true", "exception has been raised");
			}
		});
		
		it("should create an empty public key", function(){
			let enctype = secswans.ENCTYPE_NONE;
			var requestKey = secswans.createKey(new Uint8Array(), null);
			var receiverKey = secswans.createKey(null, new Uint8Array(), enctype);
			let result = secswans.computePublicKey(requestKey, receiverKey);
			let expected = new Uint8Array();
			assert.deepEqual(result, expected);
		});
		
		it("should create a public key from a private with DH 2048_256", function(){
			let privateKey1 = new Uint8Array([ 
				0x08,0x81,0x38,0x2C ,0xDB,0x87,0x66,0x0C,
				0x6D,0xC1,0x3E,0x61 ,0x49,0x38,0xD5,0xB9,
				0xC8,0xB2,0xF2,0x48 ,0x58,0x1C,0xC5,0xE3,
				0x1B,0x35,0x45,0x43 ,0x97,0xFC,0xE5,0x0E
			]);
	
			let publicKey1 = new Uint8Array([
				   0x2E,0x93,0x80,0xC8 ,0x32,0x3A,0xF9,0x75 ,0x45,0xBC,0x49,0x41 ,0xDE,0xB0,0xEC,0x37,
				   0x42,0xC6,0x2F,0xE0 ,0xEC,0xE8,0x24,0xA6 ,0xAB,0xDB,0xE6,0x6C ,0x59,0xBE,0xE0,0x24 ,0x29,0x11,0xBF,0xB9 ,0x67,0x23,0x5C,0xEB,
				   0xA3,0x5A,0xE1,0x3E ,0x4E,0xC7,0x52,0xBE ,0x63,0x0B,0x92,0xDC ,0x4B,0xDE,0x28,0x47 ,0xA9,0xC6,0x2C,0xB8 ,0x15,0x27,0x45,0x42,
				   0x1F,0xB7,0xEB,0x60 ,0xA6,0x3C,0x0F,0xE9 ,0x15,0x9F,0xCC,0xE7 ,0x26,0xCE,0x7C,0xD8 ,0x52,0x3D,0x74,0x50 ,0x66,0x7E,0xF8,0x40,
				   0xE4,0x91,0x91,0x21 ,0xEB,0x5F,0x01,0xC8 ,0xC9,0xB0,0xD3,0xD6 ,0x48,0xA9,0x3B,0xFB ,0x75,0x68,0x9E,0x82 ,0x44,0xAC,0x13,0x4A,
				   0xF5,0x44,0x71,0x1C ,0xE7,0x9A,0x02,0xDC ,0xC3,0x42,0x26,0x68 ,0x47,0x80,0xDD,0xDC ,0xB4,0x98,0x59,0x41 ,0x06,0xC3,0x7F,0x5B,
				   0xC7,0x98,0x56,0x48 ,0x7A,0xF5,0xAB,0x02 ,0x2A,0x2E,0x5E,0x42 ,0xF0,0x98,0x97,0xC1 ,0xA8,0x5A,0x11,0xEA ,0x02,0x12,0xAF,0x04,
				   0xD9,0xB4,0xCE,0xBC ,0x93,0x7C,0x3C,0x1A ,0x3E,0x15,0xA8,0xA0 ,0x34,0x2E,0x33,0x76 ,0x15,0xC8,0x4E,0x7F ,0xE3,0xB8,0xB9,0xB8,
				   0x7F,0xB1,0xE7,0x3A ,0x15,0xAF,0x12,0xA3 ,0x0D,0x74,0x6E,0x06 ,0xDF,0xC3,0x4F,0x29 ,0x0D,0x79,0x7C,0xE5 ,0x1A,0xA1,0x3A,0xA7,
				   0x85,0xBF,0x66,0x58 ,0xAF,0xF5,0xE4,0xB0 ,0x93,0x00,0x3C,0xBE ,0xAF,0x66,0x5B,0x3C ,0x2E,0x11,0x3A,0x3A ,0x4E,0x90,0x52,0x69,
				   0x34,0x1D,0xC0,0x71 ,0x14,0x26,0x68,0x5F ,0x4E,0xF3,0x7E,0x86 ,0x8A,0x81,0x26,0xFF ,0x3F,0x22,0x79,0xB5 ,0x7C,0xA6,0x7E,0x29,
			]);
	
			let publicKey2 = new Uint8Array([
				   0x57,0x5F,0x03,0x51 ,0xBD,0x2B,0x1B,0x81 ,0x74,0x48,0xBD,0xF8 ,0x7A,0x6C,0x36,0x2C,
				   0x1E,0x28,0x9D,0x39 ,0x03,0xA3,0x0B,0x98 ,0x32,0xC5,0x74,0x1F ,0xA2,0x50,0x36,0x3E ,0x7A,0xCB,0xC7,0xF7 ,0x7F,0x3D,0xAC,0xBC,
				   0x1F,0x13,0x1A,0xDD ,0x8E,0x03,0x36,0x7E ,0xFF,0x8F,0xBB,0xB3 ,0xE1,0xC5,0x78,0x44 ,0x24,0x80,0x9B,0x25 ,0xAF,0xE4,0xD2,0x26,
				   0x2A,0x1A,0x6F,0xD2 ,0xFA,0xB6,0x41,0x05 ,0xCA,0x30,0xA6,0x74 ,0xE0,0x7F,0x78,0x09 ,0x85,0x20,0x88,0x63 ,0x2F,0xC0,0x49,0x23,
				   0x37,0x91,0xAD,0x4E ,0xDD,0x08,0x3A,0x97 ,0x8B,0x88,0x3E,0xE6 ,0x18,0xBC,0x5E,0x0D ,0xD0,0x47,0x41,0x5F ,0x2D,0x95,0xE6,0x83,
				   0xCF,0x14,0x82,0x6B ,0x5F,0xBE,0x10,0xD3 ,0xCE,0x41,0xC6,0xC1 ,0x20,0xC7,0x8A,0xB2 ,0x00,0x08,0xC6,0x98 ,0xBF,0x7F,0x0B,0xCA,
				   0xB9,0xD7,0xF4,0x07 ,0xBE,0xD0,0xF4,0x3A ,0xFB,0x29,0x70,0xF5 ,0x7F,0x8D,0x12,0x04 ,0x39,0x63,0xE6,0x6D ,0xDD,0x32,0x0D,0x59,
				   0x9A,0xD9,0x93,0x6C ,0x8F,0x44,0x13,0x7C ,0x08,0xB1,0x80,0xEC ,0x5E,0x98,0x5C,0xEB ,0xE1,0x86,0xF3,0xD5 ,0x49,0x67,0x7E,0x80,
				   0x60,0x73,0x31,0xEE ,0x17,0xAF,0x33,0x80 ,0xA7,0x25,0xB0,0x78 ,0x23,0x17,0xD7,0xDD ,0x43,0xF5,0x9D,0x7A ,0xF9,0x56,0x8A,0x9B,
				   0xB6,0x3A,0x84,0xD3 ,0x65,0xF9,0x22,0x44 ,0xED,0x12,0x09,0x88 ,0x21,0x93,0x02,0xF4 ,0x29,0x24,0xC7,0xCA ,0x90,0xB8,0x9D,0x24,
				   0xF7,0x1B,0x0A,0xB6 ,0x97,0x82,0x3D,0x7D ,0xEB,0x1A,0xFF,0x5B ,0x0E,0x8E,0x4A,0x45 ,0xD4,0x9F,0x7F,0x53 ,0x75,0x7E,0x19,0x13
				   ]);
			   
			let enctype = secswans.ENCTYPE_DH_2048_256;
			var requestKey = secswans.createKey(privateKey1, null);
			var receiverKey = secswans.createKey(null,publicKey2, enctype);
			let result = secswans.computePublicKey(requestKey, receiverKey);
			let expected = publicKey1;
			assert.deepEqual(result, expected);
		});
	
});

describe("test the generation of the symetric key, given data from request/receiver keys", function(){
	it("should throw an exception when encountering an invalid encryption type", function(){
			try{
				let invalidEncType = 123456;
				var requestKey = secswans.createKey(new Uint8Array(), null, 0);
				var receiverKey = secswans.createKey(null, new Uint8Array(), invalidEncType);
				let result = secswans.computeSymetricKey(requestKey, receiverKey);
				assert.ok(false, "an exception should have been raised");
			}
			catch(e){
				assert.ok("true", "exception has been raised");
			}
		});
		
		it("should return null with an empty encryption", function(){
			let enctype = secswans.ENCTYPE_NONE;
			var requestKey = secswans.createKey(new Uint8Array(), null);
			var receiverKey = secswans.createKey(null, new Uint8Array(), enctype);
			let result = secswans.computeSymetricKey(requestKey, receiverKey);
			let expected = null;
			assert.deepEqual(result, expected);
		});
		
		it("should generate the common key with DH 2048_256", function(){
			let privateKey1 = secswans._hexstrToUint8Array("0881382CDB87660C6DC13E614938D5B9C8B2F248581CC5E31B35454397FCE50E");
			let publicKey1 = secswans._hexstrToUint8Array(
			   "2E9380C8323AF97545BC4941DEB0EC37"+
			   "42C62FE0ECE824A6ABDBE66C59BEE0242911BFB967235CEB"+
			   "A35AE13E4EC752BE630B92DC4BDE2847A9C62CB815274542"+
			   "1FB7EB60A63C0FE9159FCCE726CE7CD8523D7450667EF840"+
			   "E4919121EB5F01C8C9B0D3D648A93BFB75689E8244AC134A"+
			   "F544711CE79A02DCC34226684780DDDCB498594106C37F5B"+
			   "C79856487AF5AB022A2E5E42F09897C1A85A11EA0212AF04"+
			   "D9B4CEBC937C3C1A3E15A8A0342E337615C84E7FE3B8B9B8"+
			   "7FB1E73A15AF12A30D746E06DFC34F290D797CE51AA13AA7"+
			   "85BF6658AFF5E4B093003CBEAF665B3C2E113A3A4E905269"+
			   "341DC0711426685F4EF37E868A8126FF3F2279B57CA67E29"
			);
	
			let publicKey2 = secswans._hexstrToUint8Array(
				   "575F0351BD2B1B817448BDF87A6C362C"+
				   "1E289D3903A30B9832C5741FA250363E7ACBC7F77F3DACBC"+
				   "1F131ADD8E03367EFF8FBBB3E1C5784424809B25AFE4D226"+
				   "2A1A6FD2FAB64105CA30A674E07F7809852088632FC04923"+
				   "3791AD4EDD083A978B883EE618BC5E0DD047415F2D95E683"+
				   "CF14826B5FBE10D3CE41C6C120C78AB20008C698BF7F0BCA"+
				   "B9D7F407BED0F43AFB2970F57F8D12043963E66DDD320D59"+
				   "9AD9936C8F44137C08B180EC5E985CEBE186F3D549677E80"+
				   "607331EE17AF3380A725B0782317D7DD43F59D7AF9568A9B"+
				   "B63A84D365F92244ED120988219302F42924C7CA90B89D24"+
				   "F71B0AB697823D7DEB1AFF5B0E8E4A45D49F7F53757E1913"
				   );
			   
			let commonKeyHashed = secswans._hexstrToUint8Array("01eb7b326b9093eea16968f04915ea1097194caee5bc657dba57bb69c6b7613d" );
			
			let enctype = secswans.ENCTYPE_DH_2048_256;
			var requestKey = secswans.createKey(privateKey1);
			var receiverKey = secswans.createKey(null,publicKey2, enctype);
			let result = secswans.computeSymetricKey(requestKey, receiverKey);
			let commonKey = secswans._hexstrToUint8Array("86C70BF8D0BB81BB01078A17219CB7D27203DB2A19C877F1D1F19FD7D77EF22546A68F005AD52DC84553B78FC60330BE51EA7C0672CAC1515E4B35C047B9A551B88F39DC26DA14A09EF74774D47C762DD177F9ED5BC2F11E52C879BD95098504CD9EECD8A8F9B3EFBD1F008AC5853097D9D1837F2B18F77CD7BE01AF80A7C7B5EA3CA54CC02D0C116FEE3F95BB87399385875D7E86747E676E728938ACBFF7098E05BE4DCFB24052B83AEFFB14783F029ADBDE7F53FAE92084224090E007CEE94D4BF2BACE9FFD4B57D2AF7C724D0CAA19BF0501F6F17B4AA10F425E3EA76080B4B9D6B3CEFEA115B2CEB8789BB8A3B0EA87FEBE63B6C8F846EC6DB0C26C5D7C");
			let hashedKey = secswans._hexstrToUint8Array("01eb7b326b9093eea16968f04915ea1097194caee5bc657dba57bb69c6b7613d");
			let expected = hashedKey;
			assert.deepEqual(result, expected);
		});
	
});


describe("test key generation", function(){
	
	it("should throw an exception when encountering an invalid encryption type", function(){
			try{
				let invalidEncType = 123456;
				let key = secswans.generateKey(invalidEncType);
				assert.ok(false, "an exception should have been raised");
			}
			catch(e){
				assert.ok("true", "exception has been raised");
			}
	});
		
	it("should create an empty public/private pair", function(){
		let pair = secswans.generateKey(secswans.ENCTYPE_NONE);
		assert.deepEqual( pair.type, secswans.ENCTYPE_NONE );
		assert.deepEqual( pair.public, new Uint8Array() );
		assert.deepEqual( pair.private, new Uint8Array() );
	});
	
	it("should create a ENCTYPE_DH_2048_256 public/private pair", function(){
		let pair = secswans.generateKey(secswans.ENCTYPE_DH_2048_256);
		assert.deepEqual( pair.type, secswans.ENCTYPE_DH_2048_256 );
		assert.instanceOf( pair.public, Uint8Array );
		assert.instanceOf( pair.private, Uint8Array );
		assert.lengthOf( pair.public, 256 );
	});
	
	it("should create a rsa1024 public/private pair", function(){
		this.timeout(100000);
		let key = secswans.generateKey(secswans.ENCTYPE_RSA_1024);
		assert.deepEqual( key.type, secswans.ENCTYPE_RSA_1024 );

		// check the public key
        assert.isObject( key.public );
        assert.deepEqual( key.public.type, secswans.ENCTYPE_RSA_1024 );
        assert.isString( key.public.n );
        assert.lengthOf( key.public.n, 256 );
        assert.isString( key.public.e );
        assert.isAbove( key.public.e.length, 0);


        // check the private key
        assert.isObject( key.private );
        assert.deepEqual( key.private.type, secswans.ENCTYPE_RSA_1024 );
        assert.isString( key.private.n );
        assert.lengthOf( key.private.n, 256 );
        assert.isString( key.private.e );
        assert.isAbove( key.private.e.length, 0);
        assert.isString( key.private.d );
        assert.isAbove( key.private.d.length, 0);
	});
	
	it("should create a rsa2048 public/private pair", function(){
		this.timeout(100000);
		let key = secswans.generateKey(secswans.ENCTYPE_RSA_2048);
		assert.deepEqual( key.type, secswans.ENCTYPE_RSA_2048 );

        // check the public key
        assert.isObject( key.public );
        assert.deepEqual( key.public.type, secswans.ENCTYPE_RSA_2048 );
        assert.isString( key.public.n );
        assert.lengthOf( key.public.n, 512 );
        assert.isString( key.public.e );
        assert.isAbove( key.public.e.length, 0);

        // check the private key
        assert.isObject( key.private );
        assert.deepEqual( key.private.type, secswans.ENCTYPE_RSA_2048 );
        assert.isString( key.private.n );
        assert.lengthOf( key.private.n, 512 );
        assert.isString( key.private.e );
        assert.isAbove( key.private.e.length, 0);
        assert.isString( key.private.d );
        assert.isAbove( key.private.d.length, 0);
	});
	
	it("should create a rsa4096 public/private pair", function(){
		return;
		this.timeout(200000);
		let key = secswans.generateKey(secswans.ENCTYPE_RSA_4096);
		assert.deepEqual( key.type, secswans.ENCTYPE_RSA_4096 );
		assert.isString( key.n );
		assert.lengthOf( key.n, 1024 );
		assert.isString( key.e );
		assert.isAbove( key.e.length, 0);
		assert.isString( key.d );
		assert.isAbove( key.d.length, 0);
	});
});

describe("test signing methods", function(){

	it("sign data with a rsa1024 key and recognize right and wrong messages", function(){
		this.timeout(100000);
		let signCheckResult;
		let key = secswans.generateKey(secswans.ENCTYPE_RSA_1024);
		
		let message1 = new Uint8Array([]);
		let message2 = new Uint8Array([1,2,3,4,5,6,7,8]);
		let message3 = new Uint8Array([1,2,3,4,5,6,7,1,2,3,4,5,6,7,1,2,3,4,5,6,7,1,2,3,4,5,6,7,1,2,3,4,5,6,7,1,2,3,4,5,6,7,1,2,3,4,5,6,7,1,2,3,4,5,6,7,1,2,3,4,5,6,7,1,2,3,4,5,6,7]);
		
		let sigData = key.private
		let sigCheckData = key.public;
		
		// create the signatures
		let sig1 = secswans.sign(message1, sigData);
		assert.isString(sig1, "the signature should be a hexadecimal string");
		assert.lengthOf(sig1, 256);
		
		let sig2 = secswans.sign(message2, sigData);
		assert.isString(sig2, "the signature should be a hexadecimal string");
		assert.lengthOf(sig2, 256, "unexpected signature size : "+sig2);
		
		let sig3 = secswans.sign(message3, sigData);
		assert.isString(sig3, "the signature should be a hexadecimal string");
		assert.lengthOf(sig3, 256);
		
		/////////////// Trying to recognize the first message
		sigCheckData.message = message1;
		// test first signature
		sigCheckData.signature = sig1;
		signCheckResult = secswans.checkSign(message1, sigData, sig1);
		assert.ok(signCheckResult, "the message should have been recognized");
		// test second signature
		sigCheckData.signature = sig2;
		signCheckResult = secswans.checkSign(message1, sigData, sig2);
		assert.notOk(signCheckResult, "the message should not have been recognized");
		// test third signature
		sigCheckData.signature = sig3;
		signCheckResult = secswans.checkSign(message1, sigData, sig3);
		assert.notOk(signCheckResult, "the message should not have been recognized");
		
		/////////////// Trying to recognize the second message
		sigCheckData.message = message2;
		// test first signature
		sigCheckData.signature = sig1;
		signCheckResult = secswans.checkSign(message2, sigData, sig1);
		assert.notOk(signCheckResult, "the message should not have been recognized");
		// test second signature
		sigCheckData.signature = sig2;
		signCheckResult = secswans.checkSign(message2, sigData, sig2);
		assert.ok(signCheckResult, "the message should have been recognized");
		// test third signature
		sigCheckData.signature = sig3;
		signCheckResult = secswans.checkSign(message2, sigData, sig3);
		assert.notOk(signCheckResult, "the message should not have been recognized");

		/////////////// Trying to recognize the third message
		sigCheckData.message = message3;
		// test first signature
		sigCheckData.signature = sig1;
		signCheckResult = secswans.checkSign(message3, sigData, sig1);
		assert.notOk(signCheckResult, "the message should not have been recognized");
		// test second signature
		sigCheckData.signature = sig2;
		signCheckResult = secswans.checkSign(message3, sigData, sig2);
		assert.notOk(signCheckResult, "the message should not have been recognized");
		// test third signature
		sigCheckData.signature = sig3;
		signCheckResult = secswans.checkSign(message3, sigData, sig3);
		assert.ok(signCheckResult, "the message should have been recognized");
		
	});
	it("sign data with a rsa2048 key and recognize right and wrong messages", function(){
		this.timeout(100000);
		let signCheckResult;
		let key = secswans.generateKey(secswans.ENCTYPE_RSA_2048);
		
		let message1 = new Uint8Array([]);
		let message2 = new Uint8Array([1,2,3,4,5,6,7,8]);
		let message3 = new Uint8Array([1,2,3,4,5,6,7,1,2,3,4,5,6,7,1,2,3,4,5,6,7,1,2,3,4,5,6,7,1,2,3,4,5,6,7,1,2,3,4,5,6,7,1,2,3,4,5,6,7,1,2,3,4,5,6,7,1,2,3,4,5,6,7,1,2,3,4,5,6,7]);

        let sigData = key.private
        let sigCheckData = key.public;

        // create the signatures
        let sig1 = secswans.sign(message1, sigData);
        assert.isString(sig1, "the signature should be a hexadecimal string");
        assert.lengthOf(sig1, 512);

        let sig2 = secswans.sign(message2, sigData);
        assert.isString(sig2, "the signature should be a hexadecimal string");
        assert.lengthOf(sig2, 512);

        let sig3 = secswans.sign(message3, sigData);
        assert.isString(sig3, "the signature should be a hexadecimal string");
        assert.lengthOf(sig3, 512);

        /////////////// Trying to recognize the first message
        sigCheckData.message = message1;
        // test first signature
        sigCheckData.signature = sig1;
        signCheckResult = secswans.checkSign(message1, sigData, sig1);
        assert.ok(signCheckResult, "the message should have been recognized");
        // test second signature
        sigCheckData.signature = sig2;
        signCheckResult = secswans.checkSign(message1, sigData, sig2);
        assert.notOk(signCheckResult, "the message should not have been recognized");
        // test third signature
        sigCheckData.signature = sig3;
        signCheckResult = secswans.checkSign(message1, sigData, sig3);
        assert.notOk(signCheckResult, "the message should not have been recognized");

        /////////////// Trying to recognize the second message
        sigCheckData.message = message2;
        // test first signature
        sigCheckData.signature = sig1;
        signCheckResult = secswans.checkSign(message2, sigData, sig1);
        assert.notOk(signCheckResult, "the message should not have been recognized");
        // test second signature
        sigCheckData.signature = sig2;
        signCheckResult = secswans.checkSign(message2, sigData, sig2);
        assert.ok(signCheckResult, "the message should have been recognized");
        // test third signature
        sigCheckData.signature = sig3;
        signCheckResult = secswans.checkSign(message2, sigData, sig3);
        assert.notOk(signCheckResult, "the message should not have been recognized");


        /////////////// Trying to recognize the third message
        sigCheckData.message = message3;
        // test first signature
        sigCheckData.signature = sig1;
        signCheckResult = secswans.checkSign(message3, sigData, sig1);
        assert.notOk(signCheckResult, "the message should not have been recognized");
        // test second signature
        sigCheckData.signature = sig2;
        signCheckResult = secswans.checkSign(message3, sigData, sig2);
        assert.notOk(signCheckResult, "the message should not have been recognized");
        // test third signature
        sigCheckData.signature = sig3;
        signCheckResult = secswans.checkSign(message3, sigData, sig3);
        assert.ok(signCheckResult, "the message should have been recognized");
	});


    it("sign data with sha256 and recognize right and wrong messages", function(){
        this.timeout(100000);
        let signCheckResult;
        let key = secswans.generateKey(secswans.HASHTYPE_SHA_256);

        let message1 = new Uint8Array([]);
        let message2 = new Uint8Array([1,2,3,4,5,6,7,8]);
        let message3 = new Uint8Array([1,2,3,4,5,6,7,1,2,3,4,5,6,7,1,2,3,4,5,6,7,1,2,3,4,5,6,7,1,2,3,4,5,6,7,1,2,3,4,5,6,7,1,2,3,4,5,6,7,1,2,3,4,5,6,7,1,2,3,4,5,6,7,1,2,3,4,5,6,7]);

        let sigData = key.private;
        let sigCheckData = key.public;

        // create the signatures
        let sig1 = secswans.sign(message1, sigData);
        assert.isString(sig1, "the signature should be a hexadecimal string");
        assert.lengthOf(sig1, 64);

        let sig2 = secswans.sign(message2, sigData);
        assert.isString(sig2, "the signature should be a hexadecimal string");
        assert.lengthOf(sig2, 64);

        let sig3 = secswans.sign(message3, sigData);
        assert.isString(sig3, "the signature should be a hexadecimal string");
        assert.lengthOf(sig3, 64);

        /////////////// Trying to recognize the first message
        sigCheckData.message = message1;
        // test first signature
        sigCheckData.signature = sig1;
        signCheckResult = secswans.checkSign(message1, sigData, sig1);
        assert.ok(signCheckResult, "the message should have been recognized");
        // test second signature
        sigCheckData.signature = sig2;
        signCheckResult = secswans.checkSign(message1, sigData, sig2);
        assert.notOk(signCheckResult, "the message should not have been recognized");
        // test third signature
        sigCheckData.signature = sig3;
        signCheckResult = secswans.checkSign(message1, sigData, sig3);
        assert.notOk(signCheckResult, "the message should not have been recognized");

        /////////////// Trying to recognize the second message
        sigCheckData.message = message2;
        // test first signature
        sigCheckData.signature = sig1;
        signCheckResult = secswans.checkSign(message2, sigData, sig1);
        assert.notOk(signCheckResult, "the message should not have been recognized");
        // test second signature
        sigCheckData.signature = sig2;
        signCheckResult = secswans.checkSign(message2, sigData, sig2);
        assert.ok(signCheckResult, "the message should have been recognized");
        // test third signature
        sigCheckData.signature = sig3;
        signCheckResult = secswans.checkSign(message2, sigData, sig3);
        assert.notOk(signCheckResult, "the message should not have been recognized");


        /////////////// Trying to recognize the third message
        sigCheckData.message = message3;
        // test first signature
        sigCheckData.signature = sig1;
        signCheckResult = secswans.checkSign(message3, sigData, sig1);
        assert.notOk(signCheckResult, "the message should not have been recognized");
        // test second signature
        sigCheckData.signature = sig2;
        signCheckResult = secswans.checkSign(message3, sigData, sig2);
        assert.notOk(signCheckResult, "the message should not have been recognized");
        // test third signature
        sigCheckData.signature = sig3;
        signCheckResult = secswans.checkSign(message3, sigData, sig3);
        assert.ok(signCheckResult, "the message should have been recognized");
    });
});





////////////////////////////////////////////////////////////////////////
/////////// END OF testing methods directly used by swans //////////////
////////////////////////////////////////////////////////////////////////
