/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

/** the NIMeta agregates several spawners, enable manual creation of spawns and add hooks on new spawns
***/

import {NestedInterface} from "./NestedInterface.js"

class NIMeta extends NestedInterface {
	// the constructor
	constructor(){
		super();
		this.name = "NIMeta";
		this.spawners = [];
		this.spawns = [];
		this.core({
			spawners : this.spawners,
			spawns : this.spawns
		});
		this.status(this.RUNNING);
		this.startCron();
		
		// add a hook for spawn
		this.initializeSpawnHook();
	}
	
	// add a spawn manually to the list of spawns
	addSpawn(spawn, eventData={}, triggerEvent=true){
		if(this.spawns.includes(spawn)) return;
		this.linkHooks(spawn);
		this.spawns.push(spawn);
		eventData.spawn = spawn;
		if(triggerEvent) this.triggerSpawn(eventData);
	}
	
	// add a spawn manually to the list of spawns
	addSpawner(spawner){
		this.linkHooks(spawner);
		this.spawners.push(spawner);
	}
	
	// link the hooks of the spawn to those on the metaspawn
	linkHooks(ni){
		// if(this.spawners.includes(ni) || this.spawns.includes(ni)) return;
		// overwrite the trigger method of the NestedInterface
		let metaspawner = this;
		let oldTriggerMethod = ni.trigger;

		let newTriggerMethod = function(){
			if(metaspawner.status() === metaspawner.STOPPED) return;
			var args = Array.prototype.slice.call(arguments);
			// call the hooks of the nested interface
			oldTriggerMethod.call(ni, ...args);
			// call the hooks of the meta
			args[1] = metaspawner.hooks;
			oldTriggerMethod.call(ni, ...args);
		}
		ni.trigger = newTriggerMethod;
	}
	
	// cron task to remove dead spawns
	cron(){
		// check the spawns
		for(let i = 0; i in this.spawns; ){
			let spawn = this.spawns[i];
			let status = spawn.status();
			if( status !== this.RUNNING && status !== this.NEW){
				this.spawns.splice(i, 1);
			}
			else i++;
		}
	}
	
	startCron(){
		this.cronId = setInterval(this.cron.bind(this), 3000);
	}
	
	stopCron(){
		if(this.cronId === null) return;
		clearInterval(this.cronId);
		this.cronId = null;
	}
	
	// method to stop the server
	start(){
		this.startCron();
		NestedInterface.prototype.start.call(this);
	}
	
	// overwrite the trigger method to call the trigger method of every spawn/spawner associated to the NIMeta
	trigger(){
		var args = Array.prototype.slice.call(arguments);
		for(let spawn of this.spawns){
			spawn.trigger(...args);
		}
		for(let spawner of this.spawners){
			spawner.trigger(...args);
		}
	}
	
	// method to stop the server
	stop(){
		this.stopCron();
		// NestedInterface.prototype.stop.call(this);
		for(let spawner of this.spawners) spawner.stop();
		for(let spawn of this.spawns) spawn.stop();
		this.spawners.length = 0;
		this.spawns.length = 0;
	}
	
	getCanals(recursion=2){
		let canals = [];
		for(let spawner of this.spawners){
			let spawnerCanals = spawner.getCanals(recursion);
			canals = canals.concat(spawnerCanals);
		} 
		return canals;
	}
	
	canSpawn(canal={}){
		for(let spawner of this.spawners){
			let res = spawner.canSpawn(canal);
			if(res) return true;
		}
		return false;
	}
	
	spawn(canal){
		// if canal is an array of canals, retrieve the first canal that can be spawned
		if(Array.isArray(canal)) canal = this.canSpawnAny(canal) || {};
		for(let spawner of this.spawners){
			if(spawner.canSpawn(canal)){
				let meta = this;
				return spawner.spawn(canal)
				.then(function(ni){
					//meta.linkHooks(ni);
                    if(ni) meta.addSpawn(ni, {}, false)
					return ni;
				});

			}
		}
		return Promise.reject("no spawner available to spawn the requested canal");
	}
	
	initializeSpawnHook(){
		let metaspawner = this;
		this.addHook(this.e.SPAWN, function(event){
			metaspawner.addSpawn(event.spawn, event, false)
		});
	}
}

export {
    NIMeta
}