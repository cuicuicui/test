/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

/** encapsulate swans clients SwanClientReceiver and SwanClientCaller as spawner
 * SwanClientReceiver create spawns dynamically when receiving messages from a swans server
 * SwanClientCaller create spawns upon request (one time messages sent to swans receiver)
 ***/
import {NestedInterface} from "./NestedInterface.js"
import {SwanClientCaller} from "../../swans/lib/SwanClient.js"

class NISwansSpawner extends NestedInterface {
    // the constructor takes a server.io
    constructor(data={}){
        super();
        this.name = "NISwansSpawner";

        // may provide a SwanClientReceiver instance to create spawn from swans server messages
        // and a spawner to make swans server connection and then swansReceiver requests
        let core = {
            receiver : data.receiver,
            spawner : data.spawner,
            swansServerCanals : data.swansServerCanals || []
        }
        this.core(core);
        this.initializeReceiver(core.receiver);

        // keep in memory key pairs generated for encryption
        this.transactionKeysList = [];

        this.status(this.STARTED);

        this._initConstants();
    }

    // implement the AbstractNestedInterface methods

    // method to start the connexion
    start(){
        NestedInterface.prototype.start.call(this);
    }

    // method to stop the server
    stop(){
        if(this._core.receiver){
            this._core.receiver.setCallback(function(){});
            this._core.receiver.close();
        }
        NestedInterface.prototype.stop.call(this);
    }

    // implement the getCanals method
    getCanals(recursion=2){
        if(!this._core.receiver) return [];
        let canal = {
            type: this.CANAL_SWANS,
            // canals: this._core.swansServerCanals,
        };

        if(this._core.receiver){
            canal.canals = this._core.serverCanals || this._core.swansServerCanals;
            canal.receiver = this._core.receiver.getReceiverId();
        }
        return [canal];
    }

    // implement canSpawn
    canSpawn(canal={}){
        // cannot spawn if no canal can be spawne to contact the swans server
        if(!Array.isArray(canal.canals) ) return false;
        if(!( this._core.spawner && this._core.spawner.canSpawnAny(canal.canals))) return false;
        if(canal.type != this.CANAL_SWANS) return false;
        return true;
    }

    // implement spawn method
    spawn(canal){
        if(!(canal && this.canSpawn(canal))) return Promise.reject("invalid canal for spawn");
        // build the swans server spawn and the swans caller
        let swanServerCanal = this._core.spawner.canSpawnAny(canal.canals)
        let swansSpawnPromise = this._core.spawner.spawn(swanServerCanal);
        let $this = this;
        return swansSpawnPromise.then(function(swansSpawn){


            let swansCaller = new SwanClientCaller({
                receiverId : canal.receiver,
                port : canal.port,
                swansServerInterface : swansSpawn
            })
            // build the transactionKeys that will be used for this swansCaller
            let transactionKeys = swansCaller.getTransactionKeys(canal.receiver, $this.transactionKeysList);
            if($this.transactionKeysList.indexOf(transactionKeys) === -1) $this.transactionKeysList.push(transactionKeys)
            swansCaller.transactionKeysList([transactionKeys]);

            let spawn = new NISwansCallerSpawn({caller : swansCaller, spawner : $this});
            $this.triggerSpawn({
                spawn : spawn,
                interface : $this
            })
            return spawn
        })
            .catch(e=>console.log("err", e))
    }

    // make the receiver trigger spawns upon receving requests
    initializeReceiver(receiver){
        if(!receiver) return;
        let $this = this;
        var receiverCallback = function(bytes, event){
            let packet = $this.bytesToPacket(bytes);

            // create the promise that will be resolved when the answer is sent
            let resolvePromise;
            let answerPromise = new Promise(res=>resolvePromise = res);

            // create a custom send method for the new spawn
            let customSend = function(packet){
                let msg = $this.packetToBytes(packet);
                resolvePromise(msg);
            }
            // create a spawn and trigger the spawn event
            let spawn = new NISwansAnswer({send : customSend});
            let triggerSpawnData = {
                spawn : spawn,
                // message : packet.message,
                port : packet.port,
                fport : packet.fport,
                interface : $this
            };
            $this.triggerSpawn(triggerSpawnData);

            let triggerReceiveData = {
                message : packet.message,
                port : packet.port,
                fport : packet.fport,
                interface : spawn
            };
            spawn.triggerReceive(triggerReceiveData);
            return answerPromise;
        }
        receiver.setCallback(receiverCallback);
    }

    _initConstants(){
        this.CANAL_SWANS = 5;
    }
}

// interface to send a one time answer after receiving a swan request
class NISwansAnswer extends NestedInterface{
    constructor(data={}){
        super();
        this.name = "NISwansAnswer";
        let core = {
            send : data.send.bind(this),
        }
        this.core(core);
        this.status(this.STARTED);
    }
    // implement the send method
    send(message, dp=0, fp=0){
        let packet = {
            message : message,
            port : dp,
            fport : fp
        };
        this._core.send(packet);
        NestedInterface.prototype.send.call(this, message, dp, fp);
        this.stop();
    }
}

// enable messaging a swan receiver and listening to its answers
class NISwansCallerSpawn extends NestedInterface{
    constructor(data={}){
        super();
        this.name = "NISwansCallerSpawn";
        this.core({
            caller : data.caller, // swans caller
            spawner : data.spawner
        });
        this.status(this.RUNNING);
    }

    // implement the send method
    send(message, dp=0, fp=0){
        let packetBytes = this.packetToBytes({message:message, port:dp, fport:fp});
        let answerPromise = this._core.caller.sendRequest(packetBytes);
        let $this = this;
        let prot = NISwansCallerSpawn.prototype;
        let res = answerPromise.then(function(transactionData){
            let packetObject = $this.bytesToPacket(transactionData.body.body);
            $this.triggerReceive(packetObject);
        })
            .catch(function(packet){
            let type = packet.type;
            let isError = type.startsWith("error");
            if(isError) $this.stop();
        })
        NestedInterface.prototype.send.call(this, message, dp, fp);
        return res;
    }

    stop(){
        if(this._core.caller){
            this._core.caller.close();
        }
        super.stop(this);
    }
}

export {
    NISwansSpawner,
    NISwansAnswer,
    NISwansCallerSpawn,
}