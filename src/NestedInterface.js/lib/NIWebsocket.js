/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

/** the NestedSocketIoServerInterface encapsulate a socket.io server for the use of the NestedInterfaces
***/

import {NestedInterface} from "./NestedInterface.js"

class NIWebsocketSpawner extends NestedInterface {
	// the constructor takes a server.io
	constructor(data={}){
		super();
		this.name = "NIWebsocketSpawner";
		this.socketPort = data.socketPort || 8080;
		this.address = data.address || null;
		// create the socket.io server
        this.withServer = data.hasOwnProperty("withServer") ? data.withServer : true;
		if(this.address && this.withServer){
            if(!NIWebsocketSpawner.prototype.websocketLib){
                NIWebsocketSpawner.prototype.websocketLib = require('ws');
                NIWebsocketSpawn.prototype.websocketLib = NIWebsocketSpawner.prototype.websocketLib;
            }
            let socketIoServer = new NIWebsocketSpawner.prototype.websocketLib.Server({ port : this.socketPort });
            this.core(socketIoServer);
            this.initializeSocketIoServer(socketIoServer);
		}
		// start the spawner officially
		this.status(this.RUNNING);
		this.initializeWebsocketNIConstants();
	}
	
	// implement the AbstractNestedInterface methods
	
	// method to start the connexion
	start(){
		NestedInterface.prototype.start.call(this);
	}
	
	// method to stop the server
	stop(){
		if(this._core) this._core.close();
		NestedInterface.prototype.stop.call(this);
	}
	
	// implement the getCanals method
	getCanals(){
		let address = null;
		if(!this.withServer) return [];
		if(this.address){
			address = this.address;
			// if(this.socketPort) address += ":"+this.socketPort;
		}
		return [{
				type : this.CANAL_WEBSOCKET,
				address : address
			}];
	}
	
	// implement canSpawn
	canSpawn(canal={}){
		if(canal.type != this.CANAL_WEBSOCKET) return false;
		if(!canal.address) return false;
		return true;
	}
	
	// implement spawn method
	spawn(canal){
		if(!(canal && this.canSpawn(canal))) return Promise.reject("invalid canal for spawn");
		let spawnBuilder = function(){
			return new NIWebsocketSpawn({serverAddress : canal.address});
		};
		return this._buildAndWaitInterface(spawnBuilder);
	}
	
	// method to be launched when a new socket is opened : it should create a new spawn
	_onSpawn(newSocket, connectionData){
		// transform the socket into a nested interface
		let spawn = new NIWebsocketSpawn({socket : newSocket});
		// trigger the spawn event
		if(!connectionData) connectionData = {};
		connectionData.spawn = spawn;
		this.triggerSpawn(connectionData);
		// change the status of the spawn to connected
		spawn.status(spawn.RUNNING);
	}
	
	// initialize the socketIoServer, bind the events
	initializeSocketIoServer(socketIoServer){
		if(!socketIoServer) return;
		var spawner = this;
		socketIoServer.on('connection', function (newSpawnSocket) {
			try{
				spawner._onSpawn(newSpawnSocket);
			}
			catch(e){
				console.log(e);
			}
		});
	}
	
	initializeWebsocketNIConstants(){
		this.CANAL_WEBSOCKET = 2;
	}
}

class NIWebsocketSpawn extends NestedInterface{
	constructor(data={}){
		super();
		this.name = "NIWebsocketSpawn";
		this.status(this.STARTED);
		
		let socket = null;
		let spawnMessageBytesStr = null;
		if(data.message){
			let packet = {
				message : data.message,
				port : data.port,
				fport : data.fport
			}
			let spawnMessageBytes = this.packetToBytes(packet);
			spawnMessageBytesStr = this.bytesToAscii(spawnMessageBytes);
		}
		this.serverAddress = data.serverAddress;

		// case 1 : a socket is already provided
		if(data.socket) socket = data.socket;
		// case 2 : a socket is built on a nodejs env
		else if(this.isNode()){
			let socketQuery = spawnMessageBytesStr ? {query : spawnMessageBytesStr} : {};
			if(!NIWebsocketSpawn.prototype.websocketLib) NIWebsocketSpawn.prototype.websocketLib = require("ws");
			socket = new NIWebsocketSpawn.prototype.websocketLib(data.serverAddress);
		}
		// case 3 : a socket will be built on webbrower
		else{
			socket = new WebSocket(data.serverAddress);
		}
		this.core(socket);
		
		this.status(this.NEW);
		this.initializeSocketIo();
	}
	
	// there is no real way to restart a socket connection
	start(){
		NestedInterface.prototype.start.call(this);
	}
	
	// implementation of stop method
	stop(){
		this._core.close();
		NestedInterface.prototype.stop.call(this);
	}
	
	// implement the send method
	send(body, dp=0, fp=0){
		let packet = {
			message : body,
			port : dp,
			fport : fp,
		};
		let bytes = this.packetToBytes(packet);
		let bytesStr = this.bytesToAscii(bytes);
		try{
			this._core.send(bytesStr);
		}
		catch(e){
			console.log(e, this.manstopped);
		}

		NestedInterface.prototype.send.call(this, body, dp, fp);
	}	
	
	initializeSocketIo(){
		if(!this._core) return; 
		var spawn = this;
		
		// when receiving message, trigger the RECEIVE event 
		let onmessage = function (data) {
			let port = null;
			let bytesStr = typeof data === "string" ? data : data.data;
			let bytes = spawn.asciiToBytes(bytesStr);
			let packet = spawn.bytesToPacket(bytes);
			packet.interface = spawn;
			if(!spawn.test) spawn.test = Math.random()
			spawn.triggerReceive(packet);
		}
		let onclose = function () {
			// trigger the STOP event
			spawn.triggerDisconnect();
			spawn.status(spawn.STOPPED);
		}
		let onerror = function(e){
			if(e.code === "ECONNREFUSED"){
				this.status(this.STOPPED);
				console.log("stopped socket !");
			}
		};
		let onopen = function(){
			spawn.triggerConnect();
			spawn.status(spawn.RUNNING);
		}

		this._core.onmessage = onmessage;
		this._core.onclose = onclose;
		this._core.onerror = onerror;
		this._core.onopen = onopen;
	}

}

export {
    NIWebsocketSpawner,
    NIWebsocketSpawn,
}