/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

/** the NIWebrtcSpawner encapsulate protocols to build webrtc connections
 *  the NIWebrtcSpawner is both a nestedInterface(it can create webrtcSpawns) and a flock application(it listen to a port to receive transactions)
 *
 *
 ***/

import {NestedInterface} from "./NestedInterface.js"

class NIWebrtcSpawner extends NestedInterface {
    // the constructor takes a server.io
    constructor(data={}){
        super();
        this.setIceServers(data.ice || [ {urls: "stun:stun.l.google.com:19302"} ]);
        // number of RTCPeerConnection to build in advance : this enables to perform ice candidates before the demand occurs
        this.preheat = data.preheat || 1; // TODO : change this to a better value for release
        this.preheatedTransactions = [];
        this.passIceTimeout = data.passIceTimeout || false; // if ice gathering times out and passIceTimeout === true, the transaction is accepted anyway
        this.signalingInterface = data.signalingInterface;
        this.spawner = data.spawner; // a spawner creater interface to initiate messaging with a remote node
        this.port = data.port || this.DEFAULT_SIGNALING_PORT;
        this.cronTime = data.cronTime || 20000;
        this.transactionTimeout = data.transactionTimeout || 120000;
        this.name = "NIWebrtcSpawner";
        let core = {};
        this.core(core);
        this.status(this.STARTED);
        this._transaction = {};
        this.preheatAll();
        this.intializeSignalingInterface();
        this.start();
    }

    // method to start the connexion
    start(){
        this.cronId = setInterval(this.cron.bind(this), this.cronTime);
        NestedInterface.prototype.start.call(this);
    }

    // method to stop the server
    stop(){
        if(this.cronId !== null){
            clearInterval(this.cronId);
            this.cronId = null;
        }
        // stop every peer connections
        NestedInterface.prototype.stop.call(this);
    }

    // cron task for the spawner
    cron(){
        let time = Date.now();
        // purge expired transactions
        for(let tid in this._transaction){
            let req = this._transaction[tid];
            if(time-req.startTime > this.transactionTimeout){
                this.removeTransaction(tid);
            }
        }
    }

    // process incoming messages
    processMessage(event){
        if(this.status() !== this.RUNNING) return;
        let msg = event.message;
        let answerInterface = event.interface;
        let destinationPort = event.fport || null;
        if(!this.test) this.test = Math.random();

        // convert the byte message into object data
        let data = this.signalToObject(msg);
        if(destinationPort === null) destinationPort = this.port;

        // if the transaction exists, just add remote data to it
        let transaction = this.getTransaction(data.tid);
        if(transaction){
            this.addTransactionRemoteData(transaction.tid, data);
        }
        // if not, create a new transaction add remote data, and send signaling answer
        else{
            let $this = this;
            this.getPreheatedTransaction({
                tid : data.tid,
                interface : answerInterface,
                signalingPort : destinationPort,
                remoteOffer : data.offer,
                remoteIceCandidates : data.iceCandidates
            }).then(function(transaction){
                $this.addTransaction(transaction);
                $this.sendTransactionSignal(transaction.tid);
            })
        }
    }

    // create a peer connection object
    _createPeerConnection(){
        let servers = {
            iceServers : this.getIceServers(),
        };
        // retrieve the RTCPeerConnection object
        if(!NIWebrtcSpawner.prototype.RTCPeerConnection){
            if(typeof RTCPeerConnection != "undefined") NIWebrtcSpawner.prototype.RTCPeerConnection = RTCPeerConnection;
            else{
                try{
                    let lib = require("wrtc");
                    NIWebrtcSpawner.prototype.RTCPeerConnection = lib.RTCPeerConnection;
                }
                catch(e){ }
            }
        }
        if(! NIWebrtcSpawner.prototype.RTCPeerConnection) throw "No RTCPeerConnection class available";
        return new NIWebrtcSpawner.prototype.RTCPeerConnection(servers);
    }

    // get the ice servers
    getIceServers(){
        return this.iceServers;
    }

    setIceServers(ice){
        this.iceServers = ice;
    }

    canSpawn(canal={}){
        if(canal.type != this.CANAL_WEBRTC) return false;
        // the spawner must be able to establish a connection to a signaling interface
        if(!this.spawner) return false;
        let canalCanSpawn = this.spawner.canSpawnAny(canal.canals);
        return !!canalCanSpawn;
    }

    // implement the getCanals method
    getCanals(recursion=2){
        return [{
            type : this.CANAL_WEBRTC,
            // send the list of canals that can be used for signaling
            canals : this.signalingInterface && recursion ? this.signalingInterface.getCanals(recursion-1) : []
        }]
    }

    // send local data (ice candidates and offer) to the remote RTCPeerConnection
    sendTransactionSignal(tid){
        let transaction = this.getTransaction(tid);
        if(!transaction) return;

        // only send one offer
        let sendOffer = !transaction.offerSent;
        if(sendOffer) transaction.offerSent = true;

        // build the signalaing data to send
        let signaling = {
            version : 1,
            tid : transaction.tid,
            iceCandidates : transaction.newIceCandidates,

            testoffer : transaction.testoffer
        };
        if(sendOffer) signaling.offer = transaction.offer;
        transaction.newIceCandidates = [];
        try{
            let bytes = this.signalToBytes(signaling);
            let sendResult = transaction.interface.send(bytes, transaction.signalingPort, this.port);
            // if signaling is impossible due to signaling interface problems, reject the spawn tentative
            if(sendResult && sendResult.then){
                sendResult.catch(function(e){
                    transaction.onfailure("signaling interface error : " + JSON.stringify(e))
                })
            }
        }
        catch(e){
            console.log("signalingerror", signaling, e)
        }
    }

    // implement the spawn method
    spawn(canal){
        let signalingInterfacePromise = this.spawner.spawn(canal.canals);
        let $this = this;
        return signalingInterfacePromise.then(function(signalingInterface){
            // bind the interface to process the answer
            if(!signalingInterface.hasHook($this.e.RECEIVE, $this.port) ) signalingInterface.addHook($this.e.RECEIVE, $this.processMessage.bind($this), $this.port);

            let resolve, reject;
            let promResult = new Promise((res,rej)=>{resolve=res; reject=rej;});
            let initiateData = {
                interface : signalingInterface,
                signalingPort : canal.port,
                onsuccess : resolve,
                onfailure : reject
            }
            $this.getPreheatedTransaction(initiateData).then(function(transaction){
                if($this.status() === $this.STOPPED) return;
                $this.addTransaction(transaction);
                $this.sendTransactionSignal(transaction.tid);
            })

            return promResult;
        })
    }

    /*** retrieve a preheated transaction, add some unique data to it :
     tid : id of the ongoing transaction connection
     interface : nestedInterface through which signaling is performed
     signalingPort : port through which signaling message must be sent
     onsuccess : a callback with the spawn as argument
     onfailure : a callback with the error as argument
     ****/
    getPreheatedTransaction(data={}){
        // retrieve a preheated transaction, and rebuild the stock for next use
        let preheatPromise = data.remoteOffer ? this.preheatPeerConnection(data.remoteOffer, data.remoteIceCandidates, this.MAXIMUM_PREHEAT_TIME, false) : this.preheatedTransactions.shift();
        this.preheatAll();

        // add the personalized data and callbacks
        let $this = this;
        let t1 = Date.now();
        if(!NIWebrtcSpawner.prototype.cnt) NIWebrtcSpawner.prototype.cnt = 0;
        let cnt = NIWebrtcSpawner.prototype.cnt++;
        return preheatPromise.then(function(transaction){
            transaction.onsuccess = data.onsuccess || (()=>0);
            transaction.onfailure = data.onfailure || (()=>0);
            transaction.tid = data.tid || $this.createTid();
            transaction.interface = data.interface;
            transaction.signalingPort = data.signalingPort || $this.DEFAULT_SIGNALING_PORT;

            // when a receiveChannel is created, the webrtc connection is completed
            transaction.RTCPeerConnection.ondatachannel = function(event) {
                // stop the signaling interface :
                transaction.interface.stop();
                // fire the spawn when the channel is open
                let onOpenChannel = function(channel){
                    let spawn = new NIWebrtcSpawn(transaction.RTCPeerConnection, transaction.sendChannel, channel);
                    transaction.spawn = spawn;
                    $this.triggerSpawn({ spawn : spawn });
                    $this.removeTransaction(transaction);
                    transaction.onsuccess(spawn);
                }
                if(event.channel.readyState === "open"){
                    onOpenChannel(event.channel);
                }
                else{
                    event.channel.onopen = ()=>onOpenChannel(event.channel);
                }
            }
            return transaction;
        });
    }

    // transform a signal to bytes
    signalToBytes(data){
        if(!data.tid) data.tid = this.createTid();

        let bytes = this._flaptools.encode(data, this.TEMPLATE_SIGNAL);

        return bytes;
    }

    // transform a signal to object
    signalToObject(bytes){
        let res = this._flaptools.decode(bytes, this.TEMPLATE_SIGNAL);
        return res;
    }

    // add a transaction to the list of pending transactions
    addTransaction(req){
        if(!req.tid) req.tid = this.createTid();
        if(!req.startTime) req.startTime = Date.now();
        this._transaction[ req.tid ] = req;
    }

    // add an offer to a transaction
    addTransactionOffer(tr, offer, tst){
        let transaction = typeof tr === "object" ? tr : this.getTransaction(tr);
        if(!transaction) return;
        if(tst) transaction.testoffer = "testoffer : "+offer;
        transaction.offer = offer || "";
        transaction.RTCPeerConnection.setLocalDescription(offer);
    }

    addTransactionRemoteData(tid, data){
        let transaction = this.getTransaction(tid);
        if(!transaction) return;
        this.addTransactionRemoteCandidates(tid, data.iceCandidates || []);
        let sendSignalAnswer = Promise.resolve();
        let $this = this;
        if(data.offer){
            this.addTransactionRemoteOffer(tid, data.offer);
            // if no offer data has already been sent create an offer answer, and send it
            if(!transaction.offerSent){
                transaction.RTCPeerConnection.createAnswer().then(function(offerAnswer){
                    $this.addTransactionOffer(tid, offerAnswer);
                    $this.sendTransactionSignal(tid);
                });
            }
        }
    }

    // add ice candidates to an offer
    addTransactionRemoteCandidates(tr, candidates){
        let transaction = typeof tr === "object" ? tr : this.getTransaction(tr);
        if(!transaction) return;
        if(!Array.isArray(candidates)) candidates = [candidates];
        // TODO : implement check to not send ice candidates if connection is already happening + filter only states that allo to add ice candidate in the following line
        if(transaction.RTCPeerConnection.connectionState === "connecting") return;
        for(let candidate of candidates){
            transaction.RTCPeerConnection.addIceCandidate(candidate).catch(e => {
                // TODO uncomment the line later
                // console.log("Failure during addIceCandidate(): ", e, transaction.RTCPeerConnection.connectionState);
            });
        }
    }

    // add the remoteDecription of a transaction
    addTransactionRemoteOffer(tr, offer){
        let transaction = typeof tr === "object" ? tr : this.getTransaction(tr);
        if(!transaction) return;
        transaction.remoteOffer = offer;

        return transaction.RTCPeerConnection.setRemoteDescription(offer);
    }

    // remove a transaction from the list of pending transactions
    removeTransaction(tr){
        let transaction = typeof tr === "object" ? tr : this.getTransaction(tr);
        if(!transaction) return;
        if(this._transaction[transaction.tid]) delete this._transaction[transaction.tid];
    }

    // get the data of a pending transaction
    getTransaction(id){
        return this._transaction[id] || null;
    }

    createTid(){
        return this._flaptools.randomHex(this.TRANSACTION_ID_LENGTH);
    }

    preheatAll(){
        while(this.preheatedTransactions.length < this.preheat){
            this.preheatedTransactions.push( this.preheatPeerConnection() );
        }
    }

    // create an rtc peer connection with ice candidates and an offer already created
    // create an offer or an answer, depending if a remote offer is provided

    // if the preheating times out, try with a superior timeout time
    preheatPeerConnection(remoteOffer, iceCandidates, timeout=this.MAXIMUM_PREHEAT_TIME, icePreheating=true){
        let peerConnection = this._createPeerConnection();
        let transaction = {
            RTCPeerConnection : peerConnection,
            sendChannel : peerConnection.createDataChannel('sc1'),
            newIceCandidates : [],
        };

        // retrieve ice candidates : resolve once it is done
        let candidateResolve;
        let candidatePromise = new Promise(r=>candidateResolve=r);
        peerConnection.onconnectionstatechange = function(){ }
        if(icePreheating){
            peerConnection.onicecandidate = function(e){
                if(e.candidate) transaction.newIceCandidates.push(e.candidate);
                else candidateResolve();
            }
            peerConnection.onicegatheringstatechange = function() {
                if(peerConnection.iceGatheringState === "complete") candidateResolve();
            }
        }
        else{
            candidateResolve();
        }

        // add remote offer if provided and create an offer : resolve once it is done
        let addRemotePromise = remoteOffer ? this.addTransactionRemoteOffer(transaction, remoteOffer) : Promise.resolve();

        let $this = this;
        let offerPromise = addRemotePromise.then(function(){
            return (!!remoteOffer ? peerConnection.createAnswer() : peerConnection.createOffer() )

                .then(offer=> {
                        $this.addTransactionOffer(transaction, offer, true);
                        if(iceCandidates) $this.addTransactionRemoteCandidates(transaction, iceCandidates);
                    },
                    function(e){console.log("webrtc preheat error : ", e)}
                );
        });
        // resolve automatically after a timeout
        let isTimeout = false;
        let timeoutPromise = new Promise(r=>setTimeout(()=>{isTimeout=true; r(false);}, timeout));
        NIWebrtcSpawner.prototype.spawner = (NIWebrtcSpawner.prototype.spawner||0)+1;
        let promres = offerPromise.then( ()=> Promise.race([candidatePromise, timeoutPromise]) )
            .then(function(isCorrect){
                // if timeout occured and peerConnection needs a complete ice gathering, try again with a new peerConnection
                if(isTimeout && !$this.passIceTimeout){
                    peerConnection.close();
                    return $this.preheatPeerConnection(remoteOffer, iceCandidates, timeout*2);
                }
                else{
                    return transaction;
                }
            })
            .catch(
                function(){console.log("CATCHHHHHHHHHHHHHHHHHHH")});
        return promres;
    }

    // initialize hooks on the signalingInterface
    intializeSignalingInterface(){
        if(!this.signalingInterface) return;
        let $this = this;
        this.signalingInterface.addHook(this.e.SPAWN, function(event){
            let spawn = event.spawn;
            spawn.addHook($this.e.RECEIVE, $this.processMessage.bind($this), $this.port);
        })
    }
}


// initialize some constants in the prototype
var pr = NIWebrtcSpawner.prototype;
pr.VERSION_LENGTH = 1;
pr.TRANSACTION_ID_LENGTH = 6;
pr.OFFER_LENGTH_LENGTH = 2;
pr.CANDIDATE_NUMBER_LENGTH = 1;
pr.CANDIDATE_LENGTH_LENGTH = 1;
pr.OPERATION_INITIATE_CONNECTION = 0;
pr.OPERATION_ERROR = 1;
pr.CANAL_WEBRTC = 3;
pr.DEFAULT_SIGNALING_PORT = 516;
pr.MAXIMUM_PREHEAT_TIME = 500;
pr.PRELOADED_ICE_CANDIDATES = 100;

// initialize EE for offer

// initialize the EE for communicating with the server
var signal_ee = [
    { n : "version", t : "int", c : 0, d : 0},
    { n : "length", t : "int", c : 1},
    { n : "tid", t : "hex", c : 3},
    { n : "offer", t : "json", c : 4},
    { n : "iceCandidates", t : "arr", c : 5, tpl : { n : "iceCandidate", t : "json", c : 0} }
]
pr.TEMPLATE_SIGNAL = {
    ee : {}
};
signal_ee.forEach(ee=>pr.TEMPLATE_SIGNAL.ee[ee.n] = ee);



class NIWebrtcSpawn extends NestedInterface{
    constructor(peerConnection, sendChannel, receiveChannel){
        super();
        this.name = "NIWebrtcSpawn";
        this.core({
            peerConnection : peerConnection,
            sendChannel : sendChannel,
            receiveChannel : receiveChannel
        });
        this.messages = {};
        // create message id incrementally
        this._mid = 1;

        this.initializeChannels();
        this.start();
    }

    // extends the start method
    start(){
        super.start();
        this.startCron();
    }

    // start the cron method
    startCron(){
        this.cronId = setInterval(this.cron.bind(this), this.cronInterval);
    }

    // stop the cron method
    stopCron(){
        if(typeof this.cronId !== "number") return;
        clearInterval(this.cronId);
        this.cronId = null;
    }

    // cron method
    cron(){
        let now = Date.now();

        // remove messages that timed out
        for(let mid in this.messages){
            let message = this.messages[mid];
            if(message.time + this.expireLimit > now) continue;
            else delete this.messages[mid];
        }
    }

    // implement the send method
    send(message, dp=0, fp=0){
        //if(this.status() === this.STOPPED) return;
        let packet = {
            message : message,
            port : dp,
            fport : fp,
        }
        let bytes = this.packetToBytes(packet);

        // create a new id for the message
        let mid = this._mid++;
        // chunk id MUST start at 0
        let cid = 0;
        // send the chunk one by one
        let index = 0;

        while(index<bytes.length){
            let lastIndex = index + this.maxChunkSize;
            let isLastChunk = lastIndex >= bytes.length;
            let chunkData = {
                mid : mid,
                cid : cid++,
                chunk : bytes.slice(index, lastIndex),
                isLast : isLastChunk,
            }
            this.sendChunk(chunkData);
            if(isLastChunk) break;
            index = lastIndex;
        }
        NestedInterface.prototype.send.call(this, message, dp, fp);
    }

    // send a chunk of message
    sendChunk(chunkData){
        let bytes = this._flaptools.encode(chunkData, this.TEMPLATE_CHUNK);

        if(this._core.sendChannel.readyState !== "open"){
            throw "the webrtc canal is not open. state : " + this._core.sendChannel.readyState;
        }
        this._core.sendChannel.send( bytes );
    }

    // process a chunk of data : concatenate them into a packet to be sent to NestedInterface onReceive hook
    processChunk(chunkData){

        let mid = chunkData.mid;
        if(!this.messages[mid]){
            this.messages[mid] = {
                chunks: {},
                time : Date.now(),
                waitingChunk : 0,
                lastChunk : +Infinity,
            }
        }
        let msg = this.messages[mid];
        let cid = chunkData.cid;
        msg.chunks[cid] = chunkData.chunk;
        if(chunkData.isLast) msg.lastChunk = cid;
        while(msg.waitingChunk in msg.chunks) msg.waitingChunk++;
        if(msg.waitingChunk < msg.lastChunk) return;

        // since all the chunks are gathered, agregate them
        let chunks = [];
        for(let i=0; i<=msg.lastChunk; i++) chunks.push(msg.chunks[i]);
        let packetBytes = this._flaptools.agregateBytes(chunks);
        delete this.messages[mid];
        let packet = this.bytesToPacket(packetBytes);
        packet.interface = this;
        this.triggerReceive(packet);
    }

    // implementation of stop method
    stop(){
        this._core.peerConnection.close();
        this._core.sendChannel.close();
        this._core.receiveChannel.close();
        NestedInterface.prototype.stop.call(this);
    }

    // getters for the channels
    getSendChannel(){
        return this._core.sendChannel;
    }
    getReceiveChannel(){
        return this._core.receiveChannel;
    }
    getPeerConnetion(){
        return this._core.peerConnection;
    }

    // initialize channels events
    initializeChannels(){
        let receiveChannel = this.getReceiveChannel();
        let peerConnection = this.getPeerConnetion();
        let $this = this;
        receiveChannel.onmessage = function(event){
            if($this.status() === $this.STOPPED) return;
            let blob = event.data; // new Uint8Array(event.data);
            $this.blobToUint8Array(blob).then(function(bytes){
                try{
                    let chunkData = $this._flaptools.decode(bytes, $this.TEMPLATE_CHUNK);
                    $this.processChunk(chunkData);
                }
                catch(e){
                    console.log("process errorr :: ", e)
                }
                return;
                let packet = $this.bytesToPacket(bytes);
                packet.interface = $this;
                $this.triggerReceive(packet);
            })
        }

        peerConnection.oniceconnectionstatechange = function() {
            if(peerConnection.iceConnectionState == 'disconnected') {
                $this.stop()
            }
        }

        receiveChannel.onclose = function(){
            $this.stop()
        }
    }

    // transform a blob into a Uint8Array : return a promise
    blobToUint8Array(blob){
        // if it is an arrayBuffer
        if(blob instanceof ArrayBuffer){
            return Promise.resolve(new Uint8Array(blob));
        }

        // for browser environment
        if(typeof FileReader != "undefined"){
            let resolve;
            let prom = new Promise(r=>resolve=r);
            var fileReader = new FileReader();
            fileReader.onload = function(event) {
                let bytes = new Uint8Array(event.target.result);
                resolve(bytes);
            }
            fileReader.readAsArrayBuffer(blob);
            return prom;
        }

        if(typeof Buffer != "undefined") {
            var buff = new Buffer( blob, 'binary' );
            var bytes = new Uint8Array(buff);
            return Promise.resolve(bytes);
        }

        throw "No way to extract Blob data : fileReader and Buffer undefined";
    }
}
var pr = NIWebrtcSpawn.prototype;
pr.cronInterval = 40000;
pr.expireLimit = 40000; // time after which a pending message will not be processed
pr.maxChunkSize = 15900; // when sending messages, set the maximum chunk to be sent (compatibility firefox to chrome)

// initialize the EE for the message chunks
var message_ee = [
    { n : "version", t : "int", c : 0, d : 0},
    { n : "mid", t : "int", c : 3, d : 0}, // message id
    { n : "cid", t : "int", c : 4, d : 0}, // chunk id
    { n : "chunk", t : "bin", c : 5, d : new Uint8Array()},
    { n : "isLast", t : "bool", c : 6, d : false},
]
pr.TEMPLATE_CHUNK = {
    ee : {}
};
message_ee.forEach(ee=>pr.TEMPLATE_CHUNK.ee[ee.n] = ee);


export {
    NIWebrtcSpawner,
    NIWebrtcSpawn,
}