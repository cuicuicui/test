/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

/** the NIHttpSpawner encapsulate a http server for the use of the NestedInterfaces
***/

import {NestedInterface} from "./NestedInterface.js"

class NIHttpSpawner extends NestedInterface {
	// the constructor takes a server.io
	constructor(data={}){
		super();
		this.name = "NIHttpSpawner";
		this.address = data.address || null;
		this.serverPort = String(data.serverPort || "");
		this.serverRoute = String(data.serverRoute || "");
        this.withServer = data.hasOwnProperty("withServer") ? data.withServer : true;
		if(this.withServer){
            let core = this._createHttpServer(this.serverPort, this.serverRoute);
            this.core(core);
		}
		this.status(this.RUNNING);

		// constant initialization
		this.CANAL_HTTP = 1;
	}

	// implement the AbstractNestedInterface methods

	// method to start the connexion
	start(){
		NestedInterface.prototype.start.call(this);
	}

	// method to stop the server
	stop(){
		if(this._core) this._core.listen.close();
		NestedInterface.prototype.stop.call(this);
	}

	getCanals(){
		return this.withServer ? [{
			type : this.CANAL_HTTP,
			address : this._getFullAddress()
		}]
		: [];
	}

	// implement canSpawn method
	canSpawn(canal={}){
		if(canal.type != this.CANAL_HTTP) return false;
		if(!canal.address) return false;
		return true;
	}

	// implement the spawn method
	spawn(canal){
		if(!(canal && this.canSpawn(canal))) return Promise.reject("invalid canal for spawn");
		let spawnBuilder = function(){
			return new NIHttpRequest(canal.address);
		};
		return this._buildAndWaitInterface(spawnBuilder);
	}

	_getFullAddress(){
		if(!this.address) return null;
		let address = this.address;
		if(this.serverPort) address += ":"+this.serverPort;
		if(this.serverRoute && this.serverRoute[0]){
			if(this.serverRoute[0] !== "/") address += "/";
			address += this.serverRoute;
		}
		return address;
	}

	// create a spawn from an incoming message
	_createSpawn(packet, res){
		// create a function to send back data
		let resultMethod = function(packet){
            res.header("Access-Control-Allow-Origin", "*");
			let bytes = this.packetToBytes(packet);
			let ascii = this.bytesToAscii(bytes);
            res.end(ascii);
		};
		// create a simple interface to send back an answer
		let spawn = new NIHttpAnswer(resultMethod);
		// trigger the spawn event
		let triggerData = {
			spawn : spawn,
			message : packet.message,
			port : packet.port,
			fport : packet.fport,
			interface : spawn
		};
		// trigger a spawn then a receive event
        this.triggerSpawn(triggerData);
        this.triggerReceive(triggerData);
	}

	// create a http server
	_createHttpServer(port, route){
		if(!port) port = 80;
		if(!route) route = "/";
		if(!NIHttpSpawner.prototype.expressLib) NIHttpSpawner.prototype.expressLib = require("express");
		let server = NIHttpSpawner.prototype.expressLib();
		if(!NIHttpSpawner.prototype.bodyParserLib) NIHttpSpawner.prototype.bodyParserLib = require('body-parser');
		server.use (function(req, res, next) {
			var data='';
			req.on('data', function(chunk) {
			   data += chunk;
			});

			req.on('end', function() {
				req.body = data;
				next();
			});
		});
		// link the route the message parsing
		let spawner = this;
		let requestProcessor = function(req, res){
			let bytesStr = req.body || "";
			let bytes = spawner.asciiToBytes(bytesStr);
			let packet = spawner.bytesToPacket(bytes);
		    spawner._createSpawn(packet, res);
		};
		// link the route to the request processing
		server.post(route, requestProcessor);
		let listen = server.listen( port );

		let core =  {
			port : port,
			route : route,
			server : server,
			listen : listen
		}
		return core;
	}
}

class NIHttpAnswer extends NestedInterface{
	constructor(resultMethod){
		super();
		this.name = "NIHttpSpawn";
		this.core(resultMethod.bind(this));
		this.status(this.RUNNING);
	}
	// implement the send method
	send(message, dp=0, fp=0){
		let id = Math.random();
		this.hassent = id;
		let packet = {
			message : message,
			port : dp,
			fport : fp
		};
		this._core(packet);
		NestedInterface.prototype.send.call(this, message, dp, fp);
		this.stop();
	}
}

// indicate whether the interface can work for several message exchanges or not
NIHttpAnswer.prototype.multiExchanges = false;

class NIHttpRequest extends NestedInterface{
	constructor(fullAddress){
		super();
		this.name = "NIHttpRequest";
		this.core({
			address : fullAddress
		});
        this._environment = typeof window !== 'undefined' ? "browser" : "nodejs";
		this.status(this.RUNNING);
	}

	// implement the send method
	send(message, dp=0, fp=0){
		let spawn = this;
		let data = {
			url : this._core.address,
			message : message,
			port : dp, // destination port
			fport : fp, // origin port
			callback : function(res){
				spawn.triggerReceive({
						name : spawn.e.RECEIVE,
						port : res.port,
						fport : res.fport,
						message : res.message,
						interface : spawn
					});
				spawn.stop();
			}
		}
		this.httpRequest(data);
		NestedInterface.prototype.send.call(this, message, dp, fp);
	}

	// perform an http request
	httpRequest(data){
		if( this._environment === "browser") return this.sendHttpBrowser(data);
		else return this.sendHttpNodejs(data);
	}

	// method to send an http/https request with nodejs
	sendHttpNodejs(data){
		let spawn = this;
		if( !NIHttpRequest.prototype.request ) NIHttpRequest.prototype.request = require('request');
		let bytes = this.packetToBytes(data);
		let bytesStr = this.bytesToAscii(bytes);
		let req = NIHttpRequest.prototype.request({
		  method : "POST",
		  headers: { 'Content-Type': 'application/octet-stream' },
		  body : bytesStr,
		  url : data.url,
		}, function(error, response, body){
			if(!response) return;
			// in case of success
			try{
				let bytes = spawn.asciiToBytes(body);
				let packet = spawn.bytesToPacket(bytes);
				return data.callback(packet);
			}
			catch(e){
				console.log(e);
			}
		});
	}

	// method to send an http/https request from a web browser
	sendHttpBrowser(data){
		let $this = this;
        let bytes = this.packetToBytes(data);
        let bytesStr = this.bytesToAscii(bytes);
		let http = new XMLHttpRequest();
        http.responseType = "text";
		http.open( "POST", data.url, true);
		http.onload = function() {//Call a function when the state changes.
			if(http.readyState == 4 && http.status == 200) {
                let bytes = $this.asciiToBytes(http.response);
                let packet = $this.bytesToPacket(bytes);
                if(data.callback) data.callback(packet)
			}
		}
        http.send(bytesStr);
	}
}


export {
    NIHttpSpawner,
    NIHttpAnswer,
    NIHttpRequest
}