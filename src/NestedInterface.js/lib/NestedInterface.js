/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/



import {Flaptools} from "../../flaptools/lib/Flaptools.js"

class NestedInterface{
	constructor(){
		// initialize the events
		this.initializeEvents();
		
		// current status
		this._status = this.NEW;
	}
	
	/*** the following are abstract methods that needs to be implemented
	***/
	
	// method to start the interface
	start(){
		this.triggerConnect();
		this.status(this.RUNNING);
	}
	
	// method to stop the interface
	stop(){
		this.status(this.STOPPED);
		this.triggerDisconnect();
	}
	
	// method to send a Uint8Array through the nested interface
	send(message, dp=0, fp=0){
		this.triggerSend({
			message : message, 
			port : dp,
			fport : fp
		});
	}
	
	// check if the spawner can create a spawn from a canal data
	canSpawn(canal={}){
		return false;
	}

	// check if one of the given canal can be spawned
	canSpawnAny(canals=[]){
		for(let c of canals){
			if(this.canSpawn(c)) return c;
		}
		return false;
	}
	
	// create a new spawn from a canal data
	spawn(canal={}){
		throw "cannot create spawn from NestedInterface abstract class";
	}
	
	// for spawner only : give the list of canals that can be used to access the spawner
	// result is an array of canal, each of which must contains :
	// type : the integer type identifying the canal type
	getCanals(){
		return [];
	}
	
	status(newStatus=null){
		switch(newStatus){
			case this.RUNNING :
			case this.STOPPED :
			case this.UNKNOWN :
			case this.NEW     :
				this._status = newStatus;
				break;
			// by default, don't change the status if non eis porvide or fi invalid
			default : 
				break;
		}
		return this._status;
	}
	
	/*** THE FOLLOWING ARE THE METHDOS TO BE USED IN INHERITING CLASS
	 IT TRIGGERS REGISTERED HOOKS UPON EVENTS
	***/
	
	// trigger the CONNECT event
	triggerConnect(data={}){
		let triggerData = {
			name : this.e.CONNECT,
			interface : data.interface || this,
		}
		this.trigger( triggerData );
	}
	
	// trigger the DISCONNECT EVENT
	triggerDisconnect(data={}){
		let triggerData = {
			name : this.e.DISCONNECT,
			interface : data.interface || this,
		}
		this.trigger( triggerData );
	}
	
	// trigger the RECEIVE EVENT
	triggerReceive(data={}){
		let triggerData = {
			name : this.e.RECEIVE,
			message : data.message,
			port : data.port,
			fport : data.fport,
			interface : data.interface || this,
		}
		this.trigger( triggerData );
	}
	
	// trigger the SEND EVENT
	triggerSend(data={}){
		let triggerData = {
			name : this.e.SEND,
			message : data.message,
			port : data.port,
			fport : data.fport,
			interface : data.interface || this
		}
		this.trigger( triggerData );
	}
	
	// trigger the SPAWN EVENT : input is the bytes associated to the generation of the spawn
	triggerSpawn(data={}){
		let triggerData = {
			name : this.e.SPAWN,
			message : data.message,
			port : data.port,
			fport : data.fport,
			spawn : data.spawn,
			interface : data.interface || this,
		}
		this.trigger( triggerData );
	}

	// check if the interface has a registered hook for the event / port
	hasHook(eventName, port=null){
		let events = this.hooks[eventName] || [];
		for(let e of events){
			if(port === null || port == e.port) return e;
		}
		return false;
	}
	
	// expect at least a hook name, and any number of parameter for the hook
	// the hook can either be a hook name, or an object containing the name and the port of the hook triggered
	trigger(event, hooksData){
		// retrieve the list of hooks that might be activated
		if(!hooksData) hooksData = this.hooks;
		if(!this.e.hasOwnProperty(event.name)){
			throw "no event "+event.name+"could be triggered. Only the following events are available : "+Object.keys(this.e).join(", ");
		}
		if(!hooksData) hooksData = this.hooks;
		// check there is at least one hook to trigger
		if(hooksData[event.name].length === 0 ) return;

		for(let hookData of hooksData[event.name]){
			// skip hooks with the wrond port
			if( (event.port && hookData.port) && event.port != hookData.port) continue;
			try{
				hookData.method.call(null, event);
			}
			catch(e){
				console.log(e);
			}
		}
	}
	
	/*** methods to initialize and manage hooks
	 * ***/
	 addHook(hookName, method, port=null){
		 // check the hook to implement exists
		 if(!this.hookExists(hookName)) return false;
		 // check the method is a function
		 if( !(method && {}.toString.call(method) === '[object Function]')) return false;
		 this.hooks[hookName].push({
			method : method,
			port : port
		});
	 }
	 
	 getHooks(hookName){
		return this.hookExists(hookName) ? this.hooks[hookName] :  null;
	 }
	 
	 hookExists(hookName){
		 return typeof hookName === "string" && hookName in this.hooks;
	 }
	 
	 // remove hooks given a hookname
	 // accept additional params : if methods are provided after the hookname, remove only the given methods
	 removeHooks(hookName){
		 // check the hook to process exists
		 if(!this.hookExists(hookName)) return false;
		 let methodsToRemove = Array.from(arguments).slice(1);
		 if(methodsToRemove.length === 0){
			 this.hooks[hookName] = [];
		 }
		 else{
			var newHooks = [];
			for(let hook of this.hooks[hookName]){
				if(!methodsToRemove.includes(hook.method)){
					newHooks.push(hook);
				}
			}
			this.hooks[hookName] = newHooks;
		 }
		 return true;
	 }
	
	// initialize the list of available events for use
	initializeEvents(){
		// list some hooks to be executed upon every event
		this.hooks = {};
		for(let event in this.e) this.hooks[event] = [];
	}
	
	// retrieve/set the core data
	core(data=null){
		if(data) this._core = data;
		return this._core;
	}
	

	/*** some interface are connexions created by a server (socket.io for instance)
	 * those interfaces are called spawns : they keep a reference of a parent (spawnParent property) and their parent refer to them with an id (spawnId property)
	 * ***/
	isSpawn(){ return !!this.getSpawnParent() }
	
	getSpawnParent(){ return this._spawnParent; }
	setSpawnParent(parent){ this._spawnParent = parent; }
	
	
	/*** other helpers
	 * ***/
	 // check if an object is inheriting of AbstractNestedInterface
	isInterface(i){
		return i instanceof AbstractNestedInterface;
	}
	
	bytesToAscii(codePoints){
		let result = String.fromCharCode(...codePoints);
		return result;
	}
	
	asciiToBytes(ascii="", arr=null, offset=0){
		if(!offset) offset = 0;
		if(!arr) arr = new Uint8Array(ascii.length);
		for(let i =0; i<ascii.length; i++){
			arr[offset+i] = ascii.charCodeAt(i);
		}
		return arr;
	}
	
	// check if the environment is browser(true) or nodejs
	isNode(){
		return typeof global !== "undefined";
	}
	
	// return a promise that resolves when the interface status is not NEW anymore
	_buildAndWaitInterface(builder){
		let promise = new Promise(function(resolve, reject){
			try{
				let ni = builder();
				if(ni.status() !== ni.NEW) return resolve(ni);
				// add a hook to be triggered when the interface status is ready
				ni.addHook(ni.e.CONNECT, function(e){
					ni.removeHooks(ni.e.CONNECT);
					ni.removeHooks(ni.e.DISCONNECT);
					resolve(ni);
				});
				// add a hook in case the ni stopped right before even starting
				ni.addHook(ni.e.DISCONNECT, function(e){
					ni.removeHooks(ni.e.CONNECT);
					ni.removeHooks(ni.e.DISCONNECT);
					reject("Unable to connect");
				});
			}
			catch(e){ reject(e); }
		});
		return promise;
	}
	
	// transform an array buffer into a packet
	// structure of a buffer is the following :
	// 1 byte : number of 4bytes header words (header are everything until the message excluded)
	// 2 bytes : source port (fport)
	// 2 bytes destination port (port)
	// 4 bytes : message length
	// .... : everything that is not yet specified in the header but might be added later
	// N bytes : message
	bytesToPacket(bytes){
		if(!(bytes instanceof Uint8Array)) bytes = new Uint8Array(bytes);
        return this._flaptools.decode(bytes, this.TEMPLATE_MESSAGE);

		let res = {};
		let index = 0;
		let headerLength = bytes[index++];
		res.fport = 256 * bytes[index++] + bytes[index++];
		res.port  = 256 * bytes[index++] + bytes[index++];
		
		let messageLength = 0;
		for(let i=0; i<4; i++) messageLength = messageLength * 256 + bytes[index++];
		index = 1+4*headerLength;
		res.message = bytes.slice(index, index+messageLength);
		
		return res;
	}
	
	// transform 
	packetToBytes(packet){
        return this._flaptools.encode(packet, this.TEMPLATE_MESSAGE);

		let headLength = 8; // 2*2 for ports +4 for message length
		if(!packet.message) packet.message = new Uint8Array();
		let messageLength = packet.message.length;
		let resultLength = messageLength + headLength + 1;
		let bytes = new Uint8Array(resultLength);
		let index = 0;
		
		// insert header length (counted as 32 bytes words)
		bytes[index++] = headLength / 4;
		
		// insert port
		if(!packet.port) packet.port = 0;
		if(!packet.fport) packet.fport = 0;
		bytes[index++] = Math.floor(packet.fport/256);
		bytes[index++] = packet.fport & 0xff;
		bytes[index++] = Math.floor(packet.port/256);
		bytes[index++] = packet.port & 0xff;
		
		// insert message length
		for(let i=3; i>=0; i--){
			bytes[index+i] = messageLength & 0xff;
			messageLength = messageLength/256 >> 0;
		}
		index += 4;
		
		// insert the message
		bytes.set(packet.message, index);
		return bytes;
	}
}


// initialize NestedInterface prototype
let p = NestedInterface.prototype;
// set events
p.e = {
	CONNECT : "CONNECT",
	DISCONNECT : "DISCONNECT",
	RECEIVE : "RECEIVE",
	SEND : "SEND",
	SPAWN : "SPAWN", // event when a new sub-connexion is created (specific to server Interfaces)
};

// indicate whether the interface can work for several message exchanges or not
p.multiExchanges = true;

// set possible status
p.RUNNING = "UP";
p.UP = p.RUNNING;
p.STOPPED = "STOPPED";
p.UNKNOWN = "UNKNOWN";
p.NEW = "NEW";
		
// retrieve a flaptools instance
// let globvar = typeof global === "undefined" ? window : global;
// if(! globvar.Flaptools){
//     globvar.Flaptools = require("Flaptools")
// }
// if(typeof global !== "undefined" && global.Flaptools) p._flaptools = new global.Flaptools();
// else p._flaptools = new Flaptools();

p._flaptools = new Flaptools();


// initialize the signature dictionnary
let EE_NI_MSG = [
    {n:"version", t:"int", c:0},
    {n:"length", t:"int", c:1},
    {n:"port", t:"int", c:2, d : 0},
    {n:"fport", t:"int", c:3, d : 0},
    {n:"message", t:"bin", c:4, d : new Uint8Array() },
]
p.TEMPLATE_MESSAGE = {
    ee : EE_NI_MSG
};

// intialize the canal dictionnary
p.TEMPLATE_CANAL = {};
p.TEMPLATE_CANAL.ee = [
    {n:"type", t:"int", c:2},
    {n:"address", t:"ascii", c:3},
    {n:"key", t:"hex", c:4},
    {n:"canals", t:"arr", c:5, tpl : p.TEMPLATE_CANAL},
    {n:"receiver", t:"hex", c:6}
];


export {
    NestedInterface
}