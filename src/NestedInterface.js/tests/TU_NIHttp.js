/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

/**
 * Tests for the NestedInterface and AbstractNestedInterface
 * 
 * **/

// CONSTANTS FOR THE TESTING
let SERVER_PORT = 9010;

///////////////////// testing modules /////////////////////
let chai = require("chai");
let chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
let assert = chai.assert;

// tested modules
var flocklibs = require("../../../dist/flock-nodejs.js")
Object.assign(global, flocklibs);

const ASYNC_TIMEOUT = 200; // time for asynchronous test to occur

describe("creating a NIHttp", function() {
    it("should just create a new NIHttpSpawner without error", function() {
		let ni = new NIHttpSpawner();
		assert.ok(true);
		ni.stop();
    });
});

describe("testing the getCanals method", function() {
	let HTTP_CANAL_TYPE = 1;
	
    it("should return a canal without the address specified", function() {
		let niData = {
			serverPort : null,
			serverRoute : null
		};
		let ni = new NIHttpSpawner(niData);
		let canals = ni.getCanals();
		ni.stop();
		let expectedCanal = {
			type : HTTP_CANAL_TYPE,
			address : null
		};
		let expectedLength = 1;
		assert.isArray(canals);
		assert.lengthOf(canals, expectedLength);
		assert.deepEqual(canals[0], expectedCanal);
    });
    
    it("should return a canal with the address specified", function() {
		let niData = {
			address : "www.example.com",
			serverPort : null,
			serverRoute : null
		};
		let ni = new NIHttpSpawner(niData);
		let canals = ni.getCanals();
		ni.stop();
		let expectedCanal = {
			type : HTTP_CANAL_TYPE,
			address : "www.example.com"
		};
		let expectedLength = 1;
		assert.isArray(canals);
		assert.lengthOf(canals, expectedLength);
		assert.deepEqual(canals[0], expectedCanal);
    });
    
    it("should return a canal with the address and port specified", function() {
		let niData = {
			address : "www.example.com",
			serverPort : 12345,
			serverRoute : null
		};
		let ni = new NIHttpSpawner(niData);
		let canals = ni.getCanals();
		ni.stop();
		let expectedCanal = {
			type : HTTP_CANAL_TYPE,
			address : "www.example.com:12345"
		};
		let expectedLength = 1;
		assert.isArray(canals);
		assert.lengthOf(canals, expectedLength);
		assert.deepEqual(canals[0], expectedCanal);
    });

    it("should return a canal with the address and port and path specified", function() {
        let niData = {
            address : "www.example.com",
            serverPort : 12345,
            serverRoute : "path/path2/path3"
        };
        let ni = new NIHttpSpawner(niData);
        let canals = ni.getCanals();
        ni.stop();
        let expectedCanal = {
            type : HTTP_CANAL_TYPE,
            address : "www.example.com:12345/path/path2/path3"
        };
        let expectedLength = 1;
        assert.isArray(canals);
        assert.lengthOf(canals, expectedLength);
        assert.deepEqual(canals[0], expectedCanal);
    });

    it("should return an empty canal if no http server is attached", function() {
        let niData = {
            address : "www.example.com",
            serverPort : 12345,
            serverRoute : "path/path2/path3",
			withServer : false
        };
        let ni = new NIHttpSpawner(niData);
        let canals = ni.getCanals();
        ni.stop();
        assert.isArray(canals);
        assert.lengthOf(canals, 0);
    });
});

describe("testing the implementation of the abstract NestedInterface methods in the NIHttpSpawner", function() {
	let socketIoPort = null;
	let socketIo = null;
	let nispawner = null;
	let shiftCounter = null;
	
	// helper to create client to contact the server
	let createConnection = function(){
		let destination = 'http://127.0.0.1:'+SERVER_PORT;
		var req = new NIHttpRequest(destination); 
		return req;
	}
	
	// helper to shift a function execution in time
	let shift = function(f){
		setTimeout(f, shiftCounter * ASYNC_TIMEOUT);
		shiftCounter++;
	}
	
	beforeEach(function(){
		shiftCounter = 1;
		let data = {
			serverPort : SERVER_PORT, 
			serverRoute : "/"
		};
		nispawner = new NIHttpSpawner(data);
		return new Promise((resolve) =>  setTimeout(resolve, ASYNC_TIMEOUT)); // async to let server start enough time to occur	
	});
	
	afterEach(function(){
		nispawner.stop();
		SERVER_PORT++;
		return new Promise((resolve) =>  setTimeout(resolve, ASYNC_TIMEOUT)); // async to let server stop enough time to occur	
	});
	
    it("creating a NIWebsocketSpawner, then stop it", function() {
			assert.ok(nispawner.status(), "RUNNING");
			nispawner.stop();
			assert.ok(nispawner.status(), "STOPPED");
			assert.notOk( nispawner._core.listen.connected, "the socket should be deconnected");
    });


    it("creating a NestedServer listening to new connections", function(done) {
		let connectionTriggered = false;
		var hook = function(){
			connectionTriggered = true;
		}
		nispawner.addHook( nispawner.e.SPAWN, hook);
		
		// create a new connection : it should set our test variable to true
		assert.isFalse(connectionTriggered, "SPAWN connection is not yet established");
		let socketClient = createConnection().send( new Uint8Array() );
		shift(function(){
			assert.isTrue(connectionTriggered, "SPAWN connection is established");
			done();
		});
    });

    it("should close the server and trigger the associated hook", function(done) {
		let disconnectionTriggered = false;
		var hook = function(){
			disconnectionTriggered = true;
		}
		nispawner.addHook( nispawner.e.DISCONNECT, hook);
		
		// create a new connection : it should set our test variable to true
		assert.isFalse(disconnectionTriggered, "disconnection did not occured yet");
		nispawner.stop();
		shift(function(){
			assert.isTrue(disconnectionTriggered, "disconnection occured");
			done();
		});
    });

    it("should create a spawn when establishing a connection", function(done) {
		let spawn = null;
		var hook = function(event){
			spawn = event.spawn;
		}
		nispawner.addHook( nispawner.e.SPAWN, hook);
		
		// create a new connection : it should set our test variable to true
		assert.isNull(spawn, "connection is not yet established");
		let socketClient = createConnection().send( new Uint8Array() );
		shift(function(){
			assert.isNotNull(spawn, "connection is established");
			assert.isTrue(spawn instanceof NIHttpAnswer, "the spawn has the right type");
			done();
		});
    });

    it("should trigger the disconnect event of the server and stop receiving new connections", function(done) {
		let spawnCounter = 0;
		let disconnectProbe = false;
		var hookConnect = function(spawnReceived){
			spawnCounter++;
		}
		let hookDisconnect = function(){
			disconnectProbe = true;
		}
		nispawner.addHook( nispawner.e.SPAWN, hookConnect);
		nispawner.addHook( nispawner.e.DISCONNECT, hookDisconnect);
		
		// create a new connection : it should set our test variable to true
		assert.equal(spawnCounter, 0, "connection is not yet established");
		let socketClient = null;
		
		shift(function(){ 
			createConnection().send( new Uint8Array()) });
		
		shift(function(){
			assert.equal(spawnCounter, 1, "one connection has been established");
		});
		
		// trigger the disconnection
		shift(function(){
			assert.isFalse(disconnectProbe, "disconnection did not occured");
		});
		
		shift(function(){
			nispawner.stop();
			assert.isTrue(disconnectProbe, "disconnection occured");
		});
		
		// connect event should no longer be triggered
		shift(function(){
			spawnCounter = 0;
			createConnection().send( new Uint8Array() );
		});
		
		shift(function(){
			assert.equal(spawnCounter, 0, "no more connection have been established");
			done();
		});
    });

    it("receive messages and transfer them through the hooks", function(done) {
		let lastMessageReceived = null;
		let lastMessageReceivedInterface = null;
		let spawnMessageReceived = null;
		let spawn = null;
		let message1 = new Uint8Array([1,2,3]);
		let message2 = new Uint8Array([4,5,6]);
		let clientMessageReceived = null;
		let message3 =  new Uint8Array([7,8,9]);
		
		var hookSpawn = function(event){
			spawn = event.spawn;
			spawnMessageReceived = event.message;
		}
		var hookReceive = function(event){
			lastMessageReceived = event.message;
			lastMessageReceivedInterface = event.interface;
		}
		nispawner.addHook( nispawner.e.SPAWN, hookSpawn);
		
		// create a new connection and send a message on spawn
		let httpClientSpawn = createConnection();
		httpClientSpawn.addHook(httpClientSpawn.e.RECEIVE, hookReceive);
		httpClientSpawn.send( message1 );
		shift(function(){
			assert.deepEqual(spawnMessageReceived, message1);
		});
		
		// send back a message
		shift(function(){
			spawn.send(message2);
		});
		shift(function(){
			assert.deepEqual(lastMessageReceived, message2, "the message should have been transferred from the client to the NestedSpawn");
			assert.instanceOf(lastMessageReceivedInterface, NIHttpRequest);
		});
		
		// send a second message : doesn't work since a response has already been sent
		shift(function(){
			lastMessageReceived = null;
			try{
				spawn.send(message3);
			}
			catch(e){
			}
		});
		shift(function(){
			assert.isNull(lastMessageReceived, "no new message should be sent ");
			done();
		});
    });
    
    it("should trigger the DISCONNECT event of a spawn after the answer is sent/received", function(done) {
		let disconnectProbe = false;
		let spawn = null;
		let message = new Uint8Array();
		
		var hookSpawn = function(event){
			spawn = event.spawn;
		}
		var hookDisconnect = function(){
			disconnectProbe = true;
		}
		nispawner.addHook( nispawner.e.SPAWN, hookSpawn);
		
		// create a new connection and terminate it
		let httpClientSpawn = createConnection();
		httpClientSpawn.send(message);
		
		shift(function(){
			spawn.addHook(nispawner.e.DISCONNECT, hookDisconnect);
			assert.isFalse(disconnectProbe, "the client has not disconnected yet");
		});
		
		shift(function(){
			spawn.send(message);
		});
		
		shift(function(){
			assert.isTrue(disconnectProbe, "the client should have disconected, and the event should have been triggered on spawn side");
			done();
		});
    });
    
});


describe("testing the canSpawn method", function(){
	let nispawner;
	beforeEach(function(){
		nispawner = new NIHttpSpawner({serverPort:8080});
	});
	
	afterEach(function(){
		nispawner.stop();
	});
	
	it("should succeed with correct data", function(){
		let canal = {
			type : 1,
			address : "www.test.com"
		};
		let result = nispawner.canSpawn(canal);
		let expected = true;
		assert.equal(result, expected);
	});
	
	it("should fail when using an incorrect type", function(){
		let canal = {
			type : 123,
			address : "www.test.com"
		};
		let result = nispawner.canSpawn(canal);
		let expected = false;
		assert.equal(result, expected);
	});
	
	it("should fail without address", function(){
		let canal = {
			type : 1
		};
		let result = nispawner.canSpawn(canal);
		let expected = false;
		assert.equal(result, expected);
	});
});


describe("testing the spawn method", function(){
	let nispawner1, nispawner2;
	
	beforeEach(function(){
		nispawner1 = new NIHttpSpawner({serverPort:8080});
		nispawner2 = new NIHttpSpawner({serverPort:8081});
	});
	
	afterEach(function(){
		nispawner1.stop();
		nispawner2.stop();
	});
	
	it("should return a rejected promise when providing a wrong canal", function(done){
		let prom = nispawner1.spawn({});
		prom.then(
		function(){
			assert.notOk(true, "shoud not resolve successfully");
			done();
		},
		function(){
			assert.ok(true, "shoud fail");
			done();
		});
			
	});
	
	it("should create a spawn that are ready when the promise resolve", function(done){
		let prom = nispawner1.spawn({type : 1, address:"http://127.0.0.1:8081"});
		let eventReceived = null;
		
		prom.then(
		function(){
			assert.ok(true, "should resolve successfully");
			done();
		},
		function(){
			assert.notOk(true, "shoud not fail");
			done();
		});
	});
	
});


