/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

/**
 * Tests for the NestedInterface and AbstractNestedInterface
 * 
 * **/

// CONSTANTS FOR THE TESTING
let SERVER_PORT = 9010;

///////////////////// testing modules /////////////////////
let chai = require("chai");
let chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
let assert = chai.assert;

// tested modules
var flocklibs = require("../../../dist/flock-nodejs.js")
Object.assign(global, flocklibs);



const ASYNC_TIMEOUT = 100; // time for asynchronous test to occur
// helper to shift a function execution in time
let shift = function(f){
    setTimeout(f, shiftCounter * ASYNC_TIMEOUT);
    shiftCounter++;
}
let shiftCounter = 0;

describe("creating a NISwansSpawner", function() {
    it("should just create a new NIHttpSpawner without error", function() {
		let ni = new NISwansSpawner();
		assert.ok(true);
		ni.stop();
    });
});

describe("testing the getCanals method", function() {
	let SWANS_CANAL = 5;
	let receiverId = "aabbcc";

    it("should return no canal when no receiver are specified", function() {
		let ni = new NISwansSpawner({ });
		let canals = ni.getCanals();
		ni.stop();
		let expectedLength = 1;
		assert.isArray(canals);
		assert.isEmpty(canals);
    });
    
    it("return a swan canal with data", function() {
    	// create websocket spawner for swans server interface
		let socketPort = 1234;
		let address = "ws://127.0.0.1";
        let websocketSpawner = new NIWebsocketSpawner({
            socketPort: socketPort,
            address : address
        });

   		// create the swans spawner
		let receiver = new SwanClientReceiver({});
        let ni = new NISwansSpawner({
            receiver : receiver,
            spawner : websocketSpawner,
            swansServerCanals : [{  type : 2, address : address+":"+socketPort }]
        });
        let canals = ni.getCanals();
        ni.stop();
        let expectedCanal = {
            type : SWANS_CANAL,
            canals : [{  type : 2, address : address+":"+socketPort }],
            receiver : receiver.getReceiverId()
        };
        let expectedLength = 1;
        assert.isArray(canals);
        assert.lengthOf(canals, expectedLength);
        assert.deepEqual(canals[0], expectedCanal);
    });
});

describe("testing the canSpawn method", function(){
    // create websocket spawner for swans server interface
    let websocketSpawner = new NIWebsocketSpawner({});

    // create the swans spawner
    let receiver = new SwanClientReceiver({});
    let nispawner = new NISwansSpawner({
        receiver : receiver,
        spawner : websocketSpawner
    });

    after(function(){
        nispawner.stop();
    });

    it("should succeed with correct data", function(){
        let canal = {
            type : 5,
            receiver : "aabbcc",
			canals : [{type : 2, address : "ws://127.0.0.1"}]
        };
        let result = nispawner.canSpawn(canal);
        let expected = true;
        assert.equal(result, expected);
    });

    it("should fail when using an incorrect type", function(){
        let canal = {
            type : 125,
            receiver : "aabbcc",
            canals : [{type : 2, address : "ws://127.0.0.1"}]
        };
        let result = nispawner.canSpawn(canal);
        let expected = false;
        assert.equal(result, expected);
    });

    it("should fail without proper canals to contact the swans server", function(){
        let canal = {
            type : 5,
            receiver : "aabbcc",
            canals : [{type : 211, address : "ws://127.0.0.1"}]
        };
        let result = nispawner.canSpawn(canal);
        let expected = false;
        assert.equal(result, expected);
    });
});

describe("testing the implementation of the abstract NestedInterface methods in the NIHttpSpawner", function() {
	let swansServerClientInterface, swansServerReceiverInterface, swansServer, swansReceiverInterface, swansReceiver, swansSpawner, swansSpawner2
	let nispawner = null;

	// create a swan server
    let socketPortSwansClientInterface = 8888;
    let socketPortSwansReceiverInterface = 8889;
	let address = "ws://127.0.0.1";

	beforeEach(function(){
        shiftCounter = 1;
        swansServerClientInterface = new NIWebsocketSpawner({
            socketPort: socketPortSwansClientInterface,
            address : address+":"+socketPortSwansClientInterface
        });
        swansServerReceiverInterface = new NIWebsocketSpawner({
            socketPort: socketPortSwansReceiverInterface,
            address : address+":"+socketPortSwansReceiverInterface
        });
        swansServer = new SwanServer({
            clientInterface : swansServerClientInterface,
            receiverInterface : swansServerReceiverInterface
        })

        // create a receiver
        let initializationPromise = swansServerClientInterface.spawn({
            type : 2,
            address : address + ":" + socketPortSwansReceiverInterface
        }).then(function(spawn){
            swansReceiverInterface = spawn;
            swansReceiverInterface.test = "test 3"
            swansReceiver = new SwanClientReceiver({
                swansInterface : swansServerReceiverInterface,
            })
            swansReceiver.registerInit(swansReceiverInterface);

            // create the spawner associated to the receiver
            swansSpawner = new NISwansSpawner({
                receiver : swansReceiver,
                spawner : swansServerClientInterface
            });

            // create another swansSpawner, not linked to the receiver
            swansSpawner2 = new NISwansSpawner({
                spawner : swansServerClientInterface
            });
		})

		// end the beforeEach and start test once the spawn have been created
		return initializationPromise;
	});
	
	afterEach(function(){
        swansServerClientInterface.stop();
        swansServerReceiverInterface.stop();
        swansSpawner.stop(); swansSpawner.stop();
		return new Promise((resolve) =>  setTimeout(resolve, ASYNC_TIMEOUT)); // async to let server stop enough time to occur
	});

	// helper to create connection to the swans server
    function createConnection(msg=new Uint8Array([0])){
        return swansSpawner2.spawn({
            type : 5,
            receiver : swansReceiver.getReceiverId(),
            canals : swansServerClientInterface.getCanals()
        }).then(function(spawn) {
            spawn.send(msg, swansServer.receiverPort);
            return spawn;
        });
    }

    it("should fail to create a spawn with the spawn method with the wrong canal", function(done){
        let prom = swansSpawner2.spawn({});
        prom.then(
            function(){
                assert.notOk(true, "shoud not resolve successfully");
                done();
            },
            function(){
                assert.ok(true, "shoud fail");
                done();
            });

    });

    it("should create a spawn with the spawn method that are ready when the promise resolve", function(done){
        let prom = swansSpawner2.spawn({
            type : 5,
            receiver : swansReceiver.getReceiverId(),
            canals : swansServerClientInterface.getCanals()
        });
        let eventReceived = null;

        prom.then(
            function(){
                assert.ok(true, "should resolve successfully");
                done();
            },
            function(){
                assert.notOk(true, "shoud not fail");
                done();
            });
    });


    it("should create a spawn when establishing a connection", function(done) {
        let spawn = null;
        var hook = function(event){
            spawn = event.spawn;
        }
        swansSpawner.addHook( swansSpawner.e.SPAWN, hook);

        // create a new connection : it should set our test variable to true
        assert.isNull(spawn, "connection is not yet established");

        let swansSpawnPromise = createConnection();

        shift(function(){
            assert.isNotNull(spawn, "connection is established");
            assert.isTrue(spawn instanceof NISwansAnswer, "the spawn has the right type");

        });

        shift(done)
    });

    it("should close the server and trigger the associated hook", function(done) {
		let disconnectionTriggered = false;
		var hook = function(){
			disconnectionTriggered = true;
		}
        swansSpawner.addHook( swansSpawner.e.DISCONNECT, hook);

		// create a new connection : it should set our test variable to true
		assert.isFalse(disconnectionTriggered, "disconnection did not occured yet");
        swansSpawner.stop();
		shift(function(){
			assert.isTrue(disconnectionTriggered, "disconnection occured");
			done();
		});
    });


    it("should trigger the disconnect event of the server and stop receiving new connections", function(done) {
		let spawnCounter = 0;
		let disconnectProbe = false;
		var hookConnect = function(spawnReceived){
			spawnCounter++;
		}
		let hookDisconnect = function(){
			disconnectProbe = true;
		}
        swansSpawner.addHook( swansSpawner.e.SPAWN, hookConnect);
        swansSpawner.addHook( swansSpawner.e.DISCONNECT, hookDisconnect);
		
		// create a new connection : it should set our test variable to true
		assert.equal(spawnCounter, 0, "connection is not yet established");
		let socketClient = null;
		
		shift(function() {
            createConnection();
        });

		shift(function(){
			assert.equal(spawnCounter, 1, "one connection has been established");
		});

        shift(function() {
            createConnection();
        });

        shift(function(){
            assert.equal(spawnCounter, 2, "one connection has been established");
        });

		// trigger the disconnection
		shift(function(){
			assert.isFalse(disconnectProbe, "disconnection did not occured");
		});
		
		shift(function(){
            swansSpawner.stop();
			assert.isTrue(disconnectProbe, "disconnection occured");
		});
		
		// connect event should no longer be triggered
		shift(function(){
			spawnCounter = 0;
            createConnection();
		});
		
		shift(function(){
			assert.equal(spawnCounter, 0, "no more connection have been established");
		});
		shift(done);
    });

    it("receive messages and transfer them through the hooks", function(done) {
		let lastMessageReceived = null;
		let lastMessageReceivedInterface = null;
		let spawnMessageReceived = null;
		let spawn = null;
		let message1 = new Uint8Array([1,2,3]);
		let message2 = new Uint8Array([4,5,6]);
		let message3 =  new Uint8Array([7,8,9]);
		
		var hookSpawn = function(event){
			spawn = event.spawn;
            spawn.addHook(externalSpawn.e.RECEIVE, function(e){
                spawnMessageReceived = e.message;
            });
		}
		var hookReceive = function(event){
			lastMessageReceived = event.message;
			lastMessageReceivedInterface = event.interface;
		}
        swansSpawner.addHook( swansSpawner.e.SPAWN, hookSpawn);
		
		// create a new connection and send a message on spawn
		let externalSpawnPromise = createConnection(message1);
		let externalSpawn = null;
		externalSpawnPromise.then(function(sp){
            externalSpawn = sp;
            externalSpawn.addHook(externalSpawn.e.RECEIVE, hookReceive);
        })

		shift(function(){
			assert.deepEqual(spawnMessageReceived, message1);
		});
		
		// send back a message
		shift(function(){
			spawn.send(message2);
		});
		shift(function(){
			assert.deepEqual(lastMessageReceived, message2, "the message should have been transferred from the client to the NestedSpawn");
		});

		shift(done);
    });

    it("should trigger the DISCONNECT event of a spawn after the answer is sent/received", function(done) {
		let disconnectProbe = false;
		let spawn = null;
		let message = new Uint8Array();
		
		var hookSpawn = function(event){
			spawn = event.spawn;
		}
		var hookDisconnect = function(){
			disconnectProbe = true;
		}
        swansSpawner.addHook( swansSpawner.e.SPAWN, hookSpawn);
		
		// create a new connection and terminate it
		let externalSpawn = createConnection(message);

		shift(function(){
			spawn.addHook(spawn.e.DISCONNECT, hookDisconnect);
			assert.isFalse(disconnectProbe, "the client has not disconnected yet");
		});
		
		shift(function(){
			spawn.send(message);
		});
		
		shift(function(){
			assert.isTrue(disconnectProbe, "the client should have disconected, and the event should have been triggered on spawn side");
			done();
		});
    });
});


