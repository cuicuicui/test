/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

/**
 * Tests for the webrtc nestedInterface
 * 
 * **/

// CONSTANTS FOR THE TESTING
const SERVER_PORT = 8080;
const ASYNC_TIMEOUT = 300; // time for asynchronous test to occur

///////////////////// testing modules /////////////////////
var assert;

try{
    assert = require("chai").assert;


// tested modules
    var flocklibs = require("../../../dist/flock-nodejs.js")
    Object.assign(global, flocklibs);




}
catch(e){
    console.log(e)
}

if(!assert) assert = chai.assert;

describe("creating a NIWebrtcSpawner", function() {
    it("should just create a new NIWebrtcSpawner without error", function() {
		let ni = new NIWebrtcSpawner();
		assert.ok(true);
    });
});

describe("testing helpers of NIWebrtcSpawner", function() {
	let nispawner = new NIWebrtcSpawner({});

    it("should generate a transaction id", function(){
		let id = nispawner.createTid();
		assert.match(id, /^[a-fA-F0-9]*$/, "the tid must be an hexadecimal string");
		assert.isNotEmpty(id);
	});

	it("should convert an signal object into bytes and back", function(){
		let offer = { type: "offer", sdp: "v=0\r\no=mozilla...THIS_IS_SDPARTA-61.0.1 4880384885820639740 0 IN IP4 0.0.0.0\r\ns=-\r\nt=0 0\r\na=fingerprint:sha-256 2A:B8:54:02:51:FD:83:AE:70:EB:1F:CD:38:A6:69:66:5A:65:B2:BF:51:2C:FD:EF:9C:B3:56:AE:68:0D:44:24\r\na=group:BUNDLE sdparta_0\r\na=ice-options:trickle\r\na=msid-semantic:WMS *\r\nm=application 9 DTLS/SCTP 5000\r\nc=IN IP4 0.0.0.0\r\na=sendrecv\r\na=ice-pwd:20f423080ff7d3a6d69c9eeab4fef5e9\r\na=ice-ufrag:b9e1e565\r\na=mid:sdparta_0\r\na=sctpmap:5000 webrtc-datachannel 256\r\na=setup:actpass\r\na=max-message-size:1073741823\r\n" };
		let iceCandidates = [
			{
				candidate: "candidate:0 1 UDP 2122252543 10.183.12.91 39577 typ host",
				sdpMLineIndex: 0,
				sdpMid: "sdparta_0",
			},
			{
				candidate: "candidate:0 1 UDP 2122252543 10.183.12.91 39578 typ host",
				sdpMLineIndex: 0,
				sdpMid: "sdparta_0",
			}
		]
		
		let data = {
			version : 1,
			tid : nispawner.createTid(),
			offer : offer, 
			iceCandidates : iceCandidates
		}
		
		let toBytes = nispawner.signalToBytes(data);
		assert.instanceOf(toBytes, Uint8Array);
		
		let toObject = nispawner.signalToObject(toBytes);
		
		assert.deepEqual( toObject.version, data.version, "asserting equality of parameter : version");
		assert.deepEqual( toObject.tid, data.tid, "asserting equality of parameter : tid");
		assert.deepEqual( toObject.offer, data.offer, "asserting equality of parameter : offer");
		assert.deepEqual( toObject.iceCandidates, data.iceCandidates, "asserting equality of parameter : iceCandidates");
	});
	
	
	it("should retrieve the list of canals", function(){
		let canals = nispawner.getCanals();
		let expectedCanal = {
			type : 3,
            canals : []
		};
		let expectedLength = 1;
		assert.isArray(canals);
		assert.lengthOf(canals, expectedLength);
		assert.deepEqual(canals[0], expectedCanal);
	});


	it("should create a peerconnection with preheated data inside it", function(done){

	    let preheatPromise = nispawner.preheatPeerConnection();

	    preheatPromise.then(function(transaction){
	        assert.isNotNull(transaction);
	        assert.isObject(transaction)
            assert.isNotNull(transaction.offer);
            assert.isNotNull(transaction.sendChannel);
            assert.isNotNull(transaction.RTCPeerConnection);
            assert.isArray(transaction.newIceCandidates)
            assert.isNotEmpty(transaction.newIceCandidates);
            done();
	    })


    })


});


describe("testing message sendings between two NIWebrtcSpawner linked by various signaling interface", function() {
    this.timeout(4000);

    // first webrtcspawner : initiate a connection by startin to message niw2
    let niw1 = null;
    let signalingPort1 = 516;

    // second webrtcspawner : receive signals from niw1
    let niw2 = null;
    let signalingPort2 = 516;

    // two spawners to access the signaler
    let spawner1 = null;
    let spawner2 = null;

    // two spawner to be accessed for signaling
    let signalingInterface1 = null;
    let signalingInterface2 = null;

    let shiftCounter = null;

    // helper to shift a function execution in time
    let shift = function(f){
        setTimeout(f, shiftCounter * ASYNC_TIMEOUT);
        shiftCounter++;
    }

    beforeEach(function(){
        shiftCounter = 1;
        return new Promise((resolve) =>  setTimeout(resolve, ASYNC_TIMEOUT)); // async to let server start enough time to occur
    });

    afterEach(function(){
        niw1.stop();
        niw2.stop();
        return new Promise((resolve) =>  setTimeout(resolve, ASYNC_TIMEOUT)); // async to let server start enough time to occur
    });

    function sendMessageTesting(done) {

        niw1 = new NIWebrtcSpawner({port : signalingPort1, signalingInterface : signalingInterface1, spawner : spawner1});
        niw2 = new NIWebrtcSpawner({port : signalingPort2, signalingInterface : signalingInterface2, spawner : spawner2});

        // add hooks to detect spawn from niw1 and niw2, and messages
        let spawn1 = null;
        let spawn2 = null;
        let disconnect1 = false;
        let disconnect2 = false;
        let hookSpawnCounter = 0;
        let hookReceiveCounter = 0;
        let hookSendCounter = 0;
        let message1 = new Uint8Array([1,2,3,4,5,6,7,8,9]);
        // test with a bigger message
        // let message2 = new Uint8Array([10,20,30,40,50,60,70,80,90]);
        let message2 = new Uint8Array(10000000);
        for(let i=0; i<message2.length; i++){
            message2[i] = i%256;
        }
        let message3 = new Uint8Array([11,21,31]);
        let messageReceived1 = null;
        let messageReceived2 = null;
        let hookSpawn1 = function(e){
            hookSpawnCounter++;
            spawn1 = e.spawn;
        }
        let hookSpawn2 = function(e){
            spawn2 = e.spawn;
        }
        let hookReceive1 = function(e){
            hookReceiveCounter++;
            messageReceived1 = e.message;
        }
        let hookSend1 = function(e){
            hookSendCounter++;
        }
        let hookReceive2 = function(e){
            messageReceived2 = e.message;
        }
        let hookDisconnect1 = function(e){ disconnect1=true;}
        let hookDisconnect2 = function(e){ disconnect2=true;}
        niw1.addHook( niw1.e.SPAWN, hookSpawn1);
        niw2.addHook( niw1.e.SPAWN, hookSpawn2);
        // launch a connection transaction from niw1
        shift(function(){
            let canals = signalingInterface2.getCanals();
            niw1.spawn(canals);
        });

        shift(function(){
            assert.isNotNull(spawn1);
            assert.isNotNull(spawn2);
            spawn1.addHook( spawn1.e.RECEIVE, hookReceive1, signalingPort1);
            spawn1.addHook( spawn1.e.SEND, hookSend1);
            spawn2.addHook( spawn2.e.RECEIVE, hookReceive2, signalingPort2);
            spawn1.send(message1, signalingPort2);
            spawn2.send(message2, signalingPort1);
        });

        shift(function(){
            // check how many times the hook have been triggered
            assert.equal(hookSpawnCounter,1, "couting number of spawn triggered");
            assert.equal(hookReceiveCounter,1, "counting the number of received triggered");
            assert.equal(hookSendCounter,1, "couting the number of send triggered");
            // check the message received
            assert.deepEqual( messageReceived1, message2);
            assert.deepEqual( messageReceived2, message1);
            // send another message
            messageReceived1 = null;
            messageReceived2 = null;
            spawn1.send(message1, signalingPort2);
            spawn2.send(message1, signalingPort1);
        });

        shift(function(){
            spawn1.send(message3, signalingPort2);
            spawn2.send(message3, signalingPort1);
        });

        // testing disconnection
        shift(function(){
            assert.deepEqual( messageReceived1, message3);
            assert.deepEqual( messageReceived2, message3);
            spawn1.addHook( spawn1.e.DISCONNECT, hookDisconnect1);
            spawn2.addHook( spawn2.e.DISCONNECT, hookDisconnect2);
            spawn1.stop();
            spawn2.stop();
            messageReceived1 = null;
            messageReceived2 = null;
            try{
                spawn1.send(message1, signalingPort2);
            }
            catch(e){}
            try{
                spawn2.send(message2, signalingPort1);
            }
            catch(e){}
        });

        shift(function(){
            assert.isTrue(disconnect1);
            assert.isTrue(disconnect2);
            assert.isNull(messageReceived1);
            assert.isNull(messageReceived2);

        });

        shift(done);
    }

    it("testing NIWebsocketSpawner spawns with mock server as signaling interface", function(done){
        signalingInterface1 = new NestedInterface();
        signalingInterface2 = new NestedInterface();

        // mock the two spawners : redirect messages to the opposite signalingInterface
        spawner1 = {
            canSpawn : ()=>true,
            spawn : ()=>{
                signalingInterface1.triggerSpawn({spawn: spawner1Spawn});
                signalingInterface2.triggerSpawn({spawn: spawner2Spawn});
                return Promise.resolve(spawner1Spawn);
            }
        }
        let spawner1Spawn = new NestedInterface();
        spawner1Spawn.send = (m,p,fp) => spawner2Spawn.triggerReceive({message:m, port:p, fport:fp, interface:spawner2Spawn})+console.log("message forwarded 1");

        spawner2 = {
            canSpawn : ()=>true,
            spawn : ()=> {
                signalingInterface1.triggerSpawn({spawn: spawner1Spawn});
                signalingInterface2.triggerSpawn({spawn: spawner2Spawn});
                return Promise.resolve(spawner2Spawn);
            }
        }
        let spawner2Spawn = new NestedInterface();
        spawner2Spawn.send = (m,p,fp) => spawner1Spawn.triggerReceive({message:m, port:p, fport:fp, interface:spawner1Spawn})+console.log("message forwarded 2");

        spawner1Spawn.test = "test1";
        spawner2Spawn.test = "test2";

        sendMessageTesting(done);
    });

    it("should remove old transaction through the cron", function(done){
        let req = {};
        retrievedReq = null;
        let ni = new NIWebrtcSpawner({
            transactionTimeout : 400,
            cronTime : 100
        });
        ni.addTransaction(req);
        retrievedReq = ni.getTransaction(req.tid);
        assert.deepEqual( retrievedReq, req);

        shift(function(){
            retrievedReq = ni.getTransaction(req.tid);
            assert.deepEqual( retrievedReq, req);
        });

        shift(function(){});
        shift(function(){});
        shift(function(){});
        shift(function(){
            retrievedReq = null
            retrievedReq = ni.getTransaction(req.tid);
            assert.isNull( retrievedReq);
            done();
        });

    });
});

