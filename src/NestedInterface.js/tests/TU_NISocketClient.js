/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

/**
 * Tests for the NIWebsocketSpawn
 * before starting the test, start the TU_NISocketClient_serverMock.js on server side
 * **/

// CONSTANTS FOR THE TESTING
const SERVER_PORT = 8080;
const SERVER_ADDRESS = 'ws://127.0.0.1';
const ASYNC_TIMEOUT = 100; // time for asynchronous test to occur

///////////////////// testing modules /////////////////////
try{
	var chai = require("chai");
	var chaiAsPromised = require("chai-as-promised");
	chai.use(chaiAsPromised);

// tested modules
    var flocklibs = require("../../../dist/flock-nodejs.js")
    Object.assign(global, flocklibs);
}
catch(e){ }
var assert = chai.assert;

describe("testing the NIWebsocketSpawn", function() {
	let socketIo = null;
	let clientList = [];
	let nispawner = null;
	let spawn = null;
	let shiftCounter = null;
	// helper to create client to contact the server
	let createConnection = function(){
		let destination = SERVER_ADDRESS+":"+SERVER_PORT;
		let socket = new NIWebsocketSpawn({serverAddress : destination});
		return socket
	}
	
	// helper to shift a function execution in time
	let shift = function(f){
		setTimeout(f, shiftCounter * ASYNC_TIMEOUT);
		shiftCounter++;
	}
		
	beforeEach(function(){
		shiftCounter = 1;
		// create a spawner that send back messages like an echo
		
		return new Promise((resolve) =>  setTimeout(resolve, ASYNC_TIMEOUT)); // async to let server start enough time to occur	
	});
	
	afterEach(function(){
		for(let clientSocket of clientList){
			clientSocket.close();
		}
		return new Promise((resolve) =>  setTimeout(resolve, ASYNC_TIMEOUT)); // async to let server start enough time to occur	
	});
		
    it("should send and receive messages from a NIWebsocketSpawn", function(done) {
		let connectionTriggered = false;
		
		let client = null;
		let message1 = new Uint8Array([1,2,4,8,16,32,64,128]);
		let message2 = new Uint8Array([2,3,5,9,17,33,65,129]);
		let messageReceived = null;
		let disconnectProbe = false;
		
		let hookReceive = function(event){
			messageReceived = event.message;
		}
		let hookDisconnect = function(){
			disconnectProbe = true;
		}
		
		// create a new connection : it should set our test variable to true
		assert.isFalse(connectionTriggered, "SPAWN connection is not yet established");
		client = createConnection();
		client.addHook( client.e.RECEIVE, hookReceive);
		client.addHook( client.e.DISCONNECT, hookDisconnect);
		
		// send a message from client to server side spawn : expect an echo
		shift(function(){
				client.send(message1);
		});
		// second try with another message
		shift(function(){
				assert.deepEqual(messageReceived, message1);
				messageReceived = null;
				client.send(message2);
		});
		// test disconnection
		shift(function(){
			assert.deepEqual(messageReceived, message2);
			client.stop();
			messageReceived = null;
			client.send(message1);
		});
		shift(function(){
			assert.isNull(messageReceived, "no message should be sent/received after disconnection");
			assert.isTrue(disconnectProbe, "the client must have been disconnected");
			done();
		});
    });
    
});
