/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

/**
 * Tests for the NestedInterface and AbstractNestedInterface
 * 
 * **/

// CONSTANTS FOR THE TESTING
const SERVER_PORT = 8080;
const ASYNC_TIMEOUT = 100; // time for asynchronous test to occur

///////////////////// testing modules /////////////////////
try{
	var chai = require("chai");
	var chaiAsPromised = require("chai-as-promised");
	chai.use(chaiAsPromised);

// tested modules
    var flocklibs = require("../../../dist/flock-nodejs.js")
    Object.assign(global, flocklibs);


}
catch(e){
	console.log("error ", e)
}

var assert = chai.assert;

describe("creating a NIWebsocketSpawner", function() {
    it("should just create a new NIWebsocketSpawner without error", function() {
		let ni = new NIWebsocketSpawner();
		assert.ok(true);
		ni.stop();
    });
});

describe("testing the getCanals method", function(){
	it("should retrieve the list of canals, without the address", function(){
		let data = {
			socketPort: null,
			address : null
		};
		let ni = new NIWebsocketSpawner(data);
		let canals = ni.getCanals();
		ni.stop();
		let expectedCanal = {
			type : 2,
			address : null
		};
		let expectedLength = 1;
		assert.isArray(canals);
		assert.lengthOf(canals, expectedLength);
		assert.deepEqual(canals[0], expectedCanal);
	});
	
	it("should retrieve the list of canals, with the address and port", function(){
		let data = {
			socketPort: 12345,
			address : "www.example.com"
		};
		let ni = new NIWebsocketSpawner(data);
		let canals = ni.getCanals();
		ni.stop();
		let expectedCanal = {
			type : 2,
			address : "www.example.com"
		};
		let expectedLength = 1;
		assert.isArray(canals);
		assert.lengthOf(canals, expectedLength);
		assert.deepEqual(canals[0], expectedCanal);
	});

	it("should retrieve an empty canals list if the ni has no websocket server", function(){
        let data = {
            socketPort: 12345,
            address : "www.example.com",
			withServer : false
        };
        let ni = new NIWebsocketSpawner(data);
        let canals = ni.getCanals();
        ni.stop();
        assert.isArray(canals);
        assert.lengthOf(canals, 0);
	})
});

describe("testing the implementation of the abstract NestedInterface methods in the NIWebsocketSpawner", function() {
	let socketIoPort = null;
	let socketIo = null;
	let clientList = [];
	let nispawner = null;
	let shiftCounter = null;
	
	// helper to create client to contact the server
	let createConnection = function(){
		let destination = 'http://127.0.0.1:'+SERVER_PORT;
		var socket = new NIWebsocketSpawn({
			serverAddress : destination});
        clientList.push(socket);
		return socket;
	}
	
	// helper to shift a function execution in time
	let shift = function(f){
		setTimeout(f, shiftCounter * ASYNC_TIMEOUT);
		shiftCounter++;
	}
	
	beforeEach(function(){
		shiftCounter = 1;
		nispawner = new NIWebsocketSpawner({
			address : "127.0.0.1",
			socketPort:SERVER_PORT});
		return new Promise((resolve) =>  setTimeout(resolve, ASYNC_TIMEOUT)); // async to let server start enough time to occur	
	});
	
	afterEach(function(){
		nispawner.stop();
		for(let clientSocket of clientList){
			clientSocket.stop();
		}
        clientList = [];
		return new Promise((resolve) =>  setTimeout(resolve, ASYNC_TIMEOUT)); // async to let server start enough time to occur	
	});
	
    it("creating a NIWebsocketSpawner, then stop it", function() {
			assert.ok(nispawner.status(), "RUNNING");
			nispawner.stop();
			assert.ok(nispawner.status(), "STOPPED");
			assert.notOk( nispawner._core.connected, "the socket should be deconnected");
    });
		
    it("creating a NestedServer listening to new connections", function(done) {
		let connectionTriggered = false;
		var hook = function(){
			connectionTriggered = true;
		}
		nispawner.addHook( nispawner.e.SPAWN, hook);
		
		// create a new connection : it should set our test variable to true
		assert.isFalse(connectionTriggered, "SPAWN connection is not yet established");
		let socketClient = createConnection();
		shift(function(){
			assert.isTrue(connectionTriggered, "SPAWN connection is established");
			done();
		});
    });
    
    it("should close the server and trigger the associated hook", function(done) {
		let disconnectionTriggered = false;
		var hook = function(){
			disconnectionTriggered = true;
		}
		nispawner.addHook( nispawner.e.DISCONNECT, hook);
		
		// create a new connection : it should set our test variable to true
		assert.isFalse(disconnectionTriggered, "disconnection did not occured yet");
		nispawner.stop();
		shift(function(){
			assert.isTrue(disconnectionTriggered, "disconnection occured");
			done();
		});
		
    });
    
    it("should create a spawn when establishing a connection", function(done) {
		let spawn = null;
		var hook = function(event){
			spawn = event.spawn;
		}
		nispawner.addHook( nispawner.e.SPAWN, hook);
		
		// create a new connection : it should set our test variable to true
		assert.isNull(spawn, "connection is not yet established");
		let socketClient = createConnection();
		shift(function(){
			assert.isNotNull(spawn, "connection is established");
			assert.isTrue(spawn instanceof NIWebsocketSpawn, "the spawn has the right type");
			done();
		});
		
    });
    
    it("should trigger the disconnect event of the server and stop receiving new connections", function(done) {
		
		let spawnCounter = 0;
		let disconnectProbe = false;
		var hookConnect = function(spawnReceived){
			spawnCounter++;
		}
		let hookDisconnect = function(){
			disconnectProbe = true;
		}
		nispawner.addHook( nispawner.e.SPAWN, hookConnect);
		nispawner.addHook( nispawner.e.DISCONNECT, hookDisconnect);
		
		// create a new connection : it should set our test variable to true
		assert.equal(spawnCounter, 0, "connection is not yet established");
		let socketClient = createConnection();
		shift(function(){
			assert.equal(spawnCounter, 1, "one connection has been established");
		});
		
		// trigger the disconnection
		shift(function(){
			assert.isFalse(disconnectProbe, "disconnection did not occured");
		});
		
		shift(function(){
			nispawner.stop();
			assert.isTrue(disconnectProbe, "disconnection occured");
		});
		
		// connect event should no longer be triggered
		shift(function(){
			spawnCounter = 0;
			try{
				createConnection();
			}
			catch(e){}
		});

		shift(function(){
			assert.equal(spawnCounter, 0, "no more connection have been established");
			done();
		});
    });

    it("receive messages and transfer them through the hooks", function(done) {
		let lastMessageReceived = null;
		let lastMessageReceivedInterface = null;
		let spawn = null;
		let message1 = new Uint8Array([1,2]);
		let message2 = new Uint8Array([10,20]);
		let clientMessageReceived = null;
		let message3 =  new Uint8Array([100,200]);
			
		var hookSpawn = function(event){
			spawn = event.spawn;
		}
		var hookReceive = function(event){
			lastMessageReceived = event.message;
			lastMessageReceivedInterface = event.interface;
		}
		nispawner.addHook( nispawner.e.SPAWN, hookSpawn);
		
		// create a new connection and send messages from client to spawn
		let socketClient = createConnection();
		shift(function(){
			spawn.addHook(nispawner.e.RECEIVE, hookReceive);
		});
		
		// send one message
		shift(function(){
			socketClient.send(message1);
		});
		shift(function(){
			assert.deepEqual(lastMessageReceived, message1, "the message should have been transferred from the client to the NestedSpawn");
			assert.instanceOf(lastMessageReceivedInterface, NIWebsocketSpawn);
		});
		
		// send a second message
		shift(function(){
			socketClient.send(message2);
		});
		shift(function(){
			assert.deepEqual(lastMessageReceived, message2, "the message should have been transferred from the client to the NestedSpawn");
		});
		
		// try to send a message from the spawn to the client
		
		shift(function(){
			socketClient.addHook(nispawner.e.RECEIVE, function (e) {
				clientMessageReceived = e.message;
			});
			spawn.send(message3);
		});
		
		shift(function(){
			assert.deepEqual(clientMessageReceived, message3, "the client must have received a message from the spawn");
			done();
		});
    });
    
    it("should trigger the DISCONNECT event of a spawn when the client disconnect", function(done) {
		let disconnectProbe = false;
		let spawn = null;
		
		var hookSpawn = function(event){
			spawn = event.spawn;
		}
		var hookDisconnect = function(){
			disconnectProbe = true;
		}
		nispawner.addHook( nispawner.e.SPAWN, hookSpawn);
		
		// create a new connection and terminate it
		let socketClient = createConnection();
		shift(function(){
			spawn.addHook(nispawner.e.DISCONNECT, hookDisconnect);
			assert.isFalse(disconnectProbe, "the client has not disconnected yet");
		});
		
		shift(function(){
			spawn.stop();
		});
		
		shift(function(){
			assert.isTrue(disconnectProbe, "the client should have disconected, and the event should have been triggered on spawn side");
			done();
		});
		
    });

    it("should trigger the DISCONNECT event of a spawn when the spawn disconnect", function(done) {
		let disconnectProbe = false;
		let spawn = null;
		let lastMessageReceived = null;
		let message1 = new Uint8Array([2,4,8,12,14,16,20,24,28,32,222]);
		let message2 = new Uint8Array([1,3,5,11,17,21,25,27,29,33,111]);
		
		let hookReceive = function(event){
			lastMessageReceived = event.message;
		}
		var hookSpawn = function(event){
			spawn = event.spawn;
		}
		var hookDisconnect = function(){
			disconnectProbe = true;
		}
		nispawner.addHook( nispawner.e.SPAWN, hookSpawn);
		
		// create a new connection and send messages from client to spawn
		let socketClient = createConnection();
		shift(function(){
			spawn.addHook(nispawner.e.RECEIVE, hookReceive);
		});
		
		shift(function(){
			socketClient.send(message1);
		});
		
		shift(function(){
			assert.deepEqual(lastMessageReceived, message1, "the message should have been transferred from the client to the NestedSpawn");
		});
		
		// trigger the disconnection
		shift(function(){
			spawn.addHook(nispawner.e.DISCONNECT, hookDisconnect);
			assert.isFalse(disconnectProbe, "the client has not disconnected yet");
			spawn.stop();
		});
		shift(function(){
			assert.isTrue(disconnectProbe, "the spawn should have disconected, and the event should have been triggered on spawn side");
		});
		
		// messages should no longer be transfered
		shift(function(){
			socketClient.send(message2);
		});
		shift(function(){
			assert.notDeepEqual(lastMessageReceived, message2, "the message should not have been transferred from the client to the NestedSpawn");
			done();
		});
    });
});

describe("testing the canSpawn method", function(){
	let nispawner;
	beforeEach(function(){
		nispawner = new NIWebsocketSpawner({socketPort:8080});
	});
	
	afterEach(function(){
		nispawner.stop();
	});
	
	it("should succeed with correct data", function(){
		let canal = {
			type : 2,
			address : "www.test.com"
		};
		let result = nispawner.canSpawn(canal);
		let expected = true;
		assert.equal(result, expected);
	});
	
	it("should fail when using an incorrect type", function(){
		let canal = {
			type : 123,
			address : "www.test.com"
		};
		let result = nispawner.canSpawn(canal);
		let expected = false;
		assert.equal(result, expected);
	});
	
	it("should fail without address", function(){
		let canal = {
			type : 2
		};
		let result = nispawner.canSpawn(canal);
		let expected = false;
		assert.equal(result, expected);
	});
});


describe("testing the spawn method", function(){
	
	let nispawner1, nispawner2;
	
	beforeEach(function(){
		nispawner1 = new NIWebsocketSpawner({address : "127.0.0.1", socketPort:8080});
		nispawner2 = new NIWebsocketSpawner({address : "127.0.0.1", socketPort:8081});
	});
	
	afterEach(function(){
		nispawner1.stop();
		nispawner2.stop();
	});
	
	it("should return a rejected promise when providing a wrong canal", function(done){
		let prom = nispawner1.spawn({});
		prom.then(
		function(){
			assert.notOk(true, "shoud not resolve successfully");
			done();
		},
		function(){
			assert.ok(true, "shoud fail");
			done();
		});
			
	});
	
	it("should create a spawn that are ready when the promise resolve", function(done){
		let prom = nispawner1.spawn({type : 2, address:"ws://127.0.0.1:8081"});
		let eventReceived = null;
		nispawner2.addHook(nispawner2.e.SPAWN, function(e){eventReceived=e});
		
		prom.then(
		function(){
			assert.ok(true, "should resolve successfully");
			assert.isNotNull(eventReceived, "a spawn event should have been received from spawner 2");
			done();
		},
		function(e){
			assert.notOk(true, "shoud not fail : "+e);
			done();
		});
	});
	
	it("should return a rejected promise when the connection fail", function(done){
		let prom = nispawner1.spawn({type : 2, address:"ws://127.0.0.1:8089"});
		prom.then(
		function(){
			assert.notOk(true, "shoud not resolve successfully");
			done();
		},
		function(){
			assert.ok(true, "shoud fail");
			done();
		});
			
	});
	
});

