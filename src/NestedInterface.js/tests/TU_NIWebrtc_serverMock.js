/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

// create a swan server on localhost to test webrtc newted interfaces


// require dependencies

// tested modules
var flocklibs = require("../../../dist/flock-nodejs.js")
Object.assign(global, flocklibs);



// retrieve the server data

// build the server
var serverData = {
    serverAddress : "http://127.0.0.1",
    serverRoute : "/swans",
    socketPort : 11111,
    httpPort : 80
};

let socketReceiverInterface = new NIWebsocketSpawner({socketPort: serverData.socketPort});
let httpClientInterface = new NIHttpSpawner({ serverPort : serverData.httpPort, serverRoute : serverData.serverRoute, address : serverData.serverAddress });

server = new SwanServer({
    receiverInterface : socketReceiverInterface,
    clientInterface : httpClientInterface,
    registerTimeout : 10000,
    receiverTimeout : 10000,
    routinePeriod : 10000,
    url : serverData.serverRoute,
});