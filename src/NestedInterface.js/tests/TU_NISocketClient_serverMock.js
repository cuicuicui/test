/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

// create a NISocketSpawner before launching the TU_NISocketClient.js
const SERVER_PORT = 8080;
const SERVER_ADDRESS = 'ws://127.0.0.1';

// tested modules

// tested modules
var flocklibs = require("../../../dist/flock-nodejs.js")
Object.assign(global, flocklibs);


nispawner = new NIWebsocketSpawner({
	socketPort:SERVER_PORT,
    address : SERVER_ADDRESS
});
var hookSpawn = function(event){
	spawn = event.spawn;
	spawn.addHook( spawn.e.RECEIVE, hookReceive);
}
let hookReceive = function(event){
	event.interface.send(event.message);
}
nispawner.addHook( nispawner.e.SPAWN, hookSpawn);

spawn = null;

// prevent the script from closing
setInterval(function(){
	console.log(nispawner.status() )
}, 1000)