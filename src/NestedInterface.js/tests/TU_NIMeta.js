/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

/**
 * Tests for the NestedInterface and AbstractNestedInterface
 * 
 * **/

// CONSTANTS FOR THE TESTING
const SERVER_PORT = 8080;
const SERVER_ADDRESS =  'ws://127.0.0.1';

///////////////////// testing modules /////////////////////
let chai = require("chai");
let chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
let assert = chai.assert;

// tested modules
var flocklibs = require("../../../dist/flock-nodejs.js")
Object.assign(global, flocklibs);

const ASYNC_TIMEOUT = 100; // time for asynchronous test to occur

describe("creating a NIMeta", function() {
    it("should just create a new NIMeta without error", function() {
		let ni = new NIMeta();
		assert.ok(true);
    });
});

describe("testing the getCanals method", function(){
	
	it("should return an array every canal spawn", function(){
		let nimeta = new NIMeta();
		let nisocket1 = new NIWebsocketSpawner({socketPort : 44444, address : "www.test1.com:3333"});
		let nisocket2 = new NIWebsocketSpawner({socketPort : 55555, address : "www.test2.com:3333"});
		nimeta.addSpawner(nisocket1);
		nimeta.addSpawner(nisocket2);
		let expectedCanal1 = {
			type : 2,
			address : "www.test1.com:3333"
		};
		let expectedCanal2 = {
			type : 2,
			address : "www.test2.com:3333"
		};
		let expectedLength = 2;
		let canals = nimeta.getCanals();
		nimeta.stop();
		assert.isArray(canals);
		assert.lengthOf(canals, expectedLength);
		assert.deepEqual(canals[0], expectedCanal1);
		assert.deepEqual(canals[1], expectedCanal2);
	});
});

describe("testing NIMeta with artificial socketio spawns", function() { 
	let socketIoPort = null;
	let socketIo = null;
	let clientList = [];
	let nispawner = null; 
	let metaspawner = null;
	let shiftCounter = null;
	
	// helper to create client to contact the server
	let createConnection = function(){
		let destination = 'ws://127.0.0.1:'+SERVER_PORT;
		var socket = socketIoClientLib(destination);
		clientList.push(socket);
		return socket;
	}
	
	let createIoSpawn = function(){ 
		let client =  new NIWebsocketSpawn({serverAddress : 'ws://127.0.0.1:'+SERVER_PORT});
		clientList.push(client);
		return client;
	}
	
	// helper to shift a function execution in time
	let shift = function(f){
		setTimeout(f, shiftCounter * ASYNC_TIMEOUT);
		shiftCounter++;
	}
	
	beforeEach(function(){
		shiftCounter = 1;
		metaspawner = new NIMeta();
		nispawner = new NIWebsocketSpawner({socketPort : SERVER_PORT, address : SERVER_ADDRESS});
		return new Promise((resolve) =>  setTimeout(resolve, ASYNC_TIMEOUT)); // async to let server start enough time to occur	
	});
	
	afterEach(function(){
		nispawner.stop();
		metaspawner.stop();
		for(let clientSocket of clientList){
			clientSocket.stop();
		}
		clientList.length = 0;
		return new Promise((resolve) =>  setTimeout(resolve, ASYNC_TIMEOUT)); // async to let server start enough time to occur	
	});

    it("creating a NIMeta, then stop it", function() {
		assert.ok(metaspawner.status(), "RUNNING");
		metaspawner.stop();
		assert.ok(metaspawner.status(), "STOPPED");
    });
    
	it("should return canSpawn output of the spawners", function() {
		let canal = {
			type : 2,
			address : "www.test.com"
		};
		let canSpawn1 = metaspawner.canSpawn(canal);
		assert.notOk(canSpawn1, "should be false since the spawner has not been added yet");
		
		metaspawner.addSpawner(nispawner);
		let canSpawn2 = metaspawner.canSpawn(canal);
		assert.ok(canSpawn2, "should be true since the spawner has been added");
	});
	
    it("should create a spawn from a websocket spawner", function(done) {
		metaspawner.addSpawner(nispawner);
		let prom = metaspawner.spawn({type : 2, address:'ws://127.0.0.1:'+SERVER_PORT});
		let eventReceived = null;
		metaspawner.addHook(nispawner.e.SPAWN, function(e){eventReceived=e});
		
		prom.then(
		function(){
			assert.ok(true, "should resolve successfully");
			assert.isNotNull(eventReceived, "a spawn event should have been received");
			done();
		},
		function(){
			assert.notOk(true, "shoud not fail");
			done();
		});
	});
		
    it("creating a NIMeta with new artificial spawns", function() {
		let connectionTriggered = false;
		var hook = function(){
			connectionTriggered = true;
		}
		metaspawner.addHook( metaspawner.e.SPAWN, hook);
		
		// add a new spawn : it should set our test variable to true
		assert.isFalse(connectionTriggered, "SPAWN connection is not yet established");
		
		let iospawn = createIoSpawn();
		metaspawner.addSpawn(iospawn, {}, false);
		assert.isFalse(connectionTriggered, "SPAWN connection is not triggered when addSpawn has been called with trigger=false");
		
		let iospawn2 = createIoSpawn();
		metaspawner.addSpawn(iospawn2, {}, true);
		assert.isTrue(connectionTriggered, "SPAWN connection is triggered when addSpawn has been called with trigger=true");
    });
    
    it("should call the metaspawner hooks when triggering the metaspawn event stop", function() {
		let disconnectionTriggered = false;
		var hook = ()=>disconnectionTriggered = true;
		metaspawner.addHook( nispawner.e.DISCONNECT, hook);
		
		// create a new connection : it should set our test variable to true
		assert.isFalse(disconnectionTriggered, "disconnection did not occured yet");
		
		let iospawn = createIoSpawn();
		metaspawner.addSpawn(iospawn, {}, false);
		
		metaspawner.stop();
		assert.isTrue(disconnectionTriggered, "disconnection occured");
    });
    
    it("should call the metaspawner hooks when triggering the spawn event stop", function() {
		let disconnectionTriggered = false;
		var hook = ()=>disconnectionTriggered = true;
		metaspawner.addHook( metaspawner.e.DISCONNECT, hook);
		
		// create a new connection : it should set our test variable to true
		assert.isFalse(disconnectionTriggered, "disconnection did not occured yet");
		
		let iospawn = createIoSpawn();
		metaspawner.addSpawn(iospawn, {}, false);
		
		iospawn.stop();
		assert.isTrue(disconnectionTriggered, "disconnection occured");
    });
    
    it("should call spawn event a a spawner associated to the meta create a spawn ", function(done) {
		let spawn = null;
		metaspawner.addSpawner(nispawner);
		var hook = function(event){
			spawn = event.spawn;
		}
		metaspawner.addHook( metaspawner.e.SPAWN, hook);
		
		// create a new connection : it should set our test variable to true
		assert.isNull(spawn, "connection is not yet established");
		let socketClient = createIoSpawn();
		shift(function(){
			assert.isNotNull(spawn, "connection is established");
			assert.isTrue(spawn instanceof NIWebsocketSpawn, "the spawn has the right type");
			done();
		});
    });
    
    it("should trigger the disconnect event of the server and stop receiving new connections", function(done) {
		let spawnCounter = 0;
		let disconnectProbe = false;
		var hookConnect = function(event){
			spawnCounter++;
		}
		let hookDisconnect = function(){
			disconnectProbe = true;
		}
		metaspawner.addHook( nispawner.e.SPAWN, hookConnect);
		metaspawner.addHook( nispawner.e.DISCONNECT, hookDisconnect);
		metaspawner.addSpawner(nispawner);
		
		// create a new connection : it should set our test variable to true
		assert.equal(spawnCounter, 0, "connection is not yet established");
		let socketClient = createIoSpawn();
		shift(function(){
			assert.equal(spawnCounter, 1, "one connection has been established");
		});
		
		// trigger the disconnection
		shift(function(){
			assert.isFalse(disconnectProbe, "disconnection did not occured");
		});
		
		shift(function(){
			metaspawner.stop();
			assert.isTrue(disconnectProbe, "disconnection occured");
		});
		
		// connect event should no longer be triggered
		shift(function(){
			spawnCounter = 0;
			createIoSpawn();
		});
		
		shift(function(){
			assert.equal(spawnCounter, 0, "no more connection have been established");
			done();
		});
    });
	
    it("receive messages and transfer them through the hooks", function(done) {
		let lastMessageReceived = null;
		let spawn = null;
		let message1 = new Uint8Array([2,4,6]);
		let message2 = new Uint8Array([8,10,12]);
		let clientMessageReceived = null;
		let message3 = new Uint8Array([12,14,16]);
			
		var hookSpawn = function(event){
			spawn = event.spawn;
		}
		var hookReceive = function(event){
			lastMessageReceived = event.message;
		}
		metaspawner.addHook( nispawner.e.SPAWN, hookSpawn);
		metaspawner.addHook(metaspawner.e.RECEIVE, hookReceive);
		metaspawner.addSpawner(nispawner);
		
		// create a new connection and send messages from client to spawn
		let socketClient = createIoSpawn();

		// send one message
		shift(function(){
			socketClient.send(message1);
		});
		shift(function(){
			assert.deepEqual(lastMessageReceived, message1, "the message should have been transferred from the client to the NestedSpawn");
			// send a second message
			socketClient.send(message2);
		});
		
		shift(function(){
			assert.deepEqual(lastMessageReceived, message2, "the message should have been transferred from the client to the NestedSpawn");
		});
		
		// try to send a message from the spawn to the client
		
		shift(function(){
			socketClient.addHook(socketClient.e.RECEIVE, function (e) {
				clientMessageReceived = e.message;
			});
			spawn.send(message3);
		});
		
		shift(function(){
			assert.deepEqual(clientMessageReceived, message3, "the client must have received a message from the spawn");
			done();
		});
		
    });
    
    it("should trigger the DISCONNECT event of a spawn when the client disconnect", function(done) {
		let disconnectProbe = false;
		let spawn = null;
		
		var hookSpawn = function(event){
			spawn = event.spawn;
		}
		var hookDisconnect = function(){
			disconnectProbe = true;
		}
		metaspawner.addSpawner(nispawner);
		metaspawner.addHook(nispawner.e.DISCONNECT, hookDisconnect);
		metaspawner.addHook( nispawner.e.SPAWN, hookSpawn);
		
		// create a new connection and terminate it
		let socketClient = createIoSpawn();
		shift(function(){
			assert.isFalse(disconnectProbe, "the client has not disconnected yet");
		});
		
		shift(function(){
			socketClient.stop();
		});
		
		shift(function(){
			assert.isTrue(disconnectProbe, "the client should have disconected, and the event should have been triggered on spawn side");
			done();
		});
    });
    
});
