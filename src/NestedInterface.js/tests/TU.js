/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

/**
 * Tests for the NestedInterface and AbstractNestedInterface
 * 
 * **/

try{
	var chai = require("chai");

    // tested modules
    var flocklibs = require("../../../dist/flock-nodejs.js")
    Object.assign(global, flocklibs);
}
catch(e){
	console.log(e)
}

var assert = chai.assert;

describe("creating a NestedInterface", function() {
    it("should just create a new NestedInterface without error", function() {
		try{
			
			assert.ok(true);
		}
		catch(e){
			assert.ok(false,"it should not throw an exception : "+e);
		}
    });
});

describe("testing other helpers", function(){
	let ni;
	
	beforeEach(function(){
		ni = new NestedInterface();
	});
	
	it("should retrieve the list of canals", function(){
		let canals = ni.getCanals();
		let expectedLength = 0;
		assert.isArray(canals);
		assert.lengthOf(canals, expectedLength);
	});
	
	
});

describe("testing packet conversion from object to bytes and back", function(){
	let ni = new NestedInterface();
	
	it("should convert a well formed packet", function(){
		let packet = {
				port : 1234,
				fport : 56478,
				message : new Uint8Array([2,4,8,16,32,64,255])
		};
		let bytesConversion = ni.packetToBytes(packet);
		assert.instanceOf( bytesConversion, Uint8Array);
		let objectConversion = ni.bytesToPacket(bytesConversion);
		assert.deepEqual( objectConversion, packet);
	});
	
	it("should convert a packet with missing pots", function(){
		let packet = {
				message : new Uint8Array([2,4,8,16,32,64,255])
		};
		
		let expected = {
				port : 0,
				fport : 0,
				message : new Uint8Array([2,4,8,16,32,64,255])
		};
		
		let bytesConversion = ni.packetToBytes(packet);
		assert.instanceOf( bytesConversion, Uint8Array);
		let objectConversion = ni.bytesToPacket(bytesConversion);
		assert.deepEqual( objectConversion, expected);
	});
	
	it("should convert a packet with missing message", function(){
		let packet = {
				port : 1234,
				fport : 56478,
		};
		
		let expected = {
				port : 1234,
				fport : 56478,
				message : new Uint8Array(0)
		};
		
		let bytesConversion = ni.packetToBytes(packet);
		assert.instanceOf( bytesConversion, Uint8Array);
		let objectConversion = ni.bytesToPacket(bytesConversion);
		assert.deepEqual( objectConversion, expected);
	});
	
});

describe("testing management of hooks", function() {
	// nested interface where to add interface
	let ni = null;
	let hookName = null;
	let wrongHookName = "jhsdfoudgfuisdfyi";
	
	beforeEach(function(){
		// create a new nestedInterface for each test
		ni = new NestedInterface();
		hookName = ni.e.RECEIVE;
	});
	
    it("should add several valid hooks", function() {
		var hook1 = function(){};
		var hook2 = function(){};
				
		ni.addHook(hookName, hook1);
		ni.addHook(hookName, hook2);
		let hookDataList = ni.getHooks(hookName);
		assert.lengthOf(hookDataList, 2, "the hook list must contains the 2 methods included");
		let hookList = hookDataList.map(a=>a.method);
		assert.include(hookList, hook1, "the hook1 must be present in the list");
		assert.include(hookList, hook2, "the hook2 must be present in the list");
    });

    it("should check if the interface has some hooks registered for a given port", function(){
        var hook1 = function(){};
        var port1 = 123;
        var event1 = ni.e.RECEIVE

        var hook2 = function(){};
		var event2 = ni.e.SEND;

		var wrongPort = 789;

		assert.notOk( ni.hasHook(event1) , "no hook is added at first");
		ni.addHook( event1, hook1, port1);
		assert.ok( ni.hasHook(event1, port1), "the hook has been added on the specified port")
        assert.ok( ni.hasHook(event1), "the hook should be detected without providing a specific port")
        assert.notOk( ni.hasHook(event1, wrongPort), "the hook should not be detected with a wrong port")

		ni.addHook(event2, hook2);
        assert.ok( ni.hasHook(event2), "the hook should be detected without a specified port" )
        assert.notOk( ni.hasHook(event2, wrongPort), "the hook should not be detected with a specified port" )
	})

    it("should remove a hook upon request", function() {
		var hook1 = function(){};
		var hook2 = function(){};
		var hook3 = function(){};
		let hookList = null;
		let hookDataList = null;
		
		ni.addHook(hookName, hook1);
		ni.addHook(hookName, hook2);
		ni.addHook(hookName, hook3);
		hookDataList = ni.getHooks(hookName);
		assert.lengthOf(hookDataList, 3, "the hook list must contains the 3 methods included");
		
		ni.removeHooks(hookName, hook2);
		hookDataList = ni.getHooks(hookName);
		hookList = hookDataList.map(a=>a.method);
		assert.lengthOf(hookList, 2, "the hook list must contains the 3 methods included");
		assert.include(hookList, hook1, "the hook1 must be present in the list");
		assert.notInclude(hookList, hook2, "the hook2 must be removed from the list");
		assert.include(hookList, hook3, "the hook3 must be present in the list");
		
		// try removing all hooks
		ni.removeHooks(hookName,);
		assert.lengthOf(ni.getHooks(hookName), 0, "the hook list must contains the 3 methods included");
    });
    
    it("should not remove remove a hook if the hookName is incorrect", function() {
		var hook1 = function(){};
		var hookDataList = null;
		
		ni.addHook(hookName, hook1);
		hookDataList = ni.getHooks(hookName);
		assert.lengthOf(hookDataList, 1);
		
		// try removing the method specifically
		ni.removeHooks(wrongHookName, hook1);
		hookDataList = ni.getHooks(hookName);
		assert.lengthOf(hookDataList, 1, "no hook should have been removed");
		
		// try removing all hooks
		ni.removeHooks(wrongHookName);
		hookDataList = ni.getHooks(hookName);
		assert.lengthOf(hookDataList, 1, "no hook sghould have been removed");
		
	});
	
	it("should trigger registered hooks", function(){
		let isTriggered1 = false;
		let isTriggered2 = false;
		var hook1 = function(){isTriggered1=true};
		var hook2 = function(){isTriggered2=true};
		
		ni.addHook(hookName, hook1);
		ni.addHook(hookName, hook2);
		
		ni.trigger({ name : hookName});
		assert.isTrue(isTriggered1, "first hook must have been called");
		assert.isTrue(isTriggered2, "second hook must have been called");
	});
	
	it("should only trigger registered hooks with the right port", function(){
		let isTriggered1 = false;
		let isTriggered2 = false;
		let goodPort = 155;
		let wrongPort = 156;
		var hook1 = function(){isTriggered1=true};
		var hook2 = function(){isTriggered2=true};
		
		ni.addHook(hookName, hook1, goodPort);
		ni.addHook(hookName, hook2, wrongPort);
		
		ni.trigger({ name : hookName, port : goodPort} );
		assert.isTrue(isTriggered1, "first hook must have been called");
		assert.isFalse(isTriggered2, "second hook must not have been called");
	});


	it("should throw an exception when trying to trigger a wrong hookName", function(){
		try{
			ni.trigger(wrongHookName);
			assert.isTrue(false, "an exception should have been raised instead");
		}
		catch(e){
			assert.isTrue(true, "exception has been raised successfully");
		}
	});
    
});
