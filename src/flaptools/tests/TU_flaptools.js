/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

/**
 * Tests for the flaptools lib
 * **/

///////////////////// testing modules /////////////////////
let chai = require("chai");
let assert = chai.assert;

// tested modules
var flocklibs = require("../../../dist/flock-nodejs.js")
Object.assign(global, flocklibs);


describe("creating instance without exception", function(){
	it("should create a flaptool without throwing an exception", function(){
		let flt = new Flaptools();
		assert.isOk(true);
	});
});

/////////////////////////////////////////////////////////////////////////////
/////////////////////    KEY GENERATION TESTING         /////////////////////
/////////////////////////////////////////////////////////////////////////////

describe("testing key generation helpers", function(){
    let flt = new Flaptools();

    it("should create a random bytes array", function(){
    	let bid = flt.randomBytes();
    	assert.instanceOf(bid, Uint8Array);
    	assert.isAbove(bid.length, 0);
    	assert.notDeepEqual(bid, new Uint8Array(bid.length), "the random bytes must not be null");
	})

	it("should create a random bytes array of the specified length", function(){
		let expectedLength = 1234;
        let bid = flt.randomBytes(expectedLength);
        assert.instanceOf(bid, Uint8Array);
        assert.lengthOf(bid, expectedLength);
        assert.notDeepEqual(bid, new Uint8Array(expectedLength), "the random bytes must not be null");
	})

	it("should create a random hexadecimal", function(){
		let hid = flt.randomHex();
		assert.isString(hid);
		assert.isNotEmpty(hid);
		assert.match(hid, /^[a-fA-F0-9]*$/, "the hexadecimal id must only contains hexadecimal characters");
	})

	it("should create a random hexadecimal of the specified length", function(){
        let expectedLength = 1234;
        let hid = flt.randomHex(expectedLength);
        assert.isString(hid);
        assert.isNotEmpty(hid);
        assert.match(hid, /^[a-fA-F0-9]*$/, "the hexadecimal id must only contains hexadecimal characters");
	})

})

/////////////////////////////////////////////////////////////////////////////
///////////////////// BYTES TO VALUE CONVERSION HELPERS /////////////////////
/////////////////////////////////////////////////////////////////////////////

describe("testing bytes to hexadecimal conversion", function(){
	let flt = new Flaptools();
	
	it("should convert from bytes to hexadecimal", function(){
		let result, input, expected;
		
		input = new Uint8Array([]);
		expected = "";
		result = flt.bytesToHex(input);
		assert.deepEqual(result, expected);
		
		input = new Uint8Array([3]);
		expected = "03";
		result = flt.bytesToHex(input);
		assert.deepEqual(result, expected);
		
		input = new Uint8Array([0,4,5,255]);
		expected = "000405ff";
		result = flt.bytesToHex(input);
		assert.equal(result, expected);
		
	});
	
	it("should convert hexadecimal to bytes", function(){
		let result, input, expected;
		
		expected = new Uint8Array([]);
		input = "";
		result = flt.hexToBytes(input);
		assert.deepEqual(result, expected);
		assert.instanceOf(result, Uint8Array);
		
		expected = new Uint8Array([3]);
		input = "3";
		result = flt.hexToBytes(input);
		assert.deepEqual(result, expected);
		assert.instanceOf(result, Uint8Array);
		
		expected = new Uint8Array([0,4,5,255]);
		input = "000405ff";
		result = flt.hexToBytes(input);
		assert.deepEqual(result, expected);
		assert.instanceOf(result, Uint8Array);
	});
	
	
});


describe("testing bytes to bool conversion", function(){
	let flt = new Flaptools();
	
	it("should convert from bytes to bool", function(){
		let result, input, expected;
		
		input = new Uint8Array([]);
		expected = false;
		result = flt.bytesToBool(input);
		assert.deepEqual(result, expected);
		
		input = new Uint8Array([0]);
		expected = false;
		result = flt.bytesToBool(input);
		assert.deepEqual(result, expected);
		
		input = new Uint8Array([0,0,0,0,0,0]);
		expected = false;
		result = flt.bytesToBool(input);
		assert.deepEqual(result, expected);
		
		input = new Uint8Array([1]);
		expected = true;
		result = flt.bytesToBool(input);
		assert.deepEqual(result, expected);
		
		input = new Uint8Array([0,4,5,255]);
		expected = true;
		result = flt.bytesToBool(input);
		assert.equal(result, expected);
	});
	
	it("should convert bool to bytes", function(){
		let result, input, expected;
		
		expected = new Uint8Array([0]);
		input = false;
		result = flt.boolToBytes(input);
		assert.deepEqual(result, expected);
		assert.instanceOf(result, Uint8Array);
		
		expected = new Uint8Array([1]);
		input = true;
		result = flt.boolToBytes(input);
		assert.deepEqual(result, expected);
		assert.instanceOf(result, Uint8Array);
	});
});

describe("testing bytes to ascii conversion", function(){
	let flt = new Flaptools();
	
	it("should convert from bytes to ascii", function(){
		let result, input, expected;
		
		input = new Uint8Array([]);
		expected = "";
		result = flt.bytesToAscii(input);
		assert.deepEqual(result, expected);
		
		input = new Uint8Array([0x41, 0x5a, 0x45, 0x52, 0x54, 0x59, 0x59, 0x55, 
								0x49, 0x4f, 0x50, 0x71, 0x73, 0x64, 0x66, 0x67,
								0x68, 0x6a, 0x6b, 0x6c, 0x6d, 0x77, 0x78, 0x63, 
								0x76, 0x62, 0x6e, 0x31, 0x32, 0x33, 0x34, 0x35, 
								0x36, 0x37, 0x38, 0x39, 0x21 ,0x3a ,0x3b ,0x2c,
								0x22, 0x23]);
		expected = 'AZERTYYUIOPqsdfghjklmwxcvbn123456789!:;,"#';
		result = flt.bytesToAscii(input);
		assert.deepEqual(result, expected);

		
	});
	
	it("should convert ascii to bytes", function(){
		let result, input, expected;
		
		expected = new Uint8Array([]);
		input = "";
		result = flt.asciiToBytes(input);
		assert.deepEqual(result, expected);
		assert.instanceOf(result, Uint8Array);
		
		expected = new Uint8Array([ 0x41, 0x5a, 0x45, 0x52, 0x54, 0x59, 0x59, 0x55, 
									0x49, 0x4f, 0x50, 0x71, 0x73, 0x64, 0x66, 0x67,
									0x68, 0x6a, 0x6b, 0x6c, 0x6d, 0x77, 0x78, 0x63, 
									0x76, 0x62, 0x6e, 0x31, 0x32, 0x33, 0x34, 0x35, 
									0x36, 0x37, 0x38, 0x39, 0x21 ,0x3a ,0x3b ,0x2c,
									0x22, 0x23]);
		input = 'AZERTYYUIOPqsdfghjklmwxcvbn123456789!:;,"#';
		result = flt.asciiToBytes(input);
		assert.deepEqual(result, expected);
		assert.instanceOf(result, Uint8Array);
	});
	
	
});

describe("testing utf8 bytes to string conversion", function(){
    let flt = new Flaptools();

    it("should convert string to bytes", function(){
    	let result, input, expected;

    	input = "aA!$#&";
    	result = flt.stringToUtf8Bytes(input);
    	expected = new Uint8Array([0x61, 0x41, 0x21, 0x24, 0x23, 0x26]);
		assert.deepEqual(result, expected);

        input = "ЉΉ";
        result = flt.stringToUtf8Bytes(input);
        expected = new Uint8Array([0xD0, 0x89, 0xCE, 0x89]);
        assert.deepEqual(result, expected);

        input = "☐♪";
        result = flt.stringToUtf8Bytes(input);
        expected = new Uint8Array([0xE2, 0x98, 0x90, 0xE2, 0x99, 0xAA]);
        assert.deepEqual(result, expected);

        input = "𐀀𐀁";
        result = flt.stringToUtf8Bytes(input);
        expected = new Uint8Array([0xF0, 0x90, 0x80, 0x80, 0xF0, 0x90, 0x80, 0x81]);
        assert.deepEqual(result, expected);


	})

    it("should convert bytes to string", function(){
        let result, input, expected;

        expected = "aA!$#&";
        input = new Uint8Array([0x61, 0x41, 0x21, 0x24, 0x23, 0x26]);
        result = flt.utf8BytesToString(input);
        assert.deepEqual(result, expected);

        expected = "ЉΉ";
        input = new Uint8Array([0xD0, 0x89, 0xCE, 0x89]);
        result = flt.utf8BytesToString(input);
        assert.deepEqual(result, expected);

        expected = "☐♪";
        input = new Uint8Array([0xE2, 0x98, 0x90, 0xE2, 0x99, 0xAA]);
        result = flt.utf8BytesToString(input);
        assert.deepEqual(result, expected);

        expected = "𐀀𐀁";
        input = new Uint8Array([0xF0, 0x90, 0x80, 0x80, 0xF0, 0x90, 0x80, 0x81]);
        result = flt.utf8BytesToString(input);
        assert.deepEqual(result, expected);
	})
})

describe("testing bytes to integer conversion", function(){
	let flt = new Flaptools();
	
	it("should convert from bytes to integer", function(){
		let result, input, expected;
		
		input = new Uint8Array([]);
		expected = 0;
		result = flt.bytesToInt(input);
		assert.deepEqual(result, expected);
		
		input = new Uint8Array([0]);
		expected = 0;
		result = flt.bytesToInt(input);
		assert.deepEqual(result, expected);
		
		input = new Uint8Array([255]);
		expected = 255;
		result = flt.bytesToInt(input);
		assert.deepEqual(result, expected);
		
		input = new Uint8Array([0,0,0,0,0,0,0,0,0,0,255]);
		expected = 255;
		result = flt.bytesToInt(input);
		assert.deepEqual(result, expected);

		input = new Uint8Array([0, 1, 2, 3, 4, 5]);
		expected = 4328719365; 
		result = flt.bytesToInt(input);
		assert.deepEqual(result, expected);
	});
	
	it("should convert integer to bytes", function(){
		let result, input, expected;
		
		expected = new Uint8Array([0]);
		input = 0;
		result = flt.intToBytes(input);
		assert.deepEqual(result, expected);
		
		expected = new Uint8Array([255]);
		input = 255;
		result = flt.intToBytes(input);
		assert.deepEqual(result, expected);

		expected = new Uint8Array([1, 2, 3, 4, 5]);
		input = 4328719365; 
		result = flt.intToBytes(input);
		assert.deepEqual(result, expected);
	});

    it("should convert integer to bytes with the expected number of bytes in array result", function(){
    	let expectedByteLength = 7;
    	let result, input, expected;

        expected = new Uint8Array([0,0,0,0,0,0,0]);
        input = 0;
        result = flt.intToBytes(input, expectedByteLength);
        assert.deepEqual(result, expected);

        expected = new Uint8Array([0,0,0,0,0,0,255]);
        input = 255;
        result = flt.intToBytes(input, expectedByteLength);
        assert.deepEqual(result, expected);

        expected = new Uint8Array([0, 0, 1, 2, 3, 4, 5]);
        input = 4328719365;
        result = flt.intToBytes(input, expectedByteLength);
        assert.deepEqual(result, expected);
    });
});

describe("testing other helpers", function(){
	let flt = new Flaptools();

	it("should concatenate Uint8Array with agregateBytes", function(){
		let input = [
            new Uint8Array([2,4,6,8,10]),
            new Uint8Array([]),
            new Uint8Array([12,14,16,18,20]),
            new Uint8Array([22]),
		]
		let expected = new Uint8Array([2,4,6,8,10,12,14,16,18,20,22]);
		let result = flt.agregateBytes(input);
		assert.deepEqual(result, expected);
	})
})

/*****
/////////////////////////////////////////////////////////////////////////////
///////////////////// ENCODING AND DECODING METHODS     /////////////////////
/////////////////////////////////////////////////////////////////////////////

describe("testing the encode/decode methods", function(){
	let flt = new Flaptools();
	let eeBaseLen = 1;

	it("should encode/decode a boolean EE", function(){
		let result, expectedEncoded, encoded, decoded;
		let dict = {
			ee : {
				ee1 : { t : "bool", n : "ee1", c : 5 }
			}
		};
		
		// testing with an true boolean
		boolLen = 1;
		boolLenLen = 0;
		obj = { ee1 : true };
		encoded = flt.encode(obj, dict);
		expectedEncoded = new Uint8Array( [
			0x05, // 1 byte value, code 5
			1 // true
		]);
		assert.deepEqual(encoded, expectedEncoded);
		decoded = flt.decode(encoded, dict);
		assert.deepEqual(decoded, obj);
		
		// testing with an false boolean
		boolLen = 1;
		boolLenLen = 0;
		obj = { ee1 : false };
		encoded = flt.encode(obj, dict);
		expectedEncoded = new Uint8Array( [
			0x05, // 1 byte value, code 5
			0 // false
		]);
		assert.deepEqual(encoded, expectedEncoded);
		decoded = flt.decode(encoded, dict);
		assert.deepEqual(decoded, obj);
	});

    it("should encode/decode an utf8 EE", function() {
        let result, expectedLen, encoded, decoded, expectedEncoded, strlen, ascii, strlenLength, obj;
        let dict = {
            ee : {
                ee1 : { t : "utf8", n : "ee1", c : 6 }
            }
        };

        // testing with an empty ascii
        strLenLen = 1;
        obj = { ee1 : "aA!$#&ЉΉ☐♪𐀀𐀁" };
        encoded = flt.encode(obj, dict);
        expectedEncoded = new Uint8Array([
            38, // 1 byte lenlen + code 6
			24, // value length
            0x61, 0x41, 0x21, 0x24, 0x23, 0x26, // aA!$#&
            0xD0, 0x89, 0xCE, 0x89, // ЉΉ
            0xE2, 0x98, 0x90, 0xE2, 0x99, 0xAA, // ☐♪
            0xF0, 0x90, 0x80, 0x80, 0xF0, 0x90, 0x80, 0x81 // 𐀀𐀁
        ]);
        assert.deepEqual(encoded, expectedEncoded);
        decoded = flt.decode(encoded, dict);
        assert.deepEqual(decoded, obj);

    });

    it("should encode/decode an utf8 json EE", function() {
        let result, expectedLen, encoded, decoded, expectedEncoded, strlen, ascii, strlenLength, obj;
        let dict = {
            ee : {
                ee1 : { t : "json", n : "ee1", c : 6 }
            }
        };

        // testing with an empty ascii
        strLenLen = 1;
        obj = { ee1 :
				{
					"aA!$#&ЉΉ☐♪𐀀𐀁" : 443,
					25 : 2779,
					"sdfhsofdh" : false,
					"pppppp" : null
				}
        };
        encoded = flt.encode(obj, dict);
        decoded = flt.decode(encoded, dict);
        assert.deepEqual(decoded, obj);

    });


	it("should encode/decode an ascii EE", function(){
		let result, expectedLen, encoded, decoded, expectedEncoded, strlen, ascii, strlenLength, obj;
		let dict = {
			ee : {
				ee1 : { t : "ascii", n : "ee1", c : 6 }
			}
		};
		
		// testing with an empty ascii
		strLen = 0;
		ascii = "".padStart(strLen, "A");
		strLenLen = 1;
		obj = { ee1 : ascii };
		encoded = flt.encode(obj, dict);
		expectedLen = eeBaseLen + strLen + strLenLen;
		expectedEncoded = new Uint8Array([
			38, // 1 byte lenlen + code 6
			0 // empty ascii
		]);
		assert.lengthOf(encoded, expectedLen);
		assert.deepEqual(encoded, expectedEncoded);
		decoded = flt.decode(encoded, dict);
		assert.deepEqual(decoded, obj);
		
		// testing with a one character ascii
		strLen = 1;
		ascii = "".padStart(strLen, "A");
		strLenLen = 0;
		obj = { ee1 : ascii };
		encoded = flt.encode(obj, dict);
		expectedLen = eeBaseLen + strLen + strLenLen;
		expectedEncoded = new Uint8Array([
			6, // 1 byte value + code 6
			0x41 // A
		]);
		assert.lengthOf(encoded, expectedLen);
		assert.deepEqual(encoded, expectedEncoded);
		decoded = flt.decode(encoded, dict);
		assert.deepEqual(decoded, obj);
		
		// testing with a two character ascii
		strLen = 2;
		ascii = "".padStart(strLen, "A");
		strLenLen = 1;
		obj = { ee1 : ascii };
		encoded = flt.encode(obj, dict);
		expectedLen = eeBaseLen + strLen + strLenLen;
		expectedEncoded = new Uint8Array([
			38, // 2 byte value + code 6
			2, // 2 byte value
			0x41, 0x41 // AA
		]);
		assert.lengthOf(encoded, expectedLen);
		assert.deepEqual(encoded, expectedEncoded);
		decoded = flt.decode(encoded, dict);
		assert.deepEqual(decoded, obj);
		
		// testing with a 3 characters ascii
		strLen = 3;
		ascii = "".padStart(strLen, "A");
		strLenLen = 1;
		obj = { ee1 : ascii };
		encoded = flt.encode(obj, dict);
		expectedLen = eeBaseLen + strLen + strLenLen;
		expectedEncoded = new Uint8Array([
			38, // 1 byte lenlen + code 6
			 3, // 3 byte ascii length
			0x41,0x41,0x41 // A
		]);
		assert.lengthOf(encoded, expectedLen);
		assert.deepEqual(encoded, expectedEncoded);
		decoded = flt.decode(encoded, dict);
		assert.deepEqual(decoded, obj);
		
		// testing with a 255 character ascii
		strLen = 255;
		ascii = "".padStart(strLen, "A");
		strLenLen = 1;
		obj = { ee1 : ascii };
		encoded = flt.encode(obj, dict);
		expectedLen = eeBaseLen + strLen + strLenLen;
		expectedEncoded = new Uint8Array([
			38, // 1 byte lenlen + code 6
			 255, // 255 byte ascii
			0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41,  // A * 32
			0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41,  // A * 32
			0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41,  // A * 32
			0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41,  // A * 32
			0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41,  // A * 32
			0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41,  // A * 32
			0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41,  // A * 32
			0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41      // A * 31
		]);
		assert.lengthOf(encoded, expectedLen);
		assert.deepEqual(encoded, expectedEncoded);
		decoded = flt.decode(encoded, dict);
		assert.deepEqual(decoded, obj);
		
		// testing with a 256 character ascii
		strLen = 256;
		ascii = "".padStart(strLen, "A");
		strLenLen = 2;
		obj = { ee1 : ascii };
		encoded = flt.encode(obj, dict);
		expectedLen = eeBaseLen + strLen + strLenLen;
		expectedEncoded = new Uint8Array([
			70,    // 1 byte lenlen + code 6
			1 , 0, // 256 byte ascii
			0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41,  // A * 32
			0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41,  // A * 32
			0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41,  // A * 32
			0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41,  // A * 32
			0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41,  // A * 32
			0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41,  // A * 32
			0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41,  // A * 32
			0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41, 0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41   // A * 32
		]);
		assert.lengthOf(encoded, expectedLen);
		assert.deepEqual(encoded, expectedEncoded);
		decoded = flt.decode(encoded, dict);
		assert.deepEqual(decoded, obj);
	});

	it("should guess the encode/decode of an int EE", function(){
		let result, expectedLen, strlen, ascii, strlenLength, obj, expectedEncoded, encoded, decoded;
		let dict = {
			ee : {
				ee1 : { t : "int", n : "ee1", c : 2 }
			}
		};
		
		// testing with 0
		len = 1;
		lenLen = 0;
		obj = { ee1 : 0 };
		expectedEncoded = new Uint8Array([
			2, // 0 byte lenlen, code 2
			0, // value 0
		]);
		expectedLen = eeBaseLen + len + lenLen;
		encoded = flt.encode(obj, dict);
		assert.lengthOf(encoded, expectedLen);
		assert.deepEqual(encoded, expectedEncoded);
		decoded = flt.decode(encoded, dict);
		assert.deepEqual(decoded, obj);
		
		// testing with 255
		len = 1;
		lenLen = 0;
		obj = { ee1 : 255 };
		expectedEncoded = new Uint8Array([
			2, // 0 byte lenlen, code 2
			255, // value 0
		]);
		expectedLen = eeBaseLen + len + lenLen;
		encoded = flt.encode(obj, dict);
		assert.lengthOf(encoded, expectedLen);
		assert.deepEqual(encoded, expectedEncoded);
		decoded = flt.decode(encoded, dict);
		assert.deepEqual(decoded, obj);
		
		// testing with 256
		len = 2;
		lenLen = 1;
		obj = { ee1 : 256 };
		expectedEncoded = new Uint8Array([
			34, // 0 byte lenlen, code 2
			2, // val len
			1,0, // 2 bytes value
		]);
		expectedLen = eeBaseLen + len + lenLen;
		encoded = flt.encode(obj, dict);
		assert.lengthOf(encoded, expectedLen);
		assert.deepEqual(encoded, expectedEncoded);
		decoded = flt.decode(encoded, dict);
		assert.deepEqual(decoded, obj);
		
		// testing with 65535
		len = 2;
		lenLen = 1;
		obj = { ee1 : 65535 };
		expectedEncoded = new Uint8Array([
			34, // 0 byte lenlen, code 2
			2, // val len
			255,255, // 2 bytes value
		]);
		expectedLen = eeBaseLen + len + lenLen;
		encoded = flt.encode(obj, dict);
		assert.lengthOf(encoded, expectedLen);
		assert.deepEqual(encoded, expectedEncoded);
		decoded = flt.decode(encoded, dict);
		assert.deepEqual(decoded, obj);
		
		// testing with 65536
		len = 3;
		lenLen = 1;
		obj = { ee1 : 65536 };
		expectedEncoded = new Uint8Array([
			34, // 2 byte lenlen, code 2
			3, // val len
			1,0,0 // value
		]);
		expectedLen = eeBaseLen + len + lenLen;
		encoded = flt.encode(obj, dict);
		assert.lengthOf(encoded, expectedLen);
		assert.deepEqual(encoded, expectedEncoded);
		decoded = flt.decode(encoded, dict);
		assert.deepEqual(decoded, obj);
		
		// testing with 2^24
		len = 4;
		lenLen = 1;
		obj = { ee1 : 16777216 };
		expectedEncoded = new Uint8Array([
			34, // 2 byte lenlen, code 2
			4, // 4 len value
			1,0,0,0 // value
		]);
		expectedLen = eeBaseLen + len + lenLen;
		encoded = flt.encode(obj, dict);
		assert.lengthOf(encoded, expectedLen);
		assert.deepEqual(encoded, expectedEncoded);
		decoded = flt.decode(encoded, dict);
		assert.deepEqual(decoded, obj);
		
		// testing with 2^32
		len = 5;
		lenLen = 1;
		obj = { ee1 : 4294967296 };
		expectedEncoded = new Uint8Array([
			34, // 2 byte lenlen, code 2
			5, // 5 len value
			1,0,0,0,0 // value
		]);
		expectedLen = eeBaseLen + len + lenLen;
		encoded = flt.encode(obj, dict);
		assert.lengthOf(encoded, expectedLen);
		assert.deepEqual(encoded, expectedEncoded);
		decoded = flt.decode(encoded, dict);
		assert.deepEqual(decoded, obj);
	});
	
	it("should guess the length of an hex EE", function(){
		let result, expectedLen, strlen, ascii, strlenLength, obj, encoded, decoded, expectedEncoded;
		let dict = {
			ee : {
				ee1 : { t : "hex", n : "ee1", c : 31 }
			}
		};
		
		// testing with an empty hex
		strLen = 0;
		hex = "".padStart(2*strLen, "aa");
		strLenLen = 1;
		obj = { ee1 : hex };
		expectedLen = eeBaseLen + strLen + strLenLen;
		expectedEncoded = new Uint8Array([
			63, // 2 byte lenlen, code 31
			0, // 0 len value
		]);
		encoded = flt.encode(obj, dict);
		assert.lengthOf(encoded, expectedLen);
		assert.deepEqual(encoded, expectedEncoded);
		decoded = flt.decode(encoded, dict);
		assert.deepEqual(decoded, obj);
				
		// testing with a one character hex
		strLen = 1;
		hex = "".padStart(2*strLen, "aa");
		strLenLen = 0;
		obj = { ee1 : hex };
		expectedLen = eeBaseLen + strLen + strLenLen;
		expectedEncoded = new Uint8Array([
			31, // 0 byte lenlen, code 31
			0xaa // 1 byte value
		]);
		encoded = flt.encode(obj, dict);
		assert.lengthOf(encoded, expectedLen);
		assert.deepEqual(encoded, expectedEncoded);
		decoded = flt.decode(encoded, dict);
		assert.deepEqual(decoded, obj);
		
		// testing with a two character hex
		strLen = 2;
		hex = "".padStart(2*strLen, "aa");
		strLenLen = 1;
		obj = { ee1 : hex };
		expectedLen = eeBaseLen + strLen + strLenLen;
		expectedEncoded = new Uint8Array([
			63, // 1 byte lenlen, code 31
			2, // vlen
			0xaa,0xaa // 2 byte value
		]);
		encoded = flt.encode(obj, dict);
		assert.lengthOf(encoded, expectedLen);
		assert.deepEqual(encoded, expectedEncoded);
		decoded = flt.decode(encoded, dict);
		assert.deepEqual(decoded, obj);
		
		// testing with a 3 characters hex
		strLen = 3;
		hex = "".padStart(2*strLen, "aa");
		strLenLen = 1;
		obj = { ee1 : hex };
		expectedLen = eeBaseLen + strLen + strLenLen;
		expectedEncoded = new Uint8Array([
			63, // 0 byte lenlen, code 31
			3, // 3
			0xaa,0xaa,0xaa // 3 byte value
		]);
		encoded = flt.encode(obj, dict);
		assert.lengthOf(encoded, expectedLen);
		assert.deepEqual(encoded, expectedEncoded);
		decoded = flt.decode(encoded, dict);
		assert.deepEqual(decoded, obj);
		
		// testing with a 255 character hex
		strLen = 255;
		hex = "".padStart(2*strLen, "aa");
		strLenLen = 1;
		obj = { ee1 : hex };
		expectedLen = eeBaseLen + strLen + strLenLen;
		expectedEncoded = new Uint8Array([
			63, // 0 byte lenlen, code 31
			255, // 3
			0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,  // 0xaa * 32
			0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,  // 0xaa * 32
			0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,  // 0xaa * 32
			0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,  // 0xaa * 32
			0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,  // 0xaa * 32
			0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,  // 0xaa * 32
			0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,  // 0xaa * 32
			0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa  // 0xaa * 31
		]);
		encoded = flt.encode(obj, dict);
		assert.lengthOf(encoded, expectedLen);
		assert.deepEqual(encoded, expectedEncoded);
		decoded = flt.decode(encoded, dict);
		assert.deepEqual(decoded, obj);
		
		// testing with a 256 character hex
		strLen = 256;
		hex = "".padStart(2*strLen, "aa");
		strLenLen = 2;
		obj = { ee1 : hex };
		expectedLen = eeBaseLen + strLen + strLenLen;
		expectedEncoded = new Uint8Array([
			95, // 0 byte lenlen, code 31
			1,0, // 3
			0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,  // 0xaa * 32
			0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,  // 0xaa * 32
			0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,  // 0xaa * 32
			0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,  // 0xaa * 32
			0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,  // 0xaa * 32
			0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,  // 0xaa * 32
			0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,  // 0xaa * 32
			0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, 0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa   // 0xaa * 32
		]);
		encoded = flt.encode(obj, dict);
		assert.lengthOf(encoded, expectedLen);
		assert.deepEqual(encoded, expectedEncoded);
		decoded = flt.decode(encoded, dict);
		assert.deepEqual(decoded, obj);
	});
	
	it("should encode/decode an array of ee", function(){
		let result, expectedLen, obj, encoded, decoded, expectedEncoded;
		let dict = {
			ee : {
				ee1 : { t : "arr", n:"ee1", c : 0, tpl : { t : "hex", n:"ee2", c : 1} },	
			}
		};
		
		// testing with an empty array
		obj = { ee1 : [] };
		expectedEncoded = new Uint8Array([
			32, // 1 byte lenlen, code 0
			0, // array length
		]);
		encoded = flt.encode(obj, dict);
		assert.deepEqual(encoded, expectedEncoded);
		decoded = flt.decode(encoded, dict);
		assert.deepEqual(decoded, obj);
		
		// testing with a single element array
		obj = { ee1 : ["aa"] };
		expectedEncoded = new Uint8Array([
			32, // 1 byte lenlen, code 0
			2, // array length
			1, 170 // aa
		]);
		encoded = flt.encode(obj, dict);
		assert.deepEqual(encoded, expectedEncoded);
		decoded = flt.decode(encoded, dict);
		assert.deepEqual(decoded, obj);
		
		// testing with a 5-element array
		obj = { ee1 : ["aa", "aaaa", "", "000000", "11"] };
		expectedEncoded = new Uint8Array([
			32, // 1 byte lenlen, code 0
			12, // array length
			1, 170, // aa
			2, 170, 170, //aaaa
			0, // ""
			3, 0,0,0, // 000000
			1, 17 // 11
		]);
		encoded = flt.encode(obj, dict);
		assert.deepEqual(encoded, expectedEncoded);
		decoded = flt.decode(encoded, dict);
		assert.deepEqual(decoded, obj);

		// testing with a 256 elements array
		var arr = [];
		for(let i=0;i<256; i++) arr.push("");
        obj = { ee1 : arr };
        expectedEncoded = new Uint8Array([
            64, // 2 byte lenlen, code 0
            1,0,
            0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0
        ]);
        encoded = flt.encode(obj, dict);
        assert.deepEqual(encoded, expectedEncoded);
        decoded = flt.decode(encoded, dict);
        assert.deepEqual(decoded, obj);
	});
	
	it("should encode/decode a bin EE", function(){
		let result, expectedLen, strlen, bin, binLen, binLenLen, obj;
		let dict = {
			ee : {
				ee1 : { t : "bin", n : "ee1", c : 0 }
			}
		};
		
		// testing with an empty ascii
		binLen = 0;
		bin = new Uint8Array(binLen);
		binLenLen = 1;
		obj = { ee1 : bin };
		expectedLen = eeBaseLen + binLen + binLenLen;
		expectedEncoded = new Uint8Array([
			32, // 0 byte lenlen, code 0
			0, // len 0
		]);
		encoded = flt.encode(obj, dict);
		assert.lengthOf(encoded, expectedLen);
		assert.deepEqual(encoded, expectedEncoded);
		decoded = flt.decode(encoded, dict);
		assert.deepEqual(decoded, obj);
		
		// testing with a one character ascii
		binLen = 1;
		bin = new Uint8Array(binLen);
		binLenLen = 0;
		obj = { ee1 : bin };
		expectedLen = eeBaseLen + binLen + binLenLen;
		expectedEncoded = new Uint8Array([
			0, // 0 byte lenlen, code 0
			0, // 1 byte value
		]);
		encoded = flt.encode(obj, dict);
		assert.lengthOf(encoded, expectedLen);
		assert.deepEqual(encoded, expectedEncoded);
		decoded = flt.decode(encoded, dict);
		assert.deepEqual(decoded, obj);
		
		// testing with a two character ascii
		binLen = 2;
		bin = new Uint8Array([48,72]);
		binLenLen = 1;
		obj = { ee1 : bin };
		expectedLen = eeBaseLen + binLen + binLenLen;
		expectedEncoded = new Uint8Array([
			32, // 0 byte lenlen, code 0
			2, // vlen
			48,72 // 2 byte value
		]);
		encoded = flt.encode(obj, dict);
		assert.lengthOf(encoded, expectedLen);
		assert.deepEqual(encoded, expectedEncoded);
		decoded = flt.decode(encoded, dict);
		assert.deepEqual(decoded, obj);
		
		// testing with a 3 characters ascii
		binLen = 3;
		bin = new Uint8Array([128,255,0]);
		binLenLen = 1;
		obj = { ee1 : bin };
		expectedLen = eeBaseLen + binLen + binLenLen;
		expectedEncoded = new Uint8Array([
			32, // 0 byte lenlen, code 0
			3, // 3 byte value length
			128,255,0 // 3 byte value
		]);
		encoded = flt.encode(obj, dict);
		assert.lengthOf(encoded, expectedLen);
		assert.deepEqual(encoded, expectedEncoded);
		decoded = flt.decode(encoded, dict);
		assert.deepEqual(decoded, obj);
		
		// testing with a 255 character ascii
		binLen = 255;
		bin = new Uint8Array(binLen);
		binLenLen = 1;
		obj = { ee1 : bin };
		expectedLen = eeBaseLen + binLen + binLenLen;
		expectedEncoded = new Uint8Array([
			32, // 2 byte lenlen, code 0
			255, // 255 byte value length
			0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, // 0 * 32
			0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, // 0 * 32
			0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, // 0 * 32
			0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, // 0 * 32
			0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, // 0 * 32
			0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, // 0 * 32
			0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, // 0 * 32
			0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0    // 0 * 31
		]);
		encoded = flt.encode(obj, dict);
		assert.lengthOf(encoded, expectedLen);
		assert.deepEqual(encoded, expectedEncoded);
		decoded = flt.decode(encoded, dict);
		assert.deepEqual(decoded, obj);
		
		// testing with a 256 character ascii
		binLen = 256;
		bin = new Uint8Array(binLen);
		binLenLen = 2;
		obj = { ee1 : bin };
		expectedLen = eeBaseLen + binLen + binLenLen;
		expectedEncoded = new Uint8Array([
			64, // 2 byte lenlen, code 0
			1,0, // 256 byte value length
			0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, // 0 * 32
			0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, // 0 * 32
			0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, // 0 * 32
			0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, // 0 * 32
			0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, // 0 * 32
			0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, // 0 * 32
			0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, // 0 * 32
			0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0  // 0 * 32
		]);
		encoded = flt.encode(obj, dict);
		assert.lengthOf(encoded, expectedLen);
		assert.deepEqual(encoded, expectedEncoded);
		decoded = flt.decode(encoded, dict);
		assert.deepEqual(decoded, obj);
	});

	it("should encode/decode several ee", function(){
		let result, expectedLen, expectedEncoded, encoded, decoded;
		let dict = {
			ee : {
				ee1 : { t : "bin", n : "ee1", c : 0 },
				ee2 : { t : "hex", n : "ee2", c : 1 },
			}
		};
		
		// testing with an empty ascii and a 255 character hex
		let bin = new Uint8Array([1,2,4,8,16]);
		hex = "aaff";
		obj = { ee1 : bin, ee2 : hex };
		encoded = flt.encode(obj, dict);
		expectedEncoded = new Uint8Array([
			//ee1
			32, // 2byte lenlen + code 0 
			5, // 5 byte len
			1,2,4,8,16, // byte value
			// ee2
			33, // 2 byte value + code 1
			2, 
			0xaa, 0xff
		]);
		assert.deepEqual(encoded, expectedEncoded);
		decoded = flt.decode(encoded, dict);
		assert.deepEqual(decoded, obj);
	});
	
	it("should encode/decode a dictionnary", function(){
		let result, bin, hex, encoded, decoded, expectedEncoded, obj;
		let dict1 = {
			ee : {
				ee1 : { t : "bin", n : "ee1", c : 0 },
				ee2 : { t : "hex", n : "ee2", c : 1 },
			}
		};
		let dict2 = {
			ee : {
				ee3 : { t : "dict", n : "ee3", tpl : dict1, c :3 },
			}
		}
		
		// testing with an empty ascii and a 255 character hex
		bin = new Uint8Array([2,5,8,3,6,9,255]);
		hex = "aa";
		
		obj = { ee3 : { ee1 : bin, ee2 : hex } };
		expectedEncoded = new Uint8Array([
			//ee3
			35,
			11,
			// ee1
			32,
			7,
			2,5,8,3,6,9,255,
			// ee2
			1,
			0xaa
		]);
		encoded = flt.encode(obj, dict2);
		assert.deepEqual(encoded, expectedEncoded);
		decoded = flt.decode(encoded, dict2);
		assert.deepEqual(decoded, obj);
	});
	
	it("should encode/decode an array", function(){
		let result, bin, hex, encoded, decoded, expectedEncoded, obj;
		let dict1 = {
			ee : {
				ee1 : { t : "ascii", n : "ee1", c : 3 },
			}
		};
		let dict2 = {
			ee : {
				ee2 : { t : "arr", n : "ee2", tpl : dict1, c :0 },
			}
		}
		
		// testing with an empty ascii and a 255 character hex
		bin = new Uint8Array([2,5,8,3,6,9,255]);
		hex = "aa";
		
		obj = {  ee2 : [{ee1:""},{ee1:"AA"},{ee1:"AAAAAA"}] };
		expectedEncoded = new Uint8Array([
			//ee2
			32,
			17,
			// ei 1
			2,
			// ee1 1
			35,
			0,
			// ei2
			4,
			// ee1 2
			35,
			2,
			0x41,0x41,
			// ei 3
			8,
			// ee1 3
			35,
			6,
			0x41,0x41,0x41,0x41,0x41,0x41
		]);
		encoded = flt.encode(obj, dict2);
		assert.deepEqual(encoded, expectedEncoded);
		decoded = flt.decode(encoded, dict2);
		assert.deepEqual(decoded, obj);
	});

	it("should encode/decode an ee with a replace object", function(){
		let encoded, decoded, obj, expectedEncoded;
		let dict = {
			ee : {
				ee1 : { t : "int", n:"ee1", c : 0, rpl : { 1 : "rpl1", 5 : "rpl5"} },	
				ee2 : { t : "int", n:"ee2", c : 1, rpl : { 1 : "rpl1", 5 : "rpl5"} }
			}
		};
		
		obj = { ee1 : "rpl5", ee2 : 3};
		expectedEncoded = new Uint8Array([
			0,
			5,
			1,
			3
		]);
		encoded = flt.encode(obj, dict);
		assert.deepEqual(encoded, expectedEncoded);
		decoded = flt.decode(encoded, dict);
		assert.deepEqual(decoded, obj);
	});
	
	it("should throw an error when a required ee is not provided", function(){
		let result, expectedLen, strlen, strLenLen,  bin, binLen, binLenLen, obj;
		let dict = {
			ee : {
				ee1 : { t : "bin", n : "ee1", c : 0, r : true },
			}
		};
		let raised = false;
		obj = { };
		try{
			result = flt.encode(obj, dict);
		}
		catch(e){
			raised = true;
		}
		assert.ok(raised);
	});
	
	it("should encode with a default value", function(){
		let result, obj, encoded, expectedEncoded;
		let dict = {
			ee : {
				ee1 : { t : "ascii", n : "ee1", c : 0 , d : "AAAAAA"},
			}
		};
		
		// testing with a default ascii of 6 characters
		obj = {};
		encoded = flt.encode(obj, dict);
		expectedEncoded = new Uint8Array([
			32,
			6,
			0x41, 0x41, 0x41, 0x41, 0x41, 0x41
		]);
		assert.deepEqual(encoded, expectedEncoded);
	});
	
	it("should encode with a default encode value", function(){
		let result, obj, encoded, expectedEncoded;
		let dict = {
			ee : {
				ee1 : { t : "ascii", n : "ee1", c : 0 , d : "AAAAAA", de : "AAA"},
			}
		};
		
		// testing with a default ascii of 6 characters
		obj = {};
		encoded = flt.encode(obj, dict);
		expectedEncoded = new Uint8Array([
			32,
			3,
			0x41, 0x41, 0x41
		]);
		assert.deepEqual(encoded, expectedEncoded);
	});
	
	
	it("should decode with a default value", function(){
		let result, obj, encoded, expectedDecoded;
		let dict = {
			ee : {
				ee1 : { t : "ascii", n : "ee1", c : 0 , d : "AAAAAA"},
			}
		};
		
		// testing with a default ascii of 6 characters
		obj = {};
		encoded = new Uint8Array();
		expectedDecoded = {ee1:"AAAAAA"};
		decoded = flt.decode(encoded, dict);
		assert.deepEqual(decoded, expectedDecoded);
	});
	
	it("should decode with a default decode value", function(){
		let result, obj, encoded, expectedDecoded;
		let dict = {
			ee : {
				ee1 : { t : "ascii", n : "ee1", c : 0 , d : "AAAAAA", dd:"AAA"},
			}
		};
		
		// testing with a default ascii of 3 characters
		encoded = new Uint8Array();
		expectedDecoded = {ee1:"AAA"};
		decoded = flt.decode(encoded, dict);
		assert.deepEqual(decoded, expectedDecoded);
	});
	
	it("should throw an error while decoding because a required ee was not provided", function(){
		let result, obj, encoded, expectedDecoded;
		let dict = {
			ee : {
				ee1 : { t : "ascii", n : "ee1", c : 0 , r : true},
			}
		};
		
		// testing with a default ascii of 3 characters
		encoded = new Uint8Array();
		let raised = false;
		try{
			decoded = flt.decode(encoded, dict);
		}
		catch(e){
			raised = true;
		}
		assert.ok(raised);
	});
	
	
	it("integration test of all encode/decode functionnalities", function(){
		
		
		let dict2 = {
			ee : {
				ee4 : { t : "bin", n : "ee4", c : 0 , de : new Uint8Array([1,2]) },
				ee5 : { t : "hex", n : "ee5", c : 1 , dd : "1122334455"},
				ee6 : { t : "bool", n : "ee6", c : 2, d : true },
			}
		};
		
		let dict1 = {
			ee : {
				ee1 : { t : "ascii", n : "ee1", c : 3 },
				ee2 : { t : "hex", n : "ee2", c : 4 },
				ee3 : { t : "bool", n : "ee3", c : 5, d : true },
				ee7 : { t : "dict", n : "ee7", c : 6, d : {}, tpl : dict2 },
			}
		};
		
		// testing with a 3 bytes ascii
		let input = { 
			ee1 : "aeza,;:,;,m!",
			ee2 : "aaeeff1234567890",
			ee3 : false,
		};
		let expectedOutput = {
			ee1 : "aeza,;:,;,m!",
			ee2 : "aaeeff1234567890",
			ee3 : false,
			ee7 : {
				ee4 : new Uint8Array([1,2]),
				ee5 : "1122334455",
				ee6 : true
			}
		}
		let encoded = flt.encode(input, dict1);
		let decoded = flt.decode(encoded, dict1);
		assert.deepEqual(decoded, expectedOutput);
	});

});


describe("testing macro related methods", function(){
	let flt;

	beforeEach(function(){
		flt = new Flaptools();
	})


    it("should encode using a macro", function(){
        let result, expectedEncoded, expectedDecoded, encoded, decoded, macro;

        // check the value passed to the macro are the one expected
        let expectedmacroValue, expectedMacroEe, expectedMacroData;
        macro = function(v, ee, data){
            expectedmacroValue = v;
            expectedMacroEe = ee;
            expectedMacroData = data;
            return new Uint8Array([1,2,4,8,16, v]);
        }
        let dict = {
            ee : {
                ee1 : { t : "bin", n : "ee1", c : 0, macro : macro },
            }
        };

        obj = { ee1 : 39 };
        encoded = flt.encode(obj, dict);
        assert.deepEqual(expectedmacroValue, obj.ee1);
        assert.deepEqual(expectedMacroEe, dict.ee.ee1);
        assert.deepEqual(expectedMacroData, {});

        expectedDecoded = { ee1 : new Uint8Array([1,2,4,8,16,39]) };
        decoded = flt.decode(encoded, dict);
        assert.deepEqual(decoded, expectedDecoded);
    })

    it("should encode using a macro and an endMacro", function(){
        let result, expectedEncoded, expectedDecoded, encoded, decoded, macro, endMacro;

        // macro and endMacro data
        let endMacroExpectedData = {
            index : 0,
            len : 3,
            data : {
                customData : 94994
            }
        }
        macro = function(v, ee, data){
            for(let i in endMacroExpectedData.data) data[i] = endMacroExpectedData.data[i];
            return new Uint8Array([1,v]);
        }
        let endMacroReceivedData, endMacroReceivedEe;
        endMacro = function(bytes, ee, data){
            endMacroReceivedData = data;
            endMacroReceivedEe = ee;
            bytes[0] = 99;
        }

        let dict = {
            ee : {
                ee1 : { t : "int", n : "ee1", c : 0, macro : macro, endMacro : endMacro },
            }
        };

        obj = { ee1 : 39 };
        expectedEncoded = new Uint8Array([99, 2, 1, 39]);
        encoded = flt.encode(obj, dict);
        assert.deepEqual(endMacroReceivedData, endMacroExpectedData);
        assert.deepEqual(endMacroReceivedEe, dict.ee.ee1);
        assert.deepEqual(encoded, expectedEncoded);
    })

	it("should encode using the $length macro to calculate the length", function(){
		let maxLengthTest = 400;

		// perform the test with various ee size
		for(let i=0; i<maxLengthTest; i++){
            let result, obj, encoded, expectedDecoded;
            let dict = {
                ee : {
                    ee1 : { t : "ascii", n : "ee1", c : 0 },
                    ee2 : { t : "int", n : "ee2", c : 1 , endMacro : "$length", de : 0},
                    ee3 : { t : "ascii", n : "ee3", c : 2 },
                }
            };
			obj = {
				ee1 : "sdlfjsdfoj",
				ee3 : "a".repeat(i)
			}
            encoded = flt.encode(obj, dict);
			decoded = flt.decode(encoded, dict);
            assert.equal(decoded.ee1, obj.ee1);
            assert.equal(decoded.ee3, obj.ee3);
			assert.equal(decoded.ee2, encoded.length, "comparing length for iteration : "+i);
		}
	})

	it("should not decode data greater after index above maxLength", function(){
        let result, obj, encoded, expectedDecoded;
        let dict = {
            ee : {
                ee1 : { t : "ascii", n : "ee1", c : 0 },
                ee2 : { t : "int", n : "ee2", c : 1 , endMacro : "$length", de : 0},
                ee3 : { t : "ascii", n : "ee3", c : 2 },
				ee4 : { t : "int", n : "ee4", c : 3}
            },
			maxLength : "ee2"
        };
        obj = {
            ee1 : "sdlfjsdfoj",
            ee3 : "test"
        }

        encoded = flt.encode(obj, dict);
        // extend the encoded array with an ee4
		let extendedEncoded = new Uint8Array(encoded.length+2);
		extendedEncoded.set(encoded,0);
		extendedEncoded[extendedEncoded.length-2] = dict.ee.ee4.c;

        decoded = flt.decode(extendedEncoded, dict);
        assert.equal(decoded.ee1, obj.ee1)
        assert.equal(decoded.ee3, obj.ee3)
        assert.notProperty(decoded, "ee4", "the decoded object should not have ee4 defined");
	})
})

 ***/