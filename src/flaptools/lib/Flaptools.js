/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

// flaptools is a library to help creating flock applications (flap)
// in particular, it defines a way to create protocols and serialize/unserialize data

class Flaptools{
	
	constructor(){
		this._initConstants();
	}
	
	////////////////////////////////////////////////////////////////////
	/////////////////////   TOOLBOX   //////////////////////////////////
	////////////////////////////////////////////////////////////////////
	
	// return a new instance of threadops
	threadops(data={}){
		return new Threadops(data);
	}

    ////////////////////////////////////////////////////////////////////
    /////////////////////   KEY GENERATION    //////////////////////////
    ////////////////////////////////////////////////////////////////////

    // random bytes generation
    randomBytes(len=8){
        let res = new Uint8Array(len);
        for(let i in res) res[i] = Math.random()*256 >> 0;
        return res;
    }

	// random hexadecimal generation
    randomHex(len=8){
        return this.bytesToHex( this.randomBytes(len) );
    }

    ////////////////////////////////////////////////////////////////////
	//////////////////   DATA FORMAT HELPERS   /////////////////////////
	////////////////////////////////////////////////////////////////////
	
	// byte to hexadecimal conversion
	bytesToHex(b){
		let res = Array.from(b).map(i=>(i<16 ? "0" : "")+i.toString(16)).join("");
		return res;
	}
	
	// hexadecimal to bytes conversion
	hexToBytes(h){
		if(h.length%2) h = "0"+h;
		let res= new Uint8Array(h.length/2).map((v,i)=>parseInt(h.substr(2*i,2),16));
		return res
	}
	
	// bytes to ascii
	bytesToAscii(b){
		return String.fromCharCode(...b);
	}
	
	// ascii to bytes
	asciiToBytes(s){
		return new Uint8Array(s.length).map((v,i)=>s.charCodeAt(i));
	}
	
	// integer to bytes : assume i < 2^54
	intToBytes(i, exactLen=null){
		i = parseInt(i);
		let maxLen = 8; // enough to be used with maxLen == vLenLen == 8
		let b = new Uint8Array(maxLen);
		let k = maxLen;
		while(i && k){
			b[--k] = i%256;
			i = Math.floor(i/256);
		}
		return b.slice(exactLen ? maxLen - exactLen : Math.min(k,maxLen-1) );
	}

	// bytesToInteger
	bytesToInt(b){
		let r = 0; 
		for(let i in b) r = 256*r + b[i];
		return r;
	}
	
	boolToBytes(b){
		return b ? this.BYTE_TRUE : this.BYTE_FALSE;
	}
	
	bytesToBool(b){
		return !!this.bytesToInt(b);
	}


	stringToUtf8Bytes(s){
		let res = [];
		for(let i=0; i<s.length; i++){
			let c = s.charCodeAt(i);
			if(c < 0x80) res.push(c);
			else if(c < 0x800) res.push(0xc0 | (c >> 6), 0x80 | (c & 0x3f) )
			else if( c < 0xd800 || c >= 0xe000) res.push( 0xe0 | (c>>12),  0x80 | ((c>>6) & 0x3f) , 0x80 | (c & 0x3f) )
			else{
				let codepoint = 0x10000 + ((c & 0x03ff)<<10) + (s.charCodeAt(++i) & 0x03ff);
				res.push( 0xf0 + (0x03 & (codepoint>>18)), 0x80 | ((codepoint>>12) & 0x3f) , 0x80 | ((codepoint>>6) & 0x3f) , 0x80 | (codepoint & 0x3f) );
			}
		}
		return new Uint8Array(res);
	}

	utf8BytesToString(bytes){
		let res = [];

		for(let i=0; i<bytes.length; i++){
			let b = bytes[i];
			if(b < 0x80) res.push(b);
			else if(b < 0xe0) res.push( ((b&0x1f)<<6) + (bytes[++i] & 0x3f) );
			else if( b <0xf0) res.push( ((b&0x0f)<<12) + ((bytes[++i] & 0x3f)<<6) + (bytes[++i] & 0x3f) );
			else{
				let codepoint = ((b&0x07)<<18) + ((bytes[++i] & 0x3f)<<12) + ((bytes[++i] & 0x3f)<<6) + (bytes[++i] & 0x3f);
				let diffcode = codepoint - 0x10000;
				res.push( 0xd800 + (diffcode>>10), 0xdc00 + (diffcode & 0x03ff) );
			}
		}
		return res.map(c=>String.fromCharCode(c)).join("");
	}

    ////////////////////////////////////////////////////////////////////
    ////////////                OTHER HELPERS             //////////////
    ////////////////////////////////////////////////////////////////////

	// agregate a list of Uint8Array into one array
	agregateBytes(arrList){
		// calculate the size
		let resultSize = arrList.reduce((acc, arr)=>acc+arr.length, 0);
		let result = new Uint8Array(resultSize);
		let index = 0;
		for(let arr of arrList){
			result.set(arr, index);
			index += arr.length;
		}
		return result;
	}

	////////////////////////////////////////////////////////////////////
	//  Extensible entries : data serialization / unserialization   ////
	////////////////////////////////////////////////////////////////////

	// encode an object given a collection model
	// return an Uint8Array
	encode( obj, dict){
        if(Array.isArray(dict.ee)) dict.ee = this.eeArrayToObject(dict.ee);
		// build result Uint8Array
		// let totalLen = this.calculateLength(obj, dict);
		let resultChunks = [];

		// temporary data for macro execution
		let macroData = {};

		// parse each dictionnary field
		for(let eeName in dict.ee){

			let ee = dict.ee[eeName];
			macroData[ee.n] = { data : {}, index : resultChunks.length };
			let v;
			if(obj.hasOwnProperty(eeName)) v = obj[eeName];
			else {
				if(ee.hasOwnProperty("de")) v = ee.de;
				else if(ee.hasOwnProperty("d")) v = ee.d;
				else if(ee.r) throw "Required EE was not provided : " + eeName;
				else continue;
			}
			// compute and push data about the ee
			this._pushParts(v, ee, resultChunks, macroData[ee.n].data);

			// remember the number of chunks associated to the ee
            macroData[ee.n].len = resultChunks.length - macroData[ee.n].index;
		}

		// execute endMacro if there are any
    	for(let eeName in dict.ee){
    		let ee = dict.ee[eeName];
    		if(!ee.endMacro) continue;
    		let endMacroFunc = typeof ee.endMacro === "string" ? this[ee.endMacro].bind(this) : ee.endMacro;
    		endMacroFunc(resultChunks, ee, macroData[eeName]);
        }
		// build the result by concatenating all sub arrays
		let res = this._bytesConcat(resultChunks);
		return res;
	}

	// build and push the bytes for an ee encoding
	_pushParts(v, ee, parts, macroData={}){
        // compute the byte length of key (0 if official code)
        let withKey = !ee.hasOwnProperty("c");
        let keyLength = withKey ? 0 : Math.min(31, ee.n.length);

        // replace the user readable value by its code
        v = this._valueToCode(v, ee);

        // compute the byte value to push
        let bytes = this._eeToBytes(v,ee, macroData);

        // compute the value length length
        let vlen = bytes.length;
        let vlenlen = this._vLenLen(vlen);

        let vlenCode;
        if(vlenlen === 0) vlenCode = 0;
        else if(vlenlen === 1) vlenCode = 1;
        else if(vlenlen === 2) vlenCode = 2;
        else vlenCode = 3;

        // push the first EE byte
        parts.push( (withKey ? 128 : 0) + (vlenCode<<5) + (withKey ? keyLength : ee.c) );

        // push the key if present
        if(withKey){
            let k = this.asciiToBytes(eeName);
            parts.push(k);
        }

        // push the value length if present
        if(vlenlen){
            let vlb = this.intToBytes(vlen, vlenlen);
            // res.set(vlb, index + vlenlen - vlb.length);
            parts.push(vlb);
        }

        // push the value bytes
        parts.push(bytes);
    }

	// transform an ee value into bytes
	_eeToBytes(v, ee, macroData){
		let bytes;
		// in case a macro is defined for the ee, execute it to compute the byte value
		if(ee.macro){
			let macroFunc = typeof ee.macro === "string" ? this[ee.macro] : ee.macro;
			bytes = macroFunc(v, ee, macroData);
        }
		else {
            switch (ee.t) {
                case "ascii" :
                    bytes = this.asciiToBytes(v);
                    break;
                case "utf8" :
                    bytes = this.stringToUtf8Bytes(v);
                    break;
				case "json" :
					bytes = this.stringToUtf8Bytes( JSON.stringify(v) );
					break;
                case "bin" :
                    bytes = v;
                    break;
                case "hex" :
					bytes = this.hexToBytes(v);
                    break;
                case "bool" :
                    bytes = this.boolToBytes(v);
                    break;
                case "int" :
                    bytes = this.intToBytes(v);
                    break;
                case "dict" :
                    bytes = this.encode(v, ee.tpl);
                    break;
                case "arr" :
                	let parts = [];
                    for (let e of v) {
                        let eb = ee.tpl.ee ? this.encode(e, ee.tpl) : this._eeToBytes(e, ee.tpl);
                        // push the element length
						parts.push( this._intToEI(eb.length) );
						// push the element
                        parts.push(eb);
                    }
                    bytes = this._bytesConcat(parts);
                    break;
                default :
                    throw "Unexpected EE type : " + ee.t;
            }
        }
		return bytes;
	}

	// concatenate a list of Uint8Array and integers
	_bytesConcat(parts){
		// compute the length of the result
		let len = this._bytePartsLength(parts);

		// build the concatenation
		let res = new Uint8Array(len);
        let index = 0;
        for(let p of parts){
            if(typeof p === "object"){
                res.set(p, index);
                index += p.length;
            }
            else res[index++] = p;
        }
        return res;
    }

    // compute the total length of an array of bytes array
	_bytePartsLength(parts){
        let len = 0;
        for(let p of parts) len += typeof p === "object" ? p.length : 1;
        return len;
    }

	// push a EI in a byte array, and return the updated array
	_pushEI(bytes, index, eiVal){
		if(eiVal<128){
			bytes[index++] = eiVal;
			return index;
		}
		let bval = this.intToBytes(eiVal);
		bytes[index++] = 128 + bval.length;
		bytes.set(bval, index);
		return index + bval.length;
	}

	// transform an array of EE into a dictionnary of EE indexed by the name "n"
	eeArrayToObject(a){
		let r = {}
		for(let e of a) r[e.n]=e;
		return r;
    }

	// extract an object from an Uint8Array interpreted with a dictionnary
	decode(bytes, dict){
		if(Array.isArray(dict.ee)) dict.ee = this.eeArrayToObject(dict.ee);
		let res = {};
		let maxIndex = bytes.length;
		let index = 0;
		let maxLength = dict.hasOwnProperty("maxLength") && typeof dict.maxLength != "string" ? dict.maxLength : +Infinity;
		// map the dict by key code
		let dmap = this._mapDictByCodes(dict);
		while(index < Math.min(maxLength, maxIndex) ){
			let unknown = false; // boolean to skip unknown keys
			let b0 = bytes[index++];
			
			// extract the custom key flag, 
			let kf = b0 & 128;
			
			// extract the value length code
			let vlc = (b0 & 96)>>5;
			
			// extract the keyLength / keyCode
			let key = b0 & 31;
			
			// extract the key name
			let keyName;
			if(kf){
				keyName = this.bytesToAscii( bytes.slice(index, index+key) );
				index += key;
			}
			else if( dmap[key] && dmap[key].c === key){
				keyName = dmap[key].n;
			}
			else unknown = true;
			
			// extract the valueLength
			let vlen;
			if(vlc === 0) vlen = 1;
			else{
				let vlenlen = vlc === 3 ? 8 : vlc;
				vlen = this.bytesToInt( bytes.slice(index, index+vlenlen) );
				index += vlenlen;
			}
			
			// extract the value 
			let v = bytes.slice(index, index+vlen);
			index += vlen;
			
			// if the key is not known in the dictionnary, and no unknown keys are authorized, skip the value
			let ee = dict.ee[keyName];
			// don't add unknown keys
			if(!ee && (dict.allowUndefined !== "decode") ) continue;
			
			// determine the type to decode the data
			let eeType = (ee && ee.t) || "hex";
			let dv = this._eeDecode(v,ee); // decoded value
			
			// replace the code by its user readable value
			dv = this._codeToValue(dv, ee);
			res[keyName]=dv;

			// if the extracted ee is matching maxLength property, it defines the maximum expected bytes length to decode
			if(""+ee.n === dict.maxLength) maxLength = dv;
		}
		
		// add default decode values
		for(let eeName in dict.ee){
			if(res.hasOwnProperty(eeName)) continue;
			let ee = dict.ee[eeName];
			if(ee.hasOwnProperty("dd")) res[eeName] = ee.dd;
			else if(ee.hasOwnProperty("d")) res[eeName] = ee.d;
			else if(ee.r) throw "Missing EE while decoding : " + eeName;
		}
		return res;
	}
	
	// decode a single ee given the byte the byte sequence value
	_eeDecode(v, ee){
		let dv;
		switch(ee.t) {
			case "ascii" : 
				dv = this.bytesToAscii(v);
				break;
			case "utf8" :
				dv = this.utf8BytesToString(v);
				break;
			case "json" :
				dv = JSON.parse( this.utf8BytesToString(v) );
				break
			case "bin" : 
				dv = v;
				break;
			case "hex" : 
				dv = this.bytesToHex(v); 
				break;
			case "bool" : 
				dv = this.bytesToBool(v);
				break; 
			case "int" : 
				dv = this.bytesToInt(v);
				break;	
			case "dict" :
				dv = this.decode(v, ee.tpl);
				break;
			case "arr" : 
				dv = [];
				let arrIndex = 0;
				while(arrIndex < v.length){
					let ei = this._extractEi(v,arrIndex);
					arrIndex = ei.index;
					let elBytes = v.slice(arrIndex, arrIndex + ei.ei);
					dv.push( ee.tpl.ee ? this.decode(elBytes, ee.tpl) : this._eeDecode(elBytes, ee.tpl) );
					arrIndex += elBytes.length;
				}
				break;
			default :
				throw "Unexpected EE type : "+ee.t;
		}
		return dv;
	}
	
	// extract an EI from a byte array
	// return an object containing ei and the new index
	_extractEi(bytes, index){
		if(bytes[index]<128) return { ei : bytes[index] & 127, index : index+1};
		let eil = bytes[index++] & 127;
		let ei = 0;
		for(let i=0; i<eil; i++) ei = ei*256 + bytes[index++];
		return { ei : ei, index : index};
	}

	// convert an integer to its EI representation
	_intToEI(i){
		if(i<128) return new Uint8Array([i]);
		let res = [];
		while(i){
			res.push(i & 0xff);
			i = i>>8;
        }
        res.push(res.length+128);
		return new Uint8Array( res.reverse() );
    }

	// map a dict ee both by keyName and key code
	_mapDictByCodes(dict){
		let r = {};
		let ees = dict.ee;
		for( let n in ees){
			if( ees[n].hasOwnProperty("c") ) r[ ees[n].c ] = ees[n];
		}
		return r;
	}
	
	// compute a value length length from a value length
	_vLenLen(vlen){
		if(vlen===1) return 0;
		else if(vlen<256) return 1;
		else if(vlen<65536) return 2;
		else return 8;
	}
	
	// replace a code by its user readable value
	_codeToValue(v, ee){
		return ee.rpl && ee.rpl.hasOwnProperty(v) ? ee.rpl[v] : v;
	}

	// replace a user readable value by the matching code
	_valueToCode(v, ee){
		if(ee.rpl && !ee.rpli){
			// compute the inverse array
			ee.rpli = {};
			for(let i in ee.rpl) ee.rpli[ ee.rpl[i] ] = i;
		}
		return ee.rpli && ee.rpli.hasOwnProperty(v) ? ee.rpli[v] : v;
	}

    ////////////////////////////////////////////////////////////////////
    /////////////////////        ENCODING MACRO    /////////////////////
    ////////////////////////////////////////////////////////////////////

	// endMacro that compute the length of the object at the end of the
	$length(parts, ee, data){
		// remove ee length parts
		let lengthParts = parts.splice(data.index, data.len);

		// compute the length of all the elements
        let len = this._bytePartsLength(parts);
        let fullLength = len;
        let tmpLen;
        while(fullLength != tmpLen){
        	tmpLen = fullLength;
            lengthParts = [];
            this._pushParts(fullLength, ee, lengthParts);
			fullLength = len + this._bytePartsLength(lengthParts);
        }

        // insert replacing parts representing the length
		parts.splice(data.index, 0, ...lengthParts);
    }


	////////////////////////////////////////////////////////////////////
	//////////////////////////   OTHER HELPERS     /////////////////////
	////////////////////////////////////////////////////////////////////
	
	_initConstants(){
		// initialize constants
		this.BASE_EE_LENGTH = 1;
		this.BYTE_TRUE = new Uint8Array([1]);
		this.BYTE_FALSE = new Uint8Array([0]);
	}
}

/** 
 * threadops manage operation that rely on asynchronous threads
 * **/
class Threadops{
	
	constructor(data={}){
		this.operations = [];
		this.maxConcurrentThreads = data.maxConcurrentThreads || 2;
		this.maxThreads = data.maxThreads || 100;
		this.REQUEST_ID_LENGTH = 6;
		this.sleepTime = data.sleepTime || 1000;
	}
	
	////////////////////////////////////////////////////////////////////
	//////////////////    THREAD OPERATIONS    /////////////////////////
	////////////////////////////////////////////////////////////////////
	
	// remove an operation from its id
	removeOperation(id){
		if(this.operations[id]) delete this.operations[id];
	}
	
	// get an operation from its id
	getOperation(id){
		return this.operations[id] || null;
	}

	// get the list of operations
	getOperations(){
		return this.operations.slice();
	}
	
	// add an operation to the list of operations
	addOperation(op={}){
		if(!op.id) op.id = this._generateRequestId();
		this.operations[op.id] = op;
		if(!op.startTime) op.startTime = Date.now();
		return op.id;
	}

	// start a new operation thread, given an operation object
	// return false if it was impossible to start a new thread, true if a new thread was started
	startOperationThread(op){
		if(op.done) return false;
		if(op.currentThreads >= op.maxConcurrentThreads) return false;
		if(op.totalThreads >= op.maxThreads){
			if(!op.currentThreads) this.rejectOperation(op.id, "max threads launched ("+op.totalThreads+") without resolving for the operation : "+op.name);
			return false;
		}
		op.currentThreads++;
		op.totalThreads++;
		
		// create a callback function to update thread counters and start new threads
		let $this = this;
		let cb = function(r){
			$this.finishOperationThread(op);
			$this.startOperationThreads(op);
		}
		// launching the thread
		let p = op.thread(op);
		p.then(cb,cb);

		// return the number of available threads left
		return true;
	}

	// return a promise to sleep : to be used to avoid threads to be chained to quickly
    sleep(t=this.sleepTime){
	    return new Promise(r=>setTimeout(r,t));
    }
	
	// start operation threads until the limit is reached
	startOperationThreads(op){
		// start threads until the number of available threads returned is 0 or if it is unable to start a new thread
		let previousValue = +Infinity;
		let hasSTartedThread;
		do{
            hasSTartedThread = this.startOperationThread(op);
			//if(!threadsLeft || threadsLeft == previousValue) break;
			//else previousValue = threadsLeft;
		}
		while(hasSTartedThread)
	}
	
	// notify an operation thread has ended for the given operation
	finishOperationThread(op){
		op.currentThreads--;
		// if it was the last thread to be terminated, try to resolve at best the
        if(op.totalThreads >= op.maxThreads && op.currentThreads === 0) {
            this.resolveAtBest(op.id);
        }
	}

	// resolve an operation by using the resolveAtBest method of the operation
    // if none is provided, reject the operation
    resolveAtBest(opId){
	    let operation = this.getOperation(opId);
        if(operation && operation.resolveAtBest) return this.resolveOperation(operation.id, operation.resolveAtBest(operation) );
        else return this.rejectOperation(opId )
    }

	// resume an operation
	resumeOperation(op){
        if(!op.done) return;
        delete op.done;
        op.totalThreads = 0;
		return this.startOperation(op);
	}

	// add an operation to the list of operations
	startOperation(op){
		// if no thread method is given to start new threads, throw an exception
		if(!op.thread) throw "to start an operation, expect a 'thread' function in the data to start new threads";
		if(!op.maxConcurrentThreads) op.maxConcurrentThreads = this.maxConcurrentThreads;
		if(!op.maxThreads) op.maxThreads = this.maxThreads;
		if(!op.name) op.name = "";
		// create a timeout to reject/resolved the operation automatically
		if(op.timeout){
			op.timeoutId = setTimeout(function(){
				if(op.done) return;
				this.rejectOperation(op.id, "Timeout without resolving for the operation : "+op.name);
			}.bind(this), op.timeout);
		}
		op.currentThreads = 0;
		op.done = false;
		op.totalThreads = 0;
		op.promise = new Promise((res,rej)=>{
			op.resolve = res;
			op.reject = rej;
		});
		this.addOperation(op);
		this.startOperationThreads(op);
		return op.promise;
	}
	
	// reject the promise of an operation and stop it
	rejectOperation(opId, rejectData){
		let op = this.getOperation(opId);
		if(!op || op.done) return;
		op.reject(rejectData);
		op.done = true;
		this._stopOperation(op);
		this.removeOperation(opId);
	}

	// reject all the current operations
    rejectOperations(){
		for(let operationId in this.operations){
			this.rejectOperation(operationId, "Forced failure : rejectOperation");
		}
	}

	// prevent new threads of an operation from beeing launched
	_stopOperation(op){
		// op.maxConcurrentThreads = -1;
	}
	
	// resolve an operation and stop it
	resolveOperation(opId, resolveData){
		let op = this.getOperation(opId);
		if(!op || op.done){
            return;
		}
		op.resolve(resolveData);
		op.done = true;
		this._stopOperation(op);
		this.removeOperation(opId);
	}
	
	// create a request id
	_generateRequestId(){
		let id = Math.floor(Math.random() * Math.pow(2,this.REQUEST_ID_LENGTH*8));
		return id;
	}
}

// export the class
export {
    Flaptools,
    Threadops
}