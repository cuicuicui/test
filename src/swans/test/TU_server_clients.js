/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

/**
 * Tests for the swanClientCaller swanClientReceiver and swanServer
 * 
 * **/

// CONSTANTS FOR THE TESTING
const SOCKET_SERVER_PORT = 8080;
const HTTP_SERVER_PORT = 80;
const ASYNC_TIMEOUT = 300;

///////////////////// testing modules /////////////////////
let chai = require("chai");
let chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
let assert = chai.assert;

// tested modules
var flocklibs = require("../../../dist/flock-nodejs.js")
Object.assign(global, flocklibs);


describe("creating instance without exception", function(){
	it("should create a swan caller without throwing an exception", function(){
		let caller = new SwanClientCaller();
		assert.isOk(true);
	});
	it("should create a swan receiver without throwing an exception", function(){
		let server = new SwanClientReceiver();
		assert.isOk(true);
	});
});

describe("testing abstract swan methods", function(){

	let aswan = null;

	beforeEach(function(){
		aswan = new AbstractSwan({
			rsaLength : 512,
		});
	});

	it("should create a public key, cypher and decipher", function(){
		// create a new message to encode and decode
		let messageLength = 256;
		let message = new Uint8Array(messageLength);
		for(let i=0; i<messageLength; i++){
			message[i] = 2*i+1;
		}
		
		let publicKey = aswan.getPublicKey();
		assert.instanceOf(publicKey, Uint8Array, "the public key must be a Uint8Array");
		assert.isNotEmpty(publicKey, "the public key must not be empty");
		
		let receiverKey = aswan.getReceiverKey();
		let requestKey = aswan.createRequestKeys(null, receiverKey);
		let encrypted = aswan.encrypt(message, requestKey, receiverKey);
		
		assert.instanceOf( encrypted, Uint8Array, "the encrypted message should be a Uint8Array");
		assert.notDeepEqual(encrypted, message, "encrypted chould not be equal to clear message");
		
		let decrypted = aswan.decrypt(encrypted, requestKey, receiverKey);
		assert.equal( decrypted.length, message.length, "original bytes and result must have same length");
		assert.deepEqual( decrypted, message, "decrypted message should be the original one");
	});

	it("testing createTid method", function(){
		let randomId1 = aswan.createTid();
		let randomId2 = aswan.createTid();
		assert.match(randomId1, /^[a-fA-F0-9]*$/, "ids should be an hexadecimal");
		assert.match(randomId2, /^[a-fA-F0-9]*$/, "ids should be an hexadecimal");
		assert.notDeepEqual(randomId1, randomId2, "ids should not be equals");
	});

    it("should convert back and forth swanPacket", function(){
        let swanObject, swanByteConversion, swanObjectConversion, requestKeys;
        let receiverKey = aswan.getReceiverKey();
        // test with ping messages
        requestKeys = aswan.createRequestKeys(null, receiverKey);

        swanObject = {
            version : 1,
            tid : "1122335599aa",
            type : "ping",
            body : { bid : "554433aaff", body : new Uint8Array([1,2,4,8,16,32,64,128,127,255])},
            receiver :  aswan._flaptools.bytesToHex(requestKeys.receiverKeys.public)
        };
        swanByteConversion = aswan.swanObjectToBytes(swanObject, requestKeys);
        swanObjectConversion = aswan.swanBytesToObject(swanByteConversion, requestKeys);
        if(swanObjectConversion.body.length) delete swanObjectConversion.body.length;
        assert.deepEqual(swanObjectConversion.version, swanObject.version);
        assert.deepEqual(swanObjectConversion.tid, swanObject.tid);
        assert.deepEqual(swanObjectConversion.type, swanObject.type);
        assert.deepEqual(swanObjectConversion.body, swanObject.body);
        assert.deepEqual(swanObjectConversion.receiver, swanObject.receiver);
    });

    it("should return an existing transactionKeys with getTransactionKeys", function(){
		let transactionKeys = aswan.secswans.generateKey(aswan.DEFAULT_ENCRYPTION_TYPE);
		let receiverKeys = aswan.secswans.generateKey(aswan.DEFAULT_ENCRYPTION_TYPE);
		let receiverId = aswan.getReceiverId(receiverKeys);
		let result = aswan.getTransactionKeys(receiverId, [transactionKeys]);
		assert.equal(result, transactionKeys);
	})

	it("should create a new transactionKeys with getTransactionKeys", function(){
        let transactionKeys = aswan.secswans.generateKey(aswan.DEFAULT_ENCRYPTION_TYPE);
        let receiverKeys = aswan.secswans.generateKey(aswan.secswans.ENCTYPE_RSA_1024);
        let receiverId = aswan.getReceiverId(receiverKeys);
        let result = aswan.getTransactionKeys(receiverId, [transactionKeys]);
        assert.notEqual(result, transactionKeys);
	})
});

describe("basic user cases", function(){
	let server, caller, receiver, receiverId, httpServerListen;
	let serverAddress = "http://127.0.0.1";
	let serverRoute = "/swans";
	let serverFullAddress = serverAddress + serverRoute;
	let shiftCounter;
	socketPort = 11112;
	let httpPort = 80;
	let rsaLength = 512;
	let serverSocketBaseAddress = "ws://127.0.0.1";
	let serverSocketAddress = serverSocketBaseAddress+":"+socketPort;
	let socketReceiverInterface = null;
	let httpClientInterface;
	let clientCanals;

	function createWebsocketServer(httpServer){
		let httpLib = require("http");
		let socketioLib = require('socket.io');
		var server = httpLib.createServer(httpServer);
		let socketServer = socketioLib.listen(server);
		server.listen(socketPort);
		return socketServer;
	}
	
	beforeEach(function(){
        this.timeout(8000);
		httpClientInterface = new NIHttpSpawner({ serverPort : httpPort, serverRoute : serverRoute, address : serverAddress });
		clientCanals = httpClientInterface.getCanals();
		// let socketServer = createWebsocketServer(httpClientInterface.core.listen);
		socketReceiverInterface = new NIWebsocketSpawner({socketPort:socketPort, address : serverSocketBaseAddress});
		server = new SwanServer({
								receiverInterface : socketReceiverInterface,
								clientInterface : httpClientInterface,
								registerTimeout : 50,
								receiverTimeout : 50,
								routinePeriod : 50,
								url : serverRoute,
								});
		receiver = new SwanClientReceiver({rsaLength:rsaLength});
        receiverId = "01" + receiver._flaptools.bytesToHex( receiver.getPublicKey() ); // key of type ENCTYPE_DH_2048_256 -> 01
		shiftCounter = 1;
	});
	afterEach(function(){
		server.stop();
		return new Promise((resolve) =>  setTimeout(resolve, ASYNC_TIMEOUT)); // async to let server stop enough time to occur	
	});
	
	// helper to shift a function execution in time
	let shift = function(f){
		setTimeout(f, shiftCounter * ASYNC_TIMEOUT);
		shiftCounter++;
	}

	it("should send a ping to the server and receive a ping answer message through a promise", function(done){
		this.timeout(4000);
		let pingData = {};
		let httpNestedRequest = new NIHttpRequest(serverFullAddress);
        caller = new SwanClientCaller({
            swansServerInterface : httpNestedRequest,
            receiverId : receiverId,
            rsaLength:rsaLength,
        });
		let promise = caller.pingServer(pingData);
		assert.ok( pingData.tid, "the id of the ping request generated should not be null : " + pingData.tid);
		let pingProbe = false;
		promise.then(function(output){
			pingProbe = output;
		});
		shift(function(){
			assert.isNotNull(pingProbe, "an answer should have been received for the ping");
			assert.isObject(pingProbe, "the result of the ping should be received as a swan server object");
			assert.equal( pingProbe.type, "pingAnswer", "the swan message should be a ping answer");
			assert.equal(pingProbe.tid, pingData.tid, "the id of the answer should be the one of the initial request");
			done();
		});
	});

	it("should register a receiver on a server, receive a request from a caller and send back the response to it", function(done){
		let registerResult = false;
		let registerToken = new Uint8Array();
		let swansServerAddress = serverSocketAddress;
		let callbackGeneratedReceiver = false;
		let registerCallback = function(receiver){
            callbackGeneratedReceiver = receiver;
		}
		server.registerCallback(registerCallback);
		let registerPromise = null;
		let requestReceived = null;
		let requestMessage = new Uint8Array([1,2,4,8,16,32]);
		let responseReceived = null;
		let responseMessage = new Uint8Array([64, 128, 192, 224]);
		
		shift(function(){
				registerPromise = new Promise(onregister=>{
					let receiverInterface = new NIWebsocketSpawn({serverAddress : serverSocketAddress});
					receiver.registerInit(receiverInterface, onregister, registerToken);
				});
				registerPromise.then(function(serverMsg){
					registerResult = serverMsg;
				});
		});
		
		// test it has registered
		shift(function(){
			assert.ok(registerResult);
			assert.isObject( registerResult , "the output of the register operation must be a swans server object");
			assert.equal( registerResult.type, "registerAnswer", "the swan message should be a register answer");
			assert.deepEqual( registerResult.canals, clientCanals, "the answer should contain the canals of the swans server client interface");
			assert.property( server.receivers, receiverId, "the receiver must be present in the server receiver list" );
            assert.ok(callbackGeneratedReceiver, "the register callback must have been called")
            assert.ok(callbackGeneratedReceiver.interface, "the register callback must provide the interface of the new receiver")
		});
		
		// send a request from caller to receiver
		shift(function(){
			receiver.setCallback( function(req){
				requestReceived = req;
				return responseMessage;
			});
			let httpNestedRequest = new NIHttpRequest(serverFullAddress);
            caller = new SwanClientCaller({
                swansServerInterface : httpNestedRequest,
                receiverId : receiverId,
                rsaLength:rsaLength,
            });
			let requestPromise;
			try{
                requestPromise = caller.sendRequest( requestMessage);
			}
			catch(e){
				console.log("!!!err ", e, "receiver ???", requestMessage, "done")
			}
			requestPromise.then( function(answer){
				responseReceived = answer.body.body;
			});
		});
		

		// check the input and output of request
		shift(function(){
			assert.deepEqual( requestReceived, requestMessage);
			assert.deepEqual( responseReceived, responseMessage);
			assert.isEmpty( server.waitingPromises, "the swan server must have no pending promise" );
			done();
		});
	});

	it("should should register, send a message to a receiver, forge a signature, and reject the forged message", function(done){
		let requestMessage = new Uint8Array();
		let responseReceived = null;
		let errorReceived = null;
		let responseMessage = new Uint8Array();
		let receiverInterface = new NIWebsocketSpawn({serverAddress: serverSocketAddress});
		receiver.registerInit(receiverInterface);
		receiver.setCallback( function(req){ return responseMessage; });
		let requestData = {};
		
		// send a request from caller to receiver
		shift(function(){
			let httpNestedRequest = new NIHttpRequest(serverFullAddress);
            caller = new SwanClientCaller({
                swansServerInterface : httpNestedRequest,
                receiverId : receiverId,
                rsaLength:rsaLength,
            });
			let requestPromise = caller.sendRequest( requestMessage, requestData);
			requestPromise.then( function(answer){
				responseReceived = responseMessage;
			}).catch(function(err){
				errorReceived = err;
			});
			// try to change manually the requestId to trigger a forge error
			requestData.body.bid = receiver.createBid();
		});

		// check the input and output of request
		shift(function(){
			assert.isNull( responseReceived, "no success response should have been received : expect only an error");
			assert.isNotNull( errorReceived, "expect to receive an error");
			assert.equal( errorReceived.code, receiver.CODE_ERROR_FORGED , "expect the code of error to be explicit");
			done();
		});
	});

	it("should register, send a message to a receiver, then close connection from receiver side", function(done){
		let swansServerAddress = serverSocketAddress;
		let requestMessage = new Uint8Array();
		let responseReceived = null;
		let errorReceived = null;
		let responseMessage = new Uint8Array();
		let receiverInterface = new NIWebsocketSpawn({serverAddress: serverSocketAddress});
		receiver.registerInit(receiverInterface);
		receiver.setCallback( function(req){ return responseMessage; });
		let requestData = {};
		
		// send a request from caller to receiver
		shift(function(){ 
			let httpNestedRequest = new NIHttpRequest(serverFullAddress);
            caller = new SwanClientCaller({
                swansServerInterface : httpNestedRequest,
                receiverId : receiverId,
                rsaLength:rsaLength,
            });
			let requestPromise = caller.sendRequest( requestMessage, requestData);
			requestPromise.then( function(answer){  responseReceived = responseMessage; });
		});
		
		// check the input and output of request
		shift(function(){
			assert.isNotNull( responseReceived, "expect to receive a esponse");
			assert.ok( !!server.getReceiver(receiverId), "the receiver must be registered");
			receiver.close(false); // close the receiver connection to the swans 
		});
		
		shift(function(){
			assert.notOk( receiver.swansInterface.core.connected);
			assert.notOk( !!server.getReceiver(receiverId), "the receiver must not be registered");
			done();
		});
	});


	it("should unregister from server initiative when socket is closed", function(done){
		let swansServerAddress = serverSocketAddress;
		let requestMessage = new Uint8Array();
		let responseReceived = null;
		let errorReceived = null;
		let responseMessage = new Uint8Array();
		let receiverInterface = new NIWebsocketSpawn({serverAddress: serverSocketAddress});
		receiver.registerInit(receiverInterface);
		receiver.setCallback( function(req){ return responseMessage; });
		let requestData = {};
		
		// scheck the server is registered
		shift(function(){ 
			assert.ok( !!server.getReceiver(receiverId), "the receiver must be registered");
			// close the connection 
			receiver.swansInterface.stop();
		});
		
		shift(function(){})
		
		// check the server has removed the receiver
		shift(function(){
			assert.notOk( receiver.swansInterface.core.connected);
			assert.notOk( !!server.getReceiver(receiverId), "the receiver must not be registered");
			done();
		});
	});


	it("should return an error message when trying to contact an unregistered receiver", function(done){
		let swansServerAddress = serverSocketAddress;
		let wrongReceiverId = "01AABB";
		let requestMessage = new Uint8Array();
		let responseReceived = null;
		let errorReceived = null;
		let responseMessage = new Uint8Array();
		
		// send request on unregistered receiver
		shift(function(){ 
			let httpNestedRequest = new NIHttpRequest(serverFullAddress);
            caller = new SwanClientCaller({
                swansServerInterface : httpNestedRequest,
                receiverId : receiverId,
                rsaLength:rsaLength,
            });
			// change the receiver id
			caller.receiverId(wrongReceiverId);
			let requestPromise = caller.sendRequest( requestMessage);
			requestPromise.then( function(answer){ 
				responseReceived = responseMessage;
			})
			.catch(function(err){ errorReceived = err;});
		});
		
		//check an error has been returned
		shift(function(){
			assert.isNull(responseReceived);
			assert.isNotNull(errorReceived);
			done();
		});
		
	});
});
