/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

// abstract class that regroups methods (encryption mainly) for all types of clients
class AbstractSwan {

	// expect to be given a secswans object : if not, try to fetch it itself
	constructor(params = {}){

        // create a flaptools if none is provided
        if(!AbstractSwan.prototype._flaptools){
            AbstractSwan.prototype._flaptools = params.flaptools || new Flaptools();
		}
		// this._flaptools = params.flaptools || new Flaptools();

        // retrieve the cryptico library
        if(!AbstractSwan.prototype.secswans){
            AbstractSwan.prototype.secswans = params.secswans || new Secswans();
        }

        this.initializeAbstractClientConstants();
    }
	 
	////////////////////////////////////////////////////
	///// methods to manage swan packets      //////////
	//////////////////////////////////////////////////// 

	// transform a swan object to a byte array
	// to perform encryption, a transactionKeys object must be provided : it will be completed with missing data if needed
	// if no transactionKeys are provided, the body is not encrypted
	// retruns a bytes sequence
	swanObjectToBytes(swanObj, requestKeys=null){
		// duplicate the object to encode
		let decoded = {};
		for(let i in swanObj) decoded[i] = swanObj[i];
		// encrypt the body if it is present
		if(decoded.body && requestKeys) {
            let decrypted = this._flaptools.encode(decoded.body, this.TEMPLATE_BODY);
			let encrypted = this.encrypt(decrypted, requestKeys);
			decoded.body = encrypted;
        }
		let result = this._flaptools.encode(decoded, this.TEMPLATE_MESSAGE);
		return result;
	}

	// transform a byte packet into an object 
	// if requestKeys is provided, try to decode encrypted body and fill the requestKeys with data found in the packet
	swanBytesToObject( swanBytes, requestKeys = null){
		let result = this._flaptools.decode(swanBytes, this.TEMPLATE_MESSAGE);
        // if request keys are provided, fill it with missing, then decrypt the body
        if(result.body && requestKeys){
        	this.decodeMessage(result, requestKeys);
        }
        return result;
	}

	// decrypt and decode the body of a swan message
	decodeMessage(message, requestKeys){
        this.requestKeysFromTransaction(message, requestKeys);
        let decryptedBody = this.decrypt(message.body , requestKeys);
        message.body = this._flaptools.decode(decryptedBody, this.TEMPLATE_BODY);
	}

	// create a random requestId
	createBid(){
		return this._flaptools.randomHex(this.BID_LENGTH);
	}
	
	// check that the signature of an answer match the signature of a request (same requestId)
	checkSignature( answerObject, match){
		if(typeof match === "object") match = match.bid;
		if(typeof match !== "string") match = this._flaptools.bytesToHex(match);
		let answerSig = answerObject.bid;
		if(!answerSig) return false;
		if(typeof answerSig !== "string") answerSig = this._flaptools.bytesToHex(answerSig);
		return answerSig.toUpperCase() === match.toUpperCase();
	}

	//////////////////////////////////////////
	/////// server layer related methods /////
	//////////////////////////////////////////
	
	// create an error byte packet
	createErrorMessage(params={}){
        if(!params.type || !params.type.startsWith("error")) params.type = "error";
		let packetBytes = this.swanObjectToBytes(params);
		return packetBytes;
	}
	
	// create a ping byte packet
	createPingMessage(params={}){
		if(!params.tid) params.tid = this.createTid();
		params.type = "ping";
		let packetBytes = this.swanObjectToBytes(params);
		return packetBytes;
	}
	
	// create a ping answer byte packet
	createPingAnswerMessage(params={}){
        params.type = "pingAnswer";
		let packetBytes = this.swanObjectToBytes(params);
		return packetBytes;
	}
	
	// create a request answer byte packet
	createResponseMessage(params={}){
		// build the client layer message to send
		if(!params.body ) params.body = {};
        if(!params.body.bid ) params.body.bid = this.createBid();
        if(!params.requestKeys) params.requestKeys = {};
        this.requestKeysFromTransaction(params, params.requestKeys);
        params.type =  "requestAnswer";
		let packetBytes = this.swanObjectToBytes(params, params.requestKeys);
		return packetBytes;
	}

	// create a register byte packet
	createRegisterMessage(params={}){
		params.type = "register";
        if(!params.tid) params.tid = this.createTid();
		let packetBytes = this.swanObjectToBytes(params);
		return packetBytes;
	}
	
	// create a register answer byte packet
	createRegisterAnswerMessage(params={}){
		// build the byteArray containing the ascii url
		params.type = "registerAnswer";
		let packetBytes = this.swanObjectToBytes(params);
		return packetBytes;
	}
	
	// create a close byte packet
	createCloseMessage(params={}){
		params.type = "unregister";
		if(!params.tid) params.tid = this.createTid();
		let packetBytes = this.swanObjectToBytes(params);
		return packetBytes;
	}
	
	// create a request server byte packet
	createRequestMessage(params={}){
		if(!params.body) params.body = {};
		if(!params.body.bid) params.body.bid = this.createBid();
		if(!params.tid) params.tid = this.createTid();
		params.type = "request";
		// build the requestKeys
		if(!params.requestKeys) params.requestKeys = {};
        this.requestKeysFromTransaction(params, params.requestKeys);
        params.key = params.requestKeys.transactionKeys.public;
		let packetBytes = this.swanObjectToBytes(params, params.requestKeys);
		return packetBytes;
	}

	// add missing information to the transaction keys, build from request messages
	requestKeysFromTransaction(data, requestKeys = {}){
        let receiverKeys = data.receiver ? this.createReceiverKey(data.receiver) : requestKeys.receiverKeys;
        let transactionKeys = data.key ? this.secswans.createKey(null, data.key) : requestKeys.transactionKeys;
        // fill missing data in requestKeys
        let res = this.createRequestKeys(transactionKeys, receiverKeys, requestKeys);
		return res;
	}

	// extract a good transaction key from a list of transactionKeys, or create a new one
	getTransactionKeys(receiver, transactionKeysList=[]){
		let receiverKeyType = parseInt( receiver.slice(0,2), 16);
		for(let transactionKeys of transactionKeysList){
			if(transactionKeys.type === receiverKeyType) return transactionKeys;
		}
		SwanClientCaller.prototype.called = SwanClientCaller.prototype.called ? SwanClientCaller.prototype.called + 1 : 1;
		let result = this.secswans.generateKey( receiverKeyType );
		return result
	}

	 // create a server message id randomly
	 // possibilities are considered numerous enough to avoid randomly get the same id twice
	 createTid(){
		return this._flaptools.randomHex(this.TID_LENGTH);
	 }
	
	///////////////////////////////////////////
	/////// Encryption related functions //////
	///////////////////////////////////////////
	
	// generate a new key pair and set it at the identifying key for the receiver
	resetReceiverKey(){
		this._receiverKey = this.createReceiverKey();
	}
	
	// get the public key of the receiver
	getPublicKey(){
		return this.getReceiverKey().public;
	}
	
	// get the rsa key object
	getReceiverKey(){
		if(!this._receiverKey) this.resetReceiverKey();
		return this._receiverKey;
	}
	
	// decrypt a message.
	// some algo like DH make use of a foreign key to decrypt : common key is built from and to tempKey
	// accept only Uint8Arrays as message
	decrypt(encryptedMessage, requestKeys){
        // fetch missing data and fill the requestKeys if needed
        // this.createRequestKeys(requestKeys);
		if(!requestKeys.symetric) requestKeys.symetric = this.secswans.computeSymetricKey(requestKeys.transactionKeys, requestKeys.receiverKeys);
		return this.secswans.symetricDecrypt(encryptedMessage, requestKeys.symetric);
	}
	
	// encrypt a message given the receiver key and the request key
	// accept only Uint8array as messages
	encrypt(message, requestKeys){
		// fetch missing data and fill the requestKeys if needed
		// this.createRequestKeys(requestKeys);
        if(!requestKeys.symetric) requestKeys.symetric = this.secswans.computeSymetricKey(requestKeys.transactionKeys, requestKeys.receiverKeys);
		return this.secswans.symetricEncrypt(message, requestKeys.symetric);
	}
	
	// create a new request key, given a transactionKeys and receiverKeys
	// if the transactionKeys are not defined, get the own receiverKeys if type is the same, or create new ones randomly
	// if the receiverKeys are not defined, take the one of the client
	// create a new object or complete the requestKeys object given
	createRequestKeys(transactionKeys = null, receiverKeys=null, requestKeys = {}){
        requestKeys.receiverKeys = requestKeys.receiverKeys || receiverKeys;
        // if no receiverkeys has been provided, take local receiver keys
        if(!requestKeys.receiverKeys){
        	requestKeys.receiverKeys = this.getReceiverKey();
		}

		requestKeys.transactionKeys =  requestKeys.transactionKeys || transactionKeys;

		if(!requestKeys.transactionKeys){
			// try to add the own receiver key, else build new keys
			let type = requestKeys.receiverKeys.type;
            // requestKeys.transactionKeys = ownReceiver.type == type ?  ownReceiver :  this.secswans.generateKey( requestKeys.receiverKeys.type );
            requestKeys.transactionKeys = this.secswans.generateKey( requestKeys.receiverKeys.type );
        }
		return requestKeys;
	}
	
	// create the string representing the receiver identifier
	// basically, it is the public receiver key in hexadecimal representation, preceded by 2 characters representing the encryption type in hexadecimal
	getReceiverId( receiverKey=null){
		if(!receiverKey) receiverKey = this.getReceiverKey();
		let id = parseInt(receiverKey.type).toString(16);
		while(id.length<2) id = "0"+id;
		id += ( typeof receiverKey.public === "string" ? receiverKey.public : this._flaptools.bytesToHex(receiverKey.public) );
		return id.toLowerCase();
	}
	
	// create a receiver key from scratch (public and private) or deduce it from a receiverId (but only produce public key)
	createReceiverKey(receiverId){
		if(receiverId){
			if(typeof receiverId === "string") receiverId = this._flaptools.hexToBytes(receiverId);
			let type = receiverId[0];
			let pubkey = receiverId.slice(1);
			return this.secswans.createKey(null, pubkey, type);
		}
		else {
			let receiverKey = this.secswans.generateKey(this.DEFAULT_ENCRYPTION_TYPE);
			return receiverKey;
		}
	}

	/////////// HELPERS /////////////////
	
	// initialize constants
	initializeAbstractClientConstants(){
        this.DEFAULT_ENCRYPTION_TYPE = this.secswans.ENCTYPE_DH_2048_256;

        // set default ports
        this.DEFAULT_SERVER_CLIENT_PORT = 53475;
        this.DEFAULT_SERVER_RECEIVER_PORT = 53476
        this.DEFAULT_CLIENT_PORT = 53477;
        this.DEFAULT_RECEIVER_PORT = 53478;

		// size for each element of a swan message
		this.BID_LENGTH = 8;
		this.TID_LENGTH = 6;

		// possible operation types
		this.CODE_PING = 0;
		this.CODE_REQUEST = 1;
		this.CODE_REQUEST_ANSWER = 2;
		this.CODE_REGISTER = 3;
		this.CODE_REGISTER_ANSWER = 4;
		this.CODE_UNREGISTER = 5;
		this.CODE_PING_ANSWER = 6;
		this.CODE_ERROR = 7;
		this.CODE_ERROR_FORGED = 8;
		this.CODE_ERROR_UNREGISTERED_RECEIVER = 9;
		
		// correspondances code => type
		this.CODE_TO_TYPE = {
			[this.CODE_PING] : "ping",
			[this.CODE_REQUEST] : "request",
			[this.CODE_REQUEST_ANSWER] : "requestAnswer",
			[this.CODE_REGISTER] : "register",
			[this.CODE_REGISTER_ANSWER] : "registerAnswer",
			[this.CODE_UNREGISTER] : "unregister",
			[this.CODE_PING_ANSWER] : "pingAnswer",
			[this.CODE_ERROR] : "error",
			[this.CODE_ERROR_FORGED] : "errorForged",
			[this.CODE_ERROR_UNREGISTERED_RECEIVER] : "errorUnregisteredReceiver",
		}
		
		// correspondance type => code
		this.TYPE_TO_CODE = {};
		for(let i in this.CODE_TO_TYPE) this.TYPE_TO_CODE[ this.CODE_TO_TYPE[i] ] = i;
		
		// initialize body template
        let ee_b_version = { n : "version", t : "int", c : 0};
        let ee_b_length = { n : "length", t : "int", c : 1, endMacro : "$length", de : 0};
        let ee_b_bid = { n : "bid", t : "hex", c : 2 };
        let ee_b_body = { n : "body", t : "bin", c : 3 };
        let ee_body = [ee_b_version, ee_b_length, ee_b_bid, ee_b_body];
        this.TEMPLATE_BODY = { ee : {}, maxLength : "length" };
        for(let e of ee_body) this.TEMPLATE_BODY.ee[e.n] = e;

		// initialize message template
        let ee_m_version = { n : "version", t : "int", c : 0, dd : 0};
        let ee_m_length = { n : "length", t : "int", c : 1};
        let ee_m_type = { n : "type", t : "int", c : 2, r : true, rpl : this.CODE_TO_TYPE };
        let ee_m_tid = { n : "tid", t : "hex", c : 3, r : true };
        let ee_m_receiver = { n : "receiver", t : "hex", c : 4 };
        let ee_m_key = { n : "key", t : "bin", c : 5 };
        let ee_m_body = { n : "body", t : "bin", c : 6 };
        let ee_m_authkey = { n : "authkey", t : "hex", c : 7 };
        //let ee_m_contact = { n : "contact", t : "ascii", c : 8 };
        let ee_m_contact = {n:"canals", t:"arr", c:8, tpl : NestedInterface.prototype.TEMPLATE_CANAL };



		let ee_m_info = { n : "info", t : "ascii", c : 9 };
        let ee_message = [ee_m_version, ee_m_length, ee_m_type, ee_m_tid, ee_m_receiver, ee_m_key, ee_m_body, ee_m_authkey, ee_m_contact, ee_m_info];
        this.TEMPLATE_MESSAGE = { ee : {} };
        for(let e of ee_message) this.TEMPLATE_MESSAGE.ee[e.n] = e;
	}

    // getter for the socket-io.client lib
	getSocketioClientLib(){
		// retrieve the socket.io client library
		if(!this.socketioClientLib) this.socketioClientLib = require("socket.io-client");
		return this.socketioClientLib;
	}
}

// implementation of the swan receiver client
// it registers to the swan server by giving a public rsa encryption key and wait for the messages
class SwanClientReceiver extends AbstractSwan{
	/***
	 * possible params : 
	 * server : the address of the swan server
	 * token : a registration token (in the form of a Uint8Array) to send to the swans server to enable registration
	 * onsuccess : a callback to be triggered once the registration occured (called with a swan server object as parameter)
	 * ****/
	constructor(params){
		if(!(params instanceof Object && params)) params = {};
		super(params);
		
		// initialize various constants
		this.initializeConstants();
		
		// initialize the swan server address
		this.swansInterface = null;

		// swans server port for nested interface
		this.swansServerPort = this.swansServerPort || this.DEFAULT_SERVER_RECEIVER_PORT;

		// receiver port for nested interface
		this.port = params.port || this.DEFAULT_RECEIVER_PORT;
		
		// at first, the server is stopped
		this.status = this.STATUS_STOP;
	}
	
	// start the server connection
	registerInit(swansInterface, onregister, registerToken){
		let swClient = this;
		onregister = onregister || function(){};
		this.status = this.STATUS_STARTING;
		this.swansInterface = swansInterface;
		this._firstMessage = true; // variable to check the registration result differently
		// send a message for registration
		let registerMessage = this.registerMessage(registerToken);
		if(swansInterface.status() === "UP"){
			swansInterface.send(registerMessage, this.swansServerPort, this.port);
		}
		else{
			swansInterface.addHook(swansInterface.e.CONNECT, function(){
				swansInterface.send(registerMessage, swClient.swansServerPort, swClient.port);
			});
		}
		// set connection event
		this.swansInterface.addHook(this.swansInterface.e.CONNECT, function(){
			swClient.status = swClient.STATUS_RUNNING;
		});
		
		// set message event
		this.swansInterface.addHook(this.swansInterface.e.RECEIVE, function(event){
			let message = event.message;
			// decode the message
			let requestKeys = { receiverKeys : swClient.getReceiverKey() };
			let packetObject = swClient.swanBytesToObject(message, requestKeys);
			// the first message received is the result of the register operation
			if(swClient._firstMessage){
				swClient._firstMessage = false;
				return swClient.processRegisterAnswer(packetObject, onregister);
			}
			// when receiving ping, send a ping answer
			switch(packetObject.type){
				case "ping" : return swClient._processPing(packetObject);
				case "request" : swClient._processRequest(packetObject, requestKeys, event);
			}
		},
			// use the receiver port
			this.port
		);
		
		// set error event
		this.swansInterface.addHook(this.swansInterface.e.ERROR, function(){
			swClient.status = this.STATUS_ERROR;
		});
	}
	
	// process incoming ping message
	_processPing(packetObject){
		let response = swClient.createPingAnswerMessage({
			tid : packetObject.tid,
		});
		this.sendArrayMessage(response);
	}
	
	// process incoming request :
	// packetObject : the swan object to process
	// requestKeys : all the necessary data about how to encrypt the answer
	_processRequest(packetObject, requestKeys, event){
		// if an answer is provided for requests, send it back
		let answer = this.callback ? this.callback( (packetObject.body||{}).body, event ) : null;
		if(!(answer instanceof Promise)) answer = Promise.resolve(answer);
		// send back the request result
		let client = this;
		answer.then( function(answerValue){
			let response = client.createResponseMessage({
				tid : packetObject.tid,
				body : {
					body : answerValue || new Uint8Array(),
					bid : packetObject.body && packetObject.body.bid ? packetObject.body.bid : client.createBid()
				},
                requestKeys : requestKeys  // set the key to encrypt the answer
			});
			client.sendArrayMessage(response);
		});
	}
		
	// create a register message to send to the swans server
	registerMessage(authkey){
		let data = {
            receiver : this.getReceiverId()
        };
		if(authkey) data.authkey = authkey;
		let msg = this.createRegisterMessage(data);
		return msg;
	}
	
	// close the connection with the swan server
	close(stopInterface=true){
		this.status = this.STATUS_STOP;
		let closeMessage = this.createCloseMessage();
		this.sendArrayMessage(closeMessage);
		if(stopInterface && this.swansInterface) this.swansInterface.stop();
	}
	
	// process the register answer : create a contact url associated
	processRegisterAnswer(serverMessageObject, onregister){
		if(serverMessageObject.type.startsWith("error")){
			this.close();
			this.status = this.STATUS_ERROR;
			return;
		}
		else{
			// store the receiver canals
			this.serverCanals = serverMessageObject.canals;
		}
		onregister(serverMessageObject);
		return;
	}
	
	// get the status of the link with the swans server
	getStatus(){
		return this.status;
	}
	
	// send a message in server layer protocol format to the server
	sendArrayMessage(byteMessage){
		if(!this.swansInterface || this.swansInterface.status() !== "UP") return false;
		this.swansInterface.send(byteMessage, this.swansServerPort, this.port);
		return true;
	}
	
	// add a callback to execute upon message reception
	setCallback(f){
		if(f && {}.toString.call(f) === '[object Function]'){
			this.callback = f;
			return true;
		}
		else return false;
	}
	 
	// remove all the current callbacks
	clearCallback(){
		this.callback = null;
	} 
	
	// get the list of callbacks
	getCallback(){
		return this.callback; 
	}
	
	initializeConstants(){
		// set various possible status for receiver client 
		this.STATUS_UNDEFINED = "undefined" // parameters about the connection has not been given
		this.STATUS_STARTING = "starting"; // when connection is beeing established without result yet
		this.STATUS_STOP = "stop"; // the connexion has not been started or has been stoped 
		this.STATUS_RUNNING = "running"; // the connection is going well 
		this.STATUS_ERROR = "error"; // the connection is stopped because of an error
	}
}

// implementation of the swan client caller : 
// it contacts a swans server to transmit messages to a swan client receiver
class SwanClientCaller extends AbstractSwan {
	constructor(params={}){
		super(params);
		// must provide a swans interface where to send the messages
		this.swansServerInterface = params.swansServerInterface;
		// must provide a receiverId if request are to be made to a swans receiver
		this.receiverId(params.receiverId);
		// might provide ports where to send request
		this.port = params.port || this.DEFAULT_SERVER_CLIENT_PORT;
		// might provide a port from which to send messages
		this.fport = params.fport || this.DEFAULT_CLIENT_PORT;

		// initialize the hooks to execute callback when receiving request answers
		if(this.swansServerInterface) this.initHooks(this.swansServerInterface);

		// create a list of callbacks to be called, indexed by a tid
		this._callbacks = {};

		// keep a list of transaction keys ot use instead of generating new ones
		this._transactionKeysList = params.transactionKeysList || [];

		// remember of the pair of keys created by the caller for communication : it is initialized during the first request
		this._requestKeys = {}; // tid => requestKeys
	}

	// reset the receiver id if a value is provided, then return it
	receiverId(v){
		if(v) this._receiverId = v;
		return this._receiverId;
	}

	// get/set the list of transactionKeys to be used by default
	transactionKeysList(trk=null){
		if(trk) this._transactionKeysList = trk;
		return this._transactionKeysList;
	}

	// close the caller and all associated interface
	close(){
		if(this.swansServerInterface) this.swansServerInterface.stop();
	}

	// send a ping to a swan server : return the promise associated 
	// modify pingData in place to add missing packet parameters
	pingServer( options={}){
		// create the ping byte message (keep or create a tid)
		let pingData={ tid : options.tid };
		let pingMessage = this.createPingMessage(pingData);
		options.tid = pingData.tid;
		
		// create the packet to be sent
		let packet = {
			message : pingMessage,
			port : this.port,
			fport : this.fport
		};
		let client = this;
		let promise = new Promise((onsuccess, onerror)=>{
				client.sendMessage( packet, pingData.tid, onsuccess, onerror);
		});
		return promise;
	}
	
	// send a request message to a given receiver
	// bodyContent : the byte to send as the body of the swan request message
	// return a promise wich resolve when data is received about the request
	sendRequest(bodyContent, requestData={}){
		// create the server message
		requestData.body = { body : bodyContent };
		requestData.receiver = this.receiverId();
		// get a transaction key to use, add it if a new one is generated
		let transactionKeys = this.getTransactionKeys(requestData.receiver, this.transactionKeysList());
        requestData.requestKeys = { transactionKeys : transactionKeys }
		let serverMessage = this.createRequestMessage( requestData );
		this._requestKeys[requestData.tid] = requestData.requestKeys;
		let swCaller = this;

		// create the packet to be sent
		let packet = {
			message : serverMessage,
			port : this.port,
			fport : this.fport
		}

		let promise = new Promise(function (onsuccess, onerror){
			// when receiving a server result, decrypt the client message inside it
			let checkSignatureOnSuccess = function(swanPacket){
				// in case of error, return the server message
				if( swanPacket.type !== "requestAnswer"){ 
					onerror(swanPacket); 
					return; 
				} 
				// check the signature
				let signatureCorrect = swCaller.checkSignature( swanPacket.body, requestData.body);
				if(signatureCorrect){
                    if(swCaller._requestKeys[swanPacket.tid]) delete swCaller._requestKeys[swanPacket.tid];
                    onsuccess( swanPacket );
				}
				else onerror({code:swCaller.CODE_ERROR_FORGED, message: "swans client answer reading : incorrect signature"});
			}
			swCaller.sendMessage(packet, requestData.tid, checkSignatureOnSuccess, onerror);
		});
		return promise;
	}
	
	// send a message given through a swan server address
	sendMessage(packet, tid, onsuccess, onerror){
        // save the callbacks for later
        if(tid){
            this._callbacks[tid] = {
                onsuccess : onsuccess,
                onerror : onerror
            }
        }
		this.swansServerInterface.send(packet.message, packet.port, packet.fport);
	}

	// initialize the nested interface hooks
	initHooks(ni){
		let swclient = this;

		// when receiving a new message, check if the tid match a request tid : if so, call the approprate callback if it exists
        ni.addHook(ni.e.RECEIVE, function(event){
            let byteResult = event.message;
            let packetResult = swclient.swanBytesToObject( byteResult);
            let tid = packetResult.tid;
            // decode body if needed
            if(swclient._requestKeys[tid] && packetResult.body) {
                swclient.decodeMessage(packetResult, swclient._requestKeys[tid])
            }
            if(!swclient._callbacks[tid] ) return;
            let onsuccess = swclient._callbacks[tid].onsuccess;
            delete swclient._callbacks[tid];
            if({}.toString.call(onsuccess) === '[object Function]') onsuccess(packetResult);
        },
			this.DEFAULT_CLIENT_PORT
		);
	}

}



// try to export the class for nodejs environment
export {
    SwanClientReceiver,
    SwanClientCaller,
    AbstractSwan
}