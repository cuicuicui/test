/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

/***
 * SWANS stands sor "Signaling With ANonymous Server"
 * it defines a protocol to send message from two client separated by a server that might not be reliable
 * it uses ssl encryption to preserve the integrity of the data transmitted
 * a SwanClient register to a SwanServer by providing the server a ssl public key 
 * this establishes a connection between the client and the server. 
 * the server in return provide a url to contact the client through the server : in particular, the url contains an identifier, which is the public ssl key
 * another client uses the url to send encrypted data to the SwanServer, which retransmit it to the first swan client
***/

import {AbstractSwan} from "./SwanClient.js"

// server class to implement the swan protocol
class SwanServer extends AbstractSwan{

	/** create a new swanServer and start it
	 * it relies on express so it should be available with require
	 * constructor needs parameters :
	 * 		clientInterface :  nested interface that receives requests that are transfered to receivers
	 *      receiverInterface : interface used to register and create new connections with receivers
	 *      url  :  if not provided, the receiver url is the root url
	 * 		requestTimeout : if no answer is received for a request after that time, send a server message error (in ms)
	 * 		receiverTimeout : if no message has been received from receivers after that time, close the receiver (in ms)
	 * 		routinePeriod : frequence at which the server perform tasks like checking if the receivers or requests have expired
	 *      port : port used for listening to remote messages
	***/
	constructor(params = {}){
		super(params);

		// initialize constants to be used for the api
		this.initializeConstants();

		this.clientInterface = params.clientInterface;
		this.receiverInterface = params.receiverInterface;
		this.url = params.url || null;
		this.requestTimeout = params.requestTimeout || this.DEFAULT_REQUEST_TIMEOUT;
		this.registerTimeout = params.registerTimeout || this.DEFAULT_REGISTER_TIMEOUT;
		this.receiverTimeout = params.receiverTimeout || this.DEFAULT_RECEIVER_TIMEOUT;
		this.routinePeriod = params.routinePeriod || this.DEFAULT_ROUTINE_PERIOD;
		// this.port = params.port || this.DEFAULT_SERVER_PORT;
		this.clientPort = params.clientPort || this.DEFAULT_SERVER_CLIENT_PORT;
		this.receiverPort = params.receiverPort || this.DEFAULT_SERVER_RECEIVER_PORT;
		// create a list of waiting promises : once an answer is received, the promise is resolved
		this.initializeWaitingPromises();

		this.started = false;

		// create and start the server with those settings
		this.start();
	}

	////////////////////////////////////////////////////////
	/////// GENERAL METHODS TO HANDLE THE SWAN SERVER //////
	////////////////////////////////////////////////////////

	// start the swan server
	start(){
		this.started = true;
		// start the receiver and client interfaces
		this.startClientInterface();
		this.startReceiverInterface();

		// create a routine to check for pending promises / inactive receivers
		let $this = this;
		this.routine = setInterval(function(){
            $this.checkPendingRequests();
            $this.checkReceiverConnections();
		}, this.routinePeriod);
	}

	// stop the swan server
	stop(){
        this.started = false;
		this.stopClientInterface();
		this.stopReceiverInterface();

		// stop the check routine
		clearInterval(this.routine);
	}

	startReceiverInterface(){
		// list of the receiver connections
		// the key is the identifier of the receiver (the public rsa key written in base 64)
		// property interface contains a nestedInterface object representing the connection
		// property lastAlive contains the last time the receiver made contact
		this.receivers = {};

		this.initializeReceiverInterface();
	}

	stopReceiverInterface(){
		// close the main server
		if(this.receiverInterface) this.receiverInterface.stop();

		// close all the sockets
		for(let key in this.receivers){
			this.closeReceiver(key);
		}
	}

    /**
	 * add a callback to be launched when a registration occurs
     */
    registerCallback(c){
    	if(c) this._registerCallback = c;
    	return this._registerCallback;
	}

	initializeReceiverInterface(){
		let swServer = this;
		let spawnHook = function (event) {
			let spawnInterface = event.spawn;
            spawnInterface.test = "serverReceiverspawn"
			let receiverKey = null;
			// initialize events to receive and process messages
			spawnInterface.addHook(spawnInterface.e.RECEIVE, function (event) {
			    let t1 = Date.now();
				let data = new Uint8Array(event.message);
				let swanServerMessage = swServer.swanBytesToObject(data, null, false);
				// processing register messages
				if(swanServerMessage.type === "register"){
					receiverKey = swanServerMessage.receiver;
					// check if this receiver can register
					if( !swServer.canRegister( receiverKey , swanServerMessage.body) ){
						let registerAnswerObject = {
							id : swanServerMessage.tid,
							info : swServer.ERROR_DESCRIPTION_UNAUTHORIZED
						}
						let registrationErrorMessage = swServer.createErrorMessage( registerAnswerObject );
						spawnInterface.send(registrationErrorMessage, swServer.flockPort, event.fport);
					}
					else{
						// send to the receiver an url to be contacted through the server
						let registerAnswerMessage = swServer.createRegisterAnswerMessage({
							tid : swanServerMessage.tid,
							canals : swServer.clientInterface.getCanals()//  swServer.createReceiverUrl()
						});

						// register the receiver
						swServer.registerReceiver( receiverKey, spawnInterface, event.fport );
						// send register answer
						swServer.sendMessageToReceiver( receiverKey, registerAnswerMessage);
					}
					return;
				}

				// processing any other type of message
				let params = {
					fromReceiver : receiverKey,
					tid : swanServerMessage.tid
				};
				let requestResultPromise = swServer.processRequest(swanServerMessage, params);
				if( !requestResultPromise) return; // some request don't wait for any response to be given
				requestResultPromise.then(function(swanServerAnswerMessage){
					if(!swanServerAnswerMessage) return;
					// if the message is not a Uint8array, it is a normal object -> convert it into bytes
					if( !(swanServerAnswerMessage instanceof Uint8Array) ){
						swanServerAnswerMessage = swServer.swanObjectToBytes( swanServerAnswerMessage, false );
					}
					swServer.sendMessageToReceiver( receiverKey, swanServerAnswerMessage);
				});

			},
			swServer.receiverPort);
		}

		this.receiverInterface.addHook( this.receiverInterface.e.SPAWN, spawnHook);
	}

	// hook for spawn event on swans server interfaces
	spawnEvent(){
	}

	// start the http server
	startClientInterface(){
		// stop previous server just in case
		this.initializeClientInterface();
	}

	// stop the http server
	stopClientInterface(){
		if(!this.clientInterface) return;
		this.clientInterface.stop();
	}

	// initialize a client interface
	// on spawn, it checks if a message is attached to the event, if so, it tries to send back an answer
	initializeClientInterface(){
		var swServer = this;

		var spawnHook = function(event){
		  let spawn = event.spawn;
		  spawn.test = "serverClientSpawn";
		  let byteMessage = event.message;

		  // add a hook to process received messages
		  let receiveHook = function(event){
		  	  if(!swServer.started) return;
              let byteMessage = event.message

              // transform the request object into a swan layer server
              let swanServerMessage = swServer.swanBytesToObject( byteMessage, null, false );
              let processPromise = swServer.processRequest(swanServerMessage);
              // for requests that don't return a promise, don't do anything
              if(!processPromise) return;
              processPromise.then(function(swanServerAnswerMessage){
                  if(!swServer.started) return;
                  if(!swanServerAnswerMessage) return;
                  // if the message is not a Uint8array, it is a normal object -> convert it into bytes
                  if( !(swanServerAnswerMessage instanceof Uint8Array)){
                      swanServerAnswerMessage = swServer.swanObjectToBytes( swanServerAnswerMessage, false );
                  }
                  if(spawn.status() !== "UP"){
                      console.log("WARNING : swans Client disconnected, unable to proceed", JSON.stringify(swanServerMessage) );
                      return;
				  }
                  spawn.send( swanServerAnswerMessage, event.fport, swServer.clientPort );
                  // TODO : UNCOMMENT
                  //spawn.close();
              })
			  // .catch(function(e){
				// console.log("ERROR DURING SWANS SERVER REQUEST EXECUTION ", e);
				// spawn.close();
			  // })

		  }

		  spawn.addHook(spawn.e.RECEIVE, receiveHook, swServer.clientPort);
		  // in some interface, a message is sent during the connection initialization
		  if(event.message) spawn.triggerReceive(event);
		};

		// link the route to the request processing
		this.clientInterface.addHook(this.clientInterface.e.SPAWN, spawnHook);
	}

	////////////////////////////////////////////////////
	//////////  METHODS TO HANDLE RECEIVERS ////////////
	////////////////////////////////////////////////////

	// check if a given client can register to the swanServer
	// this function should be overwritten to match specific needs :
	// by default, it counts the number of active connections and only open a new one if there is some room left
	// receiver is the hexadecimal receiver key to register
	// request is an Uint8Array
	canRegister( receiver, request ){
		// a key must be present to identify the caller : it is generally a public rsa key
		if(!( receiver && typeof receiver === "string") ) return false;

		// cannot register the same caller twice
		if( receiver in this.receivers ) return false;

		// everything is ok
		return true;
	}

	// send a message to a receiver
	sendMessageToReceiver( receiverId, byteMessage){
		if(!this.isRegistered(receiverId)){
			throw "Unknown receiver : "+receiverId;
		}
		let rec = this.receivers[receiverId];
		if(!rec.interface){
			console.log("ERROR : CANNOT sendMessageToReceiver without interface");
			return;
		}
		if(!rec.interface.status() === "UP"){
            console.log("ERROR : CANNOT sendMessageToReceiver without UP interface");
            return;
		}
		rec.interface.send( byteMessage, rec.port, this.flockPort );
	}

	// check the socket.io connections state : remove dead ones
	checkReceiverConnections(){
		for(let key in this.receivers){
			// check the last update of the receiver
			if(this.receivers[key].interface.status() === "UP") this.receivers[key].lastAlive = Date.now();
			let isExpired = Date.now() - this.receivers[key].lastAlive > this.receiverTimeout;
			if(isExpired){
				this.closeReceiver(key);
			}
		}
	}

	// check pending requests : remove the expired ones by resolving them with errors
	checkPendingRequests(){
		for(let key in this.waitingPromises){
			let isExpired = Date.now() - this.waitingPromises[key].time > this.requestTimeout;
			if(isExpired){
				let swanTimeoutMessage = this.createErrorMessage({
					tid : this.waitingPromises[key].originalId,
					info : this.ERROR_DESCRIPTION_TIMEOUT
				});
				this.resolveWaitingPromise( key, this.waitingPromises[key].receiverKey, swanTimeoutMessage)
			}
		}
	}

	// check if an id is a registered receiver
	isRegistered(id){
		return id in this.receivers;
	}

	// register a new receiver
	registerReceiver(key, nestedInterface, port){
		this.receivers[key] = {
			interface : nestedInterface,
			port : port,
			key : key
		};
		this.updateReceiverLastAlive(key);
		let $this = this;
		nestedInterface.addHook(nestedInterface.e.DISCONNECT, function(){
            delete $this.receivers[key];
		});
		if(this._registerCallback) this._registerCallback(this.receivers[key]);
	}

	// retrieve a receiver
	getReceiver(id){
		return this.receivers[id];
	}

	// update the lastAlive date of a receiver
	updateReceiverLastAlive(receiverKey){
		let receiver = this.getReceiver(receiverKey);
		if(receiver) receiver.lastAlive = Date.now();
	}

	// close a receiver connection
	closeReceiver(key){
		if(!(key in this.receivers)) return;
		this.receivers[key].interface.stop();
		delete this.receivers[key];
	}

	////////////////////////////////////////////////////
	///// methods to manage swan server layer //////////
	////////////////////////////////////////////////////

	// create a contact url to send request to a registered client
	createReceiverUrl(){
		return this.url || "";
	}

	/*** process swan server message
	     params might define :
			fromReceiver : the id of the receiver sending the request
			promiseId : the id of the promise to resolve once an answer is retrieved
	***/
	processRequest(swanServerMessage, params = {}){

		// remember the receiver is still alive
		if(params.fromReceiver) this.updateReceiverLastAlive(params.fromReceiver);
		switch( swanServerMessage.type ){
			case "ping" :
				let swanPingAnswerMessage = this.createPingAnswerMessage({
					tid : swanServerMessage.tid
				});
				return Promise.resolve(swanPingAnswerMessage);
				break;
			case "request" :
				return this._processRequestServerMessage(swanServerMessage, params);
				break;
			case "requestAnswer" :
				return this._processResponseServerMessage(swanServerMessage, params);
				break;
			case "unregister" :
				return this._processCloseServerMessage(swanServerMessage, params);
				break;
			// registration, registrationAnswer message are not expected
			case "register" :
			case "registerAnswer" :
			// no special treatment when receiving ping answers or errors
			case "pingAnswer" :
			case "error" :
			// by default, dont do anything either
			default :
				return null;
				break;
		}
	}

	// process a close request
	// only valid if it comes from a receiver
	_processCloseServerMessage(swanRequestMessage, params){
		if(!params.fromReceiver) return;
		this.closeReceiver(params.fromReceiver);
	}

	// process a response given as a swan server layer object
	// try to find the matching request, and forward the answer to the caller
	_processResponseServerMessage( swanRequestMessage, params){
		// rename the tid if an alias is found
		swanRequestMessage.tid = this.promiseAlias( swanRequestMessage.tid, params.fromReceiver);
		// let byteMessage = this.swanObjectToBytes(swanRequestMessage, false);
		// find out if there is a waiting promise, and resolve it
		this.resolveWaitingPromise(params.tid, params.fromReceiver, swanRequestMessage);
	}

	// process a request given as a swan server layer object
	// try to find a matching receiver, forward the message and wait for an answer
	_processRequestServerMessage( swanRequestMessage, params){
		// check if the caller is registered
		let targetReceiver = swanRequestMessage.receiver;
		if(!this.isRegistered(targetReceiver)){
			let errorMessage = this.createErrorMessage({
				tid : swanRequestMessage.tid, // this.promiseAlias( params.promiseId, params.fromReceiver),
				info : "unknown receiver",
                type : this.CODE_TO_TYPE[this.CODE_ERROR_UNREGISTERED_RECEIVER]
			});
			return Promise.resolve(errorMessage);
		}
		let receiver = this.getReceiver(targetReceiver);
		// store a promise to be resolved when receiving an answer
		let originalTid = swanRequestMessage.tid;
		let tidAlias = this.createTid(); // id that will be sent to the caller
		let resolver;
		let callbackPromise = new Promise(res=>resolver=res);
		this.addWaitingPromise(tidAlias, resolver, targetReceiver, originalTid);
		// forward the request to the receiver
		swanRequestMessage.tid = tidAlias;
		let byteMessage = this.swanObjectToBytes(swanRequestMessage, false);
		this.sendMessageToReceiver( targetReceiver, byteMessage);
		// return the promise
		return callbackPromise;
	}

	// create a list of waiting promises : once an answer is received, the promise is resolved
	// key : a swan server message id
	// promise : the promise to resolve, with a byte message
	// receiverKey* : the key of the receiver if it is associated to it
	// originalId* : the id of the message when the request was received (since the server may create aliases when forwarding messages)
	// time : the ms time at which the promise was set as pending
	initializeWaitingPromises(){
		this.waitingPromises = {};
	}

	// add a promise to the list of waiting promises
	addWaitingPromise(tid, resolver, receiverKey=null, originalId=null){
		this.waitingPromises[tid] = {
			resolver : resolver,
			receiverKey : receiverKey,
			originalId : originalId,
			time : Date.now(),
		}
	}

	// check if there is a waiting promise matching
	isWaitingPromise(tid, receiverKey=null){
		return !!(this.waitingPromises[tid] && this.waitingPromises[tid].receiverKey == receiverKey);
	}

	// retrieve a given promise
	retrieveWaitingPromise(tid, receiverKey=null){
		return this.waitingPromises[tid] && this.waitingPromises[tid].receiverKey == receiverKey
		? this.waitingPromises[tid] : null;
	}

	// remove a waiting promise
	removeWaitingPromise(tid, receiverId){
		if(this.waitingPromises[tid] && this.waitingPromises[tid].receiverKey == receiverId){
			delete this.waitingPromises[tid];
		}
	}

	// retrieve the original request Id for a given waiting receiver/requestAlias
	promiseAlias( messageAlias, receiverKey){
		return this.waitingPromises[messageAlias] && this.waitingPromises[messageAlias].receiverKey == receiverKey ?
			   this.waitingPromises[messageAlias].originalId : null;
	}

	// resolve a waiting promise with a given swan server message object
	// don't do anything if no matching promise is found
	resolveWaitingPromise( tid, receiverId, swanServerMessage){
		let waitingPromise = this.retrieveWaitingPromise(tid, receiverId);
		if(!waitingPromise) return;
		waitingPromise.resolver( swanServerMessage );
		this.removeWaitingPromise(tid, receiverId);
	}

	////////////////////////////////////////////////////
	//////////   HELPERS ///////////////////////////////
	////////////////////////////////////////////////////

	initializeConstants(){
        this.DEFAULT_REQUEST_TIMEOUT = 120000;
		this.DEFAULT_RECEIVER_TIMEOUT = 10000;
		this.DEFAULT_REGISTER_TIMEOUT = 10000;
		this.DEFAULT_ROUTINE_PERIOD = 8000;
		this.ERROR_DESCRIPTION_TIMEOUT = "swans timeout";
		this.ERROR_DESCRIPTION_UNAUTHORIZED = "unauthorized";
	}
} 

export {
    SwanServer,
}