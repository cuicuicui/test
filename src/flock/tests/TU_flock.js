/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

/**
 * Tests for the Flock.js
 * 
 * **/

// CONSTANTS FOR THE TESTING
const ASYNC_TIMEOUT = 300; // time for asynchronous test to occur


// HELPERS

// helper to shift a function execution in time
let shiftCounter = 0;
let shift = function(f, n=1){
	setTimeout(f, (n - 1 + shiftCounter) * ASYNC_TIMEOUT);
	shiftCounter += n;
}
	
///////////////////// DATA THAT MUST BE IMPLEMENTED FOR THE UNIT TESTING /////////////////////
var nestedInterfaceTestHelpers = {};

////////////////////////////////////

let assert = chai.assert;

describe("creating a Flock", function() {
    it("should just create a new Flock without error", function() {
		let fl = new Flock();
		assert.ok(true);
		fl.stop();
    });
});

describe("testing various aux functions for the Flock", function(){
	let fl = null;
	
	beforeEach(function(){
		fl = new Flock();
	});
	
	afterEach(function(){
		fl.stop();
	});
	
	it("testing the getKey method", function(){
		// getKey should return a copy of the Uint8Array key property
		let result = fl.getKeys();
		assert.isArray(result, "the keys should be an array of Object");
		assert.isString(result[0].prefix,"the key prefix should be a string");
		
		let minimumExpectedLength = 31;
		assert.isAbove(result[0].prefix.length, 2*minimumExpectedLength, "the key must not be to small");
		
		assert.deepEqual(result[0], fl.keys[0], "the getKey must return a copy of the original key, not a different key");
	});

	it("testing addKey, hasKey, removeKey methods", function(){
		let testKey = {prefix : "00000000", span : 0};
		let hasKey, allKeys, lastKey;

		hasKey = fl.hasKey(testKey);
		assert.notOk(hasKey, "testKey has not been pushed yet");

		fl.addKey(testKey);
        hasKey = fl.hasKey(testKey);
        assert.ok(hasKey, "testKey has been pushed");
        allKeys = fl.getKeys();
        lastKey = allKeys[allKeys.length-1];
        assert.deepEqual(lastKey, testKey);
        let keysLength = allKeys.length;

        fl.addKey(testKey);
        allKeys = fl.getKeys();
        assert.lengthOf(allKeys, keysLength, "should not be able to add the same key twice");

        fl.removeKey(testKey);
        hasKey = fl.hasKey(testKey);
        assert.notOk(hasKey, "testKey has been removed");
        allKeys = fl.getKeys();
        lastKey = allKeys[allKeys.length-1];
        assert.notDeepEqual(lastKey, testKey);
	})

	it("testing the getId method", function(){
		// getKey should return a copy of the Uint8Array key property
		let result = fl.getId();
		assert.isString(result, "the id should be a random hexadecimal");
		assert.isNotEmpty(result, "the id must not be null");
	});
	
	it("should generate a random request id", function(){
		let id1 = fl._generateRequestId();
		let id2 = fl._generateRequestId();
		assert.isString(id1);
		assert.isString(id2);
		assert.notEqual(id1, "");
		assert.notEqual(id2, "");
		assert.notEqual(id1, id2);
	});
	
	it("should test the formation management methods : getFormations, addFormation, removeFormation", function(){
		let formations;
		let f1 = new Formation();
		let f2 = new Formation();
		let f3 = new Formation();
		
		// try getting formations
		formations = fl.getFormations();
		assert.isArray(formations);
		assert.lengthOf(formations, 0);
		
		// try adding formations
		fl.addFormation(f1);
		fl.addFormation(f2);
		fl.addFormation(f3);
		fl.addFormation({});
		formations = fl.getFormations();
		assert.isArray(formations);
		assert.lengthOf(formations, 3, "should add the 3 forùmations, but not the object that is not a formation");
		assert.equal(formations[0], f1);
		assert.equal(formations[1], f2);
		assert.equal(formations[2], f3);
		
		// try removing formations
		fl.removeFormation(f2);
		formations = fl.getFormations();
		assert.lengthOf(formations, 2, "a formations should have been removed");
		assert.equal(formations[0], f1);
		assert.equal(formations[1], f3);
	});

	it("should check if a given canal is valid", function(){
		let canal, result;

        // testing a websocket canal
        canal = { type : 2, address : "ws://127.0.0.1:80"};
        result = fl.isCanalValid(canal);
        assert.ok(result);

        // testing an incorrect websocket canal without address
        canal = { type : 2};
        result = fl.isCanalValid(canal);
        assert.notOk(result);

        // testing a http canal
        canal = { type : 1, address : "http://127.0.0.1:80"};
        result = fl.isCanalValid(canal);
        assert.ok(result);

        // testing an incorrect http canal without address
        canal = { type : 1};
        result = fl.isCanalValid(canal);
        assert.notOk(result);

        // testing an inccorect canal wih unknow type
        canal = { type : 999, address : "http://127.0.0.1:80"};
        result = fl.isCanalValid(canal);
        assert.notOk(result);

        // testing a correct webrtc canal
		canal = { type : 3 , canals : [{type : 2, address : "ws://127.0.0.1"}]}
        result = fl.isCanalValid(canal);
        assert.ok(result);

        // testing an incorrect webrtc canal
        canal = { type : 3 , canals : [{type : 933, address : "ws://127.0.0.1"}]}
        result = fl.isCanalValid(canal);
        assert.notOk(result);

        // testing an incorrect webrtc canal
        canal = { type : 3 }
        result = fl.isCanalValid(canal);
        assert.notOk(result);

        // testing a correct swans canal
		canal = { type : 5, canals : [{type : 2, address : "ws://127.0.0.1"}], receiver : "aabbcc"}
        result = fl.isCanalValid(canal);
		assert.ok(result);

        // testing an incorrect swans canal
        canal = { type : 5, canals : [{type : 2, address : "ws://127.0.0.1"}]}
        result = fl.isCanalValid(canal);
        assert.notOk(result);

        // testing an incorrect swans canal
        canal = { type : 5, receiver : "aabbcc"}
        result = fl.isCanalValid(canal);
        assert.notOk(result);

	})
});

describe("testing key related methods of the Flock", function(){
	let fl = null;
	
	beforeEach(function(){
		fl = new Flock();
	});
	
	afterEach(function(){
		fl.stop();
	});
	
	it("should generate keys with generateKey", function(){
		let keyLength = 129;
		let newKey = fl.generateKey(keyLength);
		
		// the new key must be an object with a span and a prefix
		assert.isObject(newKey);
		assert.isString(newKey.prefix);
		assert.isNumber(newKey.span);
	});
	
	it("should calculate the distance between two keys with dist method", function(){
		let k1, k2, expected, result;
		
		// testing two empty keys
		k1 = { prefix : "", span : 0}; 
		//k1 = ""; //{ prefix : new Uint8Array(0), span : 0}; 
		k2 = { prefix : "", span : 0}; 
		//k2 = ""; //{ prefix : new Uint8Array(0), span : 0}; 
		result = fl.dist(k1,k2);
		expected = "";//new Uint8Array(0);
		assert.deepEqual(result, expected);
		
		// testing an empty key and a non empty key
		k1 = { prefix : "", span : 0}; 
		k2 = { prefix : "f00d", span : 0}; 
		result = fl.dist(k1,k2);
		expected = "f00d"; //new Uint8Array([0b11110000, 0b00001101]);
		assert.deepEqual(result, expected);
		
		// testing two non empty key
		k1 = { prefix : "f0aaf00f", span : 0}; 
		k2 = { prefix : "f00d", span : 0};
		result = fl.dist(k1,k2);
		expected = "00a7f00f"; //new Uint8Array([0, 0b10100111, 0b11110000, 0b00001111]);
		assert.deepEqual(result, expected);
		
		// testing two keys with a span
		k1 = { prefix : "f0aaf00f0f0f", span : 2}; 
		k2 = { prefix : "f00d", span : 5}; 
		result = fl.dist(k1,k2);
		expected = "00a700000000"; //new Uint8Array([0, 0b10100111, 0, 0, 0, 0]);
		assert.deepEqual(result, expected);
		
		// testing two keys with a span
		k1 = { prefix : "f0aaf00f0f0f", span : 2}; 
		k2 = { prefix : "f00d", span : 2}; 
		result = fl.dist(k1,k2);
		expected = "00a700000f0f";//new Uint8Array([0, 0b10100111, 0, 0, 0b00001111, 0b00001111]);
		assert.deepEqual(result, expected);
		
	});
	
	it("should calculate the minimum distance between a key and an array of keys minDist method", function(){
		let k1, kArr, expected, result;
		
		// testing two empty keys
		k1 = { prefix : "", span : 0};
		kArr = [ { prefix : "", span : 0} ]
		result = fl.minDist(k1,kArr);
		expected = "";
		assert.deepEqual(result, expected);
		
		// testing an empty key and a non empty key
		k1 = { prefix : "", span : 0};
		kArr = [ { prefix : "f00d", span : 0}  ];
		result = fl.minDist(k1,kArr);
		expected = "f00d";
		assert.deepEqual(result, expected);
		
		// testing non empty keys
		k1 = { prefix : "f0aaf00f", span : 0};
		kArr = [ 
					{ prefix : "", span : 0},
					{ prefix : "00", span : 0},
					{ prefix : "f00d", span : 0},
					{ prefix : "3fff", span : 0}
				];
		result = fl.minDist(k1,kArr);
		expected = "00a7f00f";
		assert.deepEqual(result, expected);
	});
	
	it("should calculate the common byte length of two keys", function(){
		let k1, k2, expected, result;
		
		// testing two empty keys
		k1 = { prefix : "", span : 0};
		k2 = { prefix : "", span : 0};
		result = fl.common(k1,k2);
		expected = 0;
		assert.equal(result, expected);
		
		// testing an empty key and a non empty key
		k1 = { prefix : "", span : 0};
		k2 = { prefix : "f00d", span : 0};
		result = fl.common(k1,k2);
		expected = 0;
		assert.deepEqual(result, expected);
		
		// testing two non empty key
		k1 = { prefix : "f0aaf00f", span : 0};
		k2 = { prefix : "f00d", span : 0};
		result = fl.common(k1,k2);
		expected = 1;
		assert.deepEqual(result, expected);
		
		// testing two non empty key, with one key beeing included in the other
		k1 = { prefix : "f0aaf00f", span : 0};
		k2 = { prefix : "f0aa", span : 0};
		result = fl.common(k1,k2);
		expected = 2;
		assert.deepEqual(result, expected);
		
		// testing two non empty keys with span
		k1 = { prefix : "f0aaf00f", span : 2};
		k2 = { prefix : "f0aa", span : 7};
		result = fl.common(k1,k2);
		expected = 6;
		assert.deepEqual(result, expected);
		
		// testing two non empty keys with infinite spans
		k1 = { prefix : "f0aaf00f", span : +Infinity};
		k2 = { prefix : "f0aa", span : +Infinity};
		result = fl.common(k1,k2);
		expected = +Infinity;
		assert.deepEqual(result, expected);
		
		// testing two non empty keys with infinite spans, but with a non matching byte
		k1 = { prefix : "f0aaf00f", span : +Infinity};
		k2 = { prefix : "f0af", span : +Infinity};
		result = fl.common(k1,k2);
		expected = 1;
		assert.deepEqual(result, expected);

		// testing when span isn't specified
        k1 = { prefix: '00a1a2a3a4a5b6c7d8e9f0', dversion: 124 }
        k2 = { prefix: '00a1a2a3a4a5b6c7d8e9f0', span: 0 }
        result = fl.common(k1,k2);
        expected = 11;
        assert.deepEqual(result, expected);
	});

	it("should test the inverseSuffix method", function(){
		let key, bit, result, expected;
		
		// case : empty
		key = new Uint8Array([]);
		bit = 5;
		expected = new Uint8Array([]);
		result = fl.inverseSuffix(key, bit);
		assert.deepEqual(result, expected);
		
		// case : bit after end of ey
		key = new Uint8Array([0,0]);
		bit = 16;
		expected = new Uint8Array([0,0]);
		result = fl.inverseSuffix(key, bit);
		assert.deepEqual(result, expected);
		
		// case : index at the beginning of key
		key = new Uint8Array([0,0]);
		bit = 0;
		expected = new Uint8Array([255, 255]);
		result = fl.inverseSuffix(key, bit);
		assert.deepEqual(result, expected);
		
		// case : index in the middle of key
		key = new Uint8Array([0,0]);
		bit = 7;
		expected = new Uint8Array([1, 255]);
		result = fl.inverseSuffix(key, bit);
		assert.deepEqual(result, expected);
		
		key = new Uint8Array([0,0]);
		bit = 8;
		expected = new Uint8Array([0, 255]);
		result = fl.inverseSuffix(key, bit);
		assert.deepEqual(result, expected);
		
		key = new Uint8Array([0,0]);
		bit = 9;
		expected = new Uint8Array([0, 127]);
		result = fl.inverseSuffix(key, bit);
		assert.deepEqual(result, expected);
		
		key = new Uint8Array([2, 4, 8, 16, 32, 64, 128, 255]);
		bit = 7;
		expected = new Uint8Array([3, 251, 247, 239, 223, 191, 127, 0]);
		result = fl.inverseSuffix(key, bit);
		assert.deepEqual(result, expected);
		
		// try with hexadecimal
		key = "02040810204080ff"; //new Uint8Array([2, 4, 8, 16, 32, 64, 128, 255]);
		bit = 7;
		expected = "03fbf7efdfbf7f00";
		result = fl.inverseSuffix(key, bit);
		assert.deepEqual(result, expected);
	});
	
	it("should test the isIncluded method", function(){
		let k1, k2, result;
		
		// testing two empty keys
		k1 = { prefix : "", span : 0};
		k2 = { prefix : "", span : 0};
		result = fl.isIncluded(k1,k2);
		assert.isOk(result);
		
		// testing an empty key and a non empty key
		k1 = { prefix : "", span : 0};
		k2 = { prefix : "0000", span : 0};
		result = fl.isIncluded(k1,k2);
		assert.isOk(result);
		result = fl.isIncluded(k2,k1);
		assert.isNotOk(result);
		
		// testing two non empty key, one of which is the prefix of the other
		k1 = { prefix : "f0aa", span : 0};
		k2 = { prefix : "f0aaf00f", span : 0};
		result = fl.isIncluded(k1,k2);
		assert.isOk(result);
		result = fl.isIncluded(k2,k1);
		assert.isNotOk(result);
		
		// testing two non empty key, none of which is the prefix of the other
		k1 = { prefix : "f0aa", span : 0};
		k2 = { prefix : "f1aaf00f", span : 0};
		result = fl.isIncluded(k1,k2);
		assert.isNotOk(result);
		result = fl.isIncluded(k2,k1);
		assert.isNotOk(result);
		
		// testing two non empty key that are equal
		k1 = { prefix : "f0aaf00f", span : 0};
		k2 = { prefix : "f0aaf00f", span : 0};
		result = fl.isIncluded(k1,k2);
		assert.isOk(result);
		
		// testing two non empty key that are not equal
		k1 = { prefix : "f1aaf00f", span : 0};
		k2 = { prefix : "f0aaf00f", span : 0};
		result = fl.isIncluded(k1,k2);
		assert.isNotOk(result);

        // testing with a partially included key with limited precision
        k1 = { prefix : "f1aaf00f", span : 0};
        k2 = { prefix : "f0aaf00f", span : 0};
        result = fl.isIncluded(k1,k2, 4);
        assert.isOk(result);

        // testing with a non included key with limited precision
        k1 = { prefix : "f2aaf00f", span : 0};
        k2 = { prefix : "f0aaf00f", span : 0};
        result = fl.isIncluded(k1,k2, 7);
        assert.isNotOk(result);

		// testing with an included key with span
		k1 = { prefix : "f0aaf00f", span : 2};
		k2 = { prefix : "f0aa", span : 5};
		result = fl.isIncluded(k1,k2);
		assert.isOk(result);
		
		// testing with a non included key with span
		k1 = { prefix : "f0aaf00f", span : +Infinity};
		k2 = { prefix : "f0f0", span : 5};
		result = fl.isIncluded(k1,k2);
		assert.isNotOk(result);
	});
	
	it("should test isInf method", function(){
		let k1, k2, expected, result;
		
		// testing two empty keys
		k1 = "";
		k2 = ""
		result = fl.isInf(k1,k2);
		assert.isNotOk(result, expected);
		
		// testing an empty key and a non empty key
		k1 = "";
		k2 = "aa55";
		result = fl.isInf(k1,k2);
		assert.isOk(result, expected);
		result = fl.isInf(k2,k1);
		assert.isNotOk(result, expected);
		
		
		// testing two non empty key
		k1 = "f0aa";
		k2 = "f00d";
		result = fl.isInf(k1,k2);
		assert.isNotOk(result, expected);
		result = fl.isInf(k2,k1);
		assert.isOk(result, expected);
		
		k1 = "aa00bb";
		k2 = "aa01bb";
		result = fl.isInf(k1,k2);
		assert.isOk(result, expected);
		result = fl.isInf(k2,k1);
		assert.isNotOk(result, expected);
		
		
		// testing with two equal keys
		k1 = "f0aa";
		k2 = "f0aa";
		result = fl.isInf(k1,k2);
		assert.isNotOk(result, expected);
	});
	
});

describe("testing functions to manage nodes associated to the Flock", function() {
	let fl = null;
	
	beforeEach(function(){
		fl = new Flock();
	});
	afterEach(function(){
		fl.stop();
	});
	
	function createNode(){
		return {
			id : fl.generateId(),
			keys : [ fl.generateKey() ]
		}
	}
	
    it("should add a node to the Flock, get it and then remove it", function() {
		let node = createNode();
		
		// add a node in the node list
		fl.addNode(node);
		assert.equal(node, fl.nodes[node.id]);
		
		// remove the node
		fl.removeNode(node.id);
		assert.notProperty(fl.nodes, node.id);
    });
    
    it("should should add several nodes to the Flock, then retrieve the closest one to a given key", function() {
		let result, nresult, targetKey;
		
		// an empty node array return an empty result
		nresult = 4;
		targetKey = "000000"; //new Uint8Array([0, 0, 0]);
		result = fl.closestNodes(targetKey,nresult);
		assert.lengthOf(result, 0);
		
		// generate 256 nodes
		let nodes = [];
		let firstKeyByte = "7b";
		let lastKeyByte = "8c";
		for(let i=0; i<256;i++){
			let n = createNode();
			let midByte = (i<16 ? "0" : "") + i.toString(16);
			n.keys = [
						{ prefix : "ffff", span : 0},
						{ prefix : firstKeyByte + midByte + lastKeyByte, span : 0},
						{ prefix : "ffff", span : 0}
					];
			nodes.push(n);
			fl.addNode(n);
		}
		
		// retrieve 4 closest nodes of a given key
		nresult = 4;
		targetKey = { prefix : "000000", span : 0};
		result = fl.closestNodes(targetKey,nresult);
		assert.lengthOf(result, nresult);
		assert.equal(result[0], nodes[0], "the closest node should be the one expected");
		assert.equal(result[1], nodes[1], "the second closest node should be the one expected");
		assert.equal(result[2], nodes[2], "the third closest node should be the one expected");
		assert.equal(result[3], nodes[3], "the fourth closest node should be the one expected");
		
		// retrieve 4 nodes for another key
		nresult = 4;
		targetKey = { prefix : "008100", span : 0};
		result = fl.closestNodes(targetKey,nresult);
		assert.lengthOf(result, nresult);
		assert.equal(result[0], nodes[129], "the closest node should be the one expected");
		assert.equal(result[1], nodes[128], "the second closest node should be the one expected");
		assert.equal(result[2], nodes[131], "the third closest node should be the one expected");
		assert.equal(result[3], nodes[130], "the fourth closest node should be the one expected");
		
		// retrieve 8 nodes for another key
		nresult = 8;
		targetKey = { prefix : "16aa19", span : 0};
		result = fl.closestNodes(targetKey,nresult);
		assert.lengthOf(result, nresult);
		assert.equal(result[0], nodes[170], "the closest node should be the one expected");
		assert.equal(result[1], nodes[171], "the second closest node should be the one expected");
		assert.equal(result[2], nodes[168], "the third closest node should be the one expected");
		assert.equal(result[3], nodes[169], "the fourth closest node should be the one expected");
		assert.equal(result[4], nodes[174], "the fifth closest node should be the one expected");
		assert.equal(result[5], nodes[175], "the sixth closest node should be the one expected");
		assert.equal(result[6], nodes[172], "the seventh closest node should be the one expected");
		assert.equal(result[7], nodes[173], "the eighth closest node should be the one expected");
    });

    it("should retrieve closest node in case the node is a squawker", function(){
        let result, nresult, targetKey;
        targetKey = "0102030405060708090a0b0c0d0e0f";

        // generate 4 nodes :
		// the first node has a completely different key
		// the second node as a small portion of key matching
		// the third has almost all the key matching
		// the last has no key but is a squawker

		// the closest node should return the in order 3, 4, 2, 1

		// create the four nodes
        let n1 = createNode();
        n1.keys = [
            { prefix : "ffffffffff", span : 0},
        ];
        fl.addNode(n1);

        let n2 = createNode();
        n2.keys = [
            { prefix : targetKey.slice(0,4), span : 0},
        ];
        fl.addNode(n2);

        let n3 = createNode();
        n3.keys = [
            { prefix : targetKey, span : 0},
        ];
        fl.addNode(n3);

        let squawker = createNode();
        squawker.keys = [ ];
        squawker.squawker = true;
        fl.addNode(squawker);

        // retrieve 4 closest nodes of a given key
        nresult = 4;
        result = fl.closestNodes({ prefix : targetKey, span : 0}, nresult, false, null, true);
        assert.lengthOf(result, nresult);
        assert.equal(result[0], n3, "the closest node should be the one expected");
        assert.equal(result[1], squawker, "the second closest node should be the one expected");
        assert.equal(result[2], n2, "the third closest node should be the one expected");
        assert.equal(result[3], n1, "the fourth closest node should be the one expected");
    })


    it("should retrieve a particular node from its interface", function(){
		let result, expected;
		
		// create two nodes with a mock interface
		let n1 = createNode();
		let interface1 = {};
		n1.interface = interface1;
		fl.addNode(n1);
		
		let n2 = createNode();
		let interface2 = {};
		n2.interface = interface2;
		fl.addNode(n2);
		
		// retrieve the first node from its interface
		result = fl.nodeFromInterface(interface1);
		expected = n1;
		assert.deepEqual(result, expected);
		
		// retrieve the second node from its interface
		result = fl.nodeFromInterface(interface2);
		expected = n2;
		assert.deepEqual(result, expected);
	});
	
    it("should return the list of connected nodes with getConnectedNodes", function(){
		// create nodes to be filtered
		
		// no interface : it is not connected
		let n1 = {
			id : 1
		}
		
		// an interface in new status : it is not connected
		let n2 = {
			id : 2,
			interface : new NestedInterface()
		}
		n2.interface.status(n2.interface.NEW);
		
		// an interface in up status : the node is connected
		let n3 = {
			id : 3,
			interface : new NestedInterface()
		}
		n3.interface.status(n3.interface.UP);
		let n4 = {
			id : 4,
			interface : new NestedInterface()
		}
		n4.interface.status(n4.interface.UP);
		
		let nodes = [n1,n2,n3,n2,n4,n1];
		let filteredNodes = fl.getConnectedNodes(nodes);
		assert.isArray(filteredNodes);
		assert.lengthOf(filteredNodes, 2);
		assert.equal(filteredNodes[0], n3);
		assert.equal(filteredNodes[1], n4);
	});
});


describe("testing conversion from flock protocol packet to objects and back", function(){
	let fl = new Flock();

	after(function(){
		fl.stop();
	})

	it("should test the conversion of getNode packet", function(){
		let getNodePacket = {
			version : 1,
			method : "getNode",
			requestId : "123456",
			precision : 25,
			prefix : "01234567890123456789aa" 
		}
		let bytes = fl.getNodeToBytes(getNodePacket);
		let packet = fl.getNodeToObject(bytes);
		assert.equal(packet.version, getNodePacket.version, "checking version value");
		assert.equal(packet.method, "getNode", "checking method value");
		assert.equal(packet.requestId, getNodePacket.requestId, "checking requestId value");
		assert.equal(packet.precision, getNodePacket.precision, "checking precision value");
		assert.deepEqual(packet.prefix, getNodePacket.prefix, "checking prefix value");
	});
	
	it("should test the conversion of giveNode packet without nodes", function(){
		let giveNodePacket = {
			version : 1,
			method : "giveNode",
			requestId : "00258961",
			nodes : [],
		}
		let bytes = fl.giveNodeToBytes(giveNodePacket);
		let packet = fl.giveNodeToObject(bytes);
		assert.equal(packet.version, giveNodePacket.version, "checking version value");
		assert.equal(packet.method, "giveNode", "checking method value");
		assert.equal(packet.requestId, giveNodePacket.requestId, "checking requestId value");
		assert.deepEqual(packet.nodes, giveNodePacket.nodes, "checking nodes value");
	});
	
	it("should test the conversion of giveNode packet with nodes", function(){
		let giveNodePacket = {
			version : 1,
			method : "giveNode",
			requestId : "00112233",
			precision : 5000,
			nodes : [
				{
					id : "0281474976710655aabbccdd",
					keys : [{ prefix : "", common : 4095, span : 0, hiddenLength : 0}],
					canals : [
						{ type : 0 },
						{ type : 1 },
						{ type : 1, address : "test.com" },
						{ type : 2, address : "" },
						{ type : 2, address : "test222.com" },
						{ type : 3, address : "", key : "" },
						{ type : 3, address : "test444.com", key : "" },
						{ type : 3, address : "test444.com", key : "1234567890"}
					]
				},
				{
					id : "0123",
					keys : [{ common : 0, prefix : "aabbccddeeff0123456789", hiddenLength : 1256, span : 99999 }],
					canals : []
				},
				{
					id : "ff4567",
					keys : [
						{common : 5, prefix : "aabbccddeeff0123456789", hiddenLength : 1256, span : 99999 },
						{common : 2, prefix : "aabbccddeeff0123456789", hiddenLength : 1256, span : 99999 }
					],
					canals : []
				}
			],
		}
		let expectedDecoded = {
			version : 1,
			method : "giveNode",
			requestId : "00112233",
			precision : 5000,
			nodes : [
				{
					id : "0281474976710655aabbccdd",
					keys : [{ prefix : "", common : 4095, span : 0, hiddenLength : 0}],
					canals : [
						{ type : 0 },
						{ type : 1 },
						{ type : 1, address : "test.com" },
						{ type : 2, address : "" },
						{ type : 2, address : "test222.com" },
						{ type : 3, address : "", key : "" },
						{ type : 3, address : "test444.com", key : "" },
						{ type : 3, address : "test444.com", key : "1234567890"}
					]
				},
				{
					id : "0123",
					keys : [{ common : 0, prefix : "aabbccddeeff0123456789", hiddenLength : 1256, span : 99999  }],
					canals : []
				},
				{
					id : "ff4567",
					keys : [
						{common : 5, prefix : "aabbccddeeff0123456789", hiddenLength : 1256, span : 99999 },
						{common : 2, prefix : "aabbccddeeff0123456789", hiddenLength : 1256, span : 99999 }
					],
					canals : []
				}
			],
		};
		let bytes = fl.giveNodeToBytes( giveNodePacket);

		let packet = fl.giveNodeToObject(bytes);
		assert.equal(packet.version, expectedDecoded.version, "checking version value");
		assert.equal(packet.method, "giveNode", "checking method value");
		assert.equal(packet.requestId, expectedDecoded.requestId, "checking requestId value");
		assert.lengthOf(packet.nodes, expectedDecoded.nodes.length, "checking nodes length");
		assert.deepEqual(packet.nodes, expectedDecoded.nodes, "checking the nodes");
	});

	it("should test the conversion of getInfo packet ", function(){
		let getInfoPacket = {
			version : 1,
			method : "getInfo",
			requestId : "002255",
			getKeys : true,
			getCanals : false,
			getId : true,
			precision : 4095
		}
		let bytes = fl.getInfoToBytes(getInfoPacket);
		let packet = fl.getInfoToObject(bytes);
		assert.equal(packet.version, getInfoPacket.version, "checking version value");
		assert.equal(packet.method, "getInfo", "checking method value");
		assert.equal(packet.requestId, getInfoPacket.requestId, "checking requestId value");
		assert.equal(packet.precision, getInfoPacket.precision, "checking precision value");
		assert.deepEqual(packet.getKeys, getInfoPacket.getKeys, "checking getKey value");
		assert.deepEqual(packet.getCanal, getInfoPacket.getCanal, "checking getCanal value");
		assert.deepEqual(packet.getId, getInfoPacket.getId, "checking getId value");
	});
	
	it("should test the conversion of getInfo packet (2)", function(){
		let getInfoPacket = {
			version : 1,
			method : "getInfo",
			requestId : "669988",
			getKeys : false,
			getCanals : true,
			getId : false,
			precision : 4095
		}
		let bytes = fl.getInfoToBytes(getInfoPacket);
		let packet = fl.getInfoToObject(bytes);
		assert.equal(packet.version, getInfoPacket.version, "checking version value");
		assert.equal(packet.method, "getInfo", "checking method value");
		assert.equal(packet.requestId, getInfoPacket.requestId, "checking requestId value");
		assert.equal(packet.precision, getInfoPacket.precision, "checking precision value");
		assert.deepEqual(packet.getKeys, getInfoPacket.getKeys, "checking getKey value");
		assert.deepEqual(packet.getCanal, getInfoPacket.getCanal, "checking getCanal value");
		assert.deepEqual(packet.getId, getInfoPacket.getId, "checking getId value");
	});

	it("should test the conversion of giveInfo", function(){
		let giveInfoPacket = {
			version : 1,
			method : "giveInfo",
			requestId : "147258",
			nodes : [ {
                id : "0abcdef1",
                keys : [{prefix : "123456ae", span : 123 }],
                canals : [
                    { type : 0 },
                    { type : 1 },
                    { type : 1, address : "test.com" },
                    { type : 2 },
                    { type : 2, address : "test222.com" },
                    { type : 3, key : "" },
                    { type : 3, address : "test444.com" },
                    { type : 3, address : "test444.com", key : "01234567890123456789aa"}
                ]
			}]
		}
		
		let expectedOutput = {
			version : 1,
			method : "giveInfo",
			requestId : "147258",
			getKey : true,
			getCanals : true,
			getId : true,
			nodes : [{
                id : "0abcdef1",
                keys : [{prefix : "123456ae", span : 123, hiddenLength : 0}],
                canals : [
                    { type : 0 },
                    { type : 1},
                    { type : 1, address : "test.com" },
                    { type : 2},
                    { type : 2, address : "test222.com" },
                    { type : 3 , key : ""},
                    { type : 3, address : "test444.com" },
                    { type : 3, address : "test444.com", key : "01234567890123456789aa"}
                ]
			}]
		}
		
		let bytes = fl.giveInfoToBytes(giveInfoPacket);
		let packet = fl.giveInfoToObject(bytes);
		assert.equal(packet.version, expectedOutput.version, "checking version value");
		assert.equal(packet.method, "giveInfo", "checking method value");
		assert.equal(packet.requestId, expectedOutput.requestId, "checking requestId value");
		assert.deepEqual(packet.keys, expectedOutput.keys, "checking key value");
		assert.deepEqual(packet.id, expectedOutput.id, "checking id value");
		assert.deepEqual(packet.canals, expectedOutput.canals, "checking canals value");
	});
	

});

describe("TU for processRequest", function(){

	// tests consists of a flock with two spawners, and a connection used to send requests to this flock
	
	let fl, nimeta, spawner1, spawner2;
	let flockPort = 12345;
	let fromPort = 147;

	function createNode(){
		return {
			id : fl.generateId(),
			keys : [fl.generateKey()],
			canals : [
				{type : 2, address : "test.com"}
			]
		}
	}
	beforeEach(function(){
		shiftCounter = 1;
		nimeta = new NIMeta();
		fl = new Flock( {
			port : flockPort,
			metaInterface : nimeta,

		});
		spawner1 = nestedInterfaceTestHelpers.createSpawner(1);
		spawner2 = nestedInterfaceTestHelpers.createSpawner(2);
		nimeta.addSpawner(spawner1);
		nimeta.addSpawner(spawner2);
	});
	
	afterEach(function(){
		nimeta.stop();
        fl.stop();
		shiftCounter = 1;
	});
	
	it("should receive a getNode request and answer with a giveNode packet", function(done){
		let connection = nestedInterfaceTestHelpers.createSpawn(1);
		
		// add a node to the flock
		let node = createNode();
		fl.addNode(node);
		let getNodeRequest = {
			version : 0,
			method : "getNode",
			requestId : fl._generateRequestId(),
			prefix : node.keys[0].prefix,
			precision : 100,
			k : 5,
		}
		let getNodeBytes = fl.objectToBytes(getNodeRequest);
		connection.addHook( connection.e.RECEIVE, function(e){
			eventReceived = e;
		});
		let eventReceived = null;
		
		shift(function(){
			connection.send(getNodeBytes, flockPort, fromPort);
		});
		
		shift(function(){
			assert.isNotNull(eventReceived);
			let answer = null;
			assert.equal(eventReceived.port, fromPort, "the answer should be sent through the port sending the request");
			assert.equal(eventReceived.fport, flockPort, "the answer should be sent from the port associated to the flock");
			assert.isNotNull(eventReceived.message, "the answer should provide a byte message");
			try{
				answer = fl.bytesToObject(eventReceived.message);
			}
			catch(e){ console.log(e); }
			assert.equal(answer.method, "giveNode", "the answer should be a giveNode packet");
			assert.equal(answer.version, 0);
			assert.isArray(answer.nodes,"should return an array of nodes");
			assert.lengthOf(answer.nodes, 2, "the answer should return 2 nodes");
			
			// the first node should be the node that has been added manually
			let node1 = answer.nodes[0];
			assert.equal(node1.id, node.id);
			assert.equal(node1.keys[0].common, node.keys[0].prefix.length/2, "since the node key is the requested key, the common part is the full key");
			assert.lengthOf(node1.keys[0].prefix, 0, "no key is returned since it is exactly the one requested");
			assert.deepEqual(node1.canals, node.canals, "the canals must be those of the node");
			
			// the second node must be the flock node
			let node2 = answer.nodes[1];
			let flockId = fl.getId();
			let flockKeys = fl.getKeys();
			assert.equal(node2.id, flockId);
			assert.deepEqual( node2.keys.map(v=>{ return {prefix : getNodeRequest.prefix.slice(0, 2*(v.common||0) ) + v.prefix, span : v.span}}), flockKeys);
			assert.lengthOf(node2.canals, 2, "three canals are associated : the 2 spawners");
			let canal1 = spawner1.getCanals()[0];
			let canal2 = spawner2.getCanals()[0];

            assert.isNotNull(node2.canals[0]);
            assert.isNotNull(node2.canals[1]);

			assert.deepEqual(node2.canals[0], canal1);
			assert.deepEqual(node2.canals[1], canal2);
			done();
		});
	});

	it("should not process giveNode packet if no request is matching this answer", function(done){
		let connection = nestedInterfaceTestHelpers.createSpawn(1);
		
		// create requests and answers : getNode, giveNode
		let node1 = createNode();
		let node2 = createNode();
		let node3 = createNode();
		node1.common = 4;
		let getNodeRequest = {
			version : 0,
			requestId : "123456",
			method : "getNode",
			prefix : node1.keys[0].prefix.slice(0,4),
			precision : 4,
			k : 5,
		}
		let giveNodeAnswer = {
			version : 0,
			method : "giveNode",
			precision : 4,
			requestId : "9595959595959", // wrong requestId
			nodes : [
				{
					keys : [ {prefix : node2.keys[0].prefix, common : 2, span : 12}],
					canals : node2.canals,
					id : node2.id
				},
				{
					keys : [ {prefix : node3.keys[0].prefix, common : 0, span : 12}],
					canals : node3.canals,
					id : node3.id
				}
			]
		}
		
		// add a request manually for the flock
		fl.addRequest(getNodeRequest);
		
		shift(function(){
			let bytesAnswer = fl.objectToBytes(giveNodeAnswer);
			connection.send(bytesAnswer, flockPort, fromPort);
		});
		
		shift(function(){
			let flNodes = fl.getNodes();
			let flids = flNodes.map(node=>node.id);
			// throw "throooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo"
			assert.notInclude(flNodes, [node2.id, node3.id]);
			done();
		});
	});
	
	it("should process giveNode packet", function(done){ 
		let connection = nestedInterfaceTestHelpers.createSpawn(1);
		
		// create requests and answers : getNode, giveNode
		let node1 = createNode();
		let precision = 4;
		let requestPrefixLength = 4;
		let getNodeRequest = {
			version : 0,
			requestId : "123456",
			method : "getNode",
			prefix : node1.keys[0].prefix,
			precision : precision,
			k : 2,
		}
		let node2 = createNode();
		node2.keys[0].prefix = node2.keys[0].prefix.slice(0,2*precision);
		let node3 = createNode();
		node3.keys[0].prefix = node3.keys[0].prefix.slice(0,2*precision);
		let giveNodeAnswer = {
			version : 0,
			method : "giveNode",
			precision : 4,
			requestId : "123456",
			nodes : [
				{
					keys : [ {prefix : node2.keys[0].prefix, common : 2, span : 12, hiddenLength : 12}],
					canals : node2.canals,
					id : node2.id
				},
				{
					keys : [ {prefix : node3.keys[0].prefix, common : 0, span : 75, hiddenLength : 23}],
					canals : node3.canals,
					id : node3.id
				}
			]
		}
		
		// build nodes as they are expected to be received
		let expectedNode2 = {
			canals : node2.canals,
			keys : [ {
				prefix : node1.keys[0].prefix.substr(0,4) + node2.keys[0].prefix,
				span : 12,
				hiddenLength : 12
				}],
			id : node2.id
		};
		
		let expectedNode3 = {
			canals : node3.canals,
			keys : [{
					prefix : node3.keys[0].prefix,
					span : 75,
					hiddenLength : 23
					}],
			id : node3.id
		};
		
		// add a request manually for the flock
		fl.addRequest(getNodeRequest);
		
		shift(function(){
			let bytesAnswer = fl.objectToBytes(giveNodeAnswer);
			connection.send(bytesAnswer, flockPort, fromPort);
		});
		
		shift(function(){
			let nodesArr = fl.getNodes();
			let nodes = {};
			for(let node of nodesArr) nodes[ node.id ] = node;
			assert.deepInclude(nodes[node2.id], expectedNode2, "testing the first node sent by the giveNode packet");
			assert.deepInclude(nodes[node3.id], expectedNode3, "testing the second node sent by the giveNode packet");
			done();
		});
	});
	
	it("should insert a new node in the flock with a giveInfo answer", function(done){
		// create requests and answers : getNode, giveNode
		let node1 = createNode();
		let getInfoRequest = {
			requestId : "1234",
			method : "getInfo",
		};
			
		let k1 = "02040810204080ff"; 
		let k2 = "010203"; 
		let span1 = 28;
		let span2 = 558;
		let expectedKeys = [{ prefix : k1, span : span1 }, { prefix : k2, span : span2 }];
		
		let giveInfoAnswer = {
			requestId : "1234",
			method : "giveInfo",
			nodes : [ {
				id : "111111",
				keys : [ { prefix : k1, span : span1 }, { prefix : k2, span : span2 } ],
				canals : [
					{type : 0},
					{type : 1, address : "test.com"}
				]
			}]

		}
		
		// add a request manually for the flock
		fl.addRequest(getInfoRequest);
		let connection;

		shift(function(){
			// at first, there should be no node
			let flNodes = fl.getNodes();
			assert.lengthOf(flNodes, 0);
			// prepare the connection
            connection = nestedInterfaceTestHelpers.createSpawn(1);
		});

		shift(function(){
            // send the answer
            let bytesAnswer = fl.objectToBytes(giveInfoAnswer);
            connection.send(bytesAnswer, flockPort, fromPort);
		})

		shift(function(){
			let flNodesArr = fl.getNodes();
			let flNodes = {};
			for(let node of flNodesArr) flNodes[node.id] = node;
			let nodeRes = flNodes[giveInfoAnswer.nodes[0].id];
			assert.ok(!!nodeRes, "the expected node must be retrieved");
			assert.equal(nodeRes.id, giveInfoAnswer.nodes[0].id, "testing the id of the info retrieved");
			assert.deepEqual(nodeRes.canals, giveInfoAnswer.nodes[0].canals, "testing the canals of the info retrieved");
			assert.deepEqual(nodeRes.keys, expectedKeys, "testing the key of the info retrieved");
			done();
		});
	});

	it("should not insert a new node with giveInfo with a wrong requestId", function(done){
		let connection = nestedInterfaceTestHelpers.createSpawn(1);
		
		// create requests and answers : getNode, giveNode
		let node1 = createNode();
		let getInfoRequest = {
			requestId : "1234",
			method : "getInfo",
		};
			
		let giveInfoAnswer = {
			requestId : "2345",  // WRONG REQUEST ID OF THE ANSWER
			method : "giveInfo",
			id : "111111",
			keys : [ ],
			canals : []
		}
		
		// add a request manually for the flock
		fl.addRequest(getInfoRequest);
		shift(function(){
			// send the answer
			let bytesAnswer = fl.objectToBytes(giveInfoAnswer);
			connection.send(bytesAnswer, flockPort, fromPort);
		});
		shift(function(){
			let flNodesArr = fl.getNodes();
			let flNodes = {};
			for(let node of flNodesArr) flNodes[node.id] = node;
            assert.notProperty(flNodes, "111111");
			let nodeRes = flNodes[0];
			done();
		});
	});

	it("should add data to an already existing node with giveInfo", function(done){
		let connection;
		
		// create requests and answers : getNode, giveNode
		let node1 = createNode();
		let partialNode = {};
		let getInfoRequest = {
			requestId : "1234",
			method : "getInfo",
			node : partialNode
		};
		let k1 = "02040810204080ff"; //new Uint8Array([2,4,8,16,32,64,128,255]);
		let span1 = 36;
		let newKeys = [{prefix : k1, span : span1}];
		let giveInfoAnswer = {
			requestId : "1234",
			method : "giveInfo",
			nodes : [{
                id : "111111",
                keys : [{prefix : k1, span : span1}],
                canals : [
                    {type : 0},
                    {type : 1, address : "test.com"}]
			}]
		}
		
		// add a request manually for the flock
		fl.addRequest(getInfoRequest);
		
		shift(function(){
			// at first, there should be no node
			let flNodes = fl.getNodes();
			assert.lengthOf(flNodes, 0);
			// prepare the connection
			connection = nestedInterfaceTestHelpers.createSpawn(1);
		});

		shift(function(){
            // send the answer
            let bytesAnswer = fl.objectToBytes(giveInfoAnswer);
            connection.send(bytesAnswer, flockPort, fromPort);
		})
		
		shift(function(){
			let flNodes = fl.getNodes();
			let nodeRes = flNodes.find(node=>node.id == giveInfoAnswer.nodes[0].id);
			assert.equal(nodeRes.id, giveInfoAnswer.nodes[0].id, "testing the id of the info retrieved");
			assert.deepEqual(nodeRes.keys, newKeys, "testing the keys of the info retrieved");
			assert.deepEqual(nodeRes.canals, giveInfoAnswer.nodes[0].canals, "testing the canals of the info retrieved");
			assert.equal( nodeRes, partialNode, "the result node should be the completed partialNode");
			done();
		});
	});
	

	it("should return the info of the flock with getInfo", function(done){
		let connection = nestedInterfaceTestHelpers.createSpawn(1);
		
		let node1 = createNode();
		fl.addNode(node1);
		let partialNode = {};
		let getInfoRequest = {
			requestId : "1234",
			method : "getInfo",
			getKeys : true,
			getId : true,
			getCanals : true,
			precision : 1000
		};
		
		connection.addHook( connection.e.RECEIVE, function(e){
			eventReceived = e;
		});
		let eventReceived = null;
		
		shift(function(){
			// send the request
			let bytesRequest = fl.objectToBytes(getInfoRequest);
			connection.send(bytesRequest, flockPort, fromPort);
		});
		
		shift(function(){
			assert.isNotNull(eventReceived, "an answer should have been received");
			assert.equal(eventReceived.port, fromPort, "check the port of the event");
			assert.equal(eventReceived.fport, flockPort, "check the fport of the event");
			let answer = fl.bytesToObject(eventReceived.message);
			let answerKeys = answer.nodes[0].keys.map(kd=>kd.prefix);
			let flockId = fl.getId();
			let flockKeys = fl.getKeys();
			let flockCanals = fl.getCanals();
			assert.equal(answer.nodes[0].id, flockId, "check the id given in giveInfo match the flock id");
			assert.deepEqual(answerKeys, flockKeys.map(k=>k.prefix), "check the key given in giveInfo match the flock key");
			assert.deepEqual(answer.nodes[0].canals, flockCanals, "check the canals given in giveInfo match the flock canals");
			done();
		});
		
	});

	it("should return the canals of the flock with getInfo, and the beginning of the flock key", function(done){
		let connection = nestedInterfaceTestHelpers.createSpawn(1);
		
		let node1 = createNode();
		fl.addNode(node1);
		let partialNode = {};
		let getInfoRequest = {
			requestId : "1234",
			method : "getInfo",
			getKeys : true,
			precision : 3,
			getId : false,
			getCanals : true,
		};
		
		connection.addHook( connection.e.RECEIVE, function(e){
			eventReceived = e;
		});
		let eventReceived = null;
		
		shift(function(){
			// send the request
			let bytesRequest = fl.objectToBytes(getInfoRequest);
			connection.send(bytesRequest, flockPort, fromPort);
		});
		
		shift(function(){
			assert.isNotNull(eventReceived, "an answer should have been received");
			assert.equal(eventReceived.port, fromPort, "check the port of the event");
			assert.equal(eventReceived.fport, flockPort, "check the fport of the event");
			let answer = fl.bytesToObject(eventReceived.message);
			
			let flockId = fl.getId();
			let flockPrefix = [ fl.getKeys()[0].prefix.slice(0,6) ];
			let flockCanals = fl.getCanals();
			assert.notExists(answer.nodes[0].id, "check the id has not been returned");
			assert.deepEqual(answer.nodes[0].keys.map(kd=>kd.prefix), flockPrefix, "check the key given in giveInfo match the flock key");
			assert.deepEqual(answer.nodes[0].canals, flockCanals, "check the canals given in giveInfo match the flock canals");
			done();
		});
	});
	

	it("should return no data with getInfo", function(done){
		let connection = nestedInterfaceTestHelpers.createSpawn(1);
		
		let node1 = createNode();
		fl.addNode(node1);
		let partialNode = {};
		let getInfoRequest = {
			requestId : "1234",
			method : "getInfo",
			getKeys : false,
			precision : 3,
			getId : false,
			getCanals : false,
		};
		
		connection.addHook( connection.e.RECEIVE, function(e){
			eventReceived = e;
		});
		let eventReceived = null;
		
		shift(function(){
			// send the request
			let bytesRequest = fl.objectToBytes(getInfoRequest);
			connection.send(bytesRequest, flockPort, fromPort);
		});
		
		shift(function(){
			assert.isNotNull(eventReceived, "an answer should have been received");
			let answer = fl.bytesToObject(eventReceived.message);
			assert.notExists(answer.id, "check the id has not been returned");
			assert.notExists(answer.keys, "check the key has not been returned");
			assert.notExists(answer.canals, "check the canals have not been returned");
			done();
		});
	});

});



describe("testing some node related methods", function(){
	
	// tests consists of two flocks, with one spawner associated to each nimeta
	let fl1, fl2, fl3;
	let  nimeta1,  nimeta2, nimeta3;
	let  spawner1, spawner2, spawner3;
	let  node1, node2, node3;

	let flockPort = 12345;
	let fromPort = 12345;

	function createNode(){
		return {
			id : fl1.generateId(),
			keys : [fl1.generateKey()],
			canals : [
				{type : 2, address : "test.com"}
			]
		}
	}
	
	beforeEach(function(){
		shiftCounter = 1;
		nimeta1 = new NIMeta();
        nimeta2 = new NIMeta();
        nimeta3 = new NIMeta();
		fl1 = new Flock( {
			port : flockPort,
			metaInterface : nimeta1
		});
		spawner1 = nestedInterfaceTestHelpers.createSpawner(1);
		nimeta1.addSpawner(spawner1);

        fl2 = new Flock( {
            port : flockPort,
            metaInterface : nimeta2,
        });
        spawner2 = nestedInterfaceTestHelpers.createSpawner(2);
        nimeta2.addSpawner(spawner2);

        fl3 = new Flock( {
            port : flockPort,
            metaInterface : nimeta3,
            cronInterval : 100
        });
        spawner3 = nestedInterfaceTestHelpers.createSpawner(3);
        nimeta3.addSpawner(spawner3);
		
		// add nodes
		node1 = {
			id : fl1.getId(),
			keys : fl1.getKeys(),
			canals : fl1.getCanals(),
		};
        node2 = {
            id : fl2.getId(),
            keys : fl2.getKeys(),
            canals : fl2.getCanals(),
        };
        node3 = {
            id : fl3.getId(),
            keys : fl3.getKeys(),
            canals : fl3.getCanals(),
        };
		fl1.addNode(node2);
		fl2.addNode(node1);
	});
	
	afterEach(function(){
		nimeta1.stop();
        nimeta2.stop();
        nimeta3.stop();
        fl1.stop();
        fl2.stop();
        fl3.stop();
		shiftCounter = 1;
	});
	
	it("should create a nested interface in a node with connectNode", function(done){
		let spawnReceived = null;
		let message = new Uint8Array([1,2,3,4]);
		let messageReceived = null;
		
		// add a hook on nimeta2 to detect message sent from flock1;
		nimeta2.addHook(nimeta2.e.RECEIVE, function(e){ messageReceived = e.message; });
		
		// try to connect from fl1
		fl1.connectNode(node2).then(function(spawn){
			spawnReceived = spawn;
		});
		
		shift(function(){
			assert.isNotNull(spawnReceived, "the connectNode should have resolved with a spawn");
			assert.equal(node2.interface, spawnReceived);
			spawnReceived.send(message);
		});
		
		shift(function(){
			assert.isNotNull(messageReceived, "a message should have been received in flock 2");
			assert.deepEqual(messageReceived, message);
		});
		shift(done);
	});

	it("should remove a closed interface and create a new one with connectNode", function(done){
		let firstInterface = null;
		let secondInterface = null;
		let thirdInterface = null;
		let message = new Uint8Array([1,2,3,4]);
		let messageReceived = null;
		
		// add a hook on nimeta2 to detect message sent from flock1;
		nimeta2.addHook(nimeta2.e.RECEIVE, function(e){ messageReceived = e.message; });
		
		// try to connect from fl1
		fl1.connectNode(node2).then(function(spawn){
			firstInterface = spawn;
		});
		
		shift(function(){
			assert.isNotNull(firstInterface, "the connectNode should have resolved with a spawn");
			assert.equal(node2.interface, firstInterface);
			fl1.connectNode(node2).then(function(spawn){
				secondInterface = spawn;
			});
		});
		
		shift(function(){
			assert.isNotNull(secondInterface,  "the connectNode should have resolved with a spawn");
			assert.equal(firstInterface, secondInterface,  "the second interface returned should be the first since it was already connected");
			secondInterface.stop();
			fl1.connectNode(node2).then(function(spawn){
				thirdInterface = spawn;
			});
		});
		
		shift(function(){
			assert.isNotNull(thirdInterface,  "the connectNode should have resolved with a spawn");
			assert.notEqual(firstInterface, thirdInterface,  "a new interface should have been created in replacement");
		});
		
		shift(done);
	});

    it("should connect one node with connectNode and fail for the second because of node limits", function(done){
        let success1 = false;
    	let fail1 = false;
    	let success2 = false;
    	let fail2 = false;

    	// reduce the maximum of nodes to be connected
		fl1.maxInterfacedNodes = 1;
		fl1.addNode(node3);
		fl1.addNode(node2);

        // expect success for first node connection
        fl1.connectNode(node3).then(function(spawn){
            success1 = !!spawn;
        }).catch(function(){
            fail1 = true;
        });

		// expect failure for first node connection
		fl1.connectNode(node2).then(function(spawn){
			success2 = !!spawn;
		}).catch(function(){
			fail2 = true;
		});

        shift(function(){
            assert.ok(success1);
            assert.notOk(fail1);
            assert.notOk(success2);
            assert.ok(fail2);
            done();
        });
    });

	it("should fail to create an interface and reject a promise with connectNode when given an incorrect node", function(done){
		let wrongNode = {};
		let rejected = false;
		fl1.connectNode(wrongNode).then(function(spawn){
			assert.ok(false,"the promise should not resolve successfully");
		},
		function(e){ 
			rejected = true;
		});
		
		shift(function(){
			assert.ok(rejected, "promise rejections hould have occured");
			done();
		});
	});
	
	it("should retrieve info with getNodeInfo", function(done){
		let node = {
			canals : node2.canals
		}
		let prom = fl1.getNodeInfo(node).then(function(n){
			assert.equal(node, n, "the result of the promise should be the input node with additional data");
		});
		
		shift(function(){
			assert.deepEqual(node.id, node2.id, "checking node id");
			assert.deepEqual(node.keys, node2.keys, "checking node key");
			done();
		});
	});
	
	it("should retrieve nodes with getNodeNode", function(done){
		// create four nodes and add them to the second flock
		let n1 = createNode();
		let n2 = createNode();
		let n3 = createNode();
		let n4 = createNode();
		fl2.addNode(n1);
		fl2.addNode(n2);
		fl2.addNode(n3);
		fl2.addNode(n4);
		
		let getNodeData = {
			k : 3
		};
		let nodes;
		let prom = fl1.getNodeNode(node2, n1.keys[0].prefix, getNodeData).then(function(n){
			nodes = n;
		});
		
		shift(function(){
			assert.isArray(nodes, "the result of the promise should be an array");
			assert.lengthOf(nodes, 3);
			assert.deepEqual(nodes[0].keys[0].prefix, n1.keys[0].prefix);
			done();
		});
	});

	it("should remove old nodes duringg the cron", function(done){
        let node4 = createNode();
        let node5 = createNode();
        fl3.nodeExpireTime = 10000;
		fl3.removeNodes();
        fl3.addNode(node2);
        fl3.addNode(node1);
        fl3.getNodeInfo(node2);
        fl3.getNodeInfo(node1);

        shift(function(){
            assert.ok(!!node2.interface)
            assert.ok(!!node1.interface)
            fl3.addNode(node4);
            let nodes = fl3.getNodes();
            assert.lengthOf(nodes,3);
		})

		shift(function(){
            let nodes = fl3.getNodes();
            assert.lengthOf(nodes,3);
        	// should remove the node4 in the next cron
            fl3.nodeExpireTime = 1;
		})

		shift(function(){
            let nodes = fl3.getNodes();
            assert.lengthOf(nodes,2);
            // should remove the node3 in the next cron
            fl2.stop();
		})

        shift(function(){
            let nodes = fl3.getNodes();
            assert.lengthOf(nodes,1);
        })

		shift(done)
	})
});


describe("testing formations", function(){
	
	it("should create a formation without error", function(){
		let formation = new Formation();
		assert.ok(true);
	});
	
	it("should get/set the formation key", function(){
		let expected, result;
		let formation = new Formation();
		
		expected = new Uint8Array();
		result = formation.key();
		assert.deepEqual(result, expected, "should get the key with getter/setter method");
		let newKey = new Uint8Array([1,2,3,5]);
		formation.key(newKey);
		result = formation.key();
		assert.deepEqual(result, newKey, "should get the new key with getter/setter method");
	});
	
	it("should get/set the formation beforeKey property", function(){
		let expected, result;
		let formation = new Formation();
		
		expected = 3;
		result = formation.beforeKey();
		assert.deepEqual(result, expected, "should get the beforeKey with getter/setter method");
		let newProperty = 123;
		formation.beforeKey(newProperty);
		result = formation.beforeKey();
		assert.deepEqual(result, newProperty, "should get the new beforeKey with getter/setter method");
	});
	
	it("should get/set the formation afterKey property", function(){
		let expected, result;
		let formation = new Formation();
		
		expected = 0;
		result = formation.afterKey();
		assert.deepEqual(result, expected, "should get the afterKey with getter/setter method");
		let newProperty = 123;
		formation.afterKey(newProperty);
		result = formation.afterKey();
		assert.deepEqual(result, newProperty, "should get the new afterKey with getter/setter method");
	});
	
	it("should get/set the formation buckets property", function(){
		let expected, result;
		let formation = new Formation();
		
		expected = {};
		result = formation.buckets();
		assert.deepEqual(result, expected, "should get the buckets with getter/setter method");
		let newProperty = {2:4, 8: 16};
		formation.buckets(newProperty);
		result = formation.buckets();
		assert.deepEqual(result, newProperty, "should get the new buckets with getter/setter method");
	});
	

	it("test the bucket method", function(){
		let key = new Uint8Array([1,2]);
		let expected, result, i;
		let beforeKey = 5;
		let afterKey = 6;
		let formationData = {
			beforeKey : beforeKey,
			afterKey : afterKey
		}
		let formation = new Formation( key, formationData);
		
		i = 0;
		expected = beforeKey;
		result = formation.bucket(i);
		assert.equal(result, expected);
		
		i = 15;
		expected = beforeKey;
		result = formation.bucket(i);
		assert.equal(result, expected);
		
		i = 16;
		expected = afterKey;
		result = formation.bucket(i);
		assert.equal(result, expected);
		
		i = 1555;
		expected = afterKey;
		result = formation.bucket(i);
		assert.equal(result, expected);
		
		// try overriding the default value
		beforeKey = 32;
		afterKey = 64;
		formation.beforeKey(beforeKey);
		formation.afterKey(afterKey);
		
		i = 15;
		expected = beforeKey;
		result = formation.bucket(i);
		assert.equal(result, expected);
		
		i = 16;
		expected = afterKey;
		result = formation.bucket(i);
		assert.equal(result, expected);
		
		// try setting specific values
		let buckets = {15 : 123, 16 : 456};
		formation.buckets(buckets);
		
		i = 0;
		expected = beforeKey;
		result = formation.bucket(i);
		assert.equal(result, expected);
		
		i = 15;
		expected = buckets[i];
		result = formation.bucket(i);
		assert.equal(result, expected);
		
		i = 16;
		expected = buckets[i];
		result = formation.bucket(i);
		assert.equal(result, expected);
	});
	
	it("should return the common bit length of two keys", function(){
		let k1, k2, expected, result;
		let formation = new Formation();
		
		// empty keys should return a bucket of -1
		k1 = new Uint8Array(0);
		k2 = new Uint8Array([0b11111111]);
		expected = 0;
		result = formation.common(k1, k2);
		assert.equal(result, expected);
		result = formation.common(k2, k1);
		assert.equal(result, expected);
		
		// testing one two non empty keys
		k1 = new Uint8Array([0b11111111, 0b11111111]);
		k2 = new Uint8Array([0b11111111, 0b01111111 ]);
		expected = 8;
		result = formation.common(k1, k2);
		assert.equal(result, expected);
		
		k2 = new Uint8Array([0b11111111, 0b10111111 ]);
		expected = 9;
		result = formation.common(k1, k2);
		assert.equal(result, expected);
		
		k2 = new Uint8Array([0b11111111, 0b11011111 ]);
		expected = 10;
		result = formation.common(k1, k2);
		assert.equal(result, expected);
		
		k2 = new Uint8Array([0b11111111, 0b11101111 ]);
		expected = 11;
		result = formation.common(k1, k2);
		assert.equal(result, expected);
		
		k2 = new Uint8Array([0b11111111, 0b11110111 ]);
		expected = 12;
		result = formation.common(k1, k2);
		assert.equal(result, expected);
		
		k2 = new Uint8Array([0b11111111, 0b11111011 ]);
		expected = 13;
		result = formation.common(k1, k2);
		assert.equal(result, expected);
		
		k2 = new Uint8Array([0b11111111, 0b11111101 ]);
		expected = 14;
		result = formation.common(k1, k2);
		assert.equal(result, expected);
		
		k2 = new Uint8Array([0b11111111, 0b11111110]);
		expected = 15;
		result = formation.common(k1, k2);
		assert.equal(result, expected);
		
		k2 = new Uint8Array([0b11111111, 0b11111111]);
		expected = 16;
		result = formation.common(k1, k2);
		assert.equal(result, expected);
	});


    it("should calculate the bucket a given key belong to", function(){
        let formationKey = new Uint8Array([255,255]);
        let formation = new Formation(formationKey);

        let expected, result, keyPrefix, keySpan;

        // test a simple key without span
        keyPrefix = new Uint8Array([1]);
        keySpan = 0
        expected = 0;
        result = formation.keyBucket(keyPrefix, keySpan);
        assert.deepEqual(result, expected);

        // test a simple key without span
        keyPrefix = new Uint8Array([192]);
        keySpan = 0
        expected = 2;
        result = formation.keyBucket(keyPrefix, keySpan);
        assert.deepEqual(result, expected);

        // test a simple key without span
        keyPrefix = new Uint8Array([255,255]);
        keySpan = 0
        expected = 16;
        result = formation.keyBucket(keyPrefix, keySpan);
        assert.deepEqual(result, expected);

        // test a key with a span
        keyPrefix = new Uint8Array([0]);
        keySpan = 1;
        expected = 0;
        result = formation.keyBucket(keyPrefix, keySpan);
        assert.deepEqual(result, expected);

        // test a key with a span
        keyPrefix = new Uint8Array([192]);
        keySpan = 1;
        expected = 2;
        result = formation.keyBucket(keyPrefix, keySpan);
        assert.deepEqual(result, expected);

        // test a key with a span
        keyPrefix = new Uint8Array([255]);
        keySpan = 1;
        expected = 16;
        result = formation.keyBucket(keyPrefix, keySpan);
        assert.deepEqual(result, expected);
    })

	it("should affect a list of nodes to the buckets of a formation", function(){
        let formationKey = new Uint8Array([255,255]);
        let formation = new Formation(formationKey, {
				beforeKey : 3,
				afterKey : 3
		});
		let nodes = {
			"01" : [{prefix : new Uint8Array([0]), span : 0}], // should be bucket 0
			"02" : [{prefix : new Uint8Array([255]), span : 1}], // should be bucket 16
            "03" : [{prefix : new Uint8Array([0]), span : 0}], // should be bucket 0
            "04" : [{prefix : new Uint8Array([0]), span : 0}], // should be bucket 0
            "05" : [{prefix : new Uint8Array([0]), span : 0}], // should not be included since already 3 nodes are bucket 1
            "06" : [{prefix : new Uint8Array([255]), span : 0}], // should be bucket 8
            "07" : [{prefix : new Uint8Array([255,128]), span : 0}, {prefix : new Uint8Array([255]), span : 0} ], // should be bucket 9
		}
        let expected = {
			'0' : ["01", "03", "04"],
			"8" : ["06"],
			"9" : ["07"],
			"16" : ["02"]
		}

        // test a simple key without span
        let result = formation.mapKeysToBuckets(nodes);
		assert.deepEqual(result, expected);
	})

    it("should throw an exception when using complement without an array", function(){
		let formation = new Formation({key:new Uint8Array(0)});
		try{
			formation.complement("jhdisfhsdifhi");
			assert.notOk(true,"should have raised an exception");
		}
		catch(e){
			assert.ok(true, "exception raised as expected");
		}
	});
	
	it("should test the complement method", function(){
		let key = new Uint8Array([0b11111111, 0b11111111]);
		let formation = new Formation(key, {
			beforeKey : 2,
			afterKey : 4
		});
		let keys = [
			// complement bucket 0  should be set to 0
			{prefix : new Uint8Array( [0b01111111, 0b11111111, 0b11111111]), span : 0},
            {prefix : new Uint8Array( [0b01111111, 0b11111111, 0b11111111]), span : 0},
            {prefix : new Uint8Array( [0b01111111, 0b11111111, 0b11111111]), span : 0},
			
			// complement bucket 1 should be set to 0
            {prefix : new Uint8Array([0b10111111, 0b11111111]), span : 0},
            {prefix : new Uint8Array([0b10111111, 0b11111111]), span : 0},
			
			// complement bucket 2 should be set to 1
            {prefix : new Uint8Array([0b11011111, 0b11111111]), span : 0},
			
			// complement bucket 15 should be set to 0
            {prefix : new Uint8Array( [0b11111111, 0b11111110, 0b11111111]), span : 0},
            {prefix : new Uint8Array( [0b11111111, 0b11111110, 0b11111111]), span : 0},

			//complement bucket 16 should be set to 2
            {prefix : new Uint8Array( [0b11111111, 0b11111111, 0b01111111]), span : 0},
            {prefix : new Uint8Array( [0b11111111, 0b11111111, 0b01111111]), span : 0}
		];
		
		let complement = formation.complement(keys);
		
		assert.deepEqual( complement.key, formation.key, "formation and complement share the same key");
		assert.deepEqual( complement.beforeKey, formation.beforeKey, "formation and complement share the same beforeKey");
		assert.deepEqual( complement.afterKey, formation.afterKey, "formation and complement share the same afterKey");
		assert.equal( complement.bucket(0), 0, "asserting value of complement buckets");
		assert.equal( complement.bucket(1), 0, "asserting value of complement buckets");
		assert.equal( complement.bucket(2), 1, "asserting value of complement buckets");
		assert.equal( complement.bucket(3), 2, "asserting value of complement buckets");
		assert.equal( complement.bucket(15), 0, "asserting value of complement buckets");
		assert.equal( complement.bucket(16), 2, "asserting value of complement buckets");
		assert.equal( complement.bucket(17), 4, "asserting value of complement buckets");
	});
	
	it("should test the filterBuckets method", function(){
		let key = new Uint8Array([0]);
		
		let beforeKey = 2;
		let afterKey = 4;
		let buckets = {
			0 : 0,
			1 : 1,
			2 : 2,
			3 : 3,
			4 : 4,
			5 : 5
		};
		let formation = new Formation(key, {
			beforeKey : beforeKey,
			afterKey : afterKey,
			buckets : buckets
		});
			
		assert.deepEqual( formation.filterBuckets(0, +Infinity, 1,5), {1:1, 2:2, 3:3, 4:4, 5:5}, "testing the range of index");
		assert.deepEqual( formation.filterBuckets(0,3, 0,9), { 0:0, 1:1, 2:2,3:3, 6:2, 7:2}, "testing the superior range of value");
		assert.deepEqual( formation.filterBuckets(4, +Infinity, 0,10), { 4:4, 5:5, 8:4, 9:4, 10:4}, "testing the inferior range of value");
	});

});


describe("testing the reachNode method", function(){
	//build 256 flocks with a 1 byte key : add 10 closest nodes to each flock
	let nNodes = 1024;
	let nodePerFlock = 10;
	let nodes, flocks, nimetas;
	
	
	beforeEach(function(){
        shiftCounter = 1;

		nodes = [];
		flocks = [];
		nimetas = [];
		// create the nodes representing the flocks
		
		let address;
		for(let i=0; i<nNodes; i++){
			let spanwer;
			let throwError = false;
			do{
				throwError = false;
				try{
					spanwer = nestedInterfaceTestHelpers.createSpawner(i);
				}
				catch(e){
					throwError = true;
				}
			}
			while(throwError);
			
			// create a node
			let prefix = (i%256).toString(16);
			if(prefix.length % 2) prefix = "0"+prefix;
			let id = i.toString();
			if(id.length % 2) id = "0"+id;
			let node = {
				id : id,
				keys : [{ prefix : prefix, span : 0}],
				canals : spanwer.getCanals(),
			}
			nodes.push(node);
			
			// create nested interfaces
			let nimeta = new NIMeta();
			
			nimeta.addSpawner(spanwer);
			nimetas.push(nimeta);
			
			// create a flock
			let flock = new Flock( {
				id : node.id,
				keys : node.keys,
				metaInterface : nimeta,
                maxThreads : 100,
				cronInterval: 1000000
			});
			flocks.push(flock);
		}
		
		// for each flock, add the 10 closest nodes to it
		for(let i=0; i<nNodes; i++){
			let flock = flocks[i];
			for(let n=0; n<=8; n++){
				for(let nodeId = (parseInt(flock.getId())%256) ^ (n === 0 ? 0 : Math.pow(2,n-1)); nodeId<nNodes; nodeId += 256){
					let node = nodes[ nodeId ];
					if(node) flock.addNode(node);
				}
			}
		}
	});
		
	afterEach(function(){
		for(let flock of flocks){
			flock.stop();
		}

		for(let nimeta of nimetas){
			nimeta.stop();
		}

		// let enough time for various process to end
		return new Promise(res=>setTimeout(res,500));
	});

	it("should reject a reachNode method beacause of a timeout", function(done){
		let flock = flocks[0];
		let targetNode = nodes[nNodes-1];
		let targetKey = targetNode.keys[0];
		let operation = {
			prefix : targetKey.prefix,
			maxNodes : 1,
			maxConcurrentThreads : 1,
			timeout : 3
		};
		let reachNodePromise = flock.reachNode(operation);
		let resolved = false;
		let rejected = false;
		assert.instanceOf(reachNodePromise, Promise, "the result of a rechNode must be a promise");
		let t1 = Date.now();
		let t2;
		reachNodePromise.then(function(){
				resolved = true;
				t2 = Date.now();
			},
			function(m){ 
				rejected = true;
				t2 = Date.now();
		});
		
		shift(function(){
			assert.ok(rejected, "the promise of reachNode should have been rejected");
			assert.isBelow(t2-t1, 10, "the timeout should have occured quicker "+String(t2)+" "+String(t1));
			done();
		})
	});

	it("should use the reachNode method to access a node and establish a connection", function(done){
		let flock = flocks[0];
		let targetNode = nodes[nNodes-1];
		let targetKey = targetNode.keys[0];
		let msg;
		let operation = {
			prefix : targetKey.prefix,
			maxNodes : 1,
            maxThreads : 100
		};
		let reachNodePromise = flock.reachNode(operation);
		let reachedNodes = null;
		let resolved = false;
		let rejected = false;
		assert.instanceOf(reachNodePromise, Promise, "the result of a rechNode must be a promise");
		reachNodePromise.then(function(result){
				resolved = true;
				reachedNodes = result;
			},
			function(e){
		    msg = e;
				rejected = true;
		});
		
		shift(function(){
			assert.notOk(rejected, "the promise of reachNode should not have been rejected ::: "+msg);
			assert.ok(resolved, "the promise of reachNode should have been resolved");
			assert.isArray(reachedNodes, "the promise should resolve with an array containing the targeted node");
			assert.lengthOf(reachedNodes, 1, "only one node should be returned");
			let nodeResult = reachedNodes[0];
			assert.equal(nodeResult.id % 256, targetNode.id % 256, "the id of the result node should be the one of the targeted node");
			done();
			
		})
	});

    it("should use the reachNode method to access several nodes", function(done){
        let flock = flocks[0];
        let targetNode = nodes[nNodes-1];
        let targetKey = targetNode.keys[0];
        let operation = {
            prefix:targetKey.prefix,
            maxNodes : 4,
            maxThreads : 100
        };
        let reachNodePromise = flock.reachNode(operation);
        let reachedNodes = null;
        let resolved = false;
        let rejected = false;
        assert.instanceOf(reachNodePromise, Promise, "the result of a rechNode must be a promise");
        reachNodePromise.then(function(result){
                resolved = true;
                reachedNodes = result;
            },
            function(){
                rejected = true;
            });

        shift(function(){
            assert.notOk(rejected, "the promise of reachNode should not have been rejected");
            assert.ok(resolved, "the promise of reachNode should have been resolved");
            assert.isArray(reachedNodes, "the promise should resolve with an array containing the targeted node");
            assert.lengthOf(reachedNodes, 4, "expected more nodes to be returned");
            let nodeIds = reachedNodes.map(n=>n.id).sort(function(a,b){return a<b ? -1 : 1;});
            expectedIds = ["0255", "0511", "0767", "1023"];
            assert.deepEqual(nodeIds, expectedIds, "the id of the result node should be the one of the targeted nodes");
            done();
        })
    });


    it("should timeout the reachNode operation due to not enough nodes, but still calling the onreach callback for each valid node", function(done){
        let flock = flocks[0];
        let targetNode = nodes[nNodes-1];
        let targetKey = targetNode.keys[0];

        let callbackStack = [];
        let onreached = function(node){
            callbackStack.push(node);
        }

        let operation = {
            prefix:targetKey.prefix,
            maxNodes : 5, // only four nodes available
            maxThreads : 100,
            onreached : onreached,
            timeout : Math.floor( ASYNC_TIMEOUT/2 )
        };
        let reachNodePromise = flock.reachNode(operation);
        let reachedNodes = null;
        let resolved = false;
        let rejected = false;
        assert.instanceOf(reachNodePromise, Promise, "the result of a rechNode must be a promise");
        reachNodePromise.then(function(result){
                resolved = true;
                reachedNodes = result;
            },
            function(){
                rejected = true;
            });

        shift(function(){
            assert.ok(rejected, "the promise of reachNode should have been rejected after a timeout");
            assert.notOk(resolved, "the promise of reachNode should have been resolved");
            assert.lengthOf(callbackStack, 4, "expected more nodes to be returned");
            let nodeIds = callbackStack.map(n=>n.id).sort(function(a,b){return a<b ? -1 : 1;});
            expectedIds = ["0255", "0511", "0767", "1023"];
            assert.deepEqual(nodeIds, expectedIds, "the id of the result node should be the one of the targeted nodes");
            done();
        })
    })

    it("should reach some node but avoiding others with skipNodes", function(done){
        let flock = flocks[0];
        let targetNode = nodes[nNodes-1];
        let targetKey = targetNode.keys[0];

        let callbackStack = [];
        let onreached = function(node){
            callbackStack.push(node);
        }
		let skipNodes = {
        	"0255" : true
		}
        let operation = {
            prefix:targetKey.prefix,
            maxNodes : 5, // only four nodes available
            maxThreads : 100,
            onreached : onreached,
            timeout : Math.floor( ASYNC_TIMEOUT/2 ),
            skipNodes : skipNodes
        };
        let reachNodePromise = flock.reachNode(operation);
        let reachedNodes = null;
        let resolved = false;
        let rejected = false;
        assert.instanceOf(reachNodePromise, Promise, "the result of a rechNode must be a promise");
        let t1 = Date.now();
        let t2;
        reachNodePromise.then(function(result){
                resolved = true;
                reachedNodes = result;
                t2 = Date.now();
            },
            function(){
                rejected = true;
            });

        shift(function(){
            // assert.ok(rejected, "the promise of reachNode should have been rejected after a timeout");
            // assert.notOk(resolved, "the promise of reachNode should have been resolved");
            let nodeIds = callbackStack.map(n=>n.id).sort(function(a,b){return a<b ? -1 : 1;});
            assert.lengthOf(callbackStack, 3, "expected more nodes to be returned : "+JSON.stringify(nodeIds));

            expectedIds = ["0511", "0767", "1023"];
            assert.deepEqual(nodeIds, expectedIds, "the id of the result node should be the one of the targeted nodes");
            done();
        })
    })
	it("should use the reachNode method unsuccesfully(inexisting node) and return an empty array", function(done){
		let flock = flocks[0];
		let wrongPrefix = "010204";
		let operation = {
			prefix:wrongPrefix,
			maxNodes : 1,
            maxThreads : 20,
		};
		let reachNodePromise = flock.reachNode(operation);
		let reachedNodes = null;
		let resolved = false;
		let rejected = false;
		assert.instanceOf(reachNodePromise, Promise, "the result of a rechNode must be a promise");
		reachNodePromise.then(function(result){
				resolved = true;
				reachedNodes = result;
			},
			function(msg){
			console.log("REJECTED ::: ", msg)
				rejected = true;
		});
		
		shift(function(){
			assert.notOk(rejected, "the promise of reachNode should not have been rejected");
			assert.ok(resolved, "the promise of reachNode should have been resolved");
			assert.isArray(reachedNodes, "the promise should resolve with an array containing the targeted node");
			assert.lengthOf(reachedNodes, 0, "no node should be returned");
			done();
			
		})
	});

	it("should resolve a reachNode promise with empty array due to not enough threads", function(done){
		let flock = flocks[0];
		let targetNode = nodes[nNodes-1];
		let targetKey = targetNode.keys[0];
		let operation = {
			prefix:targetKey.prefix,
			maxNodes : 1,
			maxThreads : 6,
		};
		let reachNodePromise = flock.reachNode(operation);
		let resolved = false;
		let rejected = false;
		let res;
		assert.instanceOf(reachNodePromise, Promise, "the result of a rechNode must be a promise");
		reachNodePromise.then(function(result){
                res = result;
				resolved = true;
			},
			function(m){
				rejected = true;
		});
		
		shift(function(){
            assert.notOk(rejected, "the promise of reachNode should not have been rejected");
            assert.ok(resolved, "the promise of reachNode should have been resolved");
            assert.isArray(res, "the promise should resolve with an empty array");
            console.log(JSON.stringify(res) );
            assert.isEmpty(res, "the promise should resolve with an empty array");
			done();
		})
	});

    it("should establish multiple connections given a formation with populateFormation", function(done){
        let flock = flocks[0];
        let key = nodes[nNodes-1].keys[0];

        let formationData = {
            beforeKey : 1,
            afterKey : 0,
            buckets : {
                7 : 4
            }
        };
        let msg;
        let formation = new Formation( flock._flaptools.hexToBytes(key.prefix), formationData);
        let populateFormationPromise = flock.populateFormation({
            formation : formation
        });

        let resolved = false;
        let rejected = false;
        assert.instanceOf(populateFormationPromise, Promise, "the result of a populateFormation must be a promise");
        populateFormationPromise.then(function(result){
                resolved = true;
            },
            function(m){
                console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! rejection message : ", m);
                rejected = true;
            });
		shift(function(){})

        shift(function(){
            assert.notOk(rejected, "the promise of populateFormation should not be rejected");
            assert.ok(resolved, "the promise of populateFormation should be resolved");

            // retrieve the connected node ids
            let connectedNodes = flock.getConnectedNodes();
            let missingBuckets = Object.keys( flock.getMissingBuckets(formation, connectedNodes) );
            assert.lengthOf(missingBuckets, 0);
            done();
        })
    });

    it("should establish multiple connections with populate calling onpopulate callback", function(done){
        let flock = flocks[0];
        let key = nodes[nNodes-1].keys[0];
        let onpopulateStack = {
            0 : [],
            1 : [],
            2 : [],
            3 : [],
            4 : [],
            5 : [],
            6 : [],
            7 : [],
		};
        let onpopulateCallback = function(node, bucket){
			if(node) onpopulateStack[bucket].push(node);
		}
        let formationData = {
            beforeKey : 1,
            afterKey : 0,
            buckets : {
                7 : 5 // only four nodes are available
            }
        };
        let formation = new Formation( flock._flaptools.hexToBytes(key.prefix), formationData);
        let populateFormationPromise = flock.populateFormation({
            formation : formation,
			timeout : 3000,
			onpopulate : onpopulateCallback,
            maxConcurrentThreads: 8
        });

        shift(function(){})
        shift(function(){})
        shift(function(){})

        shift(function(){
            let populatedIds = onpopulateStack[7].map(n=>n && n.id);
            assert.lengthOf(populatedIds, 4, "checking population : "+JSON.stringify(populatedIds) );
        })

		shift(done)
    });

    it("should establish multiple connections according to the flock key during cron", function(done){
        let flock = flocks[nNodes-1];
        flock.withKeyFormations = true;
        flock.cronInterval( ASYNC_TIMEOUT );
        let key = nodes[nNodes-1].keys[0];

        let connectedNodes = flock.getConnectedNodes();
        let formation = new Formation( flock._flaptools.hexToBytes(key.prefix));
        let missingBuckets = Object.keys( flock.getMissingBuckets(formation, connectedNodes) );
        assert.lengthOf(missingBuckets, 8);

        shift(function(){
            let t2 = Date.now();
        	let expectedFormationId = flock.keyId(key);
        	assert.property( flock.formations, expectedFormationId);
        	let formation = flock.formations[expectedFormationId];
            // retrieve the connected node ids
            let connectedNodes = flock.getConnectedNodes();
            let missingBuckets = Object.keys( flock.getMissingBuckets(formation, connectedNodes) );
			assert.lengthOf(missingBuckets, 0);
            let t3 = Date.now();
        }, 3)

		shift(done);
    });

});



 try{
    module.exports = {
        nestedInterfaceTestHelpers : nestedInterfaceTestHelpers,
    }
}
catch(e){}
