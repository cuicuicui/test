/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/


// general libraries
global.chai = require("chai");

// tested modules
var flocklibs = require("../../../dist/flock-nodejs.js")
Object.assign(global, flocklibs);


// websocket specific testing DATA
var nestedInterfaceTestHelpers = {
    serverAddress : "ws://127.0.0.1",
    serverStartPort : 4000,
    uniqueSpawnerPortStart : 3000
}

// create a spawn connection associated to a unique spawner, represented by an integer
nestedInterfaceTestHelpers.createSpawn = function(i=0){
    return new NIWebsocketSpawn({serverAddress : "ws://127.0.0.1:"+(nestedInterfaceTestHelpers.serverStartPort + i) });
}

nestedInterfaceTestHelpers.createSpawner = function( i=0 ){
    let port = (nestedInterfaceTestHelpers.serverStartPort + i);
    let address = nestedInterfaceTestHelpers.serverAddress + ":" + port;
    return new NIWebsocketSpawner({ socketPort : port, address : address});
}

// load the unit tests and push the specific testing data inside it
var tuLib = require("./TU_flock.js");

for(let i in nestedInterfaceTestHelpers){
    tuLib.nestedInterfaceTestHelpers[i] = nestedInterfaceTestHelpers[i];
}