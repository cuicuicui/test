/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/


class Flock {
	constructor(data={}){

        this.metaInterface = data.metaInterface || new NIMeta();

		this.flockInit();
		this._flaptools = data.flaptools || new Flaptools();
		// build a thredops
		this._threadops = this._flaptools.threadops({
			maxThreads : data.maxThreads || 100,
			maxConcurrentThreads : data.maxConcurrentThreads || 1,
		});
		
		this.port = data.port || 1;

		this.keys = data.keys ? data.keys.slice() : [this.generateKey(32)];
		this.withKeyFormations = !!data.withKeyFormations;
		this.formationDensity = 1;
		this.formations = {}; // indexed by formationId = key + "/" + span
		this._formationOperations = {};
		this.id = "id" in data ? data.id : this.generateId();
		this.metaInterface = data.metaInterface || new NIMeta();

		this.nodeExpireTime = data.nodeExpireTime || 10000;
        this.cronIntervalId = null;
		this._cronInterval = data.cronInterval || 1000;
		this.density = data.density || 3; // number of active node per bucket
		this.iddleDensity = data.iddleDensity || 100; // number of inactive nodes (that are kept just as information)
		this.buckets = {};
		this.nodes = {};
		this._nid = 1; // connections id for internal usage only
		this.canals = [];
		this.maxInterfacedNodes = data.maxInterfacedNodes || 256;

		// add nodes if provided
		if(data.nodes){
			for(let node of data.nodes){
				if(!node.id){
				    // if no id is provided, requests the node before adding it to the list of available nodes
				    this.getNodeInfo(node);
                }
                else{
                    this.addNode(node);
                }

			}
		}

		// bind methods
        if(!this._keyToBinary_bound) this._keyToBinary_bound = this._keyToBinary.bind(this);

		// ongoing operations contains the following properties :
		// type : the name of operation (getNode, getInfo)
		// iterations : number of iterations left before generating an error (for instance, when a getNode operation requires to many calls to adjacent nodes)
		// called : list of nodes already called during the operation
		// threads : number of parallel requests running to perform a task
		// freeThreads : number of threads left for the task
		// onsuccess : success callback
		// onerror : error callback
		// startTime : beginning of the request
		// timeout : number of ms after which the request expire
		// data : data relative to the request
		
		// list of ongoing requests (getNodes, getInfo....) : ensure that newly generated requestId doesn't match those of registered requests
		this.requests = {};
		// list of ongoing operations (reachNode, populate...)
		this.operations = {};
		// list of formations to keep populated
		this._formations = [];
		
		this.start();
		this.initializeInterface(this.metaInterface);
	}
	
	////////////////////////////////////////////////////////////////////
	///////////// getters and setters //////////////////////////////////
	////////////////////////////////////////////////////////////////////
	
	getKeys(){ 
		return this.keys.map(a=>{return {prefix : a.prefix, span : a.span} });
	}

	addKey(key){
		if(typeof key !== "object") key = {prefix : key, span : 0}
		if(!this.hasKey(key)) this.keys.push(key);
	}

	removeKey(key){
        let prefix = typeof key === "object" ? key.prefix : key;
        this.keys = this.keys.filter(k=>k.prefix!== prefix);
	}

	// check if has the given key
	hasKey(key){
		let prefix = typeof key === "object" ? key.prefix : key;
		for(let k of this.keys){
			if(k.prefix == prefix) return true;
		}
		return false;
	}

	getId(){ 
		return this.id;
	}
	
	////////////////////////////////////////////////////////////////////
	//////////// flock management methods //////////////////////////////
	////////////////////////////////////////////////////////////////////
	
	start(){
		let flock = this;

		this.restartCron();
	}
	
	stop(){
		// stop the cron
		this.stopCron();
		
		// close all connections
		if(this.metaInterface) this.metaInterface.stop();
		for(let i in this.connections){
			this.connections[i].connetion.interface.close();
		}
		this.buckets = {};
		
		this.nodes = {}; // nodeId => node

		// stop all the pending operations
        this._threadops.rejectOperations();
	}

	cronInterval(newCronInterval=null){
		if(newCronInterval){
			this._cronInterval = newCronInterval;
			if(this.cronIntervalId !== null) this.restartCron();
		}
		return this._cronInterval;
	}

	restartCron(){
		this.stopCron();
        let cronMethod = this.cron.bind(this);
        this.cronIntervalId = setInterval(cronMethod, this._cronInterval);
	}

	stopCron(){
        if(this.cronIntervalId !== null && this.cronIntervalId !== undefined) clearInterval(this.cronIntervalId);
        this.cronIntervalId = null;
	}
	
	cron(){
		// remove expired nodes
		for(let id in this.nodes){
			let now = Date.now();
			let node = this.nodes[id];
			if(now - node.lastUpdate < this.nodeExpireTime) continue;
			else if(node.interface && node.interface.status() === "UP"){
                // only get node info for connections that are multi messages
				if(node.interface && node.interface.multiExchanges) this.getNodeInfo(node);
			}
			else{
				let n = this.nodes[id];
				delete this.nodes[id];
            }
		}
		// create formations for keys
		if(this.withKeyFormations){
			let keys = this.getKeys();
			for(let key of keys){
				let keyId = this.keyId(key);
				let byteKey = this._flaptools.hexToBytes(key.prefix);
				if(!this.formations[keyId]) this.formations[keyId] = new Formation(byteKey, { beforeKey : this.formationDensity, afterKey : 0 });
			}
		}

		// create operations for the formation
		for(let formationId in this.formations){
			let currentOp = this._formationOperations[formationId];
			if(currentOp && currentOp.done) delete this._formationOperations[formationId];
			if(!this._formationOperations[formationId] ){
				let operationData = {
					formation : this.formations[formationId],
				};
                this.populateFormation(operationData);
				this._formationOperations[formationId] = operationData;
			}
		}

		// display some stats in the logs
		let nodesNumber = this.getNodes().length;
		let connectedNodesNumber = this.getConnectedNodes().length;
        if(typeof document !== "undefined") document.connectedNodes = this.getConnectedNodes();
        if(typeof document !== "undefined") document.flock = this;
	}
	
	initializeInterface(ni){
        ni.addHook(ni.e.RECEIVE, this.processRequests.bind(this), this.port );
        ni.addHook(ni.e.SPAWN, this.addSpawnedNodes.bind(this));
	}

	// add new spawned interface to the list of nodes
	addSpawnedNodes(event){
		let $this = this;
		// execute in timeout to let time to other hooks to add it as a node
		setTimeout(function(){
		    if(!$this.canOpenConnection()){
		        console.log("addSpawnedNodes : Cannot create new connection, close spawn");
		        event.spawn.stop();
		        return;
            }
			// check whether or not there is already a node using that interface
			let interfaceNode = $this.nodeFromInterface(event.spawn);
		    // don't add node when interface is only for a single exchange of data
		    if(event.spawn.multiExchanges){
                $this.getNodeInfo(interfaceNode);
                $this.addNode(interfaceNode);
			}

		}, 1)
	}
	
	////////////////////////////////////////////////////////////////////
	//////////// connections related methods ///////////////////////////
	////////////////////////////////////////////////////////////////////

	nodeCheckup(){
		for(let nodeId in this.nodes){
			let node = this.nodes[nodeId];
			if(!node.interface) continue;
			let status = node.interface.status();
			if(status !== "UP" && status !== "NEW") this.removeNode(node);
		}
	}

	// initiate connections with foreign nodes matching a Formation object
	populateFormation(op={}){
		if(!(op.formation && op.formation instanceof Formation)) throw "populateFormation error : expect a Formation as the formation to populate in the options";
		op.thread = this._populateFormationThread.bind(this);
		op.name = "populateFormation for formation '"+op.formation.key().join(",")+"'";
		// keep the list of the buckets that are beeing investigated by a thread
		op.processedBuckets = {};
		// for each bucket, keep the list of visited node for reachnode operation
		op.visitedNodes = {};
		// keep the number of pending threads for a given bucket
		op.pendingThreads = {};
		op.connected = {}
		// keep a track of nodes (per bucket) that have been passed to the onpopulate callback
		op.advertisedNodes = {}; // bucket=>{ nid=> nid }
		op.retroactiveCallback = true;
		if(!op.skipNodes) op.skipNodes = {};
		// skip nodes for the reach operation
		op.skipNodesReachOperation = Object.assign({}, op.skipNodes);
        // retrieve the connected nodes, without the skipped nodes
        let $this = this;
        op.connectedNodes = function(){
            let connectedNodes = $this.getConnectedNodes();
             // let res = connectedNodes.filter(n=>!op.skipNodes.includes(""+n.id));
            let res = connectedNodes.filter(n=>!op.skipNodes[n.id]);
            return res;
        }

        // check all available connected nodes : for each one of them, call the onpopulate once per request
        op.advertiseNewPopulationNodes = function(){
            if(!op.onpopulate) return;
            let connectedNodes = op.connectedNodes();
            // build binary keys of the connected nodes
            let binaryNodes = {};
            for(let node of connectedNodes) binaryNodes[node.id] = node.keys.map($this._keyToBinary_bound)
            let population = op.formation.mapKeysToBuckets(binaryNodes);

            for(let bucket in population){
                for(let nid of population[bucket]){
                    if(!op.advertisedNodes[bucket]) op.advertisedNodes[bucket] = {};
                    if(!(nid in op.advertisedNodes[bucket])){
                        op.advertisedNodes[bucket][nid] = nid;
                        op.onpopulate( $this.getNode(nid), bucket );
                    }
                }
            }
        }

		return this._threadops.startOperation(op);
	}

	//
	_populateFormationThread(op){
		let $this = this;
		// if(this.getKeys()[0].prefix !== "00") return Promise.resolve();
		// retrieve the list of buckets where there are missing nodes
		let connectedNodes = op.connectedNodes();
		let missingBuckets = this.getMissingBuckets(op.formation, connectedNodes);
		// determine a bucket to investigate
		let minV = +Infinity;
		let targetBucket = -1;
		let hasMissingBuckets = false;
		let testmsb;

        op.advertiseNewPopulationNodes();

		// retrieve the missing buckets and target one to request
		for(let i in missingBuckets){
            hasMissingBuckets = true;
            testmsb = i;
            if(!op.processedBuckets[i]) op.processedBuckets[i] = 0;
            if(!op.pendingThreads[i]) op.pendingThreads[i] = 0;
            // start no more that a thread per missing node per bucket, and target the bucket that have least been treated
			if(op.pendingThreads[i] < missingBuckets[i] && minV >= (op.processedBuckets[i]) ){
				minV = op.processedBuckets[i];
				targetBucket = i;
			}
		}
        // if there are no empty buckets, the operation is a success
        if(!hasMissingBuckets){
            this._threadops.resolveOperation(op.id);
            return Promise.resolve();
		}

		// if all the buckets have pending operations, don't start anything
		if(targetBucket === -1){
            let res = this._threadops.sleep(1000);
			return res;
		}

		// increment this bucket in order to process all buckets equally
		op.processedBuckets[targetBucket]++;
        op.pendingThreads[targetBucket]++;
		let decreasePendingThreads = function(e){
			op.pendingThreads[targetBucket]--;
		}

		// launch a reachNode request for the bucket
		let targetKey = this.inverseSuffix( this._flaptools.bytesToHex(op.formation.key()), targetBucket);
		let fl = this;
		if(!op.visitedNodes[targetBucket]) op.visitedNodes[targetBucket] = {};

		// build a callback to connect nodes once they are reached
		let onreachedCallback = function(node){
			// connect reached nodes
			// NB : promise resolution is checked when the rechNode operation is fully resolved (see callback below)
            //let connectionProm =
			fl.connectNode(node).then(function(){
				op.advertiseNewPopulationNodes();
            })
			.catch(function(){})
		}
		let prom = this.reachNode({
			maxNodes : missingBuckets[targetBucket],
			prefix : targetKey,
			timeout : op.timeout || 0,
			visitedNodes : op.visitedNodes[targetBucket],
            skipNodes : op.skipNodesReachOperation,
			onreached : onreachedCallback,
            withOwnNode : false,
			precision : parseInt(targetBucket),
		}).then(function(nodes){
            // after reaching the nodes, connect to them
            let connectProms = [];
            for(let node of nodes){
            	// prevent the populate operation from asking for the same node twice
                op.skipNodesReachOperation[node.id] = true;
                // it might not trigger a new connection if the node is already connected or connecting (which is the case since node connection are performed one by one)
                let connectProm = fl.connectNode(node);
				connectProms.push(connectProm);
            }
            let promall = Promise.all(connectProms);
            promall.then(function(){
                    op.connected[targetBucket] = op.connected[targetBucket] ? op.connected[targetBucket]+1 : 1;
                },
                function(e){
                    console.log("ENDCONNECT ERROR", e)
                })

            return promall;
        })
		// once thread is done or an error occured, allow the creation of a new thread for this bucket
		.catch(function(e){
			// console.log("errorrrrrrrrrrr ::::", e)
		}).then(decreasePendingThreads);

		return prom;
	}

	// try to reach a node : return a promise when it is done, options may/must specify (* for facultative) :
	// prefix : the key to reach and connect
	reachNode(op={}){
		if(!op.prefix) throw "reachNode error : expect an hexadecimal as the prefix to reach in the options";
		if(!op.precision) op.precision = op.prefix.length * 4;
		op.visitedNodes = op.visitedNodes || {};
		op.maxNodes = op.maxNodes || 1;
		op.name = "reachNode for prefix '"+op.prefix+"'";
		if(!op.skipNodes) op.skipNodes = {};
        op.resolveAtBest = this._reachAtBest.bind(this);
		op.thread = this._startReachNodeThread.bind(this);
		if(!op.hasOwnProperty("withOwnNode")) op.withOwnNode = true;
		// build the list of matching nodes that have already been processed by onreached
		op.reachedList = {};
		// build the target key from the prefix
		op.key = { prefix : op.prefix, span : op.span || 0};
		// save some stats as the operation progress
		if(!op.stats) op.stats = {};
		op.stats.bestBucket = 0;
		op.stats.hexKey = op.prefix;
		op.stats.byteKey = this._flaptools.hexToBytes(op.prefix);
		op.stats.formation = new Formation(op.stats.byteKey);
		return this._threadops.startOperation(op);
	}

	_reachAtBest(operation){
        let reachedNodes = this.reachNodeLocal(operation.key, false, operation.maxNodes, operation.skipNodes, operation.precision, operation.withOwnNode);
        this._threadops.resolveOperation(operation.id, reachedNodes);
    }

	// start a thread to reach a given node
	_startReachNodeThread( operation ){
        // check if there are enough local nodes to resolve the operation
        let flock = this;
        let tryResolve = function(r){
            // retrieve the list of nodes that match the target
            let reachedNodes = flock.reachNodeLocal(operation.key, false, operation.maxNodes, operation.skipNodes, operation.precision, operation.withOwnNode);
            // before resolving the reach operation, launch a callback for each new matching node found
            if(operation.onreached){
                for(let node of reachedNodes) {
                    let nid = node.id;
                    if (!operation.reachedList[nid]) {
                        operation.reachedList[nid] = true;
                        operation.onreached(node)
                    }
                }
			}
			// if enough nodes have been found, resolve the operation
            if(reachedNodes.length >= operation.maxNodes){
                try{
                    flock._threadops.resolveOperation(operation.id, reachedNodes);
				}
                catch(e){
            		console.log("reach resolve error", e)
				}
            }
        }
        tryResolve();
        if(operation.done) return Promise.resolve();

		// find the closest node to the target that have not been visited yet
		let nodes = [];
		let now = Date.now();
		for(let i in this.nodes){
			let node = this.nodes[i];
			// pick nodes that have not already been visited
			if(node.id && !operation.visitedNodes[node.id])  nodes.push(node);
		}
		let closestNodes = this.closestNodes(operation.key, 1, false, nodes, true);
		let closestNode = closestNodes.pop();
		if(!closestNode){
		    return this._threadops.sleep();
		}

		// update the closest bucket found
		let bestBucket = 0;
		for(let key of closestNode.keys){
            bestBucket = Math.max(bestBucket, operation.stats.formation.keyBucket(this._flaptools.hexToBytes(key.prefix || "") , 0) );
		}
        operation.stats.bestBucket = bestBucket;

        // contact the node to ask targeted key
		let options = {
			k : 4,
		}
		let resultPromise = this.getNodeNode(closestNode, operation.key.prefix, options).then(tryResolve,tryResolve);

		// mark the newly visited node as visited
		operation.visitedNodes[closestNode.id] = true;

		return resultPromise;
	}

	// retrieve local nodes that match a given node
	reachNodeLocal(key, nodes=false, maxResults=5, skipNodes = {}, precision=null, withOwnNode=true){
		if(!nodes) nodes = this.getNodes();
		if(!precision) precision = key.prefix.length*4;
        if(withOwnNode) nodes.push(this.getOwnNode());
		nodes = nodes.filter(node=>!(node.id.toString() in skipNodes));
        nodes = this.closestNodes(key, maxResults, false, nodes);
        while(nodes.length){
            let match = false;
            for(let nodeKey of nodes[nodes.length-1].keys){
                if(this.isIncluded(key, nodeKey, precision)){
                    match = true;
                    break;
                }
            }
            if(match) break;
            else nodes.pop();
        }
        return nodes;
	}

	removeNodes(){
		for(let nid in this.nodes) this.removeNode(nid);
	}
	
	removeNode(nid){
        if(this.nodes[nid]) delete this.nodes[nid];
	}

	// getter for a specific node
	getNode(nid){
		return this.nodes[""+nid] || null;
	}
	
	addNode(n){
		// duplicate the node
		// n = Object.assign({},n);
		// if(n.interface && !n.interface.multiExchanges){
		// 	delete n.interface;
		// }

		// a node must at least have an id and not be the flock itself
		if(!n.hasOwnProperty("id") || n.id === this.getId()){
			return;
        }
		
		//don't push already present nodes
		if(this.nodes[n.id]){
			return;
        }

		this.nodes[n.id] = n;
		
		// create an empty key if the node has none
		if(!n.keys) n.keys = [];

		// set the last time the node data has been updated
		n.lastUpdate = Date.now();
	}

	// check if a new connection can be open (or not if the limit is reached)
    canOpenConnection(limit=this.maxInterfacedNodes){
        let interfacedNodes = this.filterNodes(null, ["UP", "NEW"]);
        return interfacedNodes.length < limit;
    }
	
	// connect a given node n, trying to use one of the available canal
	// return a promise that resolves with the nestedInterface or reject with an error message
	connectNode(n){
		// if the node has already a connected interface, return it
		if(n.interface && n.interface.status() == "UP") return Promise.resolve(n.interface);
		// if there is a pending promise for the interface, return it
        if(n.interfacePromise) return n.interfacePromise;

		// retrieve a canal that can be spawned and create a new NestedInterface
		let canal = this.metaInterface.canSpawnAny(n.canals || []);

		// if there is no valid canal to create a connection return a promise reject
		if(!canal) return Promise.reject("no matching canal for connection ::: "+ JSON.stringify(n.keys) + " ::: " + JSON.stringify(n.canals) );

		// throw if there are too much interfaces created

		if(!this.canOpenConnection()) return Promise.reject("connectNode error : maximum interfaced nodes reached : "+this.maxInterfacedNodes);

		let prom = this.metaInterface.spawn(canal).then(
		function(spawn){
			n.interface = spawn;
			return spawn;
		});

		let $this = this;
		let delProm = (e)=>{ delete n.interfacePromise};

		prom = Promise.race([prom, new Promise((res,rej)=>setTimeout(rej,2000))]);
        // save the request in order to perform connection only once
        n.interfacePromise = prom.then(delProm).catch(
        	function(e){
        		// let a promise rejection as the result, since no connection could be established
				return Promise.reject("failed to connect to node : "+JSON.stringify(e) );
            });
		return prom;
	}
	
	// get the info from a node, and push it to the node
	// return a promise
	getNodeInfo(n){
		let prom = this.connectNode(n);
		let fl = this;
		let res = prom.then(function(spawn){
			let getInfoRequest = {
				requestId : fl._generateRequestId(),
				getKeys : true,
				getCanals : true,
				getId : true,
				precision : 4095,
			};
			let bytes = fl.getInfoToBytes(getInfoRequest);
			spawn.send(bytes, fl.port, fl.port);
			getInfoRequest.node = n;
			fl.addRequest(getInfoRequest);
			let promResult = new Promise(r=>getInfoRequest.resolve=r);
			return promResult;
		})
		.then(function(r){
			return r;
		});
		return res;
	}
	
	// send the getNode request on a given node
	// return a promise
	getNodeNode(node, targetPrefix, opt={}){
		let prom = this.connectNode(node);
		let fl = this;
		let res = prom.then(function(spawn){
			let getNodeRequest = {
				requestId : fl._generateRequestId(),
				prefix : targetPrefix,
				precision : opt.precision || 2,
				k : opt.k || 4,
			};
			let bytes = fl.getNodeToBytes(getNodeRequest);
			spawn.send(bytes, fl.port, fl.port);
			fl.addRequest(getNodeRequest);
			let promResult = new Promise(r=>getNodeRequest.resolve=r);
			return promResult;
		})

		return res;
	}
	
	closestNodes(key, k, withOwnNode=false, nodes=null , squawkerPriority=false){
        if(!nodes) nodes = this.getNodes();
		if(withOwnNode) nodes.push(this.getOwnNode());
		// only keep nodes with at least one key or nodes that are considered as squawkers
		nodes = nodes.filter(n=>!!n.keys.length || (squawkerPriority && n.squawker) );
		let nodeIndex = nodes.map((v,i)=>i);
		let fl = this;
		// take the closest key of each node : nodes that are squawkers are affected a constant distance
		let dist = nodes.map((v,i)=>squawkerPriority && v.squawker ? fl.SQUAWKER_DEFAULT_DISTANCE : fl.minDist(key, v.keys));
		// sort each node by their closest key
		nodeIndex.sort(function(i1,i2){
			return fl.isInf( dist[i1], dist[i2]) ? -1 : 1;
		});
		return nodeIndex.slice(0,k).map((i)=>nodes[i]);
	}

	// check whether the node from the interface exists
	nodeExists(nodeInterface){
        let node = this.getNodes().find(n=>n.interface === nodeInterface);
		return node;
	}

	// retrieve a node data object from an interface 
	// if no node data object exist, create and store one
	nodeFromInterface(nodeInterface, opts={}){
		let node = this.nodeExists(nodeInterface);
		if(!node){
			node = {
				interface : nodeInterface,
				id : this.generateId()
			};
		}
		return node;
	}
	
	// build the node representing itself
	getOwnNode(){
		let ownNode = {
			id : this.getId(),
			keys : this.getKeys(),
			canals : this.getCanals(),
		}
		ownNode.canals.push({type : this.CANAL_CURRENT_CONNECTION});
		return ownNode;
	}
	
	// retrieve all the nodes, in an array
	getNodes(){
		return Object.values(this.nodes);
	}
	
	getConnectedNodes(nodes=null){
		if(!nodes) nodes = this.getNodes();
		let connectedNodes = nodes.filter(n=>n.interface && n.interface.status() === "UP");
		return connectedNodes;
	}

	// filter nodes given a list of status
	filterNodes(nodes=null, status=[]){
		if(!nodes) nodes = this.getNodes();
        // aux method for the filterNodes method
        let filterMethod = function (node){
            // if no status is specified, accept all nodes
            if(!status.length) return true;
            if(status.includes("NEW") && node.interfacePromise) return true;
            if(!node.interface) return false;
            let nodeStatus = node.interface.status();
            // if a connection is beeing made
            return status.includes(nodeStatus)
        }
        let filtered = nodes.filter(filterMethod);
        return filtered;
	}

	// retrieve the buckets whith not enough nodes in it
	getMissingBuckets(formation, nodes=null){
		if(!nodes) nodes = this.getNodes();
		let keys = [].concat(...nodes.map(n=>n.keys||[]));
		let flaptools = this._flaptools;
		let cpm = formation.complement(keys.map(this._keyToBinary_bound));
		return cpm.filterBuckets(1, +Infinity, 0, cpm.key().length*8 + 32);
	}

	// transform a list of nodes to an array of nodes where keys are in binary array
	nodesToBinaryArray(nodes){

	}

	// transform a key obect in binary data for prefix
	_keyToBinary(key){
        return { prefix : this._flaptools.hexToBytes(key.prefix), span : key.span}
	}

	////////////////////////////////////////////////////////////////////
	/////////////// requests methods ///////////////////////////////////
	////////////////////////////////////////////CONNECTNODE ::::////////////////////////
	
	getInfo(){
		
	}
	
	startGetNodeThread(){
		
	}
	
	getNodeThreadCallback(){
		
	}
	
	startGetInfoThread(){
		
	}
	
	getInfoThreadCallback(){
		
	}
	
	populate(){
		
	}
	
	// getter and setters for managed formations
	getFormations(){
		return this._formations.slice();
	}
	addFormation(f){
		if(f instanceof Formation) this._formations.push(f);
	}
	removeFormation(f){
		let i = this._formations.indexOf(f);
		if(i === -1) return;
		this._formations.splice(i,1);
	}
	
	// add a pending request
	addRequest(req={}){
		if(!req.requestId) req.requestId = this._generateRequestId();
		this.requests[req.requestId] = req;
		if(!req.startTime) req.startTime = Date.now();
		return req.id;
	}
	
	getRequest(id){
		return this.requests[id] || null;
	}
	
	removeRequest(id){
		if(this.requests[id]) delete this.requests[id];
	}
	
	// add the nodes to the list of nodes
	_processGiveNode(answer){
		// check that the request id of the giveNode match a requestId of a getNode request
		let req = this.getRequest(answer.requestId);
		if(!req){
		    return;
        }
		let nodes = answer.nodes || [];
		for(let node of nodes){
			let keys = [];
			for(let keyData of node.keys||[]){
				let commonPrefix = req.prefix.substr(0, keyData.common*2) + keyData.prefix;
				let prefixLength = Math.min(req.prefix.length, commonPrefix.length);
				let prefix = commonPrefix.substr(0, prefixLength);
				let span = (keyData.span||0) + (commonPrefix.length - prefixLength );
				keys.push({ prefix : prefix, span : span, hiddenLength : keyData.hiddenLength||0} );
			}
			node.keys = keys;
			this.addNode(node);
		}
		if(req.resolve) req.resolve(nodes);
		this.removeRequest(answer.requestId);
	}
	
	_processGetNode(req){
		let res = {};
		res.version = 0;
		res.method = "giveNode";
		res.requestId = req.requestId;
		res.precision = req.precision || 0;
		let reqKey = { prefix : req.prefix, span : 0};

		let nodes = this.closestNodes(reqKey, 2*req.k, true); // take twice the number of requested nodes in case of invalid nodes
		let connectedNodes = this.getConnectedNodes(nodes);
		if(connectedNodes.length >= req.k) nodes = connectedNodes;

		// duplicate nodes with filtered canals so as to send only available interfaces
		res.nodes = []; 
		for(let node of nodes){ 
			let canals = []; 
			for(let canal of node.canals){ 
				let shouldAddCanal = this.isCanalValid(canal);
				if(shouldAddCanal) canals.push(canal);
			}
			// only push non empty canals
			if(canals.length === 0) continue;
			let pushedNode = {};
			res.nodes.push(pushedNode);
			pushedNode.canals = canals;
			pushedNode.id = node.id;
			// compute the common length between the request and node keys
			pushedNode.keys = [];
			for(let nk of node.keys){
				let common = Math.min( nk.prefix.length/2, this.common(nk, reqKey) );
				pushedNode.keys.push({
					prefix : nk.prefix.substr(2*common, 2*res.precision),
					common : common,
					span : nk.span,
					hiddenLength : (nk.hiddenLength || 0) + Math.max(0, nk.prefix.length/2 - common - res.precision)
				});
			}
			if(res.nodes.length == req.k) break;
		}
		return res;
	}

    /***
	 * check if a canal is valid
     */
    isCanalValid( canal ){
        switch(canal.type){
            case this.CANAL_CURRENT_CONNECTION :
            	return false;
            	break;
            case this.CANAL_HTTP :
            case this.CANAL_WEBSOCKET :
                return !!canal.address;
                break;
            case this.CANAL_WEBRTC :
                // can use webrtc if a remote address is specified or if the node is connected and a receiver is registered
				if(!canal.canals || !Array.isArray(canal.canals)) return false;
				for(let c of canal.canals){
					if(this.isCanalValid(c)) return true;
				}
                return false;
                break;
			case this.CANAL_SWANS :
                if(!canal.canals || !Array.isArray(canal.canals)) return false;
                if(!canal.receiver || typeof canal.receiver !== "string") return false;
                for(let c of canal.canals){
                    if(this.isCanalValid(c)) return true;
                }
                return false;
                break;
			default :
				return false;
        }
	}
	
	// add the info given by the node itself
	_processGiveInfo(answer){
		// check that the request id of the giveInfo match a requestId of a getInfo
		let req = this.getRequest(answer.requestId);
		if(!req) return;
		let node = req.node || {};
		let oldId = req.node ? req.node.id : null;
		let answerNode = answer.nodes[0];
		if(answerNode.keys) node.keys = answerNode.keys.map(kdata=>{ return {prefix : kdata.prefix, span : kdata.span }});
		if(answerNode.canals) node.canals = answerNode.canals;
		if(answerNode.id) node.id = answerNode.id;
		if(oldId && oldId !== node.id && this.nodes[oldId] == node){
            delete this.nodes[oldId];
		}
		if(node.id) this.addNode(node);
		this.removeRequest(answer.requestId);
		if(req.resolve) req.resolve(node);
	}
	
	_processGetInfo(req){
		let res = {};
		res.version = 0;
		res.method = "giveInfo";
		res.requestId = req.requestId;

		let resNode = {};

		// add the id if requested
		res.getId = req.getId;
		if(res.getId) resNode.id = this.getId();
		
		// add the key if requested
		res.getKeys = req.getKeys;
		if(res.getKeys) resNode.keys = this.keys.map(function(k){ return { prefix : k.prefix.slice(0, 2*req.precision), span : k.span} });
		// add the canals if requested
		res.getCanals = req.getCanals;
		if(res.getCanals) resNode.canals = this.getCanals();
		res.nodes = [resNode];
		return res;
	}
	
	// process incoming requests
	// event : data received from a nested interface
	processRequests(event){
		let req = this.bytesToObject(event.message);
		let answer;
		try{
			switch(req.method){
				case "getNode" :
					answer = this._processGetNode(req);
					break;
				case "giveNode" :
					this._processGiveNode(req);
					break;
				case "getInfo" :
					answer = this._processGetInfo(req);
					break;
				case "giveInfo" : 
					this._processGiveInfo(req);
					break;
				default : break;
			}
			// send answer when there is one
			if(answer){

				let answerBytes = this.objectToBytes(answer);
				if(event.interface.status() === "UP") event.interface.send(answerBytes, event.fport, this.port);
			}
		}
		catch(e){ console.log(e)}
    }
	
	// transform a byte request into the associated request object
	bytesToObject(bytes){
		// extract the version and the type of message
		let metadata = this._flaptools.decode(bytes, this.TEMPLATE_METADATA);
		if(metadata.version != 0) throw "Unsupported version : " + metadata.version;
		switch(metadata.method){
			case this.OPERATION_GET_NODE :
				return this.getNodeToObject(bytes);
			case this.OPERATION_GIVE_NODE :
				return this.giveNodeToObject(bytes);
			case this.OPERATION_GET_INFO :
				return this.getInfoToObject(bytes);
			case this.OPERATION_GIVE_INFO :
				return this.giveInfoToObject(bytes);
			default :
				return {};
		}
	}
	
	// retrieve an operation type : 
	_retrieveOperationType(bytes){
		
	}
	
	// transform a request object into its byte equivalent
	objectToBytes(req){
		switch(req.method){
			case "getNode" :
				return this.getNodeToBytes(req);
			case "giveNode" :
				return this.giveNodeToBytes(req);
			case "getInfo" :
				return this.getInfoToBytes(req);
			case "giveInfo" :
				return this.giveInfoToBytes(req);
			default : return new Uint8Array(0);
		}
	}
	
	// retrieve all the canals from the meta spawners
	getCanals(){
		let canals = this.metaInterface.getCanals();
		return canals;
	}
	
	////////////////////////////////////////////////////////////////////
	//////////// key related methods ///////////////////////////////////
	////////////////////////////////////////////////////////////////////
	
	// calculate distance between two keys with xor metrics
	dist(k1,k2){
		let r = [];
		let len1 = k1.prefix.length;
		let span1 = k1.span || 0;
		let len2 = k2.prefix.length;
		let span2 = k2.span || 0;
		for(let i=0; i<Math.max(len1,len2); i+=2){
			r.push(
				(i<len1 || len1+2*span2<=i) && (i<len2 || len2+2*span2<=i) ?
				// case 1, the two bytes are defined,
				parseInt( k1.prefix.substr(i,2) || "0",16) ^ parseInt( k2.prefix.substr(i,2) || "0",16)
				// case 2, it is in the span zone of one of the key
				: 0
			);
		}
		return r.map(j=>(j<16 ? "0" : "")+j.toString(16)).join("");
	}
	
	// calculate the minimal distance between a key and an array of keys
	minDist(k, kArr){
		let fl = this;
		let dists = kArr.map(k1=>fl.dist(k, k1))
		let distsIndex = dists.map((v,i)=>i);
		distsIndex.sort((i1,i2) => fl.isInf( dists[i1], dists[i2]) ? -1 : 1 );
		return dists[ distsIndex[0] ];
	}
	
	// check if the prefix of the k1 key is included in the k2 key
	// precision can be used to limit the comparison to the first bits
	isIncluded(k1,k2, precision=k1.prefix.length*4) {
		let res = this.common(k1,k2, false) >= Math.min( k1.prefix.length*4 + (k1.span||0)*8, precision ) || !k1.prefix.length;
		return res;
	}
		
	// inverse the suffix of a key, starting at bit b
	// accept hexadecimal or Uint8array as key
	inverseSuffix(key, b){
		let isHexa = typeof key === "string";
		let res = isHexa ? this._flaptools.hexToBytes(key) : key.slice();
		let byte = b/8 >> 0;
		if(byte >= key.length) return res;
		res[byte] = res[byte] ^ (Math.pow(2,8-(b%8))-1);
		for(let i=byte+1; i<res.length; i++){
			res[i] = 255 - res[i];
		}
		return isHexa ? this._flaptools.bytesToHex(res) : res;
	}
	
	// calculate the common byte/bit length of two keys
	common(k1,k2, byteResult = true){
        let i=0;
        let p1 = this._flaptools.hexToBytes(k1.prefix);
        let p2 = this._flaptools.hexToBytes(k2.prefix);
        let s1 = k1.span||0, s2 = k2.span||0;

        let common = Formation.prototype.common.call(null, p1, p2);
        if(common === 8*Math.min(p1.length, p2.length) ){
            common = 8*Math.min( p1.length + s1, p2.length + s2);
        }
        if(byteResult) common = Math.floor(common/8)
        return common;

        // let i=0;
        // let p1 = k1.prefix.toLowerCase();
        // let p2 = k2.prefix.toLowerCase();
        // let minLen = Math.min(p1.length, p2.length);
        // let s1 = k1.span||0, s2 = k2.span||0;
		//
        // for(; i<minLen && p1[i] === p2[i]; i++){}
        // let common = (i-i%2)/2;
        // if(common === Math.min(p1.length/2, p2.length/2) ){
        //     common += Math.min( p1.length/2 - common + s1, p2.length/2 - common + s2);
        // }
        // return common;
	}


	// compare two Uint8Array, return true if the first is strictly inferior to the second
	isInf(k1,k2){
		for(let i=0; i<Math.min(k1.length, k2.length); i++){
			if(k1[i] < k2[i]) return true;
			if(k1[i] > k2[i]) return false;
		}
		return k1.length < k2.length;
	}
	
	// return a new key as an array
	// first element is the key prefix (represented as an Uint8Array)
	// second element is the key span (represented as an integer or +infinity)
	generateKey(l=32, keySpan=0){
		let r = new Uint8Array(l);
		for(let i=0; i<l;i++) r[i] = Math.random()*256 >> 0;
		return { prefix : this._flaptools.bytesToHex(r), span : Number(keySpan) };
	}
	
	generateId(){
    	return this._flaptools.randomHex(this.ID_LENGTH);
		return Math.floor( Math.random() * Math.pow(2,this.ID_LENGTH*8) );
	}

	// return the id associated to a key
	keyId(key){
		return key.prefix + "/" + (key.span || 0).toString();
	}
	
	////////////////////////////////////////////////////////////////////
	/////////////// api related methods ////////////////////////////////
	////////////////////////////////////////////////////////////////////
	
	// create a request id
	_generateRequestId(){
		while(1){
            // let id = Math.floor(Math.random() * Math.pow(2,this.REQUEST_ID_LENGTH*8));
            let id = this._flaptools.randomHex(this.REQUEST_ID_LENGTH);
			if(id in this.requests) continue;
			else return id;
		}
	}
	
	// transform a Uint8Array representing a getNode request into an object
	getNodeToObject(bytes){
		let res = this._flaptools.decode(bytes, this.TEMPLATE_GETNODE);
		res.method = "getNode";
		return res;
	} 
	
	getNodeToBytes(data){
		data.method = this.OPERATION_GET_NODE;
		let res = this._flaptools.encode(data, this.TEMPLATE_GETNODE);
		return res;
	}
	
	giveNodeToObject(bytes){
		let res = this._flaptools.decode(bytes, this.TEMPLATE_GIVENODE);
		res.method = "giveNode";
		return res;
	}

	giveNodeToBytes(data){
		data.method = this.OPERATION_GIVE_NODE;
		let res = this._flaptools.encode(data, this.TEMPLATE_GIVENODE);
		return res;
	}
	
	getInfoToObject(bytes){
		let res = this._flaptools.decode(bytes, this.TEMPLATE_GETINFO);
		res.method = "getInfo";
		let flags = res.flags || 0;
		res.getKeys = !!(flags & 128);
		res.getCanals = !!(flags & 64);
		res.getId = !!(flags & 32);
		return res;
	}
	
	getInfoToBytes(data){
		data.method = this.OPERATION_GET_INFO;
		data.flags = (data.getKeys ? 128 : 0) + (data.getCanals ? 64 : 0) + (data.getId ? 32 : 0);
		let res = this._flaptools.encode(data, this.TEMPLATE_GETINFO);
		return res;
	}

	giveInfoToObject(bytes){
		let res = this._flaptools.decode(bytes, this.TEMPLATE_GIVEINFO);
		res.method = "giveInfo";
		return res;
	}
	
	giveInfoToBytes(data){
		data.method = this.OPERATION_GIVE_INFO;
		let res = this._flaptools.encode(data, this.TEMPLATE_GIVEINFO);
		return res;
	}
	
	////////////////////////////////////////////////////////////////////
	////////////    HELPERS          ///////////////////////////////////
	////////////////////////////////////////////////////////////////////

	flockInit(){
		// constants
		this.VERSION_LENGTH = 1;
		this.RESULT_NUMBER_LENGTH = 1;
		this.REQUEST_ID_LENGTH = 4;
		this.INFO_AND_PRECISION_LENGTH = 2;
		this.PRECISION_AND_KEY_LENGTH = 3;
		this.COMMON_AND_ID_LENGTH = 3;
		this.COMMON_LENGTH_AND_LENGTH = 3;
		this.CANAL_TYPE_LENGTH = 1;
		this.CANAL_ADDRESS_LENGTH_LENGTH = 1;
		this.CANAL_RECEIVER_LENGTH_LENGTH = 2;
		
		this.OPERATION_GET_NODE = 0;
		this.OPERATION_GIVE_NODE = 1;
		this.OPERATION_GET_INFO = 2;
		this.OPERATION_GIVE_INFO = 3;
		
		this.CANAL_CURRENT_CONNECTION = 0;
		this.CANAL_HTTP = 1;
		this.CANAL_WEBSOCKET = 2;
		this.CANAL_WEBRTC = 3;
		this.CANAL_SWANS = 5;
		this.CANAL_END = 31;
		
		this.ID_LENGTH = 6;

		this.SQUAWKER_DEFAULT_DISTANCE = "000000000000ff";

		// build EE used for key dictionnaries
		let ee_k_prefix = { n : "prefix", t : "hex", c : 2, dd : ""};
		let ee_k_common = { n : "common", t : "int", c : 3};
		let ee_k_hiddenLength = { n : "hiddenLength", t : "int", c : 4, dd : 0};
		let ee_k_span = { n : "span", t : "int", c : 5, dd : 0};
		let ee_key = [ee_k_prefix, ee_k_common, ee_k_hiddenLength, ee_k_span];
		this.TEMPLATE_KEY = {
			ee : this._eeArrToObj(ee_key)
		};
		
		// build EE used in canal dictionnaries
        this.TEMPLATE_CANAL = this.metaInterface.TEMPLATE_CANAL;

		// build EE used in node dictionnaries
		let ee_n_id = {n:"id", t:"hex", c:2};
		let ee_n_keys = {n:"keys", t:"arr", tpl : this.TEMPLATE_KEY, c:3};
		let ee_n_canals = {n:"canals", t:"arr", tpl : this.TEMPLATE_CANAL, c:4};
		let ee_node = [ee_n_id, ee_n_keys, ee_n_canals];
		this.TEMPLATE_NODE = {
			ee : this._eeArrToObj(ee_node)
		};
		
		// build the EE used for flock protocols
		let ee_fl_version = { n : "version", t : "int", c : 0, d : 0};
		let ee_fl_length = { n : "length", t : "int", c : 1};
		let ee_fl_method = { n : "method", t : "int", c : 2, r : true};
		let ee_fl_rid = { n : "requestId", t : "hex", c : 3, r : true};
		let ee_fl_k = { n : "k", t : "int", c : 4, d : 1};
		let ee_fl_precision = { n : "precision", t : "int", c : 5, d : 1};
		let ee_fl_prefix = { n : "prefix", t : "hex", c : 6, d : new Uint8Array() };
		let ee_fl_nodes = { n : "nodes", t : "arr", tpl : this.TEMPLATE_NODE, c : 7, d : []};
		let ee_fl_flags = { n : "flags", t : "int", c : 8, d : 0};

		let ee_fl = [ee_fl_version, ee_fl_length,ee_fl_method ,ee_fl_rid ,ee_fl_k, ee_fl_precision, ee_fl_prefix, ee_fl_nodes, ee_fl_flags];
		this.TEMPLATE_FLOCK_REQUEST = {
			ee : this._eeArrToObj(ee_fl)
		};
		
		// create specific template for each request
		let ee_getnode = [ee_fl_version, ee_fl_method, ee_fl_rid, ee_fl_k, ee_fl_precision, ee_fl_prefix];
		this.TEMPLATE_GETNODE = {
			ee : this._eeArrToObj(ee_getnode)
		}
		
		let ee_givenode = [ee_fl_version, ee_fl_method, ee_fl_rid, ee_fl_nodes];
		this.TEMPLATE_GIVENODE = {
			ee : this._eeArrToObj(ee_givenode)
		}
		
		let ee_getinfo = [ee_fl_version, ee_fl_method, ee_fl_rid, ee_fl_precision, ee_fl_flags];
		this.TEMPLATE_GETINFO = {
			ee : this._eeArrToObj(ee_getinfo)
		}
		
		let ee_giveinfo = [ee_fl_version, ee_fl_method, ee_fl_rid, ee_fl_nodes];
		this.TEMPLATE_GIVEINFO = {
			ee : this._eeArrToObj(ee_giveinfo)
		}
		
		// create a template to extract only the method
		let ee_meta = [ee_fl_version, ee_fl_method];
		this.TEMPLATE_METADATA = {
			ee : this._eeArrToObj(ee_meta)
		}
	}
	
	// small aux method to map an array of EE to an object with ee names as keys
	_eeArrToObj(arr){
		let r={};
		for(let a of arr) r[a.n]=a;
		return r;
	}
	
	bytesToAscii(codePoints){
		let result = String.fromCharCode(...codePoints);
		return result;
	}
	
	asciiToBytes(ascii, arr, offset){
		if(!offset) offset = 0;
		if(!arr) arr = new Uint8Array(ascii.length);
		for(let i =0; i<ascii.length; i++){
			arr[offset+i] = ascii.charCodeAt(i);
		}
		return arr;
	}
	
	// transform an hex into bytes if not already done
	_hexToBytes(h){
		if(!h) return new Uint8Array(0);
		return typeof h === "string" ? this._flaptools.hexToBytes(h) : h;
	}
}

// formations is an object that links every bucket of a key to a number (assumed, a number of required node per bucket)
// it is used to determine the number of connections with foreign nodes to keep the flock network alive, or to insure reliable access to a given key
class Formation{
	constructor(key, data={}){
		this.key(key || new Uint8Array());
		this.afterKey( "afterKey" in data ? data.afterKey : 0 );
		this.beforeKey( "beforeKey" in data ? data.beforeKey : 3 );
		this.buckets( data.buckets || {} );
	}
	
	// getter and setter for the key over which the formation is built
	key(k){
		if(k) this._key = k;
		return this._key;	
	}
	
	// getter and setters for the afterKey property (number of default nodes in buckets after the last byte of the key)
	afterKey(n){
		if(n !== undefined) this._afterKey = n;
		return this._afterKey;
	}
	
	// getter and setters for the beforeKey property (number of default nodes in buckets before the last bit of the key)
	beforeKey(n){
		if(n !== undefined) this._beforeKey = n;
		return this._beforeKey;
	}
	
	// get the value for a specific bucket : ie the number of node requested for a given bucket of the key
	bucket(i){
		if(!(Number.isInteger(i) && i >= 0)) throw "bucket method require a positive integer : got "+i;
		if(i in this._buckets) return this._buckets[i];
		let byteNumber = Math.floor(i/8);
		return byteNumber < this._key.length ? this._beforeKey : this._afterKey;
	}

	// getter and setter for the bucket : object that contains the number of requested nodes per bucket (if not provided for a bucket, use default values beforeKey and afterKey)
	buckets(b){
		if(b){
			this._buckets = {};
			for(let i in b) this._buckets[i] = b[i];
		}
		let duplicate = {};
		for(let i in this._buckets) duplicate[i] = this._buckets[i];
		return duplicate;
	}
	
	// return a filtered list of buckets (in index and in range value)
	filterBuckets(minValue=0, maxValue=+Infinity, minIndex=0, maxIndex=this._key.length*8 ){
		let res = {};
		for(let i=minIndex; i<= maxIndex; i++){
			let v = this.bucket(i);
			if(v>=minValue && v <= maxValue) res[i] = v;
		}
		return res;
	};
	
	// retrieve the number of common bits between two keys
	common(k1, k2){
		let b = 0;
		let bits = [128, 64, 32, 16, 8, 4, 2, 1];
		let maxBit = Math.min(k1.length*8, k2.length*8);
		for(; b<maxBit; b++){
			let byte = b/8>>0;
			if( (k1[byte]^k2[byte]) & bits[b%8]) break;
		}
		return b;
	}

	// retrieve the bucket of the given key, represented by its prefix and its span
	keyBucket(prefix, span=0, key=this._key){
        let cb = this.common(key, prefix);
        // if the key is included into the formation key, the span of the key increase the common part
        if(cb === 8*prefix.length && span){
            cb = Math.min( 8*prefix.length + 8*span, 8*key.length);
        }
        return cb;
    }

	// map a list of nodes (nid=>keys) to the required buckets
	mapKeysToBuckets(nodes){
		let buckets = this.buckets();
		let result = {};
		for(let nid in nodes){
			let keys = nodes[nid];
			// push the node in the maximal buckets it belongs to
			let maximalNodeBucket = -1;
			for(let key of keys){
				let nodeBucket = this.keyBucket(key.prefix, key.span);
                if(!(nodeBucket in buckets)){
                    buckets[nodeBucket] = nodeBucket < this._key.length*8 ? this._beforeKey : this._afterKey;
                }
                if(buckets[nodeBucket]){
                	maximalNodeBucket = Math.max(maximalNodeBucket, nodeBucket);
                }
            }

            if(maximalNodeBucket === -1) continue;
			if(!result[maximalNodeBucket]) result[maximalNodeBucket] = [];
			if(result[maximalNodeBucket].length < buckets[maximalNodeBucket]) result[maximalNodeBucket].push(nid);
        }
		return result;
    }
	
	// return a formation object in which are removed a list of node keys : with a min of 0 per bucket
    // node keys are object with prefix(Uint8Array) and span(integer)
	complement(keys=[], reverseKey = false){
		if(!Array.isArray(keys)) throw "expected array as complement method first argument";
		let formationKey = this.key().slice();
		let cpm = new Formation(formationKey, {
			afterKey : this.afterKey(),
			beforeKey : this.beforeKey(),
		});
		let buckets = this.buckets();
		for(let key of keys){
			let cb = this.keyBucket(key.prefix , key.span);
			if(!(cb in buckets)){
				buckets[cb] = cb < formationKey.length*8 ? cpm.beforeKey() : cpm.afterKey();
			}
			if(buckets[cb]>0) buckets[cb]--;
		}

		cpm.buckets(buckets);
		return cpm;
	}
}

Flock.prototype._Formation = Formation;


export {
    Flock,
    Formation
}