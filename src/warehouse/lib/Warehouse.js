/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

/***
 * interface for warehouse
 * 
 * ***/
 
class Warehouse {

	constructor(data={}){
		// initialize default values and constants
		this._whInit();
	}

	///////////////////////////////////////////////////////////////////
	//////////////////   API METHODS //////////////////////////////////
	///////////////////////////////////////////////////////////////////

	// stop the warehouse
	stop(){}

	// check if the warehouse has a given resource, identified byt the key(prefix) and a potential suffix(feather)
	hasResource(prefix, opts={}){
		return Promise.resolve(false);
	}

	/***
	 * get available resources in a range
	 * ***/
	 availableResources(minPrefix, maxPrefix, opts={}){
		 return Promise.resolve([]);
	 }

	 /***
	  * method to be called to store a new resource
	  * opts can contain the following :
	  * 	priority : an integer between 0 and 9 that will enable to know which data to delete when support is full (lower priority, less important)
	  * 	dversion : the dversion of the document
	  * 	expire : time at which the data can be removed automatically, in milliseconds. value lower than 1000 have special significations :
	  * 		0 : never expire
	  * ***/
	 storeResource(prefix,resource, opts={}){
		 return Promise.resolve();
	 }

	 // retrieve a resource
	 getResource(prefix, opts={}){
		 return Promise.reject();
	 }

	 deleteResource(prefix, opts={}){
		 return Promise.resolve();
	 }

	 deleteResourceRange(lowPrefix, highPrefix, opts={}){
		 return Promise.resolve();
	 }

	/***
	 * every time a new resource is added to the warehouse, send a notification about it
	 * this method retrun an id of operation, to stop the watch by calling stopOperation(id)
	 * ***/
	 watch(data){
		 let watchId = data.id || ++this._id;
		 this._watch[watchId] = {
			 callback : data.callback,
		 }
		 return watchId;
	 }

	 unwatch(wid){
		if(this._watch[wid]) delete this._watch[wid];
	 }

	 /***
	  * process all the pending watch to notify about a new key
	  * ***/
	triggerNotify(keys){
         if(!Array.isArray(keys)) keys = [keys];
		 for(let wid in this._watch){
		 	this._watch[wid].callback(keys);
		}
	}

	////////////////////////////////////////////////////////////////////
	/////////////////   HELPERS  ///////////////////////////////////////
	////////////////////////////////////////////////////////////////////

	/** prepare an object before storing it
	 * ***/
	 prepareResource(prefix, resource, opts={}){
		 return {
             prefix : String(prefix).toLowerCase(),
			 data : resource,
			 dversion : opts.dversion || 0,
			 expire : opts.expire || 0,
			 priority : opts.hasOwnProperty("priority") ? opts.priority : 0,
		 }
	 }

	  /**
	  * check if a key is in a given range
	  * ***/
	 inRange(prefix, minRange, maxRange){
		 return prefix >= minRange && prefix <= maxRange;
	 }

	_whInit(){
		this.DB_WAREHOUSE = "warehouse";
		this.TABLE_DOCUMENT = "document";

		// create a list of watch request currently working
		 // data is indexed by watch id, and contains : minPrefix, maxPrefix, callback
		 this._watch = {};
		 this._id = 0;
	}
}

//


export { Warehouse }