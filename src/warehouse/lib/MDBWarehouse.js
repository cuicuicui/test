/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/


/***
 * Indexeddb base warehouse
 * ***/

import {Warehouse} from './Warehouse.js'

class MDBWarehouse extends Warehouse{
	
	constructor(data={}){
		super(data);
		this.dbUrl = data.url || "mongodb://localhost:27017";
		this.mongo = require('mongodb');
		this.mongoClient = this.mongo.MongoClient;
		this.database = data.database || this.DB_WAREHOUSE;
		this.collection = data.collection || this.TABLE_DOCUMENT;
		this._mdbInit();
	}
	
	///////////////////////////////////////////////////////////////////
	//////////////////   API METHODS //////////////////////////////////
	///////////////////////////////////////////////////////////////////

	// stop all the connections to databases
	stop(){
		if(this._client){
            this._client.then(client=>client.close())
		}
	}

	// check if the warehouse has a given resource, identified byt the prefix and a potential suffix(feather)
	hasResource(prefix, opts={}){
		return this.availableResources(prefix, prefix, opts).then(keys=>!!keys.length);
	}
	
	/***
	 * get available resources in a range
	 * ***/ 
	 availableResources(minPrefix, maxPrefix, opts={}){
		 let prom = this.dbGetKeys(this.database, this.collection, minPrefix, maxPrefix, opts);
		 // keep unique keys
		 //return prom.then(keys=> keys.filter((v,i) => keys.indexOf(v) == i) );
	 	 return prom;
	 }
	 
	 /***
	  * method to be called to store a new resource
	  * opts can contain the following :
	  * 	priority : an integer between 0 and 9 that will enable to know which data to delete when support is full (lower priority, less important)
	  * 	dversion : the dversion of the document
	  * 	expire : time at which the data can be removed automatically, in milliseconds. value lower than 1000 have special significations :
	  * 		0 : never expire
	  * ***/
	 storeResource(prefix,resource, opts={}){
		 let storeObject = this.prepareResource(prefix, resource, opts);
		 let prom = this.dbStore(this.database, this.collection, storeObject);
		 let wh = this;
		 prom.then((res)=>{
			 // after returning the result, proceed to notification of all those who are watching this warehouse new entries
			 let notifyData = {prefix : prefix};
			 if(opts.hasOwnProperty("dversion")) notifyData.dversion = opts.dversion;
			 wh.triggerNotify(notifyData);
		 });

		 return prom;
	 }
	 
	 // retrieve a resource
	 getResource(prefix, opts={}){
         prefix = prefix.toLowerCase();
		 return this.dbGet(this.database, this.collection, prefix, opts)
		 .then(v=> v ? new Uint8Array(v.data) : Promise.reject("could not retrieve "+prefix+" "+opts.dversion));
	 }
	 
	 deleteResource(prefix, opts={}){
         prefix = prefix.toLowerCase();
		 return this.dbRemove(this.database, this.collection, prefix, opts);
	 }
	 
	 deleteResourceRange(lowPrefix, highPrefix, opts={}){
         lowPrefix = lowPrefix.toLowerCase();
         highPrefix = highPrefix.toLowerCase();
		 return this.dbRemoveRange(this.database, this.collection, lowPrefix, highPrefix, opts);
	 }
	 
	 ///////////////////////////////////////////////////////////////////
	 //////////////  MONGODB   SPECIFIC METHODS  ///////////////////////
	 ///////////////////////////////////////////////////////////////////


	 // open a client
	 dbClient(url){
		 if(!this._client) this._client = this.mongoClient.connect(url);
		 return this._client;
	 }
	 
	 // open an idb, return a promise
	 dbOpen(dbName, url){
		 this._db[dbName] = this.dbClient(url)
		 .then(function(client){
			  return client.db(dbName);
		});
		return this._db[dbName];
	 }
	 
	 // retrieve a given collection
	 dbCollection(dbName, collection){
		 if(!this._collections[dbName]) this._collections[dbName] = {};
		 if(!this._collections[dbName][collection]){
			 let wh = this;
			 this._collections[dbName][collection] = this._db[dbName].then(db=>db.collection(collection));
		 }
		 return this._collections[dbName][collection];
	 }
	 
	 // store elements
	 dbStore(dbName, collectionName, value){
		  let colProm = this.dbCollection(dbName, collectionName);
		  // transform the Uint8Array data into a string
		  value.data = this.mongo.Binary( Buffer.from(value.data) );
		  return colProm.then(col=> col.update(  {prefix : value.prefix, dversion : value.dversion}, value, { upsert: true } ) );
	 }
	 
	 dbRemove(dbName, collectionName, prefix, opts={}){
		 let colProm = this.dbCollection(dbName, collectionName);
		 let filter = { prefix : prefix };
		 if(opts.dversion || opts.dversion === 0) filter.dversion = opts.dversion;
		 return colProm.then(col=> col.deleteMany(filter));
	 }
	 
	 // remove an element
	 dbRemoveRange(dbName, collectionName, prefixMin, prefixMax, opts={}){
		 let colProm = this.dbCollection(dbName, collectionName);
		 let filter = { $and: [ { prefix: { $gte: prefixMin } }, { prefix: { $lte: prefixMax } } ] };
		 if(opts.dversion || opts.dversion === 0) filter.dversion = opts.dversion;
		 return colProm.then(col=> col.deleteMany( filter ));
	 }
	 
	 // retrieve an element
	 dbGet(dbName, collectionName, prefix, opts={}){
		 let colProm = this.dbCollection(dbName, collectionName);
		 let filter = { prefix : prefix };
		 if(opts.dversion || opts.dversion === 0) filter.dversion = opts.dversion;
		 return colProm.then(col=> col.find(filter).sort({dversion:-1}).limit(1).next() )
		 .then(res=>{
			if(!res) return Promise.reject();
			res.data = res.data.buffer;
			return res; 
		 });
	 }
	 
	 // retrieve the available keys in a range
	 dbGetKeys(dbName, collectionName, prefixMin, prefixMax, opts={}){
         prefixMin = prefixMin.toLowerCase();
         prefixMax = prefixMax.toLowerCase();
		 let colProm = this.dbCollection(dbName, collectionName);
		 let filter = { $and: [ { prefix: { $gte: prefixMin } }, { prefix: { $lte: prefixMax } } ] };
		 if(opts.dversion || opts.dversion === 0) filter.dversion = opts.dversion;
		 return colProm.then(
			col=>{
				let p = col.find( filter, {prefix : true} )
				 return new Promise((res,rej)=>{
					 p.toArray(function(err, items){
						res(items ? items.map(i=>{return {prefix : i.prefix, dversion : i.dversion} }) : []);
					 });
				 });
			   }
		).catch(function(e){
			console.log("error", e)
		 });
	 }
	 
	 ///////////////////////////////////////////////////////////////////
	 //////////////////       HELPERS     //////////////////////////////
	 ///////////////////////////////////////////////////////////////////	
	
	 /*** initialize the warehouse
	  * ***/
	 _mdbInit(){
		 // create the lsit of databases 
		 this._db={};
		 
		 // store the available collections : 
		 this._collections = {};
		 
		 // create/open the db
		 this.dbOpen(this.database, this.dbUrl);
	 }
}

export {
    MDBWarehouse
}