/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

/***
 * Indexeddb base warehouse
 * ***/

import {Warehouse} from './Warehouse.js'

class IDBWarehouse extends Warehouse{
	
	constructor(data={}){
		super(data);
		this._idbInit();
	}
	
	///////////////////////////////////////////////////////////////////
	//////////////////   API METHODS //////////////////////////////////
	///////////////////////////////////////////////////////////////////
	
	// check if the warehouse has a given resource, identified byt the key(prefix) and a potential suffix(feather)
	hasResource(prefix, opts={}){
        prefix = prefix.toLowerCase();
		return this.availableResources(prefix, prefix, opts).then(keys=>!!keys.length);
	}
	
	/***
	 * get available resources in a range
	 * ***/
	 availableResources(minPrefix, maxPrefix, opts={}){
        minPrefix = minPrefix.toLowerCase();
        maxPrefix = maxPrefix.toLowerCase();
		 // if("version" in opts) version = parseInt(version);
		 let prom = this.dbGetKeys(this.DB_WAREHOUSE, this.TABLE_DOCUMENT, minPrefix, maxPrefix, opts);
		 // keep unique keys
		 return prom.then(keys=> keys.filter((v,i) => keys.indexOf(v) == i) );
	 }
	
	 /***
	  * method to be called to store a new resource
	  * opts can contain the following :
	  * 	priority : an integer between 0 and 9 that will enable to know which data to delete when support is full (lower priority, less important)
	  * 	dversion : the dversion of the document
	  * 	expire : time at which the data can be removed automatically, in milliseconds. value lower than 1000 have special significations :
	  * 		0 : never expire
	  * ***/
	 storeResource(prefix,resource, opts={}){
         prefix = prefix.toLowerCase();
		 let storeObject = this.prepareResource(prefix, resource, opts);
		 let prom = this.dbStore(this.DB_WAREHOUSE, this.TABLE_DOCUMENT, storeObject);
		 let wh = this;
		 prom.then((res)=>{
             // after returning the result, proceed to notification of all those who are watching this warehouse new entries
             let notifyData = {prefix : prefix};
             if(opts.hasOwnProperty("dversion")) notifyData.dversion = opts.dversion;
             wh.triggerNotify(notifyData);
		 });
		 prom.catch(function(e){
		 	console.log("ERROR CAUGHT ::::::: ", e)
		 })

		 return prom;
	 }
	 
	 // retrieve a resource
	 getResource(prefix, opts = {}){
         prefix = prefix.toLowerCase();
		 return this.dbGet(this.DB_WAREHOUSE, this.TABLE_DOCUMENT, prefix, opts)
		 .then(v=> v ? v.data : Promise.reject("could not retrieve "+prefix+" "+(opts.dversion||"") ));
	 }
	 
	 deleteResource(prefix, opts={}){
		 prefix = prefix.toLowerCase();
		 return this.dbRemove(this.DB_WAREHOUSE, this.TABLE_DOCUMENT, prefix, opts);
	 }
	 
	 deleteResourceRange(lowPrefix, highPrefix, opts={}){
         lowPrefix = lowPrefix.toLowerCase();
         highPrefix = highPrefix.toLowerCase();
		 return this.dbRemoveRange(this.DB_WAREHOUSE, this.TABLE_DOCUMENT, lowPrefix, highPrefix, opts);
	 }
	 
	 ///////////////////////////////////////////////////////////////////	 
	 //////////////  INDEXEDDB SPECIFIC METHODS  ///////////////////////
	 ///////////////////////////////////////////////////////////////////
	 
	 // open an idb, return a promise
	 dbOpen(name, version, schema){
		 let warehouse = this;
		 let rq = this.indexedDB.open(name, version);
		 let p = new Promise((res,rej)=>{
			 rq.onerror = (e)=>rej("IDB error : "+e.target.error);
			 rq.onsuccess = (e)=> res(e.target.result);
			 rq.onupgradeneeded = function(e){
				// create the stores
				let db = e.target.result;
				let stores = schema.stores;
				for(let storeName in stores||{}){
					let store = db.createObjectStore(storeName, stores[storeName].keyPath);
					// create store indexes
					for(let indexName in schema.index || {}){
						store.createIndex(indexName, indexName, schema.index[indexName]);
					}
				}
			 }
		 });
		 
		 // update the transactions object to accept transaction from this database
		 this._transactions[name] = {};
		 
		 // store the promise in the database list
		 this._db[name] = p;
		 return p;
	 }
	 
	 // create a transaction
	 dbTransaction(dbname, store){
		 // if a transaction has already been created for this store, return it
		 // if( this._transactions[dbname][store]) return this._transactions[dbname][store];
		 let warehouse = this;
		 let p = this._db[dbname].then((db)=>{
			 let t = db.transaction(store, "readwrite");
			 // store the transaction, but remove it once done
			 warehouse._transactions[dbname][store] = Promise.resolve(t);
			 t.onsuccess = (e)=> delete warehouse._transactions[dbname][store];
			 t.onerror = t.onsuccess;
			 t.oncomplete = t.onsuccess;
			 return t;
		 });
		 return p;
	 }
	 
	 // store elements
	 dbStore(dbname, store, value){
		 // create a transaction
		 let transProm = this.dbTransaction(dbname, store, "readwrite");
		 // create the resulting promise
		 let p = transProm.then((t)=>{
		 	 let storeObj = t.objectStore(store);
		 	 let storeProm = new Promise((res,rej)=>{
				 let storeReq = storeObj.put(value, [value.prefix, value.dversion]);
				 storeReq.onsuccess=()=>res();
				 storeReq.onerror=()=> (e)=>rej("IDB error : "+e.target.errorCode);
			 });
			 return storeProm;
		 });
		 return p;
	 }
	 
	 dbRemove(db, store, prefix, opts={}){
		 return this.dbRemoveRange(db, store, prefix, prefix, opts);
	 }
	 
	 // remove an element
	 dbRemoveRange(db, store, prefixMin, prefixMax, opts={}){
		 let transProm = this.dbTransaction(db, store, "readwrite");
		 let target = this.idbKeyRangeVersion(prefixMin, prefixMax, opts.dversion);
		 let p = transProm.then((t)=>{
			 let deleteProm = new Promise((res,rej)=>{
				 let rq = t.objectStore(store).delete(target);
				 rq.onsuccess = function(){ res()};
				 rq.onerror = (e)=>rej("IDB error : "+e.target.errorCode);
			 })
			 return deleteProm;
		 });
		 return p;
	 }
	 
	 // retrieve an element
	 dbGet(db, store, prefix, opts={}){
		 let transProm = this.dbTransaction(db, store, "readwrite");
		 let range = this.idbKeyRangeVersion(prefix, prefix, opts.dversion);
		 let p = transProm.then((t)=>{
			 let getProm = new Promise((res,rej)=>{
				 try{
                     let rq = t.objectStore(store).getAll(range);
                     rq.onsuccess = (e)=>res(e.target.result);
                     rq.onerror = (e)=>rej("IDB error : "+e.target.errorCode);
				 }
				 catch(e){
			 		console.log("IDB WAREHOUSE ERROR : ",e);
				 }
			 });
			 return getProm;
		 })
		 .then(arr=>{
			if(!arr) return Promise.reject();
			let res = null;
			for(let e of arr){
				if(!res || e.dversion>res.dversion) res=e;
			}
			return res; 
		 });
		 return p;
	 }
	 
	 // retrieve the available keys in a range
	 dbGetKeys(db, store, lower, higher, opts={}){
		 let transProm = this.dbTransaction(db, store, "readwrite");
		 let warehouse = this;
		 let p = transProm.then((t)=>{
			 let getKeysProm = new Promise((res,rej)=>{
				 var range = warehouse.idbKeyRangeVersion(lower, higher, opts.dversion)
				 let rq = t.objectStore(store).getAllKeys(range);
				 rq.onsuccess = (e)=>res(e.target.result);
				 rq.onerror = (e)=>rej("IDB error : "+e.target.errorCode);
			 });
			 return getKeysProm;
		 });
		 return p.then(keys=>keys.map(k=>{return { prefix : k[0], dversion : k[1]}}));
	 }
	 
	 // create a key range
	 idbKeyRange(low, high){
		 return this.IDBKeyRange.bound(low, high);
	 }
	 
	 // create a key range with version
	 idbKeyRangeVersion(low, high, dversion=null){
		 if(dversion === null || dversion === undefined) return this.IDBKeyRange.bound([low,0], [high,999999999999]);
		 else return this.IDBKeyRange.bound([low,dversion], [high,dversion]);
	 }
	 ///////////////////////////////////////////////////////////////////
	 //////////////////       HELPERS     //////////////////////////////
	 ///////////////////////////////////////////////////////////////////	
	
	 /*** initialize the warehouse
	  * ***/
	 _idbInit(){
		 // retrieve indexeddb objects
		 this.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
	 	 this.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction;
		 this.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange
		 
		 // create the lsit of databases 
		 this._db={};
		 
		 // store the pending transactions : remove them once done
		 // indexed by storeName
		 this._transactions = {};
		 
		 // IDB_WAREHOUSE shecma
		 this.DB_WAREHOUSE_VERSION = 1;
		 this.STORE_DOCUMENT_SCHEMA = {
			 stores : {
				 [this.TABLE_DOCUMENT] : {
					 index : {
						 expire : "expire",
						 prefix : "prefix",
                         dversion : "dversion",
						 
					 },
					 opts : { keyPath : ["prefix","dversion"] }
				 }
			 }
		 };
		 // create the DB
		 this.dbOpen(this.DB_WAREHOUSE, this.DB_WAREHOUSE_VERSION, this.STORE_DOCUMENT_SCHEMA);
	 }
}

export {
    IDBWarehouse
}