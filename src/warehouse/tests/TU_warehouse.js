/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

/**
 * generic tests for warehouses : 
 * tests are performed on a given WarehouseClass that extends Warehouse
 * 
 * **/

try{
	var chai = require("chai");

	// tested modules
    var flocklibs = require("../../../dist/flock-nodejs.js")
    Object.assign(global, flocklibs);

}
catch(e){}

var assert = chai.assert;
let testData = {};

describe("creating a Warehouse", function() {
    it("should just create a new Warehouse without error", function() {
		try{
			let wh = new testData.WarehouseClass();
			assert.ok(true);
		}
		catch(e){
			assert.ok(false,"it should not throw an exception : "+e);
		}
    });
});

describe("testing life cycle of data", function(){
	this.timeout(3000);
	let wh;
	
	let dataSize = 1000000;
	let data = new Uint8Array(dataSize);
	for(let i=0; i<dataSize; i++) data[i] = i%256;
	
	beforeEach(function(){
		wh = new testData.WarehouseClass();
	});
	
	it("should ensure that key is not found at start", function(){
		let key = "22009911";
		let goodKeyMin = "1111";
		let goodKeyMax = "3333";

		let prom = wh.hasResource(key).then(function(result){
			assert.notOk(result, "the key should not be stored at first when asking with hasResource");
			return wh.availableResources(goodKeyMin, goodKeyMax);
		})
		.then(function(availableResult){
			assert.isArray(availableResult);
			assert.lengthOf(availableResult, 0, "the key should not be stored at first when asking with availableResources");
		});
		return prom;
	});
	
	it("should reject the promise when using getResource(key) on a key that is not present", function(){
		let key = "224488eeaa009911";
		let prom = wh.getResource(key)
		.then(function(){ assert.ok(false, "the getResource should have failed");})
		.catch(function(){ assert.ok(true); });
		return prom;
	});


	it("should store a key, get it, test its presence, thene delete it", function(){
		let key = "2211";
		let goodKeyMin = "1111";
		let goodKeyMax = "2222";
		let wrongKeyMin = "8888";
		let wrongKeyMax = "9999";
		let prom = wh.storeResource(key,data)
		.then(function(){
			return wh.hasResource(key);
		})
		.then(function(result){
			assert.ok(result, "the key should be stored when asking with hasResource");
			return wh.availableResources(goodKeyMin, goodKeyMax);
		})
		.then(function(availableResult){
			assert.isArray(availableResult);
			assert.ok(availableResult.length, "the key should be stored when asking with availableResources");
			assert.deepEqual(availableResult[0], {prefix : key, dversion : 0}, "the key retrieved should be the key stored");
			return wh.getResource(key);
		})
		.then(function(result){
			assert.deepEqual(result, data, "the resource retrieved should be the resource stored");
			return wh.deleteResource(key);
		})
		.then(function(){
			return wh.hasResource(key);
		})
		.then(function(result){
			assert.notOk(result, "the key should not be stored at last when asking with hasResource");
			return wh.availableResources(goodKeyMin, goodKeyMax);
		})
		.then(function(availableResult){
			assert.isArray(availableResult);
			assert.lengthOf(availableResult, 0, "the key should not be stored at last when asking with availableResources");
		});
		return prom;
	});


	it("should store different dversion of the same key, then delete a specific dversion of the key", function(){
		let key = "bbcc";
		let goodKeyMin = "bbbb";
		let goodKeyMax = "cccc";
		let wrongKeyMin = "2222";
		let wrongKeyMax = "4444";
		let data1 = new Uint8Array([1,2,3]);
		let dversion1 = 35;
		let data2 = new Uint8Array([4,5,6]);
		let dversion2 = 55;
		let data3 = new Uint8Array([7,8,9]);
		let dversion3 = 99;
		
		let prom = wh.storeResource(key,data1, {dversion : dversion1})
		.then(function(result){
			return wh.storeResource(key,data3, {dversion : dversion3});
		})
		.then(function(){
			return wh.hasResource(key, {dversion : dversion2});
		})
		.then(function(result){
			assert.notOk(result, "the key should not be stored at version2 when asking with hasResource");
			return wh.storeResource(key,data2, {dversion : dversion2});
		})
		.then(function(result){
			return wh.hasResource(key, {dversion : dversion2});
		})
		.then(function(result){
			assert.ok(result, "the key should  be stored at version2 when asking with hasResource");
			return wh.getResource(key);
		})
		.then(function(result){
			assert.deepEqual(result, data3, "the resource retrieved should be the resource stored at the highest version");
			return wh.deleteResource(key, {dversion : dversion3});
		})
		.then(function(){
			return wh.getResource(key);
		})
		.then(function(result){
			assert.deepEqual(result, data2, "the resource retrieved should be the resource stored at the highest version");
			return wh.deleteResource(key);
		})
		.then(function(result){
			return wh.availableResources(goodKeyMin, goodKeyMax);
		})
		.then(function(availableResult){
			assert.isArray(availableResult);
			assert.lengthOf(availableResult, 0, "the key should not be stored at last when asking with availableResources");
		});
		return prom;
	});


	it("should watch for event, and send notification about them", function(done){
		let lastReceivedNotify;
		let watchCallback = function(keys){
			lastReceivedNotify = keys;
		}
		
		let watchId = wh.watch({
			callback : watchCallback
		});
		
		let key1 = "eeea";
		let key2 = "eeeb";
		wh.storeResource(key1,data, {dversion : 444});
		
		setTimeout(function(){
			assert.deepEqual(lastReceivedNotify, [{prefix : key1, dversion : 444}]);
			wh.deleteResource(key1);
			lastReceivedNotify = null;
			wh.storeResource(key2,data);
		}, 500);
		
		setTimeout(function(){
			assert.deepEqual(lastReceivedNotify, [{prefix : key2}]);
			wh.deleteResource(key2);
			done();
		}, 1000);
	});
})



try{
	module.exports = {
		testData : testData,
	}
}
catch(e){}
