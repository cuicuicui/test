/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 ***/


// helper that display a specific type of frp document in the browser

import {Frdisplayer} from "./Frdisplayer.js"

class FrdisplayerAudio extends Frdisplayer {

    constructor(data){
        // call parent constructor
        super(data);
    }

    // check if the displayer can display the given document
    canDisplay(frdoc){
        return frdoc.type === "audio";
    }

    // display a given frdoc as utf text
    display(frdoc={}, domelement){
        // aa7ea8736565a555079adf350175346f401e196348a857601a7116bc670a86c2
        var key = frdoc.key;
        let chunkStartIndex = 0;
        let chunkEndIndex = frdoc.winglength || +Infinity;
        return this._flprovider.getChunk(key, chunkStartIndex, chunkEndIndex).then(function(audioBytes){
            var subtype = frdoc.subtype || "mp3";
            var audioBlob = new Blob([audioBytes], {type : 'audio/'+subtype});
            var audioUrl = window.URL.createObjectURL(audioBlob);
            if(typeof domelement === "string") domelement = document.getElementById(domelement);
            domelement.innerHTML = "";
            var domAudio = document.createElement("audio");
            domAudio.src = audioUrl;
            domAudio.controls = true;
            domelement.appendChild(domAudio);
        });

    }

}

// export the class
export {
    FrdisplayerAudio
}