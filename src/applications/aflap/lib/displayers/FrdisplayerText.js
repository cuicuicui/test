/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 ***/

// helper that display a specific type of frp document in the browser

import {Frdisplayer} from "./Frdisplayer.js"

class FrdisplayerText extends Frdisplayer {

    constructor(data){
        // call parent constructor
        super(data);
        // keep the flock provider to access resources from the flock
        this._flprovider = data.flprovider;
        // can have several subdisplayers to use depending cases
        this._displayers = [];
    }

    // check if the displayer can display the given document
    canDisplay(frdoc){
        return (frdoc.type === "text") || (frdoc.type === "application" && frdoc.subtype === "html");
    }

    // display a given frdoc as utf text
    display(frdoc={}, domelement){
        var text = this._utf8BytesToString( frdoc.body || new Uint8Array());
        if(typeof domelement === "string") domelement = document.getElementById(domelement);
        domelement.innerHTML = text;
    }

    // create a new document from an input and save it in the frp
    cutAndStore(input="", opts={}){
        opts.type = "text";
        opts.subtype = "plain";
        let txtBytes = this._flaptools.stringToUtf8Bytes((input||"").toString());
        return this._frp.cutAndStore(txtBytes, opts);
    }


    _utf8BytesToString(bytes){
        let res = [];

        for(let i=0; i<bytes.length; i++){
            let b = bytes[i];
            if(b < 0x80) res.push(b);
            else if(b < 0xe0) res.push( ((b&0x1f)<<6) + (bytes[++i] & 0x3f) );
            else if( b <0xf0) res.push( ((b&0x0f)<<12) + ((bytes[++i] & 0x3f)<<6) + (bytes[++i] & 0x3f) );
            else{
                let codepoint = ((b&0x07)<<18) + ((bytes[++i] & 0x3f)<<12) + ((bytes[++i] & 0x3f)<<6) + (bytes[++i] & 0x3f);
                let diffcode = codepoint - 0x10000;
                res.push( 0xd800 + (diffcode>>10), 0xdc00 + (diffcode & 0x03ff) );
            }
        }
        return res.map(c=>String.fromCharCode(c)).join("");
    }

}


// export the class
export {
    FrdisplayerText
}