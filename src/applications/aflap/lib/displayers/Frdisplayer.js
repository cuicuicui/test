/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
***/

// helper that display a specific type of frp document in the browser
class Frdisplayer {

    constructor(data={}){
        this._flaptools = data.flaptools || null;
        this._frp = data.frp || null;
        this._frf = data.frf || null;
        this._flprovider = data.flprovider || null;
    }

    /////////////////////////////////////////////////////////////////////
    //////////////////////////// API METHODS ////////////////////////////
    /////////////////////////////////////////////////////////////////////
    // check if the displayer can display the given document
    canDisplay(frdoc){
        return false;
    }

    // display a given frdoc (flock resource document) in the specified dom element
    display(frdoc={}, domelement){}

    // create a new document from an input and save it in the frp
    cutAndStore(input=new Uint8Array(), opts={}){
        let defaultOpts = {
            chunkSize : 1000000,
            oneChunkLimit : 10000000,
            name : "noname",
            type : "application",
            subtype : "octet-stream",
        }
        for(let i in defaultOpts){
            if(!opts.hasOwnProperty(i)) opts[i] = defaultOpts[i];
        }

        return this._frp.cutAndStore(input, opts);
    }

    ////////////////////////////////////////////////////////////////////////////////
    /////////////////////////          HELPERS           ///////////////////////////
    ////////////////////////////////////////////////////////////////////////////////

    getDomElement(el){
        return typeof el === "string" ? document.getElementById(el) : el;
    }
}

// displayer that handles several Frdisplayer and choose one depending on given input document
class FrdisplayerMeta extends Frdisplayer {

    constructor(data={}){
        super(data)
        // can have several subdisplayers to use depending cases
        this._displayers = data.displayers ? data.displayers.slice() : [];
    }

    /////////////////////////////////////////////////////////////////////
    //////////////////////////// API METHODS ////////////////////////////
    /////////////////////////////////////////////////////////////////////
    // must be overriden if necessary

    // check if the displayer can display the given document
    canDisplay(frdoc){
        for(let displayer of this._displayers){
            if (displayer.canDisplay(frdoc)) return true;
        }
        return false;
    }

    // display a given frdoc (flock resource document) in the specified dom element
    display(frdoc={}, domelement){
        // retrieve the right displayer to use
        let displayer;
        for(let d of this._displayers){
            if(!d.canDisplay(frdoc)) continue
            displayer = d;
            break;
        }

        if(!displayer) throw "no matching displayer found for the frdoc";
        displayer.display(frdoc, domelement);
    }


}

// export the class
export {
    Frdisplayer,
    FrdisplayerMeta
}