/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 ***/

import {Frdisplayer} from "./Frdisplayer.js"

class FrdisplayerFile extends Frdisplayer {

    constructor(data){
        // call parent constructor
        super(data);
        this.displayType = data.displayType || "button"; // specify the type of element to be clicked on
        this.displayText = data.displayText || "Download &darr;"; // specify the text to be printed on the clickable element
        this.displayOptions = data.displayOptions || {};
    }

    // every frdoc can be downloaded
    canDisplay(frdoc){
        return true;
    }

    // create a download href
    createDownloadElement(docdata, container){
        container = this.getDomElement(container);
        let docKey = docdata.key;
        let mime;
        if(docdata.type){
            mime = docdata.type + (docdata.subtype ? "/"+docdata.subtype : "");
        }
        else mime = "application/octet-stream";
        let name = docdata.name || "unknown";

        return this._flprovider.getChunk(docKey).then(function(docBytes){
            let blob = new Blob([docBytes], {
                type: mime
            });
            let blobUrl = window.URL.createObjectURL(blob);
            let dom = document.createElement('a');
            dom.href = blobUrl;
            dom.download = name;
            container.appendChild(dom);
            dom.style = 'display: none';
            return dom;
        })
    }

    // download a given frdoc
    downloadFrdoc(frdoc={}){
        let domPromise = this.createDownloadElement(frdoc, document.body);
        domPromise.then(function(dom){
            dom.click();
            window.URL.revokeObjectURL(dom.href);
            dom.remove();
        })
    }

    // display a given frdoc as utf text
    display(frdoc={}, domelement){
        let $this = this;

        setTimeout(function(){
            let clickable = document.createElement($this.displayType);
            clickable[ $this.displayType === "button" ? "innerHTML" : "textContent"] = $this.displayText;
            clickable.style.cursor = "pointer";
            domelement = $this.getDomElement(domelement);
            clickable.addEventListener("click", function(){
                $this.downloadFrdoc(frdoc,domelement);
            });
            domelement.appendChild(clickable);
        },0)
    }
}

// export the class
export {
    FrdisplayerFile
}