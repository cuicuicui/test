/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 ***/

// helper that display a specific type of frp document in the browser

import {Frdisplayer} from "./Frdisplayer.js"

class FrdisplayerImage extends Frdisplayer {

    constructor(data){
        // call parent constructor
        super(data);
    }

    // check if the displayer can display the given document
    canDisplay(frdoc){
        return frdoc.type === "image";
    }

    // display a given frdoc as utf text
    display(frdoc={}, domelement){
        // 028bbbdef3a1d01e6b555cf2b0110b638c3b1990adb05b3df67e20c6f67992b2
        var key = frdoc.key;
        let chunkStartIndex = 0;
        let chunkEndIndex = frdoc.winglength || +Infinity;
        return this._flprovider.getChunk(key, chunkStartIndex, chunkEndIndex).then(function(imageBytes){
            var subtype = frdoc.subtype || "jpeg";
            var imageBlob = new Blob([imageBytes], {type : 'image/'+subtype});
            var imageUrl = window.URL.createObjectURL(imageBlob);
            if(typeof domelement === "string") domelement = document.getElementById(domelement);
            domelement.innerHTML = "";
            var domImg = document.createElement("img");
            domImg.src = imageUrl;
            domelement.appendChild(domImg);
        });

    }

}

// export the class
export {
    FrdisplayerImage
}