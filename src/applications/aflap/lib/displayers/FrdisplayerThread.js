/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 ***/

// helper that display a specific type of frp document in the browser

import {Frdisplayer} from "./Frdisplayer.js"

class FrdisplayerThread extends Frdisplayer {
    constructor(data){
        // call parent constructor
        super(data);

        // overwrite constants id provided
        if(data.maxDisplayedMessages) this.MAX_DISPLAYED_MESSAGES = data.maxDisplayedMessages;

        // array of currently displayed messages : objects contains domContainer, frdoc
        this.displayedMessages = [];

        this.displayers = {};
        this.maxMessages = data.maxMessages || this.MAX_DISPLAYED_MESSAGES;

        if(data.displayers){
            for(let i in data.displayers ){
                this.displayers[i] = data.displayers[i];
            }
        }

        this.messageDisplayer = new FrdisplayerThreadMessage(
            Object.assign({}, data,{
                frdisplayerText : this.displayers.frdisplayerText,
                frdisplayerFile : this.displayers.frdisplayerFile,
                frdisplayerVideo : this.displayers.frdisplayerVideo,
                frdisplayerImage : this.displayers.frdisplayerImage,
                frdisplayerAudio : this.displayers.frdisplayerAudio,
            })
        );
    }

    // alias to create a new thread
    newThread(input=new Uint8Array(), opts={}){
        return this.cutAndStore(input, opts);
    }

    // create a new thread
    cutAndStore(input=new Uint8Array(), opts={}){
        opts.type = this.THREAD_DOC_TYPE;
        opts.subtype = this.THREAD_DOC_SUBTYPE;
        let mainDocPromise = super.cutAndStore(input, opts);
        let $this = this;
        let allDocPromise = mainDocPromise.then(function(mainDoc){
            let mainDocData = $this._frf.extractDataFromSigned(mainDoc);
            // create a subdocument that will contain all the messages
            let opts2 = Object.assign({},opts);
            opts2.signtools = $this._frf.createKeys($this._frf._secswans.HASHTYPE_SHA_256).public;
            opts2.prefix = mainDocData.key.length/2;
            opts2.key = mainDocData.key + $this.UNSIGNED_MESSAGE_PREFIX;
            opts2.wingspan = $this.MESSAGE_WING_SPAN;
            let messagesWingPromise = Frdisplayer.prototype.cutAndStore.call($this, new Uint8Array(), opts2)
            return messagesWingPromise.then(function(){
                return mainDoc})
        })
        .catch(function(e){
            console.log("error during creation", e)
        })
        return allDocPromise;
    }

    // check if the displayer can display the given document
    canDisplay(frdoc){
        return frdoc.type == this.THREAD_DOC_TYPE && frdoc.subtype == this.THREAD_DOC_SUBTYPE;
    }

    // display a given frdoc as utf text
    display(frdoc={}, domelement){
        console.log("staaaaaaaart")
        let $this = this;
        var d = {
            get root(){ return $this.getDomElement(domelement)},
            get mainContainer(){return d.root.querySelector(".fdt-main-"+d.id)},
            get messageContainer(){return d.root.querySelector(".fdt-messages")},
            get postbarText(){return d.root.querySelector(".fdt-input")},
            get postbarFile(){return d.root.querySelector(".fdt-file")},
            get postbarSubmit(){return d.root.querySelector(".fdt-submit")},
            get postbarFilename(){return d.root.querySelector(".fdt-filename")},
            get postbarFilenameSelection(){return d.root.querySelector(".fdt-label-file")},
            get postbarFiledelete(){return d.root.querySelector(".fdt-filedelete")},
            get author(){return d.root.querySelector(".fdt-auth")},
        };
        d.id = Math.floor(Math.random() * 1000000000000); // a random unique id to use in html class/ids
        d.frdoc = frdoc;
        d.threadKey = frdoc.key;

        d.messageWingKey = d.threadKey + this.UNSIGNED_MESSAGE_PREFIX;
        d.messageWingDocumentPromise = this._flprovider.getDocument({prefix : d.messageWingKey});
        d.root.innerHTML =
            `<div class="fdt-main-${d.id}">
                <div class="fdt-messages"> </div>
                <div class="fdt-postbar">
                    <div class="fdt-content">
                        <textarea maxlength="2000" class="fdt-input"> </textarea>
                        <label for="fdt-file-${d.id}" class="fdt-label-file">join file</label>
                        <span class="fdt-filemeta">
                            <span class="fdt-filename"></span>
                            <span class="fdt-filedelete" title="remove" style="display:none">X</span>
                        </span>
                    </div>
                    <div class="fdt-admin">
                            <input type="text" maxlength="15" size="8" class="fdt-auth" placeholder="send as" />
                            <input type="submit" class="fdt-submit" value="send">
                    </div>
                </div>
            </div>
            `;
        d.messages = {};
        d.messagesDom = {};
        d.resetDomFile = function(){
            let domId = 'fdt-file-'+d.id;
            let previousDom = d.postbarFile;
            if(previousDom) previousDom.remove();
            // remove previous element if it exists
            let newDom = document.createElement("input");
            newDom.id = domId;
            newDom.type = "file";
            newDom.className = "fdt-file";
            d.postbarFilenameSelection.insertAdjacentElement("afterend", newDom);
        }
        d.resetDomFile();
        // initialize submitting of new messages
        var submitMessage = function(){
            let input;

            let file = d.postbarFile.files[0];
            if(file ){
                input = file;
                d.deleteFile();
            }
            else if(!!d.postbarText.value){
                input = d.postbarText.value;
                d.postbarText.value = "";
            }
            else{
                console.log("no message to submit");
                return;
            }

            readonlyFile(true);
            readonlyText(true);
            d.messageWingDocumentPromise.then(function(messageWingDocument){
                let opts = {
                    author : d.author.value || "anonymous",
                    time : Date.now(),
                    keys : { public : messageWingDocument.signtools, private : messageWingDocument.signtools },
                    keystart : d.messageWingKey
                }
                let msgProm = $this.messageDisplayer.createMessage(input, opts);
                msgProm.then(function(msg){
                    $this.addMessage(d,msg);
                    readonlyFile(false);
                    readonlyText(false);
                })
                .catch(function(e){
                    console.log(e);
                })
            })
        }

        let readonlyFile = function (bool=true){
            d.postbarFile.readOnly = !!bool;
            d.postbarFile.disabled = !!bool;
        };
        let readonlyText = function (bool=true){
            d.postbarText.readOnly = !!bool;
            d.postbarText.disabled = !!bool;
        };

        let onTextChange = function(){
            let hasvalue = !!d.postbarText.value;
            readonlyFile(hasvalue);
        }
        let onFileChange = function(){
            let hasvalue = !!d.postbarFile.value;
            readonlyText(hasvalue);
            let filename = "";
            if(hasvalue){
                filename = d.postbarFile.files[0].name;
            }
            d.postbarFilename.innerHTML = filename;
            d.postbarFilename.title = filename;
            // display or hide file addition
            if(hasvalue){
                d.postbarFiledelete.style.display = "";
                d.postbarFilenameSelection.style.display = "none";
            }
            else{
                d.postbarFiledelete.style.display = "none";
                d.postbarFilenameSelection.style.display = "";
            }
        }

        d.deleteFile = function(){
            d.postbarFile.value = "";
            d.resetDomFile();
            onFileChange();
        }

        // since dom elements might be reseted, set them again when needed
        let setEvents = function(){
            // if the thread disappear, remove the interval execution of the method
            if(!d.root){
                clearInterval(setEventsId);
                return;
            }
            // don't do anything if the events have already been set
            let postbarSubmit = d.postbarSubmit;
            if((postbarSubmit && postbarSubmit.onclick)) return;
            postbarSubmit.onclick = submitMessage;
            d.postbarText.addEventListener("keyup", function(e) {
                if (e.keyCode === 13) {
                    e.preventDefault();
                    postbarSubmit.click();
                }
                else{
                    onTextChange();
                }
            });

            // blur the file when text is set
            d.postbarFile.onchange = onFileChange;

            // delete the current file
            d.postbarFiledelete.onclick = d.deleteFile;
        }
        let setEventsId = setInterval(setEvents, 1000)
        setEvents();
        this.initThreadFrp(d);
        this.threadCron(d);
    }

    // cron task to be executed for a displayed thread
    threadCron(threadData){
        if(!threadData.hasOwnProperty("cronId") ){
            let $this = this;
            threadData.cronId = setInterval(function(){
                $this.threadCron(threadData);
            }, 5000)
        }
        // if the element is no longer displayed, stop all operations/cron
        if(!(threadData.root && threadData.mainContainer) ){
            if(threadData.hasOwnProperty("cronId")){
                clearInterval(threadData.cronId);
                delete threadData.cronId;
            }
            for(let feedKey in threadData.operations.feeds){
                threadData.operations.unfeed(feedKey);
                delete threadData.operations.feeds[feedKey];
            }
            return;
        }
    }

    // link a thread to a frp registration to receive documents
    // feed the given thread
    initThreadFrp(threadData){
        if(!threadData.operations) threadData.operations = {};
        threadData.operations.feeds = {}
        if(!threadData.keySpan) threadData.keySpan = 100;
        threadData.operations.feeds[threadData.key] = this.startFeed(threadData.messageWingKey, threadData.keySpan);

        let $this = this;
        let onNewDocument = function(data){
            let newKeys = data.keys;
            let expectedHexKeyLength = threadData.threadKey.length + $this.UNSIGNED_MESSAGE_PREFIX.length + 64;
            for(let key of newKeys){
                let prefix = key.prefix;
                // skip documents that are feathers of a main wing
                if(prefix.length !== expectedHexKeyLength) continue;
                $this._frp.getResource(prefix).then(function(doc){
                    $this.addMessage(threadData, doc);
                })
            }

        }
        let registerData = {
            tid : threadData.id,
            keys : [
                { prefix : threadData.messageWingKey, span : threadData.keySpan}
            ]
        }
        threadData.registerId = this._frp.addForeignRegistration(registerData, null, onNewDocument)
    }

    // start a feed operation with the frp
    startFeed(prefix, span){
        let op = {
            prefix : prefix,
            span : span,
            retro : true
        };
        this._flprovider.feed(op);
        return op;
    }


    // display a list of message into a thread
    // if a message is already beeing displayed, skip it
    // assume the order of messages doesn't change
    // remove messages that are no longer in the thread
    updateMessageDisplay(threadData){
        // sort the message to display
        let messages = this.sortMessages( Object.values( threadData.messages ) ).slice(-this.maxMessages);
        let messageKeys = messages.map(m=>m.key);

        // remove all the messages whose key are not in the list
        for(let messageKey in threadData.messages){
            if(messageKeys.indexOf(messageKey) !== -1) continue;
            if(threadData.messagesDom[messageKey]){
                threadData.messagesDom[messageKey].remove();
                delete threadData.messagesDom[messageKey];
            }
            delete threadData.messages[messageKey];
        }

        // check if the scroll of the message container is at the bottom
        let msgContainer = threadData.messageContainer;
        let isScrollBottom = msgContainer.scrollHeight - msgContainer.scrollTop === msgContainer.clientHeight;
        // append all messages that are not displayed yet
        let nextMessageContainer = null;
        // revert the messages to display oldest one first
        messages.reverse();
        for(let message of messages) {
            let key = message.key;
            if(!threadData.messagesDom[key]){
                threadData.messagesDom[key] = document.createElement("div");
                let messageDate = this._formatDate(new Date(message.time || 0) );
                let messageContainerClass = "fdt-key-"+key;
                threadData.messagesDom[key].innerHTML = `
                    <div class="fdt-msg-head">
                        <span class="fdt-author">${message.author||""}</span> at 
                        <span class="fdt-time">${ messageDate }</span>
                    </div>
                    <div class="fdt-msg ${messageContainerClass}">${key}</div>
                `;
                let messageContainer = threadData.messagesDom[key].querySelector("."+messageContainerClass);
                this.messageDisplayer.display(message, messageContainer);
                if(nextMessageContainer) threadData.messageContainer.insertBefore(threadData.messagesDom[key], nextMessageContainer);
                else threadData.messageContainer.insertAdjacentElement("beforeend", threadData.messagesDom[key]);
                nextMessageContainer = threadData.messagesDom[key];
            }
        }
        if(isScrollBottom){
            msgContainer.scrollTop = msgContainer.scrollHeight - msgContainer.clientHeight;
        }
    }


    // push a message in a given thread
    addMessage(threadData, frdocMessage){

        let messageData = this._frf.extractDataFromSigned(frdocMessage);
        if(!messageData.key || messageData.key.length <= threadData.messageWingKey.length ) return;
        if(threadData.messages[messageData.key]) return;
        threadData.messages[messageData.key] = messageData;
        this.updateMessageDisplay(threadData);
    }

    // sort a list of messages starting with older one
    // sort given time attribute
    sortMessages(messages=[]){
        let m = messages.slice();
        m.sort(this._sortMessagesAux);
        return m;
    }

    _sortMessagesAux(m1, m2){
        let t1 = m1.time || 0;
        let t2 = m2.time || 0;
        return t1 - t2 < 0 ? -1 : 1;
    }

    _formatDate(d){
        let hours = d.getHours().toString();
        if(hours.length == 1) hours = "0"+hours;
        let minutes = d.getMinutes().toString();
        if(minutes.length == 1) minutes = "0"+minutes;
        let res = d.getDate() + "/" + (d.getMonth()+1) + "/" + d.getUTCFullYear() + " "+hours+":"+minutes;
        return res;
    }
}

var pr = FrdisplayerThread.prototype;
pr.MAX_DISPLAYED_MESSAGES = 50;
pr.THREAD_DOC_TYPE = "application";
pr.THREAD_DOC_SUBTYPE = "x-frthread";
pr.MESSAGE_WING_SPAN = 32; // because keys are sha256

// a thread is made of several wings, each with its 2-byte prefix and purpose
pr.UNSIGNED_MESSAGE_PREFIX = "0000";

// class to display a signle thread message
class FrdisplayerThreadMessage extends Frdisplayer{
    constructor(data){
        // call parent constructor
        super(data);
        this._flaptools = data.flaptools;
        this._frdisplayerText = data.frdisplayerText;
        this._frdisplayerFile = data.frdisplayerFile;
        this._frdisplayerVideo = data.frdisplayerVideo;
        this._frdisplayerImage = data.frdisplayerImage;
        this._frdisplayerAudio = data.frdisplayerAudio;
    }

    display(frdoc={}, domelement){
        if( this._frdisplayerText && this._frdisplayerText.canDisplay(frdoc) ){
            return this._frdisplayerText.display(frdoc, domelement);
        }
        if( this._frdisplayerVideo && this._frdisplayerVideo.canDisplay(frdoc) ){
            return this._frdisplayerVideo.display(frdoc, domelement);
        }
        if( this._frdisplayerImage && this._frdisplayerImage.canDisplay(frdoc) ){
            return this._frdisplayerImage.display(frdoc, domelement);
        }
        if( this._frdisplayerAudio && this._frdisplayerAudio.canDisplay(frdoc) ){
            return this._frdisplayerAudio.display(frdoc, domelement);
        }
        if( this._frdisplayerFile && this._frdisplayerFile.canDisplay(frdoc) ){
            return this._frdisplayerFile.display(frdoc, domelement);
        }

        throw "cannot display message";
    }

    createMessage(input, opts={}){
        // for text
        if(typeof input === "string") return this.createTextMessage(input, opts);

        // for files
        if(input.constructor && input.constructor.name === "HTMLInputElement"){
            input = fileDom.files[0];
        }
        if(input.constructor && input.constructor.name === "File"){
            return this.createFileMessage(input, opts);
        }
        throw "unknpwn type of new message in createMessage"
    }

    // create a message from a text
    createTextMessage(input="", opts = {}){
        let inputBytes = this._flaptools.stringToUtf8Bytes( (input|| "").toString() );
        return this._frdisplayerText.cutAndStore(input, opts);
    }

    // create a message from an input file
    createFileMessage(input, opts = {}){
        return this._frdisplayerFile.cutAndStore(input, opts);
    }
}


// export the class
export {
    FrdisplayerThread
}