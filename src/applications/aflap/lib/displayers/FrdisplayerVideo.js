/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 ***/

// helper that display a specific type of frp document in the browser

import {Frdisplayer} from "./Frdisplayer.js"

class FrdisplayerVideo extends Frdisplayer { // 103355aaf5d6a080746a20890a4f4382ffec6c336832732af66b8f1c294bc5fa

    constructor(data){
        // call parent constructor
        super(data);
    }

    // check if the displayer can display the given document
    canDisplay(frdoc){
        return frdoc.type === "video" && frdoc.subtype === "webm";
    }

    // display a given frdoc as utf text
    display(frdoc={}, domelement){
        let displayer = this;

        // cannot display anything else than a webm video for the moment
        if(!this.canDisplay(frdoc)) throw "cannot display the given flock resource document";

        if(typeof domelement === "string") domelement = document.getElementById(domelement);

        domelement.innerHTML = "";
        // initialize the video data
        let videoPromise = this.initVideo(frdoc).then(function(vdata){
            domelement.appendChild(vdata.video);
            return vdata.startBuffering()
        });
        return videoPromise
    }

    // create a new videoData that will contain all the element about the video
    initVideo(frdoc){
        let videoData = new WebmVideoData({
            displayer : this,
            flprovider : this._flprovider,
            frdoc : frdoc
        });

        return videoData.initializeVideo();
    }

    /** estimate the position in a webm file of a given timestamp
     */

    /* initialize a video data object by loading metadata
     **/
    initHeadData(frdoc){
    }
}

/***
 class to handle a single video
 ****/
class WebmVideoData{

    constructor(data={}){
        this.frdoc = data.frdoc;
        this.displayer = data.displayer;
        this.flprovider = data.flprovider;

        this.key = this.frdoc ? this.frdoc.key : "";
        let chunkStartIndex = 0;
        let chunkEndIndex = this.DEFAULT_CHUNK_SIZE;

        // when clusters are parsed, keep in memory their data (position, timestamp)
        this.clusters = {};

        // list of Uint8Array to be pushed in the mediasource buffer, in order
        this.bufferstack = [];

        // memorize the last clicked videoPosition
        this.targetedStart = 0;

        // boolean to check if the next operation on sourceBuffer must be to clear the time ranges
        this.mustClearTimeRanges = false;

        // currently loading max buffer index
        this.loadingIndex = 0;

        // currently max loaded position (or ready to be pushed in bufferstack array
        this.loadedIndex = 0;

        // list of loadBuffer pending promises, indexed by starting index
        this.bufferPromises = [];

        // maximum number of simultaneous promises for loading buffer
        this.maxBufferPromises = 1;

        // length of preloaded video
        this.preloadingTarget = this.DEFAULT_PRELOADING_LENGTH;

        // build the javascript video element
        this.video = document.createElement("video");
        this.video.setAttribute("controls", "controls");
        this.video.ontimeupdate = this.onTimeUpdate.bind(this);


        this.sourceOpen = this._sourceOpen.bind(this);
    }

    // callback for mediaSource sourceOpen event
    _sourceOpen(){
        this.mediaSource.removeEventListener('sourceopen', this.sourceOpen);
        var mediaSource = this;
        this.sourceBuffer = this.mediaSource.addSourceBuffer(this.mediasourceCodec);
        this.sourceBuffer.addEventListener('updateend', this.updateBuffer.bind(this) );
        if(this.sourceOpenResolve) {
            this.sourceOpenResolve();
            return;
        }
    }

    // initialize the video
    // load the metadata, the video size, build a javascript video element
    initializeVideo() {
        let chunkStartIndex = 0;
        let chunkEndIndex = chunkStartIndex + this.DEFAULT_CHUNK_SIZE;
        let headBytesPromise = this.flprovider.getChunk(this.frdoc.key, chunkStartIndex, chunkEndIndex);
        let $this = this;

        return headBytesPromise.then(function (bytes) {

            // retrieve the total length of the webm video
            let segmentElement = $this.retrieveWebmNestedElement(bytes, [$this.WEBM_SEGMENT_ID]);
            $this.totalLength = segmentElement.idStart + segmentElement.totalLength;

            // retrieve positions of first level EBML elements
            $this.headPositions = $this.retrieveHeadPositions(bytes);

            // retrieve the codec to use for a webm element
            $this.codecs = $this.retrieveWebmCodecFromBytes(bytes)
            $this.mediasourceCodec = $this.mediasourceCodec($this.frdoc.type, $this.codecs.video, $this.codecs.audio );

            // store data about the initial cluster
            let firstCluster = $this.retrieveClusterData(bytes);
            $this.firstClusterPosition = firstCluster.idStart;
            $this.clusters[firstCluster.idStart] = firstCluster;

            // retrieve the scale to get millieconds timestamps when reading webm blocks
            let timestampScale = $this.retrieveWebmNestedElement(bytes, [$this.WEBM_SEGMENT_ID, $this.WEBM_INFO_ID, $this.WEBM_TIMESTAMP_SCALE_ID]);
            $this.timestampScale = $this.readWebmInteger(bytes, timestampScale);
            $this.timestampCorrector = 1000000 / $this.timestampScale;

            // total time duration of the webm video
            let duration = $this.retrieveWebmNestedElement(bytes, [$this.WEBM_SEGMENT_ID, $this.WEBM_INFO_ID, $this.WEBM_DURATION_ID]);
            $this.duration = $this.timestampCorrector * $this.readWebmFloat(bytes, duration);

            // push a virtual cluster representing the end of the file
            $this._setFakeEndCluster();

            // create mediaSource element
            $this.mediaSource = new MediaSource;
            $this.src = URL.createObjectURL($this.mediaSource);
            $this.video.src = $this.src;
            let sourceOpenPromise = new Promise(res=>$this.sourceOpenResolve=res).then(()=>$this);
            $this.mediaSource.addEventListener('sourceopen', $this.sourceOpen);
            return sourceOpenPromise;
        })
            .then(function(){
                return $this.retrieveHead();
            })
            .then(function(headBytes){
                $this.sourceBuffer.appendBuffer(headBytes);
                return Promise.resolve($this);
            })
    }

    // create a fake end cluster, for data parsing purpose, with timestamp and and position at the end of the last real cluster
    _setFakeEndCluster(position=this.totalLength, timestamp=this.duration){
        let prevPos = this.lastClusterPosition;
        if(prevPos && this.clusters[prevPos]) delete this.clusters[prevPos];
        this.lastClusterPosition = position;
        this.clusters[position] = {
            idStart: position,
            idLength: 0,
            lengthStart : position,
            lengthLength : 0,
            dataStart: position,
            dataLength: 0,
            timestamp: timestamp
        };
        return this.clusters[position];
    }

    // start displaying a video, loading the metadata into mediaSource first
    startBuffering(){
        let $this = this;
        return $this.jumpto($this.targetedStart).then(()=>$this);
    }

    // append the next chunk of data to be pushed in the bufferSource, and/or trigger if possible the append operation
    bufferPush(chunk){
        if(chunk){
            this.bufferstack.push(chunk);
            this.loadedIndex += chunk.length;
        }
        if(this.sourceBuffer && !this.sourceBuffer.updating && this.bufferstack.length){
            let canPush = this.splitBufferstack();

            if(canPush){
                this.sourceBuffer.appendBuffer(this.bufferstack.shift());
            }
        }
    }

    // concatenate and split the chunks in the bufferstack between two blocks
    splitBufferstack(){
        let t1 = Date.now();
        document.bst= this.bufferstack
        if(this.bufferstack.length === 0) return false;
        if(this.loadedIndex >= this.totalLength) {
            this.bufferstack = [ this.concatTypedArray(this.bufferstack) ];
            return true;
        }
        let indexOffset = 0;
        let splitpos, lastChunk;
        while(!splitpos){
            lastChunk = this.bufferstack[this.bufferstack.length - 1];
            splitpos = this.retrieveLastBlock(lastChunk, lastChunk.length - indexOffset);
            if(splitpos) break;
            if(this.bufferstack.length <= 1) return false;
            indexOffset = lastChunk.length;
            let concat = this.concatTypedArray( this.bufferstack.slice(-2));
            this.bufferstack = this.bufferstack.slice(0, -2);
            this.bufferstack.push(concat);
        }
        if(!splitpos) return;
        let chunk1 = lastChunk.slice(0,splitpos);
        let chunk2 = lastChunk.slice(splitpos);
        this.bufferstack.pop();
        this.bufferstack.push(chunk1);
        let concatenatedChunks = this.concatTypedArray(this.bufferstack)
        let clusterStart = this.webmSearch(concatenatedChunks, this.WEBM_CLUSTER_ID); //this.retrieveFirstClusterElement(concatenatedChunks, 0, 1);
        if(clusterStart && clusterStart.idStart){
            this.bufferstack = [ concatenatedChunks.slice(0,clusterStart.idStart), concatenatedChunks.slice(clusterStart.idStart) ];
        }
        else{
            this.bufferstack = [ concatenatedChunks ];
        }

        this.bufferstack.push(chunk2);
        let t2 = Date.now();
        return true;
    }

    // before pushing an array of data
    // concatenate a list of Uint8Arrays
    concatTypedArray(arrs){
        let len = 0;
        for(let arr of arrs) len += arr.length;
        let res = new Uint8Array(len);
        let index = 0;
        for(let arr of arrs){
            res.set(arr, index);
            index += arr.length;
        }
        return res;
    }

    // trigger an event when a buffer promise resolve
    prepareBufferPromiseHarvest(prom) {
        let $this = this;
        prom.then(function(dataChunk){
            if($this.bufferPromises[0] !== prom) return;
            $this.bufferPush(dataChunk);
            $this.bufferPromises.shift();
            let nextProm = $this.bufferPromises[0]
            if(nextProm) $this.prepareBufferPromiseHarvest(nextProm);
        })
    }

    updateBuffer(){
        // clear the source buffer if it was asked
        this.clearNextTimeRange();

        // try to push the next buffer
        this.bufferPush();
    }

    // method to clear the bufferSource
    clearTimeRanges(){
        this.mustClearTimeRanges = true;
        this.clearNextTimeRange();
    }

    // clear the next time range : return true if an action has been taken
    clearNextTimeRange(){
        if(this.sourceBuffer.updating || !this.mustClearTimeRanges) return;
        let ranges = this.loadedTime();
        if(ranges.length){
            this.clearTimeRange(0, this.duration);
            return true;
        }
        else{
            this.mustClearTimeRanges = false;
            return false;
        }
    }

    clearTimeRange(mstime0, mstime1){
        this.sourceBuffer.remove(0, 100000 );
    }

    // check if the given time has been loaded
    isTimeLoaded(mstime){
        let loadedRanges = this.loadedTime();
        for(let range of loadedRanges){
            if(mstime>= range[0] && mstime<= range[1]) return true;
        }
        return false;
    }

    // currently appended buffer timestamp
    loadedTime(){
        let ranges = [];
        for(let i=0; i<this.sourceBuffer.buffered.length; i++){
            ranges.push([
                1000*this.sourceBuffer.buffered.start(i),
                1000*this.sourceBuffer.buffered.end(i)
            ]);
        }
        return ranges;
    }

    minLoadedTime(){
        let loadedRanges = this.loadedTime();
        return loadedRanges.length ? loadedRanges[0][0] : 0;
    }

    maxLoadedTime(){
        let loadedRanges = this.loadedTime();
        return loadedRanges.length ? loadedRanges[loadedRanges.length-1][1] : 0;
    }

    // current time of the video
    currentTime(){
        return this.video.currentTime * 1000;
    }

    // check if a new buffer of data can be loaded
    canLoadBuffer(){
        return this.bufferPromises.length < this.maxBufferPromises;
    }

    // method to load a next buffer of data
    loadNextBuffer(startIndex=null, chunkLength=null){
        if(!this.canLoadBuffer()) return;
        if(this.loadingIndex >= this.totalLength) return;
        if(startIndex === null) startIndex = this.loadingIndex;
        let endIndex = startIndex + (chunkLength || 2*this.DEFAULT_CHUNK_SIZE);
        let loadingPromise = this.flprovider.getChunk(this.key, startIndex, endIndex, {timeout : 60000});
        this.bufferPromises.push(loadingPromise);
        if(this.bufferPromises.length === 1) {
            this.prepareBufferPromiseHarvest(loadingPromise);
        }
        this.loadingIndex = endIndex;
        return loadingPromise;
    }

    // method to load next buffers of data
    loadNextBuffers(startIndex=null, chunkLength=null){
        if(!this.canLoadBuffer()) return;
        let i=0;
        while( this.canLoadBuffer() && i<this.maxBufferPromises ){
            this.loadNextBuffer();
            i++;
        }
        // this.loadNextBuffer(startIndex, this.endIndex);
        // for(let i=0; i<this.maxBufferPromises && this.canLoadBuffer(); i++){
        //     this.loadNextBuffer();
        // }

    }

    // buffer the head of the video
    retrieveHead(){
        return this.flprovider.getChunk(this.key, 0, this.firstClusterPosition);
        // return this.loadNextBuffer(0, this.firstClusterPosition);
    }

    // retrieve the cues bytes of the webm file
    retrieveCuesBytes(){
        let cuesPosition = null;
        for(let pos in this.headPositions){
            if(this.headPositions[pos] === this.WEBM_CUES_ID){
                cuesPosition = pos;
                break;
            }
        }
        var $this = this;
        if(!cuesPosition) return Promise.resolve(new Uint8Array());
        return this.flprovider.getChunk(this.key, cuesPosition, cuesPosition+this.DEFAULT_CHUNK_SIZE).then(function(chunk){
            let cues = $this.retrieveWebmEBMLElement(chunk);
            return chunk.slice(0, cues.totalLength);
        })
    }

    // append the event that will push new buffer once the promise resolve
    linkEventToLoading(){
        if(!this.bufferPromises.length) return;
        let loadProm = this.bufferPromises[0];
        // don't do anything if the promise has been tagged
        if(loadProm.isWaiting) return;
        else loadProm.isWaiting = true;
        loadProm.then()
    }

    // event to be triggered when the user change video time manually
    onseeked(){
        let newMstime = this.currentTime();
        if(this.isTimeLoaded(newMstime)) return;
        this.jumpto(newMstime);
    }

    // event to perform once the video time is updated
    onTimeUpdate(){
        this.playTime = this.currentTime();
        if(this.video.seeking){
            if(this.playTime == this.currentJump) return;
            if(this.isTimeLoaded(this.playTime)) return;
            this.jumpto(this.playTime);
            this.currentJump = this.playTime;
        }
        else{
            let maxLoadedTime = this.maxLoadedTime();
            if(maxLoadedTime - this.playTime < this.preloadingTarget) this.loadNextBuffers();
        }
    }

    // jump to a given localization
    jumpto(mstime){
        // create an id to interrupt the jump if another jump occur in the meantime
        if(!this.currentJumpId) this.currentJumpId = 1;
        let jumpId = ++this.currentJumpId;
        mstime = Math.max(0, mstime - 4000);

        // clear the current buffers
        this.bufferstack.length = 0;
        this.bufferPromises.length = 0;
        this.targetedStart = mstime;
        this.clearTimeRanges();
        let $this = this;
        //this.video.pause();
        this.loadedIndex = 0;
        let startIndex, endIndex, cluster;
        return $this.retrieveTimeCluster(mstime)
            .then(function(cl){
                if($this.currentJumpId !== jumpId) throw "another jump took place in the meantime";
                cluster = cl;
                // retrieve the beginning of the cluster
                startIndex = cluster.idStart;
                endIndex = startIndex + (mstime == 0 ? 1 : 2    )*$this.DEFAULT_CHUNK_SIZE;
                return $this.flprovider.getChunk($this.key, startIndex, endIndex);
            })
            .then(function(clusterStartChunk){
                if($this.currentJumpId !== jumpId) throw "another jump took place in the meantime";

                let firstBlockPosition = $this.retrieveFirstBlock(clusterStartChunk, cluster.dataStart - cluster.idStart);
                let clusterHead = clusterStartChunk.slice(0, firstBlockPosition);

                $this.bufferPush(clusterHead);
                $this.loadingIndex = cluster.idStart + firstBlockPosition;
                $this.loadedIndex = $this.loadingIndex;
                // retrieve the target time in the cluster
                let relativeTime = mstime - cluster.timestamp;
                return $this.retrieveRelativeTimeBlockPosition(cluster, relativeTime)
            })
            .then(function(blockPosition){
                if($this.currentJumpId !== jumpId) throw "another jump took place in the meantime";
                $this.loadingIndex = Math.max( blockPosition, $this.loadingIndex);
                $this.loadedIndex = $this.loadingIndex;
                // restart buffering
                $this.loadNextBuffers();
            })
            .catch(function(e){
                console.log("jump error", e)
            })
    }

    //////////////////////////////////////////////////////////////////////////////////////
    ////////////////////  WEBM HELPERS ///////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////

    /*** retrieve cluster data starting from a random byte
     */
    retrieveClusterData(bytes, timestampCorrector=1, indexStart=0, byteOffset=0) {
        let elementPath = byteOffset ? [this.WEBM_CLUSTER_ID] : [this.WEBM_SEGMENT_ID, this.WEBM_CLUSTER_ID];
        let cluster = this.retrieveWebmNestedElement(bytes, elementPath, indexStart);
        let timestampElement = this.retrieveWebmNestedElement(bytes, [this.WEBM_TIMESTAMP_ID], cluster.dataStart);
        cluster.timestamp = this.readWebmInteger(bytes, timestampElement);
        if(byteOffset){
            cluster.idStart += byteOffset;
            cluster.dataStart += byteOffset;
            cluster.lengthStart += byteOffset;
        }
        return cluster;
    }


    /*** read an integer of a given webm element value
     */
    readWebmInteger(bytes, element){
        let res = 0;
        if(!element) throw "no element to read an integer from";
        for(let i=0; i<element.dataLength; i++){
            res = 256 * res + bytes[i + element.dataStart];
        }
        return res;
    }

    /*** read an float of a given webm element value
     */
    readWebmFloat(bytes, element){
        let floatSlice = bytes.slice(element.dataStart, element.dataStart + element.dataLength).reverse();
        let buff = floatSlice.buffer;
        var floats = floatSlice.length === 4 ? new Float32Array(buff) : new Float64Array(buff);
        return floats[0];
    }

    // create a valid codec for mediasource
    mediasourceCodec(videoType, videoCodec, audioCodec){
        let result = 'video/webm; codecs="' + this.WEBM_CODECS[videoCodec] + ', ' + this.WEBM_CODECS[audioCodec] + '"';
        return result;
    }

    // // retrieve the list of time range in a given sourceBuffer
    // sourceBufferRanges(sourceBuffer){
    //     let res = [];
    //     for(let i=0; i<sourceBuffer.buffered.length; i++) res.push([sourceBuffer.buffered.start(i), sourceBuffer.buffered.end(i)]);
    //     res.sort((a,b)=> a[0] < b[0] ? -1 : 1);
    //     return res;
    // }

    // // retrieve the difference between two buffer ranges, assuming this difference can be expressed as a single interval
    // // suppose this difference is the result of a newly added range
    // rangeDiff( ranges1, ranges2 ){
    //     if(ranges1.length < ranges2.length) return this.rangeDiff(ranges2, ranges1);
    //     for(let i=0; i<ranges2.length; i++){
    //         let r1 = ranges1[i];
    //         let r2 = ranges2[i];
    //         if( r1[0] != r2[0] ) return [ Math.min(r1[0], r2[0]), Math.max(r1[0], r2[0])];
    //         if( r1[1] != r2[1] ) return [ Math.min(r1[1], r2[1]), Math.max(r1[1], r2[1])];
    //     }
    //     // by default, return the last range of r1, since all the other range match with r2
    //     if(ranges1.length > ranges2.length) return ranges1[ranges1.length-1];
    //
    //     // no difference has been found
    //     return null;
    // }

    // find the first matching element (per id, in lower case) in a range of bytes
    webmSearch(bytes, searchedId, startIndex=0, endIndex=bytes.length){
        let cnt = 0;
        while(startIndex < bytes.length){
            let el = this.retrieveWebmEBMLElement( bytes, startIndex);
            if(el.id === searchedId) return el;

            startIndex += el.totalLength;
            //cnt++;
            //if(cnt > 100000)
        }
        return null;
    }

    // transform a Uint8Array into an hexadecimal string
    bytesToHex(b, start=0, end=b.length){
        return Array.from(b.slice(start, end)).map(i=>(i<16 ? "0" : "")+i.toString(16)).join("");
    }

    // retrieve data about a webm packet
    retrieveWebmEBMLElement(webm, startIndex=0, result={}, retrieveId = true, indexOffset=0){
        // extract the id info
        result.idStart = startIndex;
        result.idLength = this.retrieveWebmElementLength(webm, startIndex);
        if(result.idLength > this.WEBM_MAXIMUM_ID_LENGTH) return null;

        if(retrieveId){
            result.id = "";
            for(let i=0; i< result.idLength; i++){
                let idPart = webm[result.idStart+i].toString(16);
                result.id += (idPart.length === 1 ? "0" : "") + idPart;
            }
        }
        startIndex += result.idLength;

        // extract the data length
        result.lengthStart = startIndex;
        result.lengthLength = this.retrieveWebmElementLength(webm, startIndex);
        if(result.lengthLength > this.WEBM_MAXIMUM_LENGTH_LENGTH) return null;
        startIndex += result.lengthLength;

        // set information about the ebml data
        result.dataStart = startIndex;
        result.dataLength = (0xff >> result.lengthLength) & webm[result.lengthStart];
        for(let i = 1; i<result.lengthLength; i++){
            result.dataLength = 256 * result.dataLength + webm[result.lengthStart + i];
        }

        result.totalLength = result.idLength + result.lengthLength + result.dataLength;

        // add an offset to the extracted index
        if(indexOffset){
            result.idStart += indexOffset;
            result.dataStart += indexOffset;
            result.lengthStart += indexOffset;
        }

        return result;
    }

    // retrieve the length of a webm element at the current position
    retrieveWebmElementLength(webm, index){
        return 8 - Math.floor( Math.log2(webm[index]) );
    }

    // retrieve the list of head elements and their position
    retrieveHeadPositions(webm){
        // retrieve the seekHead element
        var seekHead = this.retrieveWebmNestedElement( webm, [this.WEBM_SEGMENT_ID, this.WEBM_SEEK_HEAD_ID]);
        let result = {};

        var seekElements = webm.slice(seekHead.dataStart, seekHead.dataStart + seekHead.dataLength);
        let index = 0;
        while(index < seekElements.length) {
            var seekElement = this.retrieveWebmNestedElement( seekElements, [this.WEBM_SEEK_ID], index);
            if(!seekElement) break;
            else index = seekElement.idStart + seekElement.totalLength;
            var seekId = this.retrieveWebmNestedElement( seekElements, [this.WEBM_SEEK_ID_ID], seekElement.dataStart);
            var seekPosition = this.retrieveWebmNestedElement( seekElements, [this.WEBM_SEEK_POSITION_ID], seekElement.dataStart);
            let id = this.bytesToHex(seekElements, seekId.dataStart, seekId.dataLength + seekId.dataStart);
            let pos = 0;
            for(let i=0; i<seekPosition.dataLength; i++) pos = 256*pos + seekElements[seekPosition.dataStart + i];
            result[pos] = id;
        }
        return result;
    }

    // retrieve the codec to use for a webm element
    // expect the bytes to be the beginning of the webm file
    retrieveWebmCodecFromBytes(webm){
        let codecs = {};

        // retrieve the first level track EBML element, assuming the webm is the first bytes of the file
        let trackElement = this.retrieveWebmNestedElement(webm, [this.WEBM_SEGMENT_ID, this.WEBM_TRACKS_ID]);
        if(!trackElement) return null;
        let startIndex = trackElement.dataStart;
        let endIndex = trackElement.dataStart + trackElement.dataLength;

        for(let index=startIndex; index<endIndex;){
            // retrieve the whole track data
            let trackEntryElement = this.retrieveWebmNestedElement(webm, [this.WEBM_TRACK_ENTRY_ID], index);
            if(!trackEntryElement) break;
            else index = trackEntryElement.idStart + trackEntryElement.totalLength;

            // retrieve the type of the track
            let typeElement = this.retrieveWebmNestedElement(webm, [this.WEBM_TRACK_TYPE_ID], trackEntryElement.dataStart);
            if(!typeElement) continue;
            let typeInteger = this.readWebmInteger(webm, typeElement);
            let type = this.WEBM_TRACK_TYPE[typeInteger];

            // retrieve the codec
            let codecIdElement = this.retrieveWebmNestedElement(webm, [this.WEBM_CODEC_ID], trackEntryElement.dataStart);
            if(!codecIdElement) continue;
            let codecBytes = webm.slice(codecIdElement.dataStart, codecIdElement.dataStart + codecIdElement.dataLength);
            let codecStr = String.fromCharCode(...codecBytes);
            codecs[ type ] = codecStr;
        }
        if(!codecs.audio) codecs.audio = "A_VORBIS";
        if(!codecs.video) codecs.video = "V_VP8";

        return codecs;
    }

    // retrieve the closest cluster element, starting from a random byte of a webm file
    retrieveClosestClusterStart(bytes, index=0){
        let clusterFirstBytes = new Uint8Array(this.WEBM_CLUSTER_ID.length/2);
        for(let i=0; i<clusterFirstBytes.length; i++){
            clusterFirstBytes[i] = parseInt( this.WEBM_CLUSTER_ID.slice(2*i, 2*i+2), 16);
        }

        var webmelement=null;
        let isClusterStart = false;
        while(index<bytes.length){
            // for each byte : check if it is the start of a cluster, ....
            for(let i=0; i<clusterFirstBytes.length;i++){
                if(clusterFirstBytes[i] !== bytes[index+i]) break;
                else if(i === clusterFirstBytes.length-1) isClusterStart = true;
            }
            if(isClusterStart){
                return index;
            }

            // if not check if it is the start of a block
            if(!webmelement){
                webmelement = this.isIndexBlockStart(bytes, index);
            }

            // go to the next element by testing the next byte, or the next webm element position if a webm element has already been found
            if(webmelement){
                index = webmelement.idStart + webmelement.totalLength;
                webmelement = this.isIndexBlockStart(bytes, index);
            }
            else index++;
        }
        return null;
    }

    // check if the given index of a byte sequence is the start of a new cluster block group or simple block
    // if so, return the element
    isIndexBlockStart(bytes, index, consecutiveCheck = 3, validIds = [this.WEBM_SIMPLE_BLOCK_ID, this.WEBM_BLOCK_GROUP_ID]){
        if(index >= bytes.length) return false;
        let headHexa = bytes[index].toString(16);
        if( validIds.indexOf(headHexa) === -1) return false;
        try{
            let el = this.retrieveWebmEBMLElement(bytes, index);
            if(!( el && el.id && el.dataStart && el.dataLength && validIds.indexOf(el.id) != -1) ) return false;
            if(consecutiveCheck) return this.isIndexBlockStart(bytes, el.idStart + el.totalLength, consecutiveCheck-1, [this.WEBM_SIMPLE_BLOCK_ID, this.WEBM_BLOCK_GROUP_ID]);
            else return el;
        }
        catch(e){
            return false;
        }
    }

    // retrieve the cluster element containing the given timestamp
    // allowJump allow to search a cluster randomly, by jumping to a given location rather than inspecting the next element
    retrieveTimeCluster( mstime, tryLeft = 10, allowJump = true, lastJumpPosition=null, lastPreviousCluster=null) {
        // retrieve the closest previous and next cluster
        var prevCluster = this.clusters[this.firstClusterPosition];
        var nextCluster = this.clusters[this.lastClusterPosition];
        for (let clusterPosition in this.clusters) {
            let cluster = this.clusters[clusterPosition];
            if (mstime >= cluster.timestamp && cluster.timestamp > prevCluster.timestamp) prevCluster = cluster;
            else if (mstime < cluster.timestamp && cluster.timestamp < nextCluster.timestamp) nextCluster = cluster;
        }
        // check if the nextCluster is the cluster that succeed after the prevCluster
        let prevClusterEnd = prevCluster.idStart + prevCluster.totalLength
        // set the duration of the prevCluster if the nextCluster follow it (almost) directly; the 30 margin is for malformations
        if (prevClusterEnd + 30 >= nextCluster.idStart) {
            // push the duration for the returned cluster
            prevCluster.duration = nextCluster.timestamp - prevCluster.timestamp;
        }

        // return the previous cluster if its duration is defined and the cluster contains the required timestamp
        if(prevCluster.hasOwnProperty("duration") && (prevCluster.timestamp+prevCluster.duration)>=mstime){
            return Promise.resolve(prevCluster);
        }
        if (tryLeft === 0) return Promise.reject(this.TOO_MANY_TRIES);

        let retrieveClusterPromise;
        var nextAllowJump = true;
        var searchPosition;
        // // if it is assume getting consecutive clusters will not take to long : use this method to reach the targeted cluster
        if (!allowJump) {
            searchPosition = prevCluster.idStart + prevCluster.totalLength;
        }
        else{
            searchPosition = prevCluster.idStart + Math.floor((nextCluster.idStart - prevCluster.idStart) * (mstime - prevCluster.timestamp) / (nextCluster.timestamp - prevCluster.timestamp));
            // avoid edge case where searchPosition falls inside the prevCluster
            if(searchPosition <= prevClusterEnd) searchPosition = prevClusterEnd;
            if (lastPreviousCluster == prevCluster) {
                nextAllowJump = false;
                searchPosition = Math.max(prevCluster.idStart + prevCluster.totalLength, lastJumpPosition - this.DEFAULT_CHUNK_SIZE);
            }
        }

        retrieveClusterPromise = this.retrieveClusterDataFromJump(searchPosition);
        let $this = this;
        return retrieveClusterPromise.then(function (cluster) {
            return $this.retrieveTimeCluster(mstime, tryLeft - 1, nextAllowJump, searchPosition, allowJump && prevCluster);
        })
    }

    // retriev the relative time block inside a cluster
    retrieveRelativeTimeBlockPosition(cluster, relativeMstime, blocks=null, tryLeft = 10, offset=0){
        let startPos = cluster.dataStart;
        let endPos = cluster.idStart + cluster.totalLength;
        if(!blocks){
            blocks = {};
            blocks[startPos] = { timecode : 0, idStart : startPos};
            blocks[endPos] = { timecode : cluster.duration+1, idStart : endPos};
        }
        let firstBlock = blocks[startPos];

        // retrieve the closest blocks from the relativeTime
        let prevBlock = firstBlock;
        let nextBlock = blocks[endPos];
        for(let pos in blocks){
            let block = blocks[pos];
            if( block.timecode > prevBlock.timecode && block.timecode <= relativeMstime) prevBlock = block;
            else if( (!nextBlock.timecode || block.timecode < nextBlock.timecode) && block.timecode > relativeMstime ) nextBlock = block;
        }
        if(relativeMstime - prevBlock.timecode < this.DEFAULT_BLOCK_TIME_PRECISION || !tryLeft){
            return Promise.resolve(prevBlock.idStart);
        }
        // retrieve a new block in the middle of the cluster to get closer
        let jumpTimeTarget = relativeMstime - this.DEFAULT_BLOCK_TIME_PRECISION;
        let bytesPerTime;
        if(nextBlock.timecode) bytesPerTime = (nextBlock.idStart - prevBlock.idStart) / (nextBlock.timecode - prevBlock.timecode);
        else if(firstBlock === prevBlock) bytesPerTime = cluster.totalLength / this.MAX_CLUSTER_DURATION;
        else bytesPerTime = (prevBlock.idStart - firstBlock.idStart) / (prevBlock.timecode - firstBlock.timecode);

        let jumpPosition = prevBlock.idStart + Math.floor( (jumpTimeTarget - prevBlock.timecode) * bytesPerTime - 1000) + offset;

        let $this = this;
        return this.retrieveBlockFromJump(jumpPosition)
        .then(function(block){
            // for edge case when the block has a negative timecode, thats a hack : TODO : improve this method
            if(block.timecode< 0) return $this.retrieveBlockFromJump(block.idStart + block.totalLength, 100)
            else return block
        })
        .then(function(block){
            blocks[ block.idStart ] = block;
            return $this.retrieveRelativeTimeBlockPosition(cluster, relativeMstime, blocks, tryLeft-1);
        })
        .catch(function(e){
            return $this.retrieveRelativeTimeBlockPosition(cluster, relativeMstime, blocks, tryLeft-1, offset-$this.DEFAULT_BLOCK_CHUNK_SIZE);
        })
    }

    // retrieve a block element y jumping to a random location
    retrieveBlockFromJump(jumpPosition, chunkSize=this.DEFAULT_BLOCK_CHUNK_SIZE){
        let startIndex = jumpPosition;
        let endIndex = startIndex + chunkSize;
        let $this = this;
        return this.flprovider.getChunk(this.key, startIndex, endIndex).then(function(chunk){
            let firstBlockPosition = $this.retrieveFirstBlockFromJump(chunk);
            if(firstBlockPosition === false) return Promise.reject("retrieveBlockFromJump error : no first block found in chunk between "+startIndex+" and "+endIndex);
            let block = $this.retrieveWebmEBMLElement(chunk, firstBlockPosition, {}, true, startIndex);
            block.timecode = $this.retrieveBlockTimecode(chunk, block.dataStart - startIndex);
            return Promise.resolve(block);
        });
    }

    // retrieveFirstBlock without jump
    retrieveFirstBlock(chunk, startIndex=0){
        // try with simple block : if it fails, try with BLOCK group
        let firstBlock = this.webmSearch(chunk, this.WEBM_SIMPLE_BLOCK_ID , startIndex);
        if(!firstBlock) firstBlock = this.webmSearch(chunk, this.WEBM_BLOCK_GROUP_ID , startIndex);
        if(!firstBlock) throw "retrieveFirstBlock error : no block found "+startIndex;
        return firstBlock.idStart;
    }

    // retrieve the first block element position in a bytes array
    retrieveFirstBlockFromJump(chunk, startIndex=0){
        for(let i=startIndex; i<chunk.length; i++){
            if(this.isIndexBlockStart(chunk, i)) return i;
        }
        return false;
    }

    // retrieve the first block element position in a bytes array
    retrieveLastBlock(chunk, lastIndex=chunk.length-1){
        for(let i=lastIndex; i>=0; i--){
            if(this.isIndexBlockStart(chunk, i, 3, [this.WEBM_SIMPLE_BLOCK_ID])) return i;
        }
        return false;
    }

    // retrieve a block timestamp
    retrieveBlockTimecode(bytes, dataStartIndex){
        // skip the track number :
        let trackLen = this.retrieveWebmElementLength(bytes, dataStartIndex);
        dataStartIndex += trackLen;
        let timecode = 256 * bytes[dataStartIndex] + bytes[dataStartIndex+1];
        return timecode >= 0x8000 ? timecode - 0x10000 : timecode;
    }

    // load a cluster at an expected position, and store the data
    retrieveClusterDataFromPosition(expectedPosition){
        let $this = this;
        let startIndex = expectedPosition;
        let endIndex = startIndex + this.DEFAULT_CHUNK_SIZE;
        return this.flprovider.getChunk(this.key, startIndex, endIndex).then(function(chunk){
            try{
                let cluster = $this.retrieveClusterData(chunk, $this.timestampCorrector, 0, startIndex);
                $this.clusters[ cluster.idStart ] = cluster;
                return Promise.resolve(cluster);
            }
            catch(e){
                return Promise.reject("retrieveClusterDataFromPosition error "+e);
            }
        })
    }

    // retrieve a cluster starting to a random byte
    // if not found, try after loading the previous chunk of bytes
    retrieveClusterDataFromJump(jumpPosition, tryLeft = 5){
        let $this = this;
        let startIndex = jumpPosition;
        let endIndex = startIndex + this.DEFAULT_CHUNK_SIZE;
        if(tryLeft == 0) throw "retrieveClusterDataFromJump no try left"
        return this.flprovider.getChunk( this.key, startIndex, endIndex).then(function(chunk){
            let clusterPosition = $this.retrieveClosestClusterStart(chunk);
            if(clusterPosition !== null){
                return $this.retrieveClusterDataFromPosition(startIndex + clusterPosition);
            }
            if(!tryLeft || !startIndex ) return Promise.reject();
            return $this.retrieveClusterDataFromJump(Math.max(0,jumpPosition-$this.DEFAULT_CHUNK_SIZE) , tryLeft-1);
        })
    }

    // retrieve a webm element given a succession of level id
    retrieveWebmNestedElement(webm, ids, startIndex=0){
        var el;
        for(let id of ids){
            if(el) startIndex = el.dataStart;
            var endIndex = el ? el.idStart + el.totalLength : webm.length;
            el = this.webmSearch(webm, id, startIndex, endIndex);
            if(!el) break;
        }
        return el;
    }
}

var pr = WebmVideoData.prototype;
pr.WEBM_SEGMENT_ID = "18538067";

pr.WEBM_SEEK_HEAD_ID = "114d9b74";
pr.WEBM_SEEK_ID = "4dbb";
pr.WEBM_SEEK_ID_ID = "53ab";
pr.WEBM_SEEK_POSITION_ID = "53ac";

pr.WEBM_CUES_ID = "1c53bb6b";
pr.WEBM_CUE_POINT_ID = "bb";
pr.WEBM_CUE_TIME_ID = "b3";
pr.WEBM_CUE_TRACK_POSITION_ID = "b7";
pr.WEBM_CUE_RELATIVE_POSITION_ID = "f0";
pr.WEBM_CUE_CLUSTER_POSITION_ID = "f1";

pr.WEBM_INFO_ID = "1549a966";
pr.WEBM_TIMESTAMP_SCALE_ID = "2ad7b1";
pr.WEBM_DURATION_ID = "4489";

pr.WEBM_TRACKS_ID = "1654ae6b";
pr.WEBM_TRACK_ENTRY_ID = "ae";
pr.WEBM_TRACK_TYPE_ID = "83";
pr.WEBM_CODEC_PRIVATE = "63a2";
pr.WEBM_CODEC_ID = "86";
pr.WEBM_CLUSTER_ID = "1f43b675";
pr.WEBM_SIMPLE_BLOCK_ID = "a3";
pr.WEBM_BLOCK_GROUP_ID = "a0";
pr.WEBM_REFERENCE_BLOCK_ID = "fb";
pr.WEBM_TIMESTAMP_ID = "e7";

pr.WEBM_BLOCK_TIMECODE_POSITION = 1;
pr.WEBM_BLOCK_FLAG_POSITION = 1;
pr.WEBM_KEYFRAME_FLAG_MASK = 128;
pr.WEBM_MAXIMUM_ID_LENGTH = 8;
pr.WEBM_MAXIMUM_LENGTH_LENGTH = 8;

pr.DEFAULT_CHUNK_SIZE = 300000;
pr.DEFAULT_BLOCK_CHUNK_SIZE = 30000;
pr.DEFAULT_BLOCK_TIME_PRECISION = 500;
pr.MAX_CLUSTER_DURATION = Math.pow(2, 16);
pr.MANUAL_CHANGE_THRESHOLD = 400;
pr.DEFAULT_PRELOADING_LENGTH = 300000;

pr.TOO_MANY_TRIES="too many tries";

pr.WEBM_TRACK_TYPE = {
    1 : "video",
    2 : "audio",
    3 : "complex",
    16 : "logo",
    17 : "subtitle",
    18 : "buttons",
    32 : "control"
}
// translate codecs id to codecs names
pr.WEBM_CODECS = {
    "V_VP8" : "vp8",
    "V_VP9" : "vp9",
    "A_VORBIS" : "vorbis",
    "A_OPUS" : "opus"
}


// export the class
export {
    FrdisplayerVideo,
    WebmVideoData
}