/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 ***/

// class gathering the resources for providing flock browsing.

var Flbrowser = class {

    constructor(data={}){
        this._flaptools = data.flaptools;
        this._frf = data.frf;
        this._frp = data.frp;

        // set maximum width and height of document displayed
        this.maximumWidth = data.maximumWidth || +Infinity;
        this.maximumHeight = data.maximumHeight || +Infinity;

        // store last documents retrieved in a local cache
        this.cachedDocuments = data.cachedDocuments ? data.cachedDocuments.slice(0) : [];
        this.MAX_CACHED_DOCUMENTS = [];
    }


    // build an object that will enable to access to limited resources of the flock
    buildResourceProvider(ifr){
        let browser = this;
        ifr = this.getIframe(ifr);
        let provider = {
            resize : function(width, height){
                browser.resizeIframe(ifr, width, height)
            },
            // return a given document from the flock
            getDocument : function(opts){
                return browser.getDocument(opts);
                return Promise.resolve({}); // TODO
            },
            // return a chunk of data from a document : if it is a wing, retrieve the byte associated by fetching and concatenating the appropriate feathers
            getChunk : function(key, chunkStart, endIndex, opts={}){
                let res = browser.getChunk(key, chunkStart, endIndex, opts);
                return res;
            },
            // give th body size of the document or the sum of body size in case of wing document
            contentSize(key){
                return browser.contentSize(key);
            },
            // create a signed document
            createSignedDocument(frdoc, keys, skyRoot=false, prefix = ""){
                return browser._frp.createSignedDocument(frdoc, keys, skyRoot, prefix);
            },
            // create a pair of keys for signing/encryption
            createKeys(){
                return browser._frp.createKeys();
            },
            // start / stop a feed
            feed(data={}){
                return browser._frp.feed(data);
            },
            unfeed(prefix){
                return browser._frp.unfeed(prefix);
            }
            //
        }
        return provider;
    }

    // retrieve a byte sequence of a given document
    getChunk(key, chunkStart, endIndex, opts={}){
        if(!chunkStart) chunkStart = 0;
        if(!endIndex) endIndex = +Infinity;
        let $this = this;
        // if(this.staticDataBytes[key]) return Promise.resolve(this.staticDataBytes[key].slice(chunkStart, endIndex));

        return this.getDocument( Object.assign({}, opts, {prefix:key}) ).then(function(docData){
            // if the document is in one block
            if(!docData.featherlength) return docData.body.slice(chunkStart, endIndex);

            // if the document is split in several parts, compute the keys to be retrieved
            let featherlength = docData.featherlength;
            let firstKey = Math.floor(chunkStart / featherlength);
            let lastKey = Math.floor(endIndex / featherlength);
            let keys = [];
            for(let subkey=firstKey; subkey<=lastKey; subkey++){
                let byteSubkey = $this._flaptools.intToBytes(subkey);
                let hexSubkey = $this._flaptools.bytesToHex(byteSubkey);
                keys.push( key + hexSubkey );
            }
            let promises = keys.map(hexKey=>$this.getDocument( Object.assign({}, opts, {prefix:hexKey}) ));
            let prom = Promise.all(promises).then(function(results){
                // concatenate all the bytes retrieved
                let totalSize = results.map(docData=> docData.body.length || 0).reduce((r,v)=>r+v,0);
                let body = new Uint8Array(totalSize);
                let currentIndex = 0;
                for(let doc of results){
                    body.set(doc.body || new Uint8Array(), currentIndex);
                    currentIndex += doc.body ? doc.body.length : 0;
                }
                let start = chunkStart - firstKey * featherlength;
                let end = endIndex - firstKey * featherlength;
                let res =  body.slice(start, end);
                return res;
            })
            return prom;
        })
    }

    // retrieve the size of a document body
    contentSize(key){
        // if(this.staticDataBytes[key]) return Promise.resolve(this.staticDataBytes[key].length);
        return this.getDocument({ prefix : key} ).then(function(docData){
            if(docData.winglength) return docData.wingLength;
            if(docData.body) return docData.body.length;
            return 0;
        })
    }

    // retrieve a given document
    getDocument(opts = {}){
        let $this = this;
        let prefix = opts.prefix;
        if(!prefix) return Promise.reject();
        try{
            this.getCachedDocument(opts);
        }
        catch(e){
            console.log("flbrowser error : getdocument : ", e)
        }
        let resultDoc = this.getCachedDocument(opts);
        if(!opts.timeout) opts.timeout = 20000;
        if(resultDoc){
            this.touchCachedDocument(resultDoc.key)
            return Promise.resolve(resultDoc);
        }

        // try to retrieve the document locally then remotely
        return this._frp.getResource(prefix, opts).then(function(doc){
            $this.cacheDocument(doc);
            try{
                $this.getCachedDocument(opts);
            }
            catch(e){
                console.log("flbrowser error : getdocument2 : ", e)
            }
            return $this.getCachedDocument(opts);
        })
    }

    // retrieve a document from the cached documents
    getCachedDocument(opts){
        let prefix = opts.prefix;
        for(let doc of this.cachedDocuments){
            if(prefix === doc.key) {
                return doc;
            }
        }
    }

    // add a document to the cache
    cacheDocument(doc){
        let docData = this._frf.extractDataFromSigned(doc);
        let isDocAlreadyPresent = this.touchCachedDocument(docData.key);
        if(!isDocAlreadyPresent) this.cachedDocuments.push(docData);
        if(this.cachedDocuments.length > this.MAX_CACHED_DOCUMENTS){
            this.cachedDocuments = this.cachedDocuments.slice(-this.MAX_CACHED_DOCUMENTS);
        }
    }

    // touch a document of the cache : when clearing cache, unused frdoc are removed first
    // return false if not found, else true;
    touchCachedDocument(key){
        for(let i=0; i<this.cachedDocuments.length; i++){
            let doc = this.cachedDocuments[i];
            if(doc.key != key) continue;
            this.cachedDocuments.splice(i, 1);
            this.cachedDocuments.push(doc);
            return true;
        }
        return false;
    }

    // resize an iframe to be of the given size
    // by default, resize to the iframe body element size
    resizeIframe(ifr, width=null, height=null, maximumWidth=this.maximumWidth, maximumHeight=this.maximumHeight){
        ifr = this.getIframe(ifr);
        let ifrDoc = ifr.contentDocument;
        let ifrBody = ifrDoc.body;
        let ifrHtml = document.documentElement;
        let currentIframeWidth = Math.max( ifrBody.scrollWidth, ifrBody.offsetWidth, ifrHtml.clientWidth, ifrHtml.scrollWidth, ifrHtml.offsetWidth );
        let currentIframeHeight = Math.max( ifrBody.scrollHeight, ifrBody.offsetHeight, ifrHtml.clientHeight, ifrHtml.scrollHeight, ifrHtml.offsetHeight  );
        ifr.style.width = Math.min(maximumWidth, width || currentIframeWidth)+"px";
        ifr.style.height = Math.min(maximumHeight, height || currentIframeHeight)+"px";
    }

    // trigger a function with given arguments in an iframe
    iframeTrigger(ifr, path, args){
        let method = this.iframeAccess(ifr, path);
        method(...args);
    }

    // access an element in an iframe, throw an exception if the path is not valid
    iframeAccess(ifr, path){
        ifr = this.getIframe(ifr);
        let pathParts = path.split(".").map(e=>e.trim())
        ifr = this.getIframe(ifr);
        if(!ifr) throw "iframe not found for triggering";
        let pathElement = ifr.contentDocument;
        for(let pathPart of pathParts){
            if(!pathElement.hasOwnProperty(pathPart)) throw "could not reach path in iframe : "+path;
            pathElement = pathElement[pathPart];
        }
        return pathElement;
    }

    // inject elements inside the iframe
    iframeInject(ifr, key, value){
        ifr = this.getIframe(ifr);
        ifr.contentDocument[key] = value;
    }

    // inject scripts inside the iframe
    iframeInjectScript(ifr, scriptText){
        ifr = this.getIframe(ifr);
        ifr.contentWindow.eval(scriptText);
    }

    // retrieve the iframe dom element
    getIframe(ifr){
        return typeof ifr === "string" ? document.getElementById(ifr) : ifr;
    }


    hexToBytes(hex){
        let bodyBytes = new Uint8Array(hex.length/2);
        for(let i=0; i<bodyBytes.length; i++){
            bodyBytes[i] = parseInt( hex.substr(2*i, 2), 16);
        }
        return bodyBytes;
    }

}
