// src/main.js

let squawkerCanals = [{"type":2,"address":"ws://127.0.0.1:81"},{"type":1,"address":"http://127.0.0.1:80"}];
let nodes = [{ canals : squawkerCanals }];

let aflapcp = {
    props : [],
    data : {
        test : "sofdjosdfjo",
        ////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////  GENERAL  //////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////
        /**** possible status and substatus :
         * watch
         * feed : selecting, cutting, feeding
         */
        status : { },
        // the current step and feeding a document : false,
        displayContainerId : "displayBox",
        ////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////// FOR WATCHING DOCUMENTS //////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////
        inputKey : "",
        currentKey : "AA2568ACDE",
        shareLink : "127.0.0.1/test",
        currentDocument : null,
        watchOperation : {}, // when requesting a document, display the progression toward reaching the right resource
        ////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////// FOR FEEDING /////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////
        feedFile : null,
        // type of document to be created
        newDocType : "file",
        // when cutting an object, extract some values from the pending cutAndSave operation
        cutOperation : {},
        // feeding stats per minute (M), hour(H) or all time (A)
        feedStats : {
            connectedNodes : {M : 0, H : 0, A : 0},
            feedingNodes : {M : 0, H : 0, A : 0},
            requests : {M : 0, H : 0, A : 0},
            bandwidth : {M : 0, H : 0, A : 0},
        },
        ////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////      OTHERS       ////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////
        swansServerCanals :  squawkerCanals,
        nodes : nodes,
        stuneServers : [
            {urls:'stun:stun.l.google.com:19302'},
            {urls:'stun:stun1.l.google.com:19302'},
        ],
        baseUrl : "",
        urlParams : {},
    },
    computed : {
        // the size of the file to be fed
        fileSize : function(){
            return this.cutOperation.size
        },
        // the part of the file that have already been cut and stored
        storedFileSize : function(){
            return this.cutOperation.storedFileSize || 0
        },
        fileName : function(){
            return this.cutOperation.name;
        },
        currentKeyUrl : function(){
            return this.createUrl({
                url : this.baseUrl,
                params : {
                    key : this.currentKey
                }
            })
        },
        currentDocName : function(){
            return (this.currentDocument||{}).name || "unknown"
        },
        currentDocType : function(){
            return (this.currentDocument||{}).type || "unknown"
        },
        currentDocSubtype : function(){
            return (this.currentDocument||{}).subtype || "unknown";
        },
        currentDocSize : function(){
            let d = this.currentDocument||{};
            return d.winglength || (d.body && d.body.length) || 0
        },
        currentDocWingspan : function(){
            return (this.currentDocument||{}).wingspan || 0;
        },
        newDocSubmitTitle(){
            return this.canCreateNewDoc() ? "" : "fill all data before creating the resource" ;
        }
    },
    watch: { },
    methods : {
        // change the status when clicking on the new doc button
        newDocClick(){
            let $this = this;
            $this.removeStatus();
            setTimeout(function(){
                $this.setStatus('feed', 'selecting', true);
            },0)
        },
        // getter and setters for the status
        hasStatus(status, substatus){
            let r;
            if(!this.status[status] ) r = false;
            else r = !substatus || this.status[status][substatus];
            return r;
        },
        setStatus(status, substatus, clearWhole=false){
            if(clearWhole)this.removeStatus();
            else this.removeStatus(status);
            this.addStatus(status, substatus);
        },
        addStatus(status, substatus){
            if(!this.status[status]) this.$set( this.status, status, {} );
            if(substatus) this.$set( this.status[status], substatus,  true)
        },
        removeStatus(status, substatus){
            if(!status){
                for(let s in this.status){
                    this.$delete( this.status, s )
                }
            }
            else if(!this.status[status]) return;
            else if(!substatus){
                for(let ss in this.status[status]) this.$delete( this.status[status], ss )
            }
            else if(this.status[status][substatus]) this.$delete( this.status[status], substatus )
        },
        /***
         * retrieve a document and display it in a container
         */
        display(key=this.currentKey, container=this.displayContainerId ){
            let $this = this;
            this.currentKey = key;
            $this.removeStatus("watch", "display")
            $this.setStatus("watch", "reach");
            // use a timeout 0 to give vuejs time to refresh the html : cause duplication bug otherwise
            $this.flbrowser.getDocument({prefix : key, stats : this.watchOperation}).then(function(document){
                $this.currentDocument = document;
                $this.setStatus("watch", "display")
                // feed displayed document
                $this.feedKey(key);
                // give time to vuejs to display the container of the document
                setTimeout(function(){
                    try{
                        document.vdp = $this.displayer.display(document,container);
                    }
                    catch(e){
                        console.log("vue/index error : ", e, document)
                    }
                },0)
            })

        },
        createDocument(){
            let newDocProm;
            let $this = this;
            if(this.newDocType === "file") newDocProm = this.newFrdoc_file();
            else if(this.newDocType === "thread") newDocProm = this.newFrdoc_thread();
            else throw "unsupported newDocument type : "+this.newDocType;
            this.setStatus("feed", "cutting");
            // remove the cutting status at the end of cutting
            newDocProm.then(function(obj){
                $this.removeStatus("feed", "cutting");
            })
            return Promise.race( [newDocProm, new Promise(r=>setTimeout(r,3000000)) ])
            .then(function(obj){
                let hexKey = obj ? obj.key : $this.cutOperation.key;
                $this.currentKey = hexKey;
                $this.inputKey = hexKey;
                $this.display(hexKey);
            })
        },
        /*** save and feed a new thread document ***/
        newFrdoc_thread(){
            let cutPromise = this.cutAndStore(new Uint8Array(), this.displayers.threadDisplayer);
            return cutPromise;
        },
        /*** save a file and start to share it through a new feed ***/
        newFrdoc_file : function(){
            let fileDom = this.feedFile.target;
            if(!(fileDom && fileDom.files && fileDom.files[0])) return;
            let file = fileDom.files[0];
            let $this = this;
            let cutPromise = this.cutAndStore(file, this.displayers.fileDisplayer);
            return cutPromise;

        },
        /**** cut a given file in documents and save them ****/
        cutAndStore : function(resource, displayer){
            this.cutOperation = {
                chunkSize : 1000000, // if file is too big and needs to be cut, pieces are 1MO big
                oneChunkLimit : 10000000, // file under 10MO are left in one piece
                // put it there so that the value is watched for changes
                storedFileSize : 0,
                name : "",
            }
            return displayer.cutAndStore(resource, this.cutOperation);
        },

        /***** go to a given key and start feeding it  ****/
        feedKey : function( keyHex ){
            let $this = this;
            this.addStatus("feed", "feeding");
            // reset the current document
            this.currentDocument = {};
            let data = {
                prefix : keyHex,
                span : 100,
                stats : {}
            }
            this.feedFrpStats = data.stats;
            if(this.feedCronUpdate) clearInterval(this.feedCronUpdate);
            this.feedCronUpdate = setInterval( function(){
                $this.flocknode.frp.updateFeedStatus(keyHex);
                $this.updateFeedStats();
            },1000);
            this.flocknode.frp.feed(data);
        },

        /** aggregate and redure the feeding stats based on data provided by the frp operation
         * stats are computed for last minute (M), last hour (H), all time (A)
         */
        updateFeedStats : function(){
            let currentMinute = Math.floor(Date.now()/60000);
            let stats = this.feedFrpStats || {};

            // retrieve the stats for the last available minute
            let statsM = this._aggregateFeedStats(stats, currentMinute-1, currentMinute);
            let availableMinutesM = statsM.availableMinutes||1;
            let currentMinuteLength = Date.now()/60000 - currentMinute;
            let minuteLength = 1 + currentMinuteLength; // for the last minute stats, it is actually an average of the last two minutes
            this.feedStats.requests.M = Math.floor( statsM.requests / minuteLength);
            this.feedStats.connectedNodes.M = Math.floor( statsM.connectedNodes / availableMinutesM );
            this.feedStats.feedingNodes.M = Math.floor( statsM.feedingNodes / availableMinutesM );
            this.feedStats.bandwidth.M = Math.floor( statsM.bandwidth / minuteLength);

            // retrieve the stats for the last hour
            let statsH = this._aggregateFeedStats(stats, currentMinute-59, currentMinute);
            let availableMinutesH = statsH.availableMinutes || 1;
            this.feedStats.requests.H = Math.floor( statsH.requests / availableMinutesH);
            this.feedStats.connectedNodes.H = Math.floor( statsH.connectedNodes / availableMinutesH );
            this.feedStats.feedingNodes.H = Math.floor( statsH.feedingNodes / availableMinutesH );
            this.feedStats.bandwidth.H = Math.floor( statsH.bandwidth / availableMinutesH );

            // retrieve the all time stats :
            let statsA = this._aggregateFeedStats(stats, 0, currentMinute);
            let availableMinutesA = (stats[0] ? stats[0].availableMinutes : 0 ) + statsA.availableMinutes;
            this.feedStats.requests.A = Math.floor( statsA.requests / availableMinutesA);
            this.feedStats.connectedNodes.A = Math.floor( statsA.connectedNodes / availableMinutesA );
            this.feedStats.feedingNodes.A = Math.floor( statsA.feedingNodes / availableMinutesA );
            this.feedStats.bandwidth.A = Math.floor( statsA.bandwidth / availableMinutesA );

            // concatenate old stats in minute 0
            if(!stats[0]) stats[0] = {
                availableMinutes : 0
            };
            for(let min in stats){
                if(min === 0 || (min>=currentMinute-60) ) continue;
                for(let i in stats[min]){
                    stats[0][i] = (stats[0][i] || 0) + stats[min];
                }
                stats[0].availableMinutes++;
                delete stats[min];
            }
        },
        /***
         * aggregate the stats between two minutes
         * count the number of minutes where could be provided in availableMinutes
         */
        _aggregateFeedStats : function(stats, minuteMin=0, minuteMax=+Infinity){
            let res = {
                connectedNodes : 0,
                feedingNodes : 0,
                bandwidth : 0,
                requests : 0
            };
            let availableMinutes = 0;
            for(let min in stats){
                if(min < minuteMin || min>minuteMax ) continue;
                for(let i in res){
                    if(stats[min].hasOwnProperty(i)) res[i] += stats[min][i];
                }
                availableMinutes++;
            }
            res.availableMinutes = availableMinutes;
            return res;
        },

        /***
         * extract parameters from an url and hydrate the current component
         */
        hydrateFromUrl(url=null){
            if(!url) url = window.location.href;
            let data = this.parseUrl(url);
            let params = data.params;

            this.baseUrl = data.url;

            if(params.hasOwnProperty("key")){
                this.currentKey = params.key;
                this.inputKey = params.key;
                this.display(key=this.currentKey, container=this.displayContainerId );
            }
            else{
                this.setStatus("feed", "selecting")
            }
        },
        /*** parse and retrieve data for an url ****/
        parseUrl : function(url){
            var result = {};
            var urlParts = url.split('?');
            result.url = urlParts[0];
            var paramList = urlParts[1] ? urlParts[1].split('&') : [];
            result.params = {};
            for(var param of paramList){
                var paramParts = param.split('=');
                result.params[ decodeURIComponent(paramParts[0]) ] = decodeURIComponent(paramParts[1]);
            }
            return result;
        },
        // create an url from an object created by parseUrl
        createUrl : function(data){
            let result = data.url;
            let params = data.params || {};
            if(Object.keys(params).length) result += "?";
            for(let p in params){
                result += encodeURIComponent(p) + "=" + encodeURIComponent(params[p]);
            }
            return result;
        },
        // copy a given value to the clipboard
        clipboardCopy : function(selector){
            var copyText = document.querySelector(selector);
            copyText.select();
            document.execCommand("copy");
        },
        totalBuckets : function(hexKey){
            return hexKey ? hexKey.length*4 : 0;
        },
        // check if a given document type for document creation is selected
        isNewDocTypeSelected : function(type){
            return this.newDocType == type;
        },
        // check if the user entered the right parameters to create the new frdoc
        canCreateNewDoc(){
            let type = this.newDocType;
            if(type == "file"){
                return !!this.feedFile;
            }
            if(type == "thread"){
                return true;
            }

            return true;
        }

    },
    created : function(){
        // create the flocknode
        this.flocknode = new FlockNode({
            keys : [],
            iceServers : this.stuneServers,
            passIceTimeout : false, // allow peerconnection even if not all ice candidates are gathered
            withWebrtcSpawner : true,
            withHttpSpawner : false,
            withSwansSpawner : false,
            withWebsocketSpawner : false,
            withSwansSpawn : true, // TODO set to true later
            withWebsocketSpawn : true,
            withHttpSpawn : true,
            swansServerCanals : this.swansServerCanals,
            nodes : this.nodes,
            maxInterfacedNodes : 255,
            nodeExpireTime : 10000,
            cronInterval : 3000
        })

        // create the displayer and browser
        this.flbrowser = new Flbrowser({
            flaptools : this.flocknode.flaptools,
            frf : this.flocknode.frf,
            frp : this.flocknode.frp,
            cachedDocuments : [ ]
        });
        this.provider = this.flbrowser.buildResourceProvider(this.displayContainerId);
        let dispConstructorData = {
            flprovider : this.provider,
            flaptools : this.flocknode.flaptools,
            frp : this.flocknode.frp,
            frf : this.flocknode.frf
        }
        this.displayers = {};
        this.displayers.videoDisplayer = new FrdisplayerVideo(dispConstructorData);
        this.displayers.imageDisplayer = new FrdisplayerImage(dispConstructorData);
        this.displayers.audioDisplayer = new FrdisplayerAudio(dispConstructorData);
        this.displayers.textDisplayer = new FrdisplayerText(dispConstructorData);
        let fileDisplayer = new FrdisplayerFile(dispConstructorData);
        this.displayers.threadDisplayer = new FrdisplayerThread(Object.assign({
            displayers : {
                frdisplayerText : this.displayers.textDisplayer,
                frdisplayerImage : this.displayers.imageDisplayer,
                frdisplayerFile : fileDisplayer,
                frdisplayerVideo : this.displayers.videoDisplayer,
                frdisplayerAudio : this.displayers.audioDisplayer,
            }
        },dispConstructorData));
        // when every other displayer fails, use the download
        this.displayers.fileDisplayer = fileDisplayer;
        this.displayer = new FrdisplayerMeta({
            flprovider: this.provider,
            displayers : Object.values(this.displayers)
        })
        // this.displayer = this.videoDisplayer
        document.aflap = this;

        // retrieve data from the current url
        this.hydrateFromUrl();
    },

    filters: {
        bytes : function (v=0) {
            if(!v) return "0";
            let suffix = ["B","kB","MB","GB","TB","PB"];
            let d = 1, s=0;
            while(v>=d*1000){
                d*=1000;
                s++;
            }
            let res = v/d;
            if(!Number.isInteger(res)) res = res.toPrecision(3);
            return res + " " +suffix[s];
        }
    }
}


setTimeout(function(){
    aflapcp.el = "#main";
    var app =  new Vue(aflapcp)
},0)
