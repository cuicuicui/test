/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.


 **/

try{
    assert = require("chai").assert;

    var displib = require("../../../../dist/aflap/lib/displayers-nodejs.js");
    Object.assign(global, displib);

    var flocklibs = require("../../../../dist/flock-nodejs.js")
    Object.assign(global, flocklibs);
}
catch(e){
    console.log(e)
}


describe("test webm parsing methods", function() {
    let displayer, provider, vdata;

    beforeEach(function () {
        // mock the provider
        let provider = {
            getChunk: function (key, chunkStartIndex, endIndex) {
                if (!resources[key]) return Promise.reject();
                return Promise.resolve(resources[key].slice(chunkStartIndex, endIndex));
            },
            contentSize: function (key) {
                if (!resources[key]) return Promise.reject();
                return Promise.resolve(resources[key].length);
            }
        }
        displayer = new FrdisplayerThread({flprovider: provider});
    });

    it("should create wel formed frdoc from a text message ", function(){ })

});