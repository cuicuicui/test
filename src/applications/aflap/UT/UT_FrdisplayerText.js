/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.


 **/


try{
    assert = require("chai").assert;
    var displib = require("../../../../dist/aflap/lib/displayers-nodejs.js");
    Object.assign(global, displib);

    var flocklibs = require("../../../../dist/flock-nodejs.js")
    Object.assign(global, flocklibs);
}
catch(e){
    console.log(e)
}


describe("test webm parsing methods", function() {
    let flaptools = new Flaptools();
    let flock, frf, frp, warehouse, displayer;

    // prepare a list of webm files, indexed by key
    let resourcesText = {
        "abcd": "test abcd",
    }
    let resourcesBytes = {};
    for (let i in resourcesText) {
        resourcesBytes[i] = flaptools.stringToUtf8Bytes(resourcesText[i]);
    }

    // mock the provider
    let provider = {
        getChunk: function (key, chunkStartIndex, endIndex) {
            if (!resourcesBytes[key]) return Promise.reject();
            return Promise.resolve(resourcesBytes[key].slice(chunkStartIndex, endIndex));
        },
        contentSize: function (key) {
            if (!resourcesBytes[key]) return Promise.reject();
            return Promise.resolve(resourcesBytes[key].length);
        }
    }

    beforeEach(function () {
        frp =  new Frp({flock : null});
        warehouse = new MDBWarehouse({ collection : "documents_utdisplayer" });
        frp.addWarehouse(warehouse);

        displayer = new FrdisplayerText({
            flprovider: provider,
            flaptools : flaptools,
            frp : frp
        });
    });

    afterEach(function(){
        frp.stop();
    })

    it("should create and save a text document", function (done) {
        let input = "test test test";
        let opts = {
            name : "somename"
        };
        let newdocPromise = displayer.cutAndStore(input, opts)

        newdocPromise.then(function(newdoc){
            let docdata = frp._frf.extractDataFromSigned(newdoc);
            assert.equal(docdata.type , "text");
            assert.equal(docdata.subtype , "plain");
            assert.equal(docdata.name , "somename");
            assert.deepEqual(docdata.body, new Uint8Array([ 116, 101, 115, 116, 32, 116, 101, 115, 116, 32, 116, 101, 115, 116 ]));
            done();
        })
    });

});