/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

// for nodejs testing
try{
    global.chai = require("chai");

    var displib = require("../../../dist/aflap/lib/displayers-nodejs.js");
    for(let i in displib) global[i] = displib[i];

    var jsdom = require("jsdom");
    var dom = new jsdom.JSDOM("");
    global.document = dom.window.document;

    // load the video file bytes
    var paths = ["./resources/videoWebmHexa.js", "./resources/edBytes.js", "./resources/shortBytes.js", "./resources/lavaHex.js",
                 "./resources/asjHex.js", "./resources/vp8Bytes.js", "./resources/vp9Bytes.js"];
    for(let path of paths){
        var lib = require(path);
        for(let i in lib){
            global[i] = lib[i];
        }
    }
}
catch(e){
    console.log("error :::: ", e);
    console.log("ENSURE YOU HAVE LOADED THE VIDEOS FOR THE TEST : TRY TO RUN COMMAND : ")
}

var assert = chai.assert;

// helper to transform a hexadecimal string into an Uint8Array
function hexToBytes(hex){
    let bodyBytes = new Uint8Array(hex.length/2);
    for(let i=0; i<bodyBytes.length; i++){
        bodyBytes[i] = parseInt( hex.substr(2*i, 2), 16);
    }
    return bodyBytes;
}

describe("test webm parsing methods", function(){
    let displayer, provider, vdata;

    // prepare some bytes of a webm test file
    let webmBytes = hexToBytes(videoWebmHexa);

    // prepare a list of webm files, indexed by key
    let resources = {
        "abcd" : hexToBytes(videoWebmHexa),
        "ef01" : hexToBytes(edHex),
        "ab23" : hexToBytes(vp8Hex),
        "cd45" : hexToBytes(vp9Hex),
        "ef78" : hexToBytes(lavaHex),
        "0011" : hexToBytes(asjHex)
    }

    beforeEach(function(){
        // mock the provider
        let provider = {
            getChunk : function (key, chunkStartIndex, endIndex){
                if(!resources[key]) return Promise.reject();
                return Promise.resolve(resources[key].slice(chunkStartIndex, endIndex));
            },
            contentSize: function(key){
                if(!resources[key]) return Promise.reject();
                return Promise.resolve(resources[key].length);
            }
        }
        displayer = new FrdisplayerVideo({flprovider : provider});
        vdata = new WebmVideoData();
    });

    it("should extract the next webm element in a succession of bytes", function(){

        let element = vdata.retrieveWebmEBMLElement(webmBytes);
        assert.isNotNull(element);

        // check the id returned
        assert.deepEqual(element.idStart, 0);
        assert.deepEqual(element.idLength, 4);
        assert.deepEqual(element.id, "1a45dfa3");

        // check the length returned
        assert.deepEqual(element.lengthStart, 4);
        assert.deepEqual(element.lengthLength, 1);

        // check the length returned
        assert.deepEqual(element.dataStart, 5);
        assert.deepEqual(element.dataLength, 35);

        // other data
        assert.deepEqual(element.totalLength, 40);
    });

    it("should retrieve a webm element given a path represented by an array of webm hexadecimal ids", function(){
        let element = vdata.retrieveWebmNestedElement(webmBytes, [
            "18538067", // WEBM_ROOT_ID
            "1654ae6b", // WEBM_TRACK_ID
            "ae", // WEBM_TRACK_ENTRY_ID
            "86", // WEBM_CODEC_ID
        ]);

        assert.isNotNull(element);

        // check the id returned
        assert.deepEqual(element.idStart, 260);
        assert.deepEqual(element.idLength, 1);
        assert.deepEqual(element.id, "86");

        // check the length returned
        assert.deepEqual(element.lengthStart, 261);
        assert.deepEqual(element.lengthLength, 1);

        // check the length returned
        assert.deepEqual(element.dataStart, 262);
        assert.deepEqual(element.dataLength, 5);

        // other data
        assert.deepEqual(element.totalLength, 7);
    });

    it("should retrieve the codec from a bytes array representing the webm file", function(){
        let codec = vdata.retrieveWebmCodecFromBytes(webmBytes);
        let expected = { video: 'V_VP8', audio: 'A_VORBIS' };
        assert.deepEqual(codec, expected);
    })

    it("should retrieve the head positions of the top level elements, contained in the SeekHead element", function(){
        let pos = vdata.retrieveHeadPositions(webmBytes);
        let expected = {
            64: "1549a966",
            172: "1654ae6b",
            3393: "1c53bb6b",
            2165107: "114d9b74"
        };
        assert.deepEqual(pos, expected);

    })

    it("should retrieve the start of the closest cluster, starting from a random byte", function(){
        let clusterStart = vdata.retrieveClosestClusterStart(webmBytes, 0);
        let expected = 3959;
        assert.deepEqual(clusterStart, expected);


        // try with an offset
        let offset = 154;
        let bytes = webmBytes.slice(offset);
        clusterStart = vdata.retrieveClosestClusterStart(bytes, offset);
        expected = 3805;
        assert.deepEqual(clusterStart, expected);
    });

    it("should retrieve all head elements of a webm file", function(done){
        let frdoc = {
            key : "abcd"
        };

        let prom = displayer.initVideo(frdoc);
        let expected = {
            codecs: {video : "V_VP8", audio : "A_VORBIS"},
            duration: 32480,
            firstClusterPosition: 3959,
            headPositions: {
                64:  "1549a966",
                172: "1654ae6b",
                3393 :  "1c53bb6b",
                2165107 :  "114d9b74",
            },
            mediasourceCodec: 'video/webm; codecs="vp8, vorbis"',
            timestampScale: 1000000,
            timestampCorrector : 1,

            clusters : {
                3959 : {
                    dataLength: 2161188,
                    dataStart: 3967,
                    id: "1f43b675",
                    idLength: 4,
                    idStart: 3959,
                    lengthLength: 4,
                    lengthStart: 3963,
                    timestamp: 0,
                    totalLength: 2161196,
                },
                2165175: {
                    dataLength: 0,
                    dataStart: 2165175,
                    lengthStart : 2165175,
                    lengthLength : 0,
                    idLength: 0,
                    idStart: 2165175,
                    timestamp: 32480,
                }
            }
        };

        prom.then(function(head){
            for(let i in expected){
                assert.deepEqual(head[i], expected[i], "check the validity for index : " + i);
            }
        }).then(done)
        .catch(function(e){
            console.log(e);
        })
    })

    it("should retrieve a cluster data given its expected start position", function(done){
        let frdoc = {
            key : "abcd"
        }
        let expectedPosition = 3959;
        let expectedCluster = {
            dataLength: 2161188,
            dataStart: 3967,
            id: "1f43b675",
            idLength: 4,
            idStart: 3959,
            lengthLength: 4,
            lengthStart: 3963,
            timestamp: 0,
            totalLength: 2161196
        };
        let videoData = null;
        let clusterDataPromise = displayer.initVideo(frdoc).then( function(vid){
                videoData = vid;
                return videoData.retrieveClusterDataFromPosition( expectedPosition)
        })
        .then(function(cluster){
            assert.deepEqual(cluster, expectedCluster);
        }).then(done)
    })

    it("should retrieve the closest cluster for a given timestamp, time at the beginning of the first cluster", function(done){
        let frdoc = {
            key: "ef78"
        }
        let videoData;
        let mstime = 0;
        let expected = {
            "idStart": 520,
            "idLength": 4,
            "id": "1f43b675",
            "lengthStart": 524,
            "lengthLength": 8,
            "dataStart": 532,
            "dataLength": 347399,
            "totalLength": 347411,
            "timestamp": 0
        }

        let clusterDataPromise = displayer.initVideo(frdoc).then( function(vid){
            videoData = vid;
            return videoData.retrieveTimeCluster(mstime);
        })
        .then(function(cluster){
            assert.ownInclude(cluster, expected);
        }).then(done)
    })

    it("should retrieve cluster data starting from a random byte", function(done){
        let frdoc = {
            key: "abcd"
        }
        let videoData;
        let jumpPosition = 900000;
        let expected = {
            dataLength: 2161188,
            dataStart: 3967,
            id: "1f43b675",
            idLength: 4,
            idStart: 3959,
            lengthLength: 4,
            lengthStart: 3963,
            timestamp: 0,
            totalLength: 2161196
        }
        let t1 = Date.now();

        let clusterDataPromise = displayer.initVideo(frdoc).then( function(vid){
            videoData = vid;
            return videoData.retrieveClusterDataFromJump(jumpPosition);
        })
            .then(function(cluster){
                let t2 = Date.now();
                assert.ownInclude(cluster, expected);
            }).then(done)
    })


    it("should retrieve cluster data starting from a random byte, in the middle of a big video", function(done){
        let frdoc = {
            key: "0011"
        }
        let videoData;
        let jumpPosition = 70000000;
        let expected = {

            idStart: 70011831,
            idLength: 4,
            id: "1f43b675",
            lengthStart: 70011835,
            lengthLength: 8,
            dataStart: 70011843,
            dataLength: 149392,
            totalLength: 149404,
            timestamp: 1940248
        }

        let clusterDataPromise = displayer.initVideo(frdoc).then( function(vid){
            videoData = vid;
            return videoData.retrieveClusterDataFromJump(jumpPosition);
        })
            .then(function(cluster){
                assert.ownInclude(cluster, expected);
            }).then(done)
    })


    it("should retrieve the closest cluster for a given timestamp, with time in the middle of the first cluster", function(done){
        let frdoc = {
            key: "ef78"
        }
        let videoData;
        let mstime = 2000;
        let expected = {
            "idStart": 520,
            "idLength": 4,
            "id": "1f43b675",
            "lengthStart": 524,
            "lengthLength": 8,
            "dataStart": 532,
            "dataLength": 347399,
            "totalLength": 347411,
            "timestamp": 0
        }

        let clusterDataPromise = displayer.initVideo(frdoc).then( function(vid){
            videoData = vid;
            return videoData.retrieveTimeCluster(mstime);
        })
            .then(function(cluster){
                assert.ownInclude(cluster, expected);
            }).then(done)
    })

    it("should retrieve the closest cluster for a given timestamp, with time at the middle of the video", function(done){
        let frdoc = {
            key: "ef78"
        }
        let videoData;
        let mstime = 60000;
        let expected = {
            dataLength: 282506,
            dataStart: 4383081,
            id: "1f43b675",
            idLength: 4,
            idStart: 4383069,
            lengthLength: 8,
            lengthStart: 4383073,
            timestamp: 59041,
            totalLength: 282518,
        }

        let clusterDataPromise = displayer.initVideo(frdoc).then( function(vid){
            videoData = vid;
            return videoData.retrieveTimeCluster(mstime);
        })
        .then(function(cluster){
            assert.ownInclude(cluster, expected);
        }).then(done)

    })

    it("retrieve a cluster given a time corresponding to a very far cluster", function(done){
        let frdoc = {
            key: "ef78"
        }
        let videoData;
        let mstime = 160000;
        let expected = {
            dataLength: 69136,
            dataStart: 11518753,
            id: "1f43b675",
            idLength: 4,
            idStart: 11518741,
            lengthLength: 8,
            lengthStart: 11518745,
            timestamp: 157421,
            totalLength: 69148,
        }
        let clusterDataPromise = displayer.initVideo(frdoc).then( function(vid){
            videoData = vid;
            return videoData.retrieveTimeCluster( mstime);
        })
            .then(function(cluster){
                assert.ownInclude(cluster, expected);
            }).then(done)
    })

    it("retrieve a cluster given a time corresponding to a very far cluster in the middle of a big video", function(done){
        let frdoc = {
            key: "0011"
        }
        let videoData;
        let mstime = 1800000;
        let expected = {
            idStart: 65362719,
            idLength: 4,
            id: "1f43b675",
            lengthStart: 65362723,
            lengthLength: 8,
            dataStart: 65362731,
            dataLength: 179777,
            totalLength: 179789,
            timestamp: 1799174,
            duration: 5001
        }
        let clusterDataPromise = displayer.initVideo(frdoc).then( function(vid){
            videoData = vid;
            return videoData.retrieveTimeCluster( mstime);
        })
        .then(function(cluster){
            assert.ownInclude(cluster, expected);
        }).then(done)
    })

    it("should retrieve a block given a cluster and a relative timestamp", function(done){
        this.timeout(6000)
        let frdoc = {
            key: "ef78"
        }
        let videoData;
        let relativeMstime = 3000;
        let expected = 104227;
        let clusterDataPromise = displayer.initVideo(frdoc).then( function(vid) {
            videoData = vid;
            return videoData.retrieveTimeCluster(relativeMstime);
        })
        .then(function(cluster){
            return videoData.retrieveRelativeTimeBlockPosition(cluster, relativeMstime);
        })
        .then(function(blockPosition){
            let index = 11750;
            assert.deepEqual(blockPosition, expected);
        }).then(done)

    })

    it("should start to display a video", function(){
        let frdoc = {
            key : "abcd",
            type : "video",
            subtype : "webm"
        }
        let containerId = "videotest";
        let videoDataPromise = displayer.display(frdoc,containerId);

    })
})