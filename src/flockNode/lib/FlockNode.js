/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

// create and configure a flock environment from a signle configuration object
// main flock components are accessible directly through properties

import {Flaptools} from "../../flaptools/lib/Flaptools.js"
import {Flock} from "../../flock/lib/flock.js"
import {Frf} from "../../frf/lib/Frf.js"
import {Frp} from "../../frp/lib/Frp.js"
import {NIMeta}  from "../../NestedInterface.js/lib/NIMeta.js"
import {NIWebsocketSpawner}  from "../../NestedInterface.js/lib/NIWebsocket.js"
import {NIHttpSpawner}  from "../../NestedInterface.js/lib/NIHttp.js"
import { SwanClientReceiver }  from "../../swans/lib/SwanClient.js"
import {NISwansSpawner}  from "../../NestedInterface.js/lib/NISwans.js"
import {NIWebrtcSpawner}  from "../../NestedInterface.js/lib/NIWebrtc.js"

import {IDBWarehouse} from '../../warehouse/lib/IDBWarehouse.js'
import {MDBWarehouse} from '../../warehouse/lib/MDBWarehouse.js'

class FlockNode{

    constructor(data={}){
        // set which nested interface will be created if the environment allow it
        this.withHttpSpawn = data.hasOwnProperty("withHttpSpawn") ? data.withHttpSpawn : true;
        this.withWebsocketSpawn = data.hasOwnProperty("withWebsocketSpawn") ? data.withWebsocketSpawn : true;
        this.withWebrtcSpawn = data.hasOwnProperty("withWebrtcSpawn") ? data.withWebrtcSpawn : true;
        this.withSwansSpawn = data.hasOwnProperty("withSwansSpawn") ? data.withSwansSpawn : true;
        this.withHttpSpawner = data.hasOwnProperty("withHttpSpawner") ? data.withHttpSpawner : true;
        this.withWebsocketSpawner = data.hasOwnProperty("withWebsocketSpawner") ? data.withWebsocketSpawner : true;
        this.withWebrtcSpawner = data.hasOwnProperty("withWebrtcSpawner") ? data.withWebrtcSpawner : true;
        this.withSwansSpawner = data.hasOwnProperty("withSwansSpawner") ? data.withSwansSpawner : true;

        // authorize or forbid the creation of warehouse
        this.withWarehouse =  data.hasOwnProperty("withWarehouse") ? data.withWarehouse : true;
        // set wich Warehouse are created if possible
        this.withMDBWarehouse = data.hasOwnProperty("withMDBWarehouse") ? data.withMDBWarehouse : true;
        this.withIDBWarehouse = data.hasOwnProperty("withIDBWarehouse") ? data.withIDBWarehouse : true;

        // retrieve environments parameters
        this.global = this.globalContext();
        this.serverAddress = data.serverAddress || "127.0.0.1";
        this.websocketPort = data.websocketPort || 8080;
        this.httpPort = data.httpPort;
        this.httpRoute = data.httpRoute;
        this.iceServers = data.iceServers || null;
        this.passIceTimeout = data.passIceTimeout || false;
        this.nodeExpireTime = data.nodeExpireTime || 10000;

        // create a flaptools
        this.flaptools = new Flaptools();

        // retrieve interface for enabling communication through a swans server
        this.swansServerCanals = data.swansServerCanals || { type : 2, address : "ws://127.0.0.1" };
        if(!Array.isArray(this.swansServerCanals)) this.swansServerCanals = [this.swansServerCanals];

        // create the useful nested interface
        this.metaInterface = this._createNIMeta();
        this.createNIMetaSpawners(this.metaInterface);

        // create the flock component
        let flockData = {
            flaptools : this.flaptools,
            nodeExpireTime : this.nodeExpireTime,
            metaInterface : this.metaInterface,
            withKeyFormations : true,
            // retrieve the first flock nodes to be contacted
            nodes : data.nodes || [],
        };
        if(data.maxInterfacedNodes) flockData.maxInterfacedNodes = data.maxInterfacedNodes;
        if(data.nodeExpireTime) flockData.nodeExpireTime = data.nodeExpireTime;
        if(data.cronInterval) flockData.cronInterval = data.cronInterval;
        if(data.keys) flockData.keys = data.keys;

        this.flock = new Flock(flockData);

        // create the frf component
        this.frf = new Frf();

        // create the frp component
        this.frp = new Frp({
            flaptools : this.flaptools,
            flock : this.flock,
            frf : this.frf,
            metaInterface : this.metaInterface
        })
        this.createFrpWarehouse(this.frp);
    }

    // start all the node components
    start(){ }

    // return a promise that resolve once all components of the flock node are ready
    // attributes of the flock node like canals should not be used until it has resolved
    isReady(){
        // list of promise to be waited for before the node is ready
        if(!this._readyproms) this._readyproms = [];

        // retrieve the number of current pending promises
        let initialPendingPromises = this._readyproms.length;
        let $this = this;
        return Promise.all(this._readyproms).then(function(){
            if($this._readyproms.length === initialPendingPromises){
                return Promise.resolve()
            }
            else{
                return $this.isReady();
            }
        })
    }

    // add a new promise to wait for before the node is ready
    addReadyPromise(prom){
        // list of promise to be waited for before the node is ready
        if(!this._readyproms) this._readyproms = [];
        this._readyproms.push(prom);
    }

    // stop all the node components
    stop(){
        this.metaInterface.stop();
        this.flock.stop();

        if(this.swansSpawner) this.swansSpawner.stop();
    }

    // create a NIMeta for nodejs or browser environment
    // since some spawner creation are asynchronous, add promises to determine when the node will be ready
    createNIMetaSpawners(nimeta=this.metaInterface){
        let $this = this;
        // for nodejs environment
        if(this.isNodejsEnvironment()){
            if(this.withWebsocketSpawn) nimeta.addSpawner( this._createWebsocketSpawner(true) );
            if(this.withHttpSpawn) nimeta.addSpawner(  this._createHttpSpawner(true) );
            if(this.withWebrtcSpawn) {
                if(!this.global.RTCPeerConnection){
                    try {
                        // this.retrieveLibrary("RTCPeerConnection", "wrtc");
                        this.global.RTCPeerConnection = require("wrtc").RTCPeerConnection;
                    }
                    catch (e) {
                        throw "couldn't retrieve webrtc : " + e
                    }
                }
                if (this.global.RTCPeerConnection){
                    let webrtcProm = this._createWebrtcSpawner().then(function(webrtcSpawner){
                        nimeta.addSpawner(webrtcSpawner);
                    })
                    this.addReadyPromise( webrtcProm );
                }
            }
            if(this.withSwansSpawn){
                let swansProm = this._createSwansSpawner(false).then(function(swansSpawner){
                    nimeta.addSpawner( swansSpawner );
                });
                this.addReadyPromise( swansProm );
            }
        }
        // for browser environment
        else{
            if(this.withWebsocketSpawn) nimeta.addSpawner( this._createWebsocketSpawner(false) );
            if(this.withHttpSpawn) nimeta.addSpawner(  this._createHttpSpawner(false) );
            if(this.withSwansSpawn){
                let swansProm = this._createSwansSpawner(false).then(function(swansSpawner){
                     nimeta.addSpawner( swansSpawner );
                })
                this.addReadyPromise( swansProm );
            }
            if(this.withWebrtcSpawn){
                let webrtcProm = this._createWebrtcSpawner().then(function(webrtcSpawner){
                    nimeta.addSpawner(webrtcSpawner);
                })
                this.addReadyPromise( webrtcProm );
            }
        }
    }

    // create warehouse for nodejs or webbrowser environment
    createFrpWarehouse(frp){
        if(!this.withWarehouse) return;
        let warehouse;
        // for nodejs environment
        if(this.isNodejsEnvironment()){
            if(this.withMDBWarehouse){
                warehouse = new MDBWarehouse();
            }
        }
        // for browser environment
        else{
            if(this.withIDBWarehouse){
                warehouse = new IDBWarehouse();
            }
        }
        if(warehouse){
            frp.addWarehouse(warehouse);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    ///////////////////// HELPERS FOR NESTED INTERFACE CREATION ////////////////////
    ////////////////////////////////////////////////////////////////////////////////

    // fill the nimeta with as many spawners it can contain
    _createNIMeta(){
        return new NIMeta();
    }

    _createWebsocketSpawner(withServer=true){
        return new NIWebsocketSpawner({
            withServer : this.withWebsocketSpawner,
            socketPort : this.websocketPort,
            address : "ws://"+this.serverAddress+":"+this.websocketPort // +(this.websocketPort ? ":"+this.websocketPort : "")
        });
    }

    _createHttpSpawner(withServer=true){
        return new NIHttpSpawner({
            withServer : this.withHttpSpawner,
            address : "http://"+this.serverAddress,
            serverPort : this.httpPort,
            serverRoute : this.httpRoute
        });
    }

    // create and initialize a receiver connected to a remote swans server
    _createSwansReceiver(){
        let swansServerPromise = this._createSwansServerInterface();
        let $this = this;
        return swansServerPromise.then(function(swansServer){
            let receiver = new SwanClientReceiver({
                flaptools : $this.flaptools
            })
            receiver.registerInit(swansServer);

            return receiver;
        })
    }

    // create a canal to a swansServer
    _createSwansServerInterface(){
        let swansServerInterfacePromise = this.metaInterface.spawn(this.swansServerCanals);
        return swansServerInterfacePromise;
    }

    _createSwansSpawner(withReceiver=true){
        let swansReceiverPromise = withReceiver ? this._createSwansReceiver() : Promise.resolve();
        let $this = this;
        return swansReceiverPromise.then(function(swansReceiver){
            return new NISwansSpawner({
                receiver : swansReceiver,
                spawner : $this.metaInterface,
                swansServerCanals : $this.swansServerCanals
            })
        })
    }

    _createWebrtcSpawner(){
        let signalingInterfacePromise = this.withWebrtcSpawner ? this._createSwansSpawner() : Promise.resolve(null);
        let $this = this;
        return signalingInterfacePromise.then(function(signalingInterface){
            $this.swansSpawner = signalingInterface;
            let webrtcSpawner = new NIWebrtcSpawner({
                signalingInterface : signalingInterface,
                spawner : $this.metaInterface,
                flaptools : $this.flaptools,
                ice : $this.iceServers,
                passIceTimeout : $this.passIceTimeout
            });
            return webrtcSpawner;
        })
    }

    ////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////   OTHER HELPERS    ////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////

    // check if the program is running through nodejs
    isNodejsEnvironment(){
        return typeof global === "object";
    }

    // retrieve the global context (document or global)
    globalContext(){
        return this.isNodejsEnvironment() ? global : window;
    }
}

var pr = FlockNode.prototype;

export {
    FlockNode
}