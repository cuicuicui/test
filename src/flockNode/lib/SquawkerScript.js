/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

// simple script to create a squawker that enable peers to create swans connections and advertise existing nodes based on their keys

// tested modules
var flocklibs = require("./flock-nodejs.js")
Object.assign(global, flocklibs);

// Modify the following data if needed
let params = {
    // address for websockets/http servers
    // serverAddress : "www.flockproject.com",
    // port to be used for new messaging through websockets
    websocketPort : 81,
    // port that will be used to receive http requests
    httpPort : 80,
    // authorize connections through websockets
    withWebsocketSpawn : true,
    withWebsocketSpawner : true,
    // authorize connections through webrtc
    withWebrtcSpawner : false,
    withWebrtcSpawn : false,
    // authorize connections through http
    withHttpSpawner : true,
    withWebsocketSpawner : true,
    //authorize swans connections
    withSwansSpawn : true,
    withSwansSpawner : true,
    nodeExpireTime : 10000,
    cronInterval : 10000 // disable cron by default
}

process.on('unhandledRejection', (reason, p) => {
    console.log('Unhandled Rejection at: Promise', p, 'reason:', reason);
    // application specific logging, throwing an error, or other logic here
});


//create the squawker
let squawker = new Squawker(params);



// prevent process from closing
setInterval(function(){}, 1000000)