/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

/***
 implementation of squawker class
 ***/

import {FlockNode} from "./FlockNode.js"
import { SwansServer }  from "../../swans/lib/SwansServer.js"

class Squawker extends FlockNode{
    constructor(data={}){
        // by default, forbid warehouse for squawkers since they are not supposed to store/feed any data
        if(!data.hasOwnProperty("withWarehouse")) data.withWarehouse = false;
        super(data);

        // create a swans server
        this.swansServer = this._createSwansServer();
        this.swansServer.registerCallback( this.swansRegisterCallback.bind(this) );

        // warn the flock it s a squawker
        this.flock.squawker = true;

        let canals = this.flock.getCanals();
    }

    // create a node to the flock every time a registration occurs on the swan server
    swansRegisterCallback(receiver){
        let newNode = {
            // this might not be the real id, but it is replaced once information about the node are retrieved
            // add a prefix to recognize this kind of id
            id : "0000000000000000" + this.flock.generateId(),
            interface : receiver.interface
        }
        // this.flock.addNode(newNode);
        this.flock.getNodeInfo(newNode);
    }

    /////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////     HELPERS      ////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////

    _createSwansServer(){
        let swansServer = new SwanServer({
            clientInterface : this.metaInterface,
            receiverInterface : this.metaInterface
        })
        return swansServer;
    }

    stop(){
        super.stop();
        if(this.swansServer) this.swansServer.stop();
    }
}


export {
    Squawker
}