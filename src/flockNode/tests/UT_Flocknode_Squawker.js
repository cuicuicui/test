/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

/**
 * Tests for the FlockNode.js and Squawker.js
 *
 * **/

///////////////////// testing modules /////////////////////
let chai = require("chai");
let chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
let assert = chai.assert;

// tested modules
var flocklibs = require("../../../dist/flock-nodejs.js")
Object.assign(global, flocklibs);


// CONSTANTS FOR THE TESTING
var ASYNC_TIMEOUT = 300; // time for asynchronous test to occur

// HELPERS

// helper to shift a function execution in time
let shiftCounter = 0;
let shift = function(f, sleeptime=ASYNC_TIMEOUT){
    shiftCounter+=sleeptime;
    setTimeout(f, shiftCounter);

}

// list of working stun servers
const STUN_SERVERS = [
    //{urls:'stun:stun.flockproject.com:3478'},
    {urls:'stun:stun.l.google.com:19302'},
    {urls:'stun:stun1.l.google.com:19302'},
    {urls:'stun:stun2.l.google.com:19302'},
    {urls:'stun:stun3.l.google.com:19302'},
    {urls:'stun:stun4.l.google.com:19302'},
    //{urls:'stun:stun.voipstunt.com'},
    //{urls:'stun:stun.voxgratia.org'}
]
let nextStun = 0;
// provide a different stun server each time to avoid beeing rejected
function getStunServer(){
    return [ STUN_SERVERS[ nextStun++ % STUN_SERVERS.length]];
}
////////////////////////////////////
 describe("creating components", function() {
    it("should just create a new FlockNode without error", function(done) {
        let fl = new FlockNode({
            withWebrtcSpawner : false
        });
        assert.ok(true);
        fl.stop();
        setTimeout(done, 500); // timeout to spare enough time to stop the interfaces
    });

    it("should just create a new Squawker without error", function(done) {
        let sq = new Squawker({
            withWebrtcSpawner : false
        });
        assert.ok(true);
        sq.stop();
        setTimeout(done, 500); // timeout to spare enough time to stop the interfaces
    });
});

 describe("trying a getNode request on a flockNode call with any of the available canal", function(){
    // the first node is the one we try to contact
    let flockNode1;
    // the second node is the one making the requests
    let flockNode2;

    // squawker for webrtc signaling
    let squawker;

    let targetKey = "00";

    let nodeRepresentation;

    beforeEach(function(){
        squawker = new Squawker({
            websocketPort : 84,
            withHttpSpawner : false,
        })

        flockNode1 = new FlockNode({
            websocketPort : 80,
            httpPort : 81,
            swansServerCanals : squawker.flock.getCanals(),
        });

        flockNode2 = new FlockNode({
            websocketPort : 82,
            httpPort : 83,
            swansServerCanals : squawker.flock.getCanals(),
        });

        return new Promise(res=>setTimeout(res, 1000));
    })

    afterEach(function(){
        flockNode1.stop();
        flockNode2.stop();
        squawker.stop();

    })

    it("should send getNode through the http canal", function(done){
        let canals = flockNode1.flock.getCanals();
        let httpCanal = canals.find(canal=>canal.type === 1);
        if(!httpCanal) throw "no http canal in htis environment";

        nodeRepresentation = {
            canals : [ httpCanal ]
        };

        let prom = flockNode2.flock.getNodeNode(nodeRepresentation, targetKey).then(function(nodes){
            assert.isArray(nodes);
            assert.isNotEmpty(nodes);
            done();
        }).catch(function(e){
            console.log("error during getNodeNode promise", e);
            throw "error caught : "+e;
        })
    })

    it("should send getNode through the websocket canal", function(done){
        let canals = flockNode1.flock.getCanals();
        let websocketCanal = canals.find(canal=>canal.type === 2);
        if(!websocketCanal) throw "no websocket canal in htis environment";

        nodeRepresentation = {
            canals : [ websocketCanal ]
        };

        let prom = flockNode2.flock.getNodeNode(nodeRepresentation, targetKey).then(function(nodes){
            assert.isArray(nodes);
            assert.isNotEmpty(nodes);
            done();
        }).catch(function(e){
            console.log("error during getNodeNode promise", e);
            throw "error caught : "+e;
        })
    })

    it("should send getNode through the webrtc canal", function(done){
        let canals = flockNode1.flock.getCanals();
        let webrtcCanal = canals.find(canal=>canal.type === 3);
        if(!webrtcCanal) throw "no webrtc canal in htis environment";

        nodeRepresentation = {
            canals : [ webrtcCanal ]
        };
        shift(function(){
            let prom = flockNode2.flock.getNodeNode(nodeRepresentation, targetKey).then(function(nodes){
                assert.isArray(nodes);
                assert.isNotEmpty(nodes);
                done();
            }).catch(function(e){
                console.log("error during getNodeNode promise", e);
                throw "error caught : "+e;
            })
        })

    })
})
 describe("trying a getNode request on a flockNode call with any of the available canal", function() {
    let squawker;
    // the first node is the one we try to contact
    let flockNode1;
    let warehouse;

    // the second node is the one making the requests
    let flockNode2;


    let frdoc = {
        version : 55,
        type : "application",
        subtype : "octet-stream",
        name : "myfile.bin",
        signtools : {
            version : 0,
            type : "rsa4096",
            n : "aabcddee",
            e : "03",
            d : "1234567890abcdef"
        },
        signature : "aabbccddeeff102455",
        expire : 12345,
        dversion :124,
        prefix : 3,
        wingspan : "22",
        nextfeather : "99",
        featherlength: 100,
        body : new Uint8Array([11,22,88,95,41])
    }

    let targetKey;

    let nodeRepresentation;

    beforeEach(function () {
        squawker = new Squawker({
            websocketPort : 84,
            withHttpSpawner : false,
        })

        flockNode1 = new FlockNode({
            websocketPort : 80,
            httpPort : 81,
            swansServerCanals : squawker.flock.getCanals(),
        });

        flockNode2 = new FlockNode({
            websocketPort : 82,
            httpPort : 83,
            swansServerCanals : squawker.flock.getCanals(),
        });
        targetKey = flockNode1.flock.getKeys()[0].prefix;
        frdoc.key = targetKey;

        warehouse = flockNode1.frp.getWarehouses().pop();
        let frdocBytes = flockNode1.frp._frf.documentToBytes(frdoc);
        return warehouse.storeResource(targetKey, frdocBytes, {dversion : frdoc.dversion});
    })

    afterEach(function () {
        flockNode1.stop();
        flockNode2.stop();
        squawker.stop();
        return warehouse.deleteResource(targetKey);
    });


    it("should retrieve a document through a http canal", function(done){
        let canals = flockNode1.flock.getCanals();
        let httpCanal = canals.find(canal=>canal.type === 1);
        if(!httpCanal) throw "no http canal in htis environment";
        nodeRepresentation = {
            id : flockNode1.flock.getId(),
            keys : [{prefix : targetKey, span : 0}],
            canals : [ httpCanal ]
        };
        flockNode2.flock.addNode(nodeRepresentation);

        flockNode2.frp.request(targetKey).then(function(result){
            assert.deepEqual(result, frdoc);
            done();
        }).catch(function(e){
            assert.ok(false, "an error occured : "+e)
        })
    })

    it("should retrieve a document through a websocket canal", function(done){
        let canals = flockNode1.flock.getCanals();
        let websocketCanal = canals.find(canal=>canal.type === 2);
        if(!websocketCanal) throw "no websocket canal in htis environment";
        nodeRepresentation = {
            id : flockNode1.flock.getId(),
            keys : [{prefix : targetKey, span : 0}],
            canals : [ websocketCanal ]
        };
        flockNode2.flock.addNode(nodeRepresentation);
        flockNode2.frp.request(targetKey).then(function(result){
            assert.deepEqual(result, frdoc);
            done();
        })
    })
    it("should retrieve a document through a webrtc canal", function(done){
        let canals = flockNode1.flock.getCanals();
        let webrtcCanal = canals.find(canal=>canal.type === 3);
        if(!webrtcCanal) throw "no webrtcCanal canal in this environment";
        nodeRepresentation = {
            id : flockNode1.flock.getId(),
            keys : [{prefix : targetKey, span : 0}],
            canals : [ webrtcCanal ]
        };
        flockNode2.flock.addNode(nodeRepresentation);
        flockNode2.frp.request(targetKey).then(function(result){
            assert.deepEqual(result, frdoc);
            done();
        })
    })
});

 describe("testing Squawker node gestion", function(){
    let squawker;
    let flockNode1, flockNode2, flockNode3, flockNode4, flockNode5;

    beforeEach(function(){
        shiftCounter = 1;
        squawker = new Squawker({
            withWebrtcSpawner : false,
            nodeExpireTime : 30,
            cronInterval : 100
        });
    })

    afterEach(function(){
        squawker.stop();
        squawker = null;
        if(flockNode1) flockNode1.stop();
        flockNode1 = null;
        if(flockNode2) flockNode2.stop();
        flockNode2 = null;
        if(flockNode3) flockNode3.stop();
        flockNode3 = null;
        if(flockNode4) flockNode4.stop();
        flockNode4 = null;
        if(flockNode5) flockNode5.stop();
        flockNode5 = null;
    })

    it("should add nodes to the squawker when flocks are registering", function(done){
        this.timeout(5000);

        // at start, the squawker must have no nodes
        let squawkNodes = squawker.flock.getNodes();
        assert.isArray(squawkNodes);
        assert.isEmpty(squawkNodes);
        let port = 9000;

        // create some flockNode with swans poiting to the squawker
        flockNode1 = new FlockNode({
            swansServerCanals : squawker.flock.getCanals(),
            websocketPort : port++,
            httpPort : port++
        });

        shift(function(){
            let squawkNodes = squawker.flock.getNodes();
            assert.isArray(squawkNodes);
            assert.isNotEmpty(squawkNodes);
            let firstNode = squawkNodes[0];
            assert.isNotNull(firstNode.interface);
            assert.equal(firstNode.id, flockNode1.flock.getId() );
            let retrievedCanals = firstNode.canals;
            let expectedCanals = flockNode1.flock.getCanals();
            let canalSort = (c1,c2) => c1.type < c2.type ? -1 : 1;
            retrievedCanals.sort(canalSort);
            expectedCanals.sort(canalSort);
            assert.deepEqual(retrievedCanals, expectedCanals);
        })

        shift(function(){
            flockNode2 = new FlockNode({
                swansServerCanals : squawker.flock.getCanals(),
                websocketPort : port++,
                httpPort : port++
            });

            flockNode3 = new FlockNode({
                swansServerCanals : squawker.flock.getCanals(),
                websocketPort : port++,
                httpPort : port++
            });

            flockNode4 = new FlockNode({
                swansServerCanals : squawker.flock.getCanals(),
                websocketPort : port++,
                httpPort : port++
            });

            flockNode5 = new FlockNode({
                swansServerCanals : squawker.flock.getCanals(),
                websocketPort : port++,
                httpPort : port++
            });
        })

        shift(function(){
            let squawkNodes = squawker.flock.getNodes();
            assert.isArray(squawkNodes);
            assert.lengthOf(squawkNodes, 5);
        })

        // stop a node, should see it in the squawker node list
        shift(function(){
            flockNode1.stop();
            squawker.flock.nodeExpireTime = 10;
        })
        shift(function(){ })
        shift(function(){
            let squawkNodes = squawker.flock.getNodes();
            assert.isArray(squawkNodes);
            assert.lengthOf(squawkNodes, 4);
        })
        shift(done);
    })
})

describe("testing peer network creation", function(){
    this.timeout(300000);
    // TODO : optimize components so that those tests can rune faster !!!!!

    // display unhandled promise rejections
    process.on('unhandledRejection', (error, p) => {
        // Will print "unhandledRejection err is not defined"
        console.log('UNHANDLEDREJ', error, p);
    });
    let flockNodes, squawkers;

    let port = 4000; // port to use for http /  websocket


    function _createSquawker(){
        let squawker = new Squawker({
            websocketPort : port++,
            httpPort : port++,
            withWebrtcSpawner : false,
            withWebrtcSpawn : false,
            withHttpSpawner : true,
            withWebsocketSpawner : true,
            withSwansSpawn : true,
            withSwansSpawner : false,
            nodeExpireTime : 1000000,
            cronInterval : 1000000000 // disable cron by default
        })
        squawker.flock.squawker = true;
        squawkers.push(squawker);
        return squawker;
    }

    function _createFlockNode(nodeKeyPrefix, swansServerCanals){
        let fn = new FlockNode({
            keys : [{prefix : nodeKeyPrefix, span:10}],
            websocketPort: port++,
            iceServers : getStunServer(),
            passIceTimeout : true, // allow peerconnection even if not all ice candidates are gathered
            withWebrtcSpawner : true,
            withWebsocketSpawner : false,
            withHttpSpawner : false,
            withSwansSpawn : true,
            withSwansSpawner : false,
            swansServerCanals : swansServerCanals,
            nodeExpireTime : 1000000,
            cronInterval : 1000000000
        })
        return fn;
    }

    // create a nodeKey, starting with the keys to populate "00"
    function getNextNodeKey(flockNodes, flockNodeKeyLength){
        let currentNodeNumber = Object.keys(flockNodes).length;
        let key;
        // create the flocks to that will be targeted by populate "00"

        if(currentNodeNumber<flockNodeKeyLength*8){
            key = ((255>>currentNodeNumber)&255)
            return toHex(key);
        }
        // create other nodes
        else{
            key = 0;
            while(toHex(key) in flockNodes) key++;
            return toHex(key);
        }
    }

    function toHex(i){
        i = i.toString(16);
        while(i.length%2) i = "0"+i;
        return i;
    }

    beforeEach(function(){
        flockNodes = {};
        squawkers = [];
        shiftCounter = 0;
    })

    afterEach(function(){
        shiftCounter = 1;
        for(let i in flockNodes) flockNodes[i].stop();
        for(let i in squawkers) squawkers[i].stop();
    })

    it("should create connections with peers automatically, with every flockNode linking to every squawker", function(done){
        // create a unique squawker
        let squawker = _createSquawker();
        squawkers.push(squawker);

        // number of nodes to create
        let nFlockNodes = 16;
        let flockNodeKeyLength = 1;

        // create several flockNodes, prioritizing those needed to populate for key "00"


        for(let i=0; i<nFlockNodes; i++){
            let nodeKeyPrefix = getNextNodeKey(flockNodes, flockNodeKeyLength);
            let swansServerCanals = squawker.flock.getCanals();
            flockNodes[nodeKeyPrefix] = _createFlockNode(nodeKeyPrefix, swansServerCanals );
        }

        // fore each node, add the squawker to know about other nodes
        for(let i in flockNodes){
            let flockNode = flockNodes[i]
            flockNode.flock.addNode({
                id : squawker.flock.getId(),
                canals : squawker.flock.getCanals(),
                squawker : true
            })
            assert.isNotEmpty(flockNode.flock.nodes);
        }

        shift(function(){
            for(let i in flockNodes){
                let flockNode = flockNodes[i];
                // start the cron to perform one iteration of it : this will trigger the populate operations
                flockNode.flock.cron();
            }
        }, 9000)

        shift(function(){
            let flockNode = flockNodes["00"];
            let connectedNodes = flockNode.flock.getConnectedNodes();
            let formation = Object.values( flockNode.flock.formations ) .pop();
            console.log("connectedNodes ::::::::::::::::::: ", connectedNodes.length, connectedNodes.map(n=>n.keys).map(v=>v[0] ? v[0].prefix : null))
            let emptyBuckets = flockNode.flock.getMissingBuckets(formation, connectedNodes);
            console.log("emptyBuckets ::::::::::::::::::: ", emptyBuckets)
            assert.isEmpty(emptyBuckets, "there should be no empty bucket ");
            assert.isAtLeast(connectedNodes.length, 1 + 8*flockNodeKeyLength)
        }, 25000)

        shift(done);
    })

     it("should populate nodes that cannot access peer directly from a squawker", function(done){
        // number of nodes to create
        let nFlockNodes = 12;
        let flockNodeKeyLength = 1;

         // create one squawker per flocknode
         let nSquawkers = nFlockNodes;
         for(let i=0; i<nSquawkers;i++) squawkers.push( _createSquawker() );

        // create several flockNodes, prioritizing those needed to populate for key "00"

        for(let i=0; i<nFlockNodes; i++){
            let nodeKeyPrefix = getNextNodeKey(flockNodes, flockNodeKeyLength);
            let swansServerCanals = squawkers[i].flock.getCanals();
            flockNodes[nodeKeyPrefix] = _createFlockNode(nodeKeyPrefix, swansServerCanals );
        }
         shift(function(){
             // let each flocknode know about a squawker and a few other nodes
             let flocknodesArr = Object.values(flockNodes);
             for(let i=0; i<nFlockNodes; i++){
                 // add the squawker
                 let flockNode = flocknodesArr[i];
                 let squawker = squawkers[i];
                 flockNode.flock.addNode({
                     id : squawker.flock.getId(),
                     canals : squawker.flock.getCanals(),
                     squawker : true
                 })
                 // add the three next nodes
                 for(let j=1; j<=3; j++){
                     let fln = flocknodesArr[(i+j)%nFlockNodes];
                     flockNode.flock.addNode({
                         id : fln.flock.getId(),
                         canals : fln.flock.getCanals(),
                         keys : fln.flock.getKeys()
                     })
                 }
             }
             // fore each node, start the cron
             for(let i in flockNodes){
                 let flockNode = flockNodes[i];
                 flockNode.flock.cron();
             }
         }, 8000)

        shift(function(){
            let flockNode = flockNodes["00"];
            let connectedNodes = flockNode.flock.getConnectedNodes();
            let formation = Object.values( flockNode.flock.formations ) .pop();
            let emptyBuckets = flockNode.flock.getMissingBuckets(formation, connectedNodes);
            assert.isEmpty(emptyBuckets, "there should be no empty bucket ");
        }, 25000)

        shift(done);
    })

    it("should create connections with peers automatically, and replacing connections when they shut down", function(done){
        // create a unique squawker
        let squawker = _createSquawker();
        squawkers.push(squawker);

        // will try to close the node corresponding to bucket 5
        let targetBucket = 5;
        let bucketKeys = ["04", "05", "06", "07"];
        let firstBucketKey; // key of the first node that is connected for this bucket
        let secondBucketKey; // key for the second node, once the first node has been stopped

        // number of nodes to create
        let nFlockNodes = 16;
        let flockNodeKeyLength = 1;

        // create several flockNodes, prioritizing those needed to populate for key "00"


        for(let i=0; i<nFlockNodes; i++){
            let nodeKeyPrefix = getNextNodeKey(flockNodes, flockNodeKeyLength);
            let swansServerCanals = squawker.flock.getCanals();
            flockNodes[nodeKeyPrefix] = _createFlockNode(nodeKeyPrefix, swansServerCanals );
        }

        // starts the tests once all nodes are ready
        let readyPromises = Promise.all( Object.values(flockNodes).map(fln=>fln.isReady()) );

        readyPromises.then(function(){
            // fore each node, add the squawker to know about other nodes
            for(let i in flockNodes){
                let flockNode = flockNodes[i]
                flockNode.flock.addNode({
                    id : squawker.flock.getId(),
                    canals : squawker.flock.getCanals(),
                    squawker : true
                })
            }

            // shift(function(){
                for(let i in flockNodes){
                    let flockNode = flockNodes[i];
                    assert.isNotEmpty(flockNode.flock.nodes);

                    // flockNode.flock.cron();
                }
                // start the cron to perform one iteration of it : this will trigger the populate operations
                flockNodes["00"].flock.cronInterval(1000);
            // }, 2000)

            shift(function(){
                let flockNode = flockNodes["00"];
                let connectedNodes = flockNode.flock.getConnectedNodes();
                let formation = Object.values( flockNode.flock.formations ).pop();
                let connectedNodeKeys = connectedNodes.map(n=>n.keys).map(v=>v[0] && v[0].prefix );
                let connectedNodeIds = connectedNodes.map(v=>v.id);

                let emptyBuckets = flockNode.flock.getMissingBuckets(formation, connectedNodes);
                assert.isEmpty(emptyBuckets, "there should be no empty bucket ::: "+JSON.stringify(emptyBuckets) );
                let bucketNodes = connectedNodeKeys.filter(v=>bucketKeys.includes(v) );
                assert.ok(bucketNodes.length >= 1, "there should be at least one connected node for bucket : " + targetBucket + " , got ::: " + JSON.stringify(bucketNodes) );
                // disconnect the given node
                let firstBucketKey = bucketNodes[0]
                flockNodes[ firstBucketKey ].stop();
            }, 8000)

            shift(function(){
                let flockNode = flockNodes["00"];
                let connectedNodes = flockNode.flock.getConnectedNodes();
                let formation = Object.values( flockNode.flock.formations ) .pop();
                let connectedNodeKeys = connectedNodes.map(n=>n.keys).map(v=>v[0] ? v[0].prefix : null);

                let emptyBuckets = flockNode.flock.getMissingBuckets(formation, connectedNodes);
                assert.isEmpty(emptyBuckets, "there should be no empty bucket ::: "+JSON.stringify(emptyBuckets) + "for key : 00 from connected nodes "+ JSON.stringify(connectedNodes.map(n=>n.keys)) );
                let bucketNodes = connectedNodeKeys.filter(v=>bucketKeys.includes(v) );
                assert.lengthOf(bucketNodes, 1, "there should be exactly one connected node for bucket : " + targetBucket + " , got ::: " + JSON.stringify(bucketNodes) );
                // disconnect the given node
                secondBucketKey = bucketNodes[0]
                assert.notEqual( secondBucketKey , firstBucketKey, "the new sample for the targeted bucket must not be the node that has been closed")
            }, 8000)
            shift(done);
        })
    })

})

