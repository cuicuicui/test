/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

/**
 * Tests for the Frf.js
 * 
 * **/

///////////////////// testing modules /////////////////////
let chai = require("chai");

// tested modules
var flocklibs = require("../../../dist/flock-nodejs.js")
Object.assign(global, flocklibs);


let assert = chai.assert;

describe("testing Frf initialization", function(){
	it("should create a Frf instance without error", function(){
		let frf = new Frf();
		assert.ok(true);
	})
});

describe("testing encode/decode for signatures", function(){
	let frf = new Frf();
	
	it("should encode a signature with rsa1024 data", function(){
		let encoded, expectedEncoded, decoded;
		let rsa1024 = {
			version : 0,
			type : "rsa1024",
			n : "aabbccddeeff00112233445577668899",
			e : "03",
			d : "1234567890abcdef"
		};
		expectedEncoded = new Uint8Array([
			// version
			0,
			0,
			// type
			2,
			1,
			// n (16) with 1 byte value length
			48, 
			// value length
			16,
			// n value
			170,
			187,
			204,
			221,
			238,
			255,
			0,
			17,
			34,
			51,
			68,
			85,
			119,
			102,
			136,
			153,
			// e(17) on 1 byte value(0)
			17,
			3,
			// d (18) on 1 byte value length
			50,
			// d length
			8,
			// d value
			18,
			52,
			86,
			120,
			144,
			171,
			205,
			239
		]);
		encoded = frf.signtoolsToBytes(rsa1024);
		decoded = frf.signtoolsToObject(encoded);
		assert.deepEqual(decoded, rsa1024);
	});
	
	
	it("should encode a signature with rsa2048 data", function(){
		let encoded, expectedEncoded, decoded;
		let rsa1024 = {
			version : 0,
			type : "rsa2048",
			n : "aabbccddeeff00112233445577668899",
			e : "03",
			d : "1234567890abcdef"
		};
		expectedEncoded = new Uint8Array([
			// version
			0,
			0,
			// type
			2,
			2,
			// n (16) with 1 byte value length
			48, 
			// value length
			16,
			// n value
			170,
			187,
			204,
			221,
			238,
			255,
			0,
			17,
			34,
			51,
			68,
			85,
			119,
			102,
			136,
			153,
			// e(17) on 1 byte value(0)
			17,
			3,
			// d (18) on 1 byte value length
			50,
			// d length
			8,
			// d value
			18,
			52,
			86,
			120,
			144,
			171,
			205,
			239
		]);
		encoded = frf.signtoolsToBytes(rsa1024);
		decoded = frf.signtoolsToObject(encoded);
		assert.deepEqual(decoded, rsa1024);
	});
	
	
	it("should encode a signature with rsa4096 data", function(){
		let encoded, expectedEncoded, decoded;
		let rsa1024 = {
			version : 0,
			type : "rsa4096",
			n : "aabbccddeeff00112233445577668899",
			e : "03",
			d : "1234567890abcdef"
		};
		expectedEncoded = new Uint8Array([
			// version
			0,
			0,
			// type
			2,
			3,
			// n (16) with 1 byte value length
			48, 
			// value length
			16,
			// n value
			170,
			187,
			204,
			221,
			238,
			255,
			0,
			17,
			34,
			51,
			68,
			85,
			119,
			102,
			136,
			153,
			// e(17) on 1 byte value(0)
			17,
			3,
			// d (18) on 1 byte value length
			50,
			// d length
			8,
			// d value
			18,
			52,
			86,
			120,
			144,
			171,
			205,
			239
		]);
		encoded = frf.signtoolsToBytes(rsa1024);
		decoded = frf.signtoolsToObject(encoded);
		assert.deepEqual(decoded, rsa1024);
	});
});



describe("testing encode/decode for documents", function(){
	let frf = new Frf();
	
	it("should encode/decode a document with all its data defined", function(){
		let document, encoded, expectedEncoded, decoded;
		document = {
			version : 55,
			type : "application",
			subtype : "octet-stream",
			name : "myfile.bin",
			signtools : {
				version : 0,
				type : "rsa4096",
				n : "aabcddee",
				e : "03",
				d : "1234567890abcdef"
			},
            featherlength : 0,
			signature : "aabbccddeeff102455",
			expire : 12345,
			dversion :124,
			key : "aabbccddeeff",
			prefix : 3,
			wingspan : "00112266",
			nextfeather : "11445566",
			body : new Uint8Array([44, 55, 66]),
			time : Date.now(),
			author : "鸟chirpchirp"
		}
		expectedEncoded = new Uint8Array([
			// version
			0,
			55,
			// type
			2,
			6,
			// subtype
			35,
			// subtype length
			12,
			// subtype value
			111,
			99,
			116,
			101,
			116,
			45,
			115,
			116,
			114,
			101,
			97,
			109,
			// name
			36,
			// name length
			10,
			// name value
			109,
			121,
			102,
			105,
			108,
			101,
			46,
			98,
			105,
			110,
			// signtools
			37,
			// signtools length
			22,
			// signtools value
			0,
			0,
			2,
			3,
			48,
			4,
			170,
			188,
			221,
			238,
			17,
			3,
			50,
			8,
			18,
			52,
			86,
			120,
			144,
			171,
			205,
			239,
			// signature
			38,
			// signature length
			9,
			// signature value
			170,
			187,
			204,
			221,
			238,
			255,
			16,
			36,
			85,
			// expire
			39,
			// expire length
			2,
			// expire value
			48,
			57,
			// dversion
			8,
			124,
			// key
			41,
			// key length
			6,
			// key value
			170,
			187,
			204,
			221,
			238,
			255,
			// prefix
			10,
			3,
			// wingspan
			43,
			// wingspan length
			4,
			// wingspan value
			0,
			17,
			34,
			102,
			// nextFeather
			44,
			// nextFeather length
			4,
			// nextFeather value
			17,
			68,
			85,
			102,
			// body
			45,
			// body length
			3,
			// body value
			44,
			55,
			66
		]);
  
		encoded = frf.documentToBytes(document);
		decoded = frf.documentToObject(encoded);
		assert.deepEqual(decoded, document);
	});
});

describe("test various signature methods", function(){
	let frf = new Frf();

	it("should create a key pair", function(){
		let keys = frf.createKeys();
		assert.isObject(keys);
        assert.isNotNull(keys.private);
        assert.isNotNull(keys.public);
	})

	it("should compute a signature of a document", function(){
		let bytes = new Uint8Array([1,2,3,4]);
		let keys = frf.createKeys();

		// test by giving either the full key or the private key
        let signatureWithFullKey = frf.computeSignature(bytes, keys);
        let signatureWithPrivateKey = frf.computeSignature(bytes, keys.private);
        assert.isNotEmpty(signatureWithFullKey)
        assert.isString(signatureWithFullKey)
		assert.equal(signatureWithFullKey, signatureWithPrivateKey);
	})

	it("should create a sky or subsky by pushing public keys in the document", function(){
        let doc1 = {
            key : "aabbccde012345",
            body : new Uint8Array([1,2,3,4]),
        }
        let doc2 = Object.assign({}, doc1);
        let keys = frf.createKeys();

        let expected = Object.assign({ signtools : keys.public}, doc1);
        // test to create a sky by giving the full key or just the public key
        let skyWithFullKey = frf.createSky(doc1, keys);
        let skyWithPublicKey = frf.createSky(doc2, keys.public);

        assert.deepEqual(skyWithFullKey, expected);
        assert.deepEqual(skyWithPublicKey, expected);
	})

	it("should create a signed document that encapsulate a given document", function(){
        let doc = {
            key : "aabbccde012345",
            body : new Uint8Array([1,2,3,4]),
        }
        let keys = frf.createKeys();
        let signedDoc = frf.createSignedDocument(doc, keys);
        assert.isString(signedDoc.signature);
        assert.isNotEmpty(signedDoc.signature);
        let expectedBody = frf.documentToBytes(doc);
        assert.instanceOf(signedDoc.body, Uint8Array);
        assert.deepEqual(signedDoc.body, expectedBody);
	})

    // it("should create a signed document that encapsulate a given document, providing a prefix to be prepended and a specific key algorithm", function(){
    //     let doc = {
    //         key : "aabbccde012345",
    //         body : new Uint8Array([1,2,3,4]),
    //     }
    //     let prefix = "aabbccddeeffaabbccddeeff";
    //     let keys = frf.createKeys( frf._secswans.HASHTYPE_SHA_256);
    //     let signedDoc = frf.createSignedDocument(doc, keys, true , prefix);
    //     assert.isString(signedDoc.signature);
    //     assert.isNotEmpty(signedDoc.signature);
    //     let expectedBody = frf.documentToBytes(doc);
    //     assert.instanceOf(signedDoc.body, Uint8Array);
    //     assert.deepEqual(signedDoc.body, expectedBody);
    // })

})

describe("testing the checkDocument method", function(){
	this.timeout(4000)
	let frf = new Frf();

	// build several signtools
    let signtools1 = frf.createKeys();
    let signtools2 = frf.createKeys();
    let signtools3 = frf.createKeys();
    let signtools4 = frf.createKeys();
    let signtools5 = frf.createKeys( frf._secswans.HASHTYPE_SHA_256 );

	// build several documents
	let skyRootDocument = frf.createSky({
		body : new Uint8Array([1,2,4,8,16,32,64,128]),
	},signtools1)
    let signed_skyRootDocument = frf.createSignedDocument(skyRootDocument, signtools1, true);
	let skyRootKey = signed_skyRootDocument.key;
    let doc1 = {
        key : skyRootKey + "bbbbbb",
        body : new Uint8Array([2,3,4,5,6,7,8,9]),
        prefix : skyRootKey.length / 2
    }

    let doc1_with_wrong_keystart = {
        key : skyRootKey + "bbbbbb",
        body : new Uint8Array([2,3,4,5,6,7,8,9]),
        prefix : skyRootKey.length / 2,
        keystart : "aabbccddeeff00112233445566"
    }

    let doc1_with_good_keystart = {
        key : skyRootKey + "bbbbbb",
        body : new Uint8Array([2,3,4,5,6,7,8,9]),
        prefix : skyRootKey.length / 2,
        keystart : skyRootKey
    }

    let doc2 = frf.createSky({
        key : doc1.key + "cccccc",
        body : new Uint8Array([88,87,86,85,84,83,82,81,80]),
        prefix : doc1.key.length/2,
    }, signtools2);


    let doc3 = {
        key : doc2.key + "dddddd",
        body : new Uint8Array([]),
        prefix : doc2.key.length/2,
    }

    let doc4 = frf.createSky({
        key : doc3.key + "eeeeee",
        body : new Uint8Array([11,12,13,14,15]),
        prefix : doc3.key.length/2,
    },signtools3);

    let doc5 = {
        key : doc4.key + "ffff",
        body : new Uint8Array([3,8,9]),
        prefix : doc4.key.length/2,
		signtools : signtools5.public
    }

    let doc6 = {
        body : new Uint8Array([3,8,9,10,11,12,13,14,15]),
        prefix : doc5.key.length/2,
    }


    // create the signed documents encapsulating the documents
    let signed_doc1 = frf.createSignedDocument(doc1, signtools1);
    let signed_doc1_with_wrong_keystart = frf.createSignedDocument(doc1_with_wrong_keystart, signtools1);
    let signed_doc1_with_good_keystart = frf.createSignedDocument(doc1_with_good_keystart, signtools1);
    let signed_doc2 = frf.createSignedDocument(doc2, signtools1);
    let signed_doc3 = frf.createSignedDocument(doc3, signtools2);
    let signed_doc4 = frf.createSignedDocument(doc4, signtools2);
    let signed_doc5 = frf.createSignedDocument(doc5, signtools3);
    let signed_doc6 = frf.createSignedDocument(doc6, signtools5, false, doc5.key);


    it("should successfully check the sky root document", function(){
    	let isValid = frf.checkDocument(signed_skyRootDocument);
    	assert.ok(isValid);
	})

	it("should fail if the root document has no signTools", function(){
		let doc = Object.assign({}, skyRootDocument);
		delete doc.signtools;
		let signedDoc = frf.createSignedDocument(doc, signtools1);
        let isValid = frf.checkDocument(signedDoc);
        assert.notOk(isValid);
	})

    it("should check document with skyroot as direct parent", function(){
        let parents = [signed_skyRootDocument];
        let isValid = frf.checkDocument(signed_doc1, parents);
        assert.ok(isValid);
    })


    it("should check document with skyroot as direct parent and correct keystart provided", function(){
        let parents = [signed_skyRootDocument];
        let isValid = frf.checkDocument(signed_doc1_with_good_keystart, parents);
        assert.ok(isValid);
    })


    it("should fail to check document with skyroot as direct parent, with an incorrect keystart provided", function(){
        let parents = [signed_skyRootDocument];
        let isValid = frf.checkDocument(signed_doc1_with_wrong_keystart, parents);
        assert.notOk(isValid);
    })

    it("should check document with skyroot as direct parent with correct start", function(){
        let parents = [signed_skyRootDocument];
        let isValid = frf.checkDocument(signed_doc1, parents);
        assert.ok(isValid);
    })

    it("should fail if direct parent is not provided", function(){
        let parents = [];
        let isValid = frf.checkDocument(signed_doc1, parents);
        assert.notOk(isValid);
    })

    it("should check document with several parents", function(){
        let parents = [signed_skyRootDocument,signed_doc1];
        let isValid = frf.checkDocument(signed_doc2, parents);
        assert.ok(isValid);
    })

    it("should check document belonging in a subsky", function(){
        let parents = [signed_skyRootDocument,signed_doc1,signed_doc2];
        let isValid = frf.checkDocument(signed_doc3, parents);
        assert.ok(isValid);
    })

    it("should check document belonging in a subsky of a subsky", function(){
        let parents = [signed_skyRootDocument,signed_doc1,signed_doc2,signed_doc3, signed_doc4];
        let isValid = frf.checkDocument(signed_doc5, parents);
        assert.ok(isValid);
    })

    it("should check document with a sha256 key used for signature", function(){
        let parents = [signed_skyRootDocument,signed_doc1,signed_doc2,signed_doc3, signed_doc4, signed_doc5];
        let isValid = frf.checkDocument(signed_doc6, parents);
        assert.ok(isValid);
    })

    it("should fail if an intermediate document is not provided", function(){
        let parents = [signed_skyRootDocument,signed_doc1,signed_doc3, signed_doc4];
        let isValid = frf.checkDocument(signed_doc5, parents);
        assert.notOk(isValid);
    })

	it("should fail to recognize a skyroot document if the key is not the hash of the signature", function(){
        let frf = new Frf();
        // build several signtools
        // build several documents
        let skyRootDocument = frf.createSky({
            body : new Uint8Array([1,2,4,8,16,32,64,128]),
        },signtools1)
        let signed_skyRootDocument = frf.createSignedDocument(skyRootDocument, signtools4, true);
        let skyRootKey = signed_skyRootDocument.key;
        let wrongKey = "aa".repeat(32);
        signed_skyRootDocument.key = wrongKey;
        let isValid = frf.checkDocument(signed_skyRootDocument, []);
        assert.notOk(isValid);
	})

})

