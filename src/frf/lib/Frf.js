/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/


/*** helper class for flock resources format (frf)
 * it converts data to/from bytes Uint8Array according to frf specifications
 * 
 * 
 * ***/

import {Flaptools} from "../../flaptools/lib/Flaptools.js"
import {Secswans} from "../../secswans/lib/secswans.js"

class Frf{
	constructor(data={}){
		// retrieve dependencies
		this._flaptools = data.flaptools || new Flaptools();
        this._secswans = data.secswans || new Secswans();

		// set the list of authorized signing algorithm
		this.authorizedSignatureTypes = data.authorizedSignatureTypes ||
			[ this._secswans.ENCTYPE_RSA_1024 , this._secswans.ENCTYPE_RSA_2048, this._secswans.ENCTYPE_RSA_4096 ];
		// initialize constants
		this._initConstants();
		this._initDictionnaries();

        this.signatureType = data.signatureType || this.DEFAULT_SIGNTOOLS_TYPE;
	}

    ////////////////////////////////////////////////////////////////////
    /////////////        SIGNATURE METHODS            //////////////////
    ////////////////////////////////////////////////////////////////////

    /***
	 * generate a new keypair
     */
    createKeys(type = this.signatureType){
		return this._secswans.generateKey(type);
	}

    /***
	 * build the signature of a given bytes array
     */
    computeSignature(bytes, keys){
    	let privkey = keys.private || keys;
        return this._secswans.sign(bytes, privkey);
	}

    /**
	 * create a sky by pushing a signtools data in a document
     */
	createSky(doc, keys){
		// allow both a full key or the public key data
		let pubkeydata = keys.public || keys;
		doc.signtools = pubkeydata;
		return doc;
	}

    /***
	 * create a new signed document whose body is the input document
     */
	createSignedDocument(doc, keys, skyroot=false, keystart = ""){
        let keyType = (keys.private && keys.private.type) || (keys.public && keys.public.type);
        let hashedKey = skyroot || keyType === this._secswans.HASHTYPE_SHA_256 || keyType === this._secswans.HASHTYPE_SHA_512;
        if(hashedKey && keystart){
        	doc.keystart = keystart;
		}
		let bytes = doc instanceof Uint8Array ? doc : this.documentToBytes(doc);
		let signature = this.computeSignature(bytes, keys);
		let signedDocument = {
			body : bytes,
			signature : signature
		}

		if(hashedKey){
			signedDocument.key = keystart+ this._secswans._hashAndPad( this._flaptools.hexToBytes(signature), "sha256", 2*this.SKY_KEY_LENGTH);
		}
		return signedDocument;
	}

    /**
	 * check the signature of a single document
     */
    checkSignature(signedDocument, signtools=null){
		if(!signedDocument.hasOwnProperty("signature")) return false;
		let providedSignature = signedDocument.signature;
		if(!signtools){
			let document = this.documentToObject(signedDocument.body);
			signtools = document.signtools;
		}
		if(!signtools || typeof signtools != "object") return false;

		// let realSignature = ;

	}

    /**
	 * ensure the validity of a document, and check the signature of the parents recursively
	 * needs every parents to check recursively
     */
	checkDocument(document, signedParents=[]){
		let parents = {};

		// extract data about the document to check and about every parent
		for(let parent of signedParents){
			let pdata = this.extractDataFromSigned(parent);
			parents[ pdata.key ] = pdata;
		}
		let docdata = this.extractDataFromSigned(document);
        parents[ docdata.key ] = docdata;

		// build the array of parent in the right order
		let key = docdata.key;
		let orderedParents = [];
		do{
			if(!parents.hasOwnProperty(key)) return false;
			let parent = parents[key];
			orderedParents.push(parent);
			if(this.isSKyRoot(key)) break;
			// if a parent is not the sky root and has no prefix, the validity can not be checked up to the root
			if(!parent.prefix) return false;
			key = key.slice(0, 2*parent.prefix) // *2 becase hexadecimal
		}
		while(true);

		let signtools = null;
		while(orderedParents.length){
			let doc = orderedParents.pop();

			// for the sky root
			if(!signtools){
                // the sky root must provide for a signtools
				if(!doc.signtools){
                    return false;
				}
				else signtools = doc.signtools;
			}

			// check either the signature if provided, or take the key if not provided
			let providedSignature = doc.signature || doc.key;
			if(!providedSignature) return false;
			let isCorrectlySigned = this._secswans.checkSign( doc.signedBytes, signtools, providedSignature );
			if(!isCorrectlySigned){
                return false;
			}

			// if keystart is provided in the document, check that the keystart match with the key provided
			if(doc.keystart && !doc.key.startsWith(doc.keystart)){
				return false;
			}

			// for sky roots, the key must be the hash of the signature
			if(this.isSKyRoot(doc.key)){
				// compute the hash
				let hashedSignature = this._secswans._hashAndPad( this._flaptools.hexToBytes(providedSignature), "sha256", 2*this.SKY_KEY_LENGTH) ;
				if(doc.key !== hashedSignature){
					return false;
                }
			}
			// if a new signtools is provided, it defines a new subsy
			if(doc.signtools) signtools = doc.signtools;
		}

		return true;
	}

	// check if a key can be consider as a sky root
	// for the moment, it just needs to be a 32 byte hexadecimal
	isSKyRoot( key ){
		if(key instanceof Uint8Array) key = this._flaptools.bytesToHex(key);
		if(typeof key !== "string") return false;
		return key.length === 2*this.SKY_KEY_LENGTH;
	}

	// aux method to extract key, signature, signtools, prefix and body bytes from a document or a signed document
    extractDataFromSigned(outerDocument){
        let result = { }

        // extract the bytes on which signature must be checked
		result.signedBytes = outerDocument.body || new Uint8Array();

        // it is expected to be a signed document containing a document
        let innerDocument = outerDocument.body ? this.documentToObject(outerDocument.body) : {};
        result.signature = outerDocument.signature;
        result.key = outerDocument.key || innerDocument.key || null;
        result.signtools = outerDocument.signtools || innerDocument.signtools;

        if(innerDocument.signtools) result.signtools = innerDocument.signtools;
        else if(outerDocument.signtools) result.signtools = outerDocument.signtools

        if(innerDocument.hasOwnProperty("prefix")) result.prefix = innerDocument.prefix;
        else if(outerDocument.hasOwnProperty("prefix")) result.prefix = outerDocument.prefix;

        // retrieve other data in the innerDocument
		if(innerDocument){
			for(let i in innerDocument){
				if(result.hasOwnProperty(i)) continue;
				result[i] = innerDocument[i];
			}
		}

        result.body = innerDocument.body || outerDocument.body;


        return result;
    }

	////////////////////////////////////////////////////////////////////
	/////////////        CONVERSION METHODS            /////////////////
	////////////////////////////////////////////////////////////////////
	
	// transform a signature Uint8Array data into its object representation
	signtoolsToObject(o){
		return this._flaptools.decode(o, this.TEMPLATE_SIGNTOOLS);
	}
	
	// transform a signature onject data into its byte representation
	signtoolsToBytes(b){
		return this._flaptools.encode(b, this.TEMPLATE_SIGNTOOLS);
	}
	
	// transform a document Uint8Array data into its object representation
	documentToObject(o){
		return this._flaptools.decode(o, this.TEMPLATE_DOCUMENT);
	}
	
	// transform a document onject data into its byte representation
	documentToBytes(b){
		return this._flaptools.encode(b, this.TEMPLATE_DOCUMENT);
	}
	
	////////////////////////////////////////////////////////////////////
	////////////////////////   HELPERS     /////////////////////////////
	////////////////////////////////////////////////////////////////////

	_initConstants(){
    	this.DEFAULT_SIGNTOOLS_TYPE = this._secswans.ENCTYPE_RSA_1024;
        this.SKY_KEY_LENGTH = 32;
	}
	
	// initialize dictionnaries and EE used by frf
	_initDictionnaries(){
		var a2o = (a)=>{
			let r={},e;
			for(e of a) r[e.n]=e;
			return r;
		}
		
		// initialize the signature dictionnary
		let EE_SIGNATURE = [
			{n:"version", t:"int", d:0, c:0},
			{n:"length", t:"int", c:1},
			{n:"type", t:"int", c:2,
				rpl : {
					1 : "rsa1024",
					2 : "rsa2048",
                    3 : "rsa4096",
                    4 : "sha256",
                    // 5 : "sha512",
				}
			},
			{n:"n", t:"hex", c:16},
			{n:"e", t:"hex", c:17},
			{n:"d", t:"hex", c:18},
		]
		this.TEMPLATE_SIGNTOOLS = {
			ee : a2o(EE_SIGNATURE),
		};
		
		// initialize the document dictionnary
		let EE_DOCUMENT = [
			{n:"version", t:"int", d:0, c:0},
			{n:"length", t:"int", c:1},
			{n:"type", t:"int", c:2,
				rpl : {
					0 : "text",
					1 : "multipart",
					2 : "message",
					3 : "image",
					4 : "audio",
					5 : "video",
					6 : "application",
					7 : "font",
					8 : "example",
					9 : "model",
					10 : "bytes",
					128 : "x-flock",
				}
			},
            {n:"subtype", t:"ascii", c:3},
			{n:"name", t:"utf8", c:4, dd : ""},
			{n:"signtools", t:"dict", tpl : this.TEMPLATE_SIGNTOOLS, c:5},
			{n:"signature", t:"hex", c:6},
			{n:"expire", t:"int", c:7, dd : 0},
			{n:"dversion", t:"int", c:8, dd : 0},
			{n:"key", t:"hex", c:9, dd : ""},
			{n:"prefix", t:"int", c:10, dd : 0},
			{n:"wingspan", t:"hex", c:11},
			{n:"nextfeather", t:"hex", c:12},
            {n:"body", t:"bin", c:13},
			{n:"featherlength", t:"int", c:14, dd : 0},
            {n:"winglength", t:"int", c:15},
            {n:"author", t:"utf8", c:16},
            {n:"time", t:"int", c:17},
            {n:"keystart", t:"hex", c:18},
		];
		this.TEMPLATE_DOCUMENT = {
			ee : a2o(EE_DOCUMENT),
		};
	}


}

export {
    Frf
}