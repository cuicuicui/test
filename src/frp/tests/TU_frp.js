/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

/**
 * generic tests for frp : 
 * tests are performed on a given WarehouseClass that extends Warehouse
 * 
 * **/

let testData = {};

try{
	var chai = require("chai");
    // tested modules
    var flocklibs = require("../../../dist/flock-nodejs.js")
    Object.assign(global, flocklibs);

    testData.WarehouseClass = flocklibs.MDBWarehouse;
	
}
catch(e){
		console.log("err ::::",e)
}

var assert = chai.assert;

// helper to shift a function execution in time
let shiftCounter = 0;
const ASYNC_TIMEOUT = 300; // time for asynchronous test to occur
let shift = function(f){
	setTimeout(f, shiftCounter * ASYNC_TIMEOUT);
	shiftCounter++;
}

describe("testing the helpers", function(){
	let frp = new Frp();
	
	after(function(){
		frp.stopCron();
	});

	beforeEach(function(){
        shiftCounter = 1;
	})

	it("should create a tid", function(){
		let tid = frp.createTid(16);
		assert.isString("tid");
		assert.lengthOf(tid,32);
	});
	
	it("should race several promises, and return the first to resolve", function(done){
		let res1 = "res1";
		let res2 = "res2";
		let res3 = "res3";
		let res4 = "res4";
		let p1 = new Promise(res=>setTimeout(()=>res(res1), 200));
		let p2 = new Promise(res=>setTimeout(()=>res(res2), 150));
		let p3 = new Promise(res=>setTimeout(()=>res(res3), 100));
		let p4 = new Promise(res=>setTimeout(()=>res(res4), 300));

		let race = frp.race([p1, p2, p3, p4]);
		race.then(function(result){
			assert.equal(result, res3, "promise 3 should have resolved first");
			done();
		})
		.catch(function(){
			asert.ok(false, "the race should have succeeded");
			done();
		});
	});
	
	it("should race several promises, and return the last to reject", function(done){
		let res1 = "res1";
		let res2 = "res2";
		let res3 = "res3";
		let res4 = "res4";
		let p1 = new Promise((res,rej)=>setTimeout(()=>rej(res1), 100));
		let p2 = new Promise((res,rej)=>setTimeout(()=>rej(res2), 400));
		let p3 = new Promise((res,rej)=>setTimeout(()=>rej(res3), 300));
		let p4 = new Promise((res,rej)=>setTimeout(()=>rej(res4), 200));

		let race = frp.race([p1, p2, p3, p4]);
		race.then(function(result){
			asert.ok(false, "the race should have rejected");
			done();
		})
		.catch(function(result){
			assert.equal(result, res2, "promise 2 should have rejected last");
			done();
		});
	});

	it("should race several promises, and reject with a timeout", function(done){
        let res1 = "res1";
        let res2 = "res2";
        let res3 = "res3";
        let res4 = "res4";
        let p1 = new Promise(res=>setTimeout(()=>res(res1), 200));
        let p2 = new Promise(res=>setTimeout(()=>res(res2), 150));
        let p3 = new Promise(res=>setTimeout(()=>res(res3), 100));
        let p4 = new Promise(res=>setTimeout(()=>res(res4), 300));

        let race = frp.race([p1, p2, p3, p4], {timeout : 5});
        race.then(function(result){
            assert.notOk(true, "the promise should have been rejected due to timeout")
            done();
        })
		.catch(function(){
			assert.ok(true, "the race should have been rejected");
			done();
		});
	})

	it("should test the busy methods", function(done){
		let busyNid;

		let now = Date.now();
		assert.deepEqual(frp.busyNodes(), {});

		// push several nodes
		let nid1 = "00";
		let nid2 = "01";
		let nid3 = "02";

        frp.isBusy(nid1, now - 5);
        frp.isBusy(nid2, now + Math.floor(ASYNC_TIMEOUT/2));
        frp.isBusy(nid3, now + 3* ASYNC_TIMEOUT);

        assert.deepEqual(frp.busyNodes(), {[nid2] : true, [nid3] : true});
        assert.notOk( frp.isBusy(nid1))
        assert.ok( frp.isBusy(nid2))
        assert.ok( frp.isBusy(nid3))

		shift(function(){
            assert.deepEqual(frp.busyNodes(), {[nid3] : true});
            assert.notOk( frp.isBusy(nid1))
            assert.notOk( frp.isBusy(nid2))
            assert.ok( frp.isBusy(nid3))
            done()
		})
	})

	it("should build keys with buildKeys method from a list of suffixes", function(){
		let prefix, suffixes, expected, result;
		
		// test with an undefined suffixes array
		prefix = "aabbccdd001122";
		result = frp.buildKeys(prefix);
		assert.deepEqual(result, ["aabbccdd001122"]);
		
		// test with an empty array
		prefix = "aabbccdd001122";
		suffixes = [];
		result = frp.buildKeys(prefix, suffixes);
		assert.deepEqual(result, []);
		
		// test with several suffixes
		prefix = "aabbccdd001122";
		suffixes = ["aa","123456789","b"];
		result = frp.buildKeys(prefix, suffixes);
		assert.deepEqual(result, ["aabbccdd001122aa", "aabbccdd0011220123456789", "aabbccdd0011220b"]);
		
		// test with an empty prefix
		prefix = "";
		suffixes = ["aa","123456789","b"];
		result = frp.buildKeys(prefix, suffixes);
		assert.deepEqual(result, ["aa","0123456789","0b"]);
	});

	it("should filter keys and factorize them with filterAndFactorize", function(){
		let keys, expected, result, prefixFactors;

		// try to factorize with no prefixFactors
		keys = [
			{ prefix : "aabbcc", dversion : 5}
		];
        prefixFactors = [];
        expected = [];
        result = frp.filterAndFactorize(keys, prefixFactors);
		assert.deepEqual(result, expected);

		// try with a single prefix
        keys = [
            { prefix : "aabbcc", dversion : 5},
            { prefix : "aabbccdd", dversion : 5},
            { prefix : "aabbcceeff002589", dversion : 6},
            { prefix : "aabbcc", dversion : 7},
            { prefix : "aabb", dversion : 8},
        ];
        prefixFactors = ["aabbcc"];
        expected = [
            {prefix : "aabbcc", dversion : "5", suffixes : ["", "dd"]},
            {prefix : "aabbcc", dversion : "6", suffixes : ["eeff002589"]},
            {prefix : "aabbcc", dversion : "7", suffixes : [""]},
		];
        result = frp.filterAndFactorize(keys, prefixFactors);
        assert.deepEqual(result, expected);

        // try with several prefixes
        keys = [
            { prefix : "aabbcc", dversion : 5},
            { prefix : "aabbccdd", dversion : 5},
            { prefix : "11223344556677", dversion : 6},
            { prefix : "112233445566778899", dversion : 7},
            { prefix : "aabb", dversion : 8},
        ];
        prefixFactors = ["aabbcc", "11223344556677"];
        expected = [
            {prefix : "aabbcc", dversion : "5", suffixes : ["", "dd"]},
            {prefix : "11223344556677", dversion : "6", suffixes : [""]},
            {prefix : "11223344556677", dversion : "7", suffixes : ["8899"]},
        ];
        result = frp.filterAndFactorize(keys, prefixFactors);
        assert.deepEqual(result, expected);
	})
});

describe("testing translation methods", function(){
	let frp = new Frp();
	
	let document = {
		version : 55,
		type : "application",
		subtype : "octet-stream",
		name : "myfile.bin",
		signtools : {
			version : 0,
			type : "rsa4096",
			n : "aabcddee",
			e : "03",
			d : "1234567890abcdef"
		},
		signature : "aabbccddeeff102455",
		expire : 12345,
		dversion :124,
		key : "aabbccddeeff",
		featherlength : 33,
		prefix : 3,
		wingspan : "00112266",
		nextfeather : "11445566",
		body : new Uint8Array([44, 55, 66])
	}
	
	after(function(){
		frp.stopCron();
	});
	
	it("should encode/decode a request message", function(){
		let message, encoded, decoded;
		
		message = {
			version : 5,
			type : "request",
			tid : frp.createTid(),
			key : {
				prefix : "aabbccddee55",
				dversion : 25,
				span : 999
			}
		}
		encoded = frp.messageToBytes(message);
		decoded = frp.messageToObject(encoded);
		assert.deepEqual(decoded, message);
	});
	
	it("should encode/decode a answer message", function(){
		let message, encoded, decoded;
		
		message = {
			version : 5,
			type : "answer",
			tid : frp.createTid(),
			status : "ANSWERED",
			document : document
		}
		encoded = frp.messageToBytes(message);
		decoded = frp.messageToObject(encoded);
		assert.deepEqual( decoded, message);
	});
	
	
	it("should encode/decode a register message", function(){
		let message, encoded, decoded;
		
		message = {
			version : 5,
			type : "register",
			tid  : frp.createTid(),
			keys : [{
					prefix : "aabbccddee55",
					dversion : 25,
					span : 12
				},
				{
					prefix : "eeffaabb225588",
					dversion : 233,
					suffixes : ["aa","bbccddee","1234567890"]
				}
			],
			retro : true
		}
		encoded = frp.messageToBytes(message);
		decoded = frp.messageToObject(encoded);
		assert.deepEqual(decoded, message);
	});
	
	it("should encode/decode a notify message", function(){
		let message, encoded, decoded;
		
		message = {
			version : 5,
			type : "notify",
			tid : frp.createTid(),
			keys : [{
					prefix : "aabbccddee55",
					dversion : 25,
					span : 12
				},
				{
					prefix : "eeffaabb225588",
					dversion : 233,
					suffixes : ["aa","bbccddee","1234567890"]
				}
			]
		}
		encoded = frp.messageToBytes(message);
		decoded = frp.messageToObject(encoded);
		assert.deepEqual(decoded, message);
	});
	
	it("should encode/decode a busy message", function(){
		let message, encoded, decoded;
		
		message = {
			version : 5,
			type : "busy",
            time : 123456,
			tid : frp.createTid()
		}
		encoded = frp.messageToBytes(message);
		decoded = frp.messageToObject(encoded);
		assert.deepEqual(decoded, message);
	});
	
	it("should encode/decode a cancel message", function(){
		let message, encoded, decoded;
		message = {
			version : 5,
			type : "cancel",
			tid : frp.createTid()
		}
		encoded = frp.messageToBytes(message);
		decoded = frp.messageToObject(encoded);
		assert.deepEqual(decoded, message);
	});
});

describe("testing some register/notify helpers", function(){
	let frp;
	
	beforeEach(function(){
		frp = new Frp();
	});
	
	it("should test the isKeyRegistered method with various registration objects", function(){
		let key, registration, expected, result;
		
		// test with an empty registration
		key = {
			prefix : "aabbccddee",
			span : 0
		}
		registration = {
			tid : "11",
			keys : [],
		};
		expected = false;
		result = frp.isKeyRegistered(key, registration);
		assert.deepEqual(result, expected);
		
		// test with a strictly included key
		key = {
			prefix : "aabbccddee",
			span : 0
		}
		registration = {
			tid : "22",
			keys : [{prefix : "aabbcc", span : 10}],
		};
		expected = true;
		result = frp.isKeyRegistered(key, registration);
		assert.deepEqual(result, expected);
		
		// test with a key included in registration suffixes
		key = {
			prefix : "aabbccddee",
			span : 0
		}
		registration = {
			tid : "33",
			keys : [{prefix : "aabbcc", suffixes : ["ddee", "ddff"] }]
		};
		expected = true;
		result = frp.isKeyRegistered(key, registration);
		assert.deepEqual(result, expected);
		
		// test with a key included with an empty suffix and a matching prefix
		key = {
			prefix : "aabbccddee",
			span : 0
		}
		registration = {
			tid : "44",
			keys : [{prefix : "aabbccddee", suffixes : ["ddee", "ddff", ""] }]
		};
		expected = true;
		result = frp.isKeyRegistered(key, registration);
		assert.deepEqual(result, expected);
		
		// test with an included key, with matching prefix and no suffix
		key = {
			prefix : "aabbccddee",
			span : 0
		}
		registration =  {
			tid : "55",
			keys : [{prefix : "aabbccddee", suffixes : [] }]
		};
		expected = true;
		result = frp.isKeyRegistered(key, registration);
		assert.deepEqual(result, expected);
		
		// test with almost matching prefix but insufficient suffix and span
		key = {
			prefix : "aabbccddee",
			span : 0
		}
		registration =  {
			tid : "77",
			keys : [{prefix : "aabbcc", suffixes : ["aa", "dd", ""], span : 0 }]
		};
		expected = false;
		result = frp.isKeyRegistered(key, registration);
		assert.deepEqual(result, expected);
	});
	
	it("it should filter among the registration that are triggered by a given key with triggeredRegistrations method", function(){
		let key, registrations, result, expected;
		let prefix = "aabbccddee";
		let r1, r2, r3, r4, r5, r6, r7;
		
		
		// build the registrations object
		r1 = {
			tid : "11",
			keys : [],
		}
		r2 = {
			tid : "22",
			keys : [{prefix : "aabbcc", span : 10}],
		}
		r3 = {
			tid : "33",
			keys : [{prefix : "aabbcc", suffixes : ["ddee", "ddff"] }]
		}
		r4 = {
			tid : "44",
			keys : [{prefix : "aabbccddee", suffixes : ["ddee", "ddff", ""] }]
		}
		r5 = {
			tid : "55",
			keys : [{prefix : "aabbccddee", suffixes : [] }]
		}
		r6 = {
			tid : "66",
			keys : [{prefix : "aabbcc", suffixes : ["aa", "dd", ""], span : 5 }]
		}
		r7 = {
			tid : "77",
			keys : [{prefix : "aabbcc", suffixes : ["aa", "dd", ""], span : 0 }]
		}
		
		
		// test with an empty registrations array
		key = {
			prefix : prefix,
			span : 0
		}
		registrations = [];
		expected = [];
		result = frp.triggeredRegistrations(key, registrations);
		assert.deepEqual(result, expected);
		
		// test with one triggered registration
		registrations = [r2];
		expected = [ r2];
		result = frp.triggeredRegistrations(key, registrations);
		assert.deepEqual(result, expected);
		
		// test with the registrations
		registrations = [r1, r2, r3, r4, r5, r6, r7];
		expected = [ r2, r3, r4, r5, r6];
		result = frp.triggeredRegistrations(key, registrations);
		assert.deepEqual(result, expected);
	});

});



describe("testing warehouse specific methods", function(){
	this.timeout(5000);
	let frp;
	
	beforeEach(function(){
		frp = new Frp();
	});
	
	afterEach(function(){
		frp.stopCron();
	});
	
	it("should add, get and remove warehouses", function(){
		let result, expected;
		let w1 = new testData.WarehouseClass();
		let w2 = new testData.WarehouseClass();
		
		result = frp.getWarehouses();
		expected = [];
		assert.deepEqual(result, expected, "at first, frp contains no warehouses");
		
		frp.addWarehouse(w1);
		result = frp.getWarehouses();
		expected = [w1];
		assert.deepEqual(result, expected);
		
		frp.addWarehouse(w2);
		result = frp.getWarehouses();
		expected = [w1,w2];
		assert.deepEqual(result, expected);
		
		frp.removeWarehouse(w1);
		result = frp.getWarehouses();
		expected = [w2];
		assert.deepEqual(result, expected);
	});
});

describe("testing END USER API", function(){
	let frp;
	
	beforeEach(function(){
		frp = new Frp();
	});
	
	afterEach(function(){
		frp.stopCron();
	});
	it("tests the hasResourceLocal method", function(done){
		let result, expected;
		let w1 = new testData.WarehouseClass();
		let w2 = new testData.WarehouseClass();
		frp.addWarehouse(w1);
		frp.addWarehouse(w2);
		
		// add one resource to each warehouse
		let key1 = "aabbccddee";
		let key2 = "0011223344";
		let key3 = "9988776655";
		let resource1 = new Uint8Array([1,2,3]);
		let resource2 = new Uint8Array([4,5,6]);
		let resource3 = new Uint8Array([7,8,9]);
		
		let p1 = w1.storeResource(key1,resource1);
		let p2 = w2.storeResource(key2,resource2);
		Promise.all([p1,p2]).then(function(){
			// try to retreive the first key
			return frp.hasResourceLocal(key1);
		})
		.then(function(result){
			assert.deepEqual(result, true, "expect the key1 to be what was stored");
			// try to retreive the second key
			return frp.hasResourceLocal(key2)
		})
		.then(function(result){
			assert.deepEqual(result, true, "expect the key2 to be what was stored");
			// try to retreive the third key
			return frp.hasResourceLocal(key3);
		})
		.then(function(result){
			assert.deepEqual(result, false, "expect the key3 to be not found");
		})
		.then(function(){
			// remove the keys
			w1.deleteResource(key1);
			w2.deleteResource(key2);
		})
		.then(done)
		.catch(function(e){
			w1.deleteResource(key1);
			w2.deleteResource(key2);
			assert.ok(false, "an error has occured : "+e)
			done();
		})
	});

	it("should test the hasResourceLocal with pusing resources with pushResourceLocal, then testing their removal with deleteResourceLocal", function(done){
		let result, expected;
		let w1 = new testData.WarehouseClass();
		let w2 = new testData.WarehouseClass();
		frp.addWarehouse(w1);
		frp.addWarehouse(w2);
		
		// add one resource to each warehouse
		let key1 = "000001111220123";
		let resource1 = new Uint8Array([1,2,3]);
		
		//let p1 = w1.storeResource(key1,resource1);
		//let p2 = w2.storeResource(key1,resource1);
		frp.hasResourceLocal(key1)
		.then(function(result){
			assert.deepEqual(result, false, "the key is not yet stored");
			// try to retreive the second key
			return frp.pushResourceLocal(key1, resource1)
		})
		.then(function(){
			return frp.hasResourceLocal(key1);
		})
		.then(function(result){
			assert.ok(result, "expect the key1 to be stored");
			// delete the key in the first warehouse
			return w1.deleteResource(key1);
		})
		.then(function(){
			return frp.hasResourceLocal(key1);
		})
		.then(function(result){
			assert.notOk(result, "expect the key1 to be deleted from any warehouse");
		})
		.then(done)
		.catch(function(e){
			// remove the keys in case of errors
			w1.deleteResource(key1);
			w2.deleteResource(key1);
			assert.ok(false,"en error occured : "+e)
			done();
		})
	});

	it("should test the getResourceKeysLocal, and its caching ability : ", function(done){
        let result, expected;
        let w1 = new testData.WarehouseClass();
        let w2 = new testData.WarehouseClass();
        frp.addWarehouse(w1);
        frp.addWarehouse(w2);

        // add one resource to each warehouse
        let key1 = "000001111220123";
        let resource1 = new Uint8Array([1,2,3]);

        // create a timer to check that the second execution of getResourceKeysLocal is extracted from cache
		let startTime;

        frp.pushResourceLocal(key1, resource1)
		.then(function(){
			return frp.getResourceKeysLocal();
		})
		.then(function(result){
			assert.isArray(result);
			assert.deepEqual(result.map(k=>k.prefix), [key1], "the array keys should contain the key of the pushed document : "+JSON.stringify(result));
			// launch the method again, mesuring its execution time
            startTime = Date.now();
            return frp.getResourceKeysLocal();
		})
		.then(function(result){
			let endTime = Date.now();
            assert.deepEqual(result.map(k=>k.prefix), [key1], "the array keys should contain the key of the pushed document");
            assert.isBelow(endTime-startTime, 3, "since the execution only return a cached promise, expect duration to be low");
		})
		.then(function(){
            w1.deleteResource(key1);
            w2.deleteResource(key1);
		})
		.then(done)
		.catch(function(e){
			console.log("ERROR !!!!!!!!!!! ",e)
			// remove the keys in case of errors
			w1.deleteResource(key1);
			w2.deleteResource(key1);
			assert.ok(false,"en error occured : "+e)
		})


	})

	it("tests the getResourceLocal method", function(){
		let result, expected;
		let w1 = new testData.WarehouseClass();
		let w2 = new testData.WarehouseClass();
		frp.addWarehouse(w1);
		frp.addWarehouse(w2);
		
		// add one resource to each warehouse
		let key1 = "aabbccddee";
		let key2 = "0011223344";
		let key3 = "9988776655";
		let resource1 = new Uint8Array([1,2,3]);
		let resource2 = new Uint8Array([4,5,6]);
		let resource3 = new Uint8Array([7,8,9]);
		
		let p1 = w1.storeResource(key1,resource1);
		let p2 = w2.storeResource(key2,resource2);
		return Promise.all([p1,p2]).then(function(){
			// try to retreive the first key
			return frp.getResourceLocal(key1).catch(function(){
				assert.ok(false, "the key1 should be present");
			});
		})
		.then(function(result){
			assert.deepEqual(result, resource1, "expect the key1 to be what was stored");
			// try to retreive the second key
			return frp.getResourceLocal(key2).catch(function(){
				assert.ok(false, "the key2 should be present");
			});
		})
		.then(function(result){
			assert.deepEqual(result, resource2, "expect the key2 to be what was stored");
			return frp.getResourceLocal(key3).then(function(result){
				assert.ok(false, "the key3 should have caused a rejection");
			})
			.catch(function(){
				assert.ok(true, "the key3 should not be present");
			});
		})
		.then(function(){
			// remove the keys
			w1.deleteResource(key1);
			w2.deleteResource(key2);
		})
	});

    it("should test the getResourceKeysLocal method", function(){
        let result, expected;
        let w1 = new testData.WarehouseClass();
        frp.addWarehouse(w1);

        // add one resource to each warehouse
        let rootKey = "aabbcc";
        let key1 = "aabbccdd";
        let key2 = "aabbccee";
        let key3 = "aabbedcd";
        let resource1 = new Uint8Array([1,2,3]);
        let resource2 = new Uint8Array([4,5,6]);
        let resource3 = new Uint8Array([7,8,9]);
        let expectedResult = [
            { prefix : key1, dversion : 0},
            { prefix : key2, dversion : 0}
        ]
        let p1 = w1.storeResource(key1,resource1);
        let p2 = w1.storeResource(key2,resource2);
        let p3 = w1.storeResource(key3,resource3);
        let deleteCallback = function(){
            // remove the keys
            w1.deleteResource(key1);
            w1.deleteResource(key2);
            w1.deleteResource(key3);
        };
        return Promise.all([p1, p2, p3]).then(function(){
            // try to retreive the first key
            return frp.getResourceKeysLocal(rootKey).catch(function(){
                assert.ok(false, "the operation should not fail");
            });
        })
		.then(function(result){
			assert.deepEqual(result, expectedResult);
			// try to retreive the second key
			return frp.getResourceLocal(key2).catch(function(){
				assert.ok(false, "the key2 should be present");
			});
		})
		.then(deleteCallback).catch(deleteCallback)
    })

    it("should store a file in one piece with cutAndStore method", function(done){
        let result, expected;
        let w1 = new testData.WarehouseClass({database: "testdb"});
        frp.addWarehouse(w1);

        let fileBytes = new Uint8Array(50);
        for(let i=0; i<fileBytes.length; i++){
        	fileBytes[i] = i+5;
		}
		frp.cutAndStore(fileBytes).then(function(signedDocument){
			assert.isNotNull(signedDocument);
            assert.isNotNull(signedDocument.body);
            assert.isString(signedDocument.key);
            let doc = frp._frf.documentToObject(signedDocument.body);
            assert.isNotNull(doc.body);
            assert.deepEqual(doc.body, fileBytes)
		})
		.then(done)
		.catch(done)
    })


    it("should store a big file in one piece with cutAndStore method", function(done){
        let result, expected;
        let w1 = new testData.WarehouseClass({database: "testdb"});
        frp.addWarehouse(w1);

        let fileBytes = new Uint8Array(1000000);
        for(let i=0; i<fileBytes.length; i++){
            fileBytes[i] = (i+5)%256;
        }
        frp.cutAndStore(fileBytes).then(function(signedDocument){
            assert.isNotNull(signedDocument);
            assert.isNotNull(signedDocument.body);
            assert.isString(signedDocument.key);
            let doc = frp._frf.documentToObject(signedDocument.body);
            assert.isNotNull(doc.body);
            assert.deepEqual(doc.body, fileBytes)
        })
            .then(done)
            .catch(done)
    })


    it("should store a file in several pieces with cutAndStore method", function(done){
        this.timeout(6000);
        let result, expected;
        let w1 = new testData.WarehouseClass({database: "testdb"});
        frp.addWarehouse(w1);

        let fileBytes = new Uint8Array(50);
        for(let i=0; i<fileBytes.length; i++){
            fileBytes[i] = i+5;
        }

        let retrievedParentDoc;
		let parentKey;

        frp.cutAndStore(fileBytes, {
        	chunkSize : 20
		}).then(function(signedDocument){
			retrievedParentDoc = signedDocument;
            parentKey = signedDocument.key;
            assert.isNotNull(signedDocument);
            assert.isNotNull(signedDocument.body);
            let doc = frp._frf.documentToObject(signedDocument.body);
            assert.isUndefined(doc.body);
            assert.equal(doc.wingspan, "02");
            assert.equal(doc.winglength, 50);
            assert.equal(doc.featherlength, 20);

			// try to retrieve the first feather
			return frp.getResourceLocal(parentKey + "00")
        })
		.then(function(firstFeatherSignedBytes){
			assert.isNotEmpty(firstFeatherSignedBytes);
			let firstFeather = frp._frf.documentToObject(firstFeatherSignedBytes);
			assert.isNotEmpty(firstFeather.body);
			let doc = frp._frf.documentToObject(firstFeather.body);
			assert.deepEqual(doc.key, parentKey + "00");
			assert.deepEqual(doc.body, fileBytes.slice(0,20) )
			assert.equal(doc.prefix, 32);
			// try to retrieve the second feather
			return frp.getResourceLocal(parentKey + "01");
		})
		.then(function(firstFeatherSignedBytes){
			assert.isNotEmpty(firstFeatherSignedBytes);
			let firstFeather = frp._frf.documentToObject(firstFeatherSignedBytes);
			assert.isNotEmpty(firstFeather.body);
			let doc = frp._frf.documentToObject(firstFeather.body);
			assert.deepEqual(doc.key, parentKey + "01");
			assert.deepEqual(doc.body, fileBytes.slice(20,40) )
			assert.equal(doc.prefix, 32);
			// try to retrieve the third feather
			return frp.getResourceLocal(parentKey + "02");
		})
		.then(function(firstFeatherSignedBytes){
			assert.isNotEmpty(firstFeatherSignedBytes);
			let firstFeather = frp._frf.documentToObject(firstFeatherSignedBytes);
			assert.isNotEmpty(firstFeather.body);
			let doc = frp._frf.documentToObject(firstFeather.body);
			assert.deepEqual(doc.key, parentKey + "02");
			assert.deepEqual(doc.body, fileBytes.slice(40) )
			assert.equal(doc.prefix, 32);
			// try to retrieve a fourth inexisting feather
			return frp.hasResourceLocal(parentKey + "03");
		})
		.then(function(hasResource){
			assert.notOk(hasResource)
		})
		.then(done)
		.catch(done)
    })
});

describe("testing the frp transactions", function(){
	let frp1, frp2, flock1, flock2;
	let warehouse1, warehouse2;
	let node1, node2;
	let socketPort = 4000;
	let document, documentBytes, documentKey;
	let document2, documentBytes2;
	let wrongDocumentKey = "99999999988";
	let wrongDocumentVersion = "123456789";
	
	beforeEach(function(){
		// reset asynchronous execution 
		shiftCounter = 1;
		
		// create the two flocks
		flock1 = new Flock();
		flock2 = new Flock();
		frp1 = new Frp({flock : flock1});
		frp2 = new Frp({flock : flock2});
		warehouse1 = new testData.WarehouseClass();
		warehouse2 = new testData.WarehouseClass();
		frp1.addWarehouse(warehouse1);
		frp2.addWarehouse(warehouse2);

		let websocketInterface, port, address;
		
		// build the first node
		port = socketPort++;
		address = "ws://127.0.0.1:"+port;
		websocketInterface = new NIWebsocketSpawner({ socketPort : port, address : address});
		node1 = {
			id : flock1.getId(),
			keys : flock1.getKeys(),
			canals : [{type : 2, address : address }]
		};
		flock1.metaInterface.addSpawner(websocketInterface);
		
		// build the second node
		port = socketPort++;
		address = "ws://127.0.0.1:"+port;
		websocketInterface = new NIWebsocketSpawner({ socketPort : port, address : address});
		node2 = {
			id : flock2.getId(),
			keys : flock2.getKeys(),
			canals : [{type : 2, address : address }]
		};
		flock2.metaInterface.addSpawner(websocketInterface);
		
		// build two documents for testing with same prefix and different version
		documentKey = "00a1a2a3a4a5b6c7d8e9f0";
		document = {
			version : 55,
			type : "application",
			subtype : "octet-stream",
			name : "myfile.bin",
			signtools : {
				version : 0,
				type : "rsa4096",
				n : "aabcddee",
				e : "03",
				d : "1234567890abcdef"
			},
			signature : "aabbccddeeff102455",
			expire : 12345,
			dversion :124,
			key : documentKey,
			prefix : 3,
			wingspan : "00112266",
			nextfeather : "11445566",
			body : new Uint8Array([44, 55, 66])
		};
		documentBytes = frp1._frf.documentToBytes(document);
		
		document2 = {
			version : 55,
			type : "application",
			subtype : "octet-stream",
			name : "myfile2.bin",
			signtools : {
				version : 0,
				type : "rsa2048",
				n : "55668899",
				e : "03",
				d : "0123"
			},
			signature : "001122",
			expire : 12345,
			dversion :139,
			key : documentKey,
			prefix : 3,
			wingspan : "0011226622",
			nextfeather : "1144556622",
			body : new Uint8Array([22, 11, 0])
		};
		documentBytes2 = frp1._frf.documentToBytes(document2);
	});

	afterEach(function(){
		// delete the documents, even if they are no longer stored
        warehouse1.deleteResource(documentKey);
        warehouse2.deleteResource(documentKey);
		return new Promise(res=>setTimeout(res,1000))
	})

	it("should send request to a remote node and answer with a not_found status", function(done){
		let prefix = "abba0110233245678987";
		let data = {
			key : {
				prefix : wrongDocumentKey,
				span : 0
			}
		};
		assert.deepEqual( frp1.targetedNodes(), {}, "no nid should be targeted at first");

		let prom = frp1.nodeRequest(node2, data);
        assert.deepEqual( frp1.targetedNodes(), { [node2.id] : 1}, "one node must be targeted");
		prom.then(function(result){
			assert.equal(result.status, "NOT_FOUND");
            assert.deepEqual( frp1.targetedNodes(), {}, "no nid should be targeted when request is done");
			done();
		})
	});

	it("should send request to a remote node and answer with a document", function(done){
		let goodVersion = document.dversion + 10;
		let data = {
			key : {
				prefix : documentKey,
				span : 0
			}
		};
		// store the document for testing
		warehouse2.storeResource(documentKey,documentBytes).then(function(){
			return frp1.nodeRequest(node2, data);
		})
		.then(function(result){
			assert.equal(result.status, "ANSWERED");
			assert.deepEqual(result.document, document);
		})
		.then(function(){
			warehouse2.deleteResource(documentKey)
			done();
		})
		.catch(function(){
			warehouse2.deleteResource(documentKey);
			done();
		});
	});

	it("should send request to a remote node and answer with the right version of a document", function(done){
		let data = {
			key : {
				prefix : documentKey,
				dversion : document2.dversion
			}
		};
		// store the document for testing
		warehouse2.storeResource(documentKey,documentBytes, {dversion : document.dversion}).then(function(){
			return warehouse2.storeResource(documentKey,documentBytes2, {dversion : document2.dversion})
		})
		.then(function(){
			return frp1.nodeRequest(node2, { key : {prefix : documentKey, dversion : document2.dversion} });
		})
		.then(function(result){
			assert.equal(result.status, "ANSWERED");
			assert.deepEqual(result.document, document2);
			// try to retrieve an inexisting version
			return frp1.nodeRequest(node2, { key : {prefix : documentKey, dversion : wrongDocumentVersion} });
		})
		.then(function(result){
			assert.equal(result.status, "NOT_FOUND");
		})
		.then(function(){
			warehouse2.deleteResource(documentKey);
			done();
		})
		.catch(function(){
			warehouse2.deleteResource(documentKey);
			done();
		});
	});
	
	it("should start a registraton on a node, and send notification back when triggered with the right key", function(done){
			// build two nodes, the first receive registration, the second receives notifications send by the first one
			// done in beforeEach function
			
			let goodPrefix = "aabbccddeeff";
			let wrongPrefix = "00112233";
			let goodKeys = [{prefix:goodPrefix, span:123}];
			let wrongKeys = [{prefix : wrongPrefix, span : 123}];
			
			// register a given key
			let registeredKeys = [
				{ prefix : goodPrefix, span : 0}
			]
			let callbackDataReceived = null;
			// callback for notifications
			let registerCallback1 = function(cbdata){
				callbackDataReceived = cbdata;
			}
			let registerData = {
                onNotify : registerCallback1,
				keys : [{prefix : goodPrefix, span : 0}],
			}
			// at first, frp have no registrations
			frp1.nodeRegister(node2, registerData);
			
			shift(function(){
				// test the node1 has a local registration and that node2 has a foreign registration
				assert.lengthOf( Object.keys(frp1.localRegistrations), 1);
				assert.lengthOf( Object.keys(frp1.foreignRegistrations), 0);
				assert.lengthOf( Object.keys(frp2.localRegistrations), 0);
				assert.lengthOf( Object.keys(frp2.foreignRegistrations) ,1);
				
				// trigger the notification sending from the second frp
				frp2.triggerForeignNotify(goodKeys[0]);
			});
			
			shift(function(){
				// test the callback has been triggered
				assert.isNotNull(callbackDataReceived);
				assert.deepEqual(callbackDataReceived.keys, goodKeys);
				
				// reset the callback for another try with the wrong keys
				callbackDataReceived = null;
				frp2.triggerForeignNotify(wrongKeys[0]);
			});
			
			shift(function(){
				// test the callback has not been triggered
				assert.isNull(callbackDataReceived);
				done();
			});
	});
	
	it("should start a registraton on a node, send notifications then stop the registration from the client", function(done){
			// build two nodes, the first receive registration, the second receives notifications send by the first one
			// done in beforeEach function
			
			let goodPrefix = "aabbccddeeff";
			let goodKey = {prefix:goodPrefix, span:123};
			let goodKeys = [goodKey];
			
			// register a given key
			let registeredKeys = [
				{ prefix : goodPrefix, span : 0}
			]
			let callbackDataReceived = null;
			// callback for notifications
			let registerCallback1 = function(cbdata){
				callbackDataReceived = cbdata;
			}
			// callback for cancel
			let canceled = false;
			let cancelCallback = function(){ canceled = true;}
			let registerData = {
                onNotify : registerCallback1,
				keys : registeredKeys,
				onCancel : cancelCallback
			}
			// at first, frp have no registrations
			
			let registrationId = frp1.nodeRegister(node2, registerData);
			assert.isNotNull(registrationId);
			
			shift(function(){
				// test the node1 has a local registration and that node2 has a foreign registration
				assert.lengthOf( Object.keys(frp1.localRegistrations), 1);
				assert.lengthOf( Object.keys(frp1.foreignRegistrations), 0);
				assert.lengthOf( Object.keys(frp2.localRegistrations), 0);
				assert.lengthOf( Object.keys(frp2.foreignRegistrations) ,1);
				
				// trigger the notification sending from the second frp
				frp2.triggerForeignNotify(goodKeys[0]);
			});
			
			shift(function(){
				// test the callback has been triggered
				assert.isNotNull(callbackDataReceived);
				assert.deepEqual(callbackDataReceived.keys, goodKeys);
				
				// reset the callback for another try
				callbackDataReceived = null;
				
				// remove the registration from frp1
				frp1.cancelLocalRegistration(registrationId);
				
				// test registrations have been removed from frp1
				assert.lengthOf( Object.keys(frp1.localRegistrations), 0);
				assert.lengthOf( Object.keys(frp1.foreignRegistrations), 0);
			});
			
			shift(function(){
				// test registrations have been removed from frp2
				assert.lengthOf( Object.keys(frp2.localRegistrations), 0);
				assert.lengthOf( Object.keys(frp2.foreignRegistrations) ,0);

                // the cancel callback has been called
                assert.notOk(canceled, "the cancel callback is only called when the cancel was initiated by the server");

				// try to trigger a notification in vain
				frp2.triggerForeignNotify(goodKeys[0]);
			});
			
			shift(function(){
				// test the callback has not been triggered
				assert.isNull(callbackDataReceived);
				done();
			});
	});
	
	it("should start a registraton on a node, send notifications then stop the registration from the server node", function(done){
			// build two nodes, the first receive registration, the second receives notifications send by the first one
			// done in beforeEach function
			
			let goodPrefix = "aabbccddeeff";
			let goodKey = {prefix:goodPrefix, span:123};
			let goodKeys = [goodKey];
			
			// register a given key
			let registeredKeys = [
				{ prefix : goodPrefix, span : 0}
			]

			// callback for notifications
       		let callbackDataReceived = null;
			let registerCallback1 = function(cbdata){
				callbackDataReceived = cbdata;
			}

			// callback for cancel
			let canceled = false;
			let cancelCallback = function(){ canceled = true;}
			let registerData = {
                onNotify : registerCallback1,
				keys : registeredKeys,
				onCancel : cancelCallback
			}
			
			// at first, frp have no registrations
			let registrationId = frp1.nodeRegister(node2, registerData);
			assert.isNotNull(registrationId);
			
			shift(function(){
				// test the node1 has a local registration and that node2 has a foreign registration
				assert.lengthOf( Object.keys(frp1.localRegistrations), 1);
				assert.lengthOf( Object.keys(frp1.foreignRegistrations), 0);
				assert.lengthOf( Object.keys(frp2.localRegistrations), 0);
				assert.lengthOf( Object.keys(frp2.foreignRegistrations) ,1);

				// trigger the notification sending from the second frp
				frp2.triggerForeignNotify(goodKeys[0]);
			});
			
			shift(function(){
				// test the callback has been triggered
				assert.isNotNull(callbackDataReceived);
				assert.deepEqual(callbackDataReceived.keys, goodKeys);
				
				// reset the callback for another try
				callbackDataReceived = null;
				
				// remove the registration from frp1
				frp2.cancelForeignRegistration( registrationId );
				
				// test registrations have been removed from frp1
				assert.lengthOf( Object.keys(frp2.localRegistrations), 0);
				assert.lengthOf( Object.keys(frp2.foreignRegistrations), 0);
			});
			
			shift(function(){
				// test registrations have been removed from frp2
				assert.lengthOf( Object.keys(frp1.localRegistrations), 0);
				assert.lengthOf( Object.keys(frp1.foreignRegistrations) ,0);

				// cancel callback must hae been called
				assert.ok(canceled);

				// try to trigger a notification in vain
				frp2.triggerForeignNotify(goodKeys[0]);
			});
			
			shift(function(){
				// test the callback has not been triggered
				assert.isNull(callbackDataReceived);
				done();
			});
	});

    it("should register and send back a notification about documents already available", function(done){
        let goodKeys = [
            {prefix:documentKey, dversion : document.dversion},
            {prefix:documentKey, dversion : document2.dversion}
		];

        // save the two versions of the document
        warehouse2.storeResource(documentKey, documentBytes,  {dversion : document.dversion})
        warehouse2.storeResource(documentKey, documentBytes2, {dversion : document2.dversion})


        // register a given key
        let registeredKeys = [
            { prefix : documentKey, span : 0}
        ]
        let callbackDataReceived = null;
        // callback for notifications
        let registerCallback1 = function(cbdata){
            callbackDataReceived = cbdata;
        }
        let registerData = {
            onNotify : registerCallback1,
            keys : registeredKeys,
			retro : true // IMPORTANT : PERFORM A RETRO NOTIFICATION
        }

        shift(function(){
            frp1.nodeRegister(node2, registerData);
        });

        shift(function(){
            // test the callback has been triggered
            assert.isNotNull(callbackDataReceived);
            assert.deepEqual(callbackDataReceived.keys, goodKeys);
			done();
        });
    })

	it("should trigger a notification when storing a document in the warehouse associated to the frp", function(done){
		let goodKeys = [
			{prefix:documentKey, dversion : document.dversion},
		];

		// register a given key
		let registeredKeys = [
			{ prefix : documentKey, span : 0}
		]
		let callbackDataReceived = null;
		// callback for notifications
		let registerCallback1 = function(cbdata){
			callbackDataReceived = cbdata;
		}
		let registerData = {
			onNotify : registerCallback1,
			keys : registeredKeys,
			retro : true // IMPORTANT : PERFORM A RETRO NOTIFICATION
		}

		shift(function(){
			registrationId = frp1.nodeRegister(node2, registerData);
		});

		shift(function(){
            warehouse2.storeResource(documentKey, documentBytes,  {dversion : document.dversion})
		})

		shift(function(){
			// test the callback has been triggered
			assert.isNotNull(callbackDataReceived);
			assert.deepEqual(callbackDataReceived.keys, goodKeys);
			done();
		});
	})
});

describe("testing global operations of frp", function(){
	this.timeout(5000);

	let frp = {};
	let flock = {};
	let warehouse = {};
	let node = {};
	let nodeInterface = {};
	let document = {};
	let documentBytes = {};
	let document2 = {};
	let documentBytes2 = {};
	let localDocument = {};
	let localDocumentBytes = {};
	let localDocumentKey = {};
	let nodePort = {};
	let nodeSpan = 100;
	let documentSuffix = "aaee00115599";
	let socketPort = 5000;
	let nodePrefixLength = 6;
	let nodeNumber = Math.pow(2,nodePrefixLength);
	
	beforeEach(function(){
		// reset socket ports
        socketPort = 5000;

		// reset asynchronous execution 
		shiftCounter = 1;
		
		for(let i = 0; i<nodeNumber; i++){
			// create the node key
			let nodePrefix = new Uint8Array(nodePrefixLength);
			for(let j=0; j<nodePrefixLength; j++){
				nodePrefix[j] = i & (1 << j) ? 1 : 0;
			}
			let nodeKey = Array.from(nodePrefix).map(i=> i ? "01" : "00").join("");
			flock[nodeKey] = new Flock({keys : [{prefix : nodeKey, span:nodeSpan}]});
			frp[nodeKey] =  new Frp({flock : flock[nodeKey]});
			warehouse[nodeKey] = new testData.WarehouseClass({collection : "documents_"+nodeKey});
			frp[nodeKey].addWarehouse(warehouse[nodeKey]);
			
			// build the websocket interfaced and add it to the metaspawner
			nodePort[nodeKey] = socketPort++;
			nodeInterface[nodeKey] = new NIWebsocketSpawner({ socketPort : nodePort[nodeKey], address : "ws://127.0.0.1:"+nodePort[nodeKey]});
			flock[nodeKey].metaInterface.addSpawner(nodeInterface[nodeKey]);
			
			// build the node
			node[nodeKey] = {
				id : flock[nodeKey].getId(),
				keys : flock[nodeKey].getKeys(),
				canals : flock[nodeKey].getCanals()
			};
			
			// build a document in the span of the node
			document[nodeKey] = {
				version : 55,
				type : "application",
				subtype : "octet-stream",
				name : "myfile.bin",
				signtools : {
					version : 0,
					type : "rsa4096",
					n : "aabcddee",
					e : "03",
					d : "1234567890abcdef"
				},
				signature : "aabbccddeeff102455",
				expire : 12345,
				dversion :124,
                "featherlength": 365,
				key : nodeKey+documentSuffix,
				prefix : 3,
				wingspan : "00112266",
				nextfeather : "11445566",
				body : new Uint8Array([44, 55, 66])
			};
			documentBytes[nodeKey] = frp[nodeKey]._frf.documentToBytes(document[nodeKey]);
			// push the documents in the warehouse
			warehouse[nodeKey].storeResource(document[nodeKey].key ,documentBytes[nodeKey], {dversion : document[nodeKey].dversion});
			
			// save another version of the document
			document2[nodeKey] = {};
			for(let p in document[nodeKey]) document2[nodeKey][p]= document[nodeKey][p];
			document2[nodeKey].dversion = 42;
			documentBytes2[nodeKey] = frp[nodeKey]._frf.documentToBytes(document2[nodeKey]);
			warehouse[nodeKey].storeResource(document2[nodeKey].key ,documentBytes2[nodeKey], {dversion : document2[nodeKey].dversion});
			
			// store some data whose key doesn't match the node key span, but can still be accessed locally
			localDocumentKey[nodeKey] = "102413";
			localDocument[nodeKey] = {
				version : 55,
				type : "application",
				subtype : "octet-stream",
				name : "myfile.bin",
				signtools : {
					version : 0,
					type : "rsa4096",
					n : "aabcddee",
					e : "03",
					d : "1234567890abcdef"
				},
				signature : "aabbccddeeff102455",
				expire : 12345,
				dversion :124,
                "featherlength": 365,
				key : localDocumentKey[nodeKey],
				prefix : 3,
				wingspan : "22",
				nextfeather : "99",
				body : new Uint8Array([11,22,88,95,41])
			}
			localDocumentBytes[nodeKey] = frp[nodeKey]._frf.documentToBytes(localDocument[nodeKey]);
			warehouse[nodeKey].storeResource(localDocumentKey[nodeKey] ,localDocumentBytes[nodeKey], {dversion : localDocument[nodeKey].dversion});
		}
		
		// create connections between nodes : 
		for(let nodeKey in node){
			for(let j=0; j<nodePrefixLength; j++){
				let remoteNodeKey = nodeKey.substr(0, 2*j) + (nodeKey.substr(2*j,2) === "00" ? "01" : "00") + nodeKey.substr(2*j+2);
				flock[nodeKey].addNode(node[remoteNodeKey]);
			}
		}
		return new Promise(r=>setTimeout(r,1000));
	});
	
	afterEach(function(){

		for(let nodeKey in frp){
		    let $frp = frp[nodeKey];
            $frp.getResourceKeysLocal().then(function(localKeys) {
                for (let key of localKeys) {
                    warehouse[nodeKey].deleteResource(key.prefix);
                }
                frp[nodeKey].stop();
            });

			// delete the documents
            // warehouse[nodeKey].deleteResource(document[nodeKey].key);
            // warehouse[nodeKey].deleteResource(document[nodeKey].key + "01").catch(function(){});
			// warehouse[nodeKey].deleteResource(localDocument[nodeKey].key);
			// stop the flocks
			flock[nodeKey].stop();
		}

        for(let nodeKey in frp){
            // frp[nodeKey].stop();
        }

        return new Promise(r=>setTimeout(r,1000));
	});

	it("should reach the resource on a remote node accessed automatically given the resource key with getResource method", function(done){
		let clientFrpKey = "01".repeat(nodePrefixLength);
		let clientFrp = frp[clientFrpKey];
		let clientNode = node[clientFrpKey];
		let targetFrpKey = "00".repeat(nodePrefixLength);
		let targetFrp = frp[targetFrpKey];
		let targetDocumentKey = targetFrpKey + documentSuffix;
		let targetDocument = document[targetFrpKey];

        flock[clientFrpKey].addNode(node[targetFrpKey]);

		clientFrp.getResource( targetDocumentKey ).then(function(doc){
			assert.deepEqual(doc, targetDocument);
			done();
		})
		.catch(function(e){
			console.log("error caught in TU ::: ",e); 
			assert.ok(false, "the operation promise should not have been rejected");
			done();
		});
	})

	it("should reach a specific version of a document with getResource method", function(done){
		let clientFrpKey = "01".repeat(nodePrefixLength);
		let clientFrp = frp[clientFrpKey];
		let clientNode = node[clientFrpKey];
		let targetFrpKey = "00".repeat(nodePrefixLength);
		let targetFrp = frp[targetFrpKey];
		let targetDocumentKey = targetFrpKey + documentSuffix;
		let targetDocument = document2[targetFrpKey];
		let opData = {
			dversion : targetDocument.dversion
		}
		clientFrp.getResource( targetDocumentKey, opData).then(function(doc){
			assert.deepEqual(doc, targetDocument);
			done();
		})
		.catch(function(e){
			console.log("error caught in TU ::: ",e );
			assert.ok(false, "the operation promise should not have been rejected");
			done();
		});
	})
	
	it("should reach a local document with the getResource method", function(done){
		let clientFrpKey = "01".repeat(nodePrefixLength);
		let clientFrp = frp[clientFrpKey];
		let targetDocument = localDocument[clientFrpKey];
		let targetDocumentKey = targetDocument.key;
		let opData = {
			dversion : targetDocument.dversion
		}
		clientFrp.getResource( targetDocumentKey, opData).then(function(doc){
			assert.deepEqual(doc, targetDocument);
			done();
		})
		.catch(function(e){
			console.log("error caught in TU ::: ",e);
			assert.ok(false, "the operation promise should not have been rejected");
			done();
		});
	})
	
	it("should fail to reach an inexisting resource with getResource method", function(done){
		let clientFrpKey = "01".repeat(nodePrefixLength);
		let clientFrp = frp[clientFrpKey];
		let clientNode = node[clientFrpKey];
		let targetFrpKey = "00".repeat(nodePrefixLength);
		let targetFrp = frp[targetFrpKey];
		let wrongDocumentKey = targetFrpKey + "195165165195498641653464894886464";
		let targetDocument = document[targetFrpKey];
		let opData = {
			maxThreads : 12
		};
		clientFrp.getResource( wrongDocumentKey, opData).then(function(doc){
			assert.notOk(true, "the operation should not resolve")
			done();
		})
		.catch(function(e){
			assert.ok(true, "the operation promise should be rejected");
			done();
		});
	})

    it("should perform a register operation, and reach one node", function(done) {
        let opData;
        // create callback for notifications
        let notifyPrefixReceived = {};
        let onNotify = function (tr) {
            notifyPrefixReceived[tr.keys[0].prefix] = tr.keys[0].prefix;
        }
        let clientFrpKey = "01".repeat(nodePrefixLength);
        let clientFrp = frp[clientFrpKey];
        let targetFrpKey = "00".repeat(nodePrefixLength);
        let targetFrp = frp[targetFrpKey];
        let targetResourceKey = targetFrpKey + documentSuffix.substr(0, 2);
        let expectedNotifyKey = targetFrpKey + documentSuffix;

        opData = {
            onNotify: onNotify,
            maxNodes: 1,
            retro: true,
            span: 100,
        };

        // expect the registration to succeed
        shift(function () {
            clientFrp.register(targetResourceKey, opData);
        })

        shift(function () {
            // check registrations have been received on the two expected frp
            assert.lengthOf(Object.keys(targetFrp.localRegistrations), 0);
            assert.lengthOf(Object.keys(targetFrp.foreignRegistrations), 1);
            // expect the callback to have been called
            assert.deepEqual(notifyPrefixReceived, { [expectedNotifyKey]: expectedNotifyKey });
            // cancel the registration
            clientFrp.unregister(targetResourceKey);
        })

        shift(function () {
            // expect the unregister to have been effective
            assert.lengthOf(Object.keys(targetFrp.localRegistrations), 0);
            assert.lengthOf(Object.keys(targetFrp.foreignRegistrations), 0);
        })

        // redo the operation one more time
        shift(function () {
            notifyPrefixReceived = {};
            clientFrp.register(targetResourceKey, opData);
        })

        shift(function () {
            // check registrations have been received on the two expected frp
            assert.lengthOf(Object.keys(targetFrp.localRegistrations), 0);
            assert.lengthOf(Object.keys(targetFrp.foreignRegistrations), 1);
            // expect the callback to have been called
            assert.deepEqual(notifyPrefixReceived, { [expectedNotifyKey]: expectedNotifyKey });
            // cancel the registration
            clientFrp.unregister(targetResourceKey);
        })

        shift(function () {
            // expect the unregister to have been effective
            assert.lengthOf(Object.keys(targetFrp.localRegistrations), 0);
            assert.lengthOf(Object.keys(targetFrp.foreignRegistrations), 0);
        })

        shift(done)
    });

    it("should register a given key, unregister remotely, and register back to the node", function(done){
        let opData;
        // create callback for notifications
        let notifyPrefixReceived = {};
        let onNotify = function (tr) {
            notifyPrefixReceived[tr.keys[0].prefix] = tr.keys[0].prefix;
        }
        let clientFrpKey = "01".repeat(nodePrefixLength);
        let clientFrp = frp[clientFrpKey];
        let targetFrpKey = "00".repeat(nodePrefixLength);
        let targetFrp = frp[targetFrpKey];
        let targetResourceKey = targetFrpKey + documentSuffix.substr(0, 2);
        let expectedNotifyKey = targetFrpKey + documentSuffix;

        opData = {
            onNotify: onNotify,
            maxNodes: 1,
            retro: true,
            span: 100,
            transitionTime: 0,
            populateTimeout : Math.floor(ASYNC_TIMEOUT/2)
        };

        // expect the registration to succeed
        shift(function () {
            clientFrp.register(targetResourceKey, opData);
        })

        shift(function () {
            // check registrations have been received on the two expected frp
            assert.lengthOf(Object.keys(targetFrp.localRegistrations), 0);
            assert.lengthOf(Object.keys(targetFrp.foreignRegistrations), 1);
            // expect the callback to have been called
            assert.deepEqual(notifyPrefixReceived, { [expectedNotifyKey]: expectedNotifyKey });
            // cancel the registration remotely
            targetFrp.cancelForeignRegistration( Object.keys(targetFrp.foreignRegistrations)[0] );
            assert.lengthOf(Object.keys(targetFrp.localRegistrations), 0);
            assert.lengthOf(Object.keys(targetFrp.foreignRegistrations), 0);
            notifyPrefixReceived = {};
        })

        shift(function(){})

        shift(function () {
            // another attempt of registration should have taken place
            assert.lengthOf(Object.keys(targetFrp.localRegistrations), 0);
            assert.lengthOf(Object.keys(targetFrp.foreignRegistrations), 1);
            assert.deepEqual(notifyPrefixReceived, { [expectedNotifyKey]: expectedNotifyKey });
        })
        shift(done);
    })

    it("should register a given key, unregister remotely, and send a busy message that prevents the node from beeing called again", function(done){
        let opData;
        // create callback for notifications
        let notifyPrefixReceived = {};
        let onNotify = function (tr) {
            notifyPrefixReceived[tr.keys[0].prefix] = tr.keys[0].prefix;
        }
        let clientFrpKey = "01".repeat(nodePrefixLength);
        let clientFrp = frp[clientFrpKey];
        let targetFrpKey = "00".repeat(nodePrefixLength);
        let targetFrp = frp[targetFrpKey];
        let targetResourceKey = targetFrpKey + documentSuffix.substr(0, 2);
        let expectedNotifyKey = targetFrpKey + documentSuffix;

        opData = {
            onNotify: onNotify,
            maxNodes: 1,
            retro: true,
            span: 100,
            transitionTime: ASYNC_TIMEOUT,
            populateTimeout : Math.floor(ASYNC_TIMEOUT/2)
        };
        // expect the registration to succeed
        shift(function () {
            clientFrp.register(targetResourceKey, opData);
        })

        shift(function () {
            // check registrations have been received on the expected frp
            assert.lengthOf(Object.keys(targetFrp.localRegistrations), 0);
            assert.lengthOf(Object.keys(targetFrp.foreignRegistrations), 1);
            // expect the callback to have been called
            assert.deepEqual(notifyPrefixReceived, { [expectedNotifyKey]: expectedNotifyKey });
            // cancel the registration remotely
            targetFrp.cancelForeignRegistration( Object.keys(targetFrp.foreignRegistrations)[0] );
			// retrieve the only node that is connected to the target, and send a busy message through the canal
			let connectedNodes = flock[targetFrpKey].getConnectedNodes();

			assert.lengthOf(connectedNodes, 1);
            targetFrp.sendBusy(connectedNodes[0], 10000);

            assert.lengthOf(Object.keys(targetFrp.localRegistrations), 0);
            assert.lengthOf(Object.keys(targetFrp.foreignRegistrations), 0);
            notifyPrefixReceived = {};
        })

        shift(function(){})

        shift(function () {
            // no registration attempts should have been made
            assert.lengthOf(Object.keys(targetFrp.localRegistrations), 0);
            assert.lengthOf(Object.keys(targetFrp.foreignRegistrations), 0);
            assert.deepEqual(notifyPrefixReceived, {});
        })
        shift(done);
    })

	it("should retrieve some documents through the fetch operation", function(done){
        let opData;

        let clientFrpKey = "01".repeat(nodePrefixLength);
        let clientFrp = frp[clientFrpKey];

        let targetFrpKey = "00".repeat(nodePrefixLength);
        let targetFrp = frp[targetFrpKey];
        let targetNode = node[targetFrpKey];
        let targetDocumentKey = targetFrpKey + documentSuffix;

        // build operation data
        // create callback for notifications
        let documentsReceived = [];
        let ondocument = function(doc, targetPrefix, node){
            documentsReceived.push(targetPrefix);
        	assert.ok(!!doc);
            assert.of(!!node);
        }

        let fetchOperation = {
            sleepTime : Math.floor(ASYNC_TIMEOUT/3),
            ondocument : ondocument
        }

        clientFrp.fetch(fetchOperation);
        // add the key to fetch
        fetchOperation.targets[targetDocumentKey] = [targetNode];

		shift(function(){
			assert.deepEqual(documentsReceived, [targetDocumentKey]);
		})

		shift(done)
    })

    it("should retrieve already existing documents through the feed operation and store them", function(done){
        let clientFrpKey = "01".repeat(nodePrefixLength);
        let clientFrp = frp[clientFrpKey];

        let targetFrpKey = "00".repeat(nodePrefixLength);
        let targetFrp = frp[targetFrpKey];
        let targetNode = node[targetFrpKey];
        let targetDocumentKey = targetFrpKey + documentSuffix;

        // build operation data
        // create callback for notifications
        let operationData = {
            prefix : targetFrpKey,
            span : nodeSpan,
            sleepTime : 50
        }

        // at first, the targeted documents shouldn't be in the warehouse
        clientFrp.hasResourceLocal(targetDocumentKey).then(function(hasResource){
            assert.equal(hasResource, false);
        })

		assert.notOk( clientFrp._flock.hasKey(targetFrpKey) );

        shift(function(){
            // start the feed operation
            clientFrp.feed(operationData);
            assert.ok( clientFrp._flock.hasKey(targetFrpKey) );
        })

        shift(function(){
            clientFrp.hasResourceLocal(targetDocumentKey).then(function(hasResource){
                assert.equal(hasResource, true);
                clientFrp.unfeed(operationData.prefix);
                assert.notOk( clientFrp._flock.hasKey(targetFrpKey) );
                done();
            })
        })
    })

    it("should retrieve a document that will be added after registrations begins", function(done){
        let clientFrpKey = "01".repeat(nodePrefixLength);
        let clientFrp = frp[clientFrpKey];

        let targetFrpKey = "00".repeat(nodePrefixLength);
        let newDocument = document[targetFrpKey];
        newDocument.key = newDocument.key + "01"
        let targetDocumentKey = newDocument.key;
        newDocumentBytes = clientFrp._frf.documentToBytes(newDocument);

        // build operation data
        // create callback for notifications
        let operationData = {
            prefix : targetFrpKey,
            span : nodeSpan,
            sleepTime : 50
        }

        // at first, the targeted documents shouldn't be in the warehouse
        clientFrp.hasResourceLocal(targetDocumentKey).then(function(hasResource){
            assert.equal(hasResource, false);
            shift(function(){
                // start the feed operation
                clientFrp.feed(operationData);
            })

            shift(function(){
                warehouse[targetFrpKey].storeResource(newDocument.key ,newDocumentBytes, {dversion : newDocument.dversion});
            })

            shift(function(){})

            shift(function(){
                clientFrp.hasResourceLocal(targetDocumentKey).then(function(hasResource){
                    assert.equal(hasResource, true);
                    clientFrp.unfeed(operationData.prefix);
                    done();
                })
            })

        })
    })

    it("should not add retrieve a document that will be added after registrations begins", function(done){
        let clientFrpKey = "01".repeat(nodePrefixLength);
        let clientFrp = frp[clientFrpKey];
        let targetFrpKey = "00".repeat(nodePrefixLength);
        let newDocument = document[targetFrpKey];
        newDocument.key = newDocument.key + "01"
        let targetDocumentKey = newDocument.key;
        newDocumentBytes = clientFrp._frf.documentToBytes(newDocument);

        // build operation data
        // create callback for notifications
        let operationData = {
            prefix : targetFrpKey,
            span : nodeSpan,
            sleepTime : 50
        }

        // at first, the targeted documents shouldn't be in the warehouse
        clientFrp.hasResourceLocal(targetDocumentKey).then(function(hasResource){
            assert.equal(hasResource, false);

            shift(function(){
                // start the feed operation
                clientFrp.feed(operationData);
            })

            shift(function(){
                warehouse[targetFrpKey].storeResource(newDocument.key ,newDocumentBytes, {dversion : newDocument.dversion});
            })

            shift(function(){})

            shift(function(){
                clientFrp.hasResourceLocal(targetDocumentKey).then(function(hasResource){
                    assert.equal(hasResource, true);
                    clientFrp.unfeed(operationData.prefix);
                    warehouse[clientFrpKey].deleteResource(newDocument.key);
                    done();
                })
            })
        })
    })
});


describe("testing various methods to fetch documents while testing their signature", function(){
	let warehouses, storedKeys;
	let frpClient, frpTarget, flockClient;
    let socketPort = 4500;
	let skyRootDocument, signed_skyRootDocument, wrongSkyRootDocument, signed_wrongSkyRootDocument, signed_subdoc1, signed_subdoc2, subdoc1Key, subdoc2Key, signed_incorrectSubdoc2;
	let skyRootKey, wrongSkyRootKey, incorrectSubdoc1Key, incorrectSubdoc2Key;
	let clientFrpNode, targetFrpNode;

	beforeEach(function(){
        this.timeout(20000);
        warehouses = [];
        storedKeys = [];

		let nodeSpan = 100;
        // build two frp

		// frp1 : the client
		let frpClientKey = "0123";
        flockClient = new Flock({keys : [{prefix : frpClientKey, span:nodeSpan}], cronInterval : 1000000});
        frpClient =  new Frp({flock : flockClient});
        let warehouseClient = new testData.WarehouseClass({ collection : "documents_" + frpClientKey });
        warehouses.push(warehouseClient);
        frpClient.addWarehouse(warehouseClient);
        let flockPort = socketPort++;
        let clientSpawner = new NIWebsocketSpawner({ socketPort : flockPort, address : "ws://127.0.0.1:"+flockPort});
        flockClient.metaInterface.addSpawner(clientSpawner);

		// frp2 : target with correct and incorrect documents
        let frpTargetKey = "4567";
        let flockTarget = new Flock({keys : [{prefix : frpTargetKey, span:nodeSpan}]});
        frpTarget =  new Frp({flock : flockTarget});
        let warehouseTarget = new testData.WarehouseClass({ collection : "documents_" + frpTargetKey });
        warehouses.push(warehouseTarget);
        frpTarget.addWarehouse(warehouseTarget);
        let flockPort2 = socketPort++;
        let clientSpawner2 = new NIWebsocketSpawner({ socketPort : flockPort2, address : "ws://127.0.0.1:"+flockPort2});
        flockTarget.metaInterface.addSpawner(clientSpawner2);

        // build several documents to be stored in the second frp
        let signtools1 = frpTarget._frf.createKeys();
		// sky root
        skyRootDocument = frpTarget._frf.createSky({
            body : new Uint8Array([1,2,4,8,16,32,64,128]),
        },signtools1)
        signed_skyRootDocument = frpTarget._frf.createSignedDocument(skyRootDocument, signtools1, true);
        skyRootKey = signed_skyRootDocument.key;
        let signed_skyRootDocument_bytes = frpTarget._frf.documentToBytes(signed_skyRootDocument);
        warehouseTarget.storeResource(skyRootKey , signed_skyRootDocument_bytes, {dversion : 0});
        storedKeys.push(skyRootKey);

        // sub document of the correct sky root : it defines a new signTools for subdocuments
        let signtools2 = frpTarget._frf.createKeys();
        subdoc1Key = skyRootKey + "aabbccdd";
        let subdoc1 = frpTarget._frf.createSky({
			key : subdoc1Key,
            prefix : skyRootKey.length/2,
            body : new Uint8Array([1,2,4,8,16,32,64,128]),
        },signtools2)
        signed_subdoc1 = frpTarget._frf.createSignedDocument(subdoc1, signtools1, false);

        let signed_subdoc1_bytes = frpTarget._frf.documentToBytes(signed_subdoc1);
        warehouseTarget.storeResource(subdoc1Key , signed_subdoc1_bytes, {dversion : 0});
        storedKeys.push(subdoc1Key);

        // subdocument of the correct subdocument
        subdoc2Key = subdoc1Key + "aabb";
        let subdoc2 = {
        	key : subdoc2Key,
			prefix : subdoc1Key.length / 2,
            body : new Uint8Array([1,2,4,8,16,32,64,128]),
        };
        signed_subdoc2 = frpTarget._frf.createSignedDocument(subdoc2, signtools2);

        let signed_subdoc2_bytes = frpTarget._frf.documentToBytes(signed_subdoc2);
        warehouseTarget.storeResource(subdoc2Key , signed_subdoc2_bytes, {dversion : 0});
        storedKeys.push(subdoc2Key);

        // wrong sky root document
        wrongSkyRootDocument = frpTarget._frf.createSky({
            body : new Uint8Array([1,2,4,8,16,32,64,128]),
        },signtools1)
        signed_wrongSkyRootDocument = frpTarget._frf.createSignedDocument(wrongSkyRootDocument, signtools1, true);
        wrongSkyRootKey = "aa".repeat(32);
        signed_wrongSkyRootDocument.key = wrongSkyRootKey;
        let signed_wrongSkyRootDocument_bytes = frpTarget._frf.documentToBytes(signed_wrongSkyRootDocument);
        warehouseTarget.storeResource(wrongSkyRootKey , signed_wrongSkyRootDocument_bytes, {dversion : 0});
        storedKeys.push(wrongSkyRootKey);

        // subdocument of the incorrect sky root
        incorrectSubdoc1Key = wrongSkyRootKey + "01"
        let incorrectSubdoc1 = {
            body : new Uint8Array([1,2,4,8,16,32,64,128]),
            key : incorrectSubdoc1Key,
			prefix : wrongSkyRootKey.length/2
        }
        let signed_incorrectSubdoc1 = frpTarget._frf.createSignedDocument(incorrectSubdoc1, signtools1, false);

        let signed_incorrectSubdoc1_bytes = frpTarget._frf.documentToBytes(signed_incorrectSubdoc1);
        warehouseTarget.storeResource(incorrectSubdoc1Key , signed_incorrectSubdoc1_bytes, {dversion : 0});
        storedKeys.push(incorrectSubdoc1Key);

        // subdocument of the incorrect subdocument
        incorrectSubdoc2Key = incorrectSubdoc1Key + "02";
        let incorrectSubdoc2 = {
            body : new Uint8Array([1,2,4,8,16,32,64,128]),
            key : incorrectSubdoc2Key,
            prefix : incorrectSubdoc1Key.length/2
        }
        signed_incorrectSubdoc2 = frpTarget._frf.createSignedDocument(incorrectSubdoc2, signtools1, false);

        let signed_incorrectSubdoc2_bytes = frpTarget._frf.documentToBytes(signed_incorrectSubdoc2);
        warehouseTarget.storeResource(incorrectSubdoc2Key , signed_incorrectSubdoc2_bytes, {dversion : 0});
        storedKeys.push(incorrectSubdoc2Key);

        flockTarget.addKey({prefix : skyRootKey, span : 100});
        flockTarget.addKey({prefix : wrongSkyRootKey, span : 100});
        targetFrpNode = {
            canals : flockTarget.getCanals(),
            id : flockTarget.getId(),
            keys : flockTarget.getKeys()
        }

        return new Promise(r=>setTimeout(r,1000))
	})

	afterEach(function(){
		let warehousePromises = [];
		for(let warehouse of warehouses){
            let deletePromises = [];
			for(let storedKey of storedKeys){
                let prom = warehouse.deleteResource(storedKey);
                deletePromises.push(prom);
			}
            warehousePromises.push(Promise.all(deletePromises));
		}
		return Promise.all(warehousePromises).then(
			function(){
                frpClient.stop();
                frpTarget.stop();
                return Promise.resolve()
			}
		)
	})

	it("should check the validity of a correct sky document", function(done){
		let prom = frpClient.checkDocumentValidity({
			document : signed_skyRootDocument
		}).then(function(result){
			assert.ok(result);
			done()
		})
	})

    it("should check the validity of an incorrect sky document", function(done){
        let prom = frpClient.checkDocumentValidity({
            document : signed_wrongSkyRootDocument,
        }).then(function(result){
            assert.notOk(result);
            done()
        })
    })

    it("should check the validity of a correct subdocument, while providing parents", function(done){
        let prom = frpClient.checkDocumentValidity({
            document : signed_subdoc1,
            parents : [signed_skyRootDocument]
        }).then(function(result){
            assert.ok(result);
            done()
        })
    })


    it("should check the validity of a correct subdocument of a subdocument (with different signtools), while providing parents", function(done){
        let prom = frpClient.checkDocumentValidity({
            document : signed_subdoc2,
            parents : [signed_skyRootDocument, signed_subdoc1]
        }).then(function(result){
            assert.ok(result);
            done()
        })
    })

    it("should check the validity of a correct subdocument, while not providing parents", function(done){
        this.timeout(50000)
        let prom = frpClient.checkDocumentValidity({
            document : signed_subdoc1,
            node : targetFrpNode, // provide the node to perform requests on it
        }).then(function(result){
            assert.ok(result);
            done()
        })
    })

    it("should check the validity of a correct subdocument of a subdocument (with different signtools), while not providing parents", function(done){
    	this.timeout(50000)
        flockClient.addNode(targetFrpNode); // provide the node to perform requests on it
        let prom = frpClient.checkDocumentValidity({
            document : signed_subdoc2
        }).then(function(result){
            assert.ok(result);
            done()
        })
    })
	it("should check the validity of an incorrect subdocument, while not providing the parents", function(done){
		this.timeout(5000)
        flockClient.addNode(targetFrpNode);
        let prom = frpClient.checkDocumentValidity({
            document : signed_incorrectSubdoc2,
			timeout : 1000
        }).then(function(result){
            assert.notOk(result);
            done()
        })
	})

    it("should retrieve a document with the request operation, check its validity", function(done){
        flockClient.addNode(targetFrpNode);
        frpClient.request(skyRootKey, {
            check : true
        }).then(function(doc){
            assert.deepInclude(doc, signed_skyRootDocument);
            done();
        }).catch(function(){
            console.log("available nodes ", flockClient.getNodes().map(n=>[n.id, n.keys]));
		})
    })

    it("should retrieve a document with the request operation, check its validity and store the parents", function(done){
        flockClient.addNode(targetFrpNode);
        frpClient.request(subdoc2Key, {
            check : true
        }).then(function(doc){
            assert.deepInclude(doc, signed_subdoc2);
            return frpClient.hasResourceLocal(subdoc1Key)
        })
		.then(function(hasResource){
			assert.ok(hasResource);
			return frpClient.hasResourceLocal(skyRootKey)
		})
		.then(function(hasResource){
			assert.ok(hasResource);
			done();
		})
    })

    it("should fail to retrieve an incorrect sky root document ", function(done){
        this.timeout(3000)
        flockClient.addNode(targetFrpNode);
        frpClient.request(wrongSkyRootKey, {
            check : true,
            timeout : 2000
        }).then(function(doc){
            assert.ok(false, "it should have failed since no correct document exists with this key")
            done()
        })
            .catch(function(){
                assert.ok(true)
                done();
            })
    })

    it("should fail to retrieve an incorrect sub document ", function(done){
        this.timeout(3000)
        flockClient.addNode(targetFrpNode);
        frpClient.request(incorrectSubdoc1Key, {
            check : true,
            timeout : 2000
        }).then(function(doc){
            assert.ok(false, "it should have failed since no correct document exists with this key")
            done()
        })
		.catch(function(){
			assert.ok(true)
			done();
		})
    })

    it("should fail to retrieve an incorrect sub document of a subdocument ", function(done){
        this.timeout(3000)
        flockClient.addNode(targetFrpNode);
        frpClient.request(incorrectSubdoc2Key, {
            check : true,
            timeout : 2000
        }).then(function(doc){
            assert.ok(false, "it should have failed since no correct document exists with this key")
            done()
        })
		.catch(function(){
			assert.ok(true)
			done();
		})
    })


	it("should retrieve documents through a feed operation, and check their validity", function(done){
        flockClient.addNode(targetFrpNode);
		this.timeout(5000);

        // build operation data
        // create callback for notifications
        let operationData = {
            prefix : skyRootKey,
            span : 100,
            sleepTime : 50,
			check : true
        }

        // at first, the targeted documents shouldn't be in the warehouse
        frpClient.hasResourceLocal(skyRootKey).then(function(hasResource){
            assert.equal(hasResource, false);
        })

        shift(function(){
            // start the feed operation
            frpClient.feed(operationData);
        })

        shift(function(){

            frpClient.hasResourceLocal(skyRootKey).then(function(hasResource){
                assert.equal(hasResource, true);
                return frpClient.hasResourceLocal(subdoc1Key)
            })
			.then(function(hasResource){
				assert.equal(hasResource, true);
				return frpClient.hasResourceLocal(subdoc2Key)
			})
			.then(function(hasResource){
				assert.equal(hasResource, true, "should have the key stored : " + subdoc2Key);
			})
			.then(function(){
				frpClient.unfeed(operationData.prefix);
				done();
			})
        })
	})

    it("should not retrieve incorrect documents through a feed operation after checking their validity ", function(done){
        this.timeout(5000);
    	flockClient.addNode(targetFrpNode);
        // build operation data
        // create callback for notifications
        let operationData = {
            prefix : wrongSkyRootKey,
            span : 100,
            sleepTime : 50,
            check : true
        }

        // at first, the targeted documents shouldn't be in the warehouse
        frpClient.hasResourceLocal(wrongSkyRootKey).then(function(hasResource){
            assert.equal(hasResource, false);
        })

        shift(function(){
            // start the feed operation
            frpClient.feed(operationData);
        })

        shift(function(){
            frpClient.hasResourceLocal(wrongSkyRootKey).then(function(hasResource){
                assert.equal(hasResource, false);
                return frpClient.hasResourceLocal(incorrectSubdoc2Key)
            })
                .then(function(hasResource){
                    assert.equal(hasResource, false);
                    return frpClient.hasResourceLocal(incorrectSubdoc2Key)
                })
                .then(function(hasResource){
                    assert.equal(hasResource, false);
                })
                .then(function(){
                    frpClient.unfeed(operationData.prefix);
                    done();
                })
        })
    })
})
