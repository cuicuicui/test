/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

/*** frp (flock resource protocol) implements frp communication messages to contact other frp nodes in order to send request/receive documents
 * frp is a plug service where you add flock applications (flaps) to request or carry resources : when receiving a request, frp will forward it to the flaps
 * 
 * ***/

import {Flaptools} from "../../flaptools/lib/Flaptools.js"
import {Flock} from "../../flock/lib/flock.js"
import {Frf} from "../../frf/lib/Frf.js"

class Frp {
	constructor(data={}){
		this._flaptools = data.flaptools || new Flaptools();
		this._threadops = this._flaptools.threadops();
		this._flock = data.hasOwnProperty("flock") ? data.flock : new Flock();
		this._metainterface = data.metaInterface || ( this._flock ? this._flock.metaInterface : null);
		this._frf = data.frf || new Frf();
		
		this.transactions = {};
		this._warehouse = [];
		this._cronId = null;
		this.cronPeriod = data.cron || 4000; // period at which cron is launched
		this.maxUnrestrained = 20; // number of parallel nodes that can be sent data
		this.frpPort = 3;
		this.stopped = false;

        // data about registration operations, indexed by prefix
        this.registrationsOperations = {};
        // data about feed operations, indexed by prefix
        this.feedOperations = {};
		// fetch operations, indexed by their operation id
		this.fetchOperations = {};

		// data about current registrations, indexed by tid
		this.localRegistrations = {};
		this.foreignRegistrations = {};

		// list of busy nodes
		this._busyNodes = {};
		this._busyNodesArray = []; // array to be passed by reference :

		this._initConstants();
		this._initNodejs();
		this._initTemplates();
		if(this._metainterface) this.initInterface(this._metainterface);
		this.startCron();
	}
	
	////////////////////////////////////////////////////////////////////
	//////////////////      END USER API              //////////////////
	////////////////////////////////////////////////////////////////////
	
	// try to retrieve a resource either locally or remotely
	getResource(prefix, opts={}){
		let $this = this;
		if(!opts.timeout) opts.timeout = 4000;
		// search for the document locally

		let prom = this.getResourceLocal(prefix,opts).then(docBytes=>$this._frf.documentToObject(docBytes))
		// if not found, search for it remotely
		.catch(()=>$this.request(prefix, opts));
		let t1 = Date.now();
		return prom;
	}
	
	// establish connections to nodes in order to retrieve a specific resource
	request(hexKey, op={}){
		// keep the list of nodes that have been asked for the resource
		op.askedNodes = op.askedNodes || {};
		// keep the list of nodes that needs to be asked for the resource
		op.resourceNodes = op.resourceNodes || {};
		op.name = "getResource for prefix '" + hexKey + "'";
		op.thread = this._getResourceThread.bind(this);
		// build the target key from the prefix
		op.key = { prefix : hexKey, span : 0};
		if(op.dversion) op.key.dversion = op.dversion;
		if(!op.hasOwnProperty("check")) op.check = false; // check the validity (signature, expiration time...) of documents retrieved
        // build data for the reach operation
        op.reachOperation = {
            prefix : hexKey,
            // keep the list of nodes visited with reachNode request
            skipNodes : op.skipNodes || {},
            maxNodes : op.maxNodes || 1,
			resourceNodes : op.resourceNodes,
			askedNodes : op.askedNodes,
			withOwnNode : false,
			stats : {}
        }
		return this._threadops.startOperation(op);
	}

    /***
	 * check the recursive validity of a document :
	 * assume the stored parents are correctly signed
	 * if the parent cannot be found locally, send requests, asking first the node that provided the last document
	 * return a promise
     */
	checkDocumentValidity(op={}){
		let $this = this;
		// prevent the algorithm from looping indefinitely
		if(!op.hasOwnProperty("maxIterations")) op.maxIterations = 8;
		if(!op.maxIterations) return Promise.reject("too many iterations");
		else op.maxIterations--;
		// extract data from all given documents / signed documents
        let document = this._frf.extractDataFromSigned(op.document);
		if(!op.docData){
            op.docData = {};
            op.docData[document.key] = document;
            if(!op.parents) op.parents = [];
            for(let parent of op.parents){
                let parentData = this._frf.extractDataFromSigned(parent);
                op.docData[parentData.key] = parentData;
            }
		}
		// ensure the presence of all needed parents
		let nextDoc = document;
		while(!this._frf.isSKyRoot(nextDoc.key)){
            if(!nextDoc.prefix) return Promise.reject("non-root document without prefix property for key : "+nextDoc.key);
            let nextKey = nextDoc.key.slice(0, 2*nextDoc.prefix);
			if(op.docData[nextKey]){
				nextDoc = op.docData[nextKey];
				continue;
			}
			// else try to retrieve the document from local database before continuing
			return this.getResourceLocal(nextKey).then(function(newDoc){
                newDoc = $this._frf.documentToObject(newDoc);
                op.parents.push(newDoc);
                let newDocData = this._frf.extractDataFromSigned(newDoc);
                op.docData[newDocData.key] = newDocData;
                return $this.checkDocumentValidity(op);
			})
			// if the missing parent is not found locally, ask for it remotely
			.catch(function(){
				// if a node is provided for the operation, ask it in priority
				let resourceNodes = {};
				if(op.node) resourceNodes[op.node.id] = op.node;
				return $this.request(nextKey, {
                    resourceNodes : resourceNodes,
					maxConcurrentThreads : 1,
					timeout : op.timeout || 3000,
					check : true
				}).then(function(newDoc){
					op.parents.push(newDoc);
					let newDocData = $this._frf.extractDataFromSigned(newDoc);
					op.docData[newDocData.key] = newDocData;
					let documentBytes = $this._frf.documentToBytes(newDoc);
					$this.pushResourceLocal(newDocData.key, documentBytes);
                    return $this.checkDocumentValidity(op);
				})
				.catch(function(e){
					console.log("checkDocumentValidity error : parent not found : " + nextKey, e);
					return Promise.resolve(false);
				})
			})
		}
		// once all the parents have been gathered, process the answer
		let isCorrect = this._frf.checkDocument(op.document, op.parents);
		return Promise.resolve(isCorrect);
	}

    /*** return the list of registered keys
	 ***/
	getRegistrations(){
		return this.registrationsOperations.slice();
	}

	getResourceLocal(key, opts={}){
		let promises = this._warehouse.map( wh => wh.getResource(key, opts) );
		let raceOpts = {};
		if(opts.timeout) raceOpts.timeout = opts.timeout;
		let res = this.race(promises, raceOpts);
        return res;
	}

	// retrieve all available keys of all warehouses : the result is cached until keys are added/removed
	// option "force" : reload all the keys by asking again the warehouse
	// return a promise that resolves with an array of keys
    getResourceKeysLocal(key="", opts={}){
		if(!this._getKeysPromise || opts.force){
            let span = opts.span || 128;
            let promises = this._warehouse.map( wh => wh.availableResources(key, key+"ff".repeat(span), opts={}) );
            this._getKeysPromise = Promise.all(promises).then(function(promiseResults){
            	let keys = [].concat(...promiseResults);
            	let filteredKeys = [];
            	let pushedKeys = {}; // prefix => dversions
            	for(let key of keys){
            		if(!pushedKeys[key.prefix]) pushedKeys[key.prefix] = [];
            		if(!pushedKeys[key.prefix].includes(key.dversion)){
                        pushedKeys[key.prefix].push(key.dversion);
                        filteredKeys.push(key);
					}
				}
				return filteredKeys;
			});
		}
		return this._getKeysPromise;
    }

    // reset the cached local keys
	resetCachedKeys(){
		if(this._getKeysPromise) delete this._getKeysPromise;
	}

	hasResourceLocal(key, opts={}){
		let promises = this._warehouse.map( wh => wh.hasResource(key, opts) );
		return Promise.all(promises).then(arr=> arr.includes(true))
	}

	deleteResourceLocal(key, opts={}){
		this.resetCachedKeys();
        let promises = this._warehouse.map( wh => wh.deleteResource(key, opts) );
        return Promise.all(promises).then(arr=> arr.includes(true))
	}
	
	pushResourceLocal(key, resource, opts={}){
		this.resetCachedKeys();
		let promises = this._warehouse.map( wh => wh.storeResource(key, resource, opts) );
		return this.race(promises);
	}

	// stop every operations of the frp
	stop(){
		for(let prefix in this.registrationsOperations){
			this.unregister(prefix);
		}
		for(let id in this.fetchOperations){
			this._threadops.resolveOperation(id);
			delete this.fetchOperations[id];
		}
		for(let warehouse of this._warehouse){
			warehouse.stop();
		}
		if(this._flock) this._flock.stop();
        this.stopped = true;
	}

	////////////////////////////////////////////////////////////////////
	//////////////          THREADOPS HELPERS          /////////////////
	////////////////////////////////////////////////////////////////////

	/*** perform a register operation, that retrieve notifications from targeted node
	 *   in parallel, perform a fetch operation that retrieves document from a list of prefix
	***/
	feed(op){
		if(!op.prefix) throw "cannot feed without a defined 'prefix' parameter";
		if(op.prefix in this.feedOperations) throw "prefix : "+op.prefix+" already beeing feeded in another operation";
		if(!op.stats) op.stats = {};
		let $this = this;
		op.key = {
			prefix : op.prefix,
			span : op.hasOwnProperty("span") ? op.span : 100
		}
		// keep track of keys that need to be asked : key => [nodes responsible of it]
		op.nextRequestedKeys = {};
		// keep track of the keys that should not be asked again
		op.skipKeys = [];
		if(!op.span) op.span = 0;
		op.name = "feed operation for prefix : "+op.prefix+" and span "+op.span;

		// start a fetch operation to retrieve documents
		op.fetchOperation = {
            maxConcurrentThreads : op.maxConcurrentThreads || 5,
			targets : {},
            sleepTime : op.sleepTime || 2000,
			check : !!op.check
		}

		op.fetchOperation.ondocument = function(doc, prefix){
			let checkPromise = !op.check ? Promise.resolve(true) : $this.checkDocumentValidity({document : doc});
			checkPromise.then(function(isValid){
				if(!isValid) return
                let byteDoc =  $this._frf.documentToBytes(doc)
                let opts = {
                    dversion : doc.dversion || 0
                }
                $this.pushResourceLocal(prefix, byteDoc, opts);
			})
		}
		this.fetch(op.fetchOperation);

		// start a register operation :
		op.registerOperation = {
			span : op.key.span,
			retro : op.hasOwnProperty("retro") ? !!op.retro : true
		}
		op.registerOperation.onNotify = function(data){
			$this.getResourceKeysLocal().then(function(localKeys){
				// create the list of keys already stored
				let alreadyStoredKeys = {};
				for(let key of localKeys){
                    alreadyStoredKeys[key.prefix] = true;
				}
                for(let key of data.keys){
                    let prefix = key.prefix;
                    if(alreadyStoredKeys[prefix]) continue;
                    // check that the notified key is included in the requested key
                    if(!$this._flock.isIncluded({prefix : key, span : 0}, op.key)) continue;
                    if(!op.fetchOperation.targets[prefix]) op.fetchOperation.targets[prefix] = [];
                    if(!op.fetchOperation.targets[prefix].includes(data.node)) op.fetchOperation.targets[prefix].push(data.node);
                }
			})
		}
		this.register(op.prefix, op.registerOperation);
		this._flock.addKey(op.key);
		this.feedOperations[op.prefix] = op;
	}

    /***
	 * stop a feed operation
     */
    unfeed(prefix){
		let op = this.feedOperations[prefix];
    	if(!op) return;
    	this._threadops.resolveOperation(op.fetchOperation.id);
    	this.unregister(op.prefix);
    	this._flock.removeKey(op.key);
	}

    /***
     * for a given feed prefix key, update the current status of the feed (in term of connected nodes...)
     */
	updateFeedStatus(prefix){
		let op = this.feedOperations[prefix];
		if(!op) return;

		let connectedNodes = this._flock.getConnectedNodes();
		let feedingNodes = [];
		for(let node of connectedNodes){
			for(let key of node.keys||[]){
				if(this._flock.isIncluded(op.key, key)){
					feedingNodes.push(node);
					break;
				}
			}
		}
        if(!op.stats) op.stats = {};
        let stats = this._prepareCurrentPeriod(op.stats);
        stats.connectedNodes = connectedNodes.length;
        stats.feedingNodes = feedingNodes.length;
	}

    /***
	 * for a given prefix key, notify of a new stats to be agregated. Possible keys are :
	 * - requests : number of requests processed
	 * - bandwidth : amount of data transmitted
     */
	updateOperationStats(prefix, data={}){

        let op = this.feedOperations[prefix];
        if(!op) return;
		if(!op.stats) op.stats = {};
        // create the data about the current minute
        let stats = this._prepareCurrentPeriod(op.stats);
        if(data.requests) stats.requests = (stats.requests || 0) + data.requests;
        if(data.bandwidth) stats.bandwidth = (stats.bandwidth || 0) + data.bandwidth;

        return data;
	}

	// compute the minute of a given time
	minute(t=Date.now()){
		return Math.floor(Date.now() / 60000);
	}

	// prepare an object for the current period in a stats object
	_prepareCurrentPeriod(stats={}){
        let currentMinute = this.minute();
        if(!stats[currentMinute]) stats[currentMinute]  = {};
        return stats[currentMinute];
	}

	// retrieve the feed operation a given key belong to
	retrieveFeedOperation(key){
		if(typeof key === "string") key = {prefix : key, span : 0};
		for(let feedPrefix in this.feedOperations){
			let op = this.feedOperations[feedPrefix];
			if(this._flock.isIncluded(key, op.key)) return feedPrefix;
		}
	}

	// retrieve documents given an object { prefix=>[nodes to contact] }
	// pause when no prefix needs to be fetched
	fetch(op){
    	// list of targets=>nodes to be used for requests
		if(!op.targets) op.targets = {};
    	// list of prefix that are currently beeing fetched by a thread
		op.pendingPrefix = {};
		// if(op.prefix){
		// 	for(let i in op.prefix) op.pendingPrefix[i] = op.prefix[i];
		// }
		// by default, run the operation indefinitely, and sleep when no new target are available
        if(!op.hasOwnProperty("maxThreads")) op.maxThreads = +Infinity;
        // timeout for  nodeRequest (request of a single document to a single node)
		if(!op.requestTimeout) op.requestTimeout = 15000;
        // when no action is to be done, sleep before executing the next thread
        if(!op.hasOwnProperty("sleepTime")) op.sleepTime = 2000;
        op.thread = this._fetchThread.bind(this);
        op.name = "fetch operation";
        this._threadops.startOperation(op);
		this.fetchOperations[op.id] = op;
    }

	_fetchThread(op){
    	let $this = this;
    	// determine the next key to reach
		let prefixTarget = null;
		for(let prefix in op.targets){
			if(prefix in op.pendingPrefix) continue;
			let isBlocked = false; // check if another key which impacts the same documents is beeing processed
			for(let pendingPrefix in op.pendingPrefix){
				if(pendingPrefix.slice(0,64) === prefix.slice(0,64)) isBlocked = true;
			}
			if(isBlocked) continue;
			prefixTarget = prefix;
			break;
		}
		// if no target are available, sleep this thread
		if(prefixTarget === null){
			return this._threadops.sleep(op.sleepTime);
		}

        // move the key to fetch in the pending keys
        op.pendingPrefix[prefixTarget] = op.targets[prefixTarget];
        delete op.targets[prefixTarget];

		// create a small aux method to push back or remove completely the prefixTarget, once the document has been retrieved
		let removeTarget = function(){
			if(op.pendingPrefix[prefixTarget]) delete op.pendingPrefix[prefixTarget];
		}
		let pushBackTarget = function(){
			if(op.pendingPrefix[prefixTarget]){
				if(op.pendingPrefix[prefixTarget].length) {
                    op.targets[prefixTarget] = (op.targets[prefixTarget] || []).concat( op.pendingPrefix[prefixTarget] );
				}
                delete op.pendingPrefix[prefixTarget];
			}
		}

		let targetNode = op.pendingPrefix[prefixTarget].pop();
		let prom = this.nodeRequest(targetNode, { key : {prefix : prefixTarget}, timeout : op.timeout });
		return prom.then(function(trAnswer){
            if(!trAnswer.document) return Promise.reject();
            // TODO  : check the document validity
            // store the document
            if(op.ondocument){
            	op.ondocument(trAnswer.document, prefixTarget, targetNode);
			}
            return resultPromise;
		})
		.catch(pushBackTarget)
	}

    /***
	 * stop registration on a given key
     */
    unregister(prefix){
        let regop = this.registrationsOperations[prefix];
        if(!regop) return;
        for(let rid in regop.nodeRegistrations){
            let tid = regop.nodeRegistrations[rid].tid;
            this.cancelLocalRegistration(tid);
        }
        regop.nodeRegistrations = {};
        this._threadops.rejectOperation( regop.id, "stopped operation" );
        delete this.registrationsOperations[prefix];
    }

    /*** reach appropriate nodes to send register request
     * expect the following data :
     * key : the key to register
     * maxNodes : number of nodes to reach
     * onNotify : callback for notify messages
     */
    register(hexKey, op={}){
        if(this.registrationsOperations[hexKey]) throw "The key "+ hexKey +" is already registered";
        else this.registrationsOperations[hexKey] = op;
        let $this = this;
        op.id = this._flaptools.randomHex(16);
        op.hexKey = hexKey;
		op.byteKey = this._flaptools.hexToBytes(hexKey);
        op.name = "register for prefix '" + hexKey + "'";
        op.key = { prefix : hexKey, span : op.span || 0};
        // minimum time to wait between two register threads
        if(!op.hasOwnProperty("transitionTime")) op.transitionTime = 1000;
        // keep the list of nodes that have been asked for the resource and should not be asked again
        op.askedNodes = op.askedNodes || {};
        // keep track of register instructions that havce been sent to a given node
		op.nodeRegistrations = {};
		if(!op.maxNodes) op.maxNodes = 4;

		// the register operation rely on a populate operation (from the Flock) that is launched regularly with a small timeout
		op.populateOperation = {
			timeout : op.populateTimeout || 3000,
			maxThreads : op.maxThreads || 100,
		};

        // build the formation to reach
		if(!op.formation){
            op.formation = new this._flock._Formation(op.byteKey, {
				buckets : {[4*hexKey.length] : op.maxNodes}, // number of nodes with the targeted key to reach
				beforeKey : 0,
				afterKey : 0
			});
		}
		op.populateOperation.formation = op.formation;

		// helper to make the main thread stops and restart it after some time
		op.stopAndRestart = function(){
			$this._threadops.resolveOperation(op.id);
			if(op.mainRegisterThread) delete op.mainRegisterThread;
			op.sleepAndRestart();
		}

		// helper to restart the operation after some time
		op.sleepAndRestart = function(){
            let sleepTime = Math.max(0, op.transitionTime - (Date.now() - op.startTime) );
			setTimeout(function(){
				// try to restart the thread if there isn't any
                op.startRegisterThread()
			}, sleepTime)
		}

		// helper to build callback for when registrations are canceled remotely
        // create a callback for nodeRegister cancels
        op.createRegisterCancelCallback = function(registerId){
            return function(){
                if(!(registerId in op.nodeRegistrations)) return;
                let node = op.nodeRegistrations[registerId].node;
                let nodeId = node.id;
                // make it possible to ask the node that triggered the registration cancel
                if(node._registrationOps && node._registrationOps.includes(op.id) ){
                	node._registrationOps.splice(node._registrationOps.indexOf(op.id),1 );
                }
                delete op.nodeRegistrations[registerId];
                // resume the operation if it was stopped
                op.sleepAndRestart();
            }
        }
		// build populate callback
        op.populateOperation.onpopulate = function(node, bucket){
        	// if the operation has been removed, don't process anything
			if(!$this.registrationsOperations[hexKey]) return;
            // remove the node from the list to nodes to be processed
			let nid = node.id;

			// if a node is already notifying the frp, dont send another request
			if(!node._registrationOps) node._registrationOps = [];
            if(node._registrationOps.includes(op.id)){
                return;
			}
            node._registrationOps.push(op.id);

            // ask this particular node for the resource notification
			let registerRandomId = $this._flaptools.randomHex(16);
            let registerData = {
                keys : [op.key],
                retro : !!op.retro,
                onNotify : op.onNotify,
                // by default, use a cancel callback that redo a register operation
                onCancel : op.onCancel || op.createRegisterCancelCallback(node.id)
            };
            let registerTid = $this.nodeRegister(node, registerData);
            op.nodeRegistrations[node.id] = {
                tid : registerTid,
                node : node
            };

			// stop the populate operation if enough nodes have been reached
            if(Object.keys(op.nodeRegistrations) >= op.maxNodes){
                op.stopAndRestart();
            }
        }
		
        // ensure the register operation should be performed
        op.isOperationRunning = function(){
			return !!$this.registrationsOperations[hexKey]
		}

        // method to start the main register thread
        // start/restart the register thread for a given key, but don't start a new one if there is already one active
        op.startRegisterThread = function(){
			if(!op.isOperationRunning() ) return;
            if(!op || op.mainRegisterThread) return;
            op.startTime = Date.now();
            // reset the list of busy nodes to not call
			op.populateOperation.skipNodes = $this.busyNodes()
            // once the operation is completed, sleep and restart
            op.mainRegisterThread = $this._flock.populateFormation(op.populateOperation).then(op.stopAndRestart).catch(op.stopAndRestart);
            return op.mainRegisterThread;
        }

        op.startRegisterThread();
    }

	_getResourceThread(op){
		if(this.stopped){
			this._threadops.rejectOperation(op.id);
			return Promise.resolve()
        }
		if(op.done) return Promise.resolve();
		// uniformize the number of threads already launched
		let totalThreads = Math.max(op.reachOperation||0, op.reachOperation.totalThreads||0);
		op.totalThreads = totalThreads;
		op.reachOperation.totalThreads = totalThreads;
		// if there are nodes stored in the rightNodes array, test those nodes to ask for the resource
		if(Object.keys(op.resourceNodes).length) return this._getResourceAskThread(op);
		// else, try to reach appropriates nodes
		else{
			op.reachOperation.skipNodes = this.saturatedNodes();
            return this._reachNodeThread(op.reachOperation);
		}
	}

	// start a thread to reach the appropriate node
    _reachNodeThread( op ){
		var $this = this;
		// find a node that might contain the resource
		return this._flock.reachNode( op ).then(function(nodes){
			let nodeWithResource = null;
			for(let node of nodes){
				if(node.id && !(op.askedNodes[node.id] || op.resourceNodes[node.id])){
					op.resourceNodes[node.id] = node;
                    op.skipNodes[node.id] = true;
                }
			}
			if(!Object.keys(op.resourceNodes).length) return $this._threadops.sleep();
			else return Promise.resolve();
		})
	}

	// return the list of node (nid=>nid) that are ongoing too many operations
	saturatedNodes(){
    	let res = {};
    	let targeted = this.targetedNodes();
    	for(let nid in targeted){
    		if(this._targeting[nid] > this.MAX_TARGETING_PER_NODE) res[nid] = nid
		}
		return res;
	}

	// indicate that given nid is undergoing an operation
	targetNode(nid, cnt=1) {
        if(!this._targeting) this._targeting = {};
        if (!this._targeting[nid]) this._targeting[nid] = 0;
        this._targeting[nid] += cnt;
        if(this._targeting[nid] <= 0) delete this._targeting[nid];
    }

    // indicate that a node is no longer under targeting
	untargetNode(nid, cnt=1){
        this.targetNode(nid, -cnt);
	}

	// return the targeted nodes
	targetedNodes(){
    	if(!this._targeting) this._targeting = {};
    	return this._targeting;
	}

	// start a thread to ask appropriate nodes for the resource
	_getResourceAskThread(op){
    	let $this = this;
		// retrieve a node to ask for the resource
		let nid = Object.keys(op.resourceNodes)[0];
		let node = op.resourceNodes[nid];
		// remove the node from the list to nodes to be processed
		delete op.resourceNodes[nid];
		op.askedNodes[nid] = true;
		let threadops = this._threadops;
		// ask this particular node for the resource
		return this.nodeRequest(node, { key : op.key} ).then(function(trAnswer){
			if(!trAnswer.document) return;
			if(op.check){
				let prom = $this.checkDocumentValidity({
					document : trAnswer.document
				}).then(function(isValid){
					if(isValid){
						threadops.resolveOperation(op.id, trAnswer.document);
					}
				})
				return prom;
			}
            threadops.resolveOperation(op.id, trAnswer.document);
		})
		.catch(function(){});
	}

    ////////////////////////////////////////////////////////////////////
    //////////////       FILE CUTTING METHODS          /////////////////
    ////////////////////////////////////////////////////////////////////

    /** given a file object (DOM elemnt for browser or path string for nodejs), cut it and transform it into frp document(s)
	 * for browser, file can be get with document.getElementById("id").files[0]
     */
	cutAndStore(file, opts={}){
		let $this = this;

		// build properties to cut the file
        opts.chunkSize = opts.chunkSize || +Infinity;
        opts.oneChunkLimit = opts.oneChunkLimit || opts.chunkSize;
        // prefix of the key of the new document to create
		opts.prefix = opts.prefix || "";
        // keys to sign the top document
        opts.keys = opts.keys || this._frf.createKeys();
		// new signtools to append to the document : the document will be considered a subSky
        opts.signtools = opts.signtools || null;

		// promise result
		let prom = new Promise(r=>opts.resolver=r);

		// for nodejs, file is a path string
		if(typeof file === "string"){
			opts.name = path.basename(file);
			let fileStats = fs.statSync(file);
			opts.size = fileStats.size;
			let mimeType = this._mime.getExtension(file);
			let mimeParts = mimeType ? mimeType.split("/") : [];
            opts.type = mimeParts[0] || "application";
            if(mimeParts[1]) opts.subtype = mimeParts[1]|| "octet-stream";

            this._fs.open('file', 'r', function(status, fd) {
                opts.getFilePart = function(start, length){
                    length = Math.max( 0, Math.min( length, opts.size - start));
                    let buffer = Buffer.alloc(length);
                    let filePartResolver;
                    let promise = new Promise(r=>filePartResolver=r);
                    fs.read(fd, buffer, 0, length, start, function(err, num) {
                        filePartResolver( new Uint8Array(buffer) );
                    });

                    return promise;
                }
                $this._cutAndStoreAux(opts);
            });
		}
		// for TU, provide Uint8Array directly
		else if(file instanceof Uint8Array){
            if(!opts.name) opts.name = "noname";
            if(!opts.type){
            	opts.type = "application";
            	opts.subtype = "octet-stream";
            }
            opts.size = file.length;
            opts.getFilePart = function(start, length){
            	let end = Math.min(file.length, start + length);
            	return Promise.resolve(file.slice(start,end));
            }
            setTimeout(function(){
                $this._cutAndStoreAux(opts);
			},0)
		}
		// for browser, file is  a File object
		else{
			opts.name = file.name.toString();
            opts.size = parseInt(file.size);
            document.opts = opts
            let mimeParts = file.type.split("/") || [];
            opts.type = mimeParts[0] || "application";
            if(mimeParts[1]) opts.subtype = mimeParts[1]|| "octet-stream";
            var reader = new FileReader();
            reader.onload = function() {
                var arrayBuffer = this.result;
                opts.getFilePart = function(start, length){
                	length = Math.max( 0, Math.min( length, arrayBuffer.byteLength - start));
                	return Promise.resolve( new Uint8Array(arrayBuffer, start, length) );
				}
				$this._cutAndStoreAux(opts);
            }
            reader.readAsArrayBuffer(file);
		}
		return prom;
    }

    /***
	 * cut and store a file, given a method that can return Uint8Arrays part of the file
	 * resolve by calling data.resolve() once it is done
     */
	_cutAndStoreAux(data){
		let $this = this;

		// aux function to trigger a promise that retrieve a part of the file and create a chunk with it
		let pushNextChunk = function(){
			if(fileIndex >= data.size){
                data.resolver(signed_mainDoc);
                return;
            }
            return data.getFilePart(fileIndex, data.chunkSize).then(function(filePart){
                fileIndex += data.chunkSize;
                data.storedFileSize = Math.min(data.size, fileIndex); // indicate the length that have been stored after this step
            	let prefixKey = data.key;
            	let subKeyBytes = $this._flaptools.intToBytes(nextSubKey++);
            	let subKeyhex = $this._flaptools.bytesToHex(subKeyBytes);
            	let key = prefixKey + subKeyhex;
                let doc = {
                	key : key,
					prefix : prefixKey.length/2,
					body : filePart
				}
				let signingKeys = data.signtools || data.keys;
				let signedDoc = $this._frf.createSignedDocument(doc, signingKeys);
                let documentBytes = $this._frf.documentToBytes(signedDoc);
                return $this.pushResourceLocal(key, documentBytes).then(pushNextChunk)
			})
		}

        // prepare the main document
        let mainDoc = {
            type : data.type || "bytes",
			subtype : data.subtype,
            name : data.name,
        }
        if(data.prefix) mainDoc.prefix = data.prefix;
        if(data.signtools) mainDoc.signtools = data.signtools;
        if(data.author) mainDoc.author = data.author;
        if(data.expire) mainDoc.expire = data.expire;
        if(data.wingspan) mainDoc.wingspan = data.wingspan;
        if(data.hasOwnProperty("dversion")) mainDoc.dversion = data.dversion;
        if(data.time) mainDoc.time = data.time;
        if(data.nextfeather) mainDoc.nextfeather = data.nextfeather;
        if(data.featherlength) mainDoc.featherlength = data.featherlength;
        if(data.winglength) mainDoc.winglength = data.winglength;
        if(data.keystart) mainDoc.keystart = data.keystart;
		if(data.key) mainDoc.key = data.key;

        let headChunkOnly = data.size <= data.oneChunkLimit;
		if(headChunkOnly){}
		else{
			mainDoc.winglength = data.size;
            mainDoc.wingspan = this._flaptools.bytesToHex( this._flaptools.intToBytes( Math.floor(data.size / data.chunkSize) ) );
            mainDoc.featherlength = data.chunkSize;
		}
        let fileIndex = headChunkOnly ? data.size : 0;
        let nextSubKey = 0;
        let bodyPromise = headChunkOnly ? data.getFilePart(0, data.size) : Promise.resolve();
        let isSkyRoot = !data.prefix;
		if(isSkyRoot) mainDoc = this._frf.createSky(mainDoc, data.keys);
		let signed_mainDoc;
        return bodyPromise.then(function(body){
        	if(headChunkOnly) mainDoc.body = body;
            signed_mainDoc = $this._frf.createSignedDocument(mainDoc, data.keys, isSkyRoot, data.keystart);
            let documentBytes = $this._frf.documentToBytes(signed_mainDoc);
            data.storedFileSize = fileIndex; // indicate the length that have been stored after this step
			data.key = signed_mainDoc.key || mainDoc.key;
            return $this.pushResourceLocal(data.key, documentBytes)
			.then(function(){
				// first callback when head is pushed
				return pushNextChunk();
			});
		})
	}

	////////////////////////////////////////////////////////////////////
	//////////////               WAREHOUSE             /////////////////
	////////////////////////////////////////////////////////////////////
	
	// warehouse are instances that can provide/store resources
	
	addWarehouse(w){
    	if(this.hasWarehouse(w)) return;
		this._warehouse.push(w);
        // create an id for warehouse watch
        if(!this._whWatchId) this._whWatchId = this.createTid(64);
        // add a watch event to redirect to the notify events
		w.watch({
			callback : this._whWatchEvent.bind(this),
			id : this._whWatchId
		})
    }
	
	removeWarehouse(w){
		let i = this._warehouse.indexOf(w);
		if(i === -1 ) return;
		this._warehouse.splice(i,1);
		// stop watching for new keys in the warehouse
		w.unwatch(this._whWatchId);
	}
	
	getWarehouses(){
		return this._warehouse.slice();
	}

	// check if a warehouse is already in frp
	hasWarehouse(w){
        return this._warehouse.indexOf(w) !== -1;
	}

	// watch event for warehouse
	_whWatchEvent(keys){
		for(let key of keys){
			this.triggerForeignNotify(key);
		}
	}

	////////////////////////////////////////////////////////////////////
	///////////////////// CRON AND REGISTRATIONS   /////////////////////
	////////////////////////////////////////////////////////////////////
	
	// method to start/stop the cron
	startCron(period=null){
		if(!period) period = this.cronPeriod;
		if(this._cronId !== null) return;
		this.cron_id = setInterval(this.cron.bind(this), period);
	}
	
	stopCron(){
		if(this._cronId === null) return;
		clearInterval(this._cronId);
		this._cronId = null;
	}
	
	// cron method to update internal frp state
	cron(){
    	let now = Date.now();
		for(let nid in this.busyNodes){
			if(this.busyNodes[nid] < now) delete this.busyNodes[nid];
		}
	}

	////////////////////////////////////////////////////////////////////
	///////////////////   NODE MANAGEMENT             //////////////////
	////////////////////////////////////////////////////////////////////
	
    // check if a node is  busy and should not be asked data
    isBusy(nid, busyUntil=null) {
    	if(busyUntil){
    		this._busyNodes[nid] = busyUntil;
        }
        return (this._busyNodes[nid] && this._busyNodes[nid] > Date.now())
    }

    // retrieve the list of busy nodes
	busyNodes(){
		this._rebuildBusyNodes();
		let res = {};
		for(let i in this._busyNodes) res[i] = true;
		return res;
	}

	_rebuildBusyNodes(){
    	this._busyNodesArray.length = 0;
        for(let nid in this._busyNodes){
            if(!this.isBusy(nid)) delete this._busyNodes[nid];
        }
	}

	// // add a node
	// addNode(n){
	// 	this._nodes.push(n);
	// }
	//
	// removeNode(n){
	// 	let i = this._nodes.indexOf(n);
	// 	if(i!==-1) n.splice(i,1);
	// }

	// retrieve the list of unrestrained nodes
	currentUnrestrainedNodes(){
		return this._nodes.filter(n=>!n.restrained);
	}
	
	// check if there is enough room to set a node to a given state
	canUnrestrain(){
		return this.currentUnrestrainedNodes().length < this.maxUnrestrained;
	}
	
	// remove dead nodes whose interface is closed
	clenDeadNodes(){
		this._nodes = this._nodes.filter(function(n){
			return n.interface.status() === n.interface.RUNNING;
		});
	}
	
	/////////////////////////////////////////////////////////////////////
	///////////////    REGISTER / NOTIFY METHODS     ////////////////////
	/////////////////////////////////////////////////////////////////////
	
	// add a registration, given the transaction data
	addLocalRegistration(data, node, local=true){
		let tid = data.tid;
		this.localRegistrations[tid] = {
			tid : tid,
			keys : data.keys || [],
			node : node,
			onNotify : data.onNotify,
			onCancel : data.onCancel || null
		};
	}
	addForeignRegistration(data, node, callback){
		let tid = data.tid || Math.floor(Math.random() * 1000000000);
		this.foreignRegistrations[tid] = {
			tid : tid,
			keys : data.keys || [],
			node : node,
			callback : callback
		};
		return tid;
	}
	
	// remove a registration, given a tid
	removeLocalRegistration(tid){
		if(this.localRegistrations[tid]) delete this.localRegistrations[tid];
	}
	removeForeignRegistration(tid){
		if(this.foreignRegistrations[tid]) delete this.foreignRegistrations[tid];
	}
	
	// retrieve the list of registations that match a given key
	triggeredRegistrations(key, registrations){
		let res = [];
		for(let tid in registrations){
			let reg = registrations[tid];
			if( this.isKeyRegistered(key, reg) ) res.push(reg);
		}
		return res;
	}
	
	// check if a key is included in a given registration
	isKeyRegistered(key, registration){
		let rkeys = registration.keys || [];
		for(let rkey of rkeys){
			let prefixes = this.buildKeys(rkey.prefix, rkey.suffixes || [] );
			// add a key limited to the prefix
			prefixes.push(rkey.prefix || "");
			for(let prefix of prefixes){
				if( this._flock.isIncluded( key, {prefix:prefix, span : rkey.span||0} ) ) return true;
			}
		}
		// by default, no key match, return false
		return false;
	}

	// perform a retroactive notification about keys that are already available
	retroNotify(node, reg){
		let res = [];
		// for each key, retrieve local resources keys
		let retrieveKeyPromises = [];
		for(let key of reg.keys){
			let opts = key.hasOwnProperty("dversion") ? {dversion : key.dversion} : {};
            retrieveKeyPromises.push( this.getResourceKeysLocal(key.prefix, opts) );
		}
		let $this = this;
		Promise.all(retrieveKeyPromises).then(function(promiseResults){
			let resultKeys = [].concat(...promiseResults);
			if(!resultKeys.length) return;
            let data = {
                tid : reg.tid,
                keys : resultKeys,
            };
            $this.nodeNotify(node, data);
		});
	}

	// filter keys uniquely and factorize them with a list of given factors
	filterAndFactorize(keys, prefixFactors) {
		// build a list of keys per version and prefix
    	let factorizedKeys = {};
    	for(let prefix of prefixFactors) factorizedKeys[prefix] = {};

    	for(let key of keys){
    		for(let prefix in factorizedKeys){
    			if(!key.prefix.startsWith(prefix)) continue;
    			let version = key.dversion || 0;
    			let suffix = key.prefix.substr(prefix.length);
    			if( !factorizedKeys[prefix][version] ) factorizedKeys[prefix][version] = {};
                factorizedKeys[prefix][version][suffix] = suffix;
    			break;
			}
		}
		// compute the result
		let result = [];
    	for(let prefix in factorizedKeys){
    		for(let version in factorizedKeys[prefix]) {
                result.push({prefix: prefix, dversion: version, suffixes: Object.keys(factorizedKeys[prefix][version])});
            }
		}
		return result;
	}

	// send notifications to all registred nodes triggered by a key
	triggerForeignNotify(key){
		// retrieve the list of registration trigger by the key

		let registrations = this.triggeredRegistrations(key, this.foreignRegistrations);
		for( let reg of registrations){
			let data = {
				tid : reg.tid,
				keys : [key],
			};
			if(reg.node) this.nodeNotify(reg.node, data);
			if(reg.callback) reg.callback(data);
		}
	}
	
	// perform execution of callbacks of all local triggered registrations
	triggerLocalNotify(tid, keys, node){
		// retrieve the registration data
		let reg = this.localRegistrations[tid];
		if(!reg) return; 
		// build the data that will be given as callback
		let data = {
			tid : tid,
			keys : keys,
			node : node // the node that emitted the notification
		}
		reg.onNotify(data);
	}
	
	// cancel a registration emitted by this frp, and send a cancel message to the matching node
	cancelLocalRegistration(tid){
		this._cancelRegistration(tid, this.localRegistrations);
	}
	
	// cancel a foreign notification and send a cancel message to the remote node
	cancelForeignRegistration(tid){
		this._cancelRegistration(tid, this.foreignRegistrations);
	}
	
	// auxiliar function to cancel notification
	_cancelRegistration(tid, registrations){
		let reg = registrations[tid];
		if(!reg) return;
		delete registrations[tid];
		this.nodeCancel( reg.node, {tid : reg.tid});
	}

	/////////////////////////////////////////////////////////////////////
	//////////////////////    TRANSACTIONS           ////////////////////
	/////////////////////////////////////////////////////////////////////
	
	// request a resource on a given node
	nodeRequest(node, opts={}){
		let tr = this._prepareTransaction(node);
		
		// add the transation to watch for an answer
		this.addTransaction(tr);
		
		// send the request message
		let msg = {
			tid : tr.tid,
			key : opts.key,
			type : "request"
		}
		this.send(node, msg);

		let result = tr.promise;
		if(opts.timeout){
			result = this.race([result], {timeout : opts.timeout});
		}

		let $this = this;
		// target the node, untarget it when the request is over
        this.targetNode(node.id);
        let callback = function(){ $this.untargetNode(node.id)}
        result.then(callback).catch(callback);

		return tr.promise;
	}
	
	// register to a foreign node
	nodeRegister(node, data={}){
		let tr = this._prepareTransaction(node);
		
		// ensure a callback is provided for incoming notifications
		if(data.onNotify) tr.onNotify = data.onNotify;
		else throw "Mandatory callback for register operation was not provided under key 'onNotify'";
		// add facultative callback for cancel
		if(data.onCancel) tr.onCancel = data.onCancel;

		
		// add the transation to watch for an answer
		this.addTransaction(tr);
		
		// store the transaction as a local registration
		this.addLocalRegistration(tr, node);
		
		// send the request message
		let msg = {
			tid : tr.tid,
			keys : data.keys,
			type : "register",
			retro : !!data.retro
		}
		this.send(node, msg);
		
		return tr.tid;
	}
	
	// send a notification about a key to a given node
	nodeNotify(node, data={}){
		let msg = {
			tid : data.tid,
			keys : data.keys,
			type : "notify"
		}
		this.send(node, msg);
	}
	
	// send a cancel message to a node
	nodeCancel(node, data = {}){
		let msg = {
			tid : data.tid, 
			type : "cancel"
		};
		this.send(node, msg);
	}
	
	// prepare a transaction object, creating an object with the following :
	// promise : a promise that resolve or reject once the transaction answer is received
	// reject : function to reject the transaction promise
	// resolve : function to reject the transaction promise
	// node : the node targeted by the transaction
	// tid : a new local tid for the transaction
	_prepareTransaction(node, opts){
		let transaction = {
			tid : this.createTid(),
			node : node,
		};
		let prom = new Promise((res,rej)=>{
			transaction.resolve = res;
			transaction.reject = rej;
		});
		transaction.promise = prom;
		return transaction;
	}
	
	// build the answer of a request
	_processRequest(rq, operationData={}){
		let opts = {
			timeout : 2000, // timeout to prevent warehouse from freezing forever
		};
		let $this = this;
		if("dversion" in rq.key) opts.dversion = rq.key.dversion;
		let answer = {
			type : "answer",
			tid : rq.tid
		};
		// retrieve the list of feeding keys that are concerned y the request
        operationData.operationKey = this.retrieveFeedOperation(rq.key.prefix || "");
        operationData.requests = (operationData.requests || 0) + 1; // notify that one requests has been processed under the feedKey span

		// retrieve the resource and then send it if available
		return this.getResourceLocal(rq.key.prefix, opts).then(function(doc){
			answer.status = $this.STATUS_ANSWERED;
			answer.document = $this._frf.documentToObject(doc);
			return answer;
		})
		.catch(function(e){
			answer.status = $this.STATUS_NOT_FOUND;
			return answer;
		});
	}
	
	// process an answer message
	_processAnswer(answer, event){
		// check that the transaction id match an existing one
		let tid = answer.tid;
		let tr = this.getTransaction(tid);
		if(!tr) return;
		tr.resolve(answer);
		this.removeTransaction(tid);
	}
	
	// process a registration message
	_processRegister(tr, event){
		let node = this._flock.nodeFromInterface(event.interface);
		this.addForeignRegistration(tr, node);
		// if asked for, perform a retro notification with documents already available
        if(tr.retro) this.retroNotify(node, tr);
	}
	
	// build the answer of a notify 
	_processNotify(tr, event){
		let node = this._flock.nodeFromInterface(event.interface);
		this.triggerLocalNotify(tr.tid, tr.keys, node);
	}
	
	// process a cancel message
	_processCancel(tr, ev){
		let tid = tr.tid;
		// check if the cancel is associated to a local registration
		if(this.localRegistrations[tid]){
            let cancel = this.localRegistrations[tid].onCancel;
            delete this.localRegistrations[tid];
            if(cancel) cancel();
		}
		// check if the cancel is associated to a foreign registration
		else if(this.foreignRegistrations[tid]){
			delete this.foreignRegistrations[tid];
		}
	}
	
	// process a busy
	_processBusy(tr, event){
    	// retrieve the node from the interface
		let nodeInterface = event.interface;
		let node = this._flock.nodeFromInterface(nodeInterface);
		if(!node ) return;
		let nid = node.id;
        if(!nid) return;
		let busyTime = tr.time || 0;

		this.isBusy(nid, busyTime + Date.now() );
    }
	
	// send a busy message : the frp should not be called by its target for the given amount of time
	sendBusy(node, busyTime = 10000){
		let msg = {
			type : "busy",
			time : busyTime
		}
		this.send(node, msg);
	}
	
	// send a message to a node
	send(node, msg){
		let bytes = this.messageToBytes(msg);
		var $this = this;
		this._flock.connectNode(node).then(function(nodeInterface){
			if($this.stopped) return;
			nodeInterface.send(bytes, $this.frpPort, $this.frpPort);
		});
	}
	
	processTransaction(event){
		// extract the transaction
		let tr = this.messageToObject(event.message);
		// toggle the tid to transform it according the this node perspective
		// tr.tid = this.toggleTid(tr.tid);
		let answerPromise;

        // create an object to store the operation to update (increment the bandwidth used)
		let operationData = {};

		try{
			switch(tr.type){
				case "request" :
					answerPromise = this._processRequest(tr, operationData);
					break;
				case "answer" :
					this._processAnswer(tr);
					break;
				case "register" :
					this._processRegister(tr, event);
					break;	
				case "notify" :
					this._processNotify(tr, event);
					break;
				case "cancel" : 
					this._processCancel(tr,event);
					break;
				case "busy" :
					this._processBusy(tr, event);
					break;
				default : break;
			}
			
			// send answer when there is one
			if(!answerPromise) return;
			let $this = this;
			answerPromise.then(function(answer){

				let answerBytes = $this.messageToBytes(answer);
				if($this.stopped ) return;
                operationData.bandwidth = (operationData.bandwidth || 0) + answerBytes.length;
				if(operationData.operationKey) $this.updateOperationStats(operationData.operationKey, operationData);
				event.interface.send(answerBytes, event.fport, $this.frpPort);
			});
		}
		catch(e){ console.log(e) }
	}
	
	// add a pending request
	addTransaction(tr={}){
		if(!tr.tid) req.tid = this.createTid();
		this.transactions[tr.tid] = tr;
		if(!tr.startTime) tr.startTime = Date.now();
		return tr.tid;
	}
	
	getTransaction(tid){
		return this.transactions[tid] || null;
	}
	
	removeTransaction(id){
		if(this.transactions[id]) delete this.transactions[id];
	}
	
	/////////////////////////////////////////////////////////////////////
	//////////////////   ENCODING AND DECODING         //////////////////
	/////////////////////////////////////////////////////////////////////

	// convert a frp message to its byte representation
	messageToBytes(o){
		if(!o.tid) o.tid = this.createTid();
		switch(o.type){
			case this.TYPE_REQUEST :
			case "request" : 
				return this.requestToBytes(o);
			case this.TYPE_ANSWER :
			case "answer" : 
				return this.answerToBytes(o);
			case this.TYPE_REGISTER :
			case "register" : 
				return this.registerToBytes(o);
			case this.TYPE_NOTIFY :
			case "notify" : 
				return this.notifyToBytes(o);
			case this.TYPE_BUSY :
			case "busy" : 
				return this.busyToBytes(o);
			case this.TYPE_CANCEL :
			case "cancel" : 
				return this.cancelToBytes(o);
			default :
				throw "unknown type for conversion "+o.type;
		}
	}
	
	// convert a frp byte message to its object representation
	messageToObject(b){
		return this._flaptools.decode(b, this.TEMPLATE_FRP_MESSAGE);
	}
	
	requestToBytes(o){
		let data = this._copyProperties(o, ["version","tid", "key"]);
		data.type = "request";
		return this._flaptools.encode(data, this.TEMPLATE_FRP_MESSAGE);
	}
	
	answerToBytes(o){
		let data = this._copyProperties(o, ["version","tid", "document", "status"]);
		data.type = "answer";
		return this._flaptools.encode(data, this.TEMPLATE_FRP_MESSAGE);
	}
	
	registerToBytes(o){
		let data = this._copyProperties(o, ["version","tid", "keys", "retro"]);
		data.type = "register";
		return this._flaptools.encode(data, this.TEMPLATE_FRP_MESSAGE);
	}
	
	notifyToBytes(o){
		let data = this._copyProperties(o, ["version","tid", "keys"]);
		data.type = "notify";
		return this._flaptools.encode(data, this.TEMPLATE_FRP_MESSAGE);
	}

	busyToBytes(o){
		let data = this._copyProperties(o, ["version","tid", "time"]);
		data.type = "busy";
		return this._flaptools.encode(data, this.TEMPLATE_FRP_MESSAGE);
	}
	
	cancelToBytes(o){
		let data = this._copyProperties(o, ["version","tid"]);
		data.type = "cancel";
		return this._flaptools.encode(data, this.TEMPLATE_FRP_MESSAGE);
	}
	
	_copyProperties(input, props, target={}){
		for(let prop of props){
			if(input.hasOwnProperty(prop)){
				target[prop] = input[prop];
			}
		}
		return target;
	}
	
	/////////////////////////////////////////////////////////////////////
	//////////////////   HELPERS                       //////////////////
	/////////////////////////////////////////////////////////////////////

	// build all the keys out of an hexadecimal prefix and a suffixes array
	buildKeys(prefix, suffixes){
		if(!suffixes) suffixes = [""];
		let res = [];
		for(let suffix of suffixes){
			if(suffix.length%2) suffix = "0"+suffix;
			res.push(prefix+suffix); 
		}
		return res;
	}
	
	createTid(length=this.TID_LENGTH){
		//if(length === null) length = this.TID_LENGTH;
		return this._flaptools.randomHex(length);

		let b = new Uint8Array(length);
		for(let i=0; i<length; i++) b[i] = Math.random()*256 % 256;
		if(length) b[0] ^=127;
		return this._flaptools.bytesToHex(b);
	}
	
	// // check if a tid has been generated on a local node or a remote node
	// isTidLocal(tid){
	// 	let b = typeof tid === "string" ? this._flaptools.hexToBytes(tid) : tid;
	// 	return !(b[0] & 128);
	// }
	
	// perform a not operation on first bit of an hexadecimal
	toggleTid(hex){
		let b = this._flaptools.hexToBytes(hex);
		b[0] ^= 128;
		return this._flaptools.bytesToHex(b);
	}
	
	// race an array of Promise : resolve with the first resolution or reject with the last rejection
	race(promises, opts={}){
		let data = {
			nprom : promises.length,
			done : false
		}
		let promRes = new Promise((res,rej)=>{
			data.res = res;
			data.rej = rej;
		});
		let raceResolve = function(v){
			if(data.done) return;
			else data.done = true;
			data.res(v);
		}
		let raceReject = function(v){
			data.nprom--;
			if(data.nprom) return;
			else data.rej(v);
		}
		for(let prom of promises){
			prom.then(raceResolve).catch(raceReject);
		}
		// timeout option : reject when too much time has passed
		if(opts.timeout){
			setTimeout(function(){
                data.rej("RACE TIMEOUT")
			},opts.timeout)
		}

		return promRes;
	}
	
	
	_initConstants(){
		this.TID_LENGTH = 8;
		this.STATUS_ANSWERED = 1;
		this.STATUS_ERROR = 100;
		this.STATUS_NOT_FOUND = 101;

		this.MAX_TARGETING_PER_NODE = 4;
	}

	_initNodejs(){
		// for nodejs environment, loads some modules
		try{
			this._fs = require("fs");
            this._path = require("path");
            this._mime = require("mime");
		}
		catch(e){}
	}
	
	initInterface(ni){
		ni.addHook(ni.e.RECEIVE, this.processTransaction.bind(this), this.frpPort );
	}
	
	_initTemplates(){
		
		// store various message types
		this.TYPE_REQUEST = 1;
		this.TYPE_ANSWER = 2;
		this.TYPE_REGISTER = 3;
		this.TYPE_NOTIFY = 4;
		this.TYPE_BUSY = 5;
		this.TYPE_CANCEL = 6;
		this.TRANSLATION_TYPE = {
			[this.TYPE_REQUEST] : "request",
			[this.TYPE_ANSWER] : "answer",
			[this.TYPE_REGISTER] : "register",
			[this.TYPE_NOTIFY] : "notify",
			[this.TYPE_BUSY] : "busy",
			[this.TYPE_CANCEL] : "cancel"
		};
		
		// set message status 
		this.STATUS_ANSWERED = 1;
		this.STATUS_ERROR = 100;
		this.STATUS_NOT_FOUND = 101;
		this.STATUS_UNAUTHORIZED = 102;
		this.STATUS_TIMEOUT = 103;
		this.TRANSLATION_STATUS = {
			[this.STATUS_ANSWERED] : "ANSWERED",
			[this.STATUS_ERROR] : "ERROR",
			[this.STATUS_NOT_FOUND] : "NOT_FOUND",
			[this.STATUS_UNAUTHORIZED] : "UNAUTHORIZED",
			[this.STATUS_TIMEOUT] : "ANSWER_TIMEOUT",
		}
		
		// build EE for frp keys
		let ee_frp_key = [
			{ n : "prefix", t : "hex", c : 8, d : ""},
			{ n : "span", t : "int", c : 9},
			{ n : "suffixes", t : "arr", c : 10, tpl :  { n : "suffix", t : "hex", c : 12} },
			{ n : "dversion", t : "int", c : 11}
		];
		let eeo_key={};
		for(let e of ee_frp_key) eeo_key[e.n]=e;
		this.TEMPLATE_FRP_KEY = {
			ee : eeo_key
		};

		// build EE for frp messages
		let ee_frp_messages = [
			{ n : "version", t : "int", c : 0, d : 0},
			{ n : "length", t : "int", c : 1},
			{ n : "type", t:"int", c : 2, rpl : this.TRANSLATION_TYPE },
			{ n : "tid", t : "hex", c : 3},
			{ n : "key", t : "dict", c : 4, tpl : this.TEMPLATE_FRP_KEY},
			{ n : "document", t : "dict", tpl : this._frf.TEMPLATE_DOCUMENT, c : 5},
			{ n : "status", t:"int", c : 6, rpl : this.TRANSLATION_STATUS },
			{ n : "keys", t : "arr", c : 7, tpl : this.TEMPLATE_FRP_KEY},
            { n : "retro", t : "bool", c : 12},
            { n : "time", t : "int", c : 13},
		];
		let eeo={};
		for(let e of ee_frp_messages) eeo[e.n]=e;
		this.TEMPLATE_FRP_MESSAGE = {
			ee : eeo
		};
	 }
}

export {
    Frp
}