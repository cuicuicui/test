<!---
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->
# About Extensible entries

Flock standards are supposed to work through multiple environment, not only those based in javascript. For that purpose, we define a way to translate environment structure into universally readable byte format.
Serialization is the process of transforming an object into bytes following a given template.
Since most of flock protocols will evolve upon time, we define a data format that can allow extensions without breaking retrocompatibility.
The spirit is to define a set of core properties, and make additional properties in later versions facultative.

A dictionnary is a set of key values properties. Those key=>value properties are called Extensible entries (EE) since they are designed to make the data format evolutive.

# Extensible entry format

an extensible entry (EE) map a keyname (recommended ascii name) to a value, encoded in the following way :
- 1 bit : custom key name flag
- 2 bits : value length / value length length : 
			0 -> the value is stored on one byte
			1 -> the value length is stored on one byte
			2 -> the value length is stored on two bytes
			3 -> the value length is stored on eight bytes
- 5 bits : - if custom key flag === 1 : key name byte length TL
		   - if custom key flag === 0 : official key defined by an EE code "c"
- TL bytes if custom key flag === 1 : key name represented as ascii bytes 
- 0, 1, 2 or 8 Bytes : the value length VL
- 1 or VL bytes : the value 

# EE ipmlementation 

When implementing the EE standard, it is advise to follow the following notations, even though only transmitted data to peers really matters.

## official EE

Official EE keys are a way to lighten the weight of serialized data, by defining a key with a code rather than a ascii string.

### EE definitions
EE definitions should be set locally to analyze data prior to exchanging data.
EE definitions should be objects with the following properties (* for mandatory) : 
- **n*** (ascii) : name of the key
- **c** (int) : official ee code (must be comprised between  0 and 31 (5 bits integer)
- **t*** ("ascii" | "int" | "hex" | "bin" | "bool" | "dict") : types expected when encoding, and type used to represent data when decoding : types are described below
- **r** (boolean) : required field (default is false) : if the field is requiered and not present, it is encoded/decoded with default "d", if no default is provided, exception is thrown
- **d** (mixed) : default value for encoding/decoding,
- **de** (mixed) : default, but only when encoding,
- **dd** (mixed) : default, but only for decoding,
- **tpl** (dictionnary) : template : when type is "dict" or "arr", provide a dictionnary definition to serialize/unserialize its elements
- **rpl** (object) : replace : object that map a code => user value to replace integer values that represents codes by their meaning during encode/decode
- **rpli** (object) : replace inverse, computed automatically from rpl if rpl is provided but rpli is not
- **macro** (function || "string" : special method or flaptools method to calculate the byte value. arguments are value(mixed), ee(object), macroData(object)
- **endMacro** (function || string : method executed at the end of encoding, to modify the value already stored : arguments bytes(Uint8Array), ee, startIndex, length
} 

### EE types
Types are defined under the key "t" of EE definition, and can take one of the following value :
- **ascii** : an ascii string
- **utf8** : an utf8 string
- **json** : an object that is transferred as the utf8 bytes of its string representation
- **int** : an integer (< 2^54)
- **hex** : a string of hexadecimal characters : always output an even value length, but accept odd length as input
- **bin** : an Uint8Array
- **bool** : a boolean (value is read on first byte : 0/empty => false, else true)
- **dict** : an object whose properties are EE
- **arr** : an array of dict element or ee element of the same template


Arrays values relies on what is called extensible integers (EI) that have the following format :
- 1 bit : extension flag
- 7 bits : integer value if extension flag is false, integer byte length L if flag is true
- L bytes : integer value if extension flag is true

An array value has the following format :
- EI for array number of elements N
- for each of N value :
	- EI for dict length DL
	- DL bytes of dict value

## Dictionnary

A dictionnary is a concatenation of EE
Dictionnaries are object with the following format : 
{
	**ee*** (Object) : dictionnary of EE to be encoded/decoded, keys beeing the name "n" of the EE
	**maxLength** (int) : maximum length taken into account for encoding or decoding 
	**allowUndefined** : "none" | "decode", // allow to decode keys that are not listed in ee (default is "none")
	**maxKeys** (int) : default is 64, number of keys that can be encoded/decoded before throwing an exception
}

## Good practice

Some ee code should be reserved for specific purposes :
- 0 : version
- 1 : length
