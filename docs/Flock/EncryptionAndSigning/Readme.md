<!---
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->
# Description

Several components in flock standards needs to encrypt or hash data. This section provides specifications for the various methods.

# Encryption and hashing codes

The different methods may be reffered with integer codes, provided below :


| name | encryption/hashing | code | description |
| ---- | ------------------ | ---- | ----------- |
| ENCTYPE_NONE | encryption | 0 | encryption that leave the data as is. |
| ENCTYPE_DH_2048_256 | encryption | 1 | Diffie-Hellman with 2048b prime and 256bit prime subgroup, defined in rfc 5114 |
|ENCTYPE_RSA_1024| encryption | 16 | RSA with a 1024 bits key|
|ENCTYPE_RSA_2048| encryption | 17 | RSA with a 2048 bits key|
|ENCTYPE_RSA_4096| encryption | 18 | RSA with a 4096 bits key|
| HASHTYPE_SHA_256 | hashing | 5 | sha 256 

