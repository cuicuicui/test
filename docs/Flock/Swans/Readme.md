<!---
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->
# Description

Swans (**S**ignaling **W**ith **AN**onmous **S**servers) is a protocol to allow two nodes to communicate indirectly, with a third party node transmitting the messages but unable to read those.
It was mainly designed to allow webrtc signaling in a flock architecture. Several components form the swan architecture :

- **the receiver** : this component listen from incoming messages. 
It is linked to a swans server that act as an intermediate.
When it starts, it generates an public and a private key : for the moment only the ENCTYPE_DH_2048_256 is supported.

- **the caller** : this components initiates connection with a receiver. To do so, it sends its packets to the swans server, with a specified receiver id to forward requests.

- **the server** : it is a third party component. It defines an interface to receive message from callers, and forward them to the registered receivers.
Receivers register through another interface.

# Data format

Data is transmited as serialized Extensible Entries. Since the data must only be readable by the caller and receivers, the main data is encrypted.
Two EE dictionnaries are used for swans protocols. The body, which contains the main data and is only transmited once encrypted, and the message, which are intermediate messages exchanged between swan servers and receivers/callers.

## EE for swans messages

The message dictionnary is constituted of the following EE :

- { n : "version", t : "int", c : 0, dd : 0}
- { n : "length", t : "int", c : 1}
- { n : "type", t : "int", c : 2, r : true }
- { n : "tid", t : "hex", c : 3, r : true }
- { n : "receiver", t : "hex", c : 4 }
- { n : "key", t : "bin", c : 5 }
- { n : "body", t : "bin", c : 6 }
- { n : "authkey", t : "hex", c : 7 }
- { n : "canals", t : "arr", c : 8  tpl : [canals](../Layers/TransportAbstraction/Readme.md)}
- { n : "info", t : "ascii", c : 9 }

**type** : type of message sent between the server and a caller/receiver. It can be one of the following : 

| type | code | description |
| ---- | ---- | ----------- |
| ping | 0 | a simple ping message. If received, a Ping answer should be emitted to notify the other and is still alive and responding |
| request | 1 | a request message emitted by a caller. such message should define a receiver id so that the server transmit it to the registered receiver. |
| requestAnswer | 2 | a message emitted by a receiver after processing the data inside a request message. The body should be encrypted with the "key" parameter defined in the request message. |
| register | 3 | message sent by a receiver to be registered by a server.|
| registerAnswer | 4 | message sent by a server to a receiver as an answer for a register message. After that, the server will transmit incoming request messages that specify the receiver as the recipient|
| unregister | 5 | message sent by a receiver to its server to cancel the registration. |
| pingAnswer | 6 | answer to a pingMessage |
| error | 7 | a generic error message that can be sent as an answer. additional information can be added in the info EE. If a more specific error code exists, it should be used instead|
| errorForged | 8 | message to notify that a request or requestAnswer provided incorrect encrypted data or key, and that the message should/could not be trusted/parsed |
| errorUnregisteredReceiver | 9 | message sent by the server as an answer for a request message, when the requested receiver id is not registered |

**tid** : transaction identification for an exchange of message. It should be random and 6 byte long. It is defined by the node exchanging the first message (request, ping, register...) and used a second time in the answer (requestAnswer, pingAnswer, registerAnswer).

**receiver** : identification of a receiver. It is the hexadecimal concatenation of the key type to use, and the public key.
This field must be set in request and register messages. For the moment, since only ENCTYPE_DH_2048_256 is defined in standards, all receiver ids should start with "01"

**key** : AES derivated key for encryption and decryption of the body for request and requestAnswer messages. An AES random key is created by the caller by applying the Diffie Hellman algorithm to a random number. Both caller and receiver own a common key. 
AES key is then obtained by applying SHA256 hash to this key. A new key should be created for each couple of request/requestAnswer message.

**body** : this is the main data that is transmitted during request/requestAnswer messages. It is encrypted with AES and only readable by caller/receiver. Once decrypted, it can be parsed as a swans body EE dictionnary.

**authkey** : a key that can be transmitted during register messsages, if the swans server is not public and require a registration key.

**canals** : must be specified in registerAnswer. List the canals that can be used by a swans client to contact the server to forward requests.

**info** : some text explanations that can be sent if needed, notably during generic errors for more precisions.


## EE for swans body

This is the data encrypted during swans request/requestAnswers messages.
The body dictionnary is constituted of the following EE :

- { n : "length", t : "int", c : 1, de : 0}
- { n : "bid", t : "hex", c : 2 }
- { n : "body", t : "bin", c : 3 }

**length** : length of the whole serialized dictionnary. It must be specified since the body is encrypted and thus decrypted value may be smaller than total value.

**bid** : random body id set by the caller. The same bid must be used when the receiver send back a requestAnswer. it ensure that a body packet from the right person

**body** : the actual data securely transmitted with swans.