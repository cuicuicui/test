<!---
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->
# Description

The FRP (Flock Resource Protocols) define ways to exchange frdocs defined in the [FRF specification](../FRF/Readme.md).

A FRP component is linked to one or several warehouses. Warehouses are the components that provides local frdocs.
When a new frdoc is stored to a warehouse, the frp might warn new frp about the new key stored.


# Messages Format 

The FRP components send and receives messages on port 3. Messages are dictionnaries with the following EE :
- { n : "type", t:"int", c : 2}
- { n : "tid", t : "hex", c : 3}
- { n : "key", t : "dict", c : 4, tpl : FRPKEY}
- { n : "document", t : "dict", tpl : FRDOC, c : 5}
- { n : "status", t:"int", c : 6 }
- { n : "keys", t : "arr", c : 7, tpl : FRPKEY}
- { n : "retro", t : "bool", c : 12},
- { n : "time", t : "int", c : 13},

**type** : determine the type of message. Can take a value among the following

| type | code | description |
| ---- | ---- | ----------- |
|REQUEST|1| send a message to a frp to retrieve a document identified by its key (FRPKEY dictionnary). Must specify the key EE
|ANSWER|2| send an answer message (of a request, register or cancel message).
|REGISTER|3| ask a frp about new keys once they are available. If the retro option is set to true, return a notification with already available keys.
|NOTIFY|4| send a message about available keys in the frp. Notify messages are sent to frp when new keys match the one asked in a register message. 
|BUSY|5| warn a frp that the node is too busy to provide resources or answers. Once received, no requests/messages should be addressed to the frp for a given time period
|CANCEL|6| cancel a registration that have been made. A registration can be revoked from both side by sending a cancel message
		
**tid** : transaction id. It is a (at least) 6 byte long random hexadecimal key that identify an exchange of message between two FRP.
tid is set in every frp requestregister/busy messages and must reuse the same in notify/answer/cancel messages

**key** : identify the resource to be fetched in a request message. The FRPKEY dictionnary is described below.		
		
**document** : frdoc returned in an answer messages. It is generally a x-flock/sign document that encapsulate another frdoc whith the relevant data.
Thus, implementation must try to unserialize body of x-flock/sign documents to retrieve actual data.
		
**status** : result of an answer message. All codes above 100 are errors.

| status name | code | description |
| ----------- | ---- | ----------- |
| ANSWERD | 1 | indicates that the initial message has been successfully processed (registration accepted, document found in request message, cancel operation executed)| 
| ERROR | 100 | generic error that indicates a failure | 
| NOT_FOUND | 101 | the document requested couldn't be found |
| UNAUTHORIZED | 102 | the transaction have failed because unauthorized |  
| TIMEOUT | 103 | the transaction failed because of a timeout |  

**retro** : when sending a register message, indicates whether notification of already available matching keys must be made.
If set to false or unset, only send notification when new key are available.

**time** : for busy message, indicates in milliseconds how long the frp should not be distrubed with request or register messages.

**keys** : list of available resources returned by a notify message. Many of them might be returned in a single notify message.
keys EE is an array of FRPKEY, which is described below :



# FRPKEY format 

Request, notify and register messages make use of FRPKEY data template, which is a dictionnary with the following EE :
- { n : "prefix", t : "hex", c : 8, d : ""} 
- { n : "span", t : "int", c : 9}
- { n : "suffixes", t : "arr", c : 10, tpl :  { n : "suffix", t : "hex", c : 12} }
- { n : "dversion", t : "int", c : 11}

**prefix** : prefix of the targeted key(s). For request message, it is the whole key. For register message, it is the prefix of a span of keys to watch for. 
For notify message, the returned keys are obtained by concatenating each suffix of the suffixes array with the prefix.

**span** : for register message, span of the keys to watch for

**suffixes** : list of suffixes of the keys that are returned in a notify message

**dversion** : Specify of the dversion of the frdoc to retrieve in request message. In register message, specify the dversion that are watched for.
		
			
