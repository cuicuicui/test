<!---
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->
# Description

FRF (flock resource format) is a way to encode data, and ensure authenticity by a signing algorithm.

The main concept associated is the document, also called frdoc (flock resource document).

# Frdoc 

## Definitions

#### Frdoc

Alias document. Frdocs are EE dictionnaries which encapsulate some data usable by EndService layer components.

#### Author

The creator of a frdoc

##### Keys

Keys are hexadecimal identifier of a resource. The way they are set is mixed : the prefix is set with an algorithm that ensure a signing mecanism (see signature section)
while the end of a key may be set manually by the author of the frdoc.

#### Sky

In order to acertain documents origins, those have to be signed. 
Author create a "x-flock/sign" outer document, which encapsulate the inner frdoc to be signed. 
The outer frdoc has only a fex properties : it contains the signature, and possibly the inner document key.

The first inner document, also called root document, contains some data called signtools, which define algorithm to compute signature. 
The signature applied to this first inner document is the key of the root document, and is called rootKey. This key is the prefix of all the keys of the subdocuments created below the root document.

All subdocument that are created will use the signtools defined in the root document to compute/check signatures.
All those documents together are called a sky.

#### Subsky

An author may want to delegate creation of frdocs to another contributor. 
He can then create a subdocument that contains the signtools data of the contributor.
This document is still signed by root document signtools, but subdocuments are then signed by the new signtools.
This grant controls of a span of keys of the sky to the contributor, defining de facto the concept of subsky.

#### Wing and feather

Some data like a video are too big to be stored in a single frdoc. The concept of wings allow one to split data of the body into several parts, called feathers.

A frdoc is a wing document if the EE wingspan is defined. A wingspan is the highest suffix a feather can have. 
For instance, if a frdoc is wing with the key "aabb0011", and a wingspan equal to "ff99", feathers can be set with keys from "aabb001100", "aabb001101" ... up to "aabb0011ff98", "aabb0011ff99".

### Frdoc signature

Signature serves to identify whether a frdoc is correct or not. It is computed on the bytes serialization of a frdoc, with the data specified in the signtools EE of the closest parent document.
The signature of the root document defines the common prefix of a sky. A document is correctly signed if :
- it is the root document, it contains a signtools EE, and the signature applied to the serialization of the frdoc is the key of the frdoc
- it is not a root document, but the parent frdoc is correctly signed, and the signature of the serialization match with the one provided.

In addition, if the startwith EE is specified, to be correct a frdoc must have its key match with the startith value.

Signtools are dictionnaries with the following EE :
- {n:"type", t:"int", c:2}
- {n:"n", t:"hex", c:16}
- {n:"e", t:"hex", c:17}
- {n:"d", t:"hex", c:18}

**n** : for RSA algorithm, it is the RSA modulus

**e** : for RSA algorithm, it is the RSA public exponent

**d** : for RSA algorithm, it is the RSA private exponent. It should'nt be shared publicly.

**type** : type of signing method. Codes and methods are detailed below

| name | code | description |
| ---- | ---- | ----------- |
|rsa1024|1    | RSA algorithm with 1024-bits keys. To sign a document, the serialization is hashed with SHA256. The hash is then encrypted with the private key. Document is check by computing SHA256 on the serialization and comparing the value to the result of public RSA decryption on the provided signature.   |
|rsa2048|2    | same as rsa1024 for 2048 bit keys   |
|rsa4096|3    | same as rsa1024 for 4096 bit keys   |
|sha256|4     | signature is obtained by applying directly SHA256 on serialized frdoc. This method provide a naming solution for frdoc, but is not restrictive since anyone can build new frdoc.   |

## Frdoc EE

Frdocs are dictionnaries with the following EE :
- {n:"type", t:"int", c:2}
- {n:"subtype", t:"ascii", c:3}
- {n:"name", t:"utf8", c:4, dd : ""}
- {n:"signtools", t:"dict", tpl : this.TEMPLATE_SIGNTOOLS, c:5}
- {n:"signature", t:"hex", c:6}
- {n:"expire", t:"int", c:7, dd : 0}
- {n:"dversion", t:"int", c:8, dd : 0}
- {n:"key", t:"hex", c:9, dd : ""}
- {n:"prefix", t:"int", c:10, dd : 0}
- {n:"wingspan", t:"hex", c:11}
- {n:"nextfeather", t:"hex", c:12}
- {n:"body", t:"bin", c:13}
- {n:"featherlength", t:"int", c:14, dd : 0}
- {n:"winglength", t:"int", c:15}
- {n:"author", t:"utf8", c:16}
- {n:"time", t:"int", c:17}
- {n:"keystart", t:"hex", c:18}


**type** is the main type of the document. If the frdoc is the container for a file (say a jpeg, a mp3), then type is the main mime type.
Types are limited to the official mime types, plus a few specific to flock management.

| type | code |
| ---- | ---- |
|text|0|
|multipart|1|
|message|2|
|image|3|
|audio|4|
|video|5|
|application|6|
|font|7|
|example|8|
|model|9|
|x-flock|128|

**subtype** : subtype of the frdoc. It is generally the second part after the "/" in mime types.
They should be written in lower case.

**name** : the name of the frdoc. Should contain the extension if it is a file name

**signtools** : data used to check signature of frdoc. Best described in the signature section

**signature** : signature of the document. described in the signature section
					
**expire** : expiration time in millisecond based on epoch time. Application layer components should not use/transmit frdocs that are expired.
If not set or set to 0, frdoc never expires.

**dversion** : document version of the frdoc. Upon request, if several frdoc with the same key are available and if no specific dversion is required, should prioritize the highest dversion.
					
**key** : key of the frdoc. See wing and feather section to set new keys

**prefix** : for feather frdoc, indicate the length of the prefix that constitutes the key of the wing document.

**wingspan** : must be provided if the document is a wing. Indicates the highest suffix of a feather frdoc.

**nextfeather** : sometimes feathers must be accessed in an order that is not the natural order. Grant a way to indicate the next feather suffix to access

**body** : main binary data encapsulated in the frdoc. It may not be specified for wing frdocs

**featherlength** : can be set in a wing frdoc. It indicates that all the feathers (the last excepted) have the same size. It is useful when trying to access a huge file split in feathers at a specific position

**wingLength** : might be specified for a wing frdoc. Total size of all the body in all the feathers of the wing.

**author** : facultative author name of the frdoc

**time** : facultative time of frdoc creation

**keystart** : when frdoc key is stored ouside the frdoc itself (see signature section), provide a way to indicate that the document key must start with the specific sequence.
If it is not verified, the frdoc should be considered as falsificated
