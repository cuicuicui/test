<!---
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->
# Description

The application layer is responsible of creating and sharing resources. 
It relies on the topology layer to contact other nodes and offer local resources to the whole network.

The flock standards define a way to encode resources to share, the **F**lock **R**esource **F**ormat ([FRF](./FRF/Readme.md)),
and a protocol to exchange those data, the **F**lock **R**esource **P**rotocols ([FRP](./FRP/Readme.md))

