<!---
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->
# Description

NIHttp are nested interfaces operating through http/https communications.
Those interface are short-lived : they shut down after after an exchange of message/answer.
A node send a message to an http server. This triggers the creation of a spawn, which opens until the answer is sent back.

# Canals

Canal associated to the NIHttp have the following properties : 
- type(integer=1) : indicate the canal is for http communication
- address(string) : full address (with protocol and port) of the server to contact

# Data Transmission

For transmission, packets are serialized into bytes, then converted into ascii strings.