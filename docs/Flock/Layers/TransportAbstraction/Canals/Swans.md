<!---
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->
# Description

NISwans are nested interfaces operating through swans infrastructure.
Spawner are built around swans receiver. Spawn are generated when servers transmits requests to receivers(short lived communication), or can be built around caller (long lived communication)

# Canals

Canal associated to the NISwans have the following properties : 
- type(integer=5) : indicate the canal is for swans communication
- receiver(hexadecimal) : id of the receiver
- canals(array of cannals) : cannals to contact the intermediate swans server
