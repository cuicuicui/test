<!---
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->
# FLOCK.js

Flock.js is the javascript (both browser and node.js) implementation of the flock standards. It enables nodes - web browser or nodejs servers alike - to host and share data through peer-to-peer protocols. Though it may be similar to other P2P protocols, the flock protocols were specifically designed to be executed natively on a web browser.

# FLOCK PROTOCOL SUITE

The flock protocol suite is defined by four layers : 

**[Transport abstraction layer](docs/Flock/TransportAbstractionLayer)** : Since nodes implementing the flock protocols may be of very different nature (a web browser, a server side application, a standalone programm...), access to the network varies as well. The transport abstraction layer provide directives to advertise of possible ways (http, websockets, webrtc...) to access the node and specifications to send data through with those methods.

**[Topology layer](sdfsdffff)** : It consists of the definiton of the flock protocol, which standardize ways to advertize and be advertized about peers. Since the Flock is all about peers, it also provides indications about a minimum number of peers to contact in order to be well integrated in the network.

**[Application layer](sdfsdffff)** : It regroups the specifications that defines how is formated and exchanged data. Though additionals protocols can be implemented, it is advised to use the **FRF** (flock resource format) for formating the data and **FRP** (flock resource protocol) to share it with other nodes.

**[End service layer](sdfsdffff)** : It regroups various software that are built on top of the application layer.


Other specifications impacts the various layers : 
**Extensible Entries** (EE) is a binary markup that specifies conversion of objects into binary data.

# JAVASCRIPT IMPLEMENTATION OF FLOCK

Flock.js implements the flock standards through the following component : 

**flaptools** : it provides various transversal tools used by other components, such as the extensible entries encoding/decoding and threads management.

**flock** : it implements the flock specifications. It retrieves information about a specific peer, reach new peers, operate a cron that ensure enough peers are connected. Reciprocally, it shares information about the node and its peers upon requests.

**warehouse** : It is an abstraction of key-value database. It enables components that relies on stored data (such as the frp), to run with different database. Currently run with IndexedDB on browser side and MongoDB and server side.

**frf** : It implements the flock resource format specifications. It transforms raw data into frdoc (flock resource documents). It checks validity of frdocs through a system of signatures

**frp** : It implements the flock resource protocol specifications. Given a key, it fetch the right peers and retrieve the matching frdoc. It implements a system of feed to watch about new documents that falls into a range of keys. It share with connected peers local frdocs.

**nested interfaces** : implementation of the transport abstraction layer. Each type of communication (webrtc, websocket, http, swans...) is encapsulated in a class inheriting the main NestedInterface class

**swans** : **S**ignaling **W**ith **A**nonymous **S**erver. It implements the swans standards and provide a receiver component that receives incoming messages. A caller component to access the receiver. A server component component must be accessible in the network to link the caller and the receiver.

**secswans** : An helper class initially created to provide encryption(RSA, DH, AES) and signing(SHA) to the swans component. Its use was broadened to other compoenents as well.

**flocknode** : a ready to use component that regroups all the other components. 

**aflap** : **A**lpha **FL**ock **AP**plication. End service components that relies on FRP and FRF to create and share contents with peers(audio, video, image, chat...). Rely on a system of displayer to manage new content types.

# INSTALLATION



# EXAMPLES

**[Accessing a flock network with aflap](test)** : a simple test that demonstrate Flock.js can work without a website

**[Extending the aflap application](test)** : Create a displayer to 

**Setting a new flock network infrastructure** : documentation here

```sh
$ npm install --production
$ NODE_ENV=production node app
```


| Plugin | README |
| ------ | ------ |
| Dropbox | [plugins/dropbox/README.md][PlDb] |
| Github | [plugins/github/README.md][PlGh] |
| Google Drive | [plugins/googledrive/README.md][PlGd] |
| OneDrive | [plugins/onedrive/README.md][PlOd] |
| Medium | [plugins/medium/README.md][PlMe] |
| Google Analytics | [plugins/googleanalytics/README.md][PlGa] |

[a relative link](other_file.md)