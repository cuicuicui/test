/**
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

import {Frdisplayer, FrdisplayerMeta}  from "../../src/applications/aflap/lib/displayers/Frdisplayer.js"
import {FrdisplayerAudio}  from "../../src/applications/aflap/lib/displayers/FrdisplayerAudio.js"
import {FrdisplayerVideo}  from "../../src/applications/aflap/lib/displayers/FrdisplayerVideo.js"
import {FrdisplayerFile}  from "../../src/applications/aflap/lib/displayers/FrdisplayerFile.js"
import {FrdisplayerImage}  from "../../src/applications/aflap/lib/displayers/FrdisplayerImage.js"
import {FrdisplayerText}  from "../../src/applications/aflap/lib/displayers/FrdisplayerText.js"
import {FrdisplayerThread}  from "../../src/applications/aflap/lib/displayers/FrdisplayerThread.js"


export {
    Frdisplayer,
    FrdisplayerMeta,
    FrdisplayerAudio,
    FrdisplayerVideo,
    FrdisplayerFile,
    FrdisplayerImage,
    FrdisplayerText,
    FrdisplayerThread,
}

// TODO : solve this dirty hack
// it should work with libraryTarget=window, somehow it doesn't
try{
    let w = window;

    w.Frdisplayer = Frdisplayer;
    w.FrdisplayerMeta = FrdisplayerMeta;
    w.FrdisplayerAudio = FrdisplayerAudio;
    w.FrdisplayerVideo = FrdisplayerVideo;
    w.FrdisplayerFile = FrdisplayerFile;
    w.FrdisplayerImage = FrdisplayerImage;
    w.FrdisplayerText = FrdisplayerText;
    w.FrdisplayerThread = FrdisplayerThread;
}
catch(e){

}