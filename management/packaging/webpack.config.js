/**
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

const path = require('path');
const webpack = require('webpack');
const ReplacePlugin = require('webpack-plugin-replace');
var nodeExternals = require('webpack-node-externals');
const CopyPlugin = require('copy-webpack-plugin');
const RemovePlugin = require('remove-files-webpack-plugin');

// compute some PATH
const RELEASE_PATH = path.resolve(__dirname, "../../dist");
const SRC_PATH = path.resolve(__dirname, "../../src");

module.exports = [];

// merge recursively several objects into a target
var deepMerge = function(target, ...sources){
    for(let source of sources){
        for(let i in source){
            if(Array.isArray(source[i])){
                if(!Array.isArray(target[i])) target[i] = [];
                target[i] = target[i].concat(source[i]);
            }
            else if(typeof source[i] === "object" && source[i].constructor && source[i].constructor.name == "RegExp"){
                target[i] = source[i];
            }
            else if(typeof source[i] === "object"){
                if(!target[i] || typeof target[i] !== "object") target[i] = {};
                deepMerge(target[i], source[i]);
            }
            else{
                target[i] = source[i];
            }
        }
    }
    return target;
}

// common part to every packaging
var licenseHeader = `/**
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
**/`;

let common = {
    optimization: {
        minimize: false
    },
    output : {
        path : RELEASE_PATH,
    },
    // devtool : "",
    externals: [nodeExternals()],
    mode : "production",
    plugins: [
        new webpack.BannerPlugin(licenseHeader),
        new webpack.IgnorePlugin(/^((?!(\.js)).)*$/)
    ],
    node : false
};

///////////////////////////////////////////////////////// packaging for flock library ///////////////////////////

let browserSpecific = {
    target : "web",
    output: {
        filename: './flock-browser.js',
        libraryTarget : "window"
    },
    entry: [
        "./management/packaging/allFlockExports.js"
    ],

};

let nodejsSpecific = {
    target : "node",
    output: {
        filename: './flock-nodejs.js',
        libraryTarget: 'commonjs',
    },
    entry: [
        "./management/packaging/allFlockExports.js"
    ],
};

let browserProdSpecific = {
    optimization: {
        minimize: true
    },
    output: {
        filename: './flock-browser.min.js',
        libraryTarget : "window"
    },
}


let browserExport = deepMerge({}, common, browserSpecific);
let nodejsExport = deepMerge({}, common, nodejsSpecific);

let prod_browserExport = deepMerge({}, browserExport, browserProdSpecific);

module.exports = module.exports.concat([browserExport, nodejsExport, prod_browserExport])


////////////////////////// displayers libraries ////////////////////////////////////////////////

let displayersCommon = {
    entry: [
        "./management/packaging/allDisplayersExport.js"
    ]
};

let displayersBrowserSpecific = {
    target : "web",
    output: {
        filename: './aflap/resources/js/displayers-browser.js',
        libraryTarget : "window"
    },
};

let displayersNodejsSpecific = {
    target : "node",
    output: {
        filename: './aflap/resources/js/displayers-nodejs.js',
        libraryTarget: 'commonjs',
    },
};

let displayersBrowserProdSpecific = {
    optimization: {
        minimize: true
    },
    output: {
        filename: './aflap/resources/js/displayers-browser.min.js',
        libraryTarget : "window",
    },
    plugins : [
        new CopyPlugin([
            { from: path.resolve(SRC_PATH, './applications/aflap/resources') , to: path.resolve(RELEASE_PATH, './aflap/resources') },
            { from: path.resolve(SRC_PATH, './applications/aflap/index.html') , to: path.resolve(RELEASE_PATH, './aflap/index.html')  },
            { from: path.resolve(RELEASE_PATH, './flock-browser.js') , to: path.resolve(RELEASE_PATH, './aflap/resources/js/flock-browser.js')  },
        ])
    ]
}


let displayerBrowserExport = deepMerge({}, common, displayersCommon, displayersBrowserSpecific);
let displayerNodejsExport = deepMerge({}, common,  displayersCommon, displayersNodejsSpecific);

let prod_displayerBrowserExport = deepMerge({}, displayerBrowserExport, displayersBrowserProdSpecific);

module.exports = module.exports.concat([displayerBrowserExport, displayerNodejsExport, prod_displayerBrowserExport])






////////////////////////// OTHER SCRIPTS ///////////////////////////////////////////////////////


let squawkerScript = {
    target : "node",
    entry: [
        "./src/flockNode/lib/SquawkerScript.js"
    ],
    output: {
        filename: './squawkerServer/squawkerScript.js',
        libraryTarget: 'commonjs',
    },
    plugins : [
        new webpack.IgnorePlugin(/flock/)
    ]
};

let squawkerScriptDependencies = {
    target : "node",
    entry: [
        "./management/packaging/blank"
    ],
    output: {
        filename: './squawkerServer/blank.js',
        libraryTarget: 'commonjs',
    },
    plugins : [
        new CopyPlugin([
            { from: path.resolve(RELEASE_PATH, './flock-nodejs.js') , to: path.resolve(RELEASE_PATH, './squawkerServer/flock-nodejs.js')  },
        ]),
        new RemovePlugin({
            after: {
                test: [
                    {
                        folder: path.resolve(RELEASE_PATH, './squawkerServer'),
                        method: (filePath) => {
                            return new RegExp(/blank/, 'm').test(filePath);
                        }
                    }
                ]
            }
        })
    ]
};
squawkerScript = Object.assign({}, common, squawkerScript);



module.exports = module.exports.concat([squawkerScript, squawkerScriptDependencies])




////////////////////////////////