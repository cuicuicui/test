/**
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

// low level dependencies
import {Secswans}  from "../../src/secswans/lib/secswans.js"
import {Flaptools}  from "../../src/flaptools/lib/Flaptools.js"
import { SwanClientReceiver, SwanClientCaller, AbstractSwan }  from "../../src/swans/lib/SwanClient.js"
import { SwanServer }  from "../../src/swans/lib/SwansServer.js"
//
// // transport abstraction layer
import {NestedInterface}  from "../../src/NestedInterface.js/lib/NestedInterface.js"
import {NIHttpSpawner, NIHttpAnswer, NIHttpRequest}  from "../../src/NestedInterface.js/lib/NIHttp.js"
import {NIMeta}  from "../../src/NestedInterface.js/lib/NIMeta.js"
import {NISwansSpawner, NISwansAnswer, NISwansCallerSpawn,}  from "../../src/NestedInterface.js/lib/NISwans.js"
import {NIWebrtcSpawner, NIWebrtcSpawn}  from "../../src/NestedInterface.js/lib/NIWebrtc.js"
import {NIWebsocketSpawner, NIWebsocketSpawn}  from "../../src/NestedInterface.js/lib/NIWebsocket.js"

// topology layer
import {Flock, Formation}  from "../../src/flock/lib/flock.js"

// application layer
import {Warehouse} from '../../src/warehouse/lib/Warehouse.js'
import {MDBWarehouse} from '../../src/warehouse/lib/MDBWarehouse.js'
import {IDBWarehouse} from '../../src/warehouse/lib/IDBWarehouse.js'
import {Frf} from "../../src/frf/lib/Frf.js"
import {Frp} from "../../src/frp/lib/Frp.js"

// high level encapsulation
import {FlockNode}  from "../../src/flockNode/lib/FlockNode.js"
import {Squawker}  from "../../src/flockNode/lib/Squawker.js"






export {
    // low level dependencies

    Flaptools,
    Secswans,
    AbstractSwan,
    SwanClientReceiver,
    SwanClientCaller,
    SwanServer,

    // transport abstraction layer
    NestedInterface,
    NIHttpSpawner,
    NIHttpAnswer,
    NIHttpRequest,
    NIMeta,
    NISwansSpawner,
    NISwansAnswer,
    NISwansCallerSpawn,
    NIWebrtcSpawner,
    NIWebrtcSpawn,
    NIWebsocketSpawner,
    NIWebsocketSpawn,

    // topology layer
    Flock,
    Formation,

    // application layer
    Warehouse,
    IDBWarehouse,
    MDBWarehouse,
    Frf,
    Frp,

    // high level encapsulation
    FlockNode,
    Squawker
}


// TODO : solve this dirty hack
// it should work with libraryTarget=window, somehow it doesn't
try{
    let w = window;

    w.Flaptools = Flaptools;
    w.Secswans = Secswans;
    w.AbstractSwan = AbstractSwan;
    w.SwanClientReceiver = SwanClientReceiver;
    w.SwanClientCaller = SwanClientCaller;
    w.SwanServer = SwanServer;
    w.NestedInterface = NestedInterface;
    w.NIHttpSpawner = NIHttpSpawner;
    w.NIHttpAnswer = NIHttpAnswer;
    w.NIHttpRequest = NIHttpRequest;
    w.NIMeta = NIMeta;
    w.NISwansSpawner = NISwansSpawner;
    w.NISwansAnswer = NISwansAnswer;
    w.NISwansCallerSpawn = NISwansCallerSpawn;
    w.NIWebrtcSpawner = NIWebrtcSpawner;
    w.NIWebrtcSpawn = NIWebrtcSpawn;
    w.NIWebsocketSpawner = NIWebsocketSpawner;
    w.NIWebsocketSpawn = NIWebsocketSpawn;
    w.Flock = Flock;
    w.Formation = Formation;
    w.Warehouse = Warehouse;
    w.IDBWarehouse = IDBWarehouse;
    w.MDBWarehouse = MDBWarehouse;
    w.Frf = Frf;
    w.Frp = Frp;
    w.FlockNode = FlockNode;
    w.Squawker = Squawker;

}
catch(e){

}